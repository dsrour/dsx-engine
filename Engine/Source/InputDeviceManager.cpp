/** InputDeviceManager.cpp
 */

#include "Debug/Debug.h"
#include "InputDeviceManager.h"


////////// KEYBOARD INPUT DEVICE //////////
KeyboardInputDevice::KeyboardInputDevice(std::shared_ptr<SystemManager> systemManager)
{
    mKeyboardDeviceActions = NULL;
    mSystemManager = systemManager;
    for (unsigned int i = 0; i < 127; i++)
    {
        mKeyDown[i] = false;
        mKeyUp[i] = false;        
        mKeyPressed[i] = false;        
        mKeyReleased[i] = true;        
    }
}

/*virtual*/
KeyboardInputDevice::~KeyboardInputDevice(void)
{
    
}

void
KeyboardInputDevice::Advance(void)
{  
	bool do_action = false;

    for (unsigned int i = 0; i < 127; i++)
    {
        if (mKeyPressed[i]) // Pressed
        {                
            if (mKeyDown[i])
                mKeyDown[i] = false;              
            
            do_action = true;    
        }
        else if (mKeyUp[i])
        {            
            if (mKeyReleased[i]) // Released
            {
                mKeyUp[i] = false;                            
                do_action = true;
            }            
        }   
    }     

	if (do_action)
		DoKeyboardActions();
}

void
KeyboardInputDevice::AddKeyboardActions(std::shared_ptr<InputDeviceActions> keyboardActions)
{
    mKeyboardDeviceActions = keyboardActions;
}

void
KeyboardInputDevice::RemoveKeyboardActions(void)
{
    mKeyboardDeviceActions.reset();
}

void
KeyboardInputDevice::SetKeyboardState(KeyboardStateDesc const& desc)
{
    if (-1 == desc.keyVal)
        return;
            
    if (!mKeyPressed[desc.keyVal])
    {
        if (desc.down) // Down
        {
            mKeyDown[desc.keyVal] = true;
            mKeyPressed[desc.keyVal] = true;
                        
            DoKeyboardActions(); 
        }       
    }   
    else if (mKeyPressed[desc.keyVal])
    {
        if (desc.up) // Up
        {
            mKeyPressed[desc.keyVal] = false;
            mKeyReleased[desc.keyVal] = true;                    
            mKeyUp[desc.keyVal] = true;
                        
            DoKeyboardActions(); 
        }
    }             
}

void
KeyboardInputDevice::DoKeyboardActions(void)
{
    if(mKeyboardDeviceActions)
        mKeyboardDeviceActions->OnStateChange();            
}

bool const 
KeyboardInputDevice::IsKeyDown(unsigned int const val) const
{    
    return mKeyDown[val];  
}

bool const
KeyboardInputDevice::IsKeyPressed(unsigned int const val) const
{    
    return mKeyPressed[val];  
}

bool const
KeyboardInputDevice::IsKeyUp(unsigned int const val) const
{
    return mKeyUp[val];    
}

bool const
KeyboardInputDevice::IsKeyReleased(unsigned int const val) const
{
    return mKeyReleased[val];
}





////////// MOUSE INPUT DEVICE //////////
MouseInputDevice::MouseInputDevice(std::shared_ptr<SystemManager> systemManager)
{
    mSystemManager = systemManager;
    mMouseDeviceActions = NULL;
    
    mDefaultState = RELEASED;    
    mButtonStates[LEFT] = mDefaultState;
    mButtonStates[MIDDLE] = mDefaultState;
    mButtonStates[RIGHT] = mDefaultState;
    mX = mY = 0;
	mWheelState = WHEEL_NEUTRAL;

	mRelativeMode = false;
}

void
MouseInputDevice::Advance(void)
{
    for (unsigned int i = 0; i < 3; i++)
    {
        if (DBLE_CLICK == mButtonStates[i] || UNCLICK == mButtonStates[i])
        {
            mButtonStates[i] = RELEASED;
            DoMouseActions();
        }
        else if (CLICK == mButtonStates[i])
        {
            mButtonStates[i] = PRESSED;
            DoMouseActions();
        }
    }

	if (mWheelState != WHEEL_NEUTRAL)
		mWheelState = WHEEL_NEUTRAL;
}

void
MouseInputDevice::AddMouseActions(std::shared_ptr<InputDeviceActions> mouseActions)
{
    mMouseDeviceActions = mouseActions;
}

void
MouseInputDevice::RemoveMouseActions()
{
    mMouseDeviceActions.reset();
}

void
MouseInputDevice::SetMouseState(MouseStateDesc& desc)
{    
    bool do_actions = false;

    for (int i = 0; i < 3; i++)
    {   
        if(desc.doubleClick[i] == true)
        {
            mButtonStates[i] = DBLE_CLICK;       
            do_actions = true;
            continue;
        }
                
        if(desc.buttonUp[i] == 1) // Marked as up
        {
            if (mButtonStates[i] == UNCLICK)
            {
                mButtonStates[i] = RELEASED;
                do_actions = true;
            }
            else if(mButtonStates[i] == CLICK || mButtonStates[i] == PRESSED)
            {
                mButtonStates[i] = UNCLICK;
                do_actions = true;
            }
        }
        else if(desc.buttonUp[i] == 0) // Marked as down
        {
            if (mButtonStates[i] == CLICK)
            {
                mButtonStates[i] = PRESSED;
                do_actions = true;
            }
            else if(mButtonStates[i] == UNCLICK || mButtonStates[i] == RELEASED)
            {
                mButtonStates[i] = CLICK;
                do_actions = true;
            }
        }    
    }

	if (mRelativeMode)
	{		
		if (desc.x != -123456789)
		{
			mX = desc.x;
			mY = desc.y;
			do_actions = true;
		}		
	}
	else
	{
		if (desc.x >= 0 && desc.x != mX)
		{
			mX = desc.x;
			do_actions = true;
		}
		if (desc.y >= 0 && desc.y != mY)    
		{
			mY = desc.y;
			do_actions = true;
		}
	}

	
	if (0 == desc.wheel)
	{
		mWheelState = WHEEL_DOWN;
		do_actions = true;
	}
	else if (1 == desc.wheel)
	{
		mWheelState = WHEEL_UP;
		do_actions = true;
	}
	else	
		mWheelState = WHEEL_NEUTRAL;
	
   
	//OutputDebugMsg(to_string(mX)+", "+ to_string(mY)+"\n");    
    
	if (do_actions)
        DoMouseActions();
}

MouseInputDevice::MouseButtonState const&
MouseInputDevice::ButtonState(MouseInputDevice::MouseButton const& button) const
{
    if (button >= 0 && button < 3)
        return mButtonStates[button];
    else
        return mDefaultState;
}

int const&
MouseInputDevice::MouseX(void) const
{
    return mX;
}

int const&
MouseInputDevice::MouseY(void) const
{
    return mY;
}

void
MouseInputDevice::DoMouseActions(void)
{
    if (mMouseDeviceActions)
        mMouseDeviceActions->OnStateChange();        
        
     // DEBUG OUTPUTS
    //if (mButtonStates[LEFT] == DBLE_CLICK)
    //    OutputDebugMsg("LEFT DOUBLE CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == DBLE_CLICK)
    //    OutputDebugMsg("MIDDLE DOUBLE CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == DBLE_CLICK)
    //    OutputDebugMsg("RIGHT DOUBLE CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[LEFT] == DOWN)
    //    OutputDebugMsg("LEFT DOWN @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == DOWN)
    //    OutputDebugMsg("MIDDLE DOWN @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == DOWN)
    //    OutputDebugMsg("RIGHT DOWN @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[LEFT] == UP)
    //    OutputDebugMsg("LEFT UP @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == UP)
    //    OutputDebugMsg("MIDDLE UP @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == UP)
    //    OutputDebugMsg("RIGHT UP @ [" + to_string(mX) + " " + to_string(mY) + "]\n");    

    //if (mButtonStates[LEFT] == PRESSED)
    //    OutputDebugMsg("LEFT PRESSED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == PRESSED)
    //    OutputDebugMsg("MIDDLE PRESSED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == PRESSED)
    //    OutputDebugMsg("RIGHT PRESSED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");    

    //if (mButtonStates[LEFT] == RELEASED)
    //    OutputDebugMsg("LEFT RELEASED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == RELEASED)
    //    OutputDebugMsg("MIDDLE RELEASED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == RELEASED)
    //    OutputDebugMsg("RIGHT RELEASED @ [" + to_string(mX) + " " + to_string(mY) + "]\n");    

    //if (mButtonStates[LEFT] == DBLE_CLICK)
    //    OutputDebugMsg("LEFT 2X CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[MIDDLE] == DBLE_CLICK)
    //    OutputDebugMsg("MIDDLE 2X CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n");

    //if (mButtonStates[RIGHT] == DBLE_CLICK)
    //    OutputDebugMsg("RIGHT 2X CLICK @ [" + to_string(mX) + " " + to_string(mY) + "]\n"); 

    //OutputDebugMsg("\n");
}

bool const& 
MouseInputDevice::RelativeMode( void ) const
{
	return mRelativeMode;
}
#include <windows.h>
void 
MouseInputDevice::ToggleRelativeMode( bool const& relative )
{
	mRelativeMode = relative;
	

	if (relative)
		while (ShowCursor(FALSE) >= 0);
	else
		while (ShowCursor(TRUE) <= 0);
}

MouseInputDevice::MouseWheelState const& 
MouseInputDevice::WheelState( void ) const
{
	return mWheelState;
}

