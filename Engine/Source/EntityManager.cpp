/**  EntityManager.cpp
 *
 *   TODO:: if efficiency becomes a problem, a lot of optimizations can
 *          be done with all the iteration over stl containers that is going on.
 */


#include "Debug/Debug.h"
#include "GuidGenerator.h"
#include "EntityManager.h"

EntityManager::~EntityManager(void)
{
    mFamRefCnt.clear();
    mComponentRelations.clear();
    mFamilies.clear();
    mEntities.clear();

    mReqIdCnter = 0;
}

unsigned int const
EntityManager::CreateEntity(void)
{
    unsigned int id = GuidGenerator::GenerateGuid();
    mEntities[id];
    return id;
}

void
EntityManager::DestroyEntity(unsigned int const entityId)
{
    std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entity_to_erase;
    entity_to_erase = mEntities.find(entityId);

    if ( mEntities.end() == entity_to_erase )
    {
        OutputDebugMsg( "EntityManager::DestroyEntity, bad entity id (" + to_string(entityId) + ").\n" );
        return;
    }

    // Get a set of famReqIds the entity might be a member of
    std::set<FamReqId> fam_req_ids;
    std::map< ComponentId, std::shared_ptr<IComponent> >::iterator comp_iter;
    for (comp_iter = mEntities[entityId].begin(); comp_iter != mEntities[entityId].end(); comp_iter++)
    {
        std::set<FamReqId>::iterator req_ids_iter;
        for (req_ids_iter = mComponentRelations[comp_iter->first].begin(); req_ids_iter != mComponentRelations[comp_iter->first].end(); req_ids_iter++)       
            fam_req_ids.insert(*req_ids_iter);
    }

    // Check possible families, and if entity is a member, then remove it    
    std::set<FamReqId>::iterator fam_ids_iter;
    for (fam_ids_iter = fam_req_ids.begin(); fam_ids_iter != fam_req_ids.end(); fam_ids_iter++)       
    {        
        mFamilies[*fam_ids_iter].erase(entityId);
    }

    // Finally, get rid of entity
    mEntities.erase(entity_to_erase);    
}

void
EntityManager::AddComponentToEntity(unsigned int const entityId, std::shared_ptr<IComponent> component)
{
    std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entity_to_modify;
    entity_to_modify = mEntities.find(entityId);

    if ( mEntities.end() == entity_to_modify )
    {
        OutputDebugMsg( "EntityManager::AddComponentToEntity, bad entity id (" + to_string(entityId) + ").\n" );
        return;
    }

    // Add the component
    entity_to_modify->second[component->GetComponentTypeGuid()] = component;

    // Get the set of famReqIds this new component relates to
    std::set<FamReqId> fam_req_ids = mComponentRelations[component->GetComponentTypeGuid()];

    // Get set of components belonging to the entity
    std::set<ComponentId> comp_ids;
    WriteEntityComponentsToSet(entityId, comp_ids);

    // Go through each family requirement and see if entity is a member, if so add it to the appropriate family.
    std::set<FamReqId>::iterator fam_req_ids_iter;
    for (fam_req_ids_iter = fam_req_ids.begin(); fam_req_ids_iter != fam_req_ids.end(); fam_req_ids_iter++)
    {
        if ( IsSetContained( comp_ids, mIdToFamReq[*fam_req_ids_iter] ) )
            mFamilies[*fam_req_ids_iter].insert(entityId);
    }
}

void
EntityManager::RemoveComponentFromEntity(unsigned int const entityId, unsigned int const componentId)
{
    std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entity_to_modify;
    entity_to_modify = mEntities.find(entityId);

    if ( mEntities.end() == entity_to_modify )
    {
        OutputDebugMsg( "EntityManager::RemoveComponentFromEntity, bad entity id (" + to_string(entityId) + ").\n" );
        return;
    }

    // Bail out if no such component in entity.
    if ( mEntities[entityId].end() == mEntities[entityId].find(componentId) )
        return;

    // Remove entity from possible families.
    std::set<FamReqId>::const_iterator fam_req_ids;
    for ( fam_req_ids = mComponentRelations[componentId].begin(); fam_req_ids != mComponentRelations[componentId].end(); fam_req_ids++ )
        mFamilies[*fam_req_ids].erase(entityId);

    // Remove component
    entity_to_modify->second.erase(componentId);
}

void
EntityManager::GetComponentFromEntity(unsigned int const entityId, unsigned int const componentId, std::shared_ptr<IComponent>& componentOut)
{    
    std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entity;
    entity = mEntities.find(entityId);

    if ( mEntities.end() == entity )
    {
        //OutputDebugMsg( "EntityManager::GetComponentFromEntity, bad entity id (" + to_string(entityId) + ").\n" );
        componentOut = NULL;
        return;
    }

    std::map< ComponentId, std::shared_ptr<IComponent> >::iterator comp;
    comp = entity->second.find(componentId);

    if ( entity->second.end() == comp )
    {
        //OutputDebugMsg( "EntityManager::GetComponentFromEntity, bad component id (" + to_string(componentId) + ").\n" );
        componentOut = NULL;
        return;
    }

    componentOut = comp->second;
}


std::set<EntityManager::EntityId> const&
EntityManager::CreateFamily(std::set<ComponentId> familyRequirements)
{
    int fam_req_id = -1;

    // Iterate through mIdToFamReq to see if it exists
    std::map< FamReqId, FamReq >::iterator fam_reqs_iter;
    for (fam_reqs_iter = mIdToFamReq.begin(); fam_reqs_iter != mIdToFamReq.end(); fam_reqs_iter++)
    {
        if (familyRequirements == fam_reqs_iter->second)
        {
            fam_req_id = fam_reqs_iter->first;
            break;
        }
    }

    if (-1 == fam_req_id) // create fam req
    {
        fam_req_id = mReqIdCnter++;
        mIdToFamReq[fam_req_id] = familyRequirements;
        mFamRefCnt[fam_req_id] = 1;

        // Add component relations
        std::set<unsigned int>::iterator comp_ids_iter;
        for (comp_ids_iter = familyRequirements.begin(); comp_ids_iter != familyRequirements.end(); comp_ids_iter++)
            mComponentRelations[*comp_ids_iter].insert(fam_req_id);

        // Now go through all current entities and see if they belong to this family
        std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entities_iter;
        for (entities_iter = mEntities.begin(); entities_iter != mEntities.end(); entities_iter++)
        {
            // Get set of components belonging to the entity
            std::set<ComponentId> comp_ids;
            WriteEntityComponentsToSet(entities_iter->first, comp_ids);

            // See if entity is a member of added family req, if so add it to the appropriate family.
            if ( IsSetContained( comp_ids, familyRequirements ) )
                mFamilies[fam_req_id].insert(entities_iter->first);        
        }
    }
    else // similar fam req already there, just increase ref cnt
        mFamRefCnt[fam_req_id] += 1;

    return mFamilies[fam_req_id];
}

void
EntityManager::DestroyFamily(std::set<ComponentId> familyRequirements)
{
    int fam_req_id = -1;

    // Iterate through mIdToFamReq to see if it exists
    std::map< FamReqId, FamReq >::iterator fam_reqs_iter;
    for (fam_reqs_iter = mIdToFamReq.begin(); fam_reqs_iter != mIdToFamReq.end(); fam_reqs_iter++)
    {
        if (familyRequirements == fam_reqs_iter->second)
        {
            fam_req_id = fam_reqs_iter->first;
            break;
        }
    }

    if (-1 == fam_req_id)
    {
        OutputDebugMsg( "EntityManager::DestroyFamily, bad parameter.  No such family requirement exists.\n" );
        return;
    }

    // Lower ref cnt, if no other system depend on the family req, do some cleanup
    if ( 0 == --mFamRefCnt[fam_req_id] )
    {
        // Delete the fam req
        mIdToFamReq.erase(fam_req_id);

        // Delete the ref cnt... not really needed but for clarity sake
        mFamRefCnt.erase(fam_req_id);

        // Get rid of the families
        mFamilies.erase(fam_req_id);

        // Go through the req components and remove the fam req id from the component relations
        std::set<ComponentId>::iterator comp_id_iter;
        for (comp_id_iter = familyRequirements.begin(); comp_id_iter != familyRequirements.end(); comp_id_iter++)
            mComponentRelations[*comp_id_iter].erase(fam_req_id);
    }
}

bool const 
EntityManager::DoesEntityHaveComponent( unsigned int const entityId, unsigned int const componentId )
{
	std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > >::iterator entity;
	entity = mEntities.find(entityId);

	if ( mEntities.end() == entity )
	{
		return false;
	}

	std::map< ComponentId, std::shared_ptr<IComponent> >::iterator comp;
	comp = entity->second.find(componentId);

	if ( entity->second.end() == comp )
	{
		return false;
	}

	return true;
}
