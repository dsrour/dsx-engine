#include "Shlwapi.h"
#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Debug/Debug.h"
#include "InputDeviceManager.h"
#include "BaseApp.h"


// For WM_INPUT
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif


// GLOBALS
extern BaseApp* gApp;
std::shared_ptr<KeyboardInputDevice> gKeyboardInputDevice;
std::shared_ptr<MouseInputDevice> gMouseInputDevice;

LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );

void deleteApp(wchar_t const* msg = NULL, wchar_t const* caption = NULL)
{
    if (gApp)
    {
        delete gApp;
        gApp = NULL;
    }
    
    if (msg != NULL)
        MessageBox(NULL, msg, caption, MB_OK);
}

// Entry point
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{  
#ifdef _DEBUG
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

    // Set current directory        
    TCHAR exe_path[MAX_PATH] = { 0 };
    GetModuleFileName( 0, exe_path, MAX_PATH );
    // Strip the exe filename from path and get folder name.
    PathRemoveFileSpec( exe_path );    
    // Set the current working directory.
    SetCurrentDirectory( exe_path );
 
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );
    
    // Register class      
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_APPLICATION );
    wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"DSX_Engine";
    wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_APPLICATION );
    if( !RegisterClassEx( &wcex ) )
    {
        deleteApp(L"Could not register window class.", L"Window failure");
        return 0;
    }   

    // Create window
    gApp->WinInstance(hInstance);
    RECT rc = { 0, 0, gApp->WinWidth(), gApp->WinHeight() };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    HWND handle = CreateWindow( L"DSX_Engine", gApp->TitleName(), WS_OVERLAPPED | WS_MINIMIZEBOX | WS_SYSMENU,
                                CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
                                NULL );                                    
    if( !handle )
    {
        deleteApp(L"Could not create window handle.", L"Window failure");
        return 0;
    }   
        
    gApp->WinHandle(handle);     

	// Init engine
	if (!gApp->InitEngine())
	{
		deleteApp(L"Could not initialize engine.", L"Engine failure");
		return 0;
	}

    ShowWindow( handle, nCmdShow );
    


    gKeyboardInputDevice = gApp->InputDeviceMngr()->Keyboard();    
    gMouseInputDevice = gApp->InputDeviceMngr()->Mouse();

    // Force the main thread to always run on CPU 0.
	SetThreadAffinityMask(GetCurrentThread(), 1);


	// For WM_INPUT
	RAWINPUTDEVICE Rid[1];
	Rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC; 
	Rid[0].usUsage = HID_USAGE_GENERIC_MOUSE; 
	Rid[0].dwFlags = RIDEV_INPUTSINK;   
	Rid[0].hwndTarget = handle;
	RegisterRawInputDevices(Rid, 1, sizeof(Rid[0]));

        
    MSG msg = {0};
    while ( WM_QUIT != msg.message )
    {           
        if ( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else if ( !gApp->HasEnded() )
        {   
            gApp->AdvanceEngine();            
        }
        else        
            break;
    }
    
    deleteApp(NULL);
    gApp = NULL;
           
    return ( int )msg.wParam;
}

// Window Callback
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    // Send event message to AntTweakBar
    if ( TwEventWin(gApp->WinHandle(), message, wParam, lParam) )
        return 0; // Event has been handled by AntTweakBar

    PAINTSTRUCT ps;
    HDC hdc;

    KeyboardInputDevice::KeyboardStateDesc keyboard_desc;
    MouseInputDevice::MouseStateDesc mouse_desc;

	static bool cursor_showing = true;
    
    switch (message)
    {
        case WM_PAINT:
            hdc = BeginPaint(hWnd, &ps);
            EndPaint(hWnd, &ps);
            break;
                    
        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        /*============================ USER INPUTS ============================*/			
        case WM_KEYDOWN:
            if ( 0 == (lParam & (1<<30)) )
            {
                keyboard_desc.down = true;               
                keyboard_desc.keyVal = (unsigned int)wParam;                
            }
            break;        

        case WM_KEYUP:           
            keyboard_desc.up = true;
            keyboard_desc.keyVal = (unsigned int)wParam;
            break;

        case WM_LBUTTONDOWN:
            mouse_desc.buttonUp[MouseInputDevice::LEFT] = false;
            break;

        case WM_LBUTTONUP:
            mouse_desc.buttonUp[MouseInputDevice::LEFT] = true;
            break;

        case WM_MBUTTONDOWN:
            mouse_desc.buttonUp[MouseInputDevice::MIDDLE] = false;
            break;

        case WM_MBUTTONUP:
            mouse_desc.buttonUp[MouseInputDevice::MIDDLE] = true;
            break;

        case WM_RBUTTONDOWN:
            mouse_desc.buttonUp[MouseInputDevice::RIGHT] = false;
            break;

        case WM_RBUTTONUP:
            mouse_desc.buttonUp[MouseInputDevice::RIGHT] = true;
            break;

        case WM_LBUTTONDBLCLK:
            mouse_desc.doubleClick[MouseInputDevice::LEFT] = true;
            break;

        case WM_MBUTTONDBLCLK:
            mouse_desc.doubleClick[MouseInputDevice::MIDDLE] = true;
            break;

        case WM_RBUTTONDBLCLK:
            mouse_desc.doubleClick[MouseInputDevice::RIGHT] = true;
            break;		

		case WM_MOUSEWHEEL:
			{
				short wheel = (short)HIWORD(wParam);
				if (wheel < 0)
					mouse_desc.wheel = 0;
				else
					mouse_desc.wheel = 1;
				break;	
			}
			
			     
        case WM_MOUSEMOVE:
			{			
				if (!gApp->InputDeviceMngr()->Mouse()->RelativeMode())
				{
					mouse_desc.x = GET_X_LPARAM(lParam);
					mouse_desc.y = GET_Y_LPARAM(lParam); 

					if (!cursor_showing)
					{
						//ShowCursor(true);
						cursor_showing = true;
					}
				}				

				//OutputDebugMsg(to_string(mouse_desc.x)+", "+ to_string(mouse_desc.y)+"\n");
				break;
			}

		case WM_INPUT: 
			{
				if (gApp->InputDeviceMngr()->Mouse()->RelativeMode())
				{
					RAWINPUT raw;
					UINT dwSize = (UINT)sizeof(raw);

					GetRawInputData((HRAWINPUT)lParam, RID_INPUT, 
						&raw, &dwSize, sizeof(RAWINPUTHEADER));

					
					if (raw.header.dwType == RIM_TYPEMOUSE) 
					{
						POINT center = {gApp->WinWidth()/2, gApp->WinHeight()/2};
						ClientToScreen(hWnd, &center);
						SetCursorPos(center.x, center.y);

						int xPosRelative = raw.data.mouse.lLastX;
						int yPosRelative = raw.data.mouse.lLastY;

						mouse_desc.x = xPosRelative;
						mouse_desc.y = yPosRelative; 
					} 
					
					if (cursor_showing)
					{
						//ShowCursor(false);
						cursor_showing = false;
					}

					//static int i = 0;
					//OutputDebugMsg(to_string(i++)+":\t"+to_string(mouse_desc.x)+", "+ to_string(mouse_desc.y)+"\n");
					break;
				}
			}

        /*=====================================================================*/

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }    

    gKeyboardInputDevice->SetKeyboardState(keyboard_desc);
    gMouseInputDevice->SetMouseState(mouse_desc);


    return 0;
}