#include <assert.h>
#include "Debug/Debug.h"
#include "BaseApp.h"

std::wstring gEngineRootDir;
std::wstring gResourcesDir;

BaseApp::BaseApp(void)
{
	// Some default settings
    mWinHeight = 480;
    mWinWidth = 640;

    mTargetFps = 0;
    
    mClientIsLoaded = false;
    mEnded = false;
    
    mEntityManager = std::make_shared<EntityManager>();
    mSystemManager = std::make_shared<SystemManager>();
    mSystemScheduler = std::make_shared<SystemScheduler>(mSystemManager);
    
    mInputDeviceManager = std::make_shared<InputDeviceManager>(mSystemManager);
}

/*virtual*/
BaseApp::~BaseApp(void)
{
 
}

bool const
BaseApp::InitEngine(void)
{    
    assert(!mClientIsLoaded);  

	// Set-up paths from .ini file (a config file should be present for every build directories that has the .exe)
	WCHAR wchar_current_dir[MAX_PATH];
	GetModuleFileName(NULL, wchar_current_dir, MAX_PATH);
	std::wstring::size_type pos = std::wstring(wchar_current_dir).find_last_of(L"\\/");
	std::wstring current_dir = std::wstring(wchar_current_dir).substr(0, pos);
	std::wstring config_file = current_dir + std::wstring(L"\\config.ini");
	WCHAR eng_root[MAX_PATH];
	WCHAR rsrcs_root[MAX_PATH];
	GetPrivateProfileString(L"paths", L"engine_root", L"", eng_root, MAX_PATH, config_file.c_str());
	GetPrivateProfileString(L"paths", L"resources_root", L"", rsrcs_root, MAX_PATH, config_file.c_str());
	gEngineRootDir = eng_root;
	gResourcesDir = rsrcs_root;
	assert(gEngineRootDir != L"" && gResourcesDir != L"");

    // Init renderer        
    mRenderer = std::make_shared<D3dRenderer>(mEntityManager);
    mRendererId = mSystemManager->AddSystem(mRenderer);          
        
    // Init + default settings (camera + others?)
    mRenderer->Init(mWinWidth, mWinHeight, mWinHandle);
    mRenderer->CurrentCamera()->ProjMatrix(DirectX::XM_PIDIV4, (float)mWinWidth, (float)mWinHeight, 0.1f, 10000.0f);    
	TargetFps(60); // 60fps  
    
    // Load client
    if ( !OnLoadApp() )    
        return false;
       
    mClientIsLoaded = true;    
    
    return true;
}

void
BaseApp::EndEngine(void)
{    
    OnUnloadApp();
    mEnded = true;
}

void
BaseApp::AdvanceEngine(void)
{    
    InputDeviceMngr()->Advance();

    OnPreUpdate();

    SystemSchdlr()->Advance(Timer().ElapsedTimeSecs());
    
    OnPostUpdate();
}

void 
BaseApp::TargetFps(unsigned int const fps)
{
    double target = 1.0 / fps;
    mSystemScheduler->StopSystem(mRendererId);
    mSystemScheduler->RunSystemEveryDelta(mRendererId, target);
}


void
BaseApp::TitleName(wchar_t const* titleName)
{    
    mTitleName = titleName;
    SetWindowText(mWinHandle, mTitleName.c_str());
}

void
BaseApp::WinHandle(HWND const handle)
{
    mWinHandle = handle;
}

void
BaseApp::WinHeight(unsigned int const height)
{
    mWinHeight = height;
}

void
BaseApp::WinInstance(HINSTANCE const instance)
{
    mWinInstance = instance;
}

void 
BaseApp::WinWidth(unsigned int const width)
{
    mWinWidth = width;
}