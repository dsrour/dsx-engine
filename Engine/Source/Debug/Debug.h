#ifndef _DEBUG_
#define _DEBUG_

#include <string>
#include <sstream>

/**
 * @brief Converts template type to stl string.
 *
 * Just a simple template.
 *
 * @param What will be changed to a string.
 *
 * @return String
 */
template <class T>
inline std::string to_string (const T& t)
{
    std::stringstream ss;
    ss.precision(10);
    ss << t;
    return ss.str();
}

std::string
to_string(std::wstring wstr);

/**
 * @brief Outputs a debugging message to VS Debug Output window.  
 *
 * @param Debug msg. 
 */
void
OutputDebugMsg(std::string msg);

#endif /*_DEBUG_*/