#include <iostream>
#include <windows.h>
#include "Debug.h"

std::string 
to_string(std::wstring wstr)
{
	return std::string(wstr.begin(), wstr.end());
}

void
OutputDebugMsg(std::string msg)
{
	std::string out = "==============:: " + msg;
	OutputDebugStringA(out.c_str());
}
