/**  Component.hpp
 *
 *  Holds data to be used by systems.
 *
 *  Uses "curiously recurring template pattern" so that it can have its own
 *  GUID even as a derived class.
 *  See: http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
 */

#ifndef _COMPONENT_
#define _COMPONENT_

#include "GuidGenerator.h"

struct IComponent 
{
public:
    virtual unsigned int const
    GetComponentTypeGuid(void) = 0;
};

template <typename T>
class Component : public IComponent
{
public:
    unsigned int const
    GetComponentTypeGuid(void);
        
    static unsigned int const
    GetGuid(void);

private:
    static unsigned int const mGuid;
};

template <typename T>  inline unsigned int const
Component<T>::GetComponentTypeGuid(void)
{
    return GetGuid();
}

template <typename T>  inline unsigned int const
Component<T>::GetGuid(void)
{
    return mGuid;
}

template <typename T> unsigned int const Component<T>::mGuid = GuidGenerator::GenerateGuid();

#endif /*_COMPONENT*/