/// Holds HRTimer class.  
/** A high performance timer for Windows platform. */


#pragma once

#include <windows.h>

class HRTimer
{
public:
    HRTimer(void) 
    { 
        unsigned __int64 pf; 
        QueryPerformanceFrequency((LARGE_INTEGER*)&pf); 
        mFreq = 1.0/(double)pf; 
        Reset(); 
    }

    virtual 
    ~HRTimer(void) {}
           
    double const
    ElapsedTimeSecs(void);
        
protected:
    unsigned __int64  mStart, mStop;
    double mFreq;

private:
    void
    Reset(void);
};

inline double const 
HRTimer::ElapsedTimeSecs(void)
{    
    QueryPerformanceCounter( (LARGE_INTEGER*)&mStop );			    
    return (double)(mStop - mStart) * mFreq;	
}

inline void
HRTimer::Reset(void)
{
    QueryPerformanceCounter( (LARGE_INTEGER*)&mStart );
}