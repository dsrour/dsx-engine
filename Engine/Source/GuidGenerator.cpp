/**  GuidGenerator.cpp
 */

 #include "GuidGenerator.h"

unsigned int GuidGenerator::mIdCounter = 0;

unsigned int const
GuidGenerator::GenerateGuid(void)
{
    return mIdCounter++;
}