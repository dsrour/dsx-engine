/**  System.cpp
 */

#include <assert.h>
#include "Debug/Debug.h"
#include "System.h"

System::System(std::shared_ptr<EntityManager> entityManager)
{
    mEntityManager = entityManager;
    mFamily = NULL;
}

/*virtual*/
System::~System(void)
{
    mFamilyReq.clear();
    mFamily = NULL;
}

void
System::OnRegister(void)
{
    //assert(NULL == mFamily);
    //assert( 0 != mFamilyReq.size() );
    
	if (!mFamilyReq.empty())
	  mFamily =  &(mEntityManager->CreateFamily(mFamilyReq));
}

void
System::OnUnregister(void)
{
    //assert(NULL != mFamily);

	if (!mFamilyReq.empty())
	{
		mEntityManager->DestroyFamily(mFamilyReq);
		mFamily = NULL;
	}
}

void
System::Run(double const currentTime)
{
    RunImplementation(mFamily, currentTime);
}

void
System::SetFamilyRequirements(std::set<unsigned int> famReq)
{
    mFamilyReq = famReq;
}
