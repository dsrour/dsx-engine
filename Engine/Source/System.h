/// Base system class.
/**
 *  A system holds behavior / logic to modify components based on some component requirements (family req).
 *  Concrete systems need to call on SetFamilyRequirements to set requirements and need to implement the RunImplementation function.
 */

#ifndef _SYSTEM_
#define _SYSTEM_


#include <set>
#include "EntityManager.h"
#include "Component.hpp"

class System 
{
public:
    System(std::shared_ptr<EntityManager> entityManager);

    virtual
    ~System(void);

    /** SHOULD NOT MANUALLY CALL! Called by SystemManager when a system is added to it.
        This function will ask the EntityManager to generate the appropriate
        family of entities with this system's family requirements (of components). */
    void
    OnRegister(void);

    /** SHOULD NOT MANUALLY CALL! Called by SystemManager when a system is removed from it.
        This function will ask the EntityManager to destroy the appropriate
        family of entities with this system's family requirements (of components). 
        Note that the entity manager keeps a reference count of the families in case some 
        other system has the same set of requirements. */
    void
    OnUnregister(void);    

    /// Makes a call on protected RunImplementation.
    void
    Run(double const currentTime);

protected:
    void
    SetFamilyRequirements(std::set<unsigned int> famReq);

    virtual void
    RunImplementation(std::set<unsigned int> const* family, double const currentTime) = 0;

    std::shared_ptr<EntityManager> mEntityManager;

private:
    std::set<unsigned int> mFamilyReq;      // id's of required component to belong to family
    std::set<unsigned int> const* mFamily;  // id's of entities that belong to the family    
};

#endif /*_SYSTEM_*/