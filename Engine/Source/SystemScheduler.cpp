/**  SystemScheduler.cpp
 */
 
#include "Debug/Debug.h"
#include "SystemScheduler.h"

SystemScheduler::SystemScheduler(std::shared_ptr<SystemManager> systemManager)
{
    mSystemManager = systemManager;
    mLastTime = 0.0;
}

void
SystemScheduler::Advance(double const currentTime)
{
    double time_difference = currentTime - mLastTime;
                    
    // Run unique events        
    std::map<unsigned int, double>::iterator unique_iter;
    for (unique_iter = mUniqueEvents.begin(); unique_iter != mUniqueEvents.end();)
    {   
        if (currentTime >= unique_iter->second)
        {
            mSystemManager->RunSystem(unique_iter->first, currentTime);
            unique_iter = mUniqueEvents.erase(unique_iter);
        }
        else
            ++unique_iter;
    }

    // Run periodical events        
    std::map<unsigned int, SystemTimeInfo>::iterator periodical_iter;
    for (periodical_iter = mPeriodicalEvents.begin(); periodical_iter != mPeriodicalEvents.end(); periodical_iter++)
    {        
        periodical_iter->second.accumulator += time_difference;                
        
		if (periodical_iter->second.timeStepSimulated)
		{
			while ( periodical_iter->second.accumulator >= periodical_iter->second.delta )
			{     
				if (periodical_iter->second.timeStepSimulated)
					mSystemManager->RunSystem(periodical_iter->first, periodical_iter->second.systemTime);

				periodical_iter->second.accumulator -= periodical_iter->second.delta;       
				periodical_iter->second.systemTime += periodical_iter->second.delta;            
			}
		}
		else
		{
			if (periodical_iter->second.accumulator >= periodical_iter->second.delta)
			{
				periodical_iter->second.accumulator = 0;
				mSystemManager->RunSystem(periodical_iter->first, currentTime);
			}
		}		
    }

    mLastTime = currentTime;
}
    
void
SystemScheduler::RunSystemEveryDelta(unsigned int const systemId, double const delta, bool const timeStepSimulated)
{
    SystemTimeInfo info;
    info.accumulator = info.systemTime = 0.0;
    info.delta = delta;
	info.timeStepSimulated = timeStepSimulated;
    mPeriodicalEvents[systemId] = info;
}

void
SystemScheduler::RunSystemInDelta(unsigned int const systemId, double const delta)
{                   
    mUniqueEvents[systemId] = delta+mLastTime;
}

void
SystemScheduler::RunSystemNow(unsigned int const systemId, double const currentTime /*= 0.0*/)
{
    mSystemManager->RunSystem(systemId, currentTime);
}

void
SystemScheduler::StopSystem(unsigned int const systemId)
{
    mPeriodicalEvents.erase(systemId);
    mUniqueEvents.erase(systemId);
}