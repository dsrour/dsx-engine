/**  Base application class.
 */

#pragma once

#include <windowsx.h>
#include <string>
#include <memory>
#include "Timer.h"
#include "InputDeviceManager.h"
#include "EntityManager.h"
#include "SystemManager.h"
#include "SystemScheduler.h"
#include "Systems/Renderer/Renderer.h"

class BaseApp
{
public:
   /** Default constructor.
    */        
    BaseApp(void);

   /** Destructor.
    */
    virtual 
    ~BaseApp(void);
    
    // Inits engine.  Triggers OnLoadApp().
    bool const
    InitEngine(void);    
    
    // Shuts down engine.  Triggers OnUnloadApp().
    void
    EndEngine(void);

    // Advances engine.
    void
    AdvanceEngine(void);

    // Sets the targeted fps.
    void 
    TargetFps(unsigned int const fps);
    
    // Loads client app.
    virtual bool const
    OnLoadApp(void) = 0;
        
    // Unloads client app.
    virtual void
    OnUnloadApp(void) = 0;

    virtual void
    OnPreUpdate(void) = 0;

    virtual void
    OnPostUpdate(void) = 0;
    




    void
    TitleName(wchar_t const* titleName);
    
    void
    WinHandle(HWND const handle);
    
    void
    WinHeight(unsigned int const height);
    
    void
    WinInstance(HINSTANCE const instance);
    
    void 
    WinWidth(unsigned int const width);   
    
    
    // ====== Access functions ======
    bool const
    HasEnded(void) const;    
    
    wchar_t const*
    TitleName(void) const;    
    
    HWND
    WinHandle(void) const;
    
    unsigned int const
    WinHeight(void) const;
    
    HINSTANCE
    WinInstance(void) const;
    
    unsigned int const
    WinWidth(void) const;

	std::shared_ptr<SystemScheduler>
	SystemSchdlr(void) const;

    std::shared_ptr<EntityManager>
    EntityMngr(void) const;

    std::shared_ptr<SystemManager>
    SystemMngr(void) const;

    std::shared_ptr<InputDeviceManager>
    InputDeviceMngr(void) const;

    std::shared_ptr<D3dRenderer>
    Renderer(void) const;

    HRTimer&
    Timer(void);

protected:   
            
    
private:
    BaseApp*                                mInstance;  
    HRTimer                                 mTimer;   

    unsigned int                            mTargetFps;
           
    HWND                                    mWinHandle; 
    HINSTANCE                               mWinInstance;

    std::shared_ptr<EntityManager>          mEntityManager;
    std::shared_ptr<SystemManager>          mSystemManager;
    std::shared_ptr<SystemScheduler>        mSystemScheduler;

    std::shared_ptr<InputDeviceManager>     mInputDeviceManager;

    std::shared_ptr<D3dRenderer>            mRenderer;
    unsigned int                            mRendererId;
    
    unsigned int                            mWinWidth;
    unsigned int                            mWinHeight;
           
    std::wstring                            mTitleName;  
    
    bool                                    mClientIsLoaded;   
    bool                                    mEnded;   
};


inline bool const
BaseApp::HasEnded(void) const
{
    return mEnded;
}

inline wchar_t const*
BaseApp::TitleName(void) const
{
    return mTitleName.c_str();
}

inline HWND
BaseApp::WinHandle(void) const
{
    return mWinHandle;
}

inline unsigned int const
BaseApp::WinHeight(void) const
{
    return mWinHeight;
}

inline HINSTANCE
BaseApp::WinInstance(void) const
{
    return mWinInstance;
}

inline unsigned int const
BaseApp::WinWidth(void) const
{
    return mWinWidth;
}

inline std::shared_ptr<EntityManager>
BaseApp::EntityMngr(void) const
{
    return mEntityManager;
}

inline std::shared_ptr<SystemManager>
BaseApp::SystemMngr(void) const
{
    return mSystemManager;
}

inline std::shared_ptr<InputDeviceManager>
BaseApp::InputDeviceMngr(void) const
{
    return mInputDeviceManager;
}

inline HRTimer&
BaseApp::Timer(void)
{
    return mTimer;
}

inline std::shared_ptr<D3dRenderer>
BaseApp::Renderer(void) const
{
    return mRenderer;
}

inline std::shared_ptr<SystemScheduler> 
BaseApp::SystemSchdlr( void ) const
{
	return mSystemScheduler;
}


