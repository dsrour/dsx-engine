/**  SystemManager.h
 *
 *  Keeps track of systems and runs them when asked to.
 */

#ifndef _SYSTEM_MANAGER_
#define _SYSTEM_MANAGER_

#include <map>
#include "System.h"

class SystemManager 
{
public:
    SystemManager(void) {}

    ~SystemManager(void);

    unsigned int const
    AddSystem(std::shared_ptr<System> system);

    void
    RemoveSystem(unsigned int const systemId);

    void
    RunSystem(unsigned int const systemId, double const currentTime);
            
private:
    std::map< unsigned int, std::shared_ptr<System> > mSystems;  // system id -> system*
};

#endif /*_SYSTEM_MANAGER_*/