/**  EntityManager.h
 *
 *  Creates entities and keeps track of requested components from systems.
 */

#ifndef _ENTITY_MANAGER_
#define _ENTITY_MANAGER_

#include <memory>
#include <set>
#include <map>
#include "Component.hpp"

class SystemManager;

class EntityManager 
{
public:
    typedef unsigned int            EntityId;
    typedef unsigned int            ComponentId;
    typedef unsigned int            FamReqId;
    typedef std::set<unsigned int>  FamReq;

    EntityManager(void) {}

    ~EntityManager(void);


    unsigned int const
    CreateEntity(void);

    void
    DestroyEntity(unsigned int const entityId);


    void
    AddComponentToEntity(unsigned int const entityId, std::shared_ptr<IComponent> component);

    void
    RemoveComponentFromEntity(unsigned int const entityId, unsigned int const componentId);
    
    void
    GetComponentFromEntity(unsigned int const entityId, unsigned int const componentId, std::shared_ptr<IComponent>& componentOut);


    std::set<EntityId> const&
    CreateFamily(std::set<ComponentId> familyRequirements);

    void
    DestroyFamily(std::set<ComponentId> familyRequirements);


	bool const
	DoesEntityHaveComponent( unsigned int const entityId, unsigned int const componentId );
            
private:    
    // Check if each of containee's element is in container.
    bool const
    IsSetContained( std::set<unsigned int> const& container, std::set<unsigned int> const& containee );

    // Writes all entity's component ids to a set
    void
    WriteEntityComponentsToSet( unsigned int const entityId, std::set<ComponentId>& compIds );
        

    std::map< EntityId, std::map< ComponentId, std::shared_ptr<IComponent> > > mEntities;   // [ entityId : [componentId : component*] ]

    std::map< FamReqId, std::set<EntityId> > mFamilies;                                     // [ famReqId : entitiesIds ]

    std::map< ComponentId, std::set<FamReqId> > mComponentRelations;                        // [ componentId : famReqIds ]

    std::map< FamReqId, unsigned int > mFamRefCnt;                                          // [ famReqId : #_of_ref_frm_systems ]    
    
    std::map< FamReqId, FamReq > mIdToFamReq;                                               // [ famReqId : famReq ] 
       
    unsigned int mReqIdCnter;                                                               // the fam req id counter
};

inline bool const
EntityManager::IsSetContained( std::set<unsigned int> const& container, std::set<unsigned int> const& containee )
{
    if ( container.size() < containee.size() )
        return false;

    // Go through subset and see if each element is in container
    for (std::set<unsigned int>::const_iterator it = containee.begin(); it != containee.end(); it++)
    {
        if ( 0 == container.count(*it) )
            return false;
    }
    
    return true;
}

inline void
EntityManager::WriteEntityComponentsToSet( unsigned int const entityId, std::set<ComponentId>& compIds )
{
    std::map< ComponentId, std::shared_ptr<IComponent> >::iterator comp_iter;
    for (comp_iter = mEntities[entityId].begin(); comp_iter != mEntities[entityId].end(); comp_iter++)
        compIds.insert(comp_iter->first);
}
#endif /*_ENTITY_MANAGER_*/