/**  GuidGenerator.h
 *
 * Generates GUID.
 */

#ifndef _GUID_GENERATOR_
#define _GUID_GENERATOR_

class GuidGenerator
{
public:
    static unsigned int const
    GenerateGuid(void);
        
private:
    static unsigned int mIdCounter;
};

#endif /*_GUID_GENERATOR_*/