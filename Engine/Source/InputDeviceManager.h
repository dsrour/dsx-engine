/// This file has input device classes.

#pragma once

#include <memory>
#include <set>
#include "SystemManager.h"

class InputDeviceManager;

///INPUT DECIVE ACTIONS
/** This is a base class. Client must implement OnStateChange().
 */
class InputDeviceActions
{
public:
    InputDeviceActions(std::shared_ptr<InputDeviceManager> inputDeviceManager)
    {
        mInputDeviceManager = inputDeviceManager;
    }

    virtual void
    OnStateChange(void) = 0;        

protected:
    // Weak ptr since InputDeviceManager has shared_ptr of me (this '*' ptr) resulting in circular ref.
    std::weak_ptr<InputDeviceManager> mInputDeviceManager;  
};



class KeyboardInputDevice
{
public:
    struct KeyboardStateDesc
    {
        KeyboardStateDesc(void)
        {
            keyVal = -1;            
            down = false;
            up = false;
        }

        int keyVal;        
        bool down;        
        bool up;                        
    };

    KeyboardInputDevice(std::shared_ptr<SystemManager> systemManager);

    virtual    
    ~KeyboardInputDevice(void);

    void
    Advance(void);

    void
    AddKeyboardActions(std::shared_ptr<InputDeviceActions> keyboardActions);

    void
    RemoveKeyboardActions(void);
    
    /**
     * Sets keyboard state.
     * 
     * Note that if KeyboardStateDesc.keyVal = -1, nothing happens.
     *
     * @param KeyboardStateDesc
     */
    void
    SetKeyboardState(KeyboardStateDesc const& desc);                     

    // INQUIRIES
    /**
     * Checks if key was pressed down.  1 frame timed event.
     */
    bool const IsKeyDown(unsigned int const val) const;

    /**
     * Checks if key has been pressed.
     */
    bool const
    IsKeyPressed(unsigned int const val) const;

    /**
     * Checks if key was let go.  1 frame timed event.
     */
    bool const
    IsKeyUp(unsigned int const val) const;

    /**
     * Checks if key has been released.
     */
    bool const
    IsKeyReleased(unsigned int const val) const;

private:
    void
    DoKeyboardActions(void);

    std::shared_ptr<SystemManager> mSystemManager;
    
    // Regular ASCII
    bool         mKeyDown[255];
    bool         mKeyUp[255];
    bool         mKeyPressed[255];
    bool         mKeyReleased[255];

    std::shared_ptr<InputDeviceActions>   mKeyboardDeviceActions;
};

class MouseInputDevice
{
public:
    enum MouseButtonState
    {
        PRESSED,
        RELEASED,
        CLICK,
        UNCLICK,
        DBLE_CLICK
    };

	enum MouseWheelState
	{		
		WHEEL_NEUTRAL,
		WHEEL_DOWN,
		WHEEL_UP,		
	};

    enum MouseButton
    {
    LEFT,
    MIDDLE,
    RIGHT
    };

    struct MouseStateDesc
    {
        int buttonUp[3]; // -1 default, 0 down, 1 up
        bool doubleClick[3];
        int x, y;
		int wheel; // -1 default, 0 down, 1 up

        MouseStateDesc()
        {
             buttonUp[0] = buttonUp[1] = buttonUp[2] = -1;
             doubleClick[0] = doubleClick[1] = doubleClick[2] = false;
             x = -123456789; // did not move TODO:: so fuckin hacky...
             y = -123456789; // did not move
			 wheel = -1;
        }
    };

    MouseInputDevice(std::shared_ptr<SystemManager> systemManager);

    /*virtual    
    ~MouseInputDevice(void);*/

    void
    AddMouseActions(std::shared_ptr<InputDeviceActions> mouseActions);

    void
    Advance(void);

    void
    RemoveMouseActions(void);
    
    /**
     * Sets Mouse state.
     *     
     * @param KeyboardStateDesc
     */
    void
    SetMouseState(MouseStateDesc& desc);                     

    MouseButtonState const&
    ButtonState(MouseButton const& button) const;

	MouseWheelState const&
	WheelState(void) const;

    int const&
    MouseX(void) const;

    int const&
    MouseY(void) const;

	void
	ToggleRelativeMode(bool const& relative);

	bool const&
	RelativeMode(void) const;

private:
    void
    DoMouseActions(void);

    MouseButtonState  mDefaultState;

    std::shared_ptr<SystemManager> mSystemManager;
    
    MouseButtonState mButtonStates[3];
	MouseWheelState  mWheelState;
    int mX, mY;

    std::shared_ptr<InputDeviceActions>   mMouseDeviceActions;

	bool mRelativeMode;
};



class InputDeviceManager
{
public:
    InputDeviceManager(std::shared_ptr<SystemManager> systemManager)
    {
        mKeyboard = std::make_shared<KeyboardInputDevice>(systemManager);
        mMouse = std::make_shared<MouseInputDevice>(systemManager);
    }

    void
    Advance(void) { mKeyboard->Advance(); mMouse->Advance(); }

    std::shared_ptr<KeyboardInputDevice>
    Keyboard(void) const { return mKeyboard; }

    std::shared_ptr<MouseInputDevice>
    Mouse(void) const { return mMouse; }
    
private:
    // KEYBOARD //
    std::shared_ptr<KeyboardInputDevice>                mKeyboard;    
    std::shared_ptr<MouseInputDevice>                   mMouse;    
};