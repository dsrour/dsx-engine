#include <d3dcompiler.h>
#include <string>
#include <assert.h>
#include <algorithm>
#include "Debug/Debug.h"
#include "Components/ShadowEmitting.h"
#include "Components/LightEmitting.h"
#include "Components/Bounded.h"
#include "BaseApp.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "ShadowSystem.h"

extern BaseApp* gApp;



/* TODO:
 *
 * - Take the camera's current velocity into consideration when calculating the cascades' frustums... 
 *	 Since there is a frame CPU read-back delay, we need to know what the camera will "see" in the future frame.
 *
 * - Culling of objects from the frustum is not correct because the shadow camera has a "tight" frustum. 
 *   Anything behind the light stops casting shadows.  This is really noticeable when a shadow caster is 
 *   behind the main camera and casting shadows in front of the camera. For now, culling test always passes.
 *
 * - Fix/Test/Finish the instanced geometry implementation.
 */


ShadowSystem::ShadowSystem(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
{
	// Set family req... when RunImplementation is ran, the family should consist of lights that emit shadows.
	std::set<unsigned int> fam_req;
	fam_req.insert(ShadowEmitting::GetGuid());       
	fam_req.insert(LightEmitting::GetGuid());
	SetFamilyRequirements(fam_req);  
	
	
	InitializeResources();


	mMinClip = 0.f;
	mMaxClip = 1.f;


	mReductionConstants.Create(gApp->Renderer()->Device());
	ZeroMemory(&mReductionConstantVariables, sizeof(ReductionCb));

	mDepthOnlyConstants.Create(gApp->Renderer()->Device());
	ZeroMemory(&mDepthOnlyConstantVariables, sizeof(DepthOnlyCb));


	mCurrFrame = 0;


	mShadowCastingFetcherSys = std::make_shared<ShadowCastingFetcherSystem>(entityManager);
	mShadowCastingFetcherSysId = gApp->SystemMngr()->AddSystem(mShadowCastingFetcherSys);


			
	// Create depth only blend desc where color writing is disabled
	mDepthOnlyBlendDesc.AlphaToCoverageEnable = false;
	mDepthOnlyBlendDesc.IndependentBlendEnable = false;
	for (auto i = 0u; i < 8; ++i)
	{
		mDepthOnlyBlendDesc.RenderTarget[i].BlendEnable = false;
		mDepthOnlyBlendDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_ADD;
		mDepthOnlyBlendDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		mDepthOnlyBlendDesc.RenderTarget[i].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		mDepthOnlyBlendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ONE;
		mDepthOnlyBlendDesc.RenderTarget[i].RenderTargetWriteMask = 0;
		mDepthOnlyBlendDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		mDepthOnlyBlendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;
	}



	// Enable depth stencil testing
	mDepthOnlyStencilDesc.DepthEnable = true;
	mDepthOnlyStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	mDepthOnlyStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	mDepthOnlyStencilDesc.StencilEnable = false;
	mDepthOnlyStencilDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	mDepthOnlyStencilDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	mDepthOnlyStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthOnlyStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthOnlyStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
	mDepthOnlyStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	mDepthOnlyStencilDesc.BackFace = mDepthOnlyStencilDesc.FrontFace;
}

void 
ShadowSystem::RunImplementation(std::set<unsigned int> const* family, double const /*currentTime*/)
{
	if (mCurrFrame < SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY)
		return;

	auto dev_c = gApp->Renderer()->DeviceContext();

	// Get a list of shadow casting renderables
	gApp->SystemSchdlr()->RunSystemNow(mShadowCastingFetcherSysId);
	auto shadow_casting_entities = mShadowCastingFetcherSys->ShadowCastingEntities();

	//if (shadow_casting_entities.empty())
	//	return;

	// NOTE: for right now, shadow maps are only processed for directional lights.
	std::set<unsigned int> directional_lights;
	GetDirectionalLightsFromFamily(family, directional_lights);

	dev_c->PSSetShader(nullptr, nullptr, 0);
		
	// Now that we have a list of all directional lights that create shadows and the renderables that cast them,
	// it's time to render the shadow maps.
	for (auto light_id : directional_lights)
	{
		if (dev_c->IsAnnotationEnabled())
			dev_c->BeginEventInt(L"Shadow Maps", 0);

		RenderDirectionalLightCascades(light_id, shadow_casting_entities);

		if (dev_c->IsAnnotationEnabled())
			dev_c->EndEvent();
	}

	// Reset viewport to window size
	D3D11_VIEWPORT viewport;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width = (float)gApp->WinWidth();
	viewport.Height = (float)gApp->WinHeight();
	dev_c->RSSetViewports(1, &viewport);

	// Unbind resources
	dev_c->VSSetShader(nullptr, nullptr, 0);
	ID3D11Buffer* nb = nullptr;
	dev_c->VSSetConstantBuffers(0, 1, &nb);
}

void 
ShadowSystem::InitializeResources()
{
	mDepthReductionResources.tex2D.clear();
	mDepthReductionResources.uav.clear();
	mDepthReductionResources.srv.clear();
	mDepthReductionResources.dispX.clear();
	mDepthReductionResources.dispY.clear();

	unsigned int w = gApp->WinWidth();
	unsigned int h = gApp->WinHeight();

	unsigned int const reduction_grid_size = 16u;

	// Calculate dispatch args
	while (w > 1 || h > 1)
	{
		int x = w / reduction_grid_size;
		x += w % reduction_grid_size > 0 ? 1 : 0;

		int y = w / reduction_grid_size;
		y += w % reduction_grid_size > 0 ? 1 : 0;

		mDepthReductionResources.dispX.push_back(x);
		mDepthReductionResources.dispY.push_back(y);

		w = x;
		h = y;
	}


	// Create reduction textures
	auto num_buffers = mDepthReductionResources.dispX.size();
	mDepthReductionResources.tex2D.resize(num_buffers);
	mDepthReductionResources.uav.resize(num_buffers);
	mDepthReductionResources.srv.resize(num_buffers);
	
	for (auto i = 0; i < num_buffers; i++)
	{
		D3D11_TEXTURE2D_DESC tex_desc;
		ZeroMemory(&tex_desc, sizeof(tex_desc));
		tex_desc.Width = mDepthReductionResources.dispX[i];
		tex_desc.Height = mDepthReductionResources.dispY[i];
		tex_desc.ArraySize = 1;
		tex_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		tex_desc.CPUAccessFlags = 0;
		tex_desc.Format = DXGI_FORMAT_R16G16_UNORM; // min and max depth values
		tex_desc.MipLevels = 1;
		tex_desc.SampleDesc.Count = 1;
		tex_desc.SampleDesc.Quality = 0;
		tex_desc.Usage = D3D11_USAGE_DEFAULT;
		tex_desc.MiscFlags = 0;
		HRESULT hr = gApp->Renderer()->Device()->CreateTexture2D(&tex_desc, nullptr, mDepthReductionResources.tex2D[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
		ZeroMemory(&uav_desc, sizeof(uav_desc));
		uav_desc.Format = DXGI_FORMAT_R16G16_UNORM;
		uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		uav_desc.Texture2D.MipSlice = 0; 
		hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mDepthReductionResources.tex2D[i].Get(), &uav_desc, mDepthReductionResources.uav[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
		ZeroMemory(&srv_desc, sizeof(srv_desc));
		srv_desc.Format = DXGI_FORMAT_R16G16_UNORM;		
		srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv_desc.Texture2D.MipLevels = 1;
		srv_desc.Texture2D.MostDetailedMip = 0;
		hr = gApp->Renderer()->Device()->CreateShaderResourceView(mDepthReductionResources.tex2D[i].Get(), &srv_desc, mDepthReductionResources.srv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));
	}

	// Create staging resources
	D3D11_TEXTURE2D_DESC tex_desc;
	ZeroMemory(&tex_desc, sizeof(tex_desc));
	tex_desc.Width = 1;
	tex_desc.Height = 1;
	tex_desc.ArraySize = 1;
	tex_desc.BindFlags = 0;
	tex_desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	tex_desc.Format = DXGI_FORMAT_R16G16_UNORM; // min and max depth values
	tex_desc.MipLevels = 1;
	tex_desc.SampleDesc.Count = 1;
	tex_desc.SampleDesc.Quality = 0;
	tex_desc.Usage = D3D11_USAGE_STAGING;
	tex_desc.MiscFlags = 0;
	for (auto i = 0; i <= SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY; i++)
	{
		HRESULT hr = gApp->Renderer()->Device()->CreateTexture2D(&tex_desc, nullptr, mDepthReductionResources.stagingTex2D[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));
	}


	// Create shaders
	std::wstring cso_name;
	
	cso_name = std::wstring(DEPTH_REDUCTION_INIT_CS_FILE) + L".cso";
	HRESULT hr = D3DReadFileToBlob(cso_name.c_str(), mDepthReductionInitCsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = gApp->Renderer()->Device()->CreateComputeShader(
		mDepthReductionInitCsBlob->GetBufferPointer(),
		mDepthReductionInitCsBlob->GetBufferSize(),
		NULL,
		mDepthReductionInitCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(DEPTH_REDUCTION_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mDepthReductionCsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = gApp->Renderer()->Device()->CreateComputeShader(
		mDepthReductionCsBlob->GetBufferPointer(),
		mDepthReductionCsBlob->GetBufferSize(),
		NULL,
		mDepthReductionCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(DEPTH_ONLY_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mDepthOnlyVsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = gApp->Renderer()->Device()->CreateVertexShader(
		mDepthOnlyVsBlob->GetBufferPointer(),
		mDepthOnlyVsBlob->GetBufferSize(),
		NULL,
		mDepthOnlyVs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(DEPTH_ONLY_INSTANCED_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mDepthOnlyInstancedVsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = gApp->Renderer()->Device()->CreateVertexShader(
		mDepthOnlyInstancedVsBlob->GetBufferPointer(),
		mDepthOnlyInstancedVsBlob->GetBufferSize(),
		NULL,
		mDepthOnlyInstancedVs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
}

void 
ShadowSystem::UpdateClipDistances(ID3D11ShaderResourceView* const depthSrv)
{
	auto device_context = gApp->Renderer()->DeviceContext();

	if (device_context->IsAnnotationEnabled())
		device_context->BeginEventInt(L"Shadow Clip Distances Update", 0);

	// Apply constants and bind cb
	auto camera = gApp->Renderer()->CurrentCamera();
	mReductionConstantVariables.projMat = DirectX::XMMatrixTranspose(XMLoadFloat4x4(&camera->ProjMatrix()));
	mReductionConstantVariables.nearPlane = camera->NearZ();
	mReductionConstantVariables.farPlane = camera->FarZ();
	mReductionConstants.SetData(device_context, mReductionConstantVariables);
	auto cb = mReductionConstants.GetBuffer();
	device_context->CSSetConstantBuffers(0, 1, &cb);

	ID3D11UnorderedAccessView* uavs[1];
	ID3D11ShaderResourceView* srvs[1];

	// Setup and run initial pass
	uavs[0] = mDepthReductionResources.uav[0].Get();
	srvs[0] = depthSrv;
	device_context->CSSetUnorderedAccessViews(0, 1, uavs, nullptr);
	device_context->CSSetShaderResources(0, 1, srvs);
	device_context->CSSetShader(mDepthReductionInitCs.Get(), nullptr, 0);
	device_context->Dispatch(mDepthReductionResources.dispX[0], mDepthReductionResources.dispY[0], 1);

	// Setup and run remaining passes
	device_context->CSSetShader(mDepthReductionCs.Get(), nullptr, 0);
	for (auto i = 1; i < mDepthReductionResources.uav.size(); i++)
	{
		uavs[0] = mDepthReductionResources.uav[i].Get();
		srvs[0] = mDepthReductionResources.srv[i-1].Get();
		device_context->CSSetUnorderedAccessViews(0, 1, uavs, nullptr);
		device_context->CSSetShaderResources(0, 1, srvs);
		device_context->Dispatch(mDepthReductionResources.dispX[i], mDepthReductionResources.dispY[i], 1);
	}

	// Unbind all
	uavs[0] = nullptr;
	srvs[0] = nullptr;
	cb = nullptr;
	device_context->CSSetUnorderedAccessViews(0, 1, uavs, nullptr);
	device_context->CSSetShaderResources(0, 1, srvs);
	device_context->CSSetConstantBuffers(0, 1, &cb);
	device_context->CSSetShader(nullptr, nullptr, 0);

	// Can now copy results to staging buffer
	unsigned int const latency = SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY + 1;
	device_context->CopyResource(
		mDepthReductionResources.stagingTex2D[mCurrFrame%latency].Get(), 
		mDepthReductionResources.tex2D[mDepthReductionResources.tex2D.size() - 1].Get());

	// Update current frame and readback from previous frame
	mCurrFrame++;

	if (mCurrFrame >= SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY)
	{
		D3D11_MAPPED_SUBRESOURCE mapped;
		HRESULT hr = device_context->Map(
			mDepthReductionResources.stagingTex2D[mCurrFrame%latency].Get(),
			0,
			D3D11_MAP_READ,
			D3D11_MAP_FLAG_DO_NOT_WAIT,
			&mapped);

		if (hr == DXGI_ERROR_WAS_STILL_DRAWING)
		{
			OutputDebugMsg("ShadowSystem: Could not read-back depth reduction within frame latency... GPU was still busy. \n");
			mCurrFrame--;
		}
		else if (!SUCCEEDED(hr))
			assert(false);
		else if (SUCCEEDED(hr))
		{
			uint16_t* results = reinterpret_cast<uint16_t*>(mapped.pData);
			mMinClip = results[0] / static_cast<float>(0xffff);
			mMaxClip = results[1] / static_cast<float>(0xffff);

			device_context->Unmap(
				mDepthReductionResources.stagingTex2D[mCurrFrame%latency].Get(),
				0);

			//OutputDebugMsg("Min Depth = " + to_string(mMinClip) + "\t Max Depth = " + to_string(mMaxClip) + "\n");
		}

	}
	else // this shouldn't matter unless the ShadowSystem's mCurrFrame is somehow reset...
	{
		mMinClip = 0.f;
		mMaxClip = 1.f;
	}

	if (device_context->IsAnnotationEnabled())
		device_context->EndEvent();
}

void 
ShadowSystem::GetDirectionalLightsFromFamily(std::set<unsigned int> const* family, std::set<unsigned int>& directionalLightsOut)
{
	std::shared_ptr<IComponent> tmp_component;

	for (auto l : *family)
	{
		gApp->EntityMngr()->GetComponentFromEntity(l, LightEmitting::GetGuid(), tmp_component);
		if (tmp_component)
		{
			auto light_emitting = (LightEmitting*)(tmp_component.get());
			if (light_emitting->Type() == DIRECTIONAL_LIGHT)
				directionalLightsOut.insert(l);
		}
	}
}

void 
ShadowSystem::RenderDirectionalLightCascades(unsigned int const lightId, std::set<unsigned int> const& shadowCastingEntities)
{
	// fetch required components
	LightEmitting* light_emitting = nullptr;
	ShadowEmitting* shadow_emitting = nullptr;
	{
		std::shared_ptr<IComponent> tmp_component;
		gApp->EntityMngr()->GetComponentFromEntity(lightId, LightEmitting::GetGuid(), tmp_component);
		assert(tmp_component);
		light_emitting = (LightEmitting*)(tmp_component.get());
		gApp->EntityMngr()->GetComponentFromEntity(lightId, ShadowEmitting::GetGuid(), tmp_component);
		assert(tmp_component);
		shadow_emitting = (ShadowEmitting*)(tmp_component.get());
	}

	float sm_size = (float)shadow_emitting->ShadowMapSize();

	unsigned int num_cascades = (unsigned int)shadow_emitting->NumCascades();

	std::vector<float> cascade_splits(num_cascades, 0.f);

	// Calculate a logarithmic split based on the depth values of the scene
	auto camera = gApp->Renderer()->CurrentCamera();
	float cam_near = camera->NearZ();
	float cam_far = camera->FarZ();
	float z_range = cam_far - cam_near;

	float min_z = cam_near + mMinClip * z_range;
	float max_z = cam_near + mMaxClip * z_range;

	float range = max_z - min_z;
	float ratio = max_z / min_z;

	for (auto i = 0u; i < num_cascades; i++)
	{
		float p = (i + 1) / static_cast<float>(num_cascades);
		float log = min_z * std::pow(ratio, p);
		float uniform = min_z + range * p;
		float d = (log - uniform) + uniform;
		cascade_splits[i] = (d - cam_near) / z_range;
	}

	auto directional_light = (DirectionalLight*)light_emitting->GetLight();
	auto device_context = gApp->Renderer()->DeviceContext();
	
	D3D11_VIEWPORT viewport;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	// Set viewport to match cascade
	viewport.Width = (float)shadow_emitting->ShadowMapSize();
	viewport.Height = (float)shadow_emitting->ShadowMapSize();
	device_context->RSSetViewports(1, &viewport);

	// Set states
	float blend_factor[4] = { 1, 1, 1, 1 };
	auto blend_state = gApp->Renderer()->StateMngr()->GetOrCreateBlendState(gApp->Renderer()->Device(), mDepthOnlyBlendDesc);
	gApp->Renderer()->DeviceContext()->OMSetBlendState(blend_state, blend_factor, 0xFFFFFFFF);

	auto ds_state = gApp->Renderer()->StateMngr()->GetOrCreateDepthStencilState(gApp->Renderer()->Device(), mDepthOnlyStencilDesc);
	gApp->Renderer()->DeviceContext()->OMSetDepthStencilState(ds_state, 0);

	// No cull with depth clipping disabled...
	// This is to implement the "pancake" technique... since we are making tight frustums, it'll
	// allow out of frustum objects to flatten on the frustum planes as to not clip shadows.
	gApp->Renderer()->DeviceContext()->RSSetState(gApp->Renderer()->StateMngr()->RasterizerState(RenderStateManager::NoCullDepthClipDisabled));
	
	// Update the shadow matrix within the ShadowEmitting component
	auto global_shadow_matrix = MakeDirectionalLightGlobalShadowMatrix(directional_light->direction);
	DirectX::XMStoreFloat4x4(&shadow_emitting->ShadowMatrix(), global_shadow_matrix);

	// Render to each cascade
	for (auto i = 0u; i < num_cascades; i++)
	{
		auto context = gApp->Renderer()->DeviceContext();
		// Set shadow map as depth target
		auto dsv = shadow_emitting->SmcResources().slices[i].Get();
		ID3D11RenderTargetView* null_rts[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT] = { nullptr };
		context->OMSetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, null_rts, dsv);
		context->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

		DirectX::XMVECTOR frustum_corners_ws[8] =
		{
			DirectX::XMVectorSet(-1.0f, 1.0f, 0.0f, 1.f),
			DirectX::XMVectorSet(1.0f, 1.0f, 0.0f, 1.f),
			DirectX::XMVectorSet(1.0f, -1.0f, 0.0f, 1.f),
			DirectX::XMVectorSet(-1.0f, -1.0f, 0.0f, 1.f),
			DirectX::XMVectorSet(-1.0f, 1.0f, 1.0f, 1.f),
			DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.f),
			DirectX::XMVectorSet(1.0f, -1.0f, 1.0f, 1.f),
			DirectX::XMVectorSet(-1.0f, -1.0f, 1.0f, 1.f),
		};

		float prev_split_dist = i == 0 ? mMinClip : cascade_splits[i - 1];
		float split_dist = cascade_splits[i];

		auto camera = gApp->Renderer()->CurrentCamera();
		DirectX::XMMATRIX inv_view_proj = DirectX::XMMatrixInverse(nullptr, XMLoadFloat4x4(&camera->ViewProjMatrix()));

		// Put corners in world space
		for (auto j = 0; j < 8; j++)
			frustum_corners_ws[j] = DirectX::XMVector3TransformCoord(frustum_corners_ws[j], inv_view_proj);

		// Get the corners of the current cascade slice of the view frustum
		for (auto j = 0u; j < 4; ++j)
		{
			DirectX::XMVECTOR corner_ray = DirectX::XMVectorSubtract(frustum_corners_ws[j + 4], frustum_corners_ws[j]);
			DirectX::XMVECTOR near_corner_ray = DirectX::XMVectorMultiply(
				corner_ray, 
				DirectX::XMVectorSet(prev_split_dist, prev_split_dist, prev_split_dist, prev_split_dist));
			DirectX::XMVECTOR far_corner_ray = DirectX::XMVectorMultiply(
				corner_ray,
				DirectX::XMVectorSet(split_dist, split_dist, split_dist, split_dist));
			frustum_corners_ws[j + 4] = DirectX::XMVectorAdd(frustum_corners_ws[j], far_corner_ray);
			frustum_corners_ws[j] = DirectX::XMVectorAdd(frustum_corners_ws[j], near_corner_ray);
		}

		// Calculate center
		DirectX::XMVECTOR frustum_center = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		for (auto j = 0; j < 8; ++j)
			frustum_center = DirectX::XMVectorAdd(frustum_center, frustum_corners_ws[j]);
		frustum_center = DirectX::XMVectorMultiply(
			frustum_center,
			DirectX::XMVectorSet(1.0f / 8.0f, 1.0f / 8.0f, 1.0f / 8.0f, 1.0f / 8.0f));

		DirectX::XMVECTOR up_dir = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);
		DirectX::XMVECTOR light_dir = DirectX::XMLoadFloat3(&directional_light->direction);
		light_dir = DirectX::XMVectorMultiply(light_dir, DirectX::XMVectorSet(-1.f, -1.f, -1.f, 1.f));

		DirectX::XMVECTOR look_at = DirectX::XMVectorSubtract(frustum_center, light_dir);
		DirectX::XMMATRIX light_view = DirectX::XMMatrixLookAtLH(frustum_center, look_at, up_dir);

		// Calculate frustum aabb
		DirectX::XMVECTOR mins = DirectX::XMVectorSet(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);
		DirectX::XMVECTOR maxs = DirectX::XMVectorSet(-FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX);
		for (auto j = 0; j < 8; ++j)
		{
			DirectX::XMVECTOR corner = DirectX::XMVector3TransformCoord(frustum_corners_ws[j], light_view);
			mins = DirectX::XMVectorMin(mins, corner);
			maxs = DirectX::XMVectorMax(maxs, corner);
		}

		// Adjust the min/max to accommodate the filtering size
		unsigned int const filter_size = 2; 
		float scale = ((float)shadow_emitting->ShadowMapSize() + filter_size) / static_cast<float>(shadow_emitting->ShadowMapSize());
		auto scale_vec = DirectX::XMVectorSet(scale, scale, 1, 1);
		mins = DirectX::XMVectorMultiply(mins, scale_vec);
		maxs = DirectX::XMVectorMultiply(maxs, scale_vec);

		DirectX::XMVECTOR cascade_extents = DirectX::XMVectorSubtract(maxs, mins);

		DirectX::XMFLOAT3 mins_floats; DirectX::XMStoreFloat3(&mins_floats, mins);
		DirectX::XMFLOAT3 maxs_floats; DirectX::XMStoreFloat3(&maxs_floats, maxs);
		DirectX::XMFLOAT3 cascade_extents_floats; DirectX::XMStoreFloat3(&cascade_extents_floats, cascade_extents);

		// Get position of the shadow camera
		DirectX::XMVECTOR shadow_cam_pos = DirectX::XMVectorAdd(
			frustum_center, 
			DirectX::XMVectorMultiply(light_dir, DirectX::XMVectorSet(-mins_floats.z, -mins_floats.z, -mins_floats.z, -mins_floats.z)));

		// Create orthographic camera for the shadow cascade to render into
		FpCamera ortho_cam;
		ortho_cam.OrthogonalProjMatrix(
			mins_floats.x, maxs_floats.x, 
			mins_floats.y, maxs_floats.y,
			0.f, cascade_extents_floats.z);
		ortho_cam.LookTo(
			shadow_cam_pos,
			DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(frustum_center, shadow_cam_pos)),
			up_dir);


		// Intersect test the cascade light frustum with each renderable
		DirectX::XMMATRIX inv_view = DirectX::XMMatrixInverse(nullptr, DirectX::XMLoadFloat4x4(&ortho_cam.ViewMatrix()));
		for (auto shadow_casting_id : shadowCastingEntities)
		{
			bool draw_hadow_caster = true;
			std::shared_ptr<IComponent> spatialized_tmp_comp, bounded_tmp_comp, renderable_tmp_comp;
			gApp->EntityMngr()->GetComponentFromEntity(shadow_casting_id, Spatialized::GetGuid(), spatialized_tmp_comp);
			gApp->EntityMngr()->GetComponentFromEntity(shadow_casting_id, Bounded::GetGuid(), bounded_tmp_comp);
			gApp->EntityMngr()->GetComponentFromEntity(shadow_casting_id, Renderable::GetGuid(), renderable_tmp_comp);
			assert(renderable_tmp_comp);

			Spatialized* spatialized = spatialized_tmp_comp ? static_cast<Spatialized*>(spatialized_tmp_comp.get()) : nullptr;
			
			// Do frustum culling if entity has spatial and bounding components
			if (spatialized && bounded_tmp_comp)
			{
				DirectX::XMMATRIX world = XMLoadFloat4x4(&spatialized->LocalTransformation());
				DirectX::XMMATRIX inv_world = XMMatrixInverse(nullptr, world);

				// view space to object's local space
				DirectX::XMMATRIX to_local = XMMatrixMultiply(inv_view, inv_world);

				// transform the camera frustum from view space to the object's local space
				DirectX::BoundingFrustum local_space_frustum = ortho_cam.CameraFrustum();				
				local_space_frustum.Transform(local_space_frustum, to_local);

				Bounded* bounded = static_cast<Bounded*>(bounded_tmp_comp.get());
//   				if (local_space_frustum.Contains(bounded->AabbBounds()) == DirectX::DISJOINT)
//   					draw_hadow_caster = false;
			}

			if (draw_hadow_caster && spatialized)
			{
				auto renderable = static_cast<Renderable*>(renderable_tmp_comp.get());

				// Is the renderable a dynamic instance? If so add it to the batch
				int batch_id = renderable->DynamicInstancedBatchId();
				if (batch_id >= 0)
				{
					gApp->Renderer()->InstancingMngr()->AddEntityToDynamicBatch(shadow_casting_id);
				}
				else
				{
					RenderDepthOnlyNonInstanced(&ortho_cam, shadow_casting_id, renderable, spatialized);
				}
			}
		}

		// Update instance mngr
		gApp->Renderer()->InstancingMngr()->Update(device_context);
		
		// Draw all instanced
		RenderDepthAllInstanced(&ortho_cam);

		
		// Calculate other constants that are needed when rendering
		// Apply the scale/offset matrix, which transforms from [-1,1]
		// post-projection space to [0,1] UV space
		DirectX::XMMATRIX tex_scale_bias;
		tex_scale_bias.r[0] = DirectX::XMVectorSet(0.5f, 0.0f, 0.0f, 0.0f);
		tex_scale_bias.r[1] = DirectX::XMVectorSet(0.0f, -0.5f, 0.0f, 0.0f);
		tex_scale_bias.r[2] = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
		tex_scale_bias.r[3] = DirectX::XMVectorSet(0.5f, 0.5f, 0.0f, 1.0f);
		DirectX::XMMATRIX shadow_matrix = DirectX::XMLoadFloat4x4(&ortho_cam.ViewProjMatrix());
		shadow_matrix = DirectX::XMMatrixMultiply(shadow_matrix, tex_scale_bias);

		// Store the split distance in terms of view space depth		
		shadow_emitting->CascadeSplit(i) = camera->NearZ() + (split_dist * z_range);

		// Calculate the position of the lower corner of the cascade partition, in the UV space
		// of the first cascade partition.
		DirectX::XMMATRIX inv_cascade_matrix = DirectX::XMMatrixInverse(nullptr, shadow_matrix);
		DirectX::XMVECTOR cascade_corner = DirectX::XMVector3TransformCoord(
			DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f),
			inv_cascade_matrix);
		cascade_corner = DirectX::XMVector3TransformCoord(cascade_corner, global_shadow_matrix);

		// Do the same for the upper corner
		DirectX::XMVECTOR other_corner = DirectX::XMVector3TransformCoord(
			DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.f), 
			inv_cascade_matrix);
		other_corner = DirectX::XMVector3TransformCoord(other_corner, global_shadow_matrix);

		// Calculate the scale and offset and store them
		DirectX::XMVECTOR corners_diff = DirectX::XMVectorSubtract(other_corner, cascade_corner);
		DirectX::XMVECTOR cascade_scale = DirectX::XMVectorDivide(
			DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.f),
			corners_diff);

		DirectX::XMFLOAT4 cascade_corner_floats, cascade_scale_floats;
		DirectX::XMStoreFloat4(&cascade_corner_floats, cascade_corner);
		cascade_corner_floats.x *= -1.f;
		cascade_corner_floats.y *= -1.f;
		cascade_corner_floats.z *= -1.f;
		cascade_corner_floats.w  =  0.f;
		DirectX::XMStoreFloat4(&cascade_scale_floats, cascade_scale);
		cascade_scale_floats.w = 1.f;

		shadow_emitting->CascadeOffset(i) = cascade_corner_floats;
		shadow_emitting->CascadeScale(i) = cascade_scale_floats;
	}
}

DirectX::XMMATRIX
ShadowSystem::MakeDirectionalLightGlobalShadowMatrix(DirectX::XMFLOAT3 const& lightDirection)
{
	auto camera = gApp->Renderer()->CurrentCamera();

	DirectX::XMVECTOR frustum_corners[8] =
	{
		DirectX::XMVectorSet(-1.0f, 1.0f, 0.0f, 1.f),
		DirectX::XMVectorSet(1.0f, 1.0f, 0.0f, 1.f),
		DirectX::XMVectorSet(1.0f, -1.0f, 0.0f, 1.f),
		DirectX::XMVectorSet(-1.0f, -1.0f, 0.0f, 1.f),
		DirectX::XMVectorSet(-1.0f, 1.0f, 1.0f, 1.f),
		DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.f),
		DirectX::XMVectorSet(1.0f, -1.0f, 1.0f, 1.f),
		DirectX::XMVectorSet(-1.0f, -1.0f, 1.0f, 1.f),
	};

	DirectX::XMMATRIX inv_view_proj = DirectX::XMMatrixInverse(nullptr, XMLoadFloat4x4(&camera->ViewProjMatrix()));

	DirectX::XMVECTOR center = DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f);
	for (auto i = 0; i < 8; i++)
	{
		frustum_corners[i] = DirectX::XMVector3TransformCoord(frustum_corners[i], inv_view_proj);
		center = DirectX::XMVectorAdd(center, frustum_corners[i]);
	}
	center = DirectX::XMVectorDivide(center, DirectX::XMVectorSet(8.f, 8.f, 8.f, 8.f));

	DirectX::XMVECTOR up_dir = DirectX::XMVectorSet(0.f, 1.f, 0.f, 1.f);

	DirectX::XMVECTOR light_dir = DirectX::XMLoadFloat3(&lightDirection);
	light_dir = DirectX::XMVectorMultiply(light_dir, DirectX::XMVectorSet(-1.f, -1.f, -1.f, 1.f));
	
	DirectX::XMVECTOR shadow_cam_pos = DirectX::XMVectorAdd (
		center,
		DirectX::XMVectorMultiply(light_dir, DirectX::XMVectorSet(-0.5f, -0.5f, -0.5f, 1.f)) );

	FpCamera ortho_cam;
	ortho_cam.ProjMatrix(0.f, 1.f, 1.f, 0.f, 1.f, false);
	ortho_cam.LookTo(
		shadow_cam_pos,
		DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(center, shadow_cam_pos)), 
		up_dir);

	DirectX::XMFLOAT4X4 tex_scale_bias;
	XMStoreFloat4x4(&tex_scale_bias, DirectX::XMMatrixIdentity());
	// Set scale
	tex_scale_bias._11 = 0.5f;
	tex_scale_bias._22 = -0.5f;
	tex_scale_bias._33 = 1.0f;
	// Set translation
	tex_scale_bias._41 = 0.5f;
	tex_scale_bias._42 = 0.5f;
	tex_scale_bias._43 = 0.f;
	
	DirectX::XMMATRIX global_mat = DirectX::XMMatrixMultiply(
		DirectX::XMLoadFloat4x4(&ortho_cam.ViewProjMatrix()),
		DirectX::XMLoadFloat4x4(&tex_scale_bias));
	return global_mat;
}

void 
ShadowSystem::RenderDepthOnlyNonInstanced(
	Camera* const shadowCamera,
	unsigned int const entityId,
	Renderable* const renderable,
	Spatialized* const spatialized)
{
	auto renderer = gApp->Renderer();
	auto context = gApp->Renderer()->DeviceContext();

	// Set shader
	context->VSSetShader(mDepthOnlyVs.Get(), nullptr, 0);

	// Input layout... only care about geometry
	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
	input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
	auto input_layout = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mDepthOnlyVsBlob.Get(), renderer->Device());
	renderer->DeviceContext()->IASetInputLayout(input_layout);

	// Primitive topology
	renderer->DeviceContext()->IASetPrimitiveTopology(renderable->PrimitiveTopology());

	// Index buffer
	renderer->DeviceContext()->IASetIndexBuffer(
		renderable->IndexBuffer().indexBuffer,
		renderable->IndexBuffer().indexBufferFormat,
		renderable->IndexBuffer().indexBufferOffset);

	// Vertex buffer
	std::vector<ID3D11Buffer*> buffers;
	std::vector<unsigned int> strides;
	std::vector<unsigned int> offsets;

	unsigned int vert_count = 0;

	std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();
	for (sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++)
	{ 
		buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
		strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
		offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);

		vert_count += renderable->VertexBuffer(*sub_input_iter).vertexCount;
	}
	renderer->DeviceContext()->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);


	// Set constants
	mDepthOnlyConstantVariables.viewProj = DirectX::XMMatrixTranspose(
		DirectX::XMLoadFloat4x4(&shadowCamera->ViewProjMatrix()));
	mDepthOnlyConstantVariables.world = DirectX::XMMatrixTranspose(
		DirectX::XMLoadFloat4x4(&spatialized->LocalTransformation()));

	mDepthOnlyConstants.SetData(context, mDepthOnlyConstantVariables);

	auto cb = mDepthOnlyConstants.GetBuffer();
	context->VSSetConstantBuffers(0, 1, &cb);
	

	// Draw		
	if (0 == renderable->IndexBuffer().indexCount)
		renderer->DeviceContext()->Draw(vert_count, 0);
	else
		renderer->DeviceContext()->DrawIndexed(renderable->IndexBuffer().indexCount, 0, 0);
	
}

void 
ShadowSystem::RenderDepthAllInstanced(Camera* const shadowCamera)
{
	auto renderer = gApp->Renderer();
	auto context = gApp->Renderer()->DeviceContext();

	// Set shader
	context->VSSetShader(mDepthOnlyInstancedVs.Get(), nullptr, 0);

	// Constants won't change since world matrix is embeded in input layout
	mDepthOnlyConstantVariables.viewProj = DirectX::XMMatrixTranspose(
	DirectX::XMLoadFloat4x4(&shadowCamera->ViewProjMatrix()));
	DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
	mDepthOnlyConstantVariables.world = DirectX::XMMatrixTranspose(world);
	mDepthOnlyConstants.SetData(context, mDepthOnlyConstantVariables);
	auto cb = mDepthOnlyConstants.GetBuffer();
	context->VSSetConstantBuffers(0, 1, &cb);

	// Input layout... only care about geometry & instancing inputs
	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
	input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
	input_layout_desc.push_back(InputLayoutManager::INSTANCED);
	auto input_layout = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mDepthOnlyVsBlob.Get(), renderer->Device());
	renderer->DeviceContext()->IASetInputLayout(input_layout);


	InstancingManager::InstancedBatchesResources& rsrcs = renderer->InstancingMngr()->InstancedBatchesRsrcs();
	std::map< unsigned int, std::shared_ptr<Renderable> >::iterator iter = rsrcs.batchRenderableRef.begin();
	for (; iter != rsrcs.batchRenderableRef.end(); ++iter)
	{
		std::vector<ID3D11Buffer*> buffers;
		std::vector<unsigned int> strides;
		std::vector<unsigned int> offsets;

		unsigned int vert_count = 0;

		std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();
		for (sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++)
		{
			if (InputLayoutManager::INSTANCED == *sub_input_iter)
				continue;
			buffers.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).vertexBuffer);
			strides.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).stride);
			offsets.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).offset);

			vert_count += rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).vertexCount;
		}
		buffers.push_back(rsrcs.instancedBuffer[iter->first]);
		strides.push_back(renderer->InstancingMngr()->InstancedBufferStride());
		offsets.push_back(0);
		renderer->DeviceContext()->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

		renderer->DeviceContext()->IASetPrimitiveTopology(rsrcs.batchRenderableRef[iter->first]->PrimitiveTopology());

		// Index buffer
		renderer->DeviceContext()->IASetIndexBuffer(rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBuffer,
			rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBufferFormat,
			rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBufferOffset);

		// Draw		
		if (0 == rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexCount)
			renderer->DeviceContext()->DrawInstanced(vert_count, rsrcs.bufferSize[iter->first], 0, 0);
		else
			renderer->DeviceContext()->DrawIndexedInstanced(rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexCount, rsrcs.bufferSize[iter->first], 0, 0, 0);
	}
}


