/**  Renderer.h
 *
 *  Rendering pipeline.
 */

#pragma once

#include <list>
#include <dxgi1_3.h>
#include <d3d11_2.h>
#include <DirectXMath.h>
#include "System.h"
#include "Systems/Renderer/LightingSystem.h"
#include "Systems/Renderer/ShadowSystem.h"
#include "Systems/Renderer/InputLayoutManager.h"
#include "Systems/Renderer/Pipeline.h"
#include "Systems/Renderer/Camera.h"
#include "Systems/Renderer/RenderStateManager.h"
#include "Systems/Renderer/TextureManager.h"
#include "Systems/Renderer/InstancingManager.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"

class ForwardRenderer;
class SkyRenderer;

// COMMON GPU INTERFACE MIRRORS
// NOTE: THESE STRUCTURES MUST MATCH THE ONES DECLARED IN HLSL!
struct LightToGpu
{
	int					lightType;

	DirectX::XMFLOAT2	pad;
	int					emitsShadows;
	DirectX::XMMATRIX   shadowMatrix;
	DirectX::XMFLOAT4	cascadeSplits;
	DirectX::XMFLOAT4	cascadeOffsets[NUM_SHADOW_CASCADES];
	DirectX::XMFLOAT4	cascadeScales[NUM_SHADOW_CASCADES];

	float				staticOffsetBias;
	float				normalOffsetScaleBias;
	int					usePlaneDepthBias;
	float				shadowIntensity;


	DirectionalLight	directionalLight;
	PointLight			pointLight;
	SpotLight			spotLight;
};

struct Fog
{
	float				fogDensity;
	float				fogLinStart;
	float				fogLinEnd;
	int					fogType;  // neg. value is no fog
	DirectX::XMFLOAT4	fogColor;

	Fog()
	{
		fogType = -1;
		fogDensity = 0.f;
		fogLinStart = 10.f;
		fogLinEnd = 100.f;
		fogColor = DirectX::XMFLOAT4(0.75f, 0.75f, 0.75f, 1.f);
	}
};

class D3dRenderer : public System, public std::enable_shared_from_this<D3dRenderer>
{
public:
    D3dRenderer(std::shared_ptr<EntityManager> entityManager);
   
    ~D3dRenderer(void);

	/// Sets the default RTV.
	void
	SetBackBufferRenderTarget();
    
    /**
     * @brief The clear color of the render target view window. 
     *    
     * @param R 
     * @param G
     * @param B
     * @param A
     */  
    void
    ClearColor(float const& r, float const& g, float const& b, float const& a);

    void
    CurrentCamera(std::shared_ptr<Camera> camera);    

    void
    EnableVsync(bool const& enabled);      
   
    bool const
    Init(unsigned int const& width, unsigned int const& height, HWND const& handle);      
    
    void
    RunImplementation(std::set<unsigned int> const* family, double const currentTime);
	
	// Sets render path of attached ordered pipelines
	void
	RenderPath(std::list< std::shared_ptr<Pipeline> > pipelines);	
    
	// Cleans up all resources we have used
    void
    Shutdown(void);
   
    // Access / Queries
    ID3D11RenderTargetView* const
    BackBufferRTV(void) const { return mBackBufferRTV; }

	ID3D11DepthStencilView* const
	DepthStencilView() const { return mDepthStencilView; }

	ID3D11ShaderResourceView* const
	DepthStencilSrv(void) const { return mDepthStencilSrv; }
    
    float const*
    ClearColor(void) const {return &mClearColor[0]; }

    std::shared_ptr<Camera> const&
    CurrentCamera(void) const { return mCurrentCamera; }
	
    ID3D11Device2* const
    Device(void) const { return mDevice; }
    
    ID3D11DeviceContext2* const
    DeviceContext(void) const { return mDeviceContext; }

    std::shared_ptr<InputLayoutManager> const&
    InputLayoutMngr(void) const { return mInputLayoutManager; }
    
    std::shared_ptr<LightingSystem> const&
    LightingSystm(void) const { return mLightingSystem; }

    std::shared_ptr<RenderStateManager> const&
    StateMngr(void) const { return mRenderStateManager; }
    
    std::shared_ptr<TextureManager> const&
	TextureMngr(void) const { return mTextureManager; }
	
	std::shared_ptr<InstancingManager> const&
	InstancingMngr(void) const { return mInstancingManager; }

	std::list< std::shared_ptr<Pipeline> > const&
	RenderPath(void) const { return mRenderPath; }

	/* This is a utility function that can be used by pipelines to quickly acquire 
	 * GPU lighting and shadow mapping data.
	 *
	 * The function will setup:
	 * - a vector of LightToGpu structs which are used within constant buffers in HLSL.
	 * - a vector of SRVs for each light's Texture2DArray shadow cascades.
	 */
	void
	SetupLightsAndShadowMapsForGPU(
		std::vector<LightToGpu>& lightsToGpuOut,
		std::vector<ID3D11ShaderResourceView*>& shadowMapCascades);
        
private:    	

    // Settings of renderer
    bool            mVsyncEnabled;
    float           mClearColor[4];
    
    // Graphic card stuff
    unsigned int    mGpuMemory;
    char            mGpuDesc[128];

    // Sub-systems for rendering
    std::shared_ptr<LightingSystem>     mLightingSystem;
    unsigned int                        mLightingSystemId;
	std::shared_ptr<ShadowSystem>       mShadowSystem;
	unsigned int                        mShadowSystemId;
    
    // Rsrcs
    IDXGISwapChain2*                    mSwapChain;
    
    ID3D11Device2*                      mDevice;
    ID3D11DeviceContext2*               mDeviceContext;
    
    ID3D11RenderTargetView*             mBackBufferRTV;

	// Depth stencil buffer and view
	ID3D11Texture2D*					mDepthStencilBuffer;
	ID3D11DepthStencilView*				mDepthStencilView;
	ID3D11ShaderResourceView*		    mDepthStencilSrv;
    
    std::shared_ptr<Camera>					mCurrentCamera;    
    std::list< std::shared_ptr<Pipeline> >  mRenderPath;	

    std::shared_ptr<RenderStateManager> mRenderStateManager;
    std::shared_ptr<InputLayoutManager> mInputLayoutManager;
    std::shared_ptr<TextureManager>     mTextureManager;
	std::shared_ptr<InstancingManager>  mInstancingManager;


	// PIPELINES
	std::shared_ptr<ForwardRenderer> mForwardRender;
	std::shared_ptr<SkyRenderer>	 mSkyRender;
};

