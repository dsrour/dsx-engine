/**  Effect.h
 *
 *   Compiles and sets up for an .fx shader.
 *   Effects are used by a rendering pipeline to draw geometries.
 */

#pragma once

#include <assert.h>
#include <d3d11.h>
#include <string>
#include <map>
#include <list>
#include "Systems/Renderer/InputLayoutManager.h"

class Effect
{
public:
    Effect(void);       
    
    virtual 
    ~Effect(void);    

	/// For debug purposes
	virtual void
	Recompile(void) {}

    // Queries
    std::list<InputLayoutManager::SubInputLayout>&
    InputLayoutDescription(std::string const& techName);    

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext) {};

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext) {};

	ID3D11InputLayout* const
	InputLayout(std::string const& techName);
        
protected:
    
	// Taken from http://msdn.microsoft.com/en-us/library/windows/desktop/hh968107(v=vs.85).aspx
	HRESULT 
	CompileShader(_In_ LPCWSTR srcFile, _In_ LPCSTR entryPoint, _In_ LPCSTR profile, _Outptr_ ID3DBlob** blob);

    std::map< std::string,  std::list<InputLayoutManager::SubInputLayout> >      mInputLayoutDescs;  // < tech name, input layout desc >
	std::map< std::string, ID3D11InputLayout* >									 mInputLayouts;
};