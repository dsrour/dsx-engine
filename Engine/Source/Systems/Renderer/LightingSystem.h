/**  LightingSystem.h
 *
 *   Gets entities with LightEmitting component. 
 *   Used by rendering pipelines to send lighting data to GPU.
 *
 *   Also holds references to ShadowEmitting components so that rendering pipelines
 *   can know if a certain light emits shadows or not.
 */

#pragma once

#include <list>
#include "System.h"
#include "Components/LightEmitting.h"
#include "Components/ShadowEmitting.h"
#include "Components/Spatialized.h"

class LightingSystem : public System
{
public:
    LightingSystem(std::shared_ptr<EntityManager> entityManager) : System(entityManager) 
    {
        // Set family req
        std::set<unsigned int> fam_req;
        fam_req.insert(LightEmitting::GetGuid());       
        SetFamilyRequirements(fam_req);  
    }
   
    ~LightingSystem(void) {}
   
    void
    RunImplementation(std::set<unsigned int> const* family, double const /*currentTime*/)
    {
        // Store list of light pointers
        mLights.clear();
		mShadowEmittingComponents.clear();
                
        std::set<unsigned int>::const_iterator fam_iter = family->begin();
        for (fam_iter; fam_iter!= family->end(); fam_iter++)
        {
            // Get LightEmitting component
            std::shared_ptr<IComponent> tmp_comp;
            mEntityManager->GetComponentFromEntity(*fam_iter, LightEmitting::GetGuid(), tmp_comp);

            if (tmp_comp)
            {
                LightEmitting* light_emitting = static_cast<LightEmitting*>(tmp_comp.get());

				// Make position relative to spatialized component
				mEntityManager->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), tmp_comp);
				if (tmp_comp)
				{
					Spatialized* sp = static_cast<Spatialized*>(tmp_comp.get());
					
					// make position the same as the entity's local position
					if (light_emitting->Type() == POINT_LIGHT)
					{
						PointLight* pl = static_cast<PointLight*>(light_emitting->GetLight());
						pl->position = sp->LocalPosition();
					}
					else if (light_emitting->Type() == SPOT_LIGHT)
					{
						SpotLight* sl = static_cast<SpotLight*>(light_emitting->GetLight());
						sl->position = sp->LocalPosition();
					}
				}

                
                mLights.push_back( std::pair<LightType, Light*>(light_emitting->Type(), light_emitting->GetLight()) );

				mEntityManager->GetComponentFromEntity(*fam_iter, ShadowEmitting::GetGuid(), tmp_comp);
				mShadowEmittingComponents.push_back(static_cast<ShadowEmitting*>(tmp_comp.get()));
            }
        }
    }

    std::list< std::pair<LightType, Light*> > const&
    Lights(void) const { return mLights; }

	std::list< ShadowEmitting* > const&
	ShadowEmittingComponents() const { return mShadowEmittingComponents; }

private:
    std::list< std::pair<LightType, Light*> > mLights;
	std::list< ShadowEmitting* > mShadowEmittingComponents;
};