/**  MathHelper.h
 *
 *   Commonly used math helper functions
 */

#pragma once

#include <windows.h>
#include <DirectXMath.h>

DirectX::XMMATRIX InverseTranspose(DirectX::CXMMATRIX mat);

DirectX::XMVECTOR
AddVectors(DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2);

DirectX::XMVECTOR
SubtractVectors(DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2);

DirectX::XMVECTOR
MultiplyVectors(DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2);

DirectX::XMVECTOR
FactorVector(DirectX::XMVECTOR const& v1, float const& factor);

bool 
IsEqual(DirectX::XMFLOAT4X4 const& m1, DirectX::XMFLOAT4X4 const& m2);
