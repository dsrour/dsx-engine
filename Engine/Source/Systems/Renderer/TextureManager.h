/**  TextureManager.h
 *
 *   Manages texture resources
 */

#pragma once

#include <vector>
#include <string>
#include <map>
#include <d3d11.h>

class TextureManager
{
public:
    ~TextureManager(void);

    ID3D11ShaderResourceView* const
    GetOrCreateTexture2dSrv(ID3D11Device* const d3dDevice, std::string const& file, bool const& srgb = false);

private:
    std::map< std::string, unsigned int  >    mTexture2dSrvId;
    std::vector< ID3D11ShaderResourceView* >  mSrvs;

	std::map< std::string, unsigned int  >    mTexture2dSrvSrgbId;
	std::vector< ID3D11ShaderResourceView* >  mSrgbSrvs;
};
