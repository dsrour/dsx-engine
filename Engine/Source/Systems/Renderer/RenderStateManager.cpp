#include <assert.h>
#include "RenderStateManager.h"

RenderStateManager::RenderStateManager(ID3D11Device* const d3dDevice)
{
	// Create default sampler state
	D3D11_SAMPLER_DESC  desc;
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.MipLODBias = 0;
	desc.MaxAnisotropy = 1;
	desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	desc.BorderColor[0] = 1.f;
	desc.BorderColor[1] = 1.f;
	desc.BorderColor[2] = 1.f;
	desc.BorderColor[3] = 1.f;
	desc.MinLOD = -FLT_MAX;
	desc.MaxLOD = FLT_MAX;
	assert(SUCCEEDED(d3dDevice->CreateSamplerState(&desc, &mDefaultSamplerState)));


    // Create raster states
    ID3D11RasterizerState* default_rs;
    D3D11_RASTERIZER_DESC default_rs_desc;
    ZeroMemory( &default_rs_desc, sizeof(D3D11_RASTERIZER_DESC) );
    default_rs_desc.FillMode = D3D11_FILL_SOLID;
    default_rs_desc.CullMode = D3D11_CULL_BACK;
    default_rs_desc.DepthClipEnable = true;
    
    HRESULT hr = d3dDevice->CreateRasterizerState(&default_rs_desc, &default_rs);
    assert(SUCCEEDED(hr));


	ID3D11RasterizerState* cull_none_rs;
	D3D11_RASTERIZER_DESC cull_none_rs_desc;
	ZeroMemory(&cull_none_rs_desc, sizeof(D3D11_RASTERIZER_DESC));
	cull_none_rs_desc.FillMode = D3D11_FILL_SOLID;
	cull_none_rs_desc.CullMode = D3D11_CULL_NONE;
	cull_none_rs_desc.DepthClipEnable = true;

	hr = d3dDevice->CreateRasterizerState(&cull_none_rs_desc, &cull_none_rs);
	assert(SUCCEEDED(hr));

	ID3D11RasterizerState* cull_none_depth_clisp_disabled_rs;
	D3D11_RASTERIZER_DESC cull_none_depth_clisp_disabled_desc;
	ZeroMemory(&cull_none_depth_clisp_disabled_desc, sizeof(D3D11_RASTERIZER_DESC));
	cull_none_depth_clisp_disabled_desc.FillMode = D3D11_FILL_SOLID;
	cull_none_depth_clisp_disabled_desc.CullMode = D3D11_CULL_NONE;
	cull_none_depth_clisp_disabled_desc.DepthClipEnable = false;

	hr = d3dDevice->CreateRasterizerState(&cull_none_depth_clisp_disabled_desc, &cull_none_depth_clisp_disabled_rs);
	assert(SUCCEEDED(hr));

	ID3D11RasterizerState* wireframe_rs;
	D3D11_RASTERIZER_DESC wireframe_rs_desc;
	ZeroMemory( &wireframe_rs_desc, sizeof(D3D11_RASTERIZER_DESC) );
	wireframe_rs_desc.FillMode = D3D11_FILL_WIREFRAME;
	wireframe_rs_desc.CullMode = D3D11_CULL_NONE;
	wireframe_rs_desc.DepthClipEnable = true;
	
	hr = d3dDevice->CreateRasterizerState(&wireframe_rs_desc, &wireframe_rs);
	assert(SUCCEEDED(hr));

    mRasterStates[Default] = default_rs;
	mRasterStates[NoCull] = cull_none_rs;
	mRasterStates[NoCullDepthClipDisabled] = cull_none_depth_clisp_disabled_rs;
	mRasterStates[Wireframe] = wireframe_rs;
}

RenderStateManager::~RenderStateManager(void)
{
	mDefaultSamplerState->Release();

    // Release all objs
    std::map<RasterizerStateDesc, ID3D11RasterizerState*>::iterator raster_iter;
    for (raster_iter = mRasterStates.begin(); raster_iter != mRasterStates.end(); raster_iter++)
    {
        ID3D11RasterizerState* state = raster_iter->second;
        if (state)
            state->Release();        
    }

	std::map<SamplerDesc, ID3D11SamplerState*, SamplerDescComparor>::iterator sampler_iter;
	for (sampler_iter = mSamplerStates.begin(); sampler_iter != mSamplerStates.end(); sampler_iter++)
	{
		ID3D11SamplerState* state = sampler_iter->second;
		if (state)
			state->Release();        
	}

	std::map<DepthStencilDesc, ID3D11DepthStencilState*, DepthStencilDescComparor>::iterator ds_iter;
	for (ds_iter = mDepthStencilStates.begin(); ds_iter != mDepthStencilStates.end(); ds_iter++)
	{
		ID3D11DepthStencilState* state = ds_iter->second;
		if (state)
			state->Release();        
	}

	std::map<BlendDesc, ID3D11BlendState*, BlendDescComparor>::iterator bs_iter;
	for (bs_iter = mBlendStates.begin(); bs_iter != mBlendStates.end(); bs_iter++)
	{
		ID3D11BlendState* state = bs_iter->second;
		if (state)
			state->Release();
	}
}

ID3D11RasterizerState* const
RenderStateManager::RasterizerState(RasterizerStateDesc const& rasterState)
{   
    std::map<RasterizerStateDesc, ID3D11RasterizerState*>::iterator raster_iter = mRasterStates.find(rasterState);
    if (mRasterStates.end() == raster_iter)
        return NULL;

    return raster_iter->second;
}

ID3D11SamplerState* const
RenderStateManager::GetOrCreateSamplerState(ID3D11Device* const d3dDevice, D3D11_SAMPLER_DESC const& stateDesc)
{
	static int id = 0;

    SamplerDesc desc;
    desc.filter = (unsigned int)stateDesc.Filter;
    desc.addressU = (unsigned int)stateDesc.AddressU;
    desc.addressV = (unsigned int)stateDesc.AddressV;
    desc.addressW = (unsigned int)stateDesc.AddressW;
    desc.mipLodBias = stateDesc.MipLODBias;
    desc.maxAnisotropy =  stateDesc.MaxAnisotropy;
    desc.comparisonFunc = stateDesc.ComparisonFunc;
    desc.borderColor[0] = stateDesc.BorderColor[0];
    desc.borderColor[1] = stateDesc.BorderColor[1];
    desc.borderColor[2] = stateDesc.BorderColor[2];
    desc.borderColor[3] = stateDesc.BorderColor[3];
    desc.minLOD = stateDesc.MinLOD;
    desc.maxLOD = stateDesc.MaxLOD;
    
    std::map<SamplerDesc, ID3D11SamplerState*, SamplerDescComparor>::iterator iter;
    
    iter = mSamplerStates.find(desc);
    
    if (iter != mSamplerStates.end())
        return iter->second;
    else
    {
		id++;
		mSamplerStates[desc] = NULL;
        assert( SUCCEEDED( d3dDevice->CreateSamplerState(&stateDesc, &mSamplerStates[desc] ) ) );
        return mSamplerStates[desc];
    }
}

ID3D11DepthStencilState* const 
RenderStateManager::GetOrCreateDepthStencilState( ID3D11Device* const d3dDevice, D3D11_DEPTH_STENCIL_DESC const& dsDesc )
{
	DepthStencilDesc desc;
	desc.depthEnable = dsDesc.DepthEnable;
	desc.depthWriteMask = dsDesc.DepthWriteMask;
	desc.depthFunc = dsDesc.DepthFunc;
	desc.stencilEnable = dsDesc.StencilEnable;
	desc.stencilReadMask = dsDesc.StencilReadMask;
	desc.stencilWriteMask = dsDesc.StencilWriteMask;
	desc.frontFace = dsDesc.FrontFace;
	desc.backFace = dsDesc.BackFace;
	

	std::map<DepthStencilDesc, ID3D11DepthStencilState*, DepthStencilDescComparor>::iterator iter;

	iter = mDepthStencilStates.find(desc);

	if (iter != mDepthStencilStates.end())
		return iter->second;
	else
	{
		mDepthStencilStates[desc] = NULL;
		assert( SUCCEEDED( d3dDevice->CreateDepthStencilState(&dsDesc, &mDepthStencilStates[desc] ) ) );
		return mDepthStencilStates[desc];
	}
}

ID3D11BlendState* const 
RenderStateManager::GetOrCreateBlendState(ID3D11Device* const d3dDevice, D3D11_BLEND_DESC const& blendDesc)
{
	BlendDesc desc;	
	desc.alphaToConverageEnable = blendDesc.AlphaToCoverageEnable;
	desc.independentBlendEnable = blendDesc.IndependentBlendEnable;
	for (unsigned int i = 0; i < 8; i++)
		desc.renderTarget[i] = blendDesc.RenderTarget[i];

	std::map<BlendDesc, ID3D11BlendState*, BlendDescComparor>::iterator iter;
	iter = mBlendStates.find(desc);

	if (iter != mBlendStates.end())
		return iter->second;
	else
	{
		mBlendStates[desc] = NULL;
		HRESULT hr = d3dDevice->CreateBlendState(&blendDesc, &mBlendStates[desc]);
		assert(SUCCEEDED(hr));
		return mBlendStates[desc];
	}
}
