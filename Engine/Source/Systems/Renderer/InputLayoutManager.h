/**  InputLayoutManager.h
 *
 *   Manages and creates input layouts when necessary.
 */

#pragma once

#include <map>
#include <list>
#include <d3d11.h>
#include <DirectXMath.h>

class InputLayoutManager
{
public:
    enum SubInputLayout
    {
        GEOMETRY,
        TEXTURE,
		INSTANCED,
    };

    //InputLayoutManager(void);      
    ~InputLayoutManager(void);     
    
	ID3D11InputLayout* const
	GetOrCreateInputLayout(std::list<SubInputLayout>& fullLayout, ID3DBlob* const vs, ID3D11Device* const device);
  
private:
    std::map< std::list<SubInputLayout>, ID3D11InputLayout* > mInputLayouts;
};

struct GeometryInputLayout
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 norm;

	GeometryInputLayout()
	{
		pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	}
};

struct TextureInputLayout
{
	DirectX::XMFLOAT2 texCoords;
	DirectX::XMFLOAT3 tangent;

	TextureInputLayout()
	{
		texCoords = DirectX::XMFLOAT2(0.f, 0.f);
		tangent = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	}
};