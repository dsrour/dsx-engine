/**  Pipeline.h
 *
 *   Uses effect objects and dictates how to render an object.
 */

#pragma once

#include <set>
#include <map>
#include <memory>
#include "Components/Renderable.h"

class D3dRenderer;

class Pipeline
{
public:
    Pipeline(std::shared_ptr<D3dRenderer> renderer) { mRenderer = renderer; }       
    
    virtual 
    ~Pipeline(void) {}   
          
    // Made part of render path
    virtual void
    MadeActive(void) {}

    // Removed from render path
    virtual void
    MadeInactive(void) {}


	virtual void 
	EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime) = 0; 

	virtual void
	RecompileShaders() = 0;

protected:
    std::weak_ptr<D3dRenderer> mRenderer;
};