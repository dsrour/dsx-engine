#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "ForwardRendererEffect.h"

extern std::wstring gEngineRootDir;

ForwardRendererEffect::ForwardRendererEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mFlatVsBlob = nullptr;
	mFlatVS = nullptr;
	mFlatPsBlob = nullptr;
	mFlatPS = nullptr;

	mVsBlob = nullptr;
	mVS = nullptr;
	mDefaultPsBlob = nullptr;
	mDefaultPS = nullptr;
	mDiffusePsBlob = nullptr;
	mDiffusePS = nullptr;
	mBumpPsBlob = nullptr;
	mBumpPS = nullptr;
	mDiffBumpPsBlob = nullptr;
	mDiffBumpPS = nullptr;

	mInstancedVsBlob = nullptr;
	mInstancedVS = nullptr;
	mInstancedDefaultPsBlob = nullptr;
	mInstancedDefaultPS = nullptr;
	mInstancedDiffusePsBlob = nullptr;
	mInstancedDiffusePS = nullptr;
	mInstancedBumpPsBlob = nullptr;
	mInstancedBumpPS = nullptr;
	mInstancedDiffBumpPsBlob = nullptr;
	mInstancedDiffBumpPS = nullptr;

	mBoundedDiffuseMap = nullptr;
	mBoundedNormalMap = nullptr;
	mBoundedReflectionMap = nullptr;

	mDiffuseMapSampler = nullptr;
	mNormalMapSampler = nullptr;
	mReflectionMapSampler = nullptr;

	mDiffuseMap = nullptr;
	mNormalMap = nullptr;
	mReflectionMap = nullptr;

	mShadowMapSampler = nullptr;

	mPerFrameCb.Create(renderer->Device());
	mPerObjectCb.Create(renderer->Device());

	ZeroMemory(&mPerFrameVariables, sizeof(CbPerFrame));
	ZeroMemory(&mPerObjectVariables, sizeof(CbPerObject));

	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}

/*virtual*/
ForwardRendererEffect::~ForwardRendererEffect()
{    
	if (mFlatVsBlob) 
		mFlatVsBlob->Release();
	if (mFlatVS) 
		mFlatVS->Release();
	if (mFlatPsBlob) 
		mFlatPsBlob->Release();
	if (mFlatPS) 
		mFlatPS->Release();

	if (mVsBlob) 
		mVsBlob->Release();
	if (mVS) 
		mVS->Release();
	if (mDefaultPsBlob) 
		mDefaultPsBlob->Release();
	if (mDefaultPS) 
		mDefaultPS->Release();
	if (mDiffusePsBlob) 
		mDiffusePsBlob->Release();
	if (mDiffusePS) 
		mDiffusePS->Release();
	if (mBumpPsBlob)
		mBumpPsBlob->Release();
	if (mBumpPS) 
		mBumpPS->Release();
	if (mDiffBumpPsBlob) 
		mDiffBumpPsBlob->Release();
	if (mDiffBumpPS) 
		mDiffBumpPS->Release();

	if (mInstancedVsBlob) 
		mInstancedVsBlob->Release();
	if (mInstancedVS) 
		mInstancedVS->Release();
	if (mInstancedDefaultPsBlob) 
		mInstancedDefaultPsBlob->Release();
	if (mInstancedDefaultPS) 
		mInstancedDefaultPS->Release();
	if (mInstancedDiffusePsBlob) 
		mInstancedDiffusePsBlob->Release();
	if (mInstancedDiffusePS) 
		mInstancedDiffusePS->Release();
	if (mInstancedBumpPsBlob) 
		mInstancedBumpPsBlob->Release();
	if (mInstancedBumpPS) 
		mInstancedBumpPS->Release();
	if (mInstancedDiffBumpPsBlob)
		mInstancedDiffBumpPsBlob->Release();
	if (mInstancedDiffBumpPS)
		mInstancedDiffBumpPS->Release();
}

void
ForwardRendererEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	cso_name = std::wstring(FR_FLAT_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mFlatVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mFlatVsBlob->GetBufferPointer(),
		mFlatVsBlob->GetBufferSize(),
		NULL,
		&mFlatVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_FLAT_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mFlatPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mFlatPsBlob->GetBufferPointer(),
		mFlatPsBlob->GetBufferSize(),
		NULL,
		&mFlatPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mVsBlob->GetBufferPointer(),
		mVsBlob->GetBufferSize(),
		NULL,
		&mVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_DEFAULT_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDefaultPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDefaultPsBlob->GetBufferPointer(),
		mDefaultPsBlob->GetBufferSize(),
		NULL,
		&mDefaultPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_DIFFUSE_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDiffusePsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDiffusePsBlob->GetBufferPointer(),
		mDiffusePsBlob->GetBufferSize(),
		NULL,
		&mDiffusePS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mBumpPsBlob->GetBufferPointer(),
		mBumpPsBlob->GetBufferSize(),
		NULL,
		&mBumpPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_DIFFUSE_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDiffBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDiffBumpPsBlob->GetBufferPointer(),
		mDiffBumpPsBlob->GetBufferSize(),
		NULL,
		&mDiffBumpPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_INSTANCED_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInstancedVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mInstancedVsBlob->GetBufferPointer(),
		mInstancedVsBlob->GetBufferSize(),
		NULL,
		&mInstancedVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_INSTANCED_DEFAULT_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInstancedDefaultPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mInstancedDefaultPsBlob->GetBufferPointer(),
		mInstancedDefaultPsBlob->GetBufferSize(),
		NULL,
		&mInstancedDefaultPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_INSTANCED_DIFFUSE_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInstancedDiffusePsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mInstancedDiffusePsBlob->GetBufferPointer(),
		mInstancedDiffusePsBlob->GetBufferSize(),
		NULL,
		&mInstancedDiffusePS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_INSTANCED_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInstancedBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mInstancedBumpPsBlob->GetBufferPointer(),
		mInstancedBumpPsBlob->GetBufferSize(),
		NULL,
		&mInstancedBumpPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FR_INSTANCED_DIFFUSE_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInstancedDiffBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mInstancedDiffBumpPsBlob->GetBufferPointer(),
		mInstancedDiffBumpPsBlob->GetBufferSize(),
		NULL,
		&mInstancedDiffBumpPS);
	assert(SUCCEEDED(hr));

	{
		// Create input layout descs
		std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

		input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
		mInputLayoutDescs[FR_FLAT_TECH] = input_layout_desc;
		mInputLayouts[FR_FLAT_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mFlatVsBlob, renderer->Device());

		input_layout_desc.push_back(InputLayoutManager::TEXTURE);
		mInputLayoutDescs[FR_DEFAULT_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_DIFFUSE_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_BUMP_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_DIFFUSE_BUMP_TECH] = input_layout_desc;

		mInputLayouts[FR_DEFAULT_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_DIFFUSE_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_DIFFUSE_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());

		input_layout_desc.push_back(InputLayoutManager::INSTANCED); // THIS SHOULDN'T BE CREATED BY THE RENDERABLE COMPONENT!  INPUT LAYOUT WILL BE EXPANDED TO FIT INSTANCED DATA BY THE RENDERING PIPELINE ITSELF
		mInputLayoutDescs[FR_INSTANCED_DEFAULT_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_INSTANCED_DIFFUSE_BUMP_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_INSTANCED_DIFFUSE_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_DIFFUSE_BUMP_TECH] = input_layout_desc;

		mInputLayouts[FR_INSTANCED_DEFAULT_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mInstancedVsBlob, renderer->Device());
		mInputLayouts[FR_INSTANCED_DIFFUSE_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mInstancedVsBlob, renderer->Device());
		mInputLayouts[FR_DIFFUSE_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mInstancedVsBlob, renderer->Device());
		mInputLayouts[FR_INSTANCED_DIFFUSE_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mInstancedVsBlob, renderer->Device());
	}
}

void
ForwardRendererEffect::UpdateLightArrayVariable(std::vector<LightToGpu> const& lights)
{
	for (int i = 0; i < (std::min)((int)lights.size(), MAX_LIGHTS); i++)
		mPerFrameVariables.lights[i] = lights[i];
}

void
ForwardRendererEffect::UpdateNumLightsVariable(unsigned int const& numLights)
{
	mPerFrameVariables.numLights = numLights;
}

void
ForwardRendererEffect::UpdateWorldEyePosVariable(DirectX::XMFLOAT3& eyePos)
{
	mPerFrameVariables.eyePos = eyePos;
}

void
ForwardRendererEffect::UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat)
{
	mPerObjectVariables.worldMat = DirectX::XMMatrixTranspose(worldMat);
}

void
ForwardRendererEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp)
{
	mPerObjectVariables.vpMat = DirectX::XMMatrixTranspose(vp);
}

void
ForwardRendererEffect::UpdateMaterialVariable(Material const& mat)
{
	mPerObjectVariables.material = mat;
}

void
ForwardRendererEffect::UpdateFogVariable(Fog const& fog)
{
	mPerFrameVariables.fog = fog;
}

void
ForwardRendererEffect::UpdateDiffuseMapVariable(ID3D11ShaderResourceView* const diffuseMap)
{
	mDiffuseMap = diffuseMap;
}

void
ForwardRendererEffect::UpdateDiffuseMapSamplerVariable(ID3D11SamplerState* const diffuseSampler)
{
	mDiffuseMapSampler = diffuseSampler;
}

void
ForwardRendererEffect::UpdateNormalMapVariable(ID3D11ShaderResourceView* const normalMap)
{
	mNormalMap = normalMap;
}

void
ForwardRendererEffect::UpdateNormalMapSamplerVariable(ID3D11SamplerState* const normalSampler)
{
	mNormalMapSampler = normalSampler;
}

void
ForwardRendererEffect::UpdateTextureTransformMatrixVariable( DirectX::XMMATRIX& mat )
{
	mPerObjectVariables.texMat = DirectX::XMMatrixTranspose(mat);
}

void 
ForwardRendererEffect::UpdateReflectionMapSamplerVariable(ID3D11SamplerState* const reflectionSampler)
{
	mReflectionMapSampler = reflectionSampler;
}

void 
ForwardRendererEffect::UpdateReflectionMapVariable(ID3D11ShaderResourceView* const reflectionMap)
{
	mReflectionMap = reflectionMap;
}

void ForwardRendererEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	auto default_st = mRendererRef->StateMngr()->DefaultSamplerState();
	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11Buffer* nb = nullptr;

	mPerObjectCb.SetData(deviceContext, mPerObjectVariables);

	auto cb = mPerObjectCb.GetBuffer();
	deviceContext->VSSetConstantBuffers(1, 1, &cb);
	deviceContext->PSSetConstantBuffers(1, 1, &cb);

	
	if (mDiffuseMapSampler)
		deviceContext->PSSetSamplers(0, 1, &mDiffuseMapSampler);
	else
		deviceContext->PSSetSamplers(0, 1, &default_st);

	if (mNormalMapSampler)
		deviceContext->PSSetSamplers(1, 1, &mNormalMapSampler);
	else
		deviceContext->PSSetSamplers(1, 1, &default_st);

	if (mReflectionMapSampler)
		deviceContext->PSSetSamplers(2, 1, &mReflectionMapSampler);
	else
		deviceContext->PSSetSamplers(2, 1, &default_st);

	if (mDiffuseMap && mDiffuseMap != mBoundedDiffuseMap)
	{
		deviceContext->PSSetShaderResources(0, 1, &mDiffuseMap);
		mBoundedDiffuseMap = mDiffuseMap;
	}
	if (mNormalMap && mNormalMap != mBoundedNormalMap)
	{
		deviceContext->PSSetShaderResources(1, 1, &mNormalMap);
		mBoundedNormalMap = mNormalMap;
	}
	if (mReflectionMap && mReflectionMap != mBoundedReflectionMap)
	{
		deviceContext->PSSetShaderResources(2, 1, &mReflectionMap);
		mBoundedReflectionMap = mReflectionMap;
	}
	

	if (FR_FLAT_TECH == techName)
	{
		deviceContext->VSSetShader(mFlatVS, nullptr, 0);
		deviceContext->PSSetShader(mFlatPS, nullptr, 0);
	}
	else if (FR_DEFAULT_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDefaultPS, nullptr, 0);
	}
	else if (FR_DIFFUSE_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDiffusePS, nullptr, 0);
	}
	else if (FR_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mBumpPS, nullptr, 0);
	}
	else if (FR_DIFFUSE_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDiffBumpPS, nullptr, 0);
	}
	else if (FR_INSTANCED_DEFAULT_TECH == techName)
	{
		deviceContext->VSSetShader(mInstancedVS, nullptr, 0);
		deviceContext->PSSetShader(mInstancedDefaultPS, nullptr, 0);
	}
	else if (FR_INSTANCED_DIFFUSE_TECH == techName)
	{
		deviceContext->VSSetShader(mInstancedVS, nullptr, 0);
		deviceContext->PSSetShader(mInstancedDiffusePS, nullptr, 0);
	}
	else if (FR_INSTANCED_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mInstancedVS, nullptr, 0);
		deviceContext->PSSetShader(mInstancedBumpPS, nullptr, 0);
	}
	else if (FR_INSTANCED_DIFFUSE_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mInstancedVS, nullptr, 0);
		deviceContext->PSSetShader(mInstancedDiffBumpPS, nullptr, 0);
	}
}

void ForwardRendererEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	deviceContext->VSSetShader(nullptr, nullptr, 0);
	deviceContext->PSSetShader(nullptr, nullptr, 0);
}

void 
ForwardRendererEffect::ApplyPerFrameConstantBuffer(ID3D11DeviceContext* const deviceContext)
{
	mPerFrameCb.SetData(deviceContext, mPerFrameVariables);

	auto cb = mPerFrameCb.GetBuffer();
	deviceContext->VSSetConstantBuffers(0, 1, &cb);
	deviceContext->PSSetConstantBuffers(0, 1, &cb);

	if (!mShadowMapCascades.empty())
	{
		deviceContext->PSSetShaderResources(3, (unsigned int)mShadowMapCascades.size(), &mShadowMapCascades[0]);
		deviceContext->PSSetSamplers(3, 1, &mShadowMapSampler);
	}
}

void 
ForwardRendererEffect::UnbindAll(ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;
	deviceContext->VSSetConstantBuffers(0, 1, &nb);
	deviceContext->VSSetConstantBuffers(1, 1, &nb);
	deviceContext->PSSetConstantBuffers(0, 1, &nb);
	deviceContext->PSSetConstantBuffers(1, 1, &nb);

	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11SamplerState* ns = nullptr;

	deviceContext->PSSetShaderResources(0, 1, &nsrv);
	deviceContext->PSSetSamplers(0, 1, &ns);
	deviceContext->PSSetShaderResources(1, 1, &nsrv);
	deviceContext->PSSetSamplers(1, 1, &ns);
	deviceContext->PSSetShaderResources(2, 1, &nsrv);
	deviceContext->PSSetSamplers(2, 1, &ns);

	deviceContext->PSSetSamplers(3, 1, &ns);
	for (auto i = 0u; i < (unsigned int)mShadowMapCascades.size(); i++)
		deviceContext->PSSetShaderResources(3 + i, 1, &nsrv);

	mBoundedDiffuseMap = nullptr;
	mBoundedNormalMap = nullptr;
	mBoundedReflectionMap = nullptr;

	mDiffuseMapSampler = nullptr;
	mNormalMapSampler = nullptr;
	mReflectionMapSampler = nullptr;

	mDiffuseMap = nullptr;
	mNormalMap = nullptr;
	mReflectionMap = nullptr;
}

void 
ForwardRendererEffect::UpdateShadowingVariables(
	ID3D11SamplerState* const shadowMapSampler, 
	std::vector<ID3D11ShaderResourceView*> const& shadowMapCascades)
{
	mShadowMapCascades.clear();
	mShadowMapSampler = shadowMapSampler;
	mShadowMapCascades = shadowMapCascades;
}
