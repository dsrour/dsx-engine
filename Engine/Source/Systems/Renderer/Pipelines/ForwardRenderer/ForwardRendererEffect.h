#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include <vector>
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class ForwardRendererEffect : public Effect
{
public:
    ForwardRendererEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~ForwardRendererEffect(void); 
	
    // Shader vars
    void
    UpdateLightArrayVariable(std::vector<LightToGpu> const& lights);

    void
    UpdateNumLightsVariable(unsigned int const& numLights);

    void
    UpdateWorldEyePosVariable(DirectX::XMFLOAT3& eyePos);
    
    void
    UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat);
    
    void
    UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp);

	void
	UpdateTextureTransformMatrixVariable(DirectX::XMMATRIX& mat);
    
    void
    UpdateMaterialVariable(Material const& mat);    

    void
    UpdateDiffuseMapVariable(ID3D11ShaderResourceView* const diffuseMap);    

    void
    UpdateDiffuseMapSamplerVariable(ID3D11SamplerState* const diffuseSampler);   

    void
    UpdateNormalMapVariable(ID3D11ShaderResourceView* const normalMap);    

    void
    UpdateNormalMapSamplerVariable(ID3D11SamplerState* const normalSampler); 

	void
	UpdateReflectionMapVariable(ID3D11ShaderResourceView* const reflectionMap);

	void
	UpdateReflectionMapSamplerVariable(ID3D11SamplerState* const reflectionSampler);

	void 
	UpdateFogVariable(Fog const& fog);

	void
	UpdateShadowingVariables(
		ID3D11SamplerState* const shadowMapSampler,
		std::vector<ID3D11ShaderResourceView*> const&  shadowMapCascades);

	// This is in its own function since it only occurs once per frame
	// No need to remap the CB per ApplyTechnique(...) calls
	void
	ApplyPerFrameConstantBuffer(ID3D11DeviceContext* const deviceContext);

	// This function unbinds all resources from the rasterizer
	void
	UnbindAll(ID3D11DeviceContext* const deviceContext);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);

	/////////////////////////////////////////////////////////////////
	ID3DBlob* mFlatVsBlob;
	ID3D11VertexShader* mFlatVS;
	ID3DBlob* mFlatPsBlob;
	ID3D11PixelShader* mFlatPS;

	ID3DBlob* mVsBlob;
	ID3D11VertexShader* mVS;
	ID3DBlob* mDefaultPsBlob;
	ID3D11PixelShader* mDefaultPS;
	ID3DBlob* mDiffusePsBlob;
	ID3D11PixelShader* mDiffusePS;
	ID3DBlob* mBumpPsBlob;
	ID3D11PixelShader* mBumpPS;
	ID3DBlob* mDiffBumpPsBlob;
	ID3D11PixelShader* mDiffBumpPS;

	ID3DBlob* mInstancedVsBlob;
	ID3D11VertexShader* mInstancedVS;
	ID3DBlob* mInstancedDefaultPsBlob;
	ID3D11PixelShader* mInstancedDefaultPS;
	ID3DBlob* mInstancedDiffusePsBlob;
	ID3D11PixelShader* mInstancedDiffusePS;
	ID3DBlob* mInstancedBumpPsBlob;
	ID3D11PixelShader* mInstancedBumpPS;
	ID3DBlob* mInstancedDiffBumpPsBlob;
	ID3D11PixelShader* mInstancedDiffBumpPS;

	struct CbPerFrame
	{	
		LightToGpu lights[MAX_LIGHTS];
		unsigned int numLights;
		DirectX::XMFLOAT3 eyePos;
		Fog fog;
	} mPerFrameVariables;
	DirectX::ConstantBuffer< CbPerFrame > mPerFrameCb;

	struct CbPerObject
	{
		DirectX::XMMATRIX worldMat;
		DirectX::XMMATRIX vpMat;
		DirectX::XMMATRIX texMat;
		Material material;
	} mPerObjectVariables;
	DirectX::ConstantBuffer< CbPerObject > mPerObjectCb;

	ID3D11SamplerState*          mDiffuseMapSampler;
	ID3D11ShaderResourceView*    mDiffuseMap;
	ID3D11SamplerState*          mNormalMapSampler;
	ID3D11ShaderResourceView*    mNormalMap;
	ID3D11SamplerState*          mReflectionMapSampler;
	ID3D11ShaderResourceView*    mReflectionMap;

	ID3D11SamplerState*          mShadowMapSampler;
	std::vector<ID3D11ShaderResourceView*>    mShadowMapCascades;

	// Since textures are expensive to submit to the gpu, keep track of what's already bound
	ID3D11ShaderResourceView*    mBoundedDiffuseMap;
	ID3D11ShaderResourceView*    mBoundedNormalMap;
	ID3D11ShaderResourceView*    mBoundedReflectionMap;
	/////////////////////////////////////////////////////////////////

	D3dRenderer* mRendererRef; /// Kept for recompile() func
};