/**  ForwardRenderer.cpp
 *
 */

#include <assert.h>
#include <vector>
#include "Debug/Debug.h"
#include "Systems/Renderer/Renderer.h"
#include "Systems/Renderer/RenderStateManager.h"
#include "Systems/Renderer/Camera.h"
#include "Systems/Renderer/MathHelper.h"
#include "BaseApp.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"

// Needed components
#include "Components/Renderable.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/ReflectionMapped.h"
#include "Components/Spatialized.h"
#include "Components/TextureTransformed.h"

#include "ForwardRenderer.h"

extern BaseApp* gApp;

ForwardRenderer::ForwardRenderer(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
{    
    // Create effect class
    mForwardRendererEffect = std::make_shared<ForwardRendererEffect>(renderer);

	mFogSettings.fogType = -1;
}

/*virtual*/ 
ForwardRenderer::~ForwardRenderer(void)
{
   
}

/*virtual*/ void
ForwardRenderer::MadeActive(void)
{    
    
}

/*virtual*/ void
ForwardRenderer::MadeInactive(void)
{   
   
}

/*virtual*/ void
ForwardRenderer::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const /*currentTime*/)
{
    std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
    if (!renderer)
        return;    
	
	renderer->SetBackBufferRenderTarget();
	auto dev_c = renderer->DeviceContext();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Forward Render Pass", 0);

	// Set blend state that blends alpha (for things like sprites)
 	ID3D11BlendState* state = NULL;	
	dev_c->OMSetBlendState(state, NULL, 0xFFFFFFFF);

	// Set default depth stencil state
	dev_c->OMSetDepthStencilState(NULL, NULL);

	ResetSRVs();
			
	// Get lights to fetch needed vars for lighting	
	std::vector<LightToGpu> lights_to_gpu;
	std::vector<ID3D11ShaderResourceView*> shadow_map_cascades;
	renderer->SetupLightsAndShadowMapsForGPU(lights_to_gpu, shadow_map_cascades);
	mForwardRendererEffect->UpdateLightArrayVariable(lights_to_gpu);
	mForwardRendererEffect->UpdateNumLightsVariable((unsigned int)lights_to_gpu.size());
	
	// Shadowing vars
	D3D11_SAMPLER_DESC samp_desc;
	samp_desc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.MipLODBias = 0.0f;
	samp_desc.MaxAnisotropy = 1;
	samp_desc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	samp_desc.BorderColor[0] = samp_desc.BorderColor[1] = samp_desc.BorderColor[2] = samp_desc.BorderColor[3] = 0;
	samp_desc.MinLOD = 0;
	samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
	auto sampler = renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), samp_desc);
	mForwardRendererEffect->UpdateShadowingVariables(sampler, shadow_map_cascades);


	// Update eye pos
	DirectX::XMFLOAT4 cam_pos = renderer->CurrentCamera()->Position();
	DirectX::XMFLOAT3 eye_pos = DirectX::XMFLOAT3(cam_pos.x, cam_pos.y, cam_pos.z);
	mForwardRendererEffect->UpdateWorldEyePosVariable(eye_pos);

	std::shared_ptr<Camera> camera = renderer->CurrentCamera();
	DirectX::XMMATRIX view =  XMLoadFloat4x4(&(camera->ViewMatrix()));

	// Update View Projection matrix variable
	DirectX::XMMATRIX proj =  XMLoadFloat4x4(&(camera->ProjMatrix()));
	DirectX::XMMATRIX vp =  view * proj;
	mForwardRendererEffect->UpdateViewProjectionMatrixVariable(vp); 

	// Set fog
	mForwardRendererEffect->UpdateFogVariable(mFogSettings);

	mForwardRendererEffect->ApplyPerFrameConstantBuffer(dev_c);
               
    // Iterate through families
	std::set<unsigned int> renderable_types_to_process;
	renderable_types_to_process.insert(Renderable::LIT);
	renderable_types_to_process.insert(Renderable::FLAT);
	for (std::set<unsigned int>::iterator type_iter = renderable_types_to_process.begin();
		 type_iter != renderable_types_to_process.end();
	     ++type_iter)
	{
		std::set<unsigned int>::const_iterator fam_iter = familyByRenderableType[*type_iter].begin();
		for (fam_iter; fam_iter != familyByRenderableType[*type_iter].end(); ++fam_iter)
		{
			// Get renderable component
			std::shared_ptr<IComponent> tmp_comp;			
				
			// Check for renderable to draw
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), tmp_comp);                
			if (tmp_comp)
			{
				Renderable* renderable = static_cast<Renderable*>(tmp_comp.get());	
				DrawNonInstancedRenderable(renderable, *fam_iter, renderer);
			}
		}    
	}

	// Draw instanced renderables
	DrawInstancedRenderables(renderer);

	mForwardRendererEffect->UnbindAll(dev_c);

	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();
}

void 
ForwardRenderer::RecompileShaders()
{
	if (mForwardRendererEffect) 
	{
		OutputDebugMsg("Recompiling Forward Renderer...\n");
		MadeInactive();
		mForwardRendererEffect->Recompile();
		MadeActive();
	}
}

void 
ForwardRenderer::DrawNonInstancedRenderable( Renderable *const renderable, unsigned int const& entityId, std::shared_ptr<D3dRenderer>& renderer )
{
	auto dev_c = renderer->DeviceContext();

	std::shared_ptr<IComponent> tmp_comp;
	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

	bool draw_with_diffuse = false;
	bool draw_with_bump = false;

	// Check if we need to change input layout
	//if (last_pipeline_setting != renderable->PipelineSettings())
	{
		if (Renderable::LIT == renderable->RenderableType())
		{
			dev_c->IASetInputLayout(mForwardRendererEffect->InputLayout(FR_DEFAULT_TECH));
			input_layout_desc = mForwardRendererEffect->InputLayoutDescription(FR_DEFAULT_TECH);
			assert( !input_layout_desc.empty() );
		}
		else if (Renderable::FLAT == renderable->RenderableType())
		{
			dev_c->IASetInputLayout(mForwardRendererEffect->InputLayout(FR_FLAT_TECH));
			input_layout_desc = mForwardRendererEffect->InputLayoutDescription(FR_FLAT_TECH);
			assert( !input_layout_desc.empty() );
		}				

		//last_pipeline_setting = (Renderable::RenderablePipelineSettings)renderable->PipelineSettings();
	}


	dev_c->IASetPrimitiveTopology(renderable->PrimitiveTopology());

	// Index buffer
	dev_c->IASetIndexBuffer(renderable->IndexBuffer().indexBuffer,
												renderable->IndexBuffer().indexBufferFormat, 
												renderable->IndexBuffer().indexBufferOffset );

	// Go through needed subinputs and accumulate vertex buffers
	std::vector<ID3D11Buffer*> buffers;
	std::vector<unsigned int> strides;
	std::vector<unsigned int> offsets;

	unsigned int vert_count = 0;

	std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();            
	for ( sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++ )
	{
		buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
		strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
		offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);    

		vert_count += renderable->VertexBuffer(*sub_input_iter).vertexCount;
	}
	dev_c->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

	// Set renderable's material
	mForwardRendererEffect->UpdateMaterialVariable(renderable->MaterialProperties());

	// Set raster state
	ID3D11RasterizerState* default_raster;
	if (renderable->BackCulled())
		default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::Default);
	else
		default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
	dev_c->RSSetState(default_raster);


	if (Renderable::LIT == renderable->RenderableType())
	{
		// Check for texture transformation
		DirectX::XMMATRIX tex_xform = DirectX::XMMatrixIdentity();
		gApp->EntityMngr()->GetComponentFromEntity(entityId, TextureTransformed::GetGuid(), tmp_comp);     
		if (tmp_comp)
			tex_xform = DirectX::XMLoadFloat4x4( &(static_cast<TextureTransformed*>(tmp_comp.get())->TextureTransformation()) );
		mForwardRendererEffect->UpdateTextureTransformMatrixVariable(tex_xform);			

		// Check for diffuse map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, DiffuseMapped::GetGuid(), tmp_comp);     
		if (tmp_comp)
		{
			DiffuseMapped* diff_map = static_cast<DiffuseMapped*>(tmp_comp.get());			
			mForwardRendererEffect->UpdateDiffuseMapSamplerVariable( renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc()));            
			mForwardRendererEffect->UpdateDiffuseMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), diff_map->DiffuseMapName(), true));
			draw_with_diffuse = true;
		}    

		// Check for normal map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, NormalMapped::GetGuid(), tmp_comp);     
		if (tmp_comp)
		{
			NormalMapped* normal_map = static_cast<NormalMapped*>(tmp_comp.get());			
			mForwardRendererEffect->UpdateNormalMapSamplerVariable( renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), normal_map->NormalMapSamplerDesc()));            
			mForwardRendererEffect->UpdateNormalMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), normal_map->NormalMapName()));
			draw_with_bump = true;
		}  

		// Check for reflection map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, ReflectionMapped::GetGuid(), tmp_comp);
		if (tmp_comp)
		{
			ReflectionMapped* reflection_map = static_cast<ReflectionMapped*>(tmp_comp.get());			
			mForwardRendererEffect->UpdateReflectionMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), reflection_map->ReflectionMapSamplerDesc()));
			mForwardRendererEffect->UpdateReflectionMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), reflection_map->ReflectionMapName()));
		}
	}

	// Update world mat from Spatialized component
	DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Spatialized::GetGuid(), tmp_comp);     
	if (tmp_comp)
	{
		Spatialized* spatial_comp = static_cast<Spatialized*>(tmp_comp.get());
		world = XMLoadFloat4x4(&spatial_comp->LocalTransformation());
	}
	mForwardRendererEffect->UpdateWorldMatrixVariable(world);

	// Apply
	if (Renderable::FLAT == renderable->RenderableType())
	{
		mForwardRendererEffect->ApplyTechnique(FR_FLAT_TECH, dev_c);
	}
	else if (Renderable::LIT == renderable->RenderableType())
	{
		if (draw_with_diffuse)
			if (draw_with_bump)
				mForwardRendererEffect->ApplyTechnique(FR_DIFFUSE_BUMP_TECH, dev_c);
			else
				mForwardRendererEffect->ApplyTechnique(FR_DIFFUSE_TECH, dev_c);
		else
			if (draw_with_bump)
				mForwardRendererEffect->ApplyTechnique(FR_BUMP_TECH, dev_c);
			else
				mForwardRendererEffect->ApplyTechnique(FR_DEFAULT_TECH, dev_c);
	}

	if (0 == renderable->IndexBuffer().indexCount)
		dev_c->Draw(vert_count, 0);
	else
		dev_c->DrawIndexed(renderable->IndexBuffer().indexCount, 0, 0);
}

void 
ForwardRenderer::DrawInstancedRenderables( std::shared_ptr<D3dRenderer>& renderer )
{
	auto dev_c = renderer->DeviceContext();

	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
	InstancingManager::InstancedBatchesResources& rsrcs = renderer->InstancingMngr()->InstancedBatchesRsrcs();

	{			
		dev_c->IASetInputLayout(mForwardRendererEffect->InputLayout(FR_INSTANCED_DEFAULT_TECH));
		input_layout_desc = mForwardRendererEffect->InputLayoutDescription(FR_INSTANCED_DEFAULT_TECH);
		assert( !input_layout_desc.empty() );
	}
				
	std::map< unsigned int, std::shared_ptr<Renderable> >::iterator iter = rsrcs.batchRenderableRef.begin();
	for (; iter != rsrcs.batchRenderableRef.end(); ++iter)
	{
		// Make sure it's a LIT or FLAT type of renderable
		if (Renderable::LIT != rsrcs.batchRenderableRef[iter->first]->RenderableType() && 
			Renderable::FLAT != rsrcs.batchRenderableRef[iter->first]->RenderableType() )
			continue;

		bool draw_with_diffuse = false;
		bool draw_with_bump = false;

		// Go through needed subinputs and accumulate vertex buffers
		std::vector<ID3D11Buffer*> buffers;
		std::vector<unsigned int> strides;
		std::vector<unsigned int> offsets;

		unsigned int vert_count = 0;

		std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();            
		for ( sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++ )
		{
			if (InputLayoutManager::INSTANCED == *sub_input_iter)
				continue;
			buffers.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).vertexBuffer);
			strides.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).stride);
			offsets.push_back(rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).offset);  

			vert_count += rsrcs.batchRenderableRef[iter->first]->VertexBuffer(*sub_input_iter).vertexCount;
		}
		buffers.push_back(rsrcs.instancedBuffer[iter->first]);
		strides.push_back(renderer->InstancingMngr()->InstancedBufferStride());
		offsets.push_back(0);              
		dev_c->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

		dev_c->IASetPrimitiveTopology(rsrcs.batchRenderableRef[iter->first]->PrimitiveTopology());

		// Index buffer
		dev_c->IASetIndexBuffer(rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBuffer,
								rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBufferFormat, 
								rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexBufferOffset );

		// Set instanced batch's material
		mForwardRendererEffect->UpdateMaterialVariable(rsrcs.batchRenderableRef[iter->first]->MaterialProperties());

		// Set raster state
		ID3D11RasterizerState* default_raster;
		if (rsrcs.batchRenderableRef[iter->first]->BackCulled())
			default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::Default);
		else
			default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
		dev_c->RSSetState(default_raster);


		if (Renderable::LIT == rsrcs.batchRenderableRef[iter->first]->RenderableType())
		{		
			DirectX::XMMATRIX tex_xform = DirectX::XMMatrixIdentity();
			mForwardRendererEffect->UpdateTextureTransformMatrixVariable(tex_xform);

			// Check for diffuse map
			if (rsrcs.batchDiffuseMapRef[iter->first])
			{					
				mForwardRendererEffect->UpdateDiffuseMapSamplerVariable( renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), rsrcs.batchDiffuseMapRef[iter->first]->DiffuseMapSamplerDesc()));            
				mForwardRendererEffect->UpdateDiffuseMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), rsrcs.batchDiffuseMapRef[iter->first]->DiffuseMapName(), true));
				draw_with_diffuse = true;
			}    

			// Check for normal map				
			if (rsrcs.batchNormalMapRef[iter->first])
			{					
				mForwardRendererEffect->UpdateNormalMapSamplerVariable( renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), rsrcs.batchNormalMapRef[iter->first]->NormalMapSamplerDesc()));            
				mForwardRendererEffect->UpdateNormalMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), rsrcs.batchNormalMapRef[iter->first]->NormalMapName()));
				draw_with_bump = true;
			}  

			// Check for reflection map
			if (rsrcs.batchReflectionMapRef[iter->first])
			{
				mForwardRendererEffect->UpdateReflectionMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), rsrcs.batchReflectionMapRef[iter->first]->ReflectionMapSamplerDesc()));
				mForwardRendererEffect->UpdateReflectionMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), rsrcs.batchReflectionMapRef[iter->first]->ReflectionMapName()));
			}
		}

		// Update world mat from Spatialized component
		DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();			
		mForwardRendererEffect->UpdateWorldMatrixVariable(world);

		// Apply
		{
			if (draw_with_diffuse)
				if (draw_with_bump)
					mForwardRendererEffect->ApplyTechnique(FR_INSTANCED_DIFFUSE_BUMP_TECH, dev_c);
				else
					mForwardRendererEffect->ApplyTechnique(FR_INSTANCED_DIFFUSE_TECH, dev_c);
			else
				if (draw_with_bump)
					mForwardRendererEffect->ApplyTechnique(FR_INSTANCED_BUMP_TECH, dev_c);
				else
					mForwardRendererEffect->ApplyTechnique(FR_INSTANCED_DEFAULT_TECH, dev_c);
		}
		
		if (0 == rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexCount)
			dev_c->DrawInstanced(vert_count, rsrcs.bufferSize[iter->first], 0, 0);
		else
			dev_c->DrawIndexedInstanced(rsrcs.batchRenderableRef[iter->first]->IndexBuffer().indexCount, rsrcs.bufferSize[iter->first], 0, 0, 0);
	}	
}

void 
ForwardRenderer::ResetSRVs()
{
	if (mForwardRendererEffect)
	{
		mForwardRendererEffect->UpdateDiffuseMapVariable(nullptr);
		mForwardRendererEffect->UpdateNormalMapVariable(nullptr);
		mForwardRendererEffect->UpdateReflectionMapVariable(nullptr);
	}
}
