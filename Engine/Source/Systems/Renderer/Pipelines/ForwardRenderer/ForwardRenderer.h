/**  ForwardRenderer.h
 *
 *   Forward Rendering pipeline.
 */

#pragma once

#include "Systems/Renderer/Pipeline.h"
#include "ForwardRendererEffect.h"

class ForwardRenderer : public Pipeline
{
public:
    ForwardRenderer(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~ForwardRenderer(void);    
          
    virtual void
    MadeActive(void);

    virtual void
    MadeInactive(void);

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime); 

	virtual void
	RecompileShaders();

	Fog&
	FogSettings() { return mFogSettings; }
        
private:
	void
	DrawNonInstancedRenderable(Renderable *const renderable, unsigned int const& entityId, std::shared_ptr<D3dRenderer>& renderer);

	void
	DrawInstancedRenderables(std::shared_ptr<D3dRenderer>& renderer);

	void
	ResetSRVs();
	   
   // Effect
   std::shared_ptr<ForwardRendererEffect> mForwardRendererEffect;   

   // Other rendering options
   Fog mFogSettings;
};