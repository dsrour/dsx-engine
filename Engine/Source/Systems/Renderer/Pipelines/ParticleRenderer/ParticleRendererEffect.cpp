#include <assert.h>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "ParticleRendererEffect.h"

extern std::wstring gEngineRootDir;

ParticleRendererEffect::ParticleRendererEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mInitCsBlob = nullptr;
	mPhysicsCsBlob = nullptr;
	mVsBlob = nullptr;
	mGsBlob = nullptr;
	mPsBlob = nullptr;

	mInitCS = nullptr;
	mPhysicsCS = nullptr;
	mVS = nullptr;
	mGS = nullptr;
	mPS = nullptr;

	mCbParticleSystemSettingsConstantBuffer.Create(renderer->Device());
	mCbPerFrameConstantBuffer.Create(renderer->Device());
	
	ZeroMemory(&mCbParticleSystemSettings, sizeof(CbParticleSystemSettings));
	ZeroMemory(&mCbPerFrame, sizeof(CbPerFrame));

	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}

/*virtual*/
ParticleRendererEffect::~ParticleRendererEffect()
{   
	if (mInitCsBlob)
		mInitCsBlob->Release();
	if (mPhysicsCsBlob)
		mPhysicsCsBlob->Release();
	if (mVsBlob)
		mVsBlob->Release();
	if (mGsBlob)
		mGsBlob->Release();
	if (mPsBlob)
		mPsBlob->Release();

	if (mInitCS)
		mInitCS->Release();
	if (mPhysicsCS)
		mPhysicsCS->Release();
	if (mVS)
		mVS->Release();
	if (mGS)
		mGS->Release();
	if (mPS)
		mPS->Release();
}

void
ParticleRendererEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	// Load shaders
	cso_name  = std::wstring(PARTICLE_SYSTEM_INIT_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInitCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mInitCsBlob->GetBufferPointer(),
		mInitCsBlob->GetBufferSize(),
		NULL,
		&mInitCS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(PARTICLE_SYSTEM_PHYSICS_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mPhysicsCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mPhysicsCsBlob->GetBufferPointer(),
		mPhysicsCsBlob->GetBufferSize(),
		NULL,
		&mPhysicsCS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(PARTICLE_SYSTEM_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mVsBlob->GetBufferPointer(),
		mVsBlob->GetBufferSize(),
		NULL,
		&mVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(PARTICLE_SYSTEM_GS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mGsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateGeometryShader(
		mGsBlob->GetBufferPointer(),
		mGsBlob->GetBufferSize(),
		NULL,
		&mGS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(PARTICLE_SYSTEM_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mPsBlob->GetBufferPointer(),
		mPsBlob->GetBufferSize(),
		NULL,
		&mPS);
	assert(SUCCEEDED(hr));
}

void 
ParticleRendererEffect::UpdateParticleSystemVariables(ParticleSystemVars& vars)
{
	mCbParticleSystemSettings.originPos = vars.originPos;
	mCbParticleSystemSettings.spawnTimeOffset = vars.spawnTimeOffset;
	mCbParticleSystemSettings.spawnDirection = vars.spawnDirection;
	mCbParticleSystemSettings.respawnDelay = vars.respawnDelay;
	mCbParticleSystemSettings.boundsCenter = vars.boundsCenter;
	mCbParticleSystemSettings.timeDelta = vars.timeDelta;
	mCbParticleSystemSettings.boundsExtents = vars.boundsExtents;
	mCbParticleSystemSettings.lifeTime = vars.lifeTime;
	mCbParticleSystemSettings.gravity = vars.gravity;

	mCbPerFrame.mvp = DirectX::XMMatrixTranspose(vars.mvp);
	mCbPerFrame.billboardDx = vars.billboardDx;
	mCbPerFrame.lifeTime_ = vars.lifeTime;
	mCbPerFrame.billboardDy = vars.billboardDy;
	mCbPerFrame.colorSpawn = vars.colorSpawn;
	mCbPerFrame.colorDie = vars.colorDie;
}

void 
ParticleRendererEffect::UpdateParticleTextureSamplerVariable(ID3D11SamplerState* const sampler)
{
	mSampler = sampler;
}

void 
ParticleRendererEffect::UpdateParticleTextureVariable(ID3D11ShaderResourceView* const texture)
{
	mParticleTexture = texture;
}

void 
ParticleRendererEffect::UpdateParticleEmitterBuffersResources(ID3D11UnorderedAccessView* const posAndTime, ID3D11ShaderResourceView* const posAndTimeRo, ID3D11UnorderedAccessView* const dir)
{
	mPosTimeUav = posAndTime;
	mPosTimeSrv = posAndTimeRo;
	mDirUav = dir;
}

void
ParticleRendererEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (PARTICLE_SYSTEM_INIT_TECH == techName ||
		PARTICLE_SYSTEM_PHYSICS_TECH == techName)
	{
		mCbParticleSystemSettingsConstantBuffer.SetData(deviceContext, mCbParticleSystemSettings);

		auto cb = mCbParticleSystemSettingsConstantBuffer.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11UnorderedAccessView* uavs[] = { mPosTimeUav, mDirUav };
		unsigned int init_cnts[] = { 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 2, uavs, init_cnts);
	}

	if (PARTICLE_SYSTEM_INIT_TECH == techName)
	{
		deviceContext->CSSetShader(mInitCS, nullptr, 0);
	}
	else if (PARTICLE_SYSTEM_PHYSICS_TECH == techName)
	{
		deviceContext->CSSetShader(mPhysicsCS, nullptr, 0);
	}
	else if (PARTICLE_SYSTEM_RENDER_TECH == techName)
	{
		mCbPerFrameConstantBuffer.SetData(deviceContext, mCbPerFrame);

		deviceContext->VSSetShaderResources(0, 1, &mPosTimeSrv);
		deviceContext->VSSetShader(mVS, nullptr, 0);
		
		auto cb = mCbPerFrameConstantBuffer.GetBuffer();
		deviceContext->GSSetConstantBuffers(0, 1, &cb);
		deviceContext->GSSetShader(mGS, nullptr, 0);
		
		deviceContext->PSSetShaderResources(1, 1, &mParticleTexture);
		deviceContext->PSSetSamplers(0, 1, &mSampler);
		deviceContext->PSSetShader(mPS, nullptr, 0);
	}
}

void 
ParticleRendererEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;
	ID3D11UnorderedAccessView* nuavs[] = { nullptr, nullptr };
	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11SamplerState* ns = nullptr;

	if (PARTICLE_SYSTEM_INIT_TECH == techName ||
		PARTICLE_SYSTEM_PHYSICS_TECH == techName)
	{
		deviceContext->CSSetShader(nullptr, nullptr, 0);
		deviceContext->CSSetConstantBuffers(0, 1, &nb);
		
		unsigned int init_cnts[] = { 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 2, &nuavs[0], init_cnts);
	}
	else if (PARTICLE_SYSTEM_RENDER_TECH == techName)
	{
		deviceContext->VSSetShaderResources(0, 1, &nsrv);
		deviceContext->VSSetShader(nullptr, nullptr, 0);

		deviceContext->GSSetConstantBuffers(0, 1, &nb);
		deviceContext->GSSetShader(nullptr, nullptr, 0);

		deviceContext->PSSetShaderResources(1, 1, &nsrv);
		deviceContext->PSSetSamplers(0, 1, &ns);
		deviceContext->PSSetShader(nullptr, nullptr, 0);
	}
}