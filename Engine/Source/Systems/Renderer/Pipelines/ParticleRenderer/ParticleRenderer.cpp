/**  ParticleRenderer.cpp
 *
 */

#include <assert.h>
#include <vector>
#include "Debug/Debug.h"
#include "Systems/Renderer/Renderer.h"
#include "Systems/Renderer/RenderStateManager.h"
#include "Systems/Renderer/Camera.h"
#include "Systems/Renderer/MathHelper.h"
#include "BaseApp.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"

// Needed components
#include "Components/ParticleEmitting.h"
#include "Components/DiffuseMapped.h"
#include "Components/Bounded.h"
#include "Components/Spatialized.h"

#include "ParticleRenderer.h"

extern BaseApp* gApp;

ParticleRenderer::ParticleRenderer(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
{    
    // Create effect class
    mParticleRendererEffect = std::make_shared<ParticleRendererEffect>(renderer);

	// Create state descs
	ZeroMemory(&mDepthStencilDesc, sizeof(mDepthStencilDesc));
	mDepthStencilDesc.DepthEnable = false;
	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	mDepthStencilDesc.StencilEnable = true;
	mDepthStencilDesc.StencilReadMask = 0xFF;
	mDepthStencilDesc.StencilWriteMask = 0xFF;
	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	ZeroMemory(&mBlendDesc, sizeof(mBlendDesc));
	mBlendDesc.AlphaToCoverageEnable = false;
	mBlendDesc.IndependentBlendEnable = false;
	mBlendDesc.RenderTarget[0].BlendEnable = true;
	mBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
}

/*virtual*/ 
ParticleRenderer::~ParticleRenderer(void)
{
   
}

/*virtual*/ void
ParticleRenderer::MadeActive(void)
{    
    
}

/*virtual*/ void
ParticleRenderer::MadeInactive(void)
{   
	mInitedParticlesRenderables.clear();	
}

/*virtual*/ void
ParticleRenderer::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime)
{
	static double old_frame_time = currentTime;

	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Particle Render Pass", 0);

	renderer->SetBackBufferRenderTarget();

	// Set raster state
	ID3D11RasterizerState* raster_state = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
	dev_c->RSSetState(raster_state);

	// Set blend state
	dev_c->OMSetBlendState(renderer->StateMngr()->GetOrCreateBlendState(renderer->Device(), mBlendDesc), NULL, 0xffffffff);

	// Set depth stencil state
	dev_c->OMSetDepthStencilState(renderer->StateMngr()->GetOrCreateDepthStencilState(renderer->Device(), mDepthStencilDesc), 1);

	// Set primitive topology to point list
	dev_c->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	
	
	// Iterate through families
	std::set<unsigned int>::const_iterator fam_iter = familyByRenderableType[Renderable::PARTICLES].begin();
	for (fam_iter; fam_iter != familyByRenderableType[Renderable::PARTICLES].end(); fam_iter++)
	{
		unsigned int entity_id = *fam_iter;		
		
		// Get renderable component
		std::shared_ptr<IComponent> tmp_comp_particle_emitting;
		std::shared_ptr<IComponent> tmp_comp_spatialized;
		std::shared_ptr<IComponent> tmp_comp_texture;
		std::shared_ptr<IComponent> tmp_comp_bounded;

		// Check for particles to draw
		gApp->EntityMngr()->GetComponentFromEntity(entity_id, ParticleEmitting::GetGuid(), tmp_comp_particle_emitting);
		gApp->EntityMngr()->GetComponentFromEntity(entity_id, Spatialized::GetGuid(), tmp_comp_spatialized);
		gApp->EntityMngr()->GetComponentFromEntity(entity_id, DiffuseMapped::GetGuid(), tmp_comp_texture);
		gApp->EntityMngr()->GetComponentFromEntity(entity_id, Bounded::GetGuid(), tmp_comp_bounded);

		if (tmp_comp_particle_emitting && tmp_comp_spatialized && tmp_comp_texture)
		{
			ParticleEmitting* particle_emitting = static_cast<ParticleEmitting*>(tmp_comp_particle_emitting.get());
			Spatialized* spatialized = static_cast<Spatialized*>(tmp_comp_spatialized.get());
			DiffuseMapped* texture = static_cast<DiffuseMapped*>(tmp_comp_texture.get());
			Bounded* bounded = static_cast<Bounded*>(tmp_comp_bounded.get());
			
			// Set effect variables ///////////////////////////////////////////////////////
			ParticleRendererEffect::ParticleSystemVars vars;

			std::shared_ptr<Camera> camera = renderer->CurrentCamera();					
			DirectX::XMMATRIX view = XMLoadFloat4x4(&(camera->ViewMatrix()));			
			DirectX::XMMATRIX proj = XMLoadFloat4x4(&(camera->ProjMatrix()));
			DirectX::XMMATRIX vp = view * proj;

			DirectX::XMFLOAT4X4 model_view_4x4; XMStoreFloat4x4(&model_view_4x4, DirectX::XMMatrixTranspose(view));
			float const* dx = model_view_4x4.m[0];
			float const* dy = model_view_4x4.m[1];
			vars.billboardDx = DirectX::XMFLOAT3(dx[0] * particle_emitting->ParticleSize(), dx[1] * particle_emitting->ParticleSize(), dx[2] * particle_emitting->ParticleSize());
			vars.billboardDy = DirectX::XMFLOAT3(dy[0] * particle_emitting->ParticleSize(), dy[1] * particle_emitting->ParticleSize(), dy[2] * particle_emitting->ParticleSize());
			vars.boundsCenter = bounded->AabbBounds().Center;
			vars.boundsExtents = bounded->AabbBounds().Extents;
			vars.colorDie = particle_emitting->ColorDie();
			vars.colorSpawn = particle_emitting->ColorSpawn();
			vars.gravity = particle_emitting->Gravity();
			vars.lifeTime = particle_emitting->LifeTime();
			vars.mvp = vp;
			vars.originPos = spatialized->LocalPosition();
			vars.respawnDelay = particle_emitting->RespawnDelay();
			vars.spawnDirection = particle_emitting->SpawnDirection();
			vars.spawnTimeOffset = particle_emitting->SpawnTimeOffset();
			vars.timeDelta = (float)(currentTime-old_frame_time);
			//OutputDebugMsg(to_string(vars.timeDelta) + "\n");

			mParticleRendererEffect->UpdateParticleSystemVariables(vars);		

			unsigned int buff_index = 0;
			mParticleRendererEffect->UpdateParticleEmitterBuffersResources(particle_emitting->PositionAndTimeUav(), particle_emitting->PositionAndTimeSrv(), particle_emitting->DirectionUav());
			mParticleRendererEffect->UpdateParticleTextureVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), texture->DiffuseMapName()));
			mParticleRendererEffect->UpdateParticleTextureSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), texture->DiffuseMapSamplerDesc()));
			///////////////////////////////////////////////////////////////////////////////

			// See if initialization is needed
			if (mInitedParticlesRenderables.find(entity_id) == mInitedParticlesRenderables.end())
			{
				mInitedParticlesRenderables.insert(entity_id);		

				mParticleRendererEffect->ApplyTechnique(PARTICLE_SYSTEM_INIT_TECH, dev_c);
				dev_c->Dispatch(particle_emitting->NumParticles() / 32, 1, 1);
				mParticleRendererEffect->ClearTechnique(PARTICLE_SYSTEM_INIT_TECH, dev_c);
			}
			else
			{
				// Do physics
				mParticleRendererEffect->ApplyTechnique(PARTICLE_SYSTEM_PHYSICS_TECH, dev_c);
				dev_c->Dispatch(particle_emitting->NumParticles() / 32, 1, 1);
				mParticleRendererEffect->ClearTechnique(PARTICLE_SYSTEM_PHYSICS_TECH, dev_c);

				// Bound resources				

				// Render
				dev_c->IASetInputLayout(NULL);
				mParticleRendererEffect->ApplyTechnique(PARTICLE_SYSTEM_RENDER_TECH, dev_c);
				dev_c->Draw(particle_emitting->NumParticles(), NULL);
				mParticleRendererEffect->ClearTechnique(PARTICLE_SYSTEM_RENDER_TECH, dev_c);
			}
		}		
	}
		
	old_frame_time = currentTime;

	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();
}

void 
ParticleRenderer::RecompileShaders()
{
	//if (mParticleRendererEffect) 
	//{
	//	OutputDebugMsg("Recompiling Particle Renderer...\n");
	//	MadeInactive();
	//	mParticleRendererEffect->Recompile();
	//	MadeActive();
	//}
}

