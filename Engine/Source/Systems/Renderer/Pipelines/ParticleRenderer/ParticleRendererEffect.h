#pragma once
#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class ParticleRendererEffect : public Effect
{
public:
    ParticleRendererEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~ParticleRendererEffect(void); 

	struct ParticleSystemVars
	{
		DirectX::XMFLOAT3 originPos;			// where the particles are being emitted from.
		float  spawnTimeOffset;					// Time difference between consecutive spawning particles.  The smaller this is, the more of a stream appearance there'll be.
		DirectX::XMFLOAT3 spawnDirection;		// Direction to spawn particles towards.
		float  respawnDelay;					// Death duration.
		DirectX::XMFLOAT3 boundsCenter;			// Center of bounds to bounce particles off.
		DirectX::XMFLOAT3 boundsExtents;		// Extents of bounds to bounce particles off.
		float  timeDelta;						// Time delta for simulation.		
		float  lifeTime;						// How long a particle lives before disappearing.
		float  gravity;							// Gravity.

		DirectX::XMMATRIX	mvp;
		DirectX::XMFLOAT3   billboardDx;
		DirectX::XMFLOAT3   billboardDy;
		DirectX::XMFLOAT3   colorSpawn;
		DirectX::XMFLOAT3   colorDie;
	};
	void
	UpdateParticleSystemVariables(ParticleSystemVars& vars);
	
	void
	UpdateParticleTextureSamplerVariable(ID3D11SamplerState* const sampler);

	void
	UpdateParticleTextureVariable(ID3D11ShaderResourceView* const texture);

	void
	UpdateParticleEmitterBuffersResources(ID3D11UnorderedAccessView* const posAndTime, ID3D11ShaderResourceView* const posAndTimeRo, ID3D11UnorderedAccessView* const dir);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);
                
private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);


	////////////////////
	ID3DBlob* mInitCsBlob;
	ID3D11ComputeShader* mInitCS;
	ID3DBlob* mPhysicsCsBlob;
	ID3D11ComputeShader* mPhysicsCS;
	ID3DBlob* mVsBlob;
	ID3D11VertexShader* mVS;
	ID3DBlob* mGsBlob;
	ID3D11GeometryShader* mGS;
	ID3DBlob* mPsBlob;
	ID3D11PixelShader* mPS;
	
	struct CbParticleSystemSettings
	{
		DirectX::XMFLOAT3	originPos;
		float				spawnTimeOffset;
		DirectX::XMFLOAT3	spawnDirection;
		float				respawnDelay;
		DirectX::XMFLOAT3	boundsCenter;
		float				timeDelta;
		DirectX::XMFLOAT3	boundsExtents;
		float				lifeTime;
		float				gravity;
		DirectX::XMFLOAT3	padding;
	} mCbParticleSystemSettings;	
	DirectX::ConstantBuffer< CbParticleSystemSettings > mCbParticleSystemSettingsConstantBuffer;
	

	struct CbPerFrame
	{
		DirectX::XMMATRIX		mvp;
		DirectX::XMFLOAT3		billboardDx;
		float					lifeTime_;
		DirectX::XMFLOAT3		billboardDy;
		float					padding1;
		DirectX::XMFLOAT3		colorSpawn;
		float					padding2;
		DirectX::XMFLOAT3		colorDie;
		float					padding3;
	} mCbPerFrame;
	DirectX::ConstantBuffer< CbPerFrame > mCbPerFrameConstantBuffer;
	
	ID3D11SamplerState* mSampler;
	ID3D11ShaderResourceView* mParticleTexture;
	ID3D11UnorderedAccessView* mPosTimeUav;
	ID3D11ShaderResourceView* mPosTimeSrv;
	ID3D11UnorderedAccessView* mDirUav;
	////////////////////
	
	D3dRenderer* mRendererRef; /// Kept for recompile() func
};