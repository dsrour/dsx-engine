/**  ParticleRenderer.h
 */

#pragma once

#include <set>
#include <map>
#include "Systems/Renderer/Pipeline.h"
#include "ParticleRendererEffect.h"

class ParticleRenderer : public Pipeline
{
public:
    ParticleRenderer(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~ParticleRenderer(void);    
          
    virtual void
    MadeActive(void);

    virtual void
    MadeInactive(void);

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime); 
	
	virtual void
	RecompileShaders();

	/// Will reinitialize incoming ParticleEmitting entities' buffers.
	void
	Reset() { mInitedParticlesRenderables.clear(); }
        
private:
	// Effect
	std::shared_ptr<ParticleRendererEffect> mParticleRendererEffect;  

	std::set<unsigned int> mInitedParticlesRenderables;  // Renderables of type "PARTICLES" that were already inited	

	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
	D3D11_BLEND_DESC mBlendDesc;
};