#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "FurRendererEffect.h"

extern std::wstring gEngineRootDir;

FurRendererEffect::FurRendererEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mVsBlob = nullptr;
	mVS = nullptr;
	mPsBlob = nullptr;
	mPS = nullptr;

	mDiffuseMapSampler = nullptr;
	mDiffuseMap = nullptr;
	mLayerMapSampler = nullptr;
	mLayerMap = nullptr;

	mBoundedDiffuseMap = nullptr;
	mBoundedLayerMap = nullptr;

	mPerFrameCb.Create(renderer->Device());
	mPerObjectCb.Create(renderer->Device());

	ZeroMemory(&mPerFrameVariables, sizeof(CbPerFrame));
	ZeroMemory(&mPerObjectVariables, sizeof(CbPerObject));

	mRendererRef = renderer.get();
	Init(renderer.get());
}

/*virtual*/
FurRendererEffect::~FurRendererEffect()
{    
	if (mVsBlob)
		mVsBlob->Release();
	if (mVS)
		mVS->Release();
	if (mPsBlob)
		mPsBlob->Release();
	if (mPS)
		mPS->Release();
}

void
FurRendererEffect::Init(D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/)
{
	std::wstring cso_name;
	HRESULT hr;

	cso_name = std::wstring(FUR_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mVsBlob->GetBufferPointer(),
		mVsBlob->GetBufferSize(),
		NULL,
		&mVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(FUR_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mPsBlob->GetBufferPointer(),
		mPsBlob->GetBufferSize(),
		NULL,
		&mPS);
	assert(SUCCEEDED(hr));

	{
		// Create input layout descs
		std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

		input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
		input_layout_desc.push_back(InputLayoutManager::TEXTURE);
		mInputLayoutDescs[FUR_TECH] = input_layout_desc;
		mInputLayouts[FUR_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
	}
}

void 
FurRendererEffect::UpdateFurLayerSamplerVariable(ID3D11SamplerState* const sampler)
{
	mLayerMapSampler = sampler;
}

void 
FurRendererEffect::UpdateFurLengthVariable(float const furLength)
{
	mPerObjectVariables.furLength = furLength;
}

void 
FurRendererEffect::UpdateFurLayerVariable(ID3D11ShaderResourceView* const texture)
{
	mLayerMap = texture;
}

void 
FurRendererEffect::UpdateForceVariable(DirectX::XMFLOAT3& force)
{
	mPerObjectVariables.force = force;
}

void 
FurRendererEffect::UpdateLayerRelativePositionVariable(float const relPos)
{
	mPerObjectVariables.layerRelativePositionToStrand = relPos;
}

void 
FurRendererEffect::UpdateLightArrayVariable(std::vector<LightToGpu> const& lights)
{
	for (int i = 0; i < (std::min)((int)lights.size(), MAX_LIGHTS); i++)
		mPerFrameVariables.lights[i] = lights[i];
}

void 
FurRendererEffect::UpdateNumLightsVariable(unsigned int const& numLights)
{
	mPerFrameVariables.numLights = numLights;
}

void 
FurRendererEffect::UpdateWorldEyePosVariable(DirectX::XMFLOAT3& eyePos)
{
	mPerFrameVariables.eyePos = eyePos;
}

void 
FurRendererEffect::UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat)
{
	mPerObjectVariables.worldMat = DirectX::XMMatrixTranspose(worldMat);
}

void 
FurRendererEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp)
{
	mPerObjectVariables.vpMat = DirectX::XMMatrixTranspose(vp);
}

void 
FurRendererEffect::UpdateTextureTransformMatrixVariable(DirectX::XMMATRIX& mat)
{
	mPerObjectVariables.texMat = DirectX::XMMatrixTranspose(mat);
}

void 
FurRendererEffect::UpdateMaterialVariable(Material const& mat)
{
	mPerObjectVariables.material = mat;
}

void 
FurRendererEffect::UpdateDiffuseMapVariable(ID3D11ShaderResourceView* const diffuseMap)
{
	mDiffuseMap = diffuseMap;
}

void 
FurRendererEffect::UpdateDiffuseMapSamplerVariable(ID3D11SamplerState* const diffuseSampler)
{
	mDiffuseMapSampler = diffuseSampler;
}

void 
FurRendererEffect::UpdateFogVariable(Fog const& fog)
{
	mPerFrameVariables.fog = fog;
}

void 
FurRendererEffect::ApplyPerFrameConstantBuffer(ID3D11DeviceContext* const deviceContext)
{
	mPerFrameCb.SetData(deviceContext, mPerFrameVariables);

	auto cb = mPerFrameCb.GetBuffer();
	deviceContext->VSSetConstantBuffers(0, 1, &cb);
	deviceContext->PSSetConstantBuffers(0, 1, &cb);
}

void 
FurRendererEffect::UnbindAll(ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;
	deviceContext->VSSetConstantBuffers(0, 1, &nb);
	deviceContext->VSSetConstantBuffers(1, 1, &nb);
	deviceContext->PSSetConstantBuffers(0, 1, &nb);
	deviceContext->PSSetConstantBuffers(1, 1, &nb);

	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11SamplerState* ns = nullptr;

	deviceContext->PSSetShaderResources(0, 1, &nsrv);
	deviceContext->PSSetSamplers(0, 1, &ns);
	deviceContext->PSSetShaderResources(1, 1, &nsrv);
	deviceContext->PSSetSamplers(1, 1, &ns);
	deviceContext->PSSetShaderResources(2, 1, &nsrv);
	deviceContext->PSSetSamplers(2, 1, &ns);
	deviceContext->PSSetShaderResources(3, 1, &nsrv);
	deviceContext->PSSetSamplers(3, 1, &ns);

	mBoundedDiffuseMap = nullptr;
	mBoundedLayerMap = nullptr;

	mLayerMap = nullptr;
	mLayerMapSampler = nullptr;

	mDiffuseMapSampler = nullptr;
	mDiffuseMap = nullptr;
}

void 
FurRendererEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	mPerObjectCb.SetData(deviceContext, mPerObjectVariables);

	auto cb = mPerObjectCb.GetBuffer();
	deviceContext->VSSetConstantBuffers(1, 1, &cb);
	deviceContext->PSSetConstantBuffers(1, 1, &cb);

	if (FUR_TECH == techName)
	{
		auto default_st = mRendererRef->StateMngr()->DefaultSamplerState();

		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mPS, nullptr, 0);

		if (mDiffuseMapSampler)
			deviceContext->PSSetSamplers(0, 1, &mDiffuseMapSampler);
		else
			deviceContext->PSSetSamplers(0, 1, &default_st);

		deviceContext->PSSetSamplers(1, 1, &default_st);
		deviceContext->PSSetSamplers(2, 1, &default_st);

		if (mLayerMapSampler)
			deviceContext->PSSetSamplers(3, 1, &mLayerMapSampler);
		else
			deviceContext->PSSetSamplers(3, 1, &default_st);

		if (mDiffuseMap && mDiffuseMap != mBoundedDiffuseMap)
		{
			deviceContext->PSSetShaderResources(0, 1, &mDiffuseMap);
			mBoundedDiffuseMap = mDiffuseMap;
		}

		if (mLayerMap && mLayerMap != mBoundedLayerMap)
		{
			deviceContext->PSSetShaderResources(3, 1, &mLayerMap);
			mBoundedLayerMap = mLayerMap;
		}
	}
}

void 
FurRendererEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	deviceContext->VSSetShader(nullptr, nullptr, 0);
	deviceContext->PSSetShader(nullptr, nullptr, 0);
}


