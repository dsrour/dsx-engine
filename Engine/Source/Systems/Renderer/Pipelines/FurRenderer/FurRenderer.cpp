#include <assert.h>
#include <vector>
#include "Debug/Debug.h"
#include "Systems/Renderer/Renderer.h"
#include "Systems/Renderer/RenderStateManager.h"
#include "Systems/Renderer/Camera.h"
#include "Systems/Renderer/MathHelper.h"
#include "BaseApp.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"

// Needed components
#include "Components/ParticleEmitting.h"
#include "Components/DiffuseMapped.h"
#include "Components/Bounded.h"
#include "Components/Spatialized.h"
#include "Components/TextureTransformed.h"
#include "Components/FurMapped.h"

#include "FurRenderer.h"

extern BaseApp* gApp;

FurRenderer::FurRenderer(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
{    
    // Create effect class
    mFurRendererEffect = std::make_shared<FurRendererEffect>(renderer);

	ZeroMemory(&mBlendDesc, sizeof(mBlendDesc));
	mBlendDesc.AlphaToCoverageEnable = false;
	mBlendDesc.IndependentBlendEnable = false;
	mBlendDesc.RenderTarget[0].BlendEnable = true;
	mBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	mBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	mBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	mBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	mFogSettings.fogType = -1;
}

/*virtual*/ 
FurRenderer::~FurRenderer(void)
{
   
}

/*virtual*/ void
FurRenderer::MadeActive(void)
{    
    
}

/*virtual*/ void
FurRenderer::MadeInactive(void)
{   
	
}

/*virtual*/ void
FurRenderer::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime)
{

	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Fur Render Pass", 0);

	renderer->SetBackBufferRenderTarget();

	dev_c->OMSetBlendState(renderer->StateMngr()->GetOrCreateBlendState(renderer->Device(), mBlendDesc), NULL, 0xffffffff);

	// Set depth stencil state	
	dev_c->OMSetDepthStencilState(NULL, NULL);

	// Get lights to fetch needed vars for lighting
	std::list< std::pair<LightType, Light*> >  lights = renderer->LightingSystm()->Lights();
	std::list< std::pair<LightType, Light*> >::iterator light_iter = lights.begin();
	std::vector<LightToGpu> lights_to_gpu;
	for (; light_iter != lights.end(); light_iter++)
	{
		LightToGpu light;

		light.lightType = light_iter->first;
		switch (light_iter->first)
		{
		case DIRECTIONAL_LIGHT:
			light.directionalLight = *(static_cast<DirectionalLight*>(light_iter->second));
			break;

		case POINT_LIGHT:
			light.pointLight = *(static_cast<PointLight*>(light_iter->second));
			break;

		case SPOT_LIGHT:
			light.spotLight = *(static_cast<SpotLight*>(light_iter->second));
			break;
		}

		lights_to_gpu.push_back(light);
	}
	mFurRendererEffect->UpdateLightArrayVariable(lights_to_gpu);
	mFurRendererEffect->UpdateNumLightsVariable((unsigned int)lights_to_gpu.size());


	// Update eye pos
	DirectX::XMFLOAT4 cam_pos = renderer->CurrentCamera()->Position();
	DirectX::XMFLOAT3 eye_pos = DirectX::XMFLOAT3(cam_pos.x, cam_pos.y, cam_pos.z);
	mFurRendererEffect->UpdateWorldEyePosVariable(eye_pos);

	std::shared_ptr<Camera> camera = renderer->CurrentCamera();
	DirectX::XMMATRIX view = XMLoadFloat4x4(&(camera->ViewMatrix()));

	// Update View Projection matrix variable
	DirectX::XMMATRIX proj = XMLoadFloat4x4(&(camera->ProjMatrix()));
	DirectX::XMMATRIX vp = view * proj;
	mFurRendererEffect->UpdateViewProjectionMatrixVariable(vp);

	// Set fog
	mFurRendererEffect->UpdateFogVariable(mFogSettings);

	mFurRendererEffect->ApplyPerFrameConstantBuffer(dev_c);

	// Iterate through families
	std::set<unsigned int> renderable_types_to_process;
	renderable_types_to_process.insert(Renderable::FUR);	
	for (std::set<unsigned int>::iterator type_iter = renderable_types_to_process.begin();
		type_iter != renderable_types_to_process.end();
		++type_iter)
	{
		std::set<unsigned int>::const_iterator fam_iter = familyByRenderableType[*type_iter].begin();
		for (fam_iter; fam_iter != familyByRenderableType[*type_iter].end(); ++fam_iter)
		{

			// Get renderable component
			std::shared_ptr<IComponent> tmp_comp;

			// Check for renderable to draw
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), tmp_comp);
			if (tmp_comp)
			{
				Renderable* renderable = static_cast<Renderable*>(tmp_comp.get());

				std::shared_ptr<IComponent> tmp_comp;
				std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
				input_layout_desc = mFurRendererEffect->InputLayoutDescription(FUR_TECH);

				dev_c->IASetPrimitiveTopology(renderable->PrimitiveTopology());

				// Index buffer
				dev_c->IASetIndexBuffer(
					renderable->IndexBuffer().indexBuffer,
					renderable->IndexBuffer().indexBufferFormat,
					renderable->IndexBuffer().indexBufferOffset);

				// Go through needed subinputs and accumulate vertex buffers
				std::vector<ID3D11Buffer*> buffers;
				std::vector<unsigned int> strides;
				std::vector<unsigned int> offsets;

				unsigned int vert_count = 0;

				std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();
				for (sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++)
				{
					buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
					strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
					offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);

					vert_count += renderable->VertexBuffer(*sub_input_iter).vertexCount;
				}
				dev_c->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

				// Set renderable's material
				mFurRendererEffect->UpdateMaterialVariable(renderable->MaterialProperties());

				// Set raster state
				ID3D11RasterizerState* default_raster;
				if (renderable->BackCulled())
					default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::Default);
				else
					default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
				dev_c->RSSetState(default_raster);

				// Check for texture transformation
				DirectX::XMMATRIX tex_xform = DirectX::XMMatrixIdentity();
				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, TextureTransformed::GetGuid(), tmp_comp);
				if (tmp_comp)
					tex_xform = DirectX::XMLoadFloat4x4(&(static_cast<TextureTransformed*>(tmp_comp.get())->TextureTransformation()));
				mFurRendererEffect->UpdateTextureTransformMatrixVariable(tex_xform);

				// Check for diffuse map
				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, DiffuseMapped::GetGuid(), tmp_comp);
				if (tmp_comp)
				{
					DiffuseMapped* diff_map = static_cast<DiffuseMapped*>(tmp_comp.get());
					renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc());
					mFurRendererEffect->UpdateDiffuseMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc()));
					mFurRendererEffect->UpdateDiffuseMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), diff_map->DiffuseMapName(), true));
				}

				// Update world mat from Spatialized component
				DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), tmp_comp);
				if (tmp_comp)
				{
					Spatialized* spatial_comp = static_cast<Spatialized*>(tmp_comp.get());
					world = XMLoadFloat4x4(&spatial_comp->LocalTransformation());
				}
				mFurRendererEffect->UpdateWorldMatrixVariable(world);

				// Get the fur component
				FurMapped* fur_mapped = nullptr;
				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, FurMapped::GetGuid(), tmp_comp);
				if (!tmp_comp)
					continue;
				fur_mapped = static_cast<FurMapped*>(tmp_comp.get());
									
				mFurRendererEffect->UpdateFurLayerSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), fur_mapped->SamplerDesc()));

				
								
				// Draw
				for (unsigned int i = 0; i < fur_mapped->NumLayers(); i++)
				{
					auto fur_lngth = fur_mapped->FurLength() / fur_mapped->NumLayers()*(i + 1);
					mFurRendererEffect->UpdateFurLengthVariable(fur_lngth);
					mFurRendererEffect->UpdateFurLayerVariable(fur_mapped->LayerSrv(i));	
					mFurRendererEffect->UpdateLayerRelativePositionVariable(i / (float)fur_mapped->NumLayers());
					mFurRendererEffect->UpdateForceVariable(fur_mapped->Force());
					mFurRendererEffect->ApplyTechnique(FUR_TECH, dev_c);

					if (0 == renderable->IndexBuffer().indexCount)
						dev_c->Draw(vert_count, 0);
					else
						dev_c->DrawIndexed(renderable->IndexBuffer().indexCount, 0, 0);
				}
			}
		}
	}

	mFurRendererEffect->ClearTechnique(FUR_TECH, renderer->DeviceContext());
	mFurRendererEffect->UnbindAll(renderer->DeviceContext());

	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();
}

void 
FurRenderer::RecompileShaders()
{
	//if (mFurRendererEffect) 
	//{
	//	OutputDebugMsg("Recompiling Fur Renderer...\n");
	//	MadeInactive();
	//	mFurRendererEffect->Recompile();
	//	MadeActive();
	//}
}