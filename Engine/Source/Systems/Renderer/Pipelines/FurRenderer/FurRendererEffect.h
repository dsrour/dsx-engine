#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include <vector>
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class FurRendererEffect : public Effect
{
public:
    FurRendererEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~FurRendererEffect(void); 

	void
	UpdateLightArrayVariable(std::vector<LightToGpu> const& lights);

	void
	UpdateNumLightsVariable(unsigned int const& numLights);

	void
	UpdateWorldEyePosVariable(DirectX::XMFLOAT3& eyePos);

	void
	UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat);

	void
	UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp);

	void
	UpdateTextureTransformMatrixVariable(DirectX::XMMATRIX& mat);

	void
	UpdateMaterialVariable(Material const& mat);

	void
	UpdateDiffuseMapVariable(ID3D11ShaderResourceView* const diffuseMap);

	void
	UpdateDiffuseMapSamplerVariable(ID3D11SamplerState* const diffuseSampler);

	void
	UpdateFogVariable(Fog const& fog);
	
	void
	UpdateFurLayerSamplerVariable(ID3D11SamplerState* const sampler);

	void
	UpdateFurLayerVariable(ID3D11ShaderResourceView* const texture);	

	void
	UpdateFurLengthVariable(float const furLength);
	
	void
	UpdateForceVariable(DirectX::XMFLOAT3& force);

	/// 0.f to 1.f
	void
	UpdateLayerRelativePositionVariable(float const relPos);

	// This is in its own function since it only occurs once per frame
	// No need to remap the CB per ApplyTechnique(...) calls
	void
	ApplyPerFrameConstantBuffer(ID3D11DeviceContext* const deviceContext);

	// This function unbinds all resources from the rasterizer
	void
	UnbindAll(ID3D11DeviceContext* const deviceContext);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

                
private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);

	ID3DBlob* mVsBlob;
	ID3D11VertexShader* mVS;
	ID3DBlob* mPsBlob;
	ID3D11PixelShader* mPS;

	struct CbPerFrame
	{
		LightToGpu lights[MAX_LIGHTS];
		unsigned int numLights;
		DirectX::XMFLOAT3 eyePos;
		Fog fog;
	} mPerFrameVariables;
	DirectX::ConstantBuffer< CbPerFrame > mPerFrameCb;

	struct CbPerObject
	{
		DirectX::XMMATRIX worldMat;
		DirectX::XMMATRIX vpMat;
		DirectX::XMMATRIX texMat;
		Material material;
		DirectX::XMFLOAT3 force;
		float layerRelativePositionToStrand;
		float furLength;
		DirectX::XMFLOAT3 pad;
	} mPerObjectVariables;
	DirectX::ConstantBuffer< CbPerObject > mPerObjectCb;

	ID3D11SamplerState*          mDiffuseMapSampler;
	ID3D11ShaderResourceView*    mDiffuseMap;
	ID3D11SamplerState*          mLayerMapSampler;
	ID3D11ShaderResourceView*    mLayerMap;

	// Since textures are expensive to submit to the gpu, keep track of what's already bound
	ID3D11ShaderResourceView*    mBoundedDiffuseMap;
	ID3D11ShaderResourceView*    mBoundedLayerMap;

	D3dRenderer* mRendererRef;
};