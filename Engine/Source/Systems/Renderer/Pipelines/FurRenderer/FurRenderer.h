/**  ParticleRenderer.h
 *
 *   Forward Rendering pipeline.
 */

#pragma once

#include <set>
#include <map>
#include "Systems/Renderer/Pipeline.h"
#include "FurRendererEffect.h"

class FurRenderer : public Pipeline
{
public:
    FurRenderer(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~FurRenderer(void);    
          
    virtual void
    MadeActive(void);

    virtual void
    MadeInactive(void);

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime); 
	
	virtual void
	RecompileShaders();
        
	Fog&
	FogSettings() { return mFogSettings; }

private:
	// Effect
	std::shared_ptr<FurRendererEffect> mFurRendererEffect;  
		
	D3D11_BLEND_DESC mBlendDesc;	
	Fog mFogSettings;
};