 #include <assert.h>
 #include <d3dcompiler.h>
 #include "Debug/Debug.h"
 #include "SkyRendererEffect.h"
 
  extern std::wstring gEngineRootDir;
  
  SkyRendererEffect::SkyRendererEffect(std::shared_ptr<D3dRenderer> renderer)
  {
	  mVsBlob = nullptr;
	  mPsBlob = nullptr;

	  mVS = nullptr;
	  mPS = nullptr;

	  mCubeMapSampler = nullptr;

	  mPerFrameCb.Create(renderer->Device());

	  ZeroMemory(&mPerFrameVariables, sizeof(CbPerFrame));

	  mRendererRef = renderer.get();
	  Init(mRendererRef);
  }
  
  /*virtual*/
  SkyRendererEffect::~SkyRendererEffect()
  {    
   		if (mVsBlob)
   			mVsBlob->Release();
   		if (mPsBlob)
   			mPsBlob->Release();
   
   		if (mVS)
   			mVS->Release();
   		if (mPS)
   			mPS->Release();
  }
  
  void 
  SkyRendererEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
  {
	  std::wstring cso_name;
	  HRESULT hr;

	  cso_name = std::wstring(SKY_VS_FILE) + L".cso";
	  hr = D3DReadFileToBlob(cso_name.c_str(), &mVsBlob);
	  assert(SUCCEEDED(hr));
	  hr = renderer->Device()->CreateVertexShader(
		  mVsBlob->GetBufferPointer(),
		  mVsBlob->GetBufferSize(),
		  NULL,
		  &mVS);
	  assert(SUCCEEDED(hr));

	  cso_name = std::wstring(SKY_PS_FILE) + L".cso";
	  hr = D3DReadFileToBlob(cso_name.c_str(), &mPsBlob);
	  assert(SUCCEEDED(hr));
	  hr = renderer->Device()->CreatePixelShader(
		  mPsBlob->GetBufferPointer(),
		  mPsBlob->GetBufferSize(),
		  NULL,
		  &mPS);
	  assert(SUCCEEDED(hr));

	  {
		   // Create input layout descs
		  std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

		  input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
		  mInputLayoutDescs[SKY_TECH] = input_layout_desc;
		  mInputLayouts[SKY_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
	  }
  }
  
  void
  SkyRendererEffect::UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat)
  {
  		mPerFrameVariables.worldMat = DirectX::XMMatrixTranspose(worldMat);
  }
  
  void
  SkyRendererEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp)
  {
  		mPerFrameVariables.vpMat = DirectX::XMMatrixTranspose(vp);
  }
  
  void
  SkyRendererEffect::UpdateCubeMapVariable(ID3D11ShaderResourceView* const cubeMap)
  {
		mCubeMap = cubeMap;
  }
  
  void
  SkyRendererEffect::UpdateCubeMapSamplerVariable(ID3D11SamplerState* const cubeSampler)
  {
		mCubeMapSampler = cubeSampler;
  }
  
  void 
  SkyRendererEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
  {
	  if (SKY_TECH == techName)
	  {
		  ID3D11SamplerState* ns = mRendererRef->StateMngr()->DefaultSamplerState();

		  mPerFrameCb.SetData(deviceContext, mPerFrameVariables);

		  auto cb = mPerFrameCb.GetBuffer();
		  deviceContext->VSSetConstantBuffers(0, 1, &cb);
		  deviceContext->VSSetShader(mVS, nullptr, 0);

		  deviceContext->PSSetShaderResources(2, 1, &mCubeMap);
		  if (mCubeMapSampler)
			 deviceContext->PSSetSamplers(2, 1, &mCubeMapSampler);
		  else
			  deviceContext->PSSetSamplers(2, 1, &ns);
		  deviceContext->PSSetShader(mPS, nullptr, 0);

	  }
  }
  
  void 
  SkyRendererEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
  {
   		ID3D11Buffer* nb = nullptr;
   		ID3D11ShaderResourceView* nsrv = nullptr;
   		ID3D11SamplerState* ns = nullptr;
   
   		if (SKY_TECH == techName)
   		{
   			deviceContext->VSSetConstantBuffers(0, 1, &nb);
			deviceContext->VSSetShader(nullptr, nullptr, 0);
   
   			deviceContext->PSSetShaderResources(2, 1, &nsrv);
   			deviceContext->PSSetSamplers(2, 1, &ns);
   			deviceContext->PSSetShader(nullptr, nullptr, 0);
   		}
  }
