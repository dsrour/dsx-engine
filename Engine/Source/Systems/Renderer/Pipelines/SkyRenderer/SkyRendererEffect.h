#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class SkyRendererEffect : public Effect
{
public:
   SkyRendererEffect(std::shared_ptr<D3dRenderer> renderer);       
   
   virtual 
   ~SkyRendererEffect(void); 
	
	virtual void
	Recompile() {}

	// Shader vars          
	void
	UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat);
   
	void
	UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp);    
  
	void
	UpdateCubeMapVariable(ID3D11ShaderResourceView* const cubeMap);    

	void
	UpdateCubeMapSamplerVariable(ID3D11SamplerState* const cubeSampler);   

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);
	                
private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);

	struct CbPerFrame 
	{
		DirectX::XMMATRIX worldMat;
		DirectX::XMMATRIX vpMat;
	} mPerFrameVariables;

	ID3DBlob* mVsBlob;
	ID3D11VertexShader* mVS;
	ID3DBlob* mPsBlob;
	ID3D11PixelShader* mPS;
	DirectX::ConstantBuffer< CbPerFrame > mPerFrameCb;

    ID3D11SamplerState*          mCubeMapSampler;
	ID3D11ShaderResourceView*    mCubeMap;

	D3dRenderer* mRendererRef; /// Kept for recompile() func
};