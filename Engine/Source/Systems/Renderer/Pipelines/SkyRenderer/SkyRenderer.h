 /**  SkyRenderer.h
  *
  *   Renders sky effect.
  */
 
 #pragma once
 
 #include "Systems/Renderer/Pipeline.h"
 #include "SkyRendererEffect.h"
 
 class SkyRenderer : public Pipeline
 {
 public:
     SkyRenderer(std::shared_ptr<D3dRenderer> renderer);       
     
     virtual 
     ~SkyRenderer(void);    
           
     virtual void
     MadeActive(void);
 
     virtual void
     MadeInactive(void);
 
     virtual void
     EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime); 
 
 	virtual void
 	RecompileShaders();
         
 private:   
 	void
 	ResetSRVs();
 
 	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
 
    
    // Effect
    std::shared_ptr<SkyRendererEffect> mSkyRendererEffect;   
 };