 /**  ForwardRenderer.cpp
  *
  */
 
 #include <assert.h>
 #include <vector>
 #include "Debug/Debug.h"
 #include "Systems/Renderer/Renderer.h"
 #include "Systems/Renderer/RenderStateManager.h"
 #include "Systems/Renderer/Camera.h"
 #include "Systems/Renderer/MathHelper.h"
 #include "BaseApp.h"
 #include "Systems/Renderer/Shaders/ShaderDefines.h"
 
 // Needed components
 #include "Components/Renderable.h"
 #include "Components/DiffuseMapped.h"
 #include "Components/Spatialized.h"
 
 #include "SkyRenderer.h"
 
 extern BaseApp* gApp;
 
 SkyRenderer::SkyRenderer(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
 {    
     // Create effect class
     mSkyRendererEffect = std::make_shared<SkyRendererEffect>(renderer);		
 
 	mDepthStencilDesc.DepthEnable = false;
 	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
 	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
 	mDepthStencilDesc.StencilEnable = true;
 	mDepthStencilDesc.StencilReadMask = 0xFF;
 	mDepthStencilDesc.StencilWriteMask = 0xFF;
 	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
 	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
 	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
 	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
 	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
 	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
 	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
 	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
 }
 
 /*virtual*/ 
 SkyRenderer::~SkyRenderer(void)
 {    
         
 }
 
 /*virtual*/ void
 SkyRenderer::MadeActive(void)
 {    
     
 }
 
 /*virtual*/ void
 SkyRenderer::MadeInactive(void)
 {   
 
 }
 
 /*virtual*/ void
 SkyRenderer::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >&  familyByRenderableType, double const /*currentTime*/)
 {
     std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
     if (!renderer)
         return;
 
 	renderer->SetBackBufferRenderTarget();

	auto dev_c = renderer->DeviceContext();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Sky Render Pass", 0);
 	
 	// Set raster state
 	ID3D11RasterizerState* raster_state = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
	dev_c->RSSetState(raster_state);
 
 	// Set default blend state
 	dev_c->OMSetBlendState(NULL, NULL, 0xffffffff);
 
 	// Set depth stencil state
 	dev_c->OMSetDepthStencilState(renderer->StateMngr()->GetOrCreateDepthStencilState(renderer->Device(), mDepthStencilDesc), 1);
 
 	std::shared_ptr<Camera> camera = renderer->CurrentCamera();
 
 	ResetSRVs();
 	
 	// Update View Projection matrix variable
 	DirectX::XMMATRIX view =  XMLoadFloat4x4(&(camera->ViewMatrix()));
 	DirectX::XMMATRIX proj =  XMLoadFloat4x4(&(camera->ProjMatrix()));
 	DirectX::XMMATRIX vp =  view * proj;
 	mSkyRendererEffect->UpdateViewProjectionMatrixVariable(vp); 
 
 	// Update world mat and make it equal to the eye pos
 	// This insures that the center of the env map is always where the camera is
 	DirectX::XMFLOAT4 eye_pos = camera->Position();
 	DirectX::XMMATRIX world = DirectX::XMMatrixTranslation(eye_pos.x, eye_pos.y, eye_pos.z);		
 	mSkyRendererEffect->UpdateWorldMatrixVariable(world);
                
     // Iterate through families
     std::set<unsigned int>::const_iterator fam_iter = familyByRenderableType[Renderable::SKY].begin();
     for (fam_iter; fam_iter!= familyByRenderableType[Renderable::SKY].end(); fam_iter++)
     {
         // Get renderable component
         std::shared_ptr<IComponent> tmp_comp;			
 				
         // Check for renderable to draw
         gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), tmp_comp);                
         if (tmp_comp)
         {
             Renderable* renderable = static_cast<Renderable*>(tmp_comp.get());	
 						
 			{
 				std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
 								
 				dev_c->IASetInputLayout( mSkyRendererEffect->InputLayout(SKY_TECH) );
 				input_layout_desc = mSkyRendererEffect->InputLayoutDescription(SKY_TECH);
 				assert( !input_layout_desc.empty() );
 
 				dev_c->IASetPrimitiveTopology(renderable->PrimitiveTopology());
 
 				// Index buffer
 				dev_c->IASetIndexBuffer(renderable->IndexBuffer().indexBuffer, 
 															renderable->IndexBuffer().indexBufferFormat, 
 															renderable->IndexBuffer().indexBufferOffset );
 
 				// Go through needed subinputs and accumulate vertex buffers
 				std::vector<ID3D11Buffer*> buffers;
 				std::vector<unsigned int> strides;
 				std::vector<unsigned int> offsets;
 
 				std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();            
 				for ( sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++ )
 				{
 					buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
 					strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
 					offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);                
 				}
 				dev_c->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);
 
 
 				// Get cube map
 				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, DiffuseMapped::GetGuid(), tmp_comp);     
 				if (tmp_comp)
 				{
 					DiffuseMapped* diff_map = static_cast<DiffuseMapped*>(tmp_comp.get());
 					renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc());
 					mSkyRendererEffect->UpdateCubeMapSamplerVariable( renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc()));            
 					mSkyRendererEffect->UpdateCubeMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), diff_map->DiffuseMapName(), true));					
 				}    
   				
 				mSkyRendererEffect->ApplyTechnique(SKY_TECH, dev_c);
 				dev_c->DrawIndexed(renderable->IndexBuffer().indexCount, 0, 0);
				mSkyRendererEffect->ClearTechnique(SKY_TECH, dev_c);
 			}
         }
     }    

	 if (dev_c->IsAnnotationEnabled())
		 dev_c->EndEvent();
 }
 
 void 
 SkyRenderer::RecompileShaders()
 {
 	//if (mSkyRendererEffect)
 	//{
 	//	OutputDebugMsg("Recompiling Sky Renderer...\n");
 	//	MadeInactive();
 	//	mSkyRendererEffect->Recompile();
 	//	MadeActive();
 	//}
 }
 
 void 
 SkyRenderer::ResetSRVs()
 {
 	if (mSkyRendererEffect)
 		mSkyRendererEffect->UpdateCubeMapVariable(nullptr);
 }
