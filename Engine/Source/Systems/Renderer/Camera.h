/**  Camera.h
 *
 * Holds base camera class.
 */

#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>

class Camera
{
public:
    Camera(void)
    {
       mPos = DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);       
    }

    virtual 
    ~Camera(void)
    {
        
    }

	DirectX::BoundingFrustum const&
    CameraFrustum(void) const;

    DirectX::XMFLOAT4X4 const&
    ViewMatrix(void) const;
    
    DirectX::XMFLOAT4X4 const&
    ProjMatrix(void) const;   

	DirectX::XMFLOAT4X4 const&
	ViewProjMatrix(void) const;
    
    DirectX::XMFLOAT4 const&
    Position(void) const; 

    float const
    NearZ(void) const { return mNearZ; }

    float const
    FarZ(void) const { return mFarZ; }

   /** Sets projection matrix.
    * 
    * @param View width.
    * @param View height.
    * @param Near plane.
    * @param Far plane.
    * @param True, then projection is perspective, otherwise orthogonal.
    */
    void
    ProjMatrix(float const fov, float const viewWidth, float const viewHeight, float const nearZ, float const farZ, bool persp = true);      

	/** Sets orthogonal projection matrix.
	 *
	 * @param left slope
	 * @param right slope 
	 * @param down slope
	 * @param up slope
	 * @param Near plane.
	 * @param Far plane.
	 * @param True, then projection is perspective, otherwise orthogonal.
	 */
	void
	OrthogonalProjMatrix(float const left, 
						 float const right, 
						 float const down, 
						 float const up, 
						 float const nearZ,
						 float const farZ);

    virtual void
    Update(void) = 0;    
      
protected:
    void
    ViewMatrix(DirectX::FXMVECTOR const& pos, DirectX::FXMVECTOR const& lookTo, DirectX::FXMVECTOR const& lookUp);
   
    DirectX::XMFLOAT4X4 mView;
    DirectX::XMFLOAT4X4 mProj;
	DirectX::XMFLOAT4X4 mViewProj;
    
    DirectX::BoundingFrustum mFrustum; 

    DirectX::XMFLOAT4   mPos;

    float mNearZ, mFarZ;
};

inline DirectX::BoundingFrustum const&
Camera::CameraFrustum(void) const
{
    return mFrustum;
}

inline DirectX::XMFLOAT4X4 const& 
Camera::ViewMatrix(void) const
{
    return mView;
}

inline DirectX::XMFLOAT4X4 const& 
Camera::ProjMatrix(void) const
{
    return mProj;
}

inline DirectX::XMFLOAT4X4 const&
Camera::ViewProjMatrix(void) const
{
	return mViewProj;
}


inline void
Camera::ViewMatrix(DirectX::FXMVECTOR const& pos, DirectX::FXMVECTOR const& lookTo, DirectX::FXMVECTOR const& lookUp)
{	
	DirectX::XMStoreFloat4(&mPos, pos);
	auto view = DirectX::XMMatrixLookToLH(pos, lookTo, lookUp);
	DirectX::XMStoreFloat4x4(&mView, view);
	DirectX::XMStoreFloat4x4(&mViewProj, DirectX::XMMatrixMultiply(view, DirectX::XMLoadFloat4x4(&mProj)));
}

inline void
Camera::ProjMatrix(float const fov, float const viewWidth, float const viewHeight, float const nearZ, float const farZ, bool persp /* = true */)
{
    if (persp)
	{
        DirectX::XMStoreFloat4x4( &mProj, DirectX::XMMatrixPerspectiveFovLH( fov, viewWidth / (float)viewHeight, nearZ, farZ ) );
		DirectX::BoundingFrustum::CreateFromMatrix(mFrustum, DirectX::XMLoadFloat4x4(&mProj));
	}
    else
	{
        DirectX::XMStoreFloat4x4( &mProj, DirectX::XMMatrixOrthographicLH( viewWidth, viewHeight, nearZ, farZ ) );
		mFrustum.LeftSlope = 0.f - (viewWidth / 2.f);
		mFrustum.RightSlope = 0.f + (viewWidth / 2.f);
		mFrustum.BottomSlope = 0.f - (viewHeight / 2.f);
		mFrustum.TopSlope = 0.f + (viewHeight / 2.f);
		mFrustum.Near = nearZ;
		mFrustum.Far = farZ;
	}

    mNearZ = nearZ;
    mFarZ = farZ;
}

inline void
Camera::OrthogonalProjMatrix(
	float const left,
	float const right,
	float const down,
	float const up,
	float const nearZ,
	float const farZ)
{	
	DirectX::XMStoreFloat4x4(&mProj, DirectX::XMMatrixOrthographicOffCenterLH(left, right, down, up, nearZ, farZ));
	mFrustum.LeftSlope = left;
	mFrustum.RightSlope = right;
	mFrustum.BottomSlope = down;
	mFrustum.TopSlope = up;
	mFrustum.Near = nearZ;
	mFrustum.Far = farZ;
	
	mNearZ = nearZ;
	mFarZ = farZ;
}

inline DirectX::XMFLOAT4 const&
Camera::Position(void) const
{
    return mPos;
}