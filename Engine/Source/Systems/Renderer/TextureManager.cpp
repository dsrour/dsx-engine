/**  TextureManager.cpp
 *
 */

#include <assert.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/DirectXTK/Inc/DDSTextureLoader.h"
#include "TextureManager.h"

TextureManager::~TextureManager(void)
{
    std::vector< ID3D11ShaderResourceView* >::iterator iter;
    for (iter = mSrvs.begin(); iter != mSrvs.end(); iter++)
    {
        (*iter)->Release();
    }    
		
	for (iter = mSrgbSrvs.begin(); iter != mSrgbSrvs.end(); iter++)
	{
		(*iter)->Release();
	}    
}

/// Can only open DDS.
ID3D11ShaderResourceView* const
TextureManager::GetOrCreateTexture2dSrv(ID3D11Device* const d3dDevice, std::string const& file, bool const& srgb)
{    
	if (!srgb)
	{
		std::map< std::string, unsigned int >::iterator iter = mTexture2dSrvId.find(file);
		if (iter != mTexture2dSrvId.end())
			return mSrvs[iter->second];
	}
	else
	{
		std::map< std::string, unsigned int >::iterator iter = mTexture2dSrvSrgbId.find(file);
		if (iter != mTexture2dSrvSrgbId.end())
			return mSrgbSrvs[iter->second];
	}
 
    ID3D11ShaderResourceView* srv;
 
    std::wstring w_str_file = std::wstring(file.begin(), file.end());

	if (!srgb)
	{
		HRESULT hr = DirectX::CreateDDSTextureFromFile(d3dDevice, w_str_file.c_str(), nullptr, &srv);
		if ( FAILED(hr) )
			OutputDebugMsg("Could not open texture file.\n");
		assert(SUCCEEDED(hr));

		mTexture2dSrvId[file] = (unsigned int)mSrvs.size();
		mSrvs.push_back(srv);
	}
	else
	{
		HRESULT hr = DirectX::CreateDDSTextureFromFileEx(d3dDevice, w_str_file.c_str(), 0, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, true, nullptr, &srv );
		if ( FAILED(hr) )
			OutputDebugMsg("Could not open texture file.\n");
		assert(SUCCEEDED(hr));

		mTexture2dSrvSrgbId[file] = (unsigned int)mSrgbSrvs.size();
		mSrgbSrvs.push_back(srv);
	}

    return srv;
}