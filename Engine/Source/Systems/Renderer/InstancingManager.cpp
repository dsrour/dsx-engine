/**  TextureManager.cpp
 *
 */

#include <assert.h>
#include "BaseApp.h"
#include "Components/Spatialized.h"
#include "Debug/Debug.h"
#include "InstancingManager.h"

extern BaseApp* gApp;

InstancingManager::~InstancingManager(void)
{
	{
		std::map<unsigned int, ID3D11Buffer*>::iterator iter = mBatches.instancedBuffer.begin();
		for (; iter != mBatches.instancedBuffer.end(); ++iter)
			iter->second->Release();
	}

	mBatches.instancedBuffer.clear();
	mBatches.batchRenderableRef.clear();
	mBatches.bufferSize.clear();
	mBatches.bufferCapacity.clear();	
}

void 
InstancingManager::FreeBatchBuffer( unsigned int const& bufferId )
{
	if (mBatches.instancedBuffer.find(bufferId) == mBatches.instancedBuffer.end())
		return;

	mBatches.instancedBuffer[bufferId]->Release();
	mBatches.instancedBuffer.erase(bufferId);
	mBatches.batchRenderableRef.erase(bufferId);
	mBatches.bufferSize.erase(bufferId);
	mBatches.bufferCapacity.erase(bufferId);
	mBatches.batchDiffuseMapRef.erase(bufferId);
	mBatches.batchNormalMapRef.erase(bufferId);
	mDynamicBatches.erase(bufferId);
}

void 
InstancingManager::CreateDynamicBatchBuffer(	ID3D11Device* device, unsigned int const& batchId, 
												unsigned int const& capacity, std::shared_ptr<Renderable>& renderable, 
												std::shared_ptr<DiffuseMapped>& diffuse, std::shared_ptr<NormalMapped>& normal, std::shared_ptr<ReflectionMapped>& reflection)
{
	if (mBatches.batchRenderableRef.find(batchId) != mBatches.batchRenderableRef.end())
	{
		OutputDebugMsg("InstancingManager::CreateBatchBuffer(...): An instanced batch id " + to_string(batchId) + " was already created!\n");
		return;
	}
			
	mBatches.batchRenderableRef[batchId] = renderable;
	mBatches.batchDiffuseMapRef[batchId] = diffuse;
	mBatches.batchNormalMapRef[batchId] = normal;
	mBatches.batchReflectionMapRef[batchId] = reflection;
	mBatches.bufferSize[batchId] = 0;
	mBatches.bufferCapacity[batchId] = capacity;
	mDynamicBatches.insert(batchId);

	// Create the instanced buffer
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.ByteWidth = sizeof(InstancedData) * capacity;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	mBatches.instancedBuffer[batchId] = NULL;
	HRESULT hr = device->CreateBuffer(&vbd, 0, &mBatches.instancedBuffer[batchId]);
	assert(SUCCEEDED(hr));
}

void 
InstancingManager::AddEntityToDynamicBatch( unsigned int const& entityId )
{
	Renderable* renderable = NULL;
	Spatialized* spatialized = NULL;

	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Renderable::GetGuid(), tmp);
	if (tmp)
		renderable = static_cast<Renderable*>(tmp.get());
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Spatialized::GetGuid(), tmp);
	if (tmp)
		spatialized = static_cast<Spatialized*>(tmp.get());

	if (mBatches.batchRenderableRef.find(renderable->DynamicInstancedBatchId()) == mBatches.batchRenderableRef.end())
	{
		OutputDebugMsg("InstancingManager::AddRenderableToBatch(...): Instanced batch id " + to_string(renderable->DynamicInstancedBatchId()) + " was never created.\n");
		return;
	}

	if (mDynamicBatches.find(renderable->DynamicInstancedBatchId()) == mDynamicBatches.end())
	{
		OutputDebugMsg("InstancingManager::AddRenderableToBatch(...): Instanced batch id " + to_string(renderable->DynamicInstancedBatchId()) + " is static, can't modify it's data!\n");
		return;
	}

	if (!spatialized)
	{
		OutputDebugMsg("InstancingManager::AddRenderableToBatch(...): Entity marked as renderable instance but missing spatialized component.\n");
		return;
	}

	assert (NULL != renderable);

	// Reset count?  TODO:: Is this really needed.
	if (mBatchInstancedData[renderable->DynamicInstancedBatchId()].empty())
		mBatches.bufferSize[renderable->DynamicInstancedBatchId()] = 0;

	if (mBatchInstancedData[renderable->DynamicInstancedBatchId()].size() >= mBatches.bufferCapacity[renderable->DynamicInstancedBatchId()])
	{
		OutputDebugMsg("InstancingManager::AddRenderableToBatch(...): Instanced batch id " + to_string(renderable->DynamicInstancedBatchId()) + " has filled its capacity of " + to_string(mBatches.bufferCapacity[renderable->DynamicInstancedBatchId()]) + ".\n");
		return;
	}

	// Fetch color and transform matrix
	InstancedData data;
	data.color = renderable->InstancedColor();
	auto world = DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&spatialized->LocalTransformation()));
	DirectX::XMStoreFloat4x4(&data.world, world);
	mBatchInstancedData[renderable->DynamicInstancedBatchId()].push_back(data);
		
	mBatches.bufferSize[renderable->DynamicInstancedBatchId()]++;
}

void 
InstancingManager::Update(ID3D11DeviceContext* d3dContext)
{	
	static std::set<unsigned int> buffers_needing_clearing;
	std::set<unsigned int> buffers_used;
		
	std::set< unsigned int >::iterator iter = mDynamicBatches.begin();
		
	for (; iter != mDynamicBatches.end(); ++iter)
	{
		// First we update the batch buffers
		D3D11_MAPPED_SUBRESOURCE mapped_data; 
		d3dContext->Map(mBatches.instancedBuffer[*iter], 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_data);

		InstancedData* data_view = reinterpret_cast<InstancedData*>(mapped_data.pData);

		for (unsigned int i = 0; i < mBatchInstancedData[*iter].size(); i++)
		{
			data_view[i] = mBatchInstancedData[*iter][i];
		}

		d3dContext->Unmap(mBatches.instancedBuffer[*iter], 0);

		buffers_needing_clearing.insert(*iter);	
		buffers_used.insert(*iter);
	}

	// Unmap buffers that aren't used anymore
	std::set<unsigned int>::iterator to_clear_iter = buffers_needing_clearing.begin();
	while (to_clear_iter != buffers_needing_clearing.end())
	{
		if ( buffers_used.find(*to_clear_iter) == buffers_used.end() )
		{
			mBatchInstancedData.erase(*to_clear_iter);
			mBatches.bufferSize.clear();
			buffers_needing_clearing.erase(to_clear_iter++);
		}
		else
			++to_clear_iter;		
	}
	
	// Clear for next frame
	mBatchInstancedData.clear();
}

void 
InstancingManager::CreateStaticBatchBuffer( ID3D11Device* device, unsigned int const& batchId, std::shared_ptr<Renderable>& renderable, 
											std::shared_ptr<DiffuseMapped>& diffuse, 
											std::shared_ptr<NormalMapped>& normal, 
											std::vector<InstancedData>& instancedData,
											std::shared_ptr<ReflectionMapped>& reflection)
{
	if (mBatches.batchRenderableRef.find(batchId) != mBatches.batchRenderableRef.end())
	{
		OutputDebugMsg("InstancingManager::CreateBatchBuffer(...): An instanced batch id " + to_string(batchId) + " was already created!\n");
		return;
	}

	mBatches.batchRenderableRef[batchId] = renderable;
	mBatches.batchDiffuseMapRef[batchId] = diffuse;
	mBatches.batchNormalMapRef[batchId] = normal;
	mBatches.batchReflectionMapRef[batchId] = reflection;
	mBatches.bufferSize[batchId] = (unsigned int)instancedData.size();
	mBatches.bufferCapacity[batchId] = (unsigned int)instancedData.size();

	// Create the instanced buffer
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = (unsigned int)(sizeof(InstancedData) * instancedData.size());
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA init_data;
	init_data.pSysMem = &instancedData[0];
	init_data.SysMemPitch = 0;
	init_data.SysMemSlicePitch = 0;


	mBatches.instancedBuffer[batchId] = NULL;
	HRESULT hr = device->CreateBuffer(&vbd, &init_data, &mBatches.instancedBuffer[batchId]);
	assert(SUCCEEDED(hr));
}
