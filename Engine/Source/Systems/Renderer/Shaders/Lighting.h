// LIGHTING MODELS
#define PHONG			0
#define COOK_TORRANCE	1

#define DIRECTIONAL_LIGHT	0
#define POINT_LIGHT			1
#define SPOT_LIGHT			2

struct Material
{
	float4 ambient;
	float4 diffuse;
	float4 specular; // w = spec brightness (Phong), roughness (Cook-Torrance). [0, 1]
	float4 reFlect;	
	float  reflectionCoeff; // for Fresnel term in Cook-Torrance (reflection coefficient @ normal incidence)
	uint   lightingModel;
	float2 pad;
};

struct DirectionalLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 direction;
	float  lightSize; // used for soft shadows
};

struct PointLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	
	float3 position;
	float  range;

	float3 attenuation;
	float  pad;
};

struct SpotLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	
	float3 position;
	float  range;

	float3 direction;
	float  spot;

	float3 attenuation;
	float  pad;
};

struct Light
{
	uint	lightType;

	float2				pad;
	int					emitsShadows;
	float4x4			shadowMatrix;
	float4				cascadeSplits;
	float4				cascadeOffsets[NUM_SHADOW_CASCADES];
	float4				cascadeScales[NUM_SHADOW_CASCADES];

	// Shadow mapping biases
	float		staticOffsetBias;
	float		normalOffsetScaleBias;
	int			usePlaneDepthBias;
	float		shadowIntensity;

	DirectionalLight	directionalLight;
	PointLight			pointLight;
	SpotLight			spotLight;
};


float
ComputePhongSpecularTerm(float3 lightVec, float3 normal, float3 toEye, float specPowerTerm)
{
	float3 reflection = reflect(-lightVec, normal);

	// exp2(mat.specular.w*12.0f) is like so because mat.specular.w is expected to be between 0-1.  
	// This is to keep it the same as the mat.specular.xyz color components which are also to be in the 0-1 range.
	return pow(max(dot(reflection, toEye), 0.0f), exp2(specPowerTerm*12.0f));
}

float
ComputeCookTorranceSpecularTerm(float3 lightVec, float3 normal, float3 toEye, float roughness, float reflectionCoeff)
{
	/* Based off http://content.gpwiki.org/index.php/D3DBook:(Lighting)_Cook-Torrance & Prof. Engel's lecture on Physically Based Lighting.
	 * All of which is based of Cook/Torrance, Schlick, and Beckmann's work.
	 */
	float EPSILON = 0.00001f;

	float3 half_vec = normalize(lightVec + toEye);
	float NdotL = saturate(dot(normal, lightVec));
	float NdotH = saturate( dot(normal, half_vec) );
	float NdotV = saturate(dot(normal, toEye));
	float VdotH = saturate(dot(toEye, half_vec));
	float r_sq = max(roughness * roughness, EPSILON);

	// Geometric term
	float G = min( 1.f, min( (2.f * NdotH * NdotL) / VdotH, (2.f * NdotH*NdotV) / VdotH) );

	// Roughness term (Beckmann distribution)
	float roughness_a = 1.f / max( (4.f * r_sq * pow(NdotH, 4)), EPSILON);
	float NH2 = NdotH * NdotH;
	float roughness_b = NH2 - 1.f;
	float roughness_c = r_sq * NH2;
	float D = roughness_a * exp(roughness_b / roughness_c);

	// Fresnel term (Schlick)
	float F = reflectionCoeff + (1 - reflectionCoeff) * pow(1.f - NdotV, 5.f);

	return  (F * G * D) / max((NdotL * NdotV), EPSILON);
}

void
ComputeDirectionalLight( Material mat, DirectionalLight L, float3 normal, float3 toEye,
						 out float4 ambient,
						 out float4 diffuse,
						 out float4 spec)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 light_vec = -L.direction;

	ambient = mat.ambient * L.ambient;

	float diffuse_factor = dot(light_vec, normal);

	[flatten]
	if (diffuse_factor > 0.0f)
	{		
		float spec_factor = 0.f;
		if (0 == mat.lightingModel)
			spec_factor = ComputePhongSpecularTerm(light_vec, normal, toEye, mat.specular.w);
		else if (1 == mat.lightingModel)
			spec_factor = ComputeCookTorranceSpecularTerm(light_vec, normal, toEye, mat.specular.w, mat.reflectionCoeff);

		diffuse = diffuse_factor * mat.diffuse * L.diffuse;
		spec = spec_factor * mat.specular * L.specular;
	}
}

void
ComputePointLight( Material mat, PointLight L, float3 pos, float3 normal, float3 toEye,
				   out float4 ambient,
				   out float4 diffuse,
				   out float4 spec)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 light_vec = L.position - pos;

	float d = length(light_vec);

	if (d > L.range)
		return;

	light_vec /= d; // normalize like that since we already got the distance

	ambient = mat.ambient * L.ambient;

	float diffuse_factor = dot(light_vec, normal);

	[flatten]
	if (diffuse_factor > 0.0f)
	{
		float spec_factor = 0.f;
		if (0 == mat.lightingModel)
			spec_factor = ComputePhongSpecularTerm(light_vec, normal, toEye, mat.specular.w);
		else if (1 == mat.lightingModel)
			spec_factor = ComputeCookTorranceSpecularTerm(light_vec, normal, toEye, mat.specular.w, mat.reflectionCoeff);

		diffuse = diffuse_factor * mat.diffuse * L.diffuse;
		spec = spec_factor * mat.specular * L.specular;
	}

	// Attenuation
	float att = 1.0f / dot( L.attenuation, float3(1.0f, d, d*d) );
	diffuse *= att;
	spec    *= att;		
}

void
ComputeSpotLight( Material mat, SpotLight L, float3 pos, float3 normal, float3 toEye,
				   out float4 ambient,
				   out float4 diffuse,
				   out float4 spec)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 light_vec = L.position - pos;

	float d = length(light_vec);

	if (d > L.range)
		return;

	light_vec /= d; // normalize like that since we already got the distance

	ambient = mat.ambient * L.ambient;

	float diffuse_factor = dot(light_vec, normal);

	[flatten]
	if (diffuse_factor > 0.0f)
	{
		float spec_factor = 0.f;
		if (0 == mat.lightingModel)
			spec_factor = ComputePhongSpecularTerm(light_vec, normal, toEye, mat.specular.w);
		else if (1 == mat.lightingModel)
			spec_factor = ComputeCookTorranceSpecularTerm(light_vec, normal, toEye, mat.specular.w, mat.reflectionCoeff);

		diffuse = diffuse_factor * mat.diffuse * L.diffuse;
		spec = spec_factor * mat.specular * L.specular;
	}

	float spot = pow(max(dot(-light_vec, L.direction), 0.0f), L.spot);
		
	// Attenuation
	float att = spot / dot( L.attenuation, float3(1.0f, d, d*d) );

	ambient *= spot;
	diffuse *= att;
	spec    *= att;
}

/// Transforms a normal map sample to world space.
float3 NormalSampleToWorldSpace(float3 normalMapSample, float3 unitNormalW, float3 tangentW)
{
	float3 normal_t = 2.0f * normalMapSample - 1.0f;

	// Build othonormal basis
	float3 N = unitNormalW;
	float3 T = normalize(tangentW - dot(tangentW, N) * N );
	float3 B = cross(N, T);

	float3x3 TBN = float3x3(T, B, N);

	// Transform from tangent space to world space.
	float3 bumped_normal_w = mul(normal_t, TBN);

	return bumped_normal_w;
}