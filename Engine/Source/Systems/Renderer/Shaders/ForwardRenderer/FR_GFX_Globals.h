#include "..\\ShaderDefines.h"
#include "..\\Lighting.h"
#include "..\\Texturing.h"
#include "..\\Fog.h"

cbuffer cbPerFrame : register(b0)
{
	Light		gLights[MAX_LIGHTS];
	uint		gNumLights;
	float3		gEyePosW;
	Fog			gFog;
};

cbuffer cbPerObject : register(b1)
{
	float4x4 gWorld;
	float4x4 gViewProj;
	float4x4 gTextureTransform;
	Material gMaterial;
};

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexCoord : TEXCOORD;
	float3 TangentL : TANGENT;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float3 TangentW : TANGENT;
	float2 TexCoord : TEXCOORD;
	float  DepthVS : DEPTHVS;
};

struct InstancedVertexIn
{
	float3		PosL : POSITION;
	float3		NormalL : NORMAL;
	float2		TexCoord : TEXCOORD;
	float3		TangentL : TANGENT;
	float4x4	World : WORLD;
	float4		Color : COLOR;
	uint		InstanceId : SV_InstanceID;
};

struct InstancedVertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float3 TangentW : TANGENT;
	float2 TexCoord : TEXCOORD;
	float4 Color : COLOR;
};

struct FlatVertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
};

struct FlatVertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
};