#include "FR_GFX_Globals.h"

// SHADOWING FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////
float3 
SampleShadowCascade(
	in int lightId,	in float3 shadowPosition, 
	in float3 shadowPosDX, in float3 shadowPosDY, 
	in uint cascadeIdx)
{
	shadowPosition += gLights[lightId].cascadeOffsets[cascadeIdx].xyz;
	shadowPosition *= gLights[lightId].cascadeScales[cascadeIdx].xyz;

	shadowPosDX *= gLights[lightId].cascadeScales[cascadeIdx].xyz;
	shadowPosDY *= gLights[lightId].cascadeScales[cascadeIdx].xyz;

	float3 cascade_color = 1.0f;

	// Uncomment to visualize cascades
	//const float3 cascade_colors[8] =
	//{
	//	float3(1.0f, 0.0, 0.0f),
	//	float3(0.0f, 1.0f, 0.0f),
	//	float3(0.0f, 0.0f, 1.0f),
	//	float3(1.0f, 1.0f, 0.0f),
	//	float3(1.0f, 0.0f, 1.0f),
	//	float3(0.0f, 1.0f, 1.0f),
	//	float3(1.0f, 1.0f, 1.0f),
	//	float3(0.2f, 0.2f, 0.2f),
	//};
	//cascade_color = cascade_colors[cascadeIdx];

	float shadow = SampleShadowMapFixedSizePCF(
		lightId, shadowPosition, shadowPosDX, 
		shadowPosDY, cascadeIdx, gLights[lightId].usePlaneDepthBias,
		gLights[lightId].staticOffsetBias);

	return shadow * cascade_color;
}

//-------------------------------------------------------------------------------------------------
// Calculates the offset to use for sampling the shadow map, based on the surface normal
//-------------------------------------------------------------------------------------------------
float3 GetShadowPosOffset(in int lightId, in float nDotL, in float3 normal)
{
	float2 shadow_map_size;
	float num_slices;
	gShadowMap[lightId].GetDimensions(shadow_map_size.x, shadow_map_size.y, num_slices);
	float texel_size = 2.0f / shadow_map_size.x;
	float nml_offset_scale = saturate(1.0f - nDotL);
	return texel_size * gLights[lightId].normalOffsetScaleBias * nml_offset_scale * normal;
}

void
CalculateShadowProjectionVariables(
	in int lightId,
	in float3 positionWS,
	in float depthVS,
	in float nDotL, 
	in float3 normal,
	out float3 shadowPos,
	out float3 shadowPosDx,
	out float3 shadowPosDy,
	out int cascadeIdx)
{
	cascadeIdx = 0;

	// Figure out which cascade to sample from
	[unroll]
	for (uint i = 0; i < NUM_SHADOW_CASCADES - 1; ++i)
	{
		[flatten]
		if (depthVS > gLights[lightId].cascadeSplits[i])
			cascadeIdx = i + 1;
	}

	
	// Apply offset towards normal
	float3 offset = GetShadowPosOffset(lightId, nDotL, normal) / abs(gLights[lightId].cascadeScales[cascadeIdx].z);
	float3 samplePos = positionWS + offset;
	// Project into shadow space
	shadowPos = mul(float4(samplePos, 1.0f), gLights[lightId].shadowMatrix).xyz;

	shadowPosDx = ddx_fine(shadowPos);
	shadowPosDy = ddy_fine(shadowPos);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////




float4 PS(VertexOut pixelIn) : SV_Target
{
	pixelIn.NormalW = normalize(pixelIn.NormalW);

	float3 to_eye_w = gEyePosW - pixelIn.PosW;
	float3 dist_to_eye = length(to_eye_w);
	to_eye_w /= (dist_to_eye + 0.0001f);

	// Diffuse Map
	float4 diffuse_map_color = gMaterial.diffuse;
#if defined(USE_DIFFUSE_MAP)
	diffuse_map_color = SampleDiffuseMap(pixelIn.TexCoord);
#endif

	// Normal mapping
#if defined(USE_BUMP_MAP)
	float3 normal_map_sample = SampleNormalMap(pixelIn.TexCoord);
	pixelIn.NormalW = NormalSampleToWorldSpace(normal_map_sample, pixelIn.NormalW, pixelIn.TangentW);
#endif

	// Lighting
	uint num_lights = min(gNumLights, MAX_LIGHTS);

	float4 lit_color = diffuse_map_color;
	clip(lit_color.w - 0.01f);

	if (num_lights > 0)
	{		
		float4 final_ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

		[unroll]
		for (uint i = 0; i < num_lights; i++)
		{
			float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

			if (DIRECTIONAL_LIGHT == gLights[i].lightType)
			{
				ComputeDirectionalLight(gMaterial, gLights[i].directionalLight, pixelIn.NormalW, to_eye_w,
					ambient, diffuse, spec);

				// Calculate shadow visibility
				if (gLights[i].emitsShadows > 0)
				{
					float3 shadow_visibility = 1.f;
						
					float3 shadow_pos;
					float3 shadow_pos_dx;
					float3 shadow_pos_dy;
					int cascade_idx;

					float n_dot_l = saturate(dot(pixelIn.NormalW, -gLights[i].directionalLight.direction));

					CalculateShadowProjectionVariables(
						i, pixelIn.PosW, pixelIn.DepthVS, n_dot_l, pixelIn.NormalW, shadow_pos, shadow_pos_dx, shadow_pos_dy, cascade_idx);

					shadow_visibility = SampleShadowCascade(i, shadow_pos, shadow_pos_dx, shadow_pos_dy, cascade_idx);

					float3 diffuse_shade = (1.f- gLights[i].shadowIntensity + (gLights[i].shadowIntensity * shadow_visibility));
					diffuse *= float4(diffuse_shade, 1.f);
					spec *= float4(diffuse_shade, 1.f);
				}
			}

			if (POINT_LIGHT == gLights[i].lightType)
				ComputePointLight(gMaterial, gLights[i].pointLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w,
				ambient, diffuse, spec);

			if (SPOT_LIGHT == gLights[i].lightType)
				ComputeSpotLight(gMaterial, gLights[i].spotLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w,
				ambient, diffuse, spec);

			final_ambient += ambient;
			final_diffuse += diffuse;
			final_spec += spec;			
		}

		// Modulate with late add
		lit_color = diffuse_map_color*(final_ambient + final_diffuse) + final_spec;

		// Common to take alpha from diffuse material
#if defined(USE_DIFFUSE_MAP)
		lit_color.a = diffuse_map_color.a * gMaterial.diffuse.a;
#endif
	}


	// If any of the reflection channel is set, sample the reflection map
	if ((gMaterial.reFlect.x + gMaterial.reFlect.y + gMaterial.reFlect.z + gMaterial.reFlect.w) > 0.01f)
	{
		float3 incident = -to_eye_w;
		float3 reflection_vector = reflect(incident, pixelIn.NormalW);
		float4 reflection_color = SampleCubeMap(reflection_vector);

		lit_color += gMaterial.reFlect * reflection_color;
	}

	// Final = FogFactor * lit_color + (1.0 - FogFactor) * FogColor
	float fog_factor = ComputeFog(gFog, distance(gEyePosW, pixelIn.PosW));
	lit_color = fog_factor * lit_color + (1.f - fog_factor) * gFog.fogColor;

	return lit_color;
}
