#include "FR_GFX_Globals.h"

FlatVertexOut FLAT_VS( FlatVertexIn vertexIn )
{
	FlatVertexOut vertex_out;

	// Transform to world space
	vertex_out.PosW		= mul( float4(vertexIn.PosL, 1.0f), gWorld ).xyz;
	//vertex_out.NormalW  = mul( vertexIn.NormalL, (float3x3)gWorld ).xyz;	
	vertex_out.PosH = mul( float4(vertex_out.PosW, 1.0f), gViewProj );

	return vertex_out;
}