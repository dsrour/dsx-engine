#include "FR_GFX_Globals.h"

InstancedVertexOut InstancedVS(InstancedVertexIn vertexIn)
{
	InstancedVertexOut vertex_out;

	// Transform to world space
	vertex_out.PosW = mul(float4(vertexIn.PosL, 1.0f), vertexIn.World).xyz;
	vertex_out.NormalW = mul(vertexIn.NormalL, (float3x3)vertexIn.World).xyz;
	vertex_out.TangentW = mul(vertexIn.TangentL, (float3x3)vertexIn.World).xyz;
	vertex_out.TexCoord = vertexIn.TexCoord;
	vertex_out.Color = vertexIn.Color;
	// Transform to homogeneous clip space
	vertex_out.PosH = mul(float4(vertex_out.PosW, 1.0f), gViewProj);

	return vertex_out;
}