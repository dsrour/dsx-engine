/* Based on MJP's demo:
* http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/
*/

#define GRID_SIZE 16

cbuffer ReductionConstants : register(b0)
{
	float4x4 Projection;
	float NearClip;
	float FarClip;
	float2 pad;
}

Texture2D<float2> gInputMap : register(t0);
RWTexture2D<float2> gOutputMap : register(u0);

groupshared float2 gDepthSamples[GRID_SIZE*GRID_SIZE];

[numthreads(GRID_SIZE, GRID_SIZE, 1)]
void
DepthReduction(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	uint2 texture_size;
	gInputMap.GetDimensions(texture_size.x, texture_size.y);

	uint2 sample_pos = Gid.xy * GRID_SIZE + GTid.xy;
	sample_pos = min(sample_pos, texture_size - 1);

	float min_depth = gInputMap[sample_pos].x;
	float max_depth = gInputMap[sample_pos].y;

	if (min_depth == 0.0f)
		min_depth = 1.0f;

	gDepthSamples[GI] = float2(min_depth, max_depth);
	GroupMemoryBarrierWithGroupSync();

	[unroll]
	for (uint s = GRID_SIZE * GRID_SIZE / 2; s > 0; s >>= 1)
	{
		if (GI < s)
		{
			gDepthSamples[GI].x = min(gDepthSamples[GI].x, gDepthSamples[GI + s].x);
			gDepthSamples[GI].y = max(gDepthSamples[GI].y, gDepthSamples[GI + s].y);
		}

		GroupMemoryBarrierWithGroupSync();
	}

	if (GI == 0)
	{
		min_depth = gDepthSamples[0].x;
		max_depth = gDepthSamples[0].y;
		gOutputMap[Gid.xy] = float2(min_depth, max_depth);
	}
}