cbuffer cbPerFrame
{
	float4x4 gWorld;
	float4x4 gViewProj;
};

struct VertexIn
{
	float3 PosL		: POSITION;	
	float3 Color	: NORMAL;
};

struct VertexOut
{
	float4 PosH		: SV_POSITION;
	float3 Color	: NORMAL;	
};
 
VertexOut VS(VertexIn vin)
{
	VertexOut vout;
		
	float3 pos_w = mul( float4(vin.PosL, 1.0f), gWorld ).xyz;
	vout.PosH = mul( float4(pos_w, 1.0f), gViewProj );
	vout.Color = vin.Color;

	return vout;
}