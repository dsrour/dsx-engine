struct PsIn
{
	float4 Pos		: SV_Position;
	float2 TexCoord : TexCoord;
};

PsIn
VertexResolve(uint VertexID : SV_VertexID)
{
	PsIn Out;

	// Produce a fullscreen triangle
	float4 position;
	position.x = (VertexID == 2) ? 3.0 : -1.0;
	position.y = (VertexID == 0) ? -3.0 : 1.0;
	position.zw = 1.0;

	Out.Pos = position;
	Out.TexCoord = position.xy * float2(0.5, -0.5) + 0.5;

	return Out;
}