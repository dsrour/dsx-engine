struct PsIn
{
	float4 Pos		: SV_Position;
	float2 TexCoord : TexCoord;
};

SamplerState gPointCloudTextureSampler;
Texture2D gPointCloudTexture;

float4
PsResolve(PsIn In) : SV_Target
{
	return gPointCloudTexture.Sample(gPointCloudTextureSampler, In.TexCoord);
}