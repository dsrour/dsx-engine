#include "HR_Globals.h"

#define GRP_DIM 1024

cbuffer cbConstants
{
	unsigned int gNumVoxelsPerLength;
	float3		 pad;
};
RWByteAddressBuffer  gVoxelGridOut   : register(u0); // voxel structure

/* This kernel will be ran first to reset grid datastructures appropriately.
 * 
 * Dispatch:
 * - A voxel grid of N^3 will dispatch ((pow(mSimulationGrid.NumVoxelsPerLength(), 3) / 1024) + 1) thread groups in the X dimension.
 *
 * Algorithm:
 * - Each voxel is a tile of 64KB. 
 * - Many nodes of different sizes will be packed into this tile. 
 * - A node always has a header that references the next node in the tile.
 */
[numthreads(GRP_DIM, 1, 1)]
void
InitGrid(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{	
	unsigned int tile_index = ((Gid.x * GRP_DIM) + GI);
	unsigned int tile_head_node_offset = tile_index * BYTES_PER_TILE;

	unsigned int total_size = gNumVoxelsPerLength * gNumVoxelsPerLength * gNumVoxelsPerLength * BYTES_PER_TILE;

	if (tile_head_node_offset < total_size)
	{
		// 1st int of header represents the offset to the next node in the tile
		gVoxelGridOut.Store(tile_head_node_offset, -1);
		// 2nd int provides a counter for how many triangles are in the node
		gVoxelGridOut.Store(tile_head_node_offset + 4, 0);
		//gVoxelGridOut.Store(tile_head_node_offset + 8, tile_index);
	}
}