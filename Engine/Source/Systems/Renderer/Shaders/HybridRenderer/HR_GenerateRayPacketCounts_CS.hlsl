#include "HR_Globals.h"
#include "HR_SimulationGridUtils.h"

cbuffer cbConstants : register(b0)
{
	float4			  gSimGridCenterAndVoxelSizePerLength;
	int				  gNumVoxelsPerLength;
	int3			  gDispatchParams;
};


// Hash function from: http://stackoverflow.com/questions/664014/what-integer-hash-function-are-good-that-accepts-an-integer-hash-key
//                     by "Thomas Mueller"	
//unsigned int Hash(unsigned int x)
//{
//	unsigned int old_range = 4294967294;
//	static unsigned int new_range = MAX_RAYS_PER_PACKET;
//
//	x = ((x >> 16) ^ x) * 0x45d9f3b;
//	x = ((x >> 16) ^ x) * 0x45d9f3b;
//	x = ((x >> 16) ^ x);
//
//	x = (unsigned int)(x / (float)old_range * new_range);
//
//	return x;
//}




Texture2D<int>										gRayVoxelIds			: register(t0); // 1 per pixel... -1 = no active ray at that thread
RWBuffer<int>										gRayPacketCounts	    : register(u0);
RWBuffer<int>										gHashLookup			    : register(u1);
RWBuffer<uint>										gIndirectDispArgs	    : register(u2);

groupshared int  gLocalVoxelIdsLookup[MAX_RAYS_PER_PACKET];
groupshared uint gLocalVoxelIdsCount[MAX_RAYS_PER_PACKET];

groupshared int gLocalHashLookup[MAX_RAYS_PER_PACKET];

groupshared uint gLocalTotalPackets;

/* This kernel takes in a "RayLink" buffer which is a 2D texture 
 * of the rays generated during rasterization.
 *
 * The dispatch is 1 thread per pixel.
 *
 * Each thread checks on the RayHit to first see if its ray is active.
 * If it is active, the ray id and Morton code corresponding to the voxel
 * the ray is currently in is stored arbitrarily into the "ActiveRayLinks"
 * buffer.
 */
[numthreads(RAY_MARCH_GRP_DIM_X, RAY_MARCH_GRP_DIM_Y, 1)]
void
GenerateRayPacketCounts(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{	
	int total_voxels = gNumVoxelsPerLength*gNumVoxelsPerLength*gNumVoxelsPerLength;

	uint flatten_group_index = (Gid.y * gDispatchParams.x + Gid.x);		
	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };
	int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;
			
	gLocalVoxelIdsLookup[GI] = -1;
	gLocalHashLookup[GI] = -1;
	gLocalVoxelIdsCount[GI] = 0;

	if (GI == 0)			
		gLocalTotalPackets = 0;
	
	if (GI + flatten_group_index == 0)
	{		
		gIndirectDispArgs[0] = 0;
		gIndirectDispArgs[1] = 1;
		gIndirectDispArgs[2] = 1;
	}

			
	int write_index = -1;
	int voxel_id = gRayVoxelIds[texture_index];

	if (voxel_id >= 0)
		gLocalVoxelIdsLookup[GI] = voxel_id;

	AllMemoryBarrierWithGroupSync();

	uint count = 0;
	uint hash = 0;
	if (voxel_id >= 0)
	{

		[unroll(MAX_RAYS_PER_PACKET)]
		for (uint i = 0; i < MAX_RAYS_PER_PACKET; i++)
		{
			if (gLocalVoxelIdsLookup[i] == voxel_id) 
				count++;
		}		
				
		// Generate hash lookup
		hash = count - 1;
		if (count != MAX_RAYS_PER_PACKET) // No need to find a has lookup if all rays point to the same voxel id... trying to skip interlocked operations
		{
			int orig;
			int cnter = 0;

			[allow_uav_condition]
			do
			{
				hash = (hash + cnter) % 63;
				InterlockedCompareExchange(gLocalHashLookup[hash], -1, voxel_id, orig);
				cnter++;
			} while (orig != -1 && orig != voxel_id);
		}

		gHashLookup[ray_voxel_id_index] = hash;
		gLocalVoxelIdsCount[hash] = count; // just pray to baby Jesus there isn't a collision.	
	}

	GroupMemoryBarrierWithGroupSync();

	uint local_ray_count = gLocalVoxelIdsCount[GI];
	if (local_ray_count > 0)
	{
		uint num_packets = ceil(local_ray_count  / (float)MAX_RAYS_PER_PACKET);
		InterlockedAdd(gLocalTotalPackets, num_packets);
	}

	GroupMemoryBarrierWithGroupSync();

	if (GI == 0)
	{
		gRayPacketCounts[flatten_group_index] = gLocalTotalPackets;

		InterlockedAdd(gIndirectDispArgs[0], gLocalTotalPackets);		
	}
}