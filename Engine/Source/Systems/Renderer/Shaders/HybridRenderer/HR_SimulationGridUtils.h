
void
CoordinatesFromVoxelId( int voxelId, int numVoxelsInLength,
						out int iOut, out int jOut, out int kOut)
{
	int tmp_vox_id = voxelId;
	kOut = (int)floor(tmp_vox_id / float(numVoxelsInLength * numVoxelsInLength));
	tmp_vox_id -= (numVoxelsInLength * numVoxelsInLength * kOut);
	jOut = (int)floor(tmp_vox_id / (float)numVoxelsInLength);
	tmp_vox_id -= numVoxelsInLength * jOut;
	iOut = tmp_vox_id;
}


