#include "HR_Globals.h"

#define SQRT_NUM_THREADS 8

cbuffer cbConstants : register(b0)
{
	int3  gDispatchParams;
	int   gMaxDilation;
	int2  gResolution;
	int2  pad2;
	float4x4 gViewProj;
};

Texture2DArray<float4>			gRaysList				: register(t0); // list of rays 
Texture2D<int>					gRayVoxelIds			: register(t1); // 1 per pixel... -1 = no active ray at that thread
RWBuffer<float4>				gDilationMask			: register(u0);

[numthreads(SQRT_NUM_THREADS, SQRT_NUM_THREADS, 1)]
void
GenerateDilationMask(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{		
	if (gMaxDilation <= 0)
		return;

	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };
	//int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;	

	float4 ray_orig_dist = gRaysList[int3(texture_index, 0)];
	float4 dir_shadow_intensity = gRaysList[int3(texture_index, 1)];

	/*bool is_edge = 0;
	int pen_size_avg_half_kernel_size = 2;
	for (int x = -pen_size_avg_half_kernel_size; x <= pen_size_avg_half_kernel_size; x++)
		for (int y = -pen_size_avg_half_kernel_size; y <= pen_size_avg_half_kernel_size; y++)
		{
			if (0 == x && 0 == y)
				continue;

			int2 coords = texture_index + int2(x, y);

			if (coords.x >= 0 && coords.x < gResolution[0] &&
				coords.y >= 0 && coords.y < gResolution[1] &&
				gRaysList[int3(coords, 1)].w == 0.f)
			{
				is_edge = true;
				break;
			}
		}*/

	if (dir_shadow_intensity.w > 0.f/* && is_edge*/)
	{
		int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;
		gDilationMask[ray_voxel_id_index] = dir_shadow_intensity;

		//float max_half_pen_width = gMaxDilation / 2.f; 
		//max_half_pen_width = mul(
		//	float4(max_half_pen_width, max_half_pen_width, max_half_pen_width, max_half_pen_width), 
		//	gViewProj).x;	
		//if (max_half_pen_width > gMaxDilation || max_half_pen_width < 0)
		//	max_half_pen_width = gMaxDilation;
		
		/*for (int x = -max_half_pen_width; x <= max_half_pen_width; x++)
			for (int y = -max_half_pen_width; y <= max_half_pen_width; y++)
			{
				int i = texture_index.x + x;
				if (i >= 0 && i < gResolution[0])
				{
					int j = texture_index.y + y;
					if (j >= 0 && j < gResolution[1])
					{
						ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * j) + i;
						if (gRaysList[int3(texture_index + int2(x, y), 1)].w > 0.f)
							gDilationMask[ray_voxel_id_index] = dir_shadow_intensity;
					}
				}
			}*/
		
	}	
}