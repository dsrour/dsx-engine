#define BYTES_PER_NODE_HEADER	16
#define BYTES_PER_TRIANGLE		36
#define BYTES_PER_RAY			32
#define BYTES_PER_TILE			65536

#define RAY_MARCH_GRP_DIM_X 8
#define RAY_MARCH_GRP_DIM_Y 8
#define MAX_RAYS_PER_PACKET		(RAY_MARCH_GRP_DIM_X*RAY_MARCH_GRP_DIM_Y)

struct Triangle48
{
	float4 v1;
	float4 v2;
	float4 v3;
};

struct Triangle36
{
	float3 v1;
	float3 v2;
	float3 v3;
};

struct PacketHeader
{	
	int		voxelId;
	int		offsetLocation;			// into the ray packets buffer (tiled resource)	
	int2	packetRayHitCoords;		// coords to the top left pixel of the section of 
									// the hit info texture that corresponds to the ray packet
};