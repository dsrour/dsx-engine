#include "HR_FR_GFX_Globals.h"
#include "HR_Globals.h"

//RWTexture2D<float4>		  gShadowRaysHit   : register(u3);
//RWTexture2D<int>			  gRayVoxelIds	   : register(u4);

// SHADOWING FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////
float3
SampleShadowCascade(
	in int lightId, in float3 shadowPosition,
	in float3 shadowPosDX, in float3 shadowPosDY,
	in uint cascadeIdx,
	out float panumbraSize)
{
	shadowPosition += gLights[lightId].cascadeOffsets[cascadeIdx].xyz;
	shadowPosition *= gLights[lightId].cascadeScales[cascadeIdx].xyz;

	shadowPosDX *= gLights[lightId].cascadeScales[cascadeIdx].xyz;
	shadowPosDY *= gLights[lightId].cascadeScales[cascadeIdx].xyz;

	float3 cascade_color = 1.0f;

	// Uncomment to visualize cascades
	//const float3 cascade_colors[8] =
	//{
	//	float3(1.0f, 0.0, 0.0f),
	//	float3(0.0f, 1.0f, 0.0f),
	//	float3(0.0f, 0.0f, 1.0f),
	//	float3(1.0f, 1.0f, 0.0f),
	//	float3(1.0f, 0.0f, 1.0f),
	//	float3(0.0f, 1.0f, 1.0f),
	//	float3(1.0f, 1.0f, 1.0f),
	//	float3(0.2f, 0.2f, 0.2f),
	//};
	//cascade_color = cascade_colors[cascadeIdx];

	float sampled_depth = 1.f;
	float shadow = SampleShadowMapFixedSizePCFWithDepth(
		lightId, shadowPosition, shadowPosDX,
		shadowPosDY, cascadeIdx, gLights[lightId].usePlaneDepthBias,
		gLights[lightId].staticOffsetBias, sampled_depth);
	
	if (shadow < 1.f)
	{		
		// NOTE::	Since we are tightly fitting the shadow frustum to the scene that is currently viewable,
		//			the penumbra size will change depending on the camera rotation. 
		//			Instead, mark it to 1.f to let the penumbra dilation shader there is supposed to be "some"
		//			dilation to be done from this pixel. But uniformly dilate to the "max dilation" parameter instead.
		//			We are increasing ray tests at the expense of not seeing the penumbra change directions when the camera 
		//			has its rotation changed. The overall number of rays can be lowered later on by downsampling the resolution
		//			of the shadow rays list.
		//panumbraSize = (shadowPosition.z - sampled_depth) * gLights[lightId].directionalLight.lightSize;
		//panumbraSize /= shadowPosition.z;
		panumbraSize = 1.f;
	}
	else
		panumbraSize = 0.f;

	return shadow * cascade_color;
}

//-------------------------------------------------------------------------------------------------
// Calculates the offset to use for sampling the shadow map, based on the surface normal
//-------------------------------------------------------------------------------------------------
float3 GetShadowPosOffset(in int lightId, in float nDotL, in float3 normal)
{
	float2 shadow_map_size;
	float num_slices;
	gShadowMap[lightId].GetDimensions(shadow_map_size.x, shadow_map_size.y, num_slices);
	float texel_size = 2.0f / shadow_map_size.x;
	float nml_offset_scale = saturate(1.0f - nDotL);
	return texel_size * gLights[lightId].normalOffsetScaleBias * nml_offset_scale * normal;
}

void
CalculateShadowProjectionVariables(
	in int lightId,
	in float3 positionWS,
	in float depthVS,
	in float nDotL,
	in float3 normal,
	out float3 shadowPos,
	out float3 shadowPosDx,
	out float3 shadowPosDy,
	out int cascadeIdx)
{
	cascadeIdx = 0;

	// Figure out which cascade to sample from
	[unroll]
	for (uint i = 0; i < NUM_SHADOW_CASCADES - 1; ++i)
	{
		[flatten]
		if (depthVS > gLights[lightId].cascadeSplits[i])
			cascadeIdx = i + 1;
	}


	// Apply offset towards normal
	float3 offset = GetShadowPosOffset(lightId, nDotL, normal) / abs(gLights[lightId].cascadeScales[cascadeIdx].z);
	float3 samplePos = positionWS + offset;
	// Project into shadow space
	shadowPos = mul(float4(samplePos, 1.0f), gLights[lightId].shadowMatrix).xyz;

	shadowPosDx = ddx_fine(shadowPos);
	shadowPosDy = ddy_fine(shadowPos);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/* TODO: when doing more ray-tracing effects that require different rays,
 *       store only bare necessary info needed into the MRT to create the 
 *		 rays in a deferred pass.
 */
struct PsOutput
{
	float4 color : SV_Target0;
	float4 posAndShadowIntensity : SV_Target1;
	//float4 shadowRayOrigDist : SV_Target1;
	//float4 shadowRayDirPenumSize : SV_Target2;
};

[earlydepthstencil] // This is needed since UAVs above force the early-z to be disabled. 
					// This can cause problems and make rays of forward objects "disappear" if objects in front of it are drawn after.
PsOutput PS(VertexOut pixelIn)
{
	PsOutput ret = (PsOutput)0;

	pixelIn.NormalW = normalize(pixelIn.NormalW);

	float3 to_eye_w = gEyePosW - pixelIn.PosW;
	float3 dist_to_eye = length(to_eye_w);
	to_eye_w /= (dist_to_eye + 0.0001f);

	// Diffuse Map
	float4 diffuse_map_color = gMaterial.diffuse;
#if defined(USE_DIFFUSE_MAP)
	diffuse_map_color = SampleDiffuseMap(pixelIn.TexCoord);
#endif

	// Normal mapping
#if defined(USE_BUMP_MAP)
	float3 normal_map_sample = SampleNormalMap(pixelIn.TexCoord);
	pixelIn.NormalW = NormalSampleToWorldSpace(normal_map_sample, pixelIn.NormalW, pixelIn.TangentW);
#endif

	// Lighting
	uint num_lights = min(gNumLights, MAX_LIGHTS);

	ret.color = diffuse_map_color;
	clip(ret.color.w - 0.01f);
	ret.posAndShadowIntensity = float4(pixelIn.PosW.xyz, 0);

	if (num_lights > 0)
	{
		float4 final_ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_spec = float4(0.0f, 0.0f, 0.0f, 0.0f);		

		[unroll]
		for (uint i = 0; i < num_lights; i++)
		{
			float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);


			if (DIRECTIONAL_LIGHT == gLights[i].lightType) 
			{
				ComputeDirectionalLight(gMaterial, gLights[i].directionalLight, pixelIn.NormalW, to_eye_w,
					ambient, diffuse, spec);

				// Calculate shadow visibility
				if (gLights[i].emitsShadows > 0)
				{
					float3 shadow_visibility = 1.f;

					float3 shadow_pos;
					float3 shadow_pos_dx;
					float3 shadow_pos_dy;
					int cascade_idx;

					float n_dot_l = saturate(dot(pixelIn.NormalW, -gLights[i].directionalLight.direction));

					CalculateShadowProjectionVariables(
						i, pixelIn.PosW, pixelIn.DepthVS, n_dot_l, pixelIn.NormalW, shadow_pos, shadow_pos_dx, shadow_pos_dy, cascade_idx);
					
					float penumbra_size = 0;
					shadow_visibility = SampleShadowCascade(i, shadow_pos, shadow_pos_dx, shadow_pos_dy, cascade_idx, penumbra_size);

					//if (shadow_visibility.x == 0.f || shadow_visibility.x == 1.f)
					//	penumbra_size = 0.f;
					
					float3 diffuse_shade = (1.f - gLights[i].shadowIntensity + (gLights[i].shadowIntensity * shadow_visibility));
					float shadow_intensity = 1.f - diffuse_shade.x;
					ret.posAndShadowIntensity.w += shadow_intensity;

					//diffuse *= float4(diffuse_shade, 1.f);
					//spec *= float4(diffuse_shade, 1.f);
									

					// Penumbras are calculated if light 0 is a directional light	
					//if (0 == i)
					//{
					//	float3  direction = { 0.f, 0.f, 0.f };
					//	direction = -(gLights[i].directionalLight.direction);
					//	
					//	gShadowRaysHit[pixelIn.PosH.xy] = float4(0.f, 0.f, 0.f, 0.f);

					//	// slice 1 = origin xyz, distance; slice 2 = direction, panumbra size
					//	float4 orig_dist = { pixelIn.PosW.xyz + (float3(0.01f, 0.01f, 0.01f) * direction), 0.f }; // offset the ray a little bit from the primitive				

					//	ret.shadowRayOrigDist = orig_dist;
					//	ret.shadowRayDirPenumSize = float4(direction, 1.f - diffuse_shade.x); // instead of panumbra size, store shadow intensity in the ray normal w component

					//	float world_extent = gSimGridCenterAndVoxelSizePerLength.w * (gNumVoxelsPerLength / 2.f);
					//	float3 world_min = gSimGridCenterAndVoxelSizePerLength.xyz - float3(world_extent, world_extent, world_extent);

					//	int i = (int)floor((orig_dist.x - world_min.x) / gSimGridCenterAndVoxelSizePerLength.w);
					//	int j = (int)floor((orig_dist.y - world_min.y) / gSimGridCenterAndVoxelSizePerLength.w);
					//	int k = (int)floor((orig_dist.z - world_min.z) / gSimGridCenterAndVoxelSizePerLength.w);

					//	if (i >= 0 && i < gNumVoxelsPerLength &&
					//		j >= 0 && j < gNumVoxelsPerLength &&
					//		k >= 0 && k < gNumVoxelsPerLength)
					//	{
					//		int voxel_id = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);

					//		int2 xy = { floor(pixelIn.PosH.x), floor(pixelIn.PosH.y) };
					//		int buffer_index = (gViewportSize.x * xy.y) + xy.x;
					//		
					//		// NOTE:	This if check is crucial for edge detection in intermediate pass during penumbra 
					//		//			dilation as to know which way to dilate (towards the center of the shadow map).
					//		if (penumbra_size > 0.f) 
					//			gRayVoxelIds[pixelIn.PosH.xy] = voxel_id;
					//	}					
					//}
				}				
			}

			if (POINT_LIGHT == gLights[i].lightType)
				ComputePointLight(gMaterial, gLights[i].pointLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w,
				ambient, diffuse, spec);

			if (SPOT_LIGHT == gLights[i].lightType)
				ComputeSpotLight(gMaterial, gLights[i].spotLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w,
				ambient, diffuse, spec);

			final_ambient += ambient;
			final_diffuse += diffuse;
			final_spec += spec;
		}

		// Modulate with late add
		ret.color = diffuse_map_color*(final_ambient + final_diffuse) + final_spec;

		// Common to take alpha from diffuse material
#if defined(USE_DIFFUSE_MAP)
		ret.color.a = diffuse_map_color.a * gMaterial.diffuse.a;
#endif
	}

	// If any of the reflection channel is set, sample the reflection map
	if ((gMaterial.reFlect.x + gMaterial.reFlect.y + gMaterial.reFlect.z + gMaterial.reFlect.w) > 0.01f)
	{
		float3 incident = -to_eye_w;
		float3 reflection_vector = reflect(incident, pixelIn.NormalW);
		float4 reflection_color = SampleCubeMap(reflection_vector);

		ret.color += gMaterial.reFlect * reflection_color;
	}
		
	float fog_factor = ComputeFog(gFog, distance(gEyePosW, pixelIn.PosW));
	ret.color = fog_factor * ret.color + (1.f - fog_factor) * gFog.fogColor;

	return ret;
}
