#include "HR_Globals.h"
#include "..\\Random.h"
#include "..\\PoissonDiscKernels.h"

#define SQRT_NUM_THREADS 8

cbuffer cbConstants : register(b0)
{
	int3  gDispatchParams;
	int   pad1;
	int2  gResolution;
	int   gCurrSample;
	float gDirLightSize;
};

RWTexture2DArray<float4>		gRaysList				: register(u0); // list of rays 
RWTexture2D<int>				gRayVoxelIds			: register(u1); // 1 per pixel... -1 = no active ray at that thread
Buffer<float4>					gDilationMask			: register(t0);

[numthreads(SQRT_NUM_THREADS, SQRT_NUM_THREADS, 1)]
void
ApplyDilationMask(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{		
	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };
	int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;	

	float4 mask_val = gDilationMask[ray_voxel_id_index]; // mask_val is the normalized direction of the shadow ray and shadow intensity for that pixel
	if (!any(mask_val)) // (0,0,0,0) indicates empty space within the mask			
		gRayVoxelIds[texture_index] = -1;	
	else
	{
		static const float PI = 3.14159265f;

		float3 light_dir = mask_val.xyz;

		// Light size represents how much of an angle jitter on the hemisphere of a unit sphere we use
		float angle = gDirLightSize / 2.f; 

		// Get random val between -.5 and -5
		// Note that the seed must be different depending on the current sample
		uint seed = ((ray_voxel_id_index + gCurrSample) * (texture_index.x)) + texture_index.y;
		float random = (float(WangHash(seed)) * (1.f / 4294967296.f)) - 0.49999999f;
		
		// Jitter ray
		//light_dir += (random * gDirLightSize);
		/*float3 jitter = PoissonDisc3D4[gCurrSample] - float3(0.5f, 0.5f, 0.5f);
		jitter *= float3(gDirLightSize, gDirLightSize, gDirLightSize);
		light_dir += jitter;
		light_dir += random;*/

		light_dir += (PoissonDisc3D16[gCurrSample] - float3(0.5f, 0.5f, 0.5f)) * gDirLightSize;
		float3 ray_direction = normalize(light_dir);

		gRaysList[int3(texture_index, 1)] = float4(ray_direction, -1.f);
	}
}