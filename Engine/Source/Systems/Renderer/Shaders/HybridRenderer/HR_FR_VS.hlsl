#include "HR_FR_GFX_Globals.h"

VertexOut VS(VertexIn vertexIn)
{
	VertexOut vertex_out;

	// Transform to world space
	vertex_out.PosW = mul(float4(vertexIn.PosL, 1.0f), gWorld).xyz;
	vertex_out.NormalW = mul(vertexIn.NormalL, (float3x3)gWorld).xyz;
	vertex_out.TangentW = mul(vertexIn.TangentL, (float3x3)gWorld).xyz;
	vertex_out.TexCoord = mul(float4(vertexIn.TexCoord, 0.f, 1.f), gTextureTransform).xy;

	// Transform to homogeneous clip space
	vertex_out.PosH = mul(float4(vertex_out.PosW, 1.0f), gViewProj);

	vertex_out.DepthVS = vertex_out.PosH.w;

	return vertex_out;
}