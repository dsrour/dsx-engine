#include "HR_Globals.h"
#include "..\\Intersection.h"

#define ConstructVoxelGrid_GRP_DIM 512

cbuffer cbConstants : register(b0)
{
	float4x4		  gWorldTransform;
	float4			  gSimGridCenterAndVoxelSizePerLength;
	int				  gNumTris;
	int				  gNumVoxelsPerLength;
	float2			  pad;
};

StructuredBuffer<Triangle48> gTrianglesIn			: register(t0);
RWByteAddressBuffer		     gBinaryVoxelGridOut    : register(u0); // flattened indexed buffer of the simulation grid

[numthreads(ConstructVoxelGrid_GRP_DIM, 1, 1)]
void
ConstructVoxelGrid(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	int index = Gid.x * ConstructVoxelGrid_GRP_DIM + GI;
	if (index >= gNumTris)
		return;

	Triangle48 tri = gTrianglesIn[index];

	float3 w_v1 = mul(tri.v1, gWorldTransform).xyz;
	float3 w_v2 = mul(tri.v2, gWorldTransform).xyz;
	float3 w_v3 = mul(tri.v3, gWorldTransform).xyz;

	float extent = gSimGridCenterAndVoxelSizePerLength.w * gNumVoxelsPerLength / 2.f;

	float3 world_min = gSimGridCenterAndVoxelSizePerLength.xyz - float3(extent, extent, extent);
	//float3 world_max = gSimGridCenterAndVoxelSizePerLength.xyz + float3(extent, extent, extent);

	// xyz's to ijk's
	int i1 = (int)floor((w_v1.x - world_min.x) / gSimGridCenterAndVoxelSizePerLength.w);
	int j1 = (int)floor((w_v1.y - world_min.y) / gSimGridCenterAndVoxelSizePerLength.w);
	int k1 = (int)floor((w_v1.z - world_min.z) / gSimGridCenterAndVoxelSizePerLength.w);
	int i2 = (int)floor((w_v2.x - world_min.x) / gSimGridCenterAndVoxelSizePerLength.w);
	int j2 = (int)floor((w_v2.y - world_min.y) / gSimGridCenterAndVoxelSizePerLength.w);
	int k2 = (int)floor((w_v2.z - world_min.z) / gSimGridCenterAndVoxelSizePerLength.w);
	int i3 = (int)floor((w_v3.x - world_min.x) / gSimGridCenterAndVoxelSizePerLength.w);
	int j3 = (int)floor((w_v3.y - world_min.y) / gSimGridCenterAndVoxelSizePerLength.w);
	int k3 = (int)floor((w_v3.z - world_min.z) / gSimGridCenterAndVoxelSizePerLength.w);

	// Voxel grid bounding box
	int min_i = min(i1, min(i2, i3));
	int max_i = max(i1, max(i2, i3));
	int min_j = min(j1, min(j2, j3));
	int max_j = max(j1, max(j2, j3));
	int min_k = min(k1, min(k2, k3));
	int max_k = max(k1, max(k2, k3));

	float voxel_half_size = gSimGridCenterAndVoxelSizePerLength.w / 2.f;

	// Sweep through voxels in bounding box and check if they intersect
	for (int i = max(0, min_i); i <= min(max_i, gNumVoxelsPerLength - 1); i++)
		for (int j = max(0, min_j); j <= min(max_j, gNumVoxelsPerLength - 1); j++)
			for (int k = max(0, min_k); k <= min(max_k, gNumVoxelsPerLength - 1); k++)
			{
				unsigned int voxel_index = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);

				float3 voxel_center = {
					world_min.x + (i * gSimGridCenterAndVoxelSizePerLength.w) + voxel_half_size,
					world_min.y + (j * gSimGridCenterAndVoxelSizePerLength.w) + voxel_half_size,
					world_min.z + (k * gSimGridCenterAndVoxelSizePerLength.w) + voxel_half_size };

				float3 verts[3] = { w_v1, w_v2, w_v3 };


				int intersect_result = TriBoxOverlap(voxel_center, float3(voxel_half_size, voxel_half_size, voxel_half_size), verts);

				// Write triangle in voxel
				if (0 != intersect_result)
				{
					int node_offset = voxel_index * BYTES_PER_TILE;
										
					// Get index of where to write triangle
					int num_tris;
					gBinaryVoxelGridOut.InterlockedAdd(node_offset + 4, 1, num_tris); // increment num triangles
					
					// Only update if there is room
					if (num_tris < (BYTES_PER_TILE - BYTES_PER_NODE_HEADER) / (BYTES_PER_TRIANGLE))
					{
						unsigned int write_offset = node_offset + BYTES_PER_NODE_HEADER + (num_tris * BYTES_PER_TRIANGLE);
						//gBinaryVoxelGridOut.Store3(write_offset, asint(float3(1,2,3)));
						//gBinaryVoxelGridOut.Store3(write_offset + 12, asint(float3(1, 2, 3)));
						//gBinaryVoxelGridOut.Store3(write_offset + 24, asint(float3(1, 2, 3)));
						gBinaryVoxelGridOut.Store3(write_offset, asint(verts[0]));
						gBinaryVoxelGridOut.Store3(write_offset + 12, asint(verts[1]));
						gBinaryVoxelGridOut.Store3(write_offset + 24, asint(verts[2]));
					}
					//else
					//	gBinaryVoxelGridOut.InterlockedAdd(node_offset + 4, -1);										
				}				
			}	
}