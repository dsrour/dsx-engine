#include "HR_Globals.h"
#include "HR_SimulationGridUtils.h"

/* BASED ON:
 * A Fast Voxel Traversal Algorithm
 * for
 * Ray Tracing
 * John Amanatides
 * Andrew Woo
 * Dept. of Computer Science
 * University of Toronto
 * Toronto, Ontario, Canada M5S 1A4
 */

Texture2DArray<float4>								gRaysList				: register(t0);
RWByteAddressBuffer									gVoxelGrid				: register(u0); 
RWTexture2D<int>									gRayVoxelIds			: register(u1);
RWTexture2D<float>									gRaysHit				: register(u2);

cbuffer cbConstants : register(b0)
{
	float4			  gSimGridCenterAndVoxelSizePerLength;
	int				  gNumVoxelsPerLength;
	int3			  gDispatchParams;
};

[numthreads(RAY_MARCH_GRP_DIM_X, RAY_MARCH_GRP_DIM_Y, 1)]
void
TraverseGrid(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };	
	int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;
		
	float4 ray_orig_dist = gRaysList[int3(texture_index, 0)];

	if (gRayVoxelIds[texture_index] >= 0) // check that ray is still active (not negative distance)
	{	
		float3 direction = gRaysList[int3(texture_index, 1)].xyz;
		float3 ray_current_pos = ray_orig_dist.xyz + (ray_orig_dist.www * direction);

		// Generate center of voxel as a unit cube relative to the simulation grid bounds
		float world_extent = gSimGridCenterAndVoxelSizePerLength.w * gNumVoxelsPerLength / 2.f;
		float3 world_min = gSimGridCenterAndVoxelSizePerLength.xyz - float3(world_extent, world_extent, world_extent);
		float3 world_max = gSimGridCenterAndVoxelSizePerLength.xyz + float3(world_extent, world_extent, world_extent);

		int i = -1;
		int j = -1;
		int k = -1;
		CoordinatesFromVoxelId(gRayVoxelIds[texture_index], gNumVoxelsPerLength, i, j, k);

		//DEBUG SHIZNIT
	    //if (i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength) != gRayVoxelIds[ray_voxel_id_index])
		//{
		//	gRaysHit[texture_index] = float4(1, 1, 1, 0);
		//	gRayVoxelIds[ray_voxel_id_index] = -1;
		//	return;
		//}

					
		if (i >= 0 && i < gNumVoxelsPerLength && // Need to ensure ray is in simulation grid
			j >= 0 && j < gNumVoxelsPerLength &&
			k >= 0 && k < gNumVoxelsPerLength)
		{
			int stepX = direction.x < 0.f ? -1 : 1;
			int stepY = direction.y < 0.f ? -1 : 1;
			int stepZ = direction.z < 0.f ? -1 : 1;
						
			float voxel_half_size = gSimGridCenterAndVoxelSizePerLength.w / 2.f;
			
			float3 boundary;
			if (stepX < 0)
				boundary.x = world_min.x + (i * gSimGridCenterAndVoxelSizePerLength.w);
			else
				boundary.x = world_min.x + ((i + 1) * gSimGridCenterAndVoxelSizePerLength.w);
			if (stepY < 0)
				boundary.y = world_min.y + (j * gSimGridCenterAndVoxelSizePerLength.w);
			else
				boundary.y = world_min.y + ((j + 1) * gSimGridCenterAndVoxelSizePerLength.w);
			if (stepZ < 0)
				boundary.z = world_min.z + (k * gSimGridCenterAndVoxelSizePerLength.w);
			else
				boundary.z = world_min.z + ((k + 1) * gSimGridCenterAndVoxelSizePerLength.w);
			
			
			float3 tDelta = abs(gSimGridCenterAndVoxelSizePerLength.www / direction);
			float3 tMax = abs((boundary - ray_orig_dist.xyz) / direction);

			//int orig_voxel_id = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);
			//{//DEBUG SHIT	
			//	int voxel_id = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);
			//	if (voxel_id == 13934)
			//		gRaysHit[texture_index] = float4(0, 1, 0, 0);
			//	if (voxel_id == 13935)
			//		gRaysHit[texture_index] = float4(1, 1, 1, 0);
			//}

			bool traversing = true;
			//int3 xyz_favor = { 0, 0, 0 };
			// Traversal
			do
			{
				if (tMax.x < tMax.y)
				{
					if (tMax.x < tMax.z)
					{
						i = i + stepX;
						tMax.x = tMax.x + tDelta.x;		
						//xyz_favor.x = 1;
					}
					else
					{
						k = k + stepZ;
						tMax.z = tMax.z + tDelta.z;						
						//xyz_favor.z = 1;
					}
				}
				else
				{
					if (tMax.y < tMax.z)
					{
						j = j + stepY;
						tMax.y = tMax.y + tDelta.y;						
						//xyz_favor.y = 1;
					}
					else
					{
						k = k + stepZ;
						tMax.z = tMax.z + tDelta.z;	
						//xyz_favor.z = 1;
					}
				}

				// 1st out if ray gets out of bounds of the grid
				if (i < 0 || i >= gNumVoxelsPerLength || // Need to ensure ray is in simulation grid
					j < 0 || j >= gNumVoxelsPerLength ||
					k < 0 || k >= gNumVoxelsPerLength )						
					traversing = false;
				else
				{
					// 2nd out if there is geometry in the current voxel
					int voxel_index = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);
					int node_offset = voxel_index * BYTES_PER_TILE;
					int num_tris = asint(gVoxelGrid.Load(node_offset + 4));

					if (num_tris > 0)
						traversing = false;
				}
			} while (traversing);

			// Write out the voxel id the ray has traversed to
			if (i < 0 || i >= gNumVoxelsPerLength || // Need to ensure ray is in simulation grid
				j < 0 || j >= gNumVoxelsPerLength ||
				k < 0 || k >= gNumVoxelsPerLength)
			{		
				// Make ray inactive
				gRayVoxelIds[texture_index] = -1;
				//gRaysHit[texture_index] = float4(1, 0, 0, 0);	 
			}
			else
			{
				gRayVoxelIds[texture_index] = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);
				//gRaysHit[texture_index] = float4(//xyz_favor, 0);
			}
			
		}
		else // should not get here, but in case it does, mark ray as inactive		
			gRayVoxelIds[texture_index] = -1;
	}	
			
}

