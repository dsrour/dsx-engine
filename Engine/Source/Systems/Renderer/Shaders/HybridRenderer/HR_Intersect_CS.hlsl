#include "HR_Globals.h"
#include "..\\Intersection.h"

cbuffer cbConstants : register(b0)
{
	int2			gViewportSize;
	int2			pad;
};

RWByteAddressBuffer		    						gRayPackets			    : register(u0);
RWByteAddressBuffer									gVoxelGrid				: register(u1); 
RWStructuredBuffer<PacketHeader>					gPacketHeaders			: register(u2);
RWTexture2D<float>									gRaysHit				: register(u3);
RWTexture2DArray<float4>							gRaysList				: register(u4);
RWTexture2D<int>									gRayVoxelIds			: register(u5);
RWBuffer<uint>										gMetrics				: register(u6);

#define NUM_THREADS 512 // NOTE: THIS MUST BE >= MAX_RAYS_PER_PACKET!!!!!
groupshared PacketHeader gHeader;

groupshared int		gNumTris;

groupshared int		gClosestIntersectionTriangle[MAX_RAYS_PER_PACKET]; // triangle index of the closest hit
groupshared int		gActiveRaysIndexList[MAX_RAYS_PER_PACKET];
groupshared float4	gActiveRaysDirections[MAX_RAYS_PER_PACKET];
groupshared float4	gActiveRaysOrigDists[MAX_RAYS_PER_PACKET];
groupshared int		gNumActiveRays;

//groupshared Triangle36 gTris[NUM_THREADS];

[numthreads(NUM_THREADS, 1, 1)]
void
Intersect(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{	

	// Init shared memory triangles, consume a ray packet
	{
		if (GI == 0)
		{
			gHeader = gPacketHeaders[Gid.x]; // TODO:: convert Consume to regular structured buffer	
			gNumTris = asint(gVoxelGrid.Load(gHeader.voxelId * BYTES_PER_TILE + 4));
			gNumActiveRays = 0;
		}
	}
	GroupMemoryBarrierWithGroupSync();

	if (GI < MAX_RAYS_PER_PACKET)
	{
		uint ray_offset = gHeader.offsetLocation + (GI * BYTES_PER_RAY);

		float4 ray_dir = asfloat(gRayPackets.Load4(ray_offset + 16));
									
		// Build active rays list
		if (ray_dir.w >= 0.f)
		{
			int index;
			InterlockedAdd(gNumActiveRays, 1, index);

			gActiveRaysIndexList[index] = GI;
			gActiveRaysDirections[index] = ray_dir;
			gActiveRaysOrigDists[index] = asfloat(gRayPackets.Load4(ray_offset));
			gClosestIntersectionTriangle[index] = -1;
		}
	}	

	GroupMemoryBarrierWithGroupSync();

	if (0 == gNumActiveRays)
		return;

	GroupMemoryBarrierWithGroupSync();


	// Number of iterations needed to complete all intersections
	uint num_intersections = (uint)gNumTris * gNumActiveRays;
	uint num_iterations = ceil(num_intersections / (float)NUM_THREADS);
		
	// TODO: try unrolling loops	
	for (uint i = 0; i < num_iterations; i++)
	{		
		uint ray_index = ((i * NUM_THREADS + GI) / gNumTris);
		if (ray_index < (uint)gNumActiveRays)
		{
			uint tri_index = (i * NUM_THREADS + GI) % gNumTris;
			uint tri_offset = ((gHeader.voxelId * BYTES_PER_TILE) + BYTES_PER_NODE_HEADER) + ((tri_index)* (BYTES_PER_TRIANGLE));

			Triangle36 tri;
			tri.v1 = asfloat(gVoxelGrid.Load3(tri_offset));
			tri.v2 = asfloat(gVoxelGrid.Load3(tri_offset + 12));
			tri.v3 = asfloat(gVoxelGrid.Load3(tri_offset + 24));

			// Intersect thread's triangle with all active rays			
			float4 ray_orig_dist = gActiveRaysOrigDists[ray_index];
			float4 ray_dir = gActiveRaysDirections[ray_index];

			float hit_dist = 0.f;

			// TODO: will need to add synchronization of closest hit for reflection/refraction effects
			int hit = RayTriIntersection(tri.v1, tri.v2, tri.v3, ray_orig_dist.xyz, ray_dir.xyz, hit_dist);
			if (hit != 0 /*&& (hit_dist < ray_orig_dist.w || ray_orig_dist.w == 0.f)*/)			
			{
				gClosestIntersectionTriangle[ray_index] = tri_index;
				gActiveRaysOrigDists[ray_index].w = hit_dist;				
			}
		}
	}
	
	GroupMemoryBarrierWithGroupSync();
		
	if (GI == 0)
	{
		InterlockedAdd(gMetrics[0], num_intersections);
	}
	
	// Record hit data 
	if (GI < (uint)gNumActiveRays)
	{
		if (gClosestIntersectionTriangle[GI] >= 0)
		{
			uint active_ray_index = gActiveRaysIndexList[GI];

			// Need to calculate the index into the rays hit texture
			int2 hit_coords = gHeader.packetRayHitCoords;
			int2 offset = { active_ray_index % RAY_MARCH_GRP_DIM_X, active_ray_index / RAY_MARCH_GRP_DIM_X };
			int2 write_offset = hit_coords + offset;

			// Store ray hit info && new hit distance				
			gRaysHit[write_offset] = 1.f;
			gRaysList[int3(write_offset, 0)] = gActiveRaysOrigDists[GI];

			// Make ray inactive
			gRayVoxelIds[write_offset] = -1;
		}
	}	
}

