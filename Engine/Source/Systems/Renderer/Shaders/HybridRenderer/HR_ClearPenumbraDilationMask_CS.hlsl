#include "HR_Globals.h"

#define SQRT_NUM_THREADS 8

cbuffer cbConstants : register(b0)
{
	int3  gDispatchParams;
	int   pad1;
	int2  gResolution;
	int2  pad2;
};

RWBuffer<float4>					gDilationMask			: register(u0);

[numthreads(SQRT_NUM_THREADS, SQRT_NUM_THREADS, 1)]
void
ClearDilationMask(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };
	int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;

	gDilationMask[ray_voxel_id_index] = float4(0,0,0,0);
}