#include "HR_FR_GFX_Globals.h"
#define GRP_DIM 32

RWTexture2DArray<float4> gRayList		: register(u0); // slice 1 = origin xyz, distance; slice 2 = direction, pad
RWTexture2D<float>		 gRaysHit		: register(u1);
RWTexture2D<int>		 gRayVoxelIds	: register(u2);
RWBuffer<uint>			 gMetrics		: register(u3);



/* This kernel will be ran prior to the forward renderer to reset the RayLinks texture.
 *
 * Dispatch:
 * - The RayLinks is the same size as the render target. 
 *   Hence:
 *   X_GRPS = RT_Width  / 32
 *   Y_GRPS = RT_Height / 32
 * 
 *
 */
[numthreads(GRP_DIM, GRP_DIM, 1)]
void
InitResources(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	//uint flatten_group_index = (Gid.y * gDispatchParams.x + Gid.x);
	//uint thread_index = flatten_group_index * GRP_DIM * GRP_DIM + GI;

	uint3 index = { ((Gid.x * GRP_DIM) + GTid.x), ((Gid.y * GRP_DIM) + GTid.y), 0 };
	gRayList[index] = float4(0.f, 0.f, 0.f, -1.f);
	gRayList[uint3(index.xy, 1)] = float4(0.f, 0.f, 0.f, 0.f);
	gRaysHit[index.xy] = 0.f;
	gRayVoxelIds[index.xy] = -1; // inactive

	if (GI == 0)
		gMetrics[0] = 0;
}