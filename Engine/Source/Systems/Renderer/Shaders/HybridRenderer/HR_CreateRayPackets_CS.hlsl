/* NOTE::
 * Turning on FCX compiler optimizations makes this whole kernel almost 3ms slower! 
 */

#include "HR_Globals.h"
#include "HR_SimulationGridUtils.h"

cbuffer cbConstants : register(b0)
{
	int				maxNumPacketsGenerated;
	int3			gDispatchParams;
};


// Hash function from: http://stackoverflow.com/questions/664014/what-integer-hash-function-are-good-that-accepts-an-integer-hash-key
//                     by "Thomas Mueller"	
//unsigned int Hash(unsigned int x)
//{
//	unsigned int old_range = 4294967294;
//	static unsigned int new_range = MAX_RAYS_PER_PACKET;
//
//	x = ((x >> 16) ^ x) * 0x45d9f3b;
//	x = ((x >> 16) ^ x) * 0x45d9f3b;
//	x = ((x >> 16) ^ x);
//
//	x = (unsigned int)(x / (float)old_range * new_range);
//
//	return x;
//}



Texture2DArray<float4>								gRaysList				: register(t0); // list of rays 
Buffer<int>											gPrefixSum				: register(t1);
Texture2D<int>										gRayVoxelIds			: register(t2);
Buffer<int>											gHashLookup			    : register(t3);
RWByteAddressBuffer		    						gRayPackets			    : register(u0);
AppendStructuredBuffer<PacketHeader>				gPacketHeaders			: register(u1);

groupshared int gPrefix;
groupshared int gLocalPrefix[MAX_RAYS_PER_PACKET];
groupshared int gLocalVoxelId[MAX_RAYS_PER_PACKET];
groupshared int gLocalPrefixCount;


[numthreads(RAY_MARCH_GRP_DIM_X, RAY_MARCH_GRP_DIM_Y, 1)]
void
CreateRayPackets(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	uint flatten_group_index = (Gid.y * gDispatchParams.x + Gid.x);	
	int2 texture_index = { ((Gid.x * RAY_MARCH_GRP_DIM_X) + GTid.x), ((Gid.y * RAY_MARCH_GRP_DIM_Y) + GTid.y) };
	int ray_voxel_id_index = ((gDispatchParams.x * RAY_MARCH_GRP_DIM_X) * (texture_index.y)) + texture_index.x;
			
	gLocalVoxelId[GI] = 0; 

	if (0 == GI)
	{
		gPrefix = gPrefixSum[flatten_group_index];
		gLocalPrefixCount = 0;
	}


	int ray_voxel_id = gRayVoxelIds[texture_index];

	gLocalPrefix[GI] = -1;

	GroupMemoryBarrierWithGroupSync();
			
	int hash = 0;
	if (ray_voxel_id >= 0)
	{
		hash = gHashLookup[ray_voxel_id_index];
		gLocalPrefix[hash] = 1;
		gLocalVoxelId[hash] = ray_voxel_id;
	}
	
	GroupMemoryBarrierWithGroupSync();

	// LOCAL PREFIX SUM //////////////////////////////////////////////////////
	if (gLocalPrefix[GI] == 1)
	{
		InterlockedAdd(gLocalPrefixCount, 1, gLocalPrefix[GI]);
		
		// Create header
		PacketHeader header;
		header.voxelId = gLocalVoxelId[GI];
		header.offsetLocation = (gPrefix + gLocalPrefix[GI]) * (BYTES_PER_RAY * MAX_RAYS_PER_PACKET);
		header.packetRayHitCoords = int2(Gid.x * RAY_MARCH_GRP_DIM_X, Gid.y * RAY_MARCH_GRP_DIM_Y);				
		gPacketHeaders.Append(header);		
	}
	//////////////////////////////////////////////////////////////////////////

	GroupMemoryBarrierWithGroupSync();

	// Write ray into packets.
	float4 ray_orig_dist = gRaysList[int3(texture_index, 0)];
	float3 direction = gRaysList[int3(texture_index, 1)].xyz;
	for (int packet = 0; packet < gLocalPrefixCount; packet++)
	{		
		int offset = ((gPrefix + packet) * (BYTES_PER_RAY * MAX_RAYS_PER_PACKET)) + (GI * BYTES_PER_RAY);

		if (ray_voxel_id >= 0 && packet == gLocalPrefix[hash])
		{
			gRayPackets.Store4(offset, asint(ray_orig_dist));
			gRayPackets.Store4(offset + 16, asint(float4(direction, 0.f)));			
		}
		else
			gRayPackets.Store(offset + 28, asint(-1.f)); // mark w component of the ray direction negative to indicate inactivity within a ray packet
	}
}

