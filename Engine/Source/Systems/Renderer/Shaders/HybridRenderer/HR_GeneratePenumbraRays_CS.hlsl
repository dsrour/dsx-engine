#include "HR_Globals.h"

#define SQRT_NUM_THREADS 32

cbuffer cbConstants : register(b0)
{
	int		gLod;
	int2	gRes;
	int		pad;
	float3	gLightDirection;	
	int		gNumVoxelsPerLength;
	float4	gSimGridCenterAndVoxelSizePerLength;
};

SamplerState				gSampler;

Texture2D<float4>			gPosShadowIntensity		: register(t0);
RWTexture2DArray<float4>	gShadowRaysList	: register(u0);
RWTexture2D<int>			gRayVoxelIds	: register(u1);

[numthreads(SQRT_NUM_THREADS, SQRT_NUM_THREADS, 1)]
void
GeneratePenumbraRays(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	int2 texture_index = { ((Gid.x * SQRT_NUM_THREADS) + GTid.x), ((Gid.y * SQRT_NUM_THREADS) + GTid.y) };
	float2 texture_coords = { (float)texture_index.x / gRes.x, (float)texture_index.y / gRes.y };
	
	if (texture_index.x < gRes.x && texture_index.y < gRes.y)
	{
		float4 pos_intensity = gPosShadowIntensity.SampleLevel(gSampler, texture_coords, gLod - 1);

		if (pos_intensity.w > 0.f)
		{
			/* NOTE:
			 * Even if the light is directional, no such thing exist.
			 * To calculate penumbras, the light must have a physical position and size.
			 * Hence we assume the light is at a constant distance away from the scene's origin (0,0,0).
			 */
			float3  light_pos = { 0.f, 0.f, 0.f };
			light_pos -= -(gLightDirection)* 10000.f;
			float3 direction = (pos_intensity.xyz - light_pos); // pass it non-normalized so we can jitter it

			float4 orig_dist = { pos_intensity.xyz + (float3(0.0001f, 0.0001f, 0.0001f) * normalize(direction)), 0.f };
			gShadowRaysList[int3(texture_index.xy, 0)] = orig_dist;
			gShadowRaysList[int3(texture_index.xy, 1)] = float4(direction, pos_intensity.w);

			float world_extent = gSimGridCenterAndVoxelSizePerLength.w * (gNumVoxelsPerLength / 2.f);
			float3 world_min = gSimGridCenterAndVoxelSizePerLength.xyz - float3(world_extent, world_extent, world_extent);

			int i = (int)floor((orig_dist.x - world_min.x) / gSimGridCenterAndVoxelSizePerLength.w);
			int j = (int)floor((orig_dist.y - world_min.y) / gSimGridCenterAndVoxelSizePerLength.w);
			int k = (int)floor((orig_dist.z - world_min.z) / gSimGridCenterAndVoxelSizePerLength.w);

			if (i >= 0 && i < gNumVoxelsPerLength &&
				j >= 0 && j < gNumVoxelsPerLength &&
				k >= 0 && k < gNumVoxelsPerLength)
			{
				int voxel_id = i + (j * gNumVoxelsPerLength) + (k * gNumVoxelsPerLength * gNumVoxelsPerLength);
					
				// NOTE:	This if check is crucial for edge detection in intermediate pass during penumbra 
				//			dilation as to know which way to dilate (towards the center of the shadow map).
				if (pos_intensity.w > 0.f)
					gRayVoxelIds[texture_index] = voxel_id;
			}					
		}		
	}
}