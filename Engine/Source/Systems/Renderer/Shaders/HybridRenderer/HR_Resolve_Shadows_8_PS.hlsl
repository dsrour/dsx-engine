struct PsIn
{
	float4 Pos		: SV_Position;
	float2 TexCoord : TexCoord;
};

cbuffer cbConstants : register(b0)
{
	int				numSamples;
	int				computedSamples;
	float			lightShadowIntensity;
	int				pad;
};

SamplerState					gRaysHitTextureSampler;
Texture2DArray<float4>			gRaysList : register(t0); // list of rays 
Texture2D<float>				gRaysHitTexture1 : register(t1);
Texture2D<float>				gRaysHitTexture2 : register(t2);
Texture2D<float>				gRaysHitTexture3 : register(t3);
Texture2D<float>				gRaysHitTexture4 : register(t4);
Texture2D<float>				gRaysHitTexture5 : register(t5);
Texture2D<float>				gRaysHitTexture6 : register(t6);
Texture2D<float>				gRaysHitTexture7 : register(t7);
Texture2D<float>				gRaysHitTexture8 : register(t8);

float4
PsResolve(PsIn In) : SV_Target
{
	// See if any sample hit geometry
	float hit = 0;
	if (any(gRaysHitTexture1.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture2.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture3.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture4.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture5.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture6.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture7.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	if (any(gRaysHitTexture8.Sample(gRaysHitTextureSampler, In.TexCoord)))
		hit += lightShadowIntensity;
	hit /= 8.f;

	//float4 ray_pos_dist = gRaysList.Sample(gRaysHitTextureSampler, float3(In.TexCoord, 0));
	float4 ray_dir_sh_int = gRaysList.Sample(gRaysHitTextureSampler, float3(In.TexCoord, 1));

	float3 shadow_intensity = ray_dir_sh_int.w;

	if (hit > 0)
	{
		//shadow_intensity < 0 means the pixel is on the penumbra
		if (shadow_intensity.x < 0)
		{
			if (computedSamples != 7) // not all 4 samples computed, don't display effect
			{
				float3 entsity = lightShadowIntensity;
					return float4(1 - entsity, 1);
			}
			else
			{
				shadow_intensity = hit;
			}
		}

		return float4(1 - shadow_intensity, 1);
	}
	else
	{
		//shadow_intensity < 0 means the pixel is on the penumbra
		if (shadow_intensity.x < 0)
		{
			return float4(1, 1, 1, 1);
			float3 entsity = lightShadowIntensity;
				return float4(1 - entsity, 1);
		}
		else
			return float4(1 - shadow_intensity, 1);
	}
}