/*
 * The Pack and Unpack functions need to abide to IEEE float standards.
 * More info can be found on Wolfgang Engel's blog (http://diaryofagraphicsprogrammer.blogspot.com/2009/10/bitmasks-packing-data-into-fp-render.html)
 */
float4
UnpackFloat4FromUint(uint packed)
{
	float4 output;
	output.x = (float)(packed & 0x000000ff);				output.x /= 255.f;
	output.y = (float)((packed >> 8) & 0x000000ff);			output.y /= 255.f;
	output.z = (float)((packed >> 16) & 0x000000ff);		output.z /= 255.f;
	output.w = (float)(((packed >> 24) & 0x000000ff) - 1);	output.w /= 253.f;
	return output;
}

uint
PackFloat4ToUint(float4 unpacked)
{
	unpacked = saturate(unpacked);
	uint x = (uint)floor(unpacked.x * 255 + 0.5);
	uint y = (uint)floor(unpacked.y * 255 + 0.5);		y <<= 8;
	uint z = (uint)floor(unpacked.z * 255 + 0.5);		z <<= 16;
	uint w = (uint)floor(unpacked.w * 253 + 1.5);		w <<= 24;
	return (x | y | z | w);
}