#include "..\\Texturing.h"
#include "FurRenderer_GFX_Globals.h"

SamplerState gLayerMapSampler : register(s3);
Texture2D<float> gLayerMap : register(t3);

float4 PS( VertexOut pixelIn ) : SV_Target
{
	pixelIn.NormalW = normalize(pixelIn.NormalW);
		
	float3 to_eye_w = gEyePosW - pixelIn.PosW;
	float3 dist_to_eye = length(to_eye_w);
	to_eye_w /= dist_to_eye;
	
	// Diffuse Map
	float4 diffuse_map_color = SampleDiffuseMap(pixelIn.TexCoord);
		
	// Lighting
	uint num_lights = min(gNumLights, MAX_LIGHTS);

	float4 lit_color = diffuse_map_color;
	clip(lit_color.w - 0.01f);

	if (num_lights > 0)
	{
		float4 final_ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 final_spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

		[unroll]
		for (uint i = 0; i < num_lights; i++)
		{
			float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
			float4 spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);
				
			if (DIRECTIONAL_LIGHT == gLights[i].lightType)
				ComputeDirectionalLight(gMaterial, gLights[i].directionalLight, pixelIn.NormalW, to_eye_w, 
										ambient, diffuse, spec);

			if (POINT_LIGHT == gLights[i].lightType)
				ComputePointLight(gMaterial, gLights[i].pointLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w, 
								  ambient, diffuse, spec);			

			if (SPOT_LIGHT == gLights[i].lightType)
				ComputeSpotLight(gMaterial, gLights[i].spotLight, pixelIn.PosW, pixelIn.NormalW, to_eye_w, 
								  ambient, diffuse, spec);
	
			final_ambient += ambient;
			final_diffuse += diffuse;
			final_spec    += spec;
		}

		// Modulate with late add
		lit_color = diffuse_map_color*(final_ambient + final_diffuse) + final_spec;

		// Common to take alpha from diffuse material
		lit_color.a = diffuse_map_color.a * gMaterial.diffuse.a;
	}	
	
	// Uncomment to see world normals
	//lit_color.r = pixelIn.NormalW.r;
	//lit_color.g = pixelIn.NormalW.g;
	//lit_color.b = pixelIn.NormalW.b;
	//lit_color.a = 1;

	// Uncomment to see world tangents
	//if (useBumpMap) 
	//{
	//	lit_color.r = pixelIn.TangentW.r;
	//	lit_color.g = pixelIn.TangentW.g;
	//	lit_color.b = pixelIn.TangentW.b;
	//	lit_color.a = 1;
	//}

	// Final = FogFactor * lit_color + (1.0 - FogFactor) * FogColor
	float fog_factor = ComputeFog(gFog, distance(gEyePosW, pixelIn.PosW));
	lit_color = fog_factor * lit_color + (1.f - fog_factor) * gFog.fogColor;

	// Alpha	
	float alpha = gLayerMap.Sample(gLayerMapSampler, pixelIn.TexCoord);
	lit_color.w = alpha;

	return lit_color;
}