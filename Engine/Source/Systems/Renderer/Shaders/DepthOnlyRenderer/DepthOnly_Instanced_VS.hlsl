struct InstancedVertexIn
{
	float3		PosL : POSITION;
	float3		NormalL : NORMAL;
	float2		TexCoord : TEXCOORD;
	float3		TangentL : TANGENT;
	float4x4	World : WORLD;
	float4		Color : COLOR;
	uint		InstanceId : SV_InstanceID;
};

cbuffer cb : register(b0)
{
	float4x4 gWorld;
	float4x4 gViewProj;
};

float4 DepthOnlyInstancedVS(InstancedVertexIn vertexIn) : SV_Position
{
	float3 pos_w = mul(float4(vertexIn.PosL, 1.0f), vertexIn.World).xyz;
	return  mul(float4(pos_w, 1.0f), gViewProj);
}