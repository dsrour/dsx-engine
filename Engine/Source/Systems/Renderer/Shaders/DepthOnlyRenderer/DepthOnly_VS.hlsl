struct VertexIn 
{
	float3 PosL : POSITION;	
};

cbuffer cb : register(b0)
{
	float4x4 gWorld;
	float4x4 gViewProj;
};

float4 DepthOnlyVS(VertexIn vertexIn) : SV_Position
{		
	float3 PosW = mul(float4(vertexIn.PosL, 1.0f), gWorld).xyz;	
	return mul(float4(PosW, 1.0f), gViewProj);
}