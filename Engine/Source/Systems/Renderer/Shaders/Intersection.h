/*
 * Ray - Triangle intersection ported from M�ller�Trumbore work:
 * http://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
 */

#define EPSILON 0.0001f
#pragma warning(disable: 4000) // "use of potentially uninitialized variable (RayTriIntersection)" on return 0 statements
int
RayTriIntersection(in float3 v1, in float3 v2, in float3 v3, in float3 O, in float3 D, out float distOut)
{
	distOut = 0.f;
	float3 e1, e2;
	float3 P, Q, T;
	float det, inv_det, u, v;
	float t;

	//Find vectors for two edges sharing V1
	e1 = v2 - v1;
	e2 = v3 - v1;

	//Begin calculating determinant - also used to calculate u parameter
	P = cross(D, e2);

	//if determinant is near zero, ray lies in plane of triangle
	det = dot(e1, P);

	//NOT CULLING
	if (det > -EPSILON && det < EPSILON) 
		return 0;

	inv_det = 1.f / det;

	//calculate distance from V1 to ray origin
	T = O - v1;

	//Calculate u parameter and test bound
	u = dot(T, P) * inv_det;

	//The intersection lies outside of the triangle
	if (u < 0.f || u > 1.f)
		return 0;

	//Prepare to test v parameter
	Q = cross(T, e1);

	//Calculate V parameter and test bound
	v = dot(D, Q) * inv_det;

	//The intersection lies outside of the triangle
	if (v < 0.f || u + v  > 1.f)
		return 0;

	t = dot(e2, Q) * inv_det;

	if (t > EPSILON) 
	{ //ray intersection
		distOut = t;
		return 1;
	}

	// No hit, no win
	return 0;
}
#pragma warning(enable: 4000)





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 * AABB - Triangle intersection ported from Tomas Akenine-M�ller's work:
 * http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox3.txt
 */

#define X 0
#define Y 1
#define Z 2

void 
FindMinMax(in float x0, in float x1, in float x2, out float min, out float max)
{
	min = max = x0;
	if (x1 < min) 
		min = x1; 
	if (x1 > max) 
		max = x1; 
	if (x2 < min) 
		min = x2; 
	if (x2 > max) 
		max = x2;
}

int
PlaneBoxOverlap(in float3 normal, in float3 vert, in float3 maxbox)
{
	int		q;
	float3	vmin;
	float3	vmax;
	float	v;

	[unroll]
	for (q = X; q <= Z; q++)
	{
		v = vert[q];				
		if (normal[q] > 0.0f)
		{
			vmin[q] = -maxbox[q] - v;
			vmax[q] = maxbox[q] - v;
		}
		else
		{
			vmin[q] = maxbox[q] - v;	
			vmax[q] = -maxbox[q] - v;
		}
	}

	if ( dot(normal, vmin) > 0.0f) 
		return 0;	
	else if ( dot(normal, vmax) >= 0.0f) 
		return 1;	
	else
		return 0;
}

// X-tests ////////////////////////////////////////////////
int
AxisTestX01(
	in float a, in float b, 
	in float fa, in float fb, 
	in float3 boxhalfsize,
	in float3 v0, in float3 v2,
	out float p0, out float p2,
	out float min, out float max, out float rad)
{
	p0 = a*v0[Y] - b*v0[Z];
	p2 = a*v2[Y] - b*v2[Z];			       	   
	
	if (p0<p2) 
	{ 
		min = p0; 
		max = p2; 
	}
	else
	{ 
		min = p2; 
		max = p0; 
	} 

	rad = fa * boxhalfsize[Y] + fb * boxhalfsize[Z];   
	
	if (min > rad || max < -rad) 
		return 0;
	
	return 1;
}

int
AxisTestX2(
	in float a, in float b,
	in float fa, in float fb,
	in float3 boxhalfsize,
	in float3 v0, in float3 v1,
	out float p0, out float p1,
	out float min, out float max, out float rad)
{
	p0 = a*v0[Y] - b*v0[Z];		
	p1 = a*v1[Y] - b*v1[Z];			       	   
	
	if (p0 < p1) 
	{ 
		min = p0; 
		max = p1; 
	}
	else 
	{ 
		min = p1; 
		max = p0; 
	} 
	
	rad = fa * boxhalfsize[Y] + fb * boxhalfsize[Z];   

	if (min > rad || max < -rad) 
		return 0;

	return 1;
}
///////////////////////////////////////////////////////////

// Y-tests ////////////////////////////////////////////////
int
AxisTestY02(
	in float a, in float b,
	in float fa, in float fb,
	in float3 boxhalfsize,
	in float3 v0, in float3 v2,
	out float p0, out float p2,
	out float min, out float max, out float rad)
{
	p0 = -a*v0[X] + b*v0[Z];
	p2 = -a*v2[X] + b*v2[Z];

	if (p0 < p2) 
	{ 
		min = p0; 
		max = p2; 
	}
	else 
	{ 
		min = p2; 
		max = p0; 
	}
	
	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Z];   

	if (min > rad || max < -rad)
		return 0;

	return 1;
}

int
AxisTestY1(
	in float a, in float b,
	in float fa, in float fb,
	in float3 boxhalfsize,
	in float3 v0, in float3 v1,
	out float p0, out float p1,
	out float min, out float max, out float rad)
{
	p0 = -a*v0[X] + b*v0[Z];	
	p1 = -a*v1[X] + b*v1[Z];	     	       	   

	if (p0 < p1) 
	{ 
		min = p0; 
		max = p1; 
	}
	else 
	{ 
		min = p1; 
		max = p0; 
	} 

	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Z];   

	if (min > rad || max < -rad)
		return 0;

	return 1;
}
///////////////////////////////////////////////////////////

// Z-tests ////////////////////////////////////////////////
int
AxisTestZ12(
	in float a, in float b,
	in float fa, in float fb,
	in float3 boxhalfsize,
	in float3 v1, in float3 v2,
	out float p1, out float p2,
	out float min, out float max, out float rad)
{

	p1 = a*v1[X] - b*v1[Y];			           
	p2 = a*v2[X] - b*v2[Y];			       	   

	if (p2 < p1) 
	{
		min = p2; 
		max = p1; 
	}
	else 
	{ 
		min = p1; 
		max = p2; 
	}

	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Y];   

	if (min > rad || max < -rad)
		return 0;

	return 1;
}

int
AxisTestZ0(
	in float a, in float b,
	in float fa, in float fb,
	in float3 boxhalfsize,
	in float3 v0, in float3 v1,
	out float p0, out float p1,
	out float min, out float max, out float rad)
{
	p0 = a*v0[X] - b*v0[Y];				   
	p1 = a*v1[X] - b*v1[Y];			           

	if (p0 < p1) 
	{ 
		min = p0; 
		max = p1; 
	}
	else 
	{ 
		min = p1; 
		max = p0; 
	} 

	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Y];   
			
	if (min > rad || max < -rad)
		return 0;

	return 1;
}
///////////////////////////////////////////////////////////


int 
TriBoxOverlap(in float3 boxcenter, in float3 boxhalfsize, in float3 triverts[3])
{
	/*    use separating axis theorem to test overlap between triangle and box */
	/*    need to test for overlap in these directions: */
	/*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
	/*       we do not even need to test these) */
	/*    2) normal of the triangle */
	/*    3) crossproduct(edge from tri, {x,y,z}-directin) */
	/*       this gives 3x3=9 more tests */

	float3 v0, v1, v2;
	//   float axis[3];
	float min, max, p0, p1, p2, rad, fex, fey, fez;		
	float3 normal, e0, e1, e2;
	
	v0 = triverts[0] - boxcenter;
	v1 = triverts[1] - boxcenter;
	v2 = triverts[2] - boxcenter;
	
	/* compute triangle edges */
	e0 = v1 - v0;      /* tri edge 0 */
	e1 = v2 - v1;      /* tri edge 0 */
	e2 = v0 - v2;      /* tri edge 0 */
	
	/* Bullet 3:  */
	/*  test the 9 tests first (this was faster) */
	fex = abs(e0[X]);
	fey = abs(e0[Y]);
	fez = abs(e0[Z]);

	AxisTestX01(e0[Z], e0[Y], fez, fey, boxhalfsize, v0, v2, p0, p2, min, max, rad);
	AxisTestY02(e0[Z], e0[X], fez, fex, boxhalfsize, v0, v2, p0, p2, min, max, rad);
	AxisTestZ12(e0[Y], e0[X], fey, fex, boxhalfsize, v1, v2, p1, p2, min, max, rad);



	fex = abs(e1[X]);
	fey = abs(e1[Y]);
	fez = abs(e1[Z]);

	AxisTestX01(e1[Z], e1[Y], fez, fey, boxhalfsize, v0, v2, p0, p2, min, max, rad);
	AxisTestY02(e1[Z], e1[X], fez, fex, boxhalfsize, v0, v2, p0, p2, min, max, rad);
	AxisTestZ0(e1[Y], e1[X], fey, fex, boxhalfsize, v0, v1, p0, p1, min, max, rad);



	fex = abs(e2[X]);
	fey = abs(e2[Y]);
	fez = abs(e2[Z]);

	AxisTestX2(e2[Z], e2[Y], fez, fey, boxhalfsize, v0, v1, p0, p1, min, max, rad);
	AxisTestY1(e2[Z], e2[X], fez, fex, boxhalfsize, v0, v1, p0, p1, min, max, rad);
	AxisTestZ12(e2[Y], e2[X], fey, fex, boxhalfsize, v1, v2, p1, p2, min, max, rad);



	/* Bullet 1: */
	/*  first test overlap in the {x,y,z}-directions */
	/*  find min, max of the triangle each direction, and test for overlap in */
	/*  that direction -- this is equivalent to testing a minimal AABB around */
	/*  the triangle against the AABB */

	/* test in X-direction */
	FindMinMax(v0[X], v1[X], v2[X], min, max);
	if (min > boxhalfsize[X] || max < -boxhalfsize[X]) 
		return 0;
	else 
	{
		/* test in Y-direction */
		FindMinMax(v0[Y], v1[Y], v2[Y], min, max);
		if (min > boxhalfsize[Y] || max < -boxhalfsize[Y])
			return 0;
		else
		{
			/* test in Z-direction */
			FindMinMax(v0[Z], v1[Z], v2[Z], min, max);
			if (min > boxhalfsize[Z] || max < -boxhalfsize[Z])
				return 0;
			else
			{
				/* Bullet 2: */
				/*  test if the box intersects the plane of the triangle */
				/*  compute plane equation of triangle: normal*x+d=0 */
				normal = cross(e0, e1);
				return PlaneBoxOverlap(normal, v0, boxhalfsize);
			}
		}		
	}
	
}
