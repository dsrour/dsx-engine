// Defines needed by both renderer & shaders

#define MAX_LIGHTS 16
#define NUM_SHADOW_CASCADES 4 // 16 texture arrays with each 4 textures

////////////////////////////////////////////////////////////////////////////////////////////////
#define DEPTH_ONLY_VS_FILE								L"DepthOnly_VS"
#define DEPTH_ONLY_INSTANCED_VS_FILE					L"DepthOnly_Instanced_VS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define DEPTH_REDUCTION_INIT_CS_FILE					L"Depth_Reduce_Init_CS"
#define DEPTH_REDUCTION_CS_FILE							L"Depth_Reduce_CS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define POINT_CLOUD_TECH								"PointCloudTech"
#define POINT_CLOUD_VS_FILE								L"PointCloud_VS"
#define POINT_CLOUD_PS_FILE								L"PointCloud_PS"
#define POINT_CLOUD_RESOLVE_TECH						"PointCloudResolveTech"
#define POINT_CLOUD_RESOLVE_VS_FILE						L"PointCloudResolve_VS"
#define POINT_CLOUD_RESOLVE_PS_FILE						L"PointCloudResolve_PS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define FUR_TECH										"FurTech"
#define FUR_VS_FILE										L"FurRenderer_VS"
#define FUR_PS_FILE										L"FurRenderer_PS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define FR_FLAT_TECH									"FlatTech"
#define FR_FLAT_VS_FILE									L"FR_Flat_VS"
#define FR_FLAT_PS_FILE									L"FR_Flat_PS"

#define FR_DEFAULT_TECH									"DefaultTech"
#define FR_DIFFUSE_TECH									"DiffuseTech"
#define FR_BUMP_TECH									"BumpTech"
#define FR_DIFFUSE_BUMP_TECH							"DiffuseBumpTech"
#define FR_VS_FILE										L"FR_VS"
#define FR_DEFAULT_PS_FILE								L"FR_Default_PS"
#define FR_DIFFUSE_PS_FILE								L"FR_Diff_PS"
#define FR_BUMP_PS_FILE									L"FR_Bump_PS"
#define FR_DIFFUSE_BUMP_PS_FILE							L"FR_Diff_Bump_PS"

#define FR_INSTANCED_DEFAULT_TECH						"InstancedDefaultTech"
#define FR_INSTANCED_DIFFUSE_TECH						"InstancedDiffuseTech"
#define FR_INSTANCED_BUMP_TECH							"InstancedBumpTech"
#define FR_INSTANCED_DIFFUSE_BUMP_TECH					"InstancedDiffuseBumpTech"
#define FR_INSTANCED_VS_FILE							L"FR_Instanced_VS"
#define FR_INSTANCED_DEFAULT_PS_FILE					L"FR_Instanced_Default_PS"
#define FR_INSTANCED_DIFFUSE_PS_FILE					L"FR_Instanced_Diff_PS"
#define FR_INSTANCED_BUMP_PS_FILE						L"FR_Instanced_Bump_PS"
#define FR_INSTANCED_DIFFUSE_BUMP_PS_FILE				L"FR_Instanced_Diff_Bump_PS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define SKY_TECH										"SkyTech"
#define SKY_VS_FILE										L"SkyRenderer_VS"
#define SKY_PS_FILE										L"SkyRenderer_PS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define PARTICLE_SYSTEM_INIT_TECH						"ParticleSysInitTech"
#define PARTICLE_SYSTEM_PHYSICS_TECH					"ParticleSysPhysicsTech"
#define PARTICLE_SYSTEM_RENDER_TECH						"ParticleSysRenderTech"
#define PARTICLE_SYSTEM_INIT_CS_FILE					L"Particle_Init_CS"
#define PARTICLE_SYSTEM_PHYSICS_CS_FILE					L"Particle_Physics_CS"
#define PARTICLE_SYSTEM_VS_FILE							L"Particle_VS"
#define PARTICLE_SYSTEM_GS_FILE							L"Particle_GS"
#define PARTICLE_SYSTEM_PS_FILE							L"Particle_PS"
////////////////////////////////////////////////////////////////////////////////////////////////
#define DEBUG_VOXEL_GRID_TECH							"DebugVoxelGrid"

// Reusing the tech names since the forward renderer used in the hybrid pipeline is similar
// to the original forward renderer.
//#define FR_DEFAULT_TECH								"DefaultTech"
//#define FR_DIFFUSE_TECH								"DiffuseTech"
//#define FR_BUMP_TECH									"BumpTech"
//#define FR_DIFFUSE_BUMP_TECH							"DiffuseBumpTech"
#define HR_FR_VS_FILE									L"HR_FR_VS"
#define HR_FR_DEFAULT_PS_FILE							L"HR_FR_Default_PS"
#define HR_FR_DIFFUSE_PS_FILE							L"HR_FR_Diff_PS"
#define HR_FR_BUMP_PS_FILE								L"HR_FR_Bump_PS"
#define HR_FR_DIFFUSE_BUMP_PS_FILE						L"HR_FR_Diff_Bump_PS"
#define HR_FR_INIT_RESOURCES_TECH						"HybridRendererInitResources"
#define HR_FR_INIT_RESOURCES_CS_FILE					L"HR_FR_InitResources_CS"

#define HR_INIT_GRID_TECH								"HybridRendererInitGrid"
#define HR_INIT_GRID_CS_FILE							L"HR_InitGrid_CS"
#define HR_CONSTRUCT_VOXEL_GRID_TECH					"HybridRendererConstructVoxelGrid"
#define HR_CONSTRUCT_VOXEL_GRID_CS_FILE					L"HR_ConstructVoxelGrid_CS"

#define HR_RAY_MARCH_GENERATE_RAY_PACKET_COUNTS_FILE	L"HR_GenerateRayPacketCounts_CS"
#define HR_RAY_MARCH_GENERATE_RAY_PACKET_COUNTS_TECH	"HybridRendererGenerateRayPacketCounts"
#define HR_RAY_MARCH_CREATE_RAY_PACKETS_FILE			L"HR_CreateRayPackets_CS"
#define HR_RAY_MARCH_CREATE_RAY_PACKETS_TECH			"HybridRendererCreateRayPackets"
#define HR_RAY_MARCH_INTERSECT_FILE						L"HR_Intersect_CS"
#define HR_RAY_MARCH_INTERSECT_TECH						"HybridRendererIntersect"
#define HR_RAY_MARCH_TRAVERSE_GRID_FILE					L"HR_TraverseGrid_CS"
#define HR_RAY_MARCH_TRAVERSE_GRID_TECH					"HybridRendererTraverseGrid"

#define HR_FULL_SCREEN_TRI_VS_FILE						L"HR_FullScreenTri_VS"

#define HR_GENERATE_PENUMBRA_RAYS_CS_FILE				L"HR_GeneratePenumbraRays_CS"
#define HR_GENERATE_PENUMBRA_RAYS_TECH					"GeneratePenumbraRAYS"
#define HR_CLEAR_PENUMBRA_DILATION_MASK_CS_FILE			L"HR_ClearPenumbraDilationMask_CS"
#define HR_CLEAR_PENUMBRA_DILATION_MASK_TECH			"ClearPenumbraDilation"
#define HR_GENERATE_PENUMBRA_DILATION_MASK_CS_FILE		L"HR_GeneratePenumbraDilationMask_CS"
#define HR_GENERATE_PENUMBRA_DILATION_MASK_TECH			"GeneratePenumbraDilation"
#define HR_APPLY_PENUMBRA_DILATION_MASK_CS_FILE			L"HR_ApplyPenumbraDilationMask_CS"
#define HR_APPLY_PENUMBRA_DILATION_MASK_TECH			"ApplyPenumbraDilation"
#define HR_RESOLVE_SHADOWS_PS_4_FILE					L"HR_Resolve_Shadows_4_PS"
#define HR_RESOLVE_SHADOWS_PS_8_FILE					L"HR_Resolve_Shadows_8_PS"
#define HR_RESOLVE_SHADOWS_PS_16_FILE					L"HR_Resolve_Shadows_16_PS"
#define HR_RESOLVE_SHADOWS_TECH							"HybridRendererResolveShadows"