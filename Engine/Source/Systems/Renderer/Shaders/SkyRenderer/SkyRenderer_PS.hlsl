#include "..\\Texturing.h"
#include "SkyRenderer_GFX_Globals.h"

float4 PS(VertexOut pin) : SV_Target
{	
	return SampleCubeMap(pin.PosL);
}
