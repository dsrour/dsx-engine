#include "SkyRenderer_GFX_Globals.h"

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
		
	float3 pos_w = mul( float4(vin.PosL, 1.0f), gWorld ).xyz;
	vout.PosH = mul( float4(pos_w, 1.0f), gViewProj ).xyww;
	vout.PosL = vin.PosL;

	return vout;
}