cbuffer cbPerFrame : register(b0)
{
	float4x4 gWorld;
	float4x4 gViewProj;
};

struct VertexIn
{
	float3 PosL : POSITION;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosL : POSITION;
};