

////////////////////////////////////////////////////////////////
// Fog Formula
/////////////////////////////////////////////////////////////////
// Calculate fog based on http://msdn.microsoft.com/en-us/library/bb173401(VS.85).aspx
// in - fogType :: 0:linearFog 1:exp1Fog 2:exp2Fog
// in - fogDensity :: 0.0 to 1.0
// in - fogDistance :: depth, or the distance from the viewpoint to the vertex/pixel
// in - fogLinStart :: start is the distance at which fog effects begin
// in - fogLinEnd :: end is the distance at which fog effects no longer increase
// out - result 0.0 to 1.0
//
//
// Credit: "jeepee" (http://forum.quest3d.com/index.php?topic=67119.0)
/////////////////////////////////////////////////////////////////

struct Fog
{
	float	fogDensity;	
	float	fogLinStart;
	float	fogLinEnd;
	int		fogType; // If -, no fog
	float4  fogColor;
};

float 
ComputeFog(Fog fog, float fogDistance)
{
	float factor = 1.f;

	if (0 == fog.fogType)
	{
		// Return linear fog
		factor = (fog.fogLinEnd - fogDistance) / (fog.fogLinEnd - fog.fogLinStart);
	}
	else if (fog.fogType >= 1)
	{
		uint uint_exp = fog.fogType; // to remove warnings about negative pow() params
		// Return exponential fog
		factor = 1 / pow(2.71828, pow(abs(fogDistance * fog.fogDensity), uint_exp));
	}

	return saturate(factor);
}