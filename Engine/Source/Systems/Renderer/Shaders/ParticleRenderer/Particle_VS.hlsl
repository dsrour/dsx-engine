#include "Particle_GFX_Globals.h"

GsIn
VertexMain(uint VertexID : SV_VertexID)
{
	GsIn out_;

	out_.Pos = gPosAndTimeReadOnly.Load(VertexID);
	out_.TexCoord = float2(0.f, 0.f);
	out_.Color = float3(0.f, 0.f, 0.f);


	return out_;
}