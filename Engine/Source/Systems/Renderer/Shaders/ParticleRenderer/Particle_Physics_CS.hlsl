#include "Particle_CS_Globals.h"


[numthreads(GRP_DIM, 1, 1)]
void
Physics(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	uint particle_index = DTid.x;

	float4 cur_pos = PositionAndTime[particle_index]; // w has current time
		float3 cur_dir = Direction[particle_index];

		// Increate life time
		cur_pos.w += timeDelta;

	// Did the particle just die?
	if (cur_pos.w >= lifeTime)
	{
		cur_dir.xyz = spawnDirection;
		cur_pos.xyz = originPos;
		cur_pos.w = -respawnDelay + (cur_pos.w - lifeTime);
	}
	else if (cur_pos.w >= 0.f)
	{
		// Apply gravity
		cur_dir.y -= gravity * timeDelta;

		// Update position
		cur_pos.xyz += cur_dir.xyz * timeDelta;

		// Intersect & bounce off bounds.
		float3 max_bounds_xyz = boundsCenter + (boundsExtents / 2.f);
			float3 min_bounds_xyz = boundsCenter - (boundsExtents / 2.f);
			cur_dir.xyz = (cur_pos.xyz > max_bounds_xyz) ? -0.8 * abs(cur_dir.xyz) : cur_dir.xyz;
		cur_dir.xyz = (cur_pos.xyz < min_bounds_xyz) ? 0.8 * abs(cur_dir.xyz) : cur_dir.xyz;

		cur_pos.xyz = clamp(cur_pos.xyz, min_bounds_xyz, max_bounds_xyz);
	}
	else // Lifetime is negative... just update the spawn direction in case it has changed
	{
		cur_dir.xyz = spawnDirection;
		cur_pos.xyz = originPos;
	}

	PositionAndTime[particle_index] = cur_pos;
	Direction[particle_index] = cur_dir;
}
