#include "Particle_CS_Globals.h"


[numthreads(GRP_DIM, 1, 1)]
void 
Init(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	uint particle_index = DTid.x;

	PositionAndTime[particle_index] = float4(originPos, -respawnDelay + -(spawnTimeOffset * particle_index));
	Direction[particle_index] = spawnDirection;
}


