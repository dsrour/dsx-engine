#include "Particle_GFX_Globals.h"

float3
PsMain(PsIn In) : SV_Target
{
	float3 base = gParticleTexture.Sample(gParticleTextureSampler, In.TexCoord).rgb;
	return base * In.Color;
}
