cbuffer cbPerFrame : register(b0)
{
	float4x4 mvp			: packoffset(c0);
	float3   billboardDx	: packoffset(c4);
	float	 lifeTime_		: packoffset(c4.w);
	float3   billboardDy	: packoffset(c5);
	float    padding1		: packoffset(c5.w);
	float3	 colorSpawn		: packoffset(c6);
	float    padding2		: packoffset(c6.w);
	float3	 colorDie		: packoffset(c7);
	float    padding3		: packoffset(c7.w);
};

struct GsIn
{
	float4 Pos		: POSITION;
	float2 TexCoord	: TexCoord;
	float3 Color	: Color;
};

struct PsIn
{
	float4 Pos		: SV_Position;
	float2 TexCoord : TexCoord;
	float3 Color    : Color;
};

SamplerState gParticleTextureSampler : register(s0);
StructuredBuffer<float4> gPosAndTimeReadOnly : register(t0);
Texture2D gParticleTexture : register(t1);