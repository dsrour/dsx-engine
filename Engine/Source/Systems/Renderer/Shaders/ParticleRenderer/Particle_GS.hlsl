#include "Particle_GFX_Globals.h"

[maxvertexcount(4)]
void
GeometryMain(point GsIn In[1], inout TriangleStream<PsIn> Stream)
{
	PsIn out_;
	if (In[0].Pos.w > 0.f)
		out_.Color = lerp(colorSpawn, colorDie, In[0].Pos.w / lifeTime_);
	else
		out_.Color = float3(0.f, 0.f, 0.f);

	// Output a billboarded quad
	float3 dx = billboardDx / 2.f;
	float3 dy = billboardDy / 2.f;
	out_.Pos = mul(float4(In[0].Pos.xyz - dx - dy, 1.0), mvp);
	out_.TexCoord = float2(0.f, 0.f);
	Stream.Append(out_);

	out_.Pos = mul(float4(In[0].Pos.xyz + dx - dy, 1.0), mvp);
	out_.TexCoord = float2(1.f, 0.f);
	Stream.Append(out_);

	out_.Pos = mul(float4(In[0].Pos.xyz - dx + dy, 1.0), mvp);
	out_.TexCoord = float2(0.f, 1.f);
	Stream.Append(out_);

	out_.Pos = mul(float4(In[0].Pos.xyz + dx + dy, 1.0), mvp);
	out_.TexCoord = float2(1.f, 1.f);
	Stream.Append(out_);
}