#define GRP_DIM 32

RWStructuredBuffer<float4> PositionAndTime;
RWStructuredBuffer<float3> Direction;

cbuffer cbParticleSysSettings : register(b0)
{	 
	float3 originPos		: packoffset(c0);		// where the particles are being emitted from.
	float  spawnTimeOffset	: packoffset(c0.w);		// Time difference between consecutive spawning particles.  The smaller this is, the more of a stream appearance there'll be.
	float3 spawnDirection	: packoffset(c1);		// Direction to spawn particles towards.
	float  respawnDelay		: packoffset(c1.w);		// Death duration.
	float3 boundsCenter		: packoffset(c2);		// Bounds center.
	float  timeDelta		: packoffset(c2.w);		// Time delta for simulation.
	float3 boundsExtents	: packoffset(c3);		// Bounds extents.
	float  lifeTime			: packoffset(c3.w);		// How long a particle lives before disappearing.
	float  gravity			: packoffset(c4);		// Gravity.
	float3 padding			: packoffset(c4.y);
};