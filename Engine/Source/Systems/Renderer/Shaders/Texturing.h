#include "ShaderDefines.h"

SamplerState gDiffuseMapSampler : register(s0);
Texture2D gDiffuseMap : register(t0);

SamplerState gNormalMapSampler : register(s1);
Texture2D gNormalMap : register(t1);

SamplerState gCubeMapSampler : register(s2);
TextureCube gCubeMap : register(t2);

SamplerComparisonState gShadowMapCompSamplerPCF : register(s3);
SamplerState gShadowMapSamplerPCF : register(s4);
Texture2DArray<float> gShadowMap[NUM_SHADOW_CASCADES] : register(t3);

float4 
SampleDiffuseMap(float2 texCoord)
{
	return gDiffuseMap.Sample(gDiffuseMapSampler, texCoord);
};

float3 
SampleNormalMap(float2 texCoord)
{
	return gNormalMap.Sample(gNormalMapSampler, texCoord).xyz;
};

float4 
SampleCubeMap(float3 texCoord)
{
	return gCubeMap.Sample(gCubeMapSampler, texCoord);
}

float2 
ComputeReceiverPlaneDepthBias(float3 texCoordDX, float3 texCoordDY)
{
	float2 bias_UV;
	bias_UV.x = texCoordDY.y * texCoordDX.z - texCoordDX.y * texCoordDY.z;
	bias_UV.y = texCoordDX.x * texCoordDY.z - texCoordDY.x * texCoordDX.z;	
	bias_UV *= 1.0f / ((texCoordDX.x * texCoordDY.y) - (texCoordDX.y * texCoordDY.x));
	return bias_UV;
}

//-------------------------------------------------------------------------------------------------
// Samples the shadow map with a fixed-size PCF kernel optimized with GatherCmp. Uses code
// from "Fast Conventional Shadow Filtering" by Holger Gruen, in GPU Pro.
//-------------------------------------------------------------------------------------------------
float 
SampleShadowMapFixedSizePCF(
	in int lightId,
	in float3 shadowPos, 
	in float3 shadowPosDX,
	in float3 shadowPosDY, 
	in uint cascadeIdx,
	in int usePlaneDepthBias,
	in float staticOffsetBias) 
{
	float2 shadow_map_size;
	float num_slices;
	gShadowMap[lightId].GetDimensions(shadow_map_size.x, shadow_map_size.y, num_slices);

	float light_depth = shadowPos.z;

	float2 texel_size = 1.0f / shadow_map_size;

	if (usePlaneDepthBias)
	{
		float2 receiver_plane_depth_bias = ComputeReceiverPlaneDepthBias(shadowPosDX, shadowPosDY);

		// Static depth biasing to make up for incorrect fractional sampling on the shadow map grid
		float fractional_sampling_error = dot(float2(1.0f, 1.0f) * texel_size, abs(receiver_plane_depth_bias));
		light_depth -= min(fractional_sampling_error, 0.01f);
	}

	light_depth -= staticOffsetBias;

	return gShadowMap[lightId].SampleCmpLevelZero(gShadowMapCompSamplerPCF, float3(shadowPos.xy, cascadeIdx), light_depth);
}

//-------------------------------------------------------------------------------------------------
// Samples the shadow map with a fixed-size PCF kernel optimized with GatherCmp. Uses code
// from "Fast Conventional Shadow Filtering" by Holger Gruen, in GPU Pro.
// This function also assigns the out parameter the sampled depth.
//-------------------------------------------------------------------------------------------------
float
SampleShadowMapFixedSizePCFWithDepth(
	in int lightId,
	in float3 shadowPos,
	in float3 shadowPosDX,
	in float3 shadowPosDY,
	in uint cascadeIdx,
	in int usePlaneDepthBias,
	in float staticOffsetBias,
	out float sampledDepthOut)
{
	float2 shadow_map_size;
	float num_slices;
	gShadowMap[lightId].GetDimensions(shadow_map_size.x, shadow_map_size.y, num_slices);

	float light_depth = shadowPos.z;

	float2 texel_size = 1.0f / shadow_map_size;

	if (usePlaneDepthBias)
	{
		float2 receiver_plane_depth_bias = ComputeReceiverPlaneDepthBias(shadowPosDX, shadowPosDY);

		// Static depth biasing to make up for incorrect fractional sampling on the shadow map grid
		float fractional_sampling_error = dot(float2(1.0f, 1.0f) * texel_size, abs(receiver_plane_depth_bias));
		light_depth -= min(fractional_sampling_error, 0.01f);
	}

	light_depth -= staticOffsetBias;

	sampledDepthOut = gShadowMap[lightId].Sample(gShadowMapSamplerPCF, float3(shadowPos.xy, cascadeIdx));	

	return gShadowMap[lightId].SampleCmpLevelZero(gShadowMapCompSamplerPCF, float3(shadowPos.xy, cascadeIdx), light_depth);
}