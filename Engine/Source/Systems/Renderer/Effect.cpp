#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Effect.h"

Effect::Effect(void)
{
}

/*virtual*/
Effect::~Effect(void)
{    
}
 
std::list<InputLayoutManager::SubInputLayout>&
Effect::InputLayoutDescription(std::string const& techName)
{     
	std::map< std::string, std::list<InputLayoutManager::SubInputLayout> >::iterator input_desc_iter = mInputLayoutDescs.find(techName);
    assert(mInputLayoutDescs.end() != input_desc_iter);

    return input_desc_iter->second;
}

ID3D11InputLayout* const 
Effect::InputLayout(std::string const& techName)
{
	std::map< std::string, ID3D11InputLayout* >::iterator iter = mInputLayouts.find(techName);

	if (iter == mInputLayouts.end())
		return NULL;

	return iter->second;	
}

HRESULT
Effect::CompileShader(_In_ LPCWSTR srcFile, _In_ LPCSTR entryPoint, _In_ LPCSTR profile, _Outptr_ ID3DBlob** blob)
{
	// Function taken from:
	// http://msdn.microsoft.com/en-us/library/windows/desktop/hh968107(v=vs.85).aspx

	if (!srcFile || !entryPoint || !profile || !blob)
		return E_INVALIDARG;

	*blob = nullptr;

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3DCOMPILE_DEBUG;
#endif

	const D3D_SHADER_MACRO defines[] =
	{
		"EXAMPLE_DEFINE", "1",
		NULL, NULL
	};

	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;
	HRESULT hr = D3DCompileFromFile(srcFile, defines, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		entryPoint, profile,
		flags, 0, &shaderBlob, &errorBlob);
	if (FAILED(hr))
	{
		if (errorBlob)
		{
			OutputDebugMsg((char*)errorBlob->GetBufferPointer());
			errorBlob->Release();
		}

		if (shaderBlob)
			shaderBlob->Release();

		return hr;
	}

	*blob = shaderBlob;

	return hr;
}