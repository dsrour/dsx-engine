#include <vector>
#include <string>
#include <assert.h>
#include "InputLayoutManager.h"

InputLayoutManager::~InputLayoutManager(void)
{
    std::map< std::list<SubInputLayout>, ID3D11InputLayout* >::iterator layout_iter;
    for (layout_iter = mInputLayouts.begin(); layout_iter != mInputLayouts.end(); layout_iter++)
    {
        if (layout_iter->second)
            layout_iter->second->Release();    
    }    

    mInputLayouts.clear();
}

ID3D11InputLayout* const 
InputLayoutManager::GetOrCreateInputLayout(std::list<SubInputLayout>& fullLayout, ID3DBlob* const vs, ID3D11Device* const device)
{
	std::map< std::list<SubInputLayout>, ID3D11InputLayout* >::iterator input_layout_iter = mInputLayouts.find(fullLayout);
	if (mInputLayouts.end() != input_layout_iter)
		return input_layout_iter->second;

	// Not found, so create it.
	std::vector<D3D11_INPUT_ELEMENT_DESC> full_input_layout_desc;
	std::map< std::string, unsigned int > semantic_index;
	unsigned int input_slot = 0;

	std::list<SubInputLayout>::iterator sub_layout_iter;
	for (sub_layout_iter = fullLayout.begin(); sub_layout_iter != fullLayout.end(); sub_layout_iter++)
	{
		switch (*sub_layout_iter)
		{
		case GEOMETRY:
		{
			D3D11_INPUT_ELEMENT_DESC pos = { "POSITION", semantic_index["POSITION"]++, DXGI_FORMAT_R32G32B32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			D3D11_INPUT_ELEMENT_DESC norm = { "NORMAL", semantic_index["NORMAL"]++, DXGI_FORMAT_R32G32B32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

			full_input_layout_desc.push_back(pos);
			full_input_layout_desc.push_back(norm);
			break;
		}

		case TEXTURE:
		{
			D3D11_INPUT_ELEMENT_DESC tex_coord = { "TEXCOORD", semantic_index["TEXCOORD"]++, DXGI_FORMAT_R32G32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			D3D11_INPUT_ELEMENT_DESC tangent = { "TANGENT", semantic_index["TANGENT"]++, DXGI_FORMAT_R32G32B32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

			full_input_layout_desc.push_back(tex_coord);
			full_input_layout_desc.push_back(tangent);
			break;
		}

		case INSTANCED:
		{
			D3D11_INPUT_ELEMENT_DESC world_mat_0 = { "WORLD", semantic_index["INSTANCED_WORLD"]++, DXGI_FORMAT_R32G32B32A32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 };
			D3D11_INPUT_ELEMENT_DESC world_mat_1 = { "WORLD", semantic_index["INSTANCED_WORLD"]++, DXGI_FORMAT_R32G32B32A32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 };
			D3D11_INPUT_ELEMENT_DESC world_mat_2 = { "WORLD", semantic_index["INSTANCED_WORLD"]++, DXGI_FORMAT_R32G32B32A32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 };
			D3D11_INPUT_ELEMENT_DESC world_mat_3 = { "WORLD", semantic_index["INSTANCED_WORLD"]++, DXGI_FORMAT_R32G32B32A32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 };
			D3D11_INPUT_ELEMENT_DESC color = { "COLOR", semantic_index["INSTANCED_COLOR"]++, DXGI_FORMAT_R32G32B32A32_FLOAT, input_slot, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 };

			full_input_layout_desc.push_back(world_mat_0);
			full_input_layout_desc.push_back(world_mat_1);
			full_input_layout_desc.push_back(world_mat_2);
			full_input_layout_desc.push_back(world_mat_3);
			full_input_layout_desc.push_back(color);
			break;
		}
		}

		input_slot++;
	}

	ID3D11InputLayout* input_layout;
	HRESULT hr = device->CreateInputLayout(&full_input_layout_desc[0], (unsigned int)full_input_layout_desc.size(),
		vs->GetBufferPointer(),
		vs->GetBufferSize(),
		&input_layout);

	assert(SUCCEEDED(hr));

	mInputLayouts[fullLayout] = input_layout;

	return input_layout;
}
