#include <assert.h>
#include <algorithm>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Systems/Renderer/Cameras/FpCamera.h"

#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"

// Needed components
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"

#include "Renderer.h"

extern BaseApp* gApp;

D3dRenderer::D3dRenderer(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
{
    mSwapChain = NULL;
    mDevice = NULL;
    mDeviceContext = NULL;
    mBackBufferRTV = NULL;
    mVsyncEnabled = false;	

    // Default values.
    ClearColor(0.0f, 0.0f, 0.0f, 0.0f); 
        
    // Set family req
    std::set<unsigned int> fam_req;
    fam_req.insert(Renderable::GetGuid());       
    SetFamilyRequirements(fam_req);       

    // Set a default cam
    CurrentCamera( std::make_shared<FpCamera>() );
}

D3dRenderer::~D3dRenderer()
{   
    // Ant Tweak Bar
    TwTerminate();

    Shutdown();    
}

void
D3dRenderer::ClearColor(float const& r, float const& g, float const& b, float const& a)
{       
    mClearColor[0] = r;  
    mClearColor[1] = g;  
    mClearColor[2] = b;  
    mClearColor[3] = a;  
}

void
D3dRenderer::CurrentCamera(std::shared_ptr<Camera> camera) 
{
    if (NULL == camera) 
    {
        OutputDebugMsg("CurrentCamera(std::shared_ptr<Camera> camera): NULL parameter.\n");
        return;
    }

    mCurrentCamera = camera; 
}

void
D3dRenderer::RenderPath(std::list< std::shared_ptr<Pipeline> > pipelines)
{
    // Update current camera
    if (mCurrentCamera)
        mCurrentCamera->Update();

	for ( std::list< std::shared_ptr<Pipeline> >::iterator iter = mRenderPath.begin();
		  iter != mRenderPath.end();
		  ++iter )
        (*iter)->MadeInactive();
    
    mRenderPath = pipelines;
    
	for ( std::list< std::shared_ptr<Pipeline> >::iterator iter = mRenderPath.begin();
		  iter != mRenderPath.end();
		  ++iter )
		(*iter)->MadeActive();
}

void
D3dRenderer::EnableVsync(bool const& enabled)
{
    mVsyncEnabled = enabled;

    // Probably got to do work on dx side though
}

bool const
D3dRenderer::Init(unsigned int const& width, unsigned int const& height, HWND const& handle)
{
    HRESULT result;
    bool ret = true;

    // Create factory
    IDXGIFactory2* factory;
#ifdef _DEBUG
	result = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, __uuidof(IDXGIFactory2), (void**)&factory);
#else
	result = CreateDXGIFactory2(0, __uuidof(IDXGIFactory2), (void**)&factory);
#endif

    if ( FAILED(result) )
    {        
        OutputDebugMsg("Could not create DXGIFactory.\n");        
        ret = false;
    }
    assert(SUCCEEDED(result));

    // Create adapter for main GPU
    IDXGIAdapter2* adapter;
	result = factory->EnumAdapters1(0, (IDXGIAdapter1**)&adapter);
    if ( FAILED(result) )
    {        
        OutputDebugMsg("Could not create adapter for GPU.\n");      
        ret = false;  
    }
    assert(SUCCEEDED(result));

    // Enumerate display outputs
    IDXGIOutput1* adapter_output;
	result = adapter->EnumOutputs(0, (IDXGIOutput**)&adapter_output);
    if ( FAILED(result) ) 
    {        
        OutputDebugMsg("Could not enumerate display outputs.\n"); 
        ret = false;       
    }
    assert(SUCCEEDED(result));

    // Get # of modes that support 32bit color format for the display output.
    unsigned int num_modes;
    result = adapter_output->GetDisplayModeList1( DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, NULL);
    if ( FAILED(result) || 0 == num_modes )
    {
        OutputDebugMsg("Could not find a display output that supports 32bit colors.\n");  
        ret = false;      
    }
    assert(SUCCEEDED(result));

    // Create a list to hold all possible display modes for found display output & gpu.
    DXGI_MODE_DESC1* display_mode_list = new DXGI_MODE_DESC1[num_modes];
    result = adapter_output->GetDisplayModeList1(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, display_mode_list);
    if(FAILED(result))
    {
        OutputDebugMsg("Could not create display mode list.\n");
        ret = false;		
    }
    assert(SUCCEEDED(result));

    // Go through the list and find a mode that matches width and height and get numerator and denominator
    unsigned int numerator = 0;
    unsigned int denominator = 0;
    for (unsigned int i = 0; i < num_modes; i++)
    {
        if (display_mode_list[i].Width == width)
        {
            if (display_mode_list[i].Height == height)
            {
                numerator = display_mode_list[i].RefreshRate.Numerator;
                denominator = display_mode_list[i].RefreshRate.Denominator;
            }
        }   
    }

    // Get gpu info
    DXGI_ADAPTER_DESC2 adapter_desc;
    result = adapter->GetDesc2(&adapter_desc);
    if(FAILED(result))
    {
        OutputDebugMsg("Could not get adapter description.\n");	
        ret = false;
    }
    assert(SUCCEEDED(result));

    size_t str_lngth;
    mGpuMemory = (int)(adapter_desc.DedicatedVideoMemory / 1024 / 1024);
    if ( 0 != wcstombs_s(&str_lngth, mGpuDesc, 128, adapter_desc.Description, 128) )
    {
        OutputDebugMsg("Error setting gpu description.\n");
        // Maybe should set mGpuDesc instead of possible garbage due to failing.   
    }

#ifdef _DEBUG
    OutputDebugMsg("Gpu: " + to_string(mGpuDesc) + ".  " + to_string(mGpuMemory) + "MB\n");	    
#endif

    // Release resources    
    delete [] display_mode_list;
    display_mode_list = NULL;

    adapter_output->Release();
    adapter_output = NULL;

    adapter->Release();
    adapter = NULL;

    factory->Release();
    factory = NULL;


     //Setup swap chain
    DXGI_SWAP_CHAIN_DESC swap_chain_desc;
    ZeroMemory(&swap_chain_desc, sizeof(swap_chain_desc));
    swap_chain_desc.BufferCount = 1;  // 1 back buffer for now
    swap_chain_desc.BufferDesc.Width = width;
    swap_chain_desc.BufferDesc.Height = height;
    swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

    // Set the refresh rate of back buffer
    if (mVsyncEnabled)
    {
        swap_chain_desc.BufferDesc.RefreshRate.Numerator = numerator;
        swap_chain_desc.BufferDesc.RefreshRate.Denominator = denominator;
    }
    else
    {
        swap_chain_desc.BufferDesc.RefreshRate.Numerator = 0;
        swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
    }

    // Make back buffer a render target output
    swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

    swap_chain_desc.OutputWindow = handle;

    // No multisampling
    swap_chain_desc.SampleDesc.Count = 1;
    swap_chain_desc.SampleDesc.Quality = 0;

    // Always start in windowed mode
    swap_chain_desc.Windowed = true;

    // Set the scan line ordering and scaling to unspecified.
    swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swap_chain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    // Discard the back buffer contents after presenting.
    swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    // Don't set the advanced flags.
    swap_chain_desc.Flags = 0;

    // Feature level must support DX11.
    D3D_FEATURE_LEVEL feature_level = D3D_FEATURE_LEVEL_11_0;

    UINT flags = 0;
#ifdef _DEBUG
    flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	 
    // Create swap chain, device, and device context.
    result = D3D11CreateDeviceAndSwapChain( NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, &feature_level, 1, D3D11_SDK_VERSION, 
		&swap_chain_desc, (IDXGISwapChain**)&mSwapChain, (ID3D11Device**)&mDevice, NULL, (ID3D11DeviceContext**)&mDeviceContext);
    if ( FAILED(result) )
    {
        OutputDebugMsg("Could not create device and swap chain.\n");	    
    }
    assert(SUCCEEDED(result));

    // Get back buffer pointer.
    ID3D11Texture2D* back_buffer;
    result = mSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&back_buffer);
    if ( FAILED(result) )
    {
        OutputDebugMsg("Could not get back buffer of swap chain.\n");	
        ret = false;    
    }    
    assert(SUCCEEDED(result));

    // Set back buffer to newly created render target view.	
    result = mDevice->CreateRenderTargetView(back_buffer, NULL, &mBackBufferRTV);
    if ( FAILED(result) )
    {
        OutputDebugMsg("Could not createFrender target view.\n");	
        ret = false;    
    }
    assert(SUCCEEDED(result));

    // Release resources
    back_buffer->Release();
    back_buffer = NULL;

    // Set viewport
    D3D11_VIEWPORT viewport;	
    viewport.Width = (float)width;
    viewport.Height = (float)height;
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;
    viewport.TopLeftX = 0.0f;
    viewport.TopLeftY = 0.0f;

    // Create the viewport.
    mDeviceContext->RSSetViewports(1, &viewport);

    // Input layout manager
    mInputLayoutManager = std::make_shared<InputLayoutManager>();

    // Render state manager
    mRenderStateManager = std::make_shared<RenderStateManager>(mDevice);

    // Texture manager
    mTextureManager = std::make_shared<TextureManager>();

	// Instancing Manager
	mInstancingManager = std::make_shared<InstancingManager>();

    // Create default pipelines and set a current pipeline (forward renderer in this case) 
	/*mForwardRender = std::make_shared<ForwardRenderer>( shared_from_this() );
	mSkyRender = std::make_shared<SkyRenderer>( shared_from_this() );
	std::list< std::shared_ptr<Pipeline> > render_path;
	render_path.push_back(mForwardRender);
	render_path.push_back(mSkyRender);
    RenderPath( render_path );*/

    // Set a default topology (this will remove stupid warning msgs if none is set)
    mDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Initialize depth/stencil buffer and view
	auto depth_stencil_format = DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT;
	auto depth_stencil_resource_format = DXGI_FORMAT::DXGI_FORMAT_R24G8_TYPELESS;
	auto depth_stencil_srv_format = DXGI_FORMAT::DXGI_FORMAT_R24_UNORM_X8_TYPELESS;

	D3D11_TEXTURE2D_DESC depth_stencil_desc;
	depth_stencil_desc.Width = gApp->WinWidth();
	depth_stencil_desc.Height = gApp->WinHeight();
	depth_stencil_desc.MipLevels = 1;
	depth_stencil_desc.ArraySize = 1;
	depth_stencil_desc.Format = depth_stencil_resource_format;
	// No MSAA
	depth_stencil_desc.SampleDesc.Count = 1;
	depth_stencil_desc.SampleDesc.Quality = 0;
	depth_stencil_desc.Usage = D3D11_USAGE_DEFAULT;
	depth_stencil_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	depth_stencil_desc.CPUAccessFlags = 0;
	depth_stencil_desc.MiscFlags = 0;
		
	result = Device()->CreateTexture2D( &depth_stencil_desc, 0, &mDepthStencilBuffer );
	assert( SUCCEEDED(result) );

	D3D11_DEPTH_STENCIL_VIEW_DESC ds_view_desc;
	ZeroMemory(&ds_view_desc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	ds_view_desc.Format = depth_stencil_format;
	ds_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	ds_view_desc.Texture2D.MipSlice = 0;

	result = Device()->CreateDepthStencilView(mDepthStencilBuffer, &ds_view_desc, &mDepthStencilView);
	assert( SUCCEEDED(result) );

	D3D11_SHADER_RESOURCE_VIEW_DESC ds_srv_desc;
	ZeroMemory(&ds_srv_desc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	ds_srv_desc.Format = depth_stencil_srv_format;
	ds_srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	ds_srv_desc.Texture2D.MipLevels = 1;

	result = Device()->CreateShaderResourceView(mDepthStencilBuffer, &ds_srv_desc, &mDepthStencilSrv);
	assert(SUCCEEDED(result));


	// Create sub-systems and add them to system manager
	mLightingSystem = std::make_shared<LightingSystem>(mEntityManager);
	mLightingSystemId = gApp->SystemMngr()->AddSystem(mLightingSystem);
	mShadowSystem = std::make_shared<ShadowSystem>(mEntityManager);
	mShadowSystemId = gApp->SystemMngr()->AddSystem(mShadowSystem);


	// ANT TWEAK BAR
	TwInit(TW_DIRECT3D11, mDevice);
	TwWindowSize(width, height);

    return ret;
}

void
D3dRenderer::RunImplementation(std::set<unsigned int> const* family, double const currentTime)
{
    // Update cam
    if (mCurrentCamera)
        mCurrentCamera->Update();
	
    // Update lighting system by running it
    gApp->SystemSchdlr()->RunSystemNow(mLightingSystemId);

	// Update shadow system by running it... this will update shadow maps.
	// It should be done here since it does it's own culling and makes use of the instancing manager.
	// The instancing manager is then reset and reused by the main pass in this function.
	gApp->SystemSchdlr()->RunSystemNow(mShadowSystemId);
	
	mDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    mDeviceContext->ClearRenderTargetView(mBackBufferRTV, &mClearColor[0]);

	// Sky renderable that'll be rendered last
	std::set<unsigned int> skys;
    // Update and enter pipeline here and pass family & currentTime
    if (!mRenderPath.empty())
    {
		// Organize entities by their renderable types
		std::map< unsigned int, std::set<unsigned int> > families_by_type;
                
        // Do frustum culling        
        DirectX::XMMATRIX cam_view = XMLoadFloat4x4(&mCurrentCamera->ViewMatrix());      
        DirectX::XMMATRIX inv_view = XMMatrixInverse(nullptr, cam_view);

        std::set<unsigned int>::const_iterator fam_iter = family->begin();
        for (fam_iter; fam_iter!= family->end(); fam_iter++)
        {   						
            std::shared_ptr<IComponent> spatialized_tmp_comp, bounded_tmp_comp, renderable_tmp_comp;
			            
            gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), spatialized_tmp_comp);     
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Bounded::GetGuid(), bounded_tmp_comp);  
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), renderable_tmp_comp);  			


			// Exceptions that don't need frustum culling
			if (renderable_tmp_comp)
				if ( Renderable::SKY == ((Renderable*)(renderable_tmp_comp.get()))->RenderableType() )
				{
					families_by_type[Renderable::SKY].insert(*fam_iter);
					continue;
				}
			
			if (spatialized_tmp_comp && bounded_tmp_comp)
            {
                Spatialized* spatialized = static_cast<Spatialized*>(spatialized_tmp_comp.get());
				Bounded* bounded = static_cast<Bounded*>(bounded_tmp_comp.get());
                
                DirectX::XMMATRIX world = XMLoadFloat4x4(&spatialized->LocalTransformation());
				DirectX::XMMATRIX inv_world = XMMatrixInverse(nullptr, world);

                // view space to object's local space
                DirectX::XMMATRIX to_local = XMMatrixMultiply(inv_view, inv_world);

                // transform the camera frustum from view space to the object's local space
                DirectX::BoundingFrustum local_space_frustum = mCurrentCamera->CameraFrustum();
				local_space_frustum.Transform(local_space_frustum, to_local);

                // perform box/frustum intersection test in local space
                if ( local_space_frustum.Contains(bounded->AabbBounds()) != DirectX::DISJOINT )
				{
					// Add to batch if renderable is instanced
					if (renderable_tmp_comp)
					{
						Renderable* renderable = static_cast<Renderable*>(renderable_tmp_comp.get());

						int batch_id = renderable->DynamicInstancedBatchId();
						if (batch_id >= 0)
						{
							mInstancingManager->AddEntityToDynamicBatch(*fam_iter);
							continue;
						}
						else
						{
							families_by_type[renderable->RenderableType()].insert(*fam_iter);
						}
					}					
				}
            }  
            else
			{
				// Add to batch if renderable is instanced
				if (renderable_tmp_comp)
				{
					Renderable* renderable = static_cast<Renderable*>(renderable_tmp_comp.get());

					int batch_id = renderable->DynamicInstancedBatchId();
					if (batch_id >= 0)
					{
						mInstancingManager->AddEntityToDynamicBatch(*fam_iter);
						continue;
					}
					else
					{
						 families_by_type[renderable->RenderableType()].insert(*fam_iter);      
					}
				}                      
			}
        }

		// Update instancing manager
		mInstancingManager->Update(mDeviceContext);		
				
        // Go through rendering path
		std::list< std::shared_ptr<Pipeline> >::iterator render_path_iter = mRenderPath.begin();
		for (; render_path_iter != mRenderPath.end(); ++render_path_iter)			
			(*render_path_iter)->EnterPipeline(families_by_type, currentTime);				
    }


	// Unbind render targets
	ID3D11RenderTargetView* rtv = BackBufferRTV();
	DeviceContext()->OMSetRenderTargets(1, &rtv, NULL);

	// Update shadow system's clip distances now that the depth buffer has all objects
	mShadowSystem->UpdateClipDistances(DepthStencilSrv()); 
		        
    // Draw Ant Tweak Bar gui
	if (mDeviceContext->IsAnnotationEnabled())
		mDeviceContext->BeginEventInt(L"AntTweakBar Menu", 0);
    TwDraw();     	
	if (mDeviceContext->IsAnnotationEnabled())
		mDeviceContext->EndEvent();

    if(mVsyncEnabled)
    {
        // Lock to screen refresh rate.
        mSwapChain->Present(1, 0);
    }
    else
    {
        // Present as fast as possible.
        mSwapChain->Present(0, 0);
    }
}

void
D3dRenderer::Shutdown(void)
{
    for ( std::list< std::shared_ptr<Pipeline> >::iterator iter = mRenderPath.begin();
		  iter != mRenderPath.end();
		  ++iter )
    {
       (*iter)->MadeInactive();
       iter->reset();
    }
		
    if (mSwapChain)
    {
        // Window mode
        mSwapChain->SetFullscreenState(false, NULL);
    }

	if (mDepthStencilView)
	{
		mDepthStencilView->Release();
		mDepthStencilView = nullptr;
	}

	if (mDepthStencilBuffer)
	{
		mDepthStencilBuffer->Release();
		mDepthStencilBuffer = nullptr;
	}
        
    if (mBackBufferRTV)
    {
        mBackBufferRTV->Release();
		mBackBufferRTV = nullptr;
    }

	if (mDepthStencilSrv)
	{
		mDepthStencilSrv->Release();
		mDepthStencilSrv = nullptr;
	}

    if (mDeviceContext)
    {
        mDeviceContext->Release();
		mDeviceContext = nullptr;
    }

    if (mDevice)
    {        
        mDevice->Release();
		mDevice = nullptr;
    }

    if (mSwapChain)
    {
        mSwapChain->Release();
		mSwapChain = nullptr;
    }	   	    
}

void 
D3dRenderer::SetBackBufferRenderTarget()
{
	// Bind render targets
	ID3D11RenderTargetView* rtv = BackBufferRTV();
	DeviceContext()->OMSetRenderTargets(1, &rtv, mDepthStencilView);	
}

void 
D3dRenderer::SetupLightsAndShadowMapsForGPU(
	std::vector<LightToGpu>& lightsToGpuOut, 
	std::vector<ID3D11ShaderResourceView*>& shadowMapCascades)
{
	lightsToGpuOut.clear();
	shadowMapCascades.clear();

	// Get lights to fetch needed vars for lighting
	std::list< std::pair<LightType, Light*> >  lights = LightingSystm()->Lights();
	std::list< std::pair<LightType, Light*> >::iterator light_iter = lights.begin();
	std::list< ShadowEmitting* > shadow_emitting_comp_refs = LightingSystm()->ShadowEmittingComponents();
	std::list< ShadowEmitting* >::iterator  shadow_emitting_iter = shadow_emitting_comp_refs.begin();

	for (; light_iter != lights.end(); light_iter++, shadow_emitting_iter++)
	{
		bool submit_cascades = false;

		LightToGpu light;

		light.lightType = light_iter->first;
		switch (light_iter->first)
		{
		case DIRECTIONAL_LIGHT:
		{
			light.directionalLight = *(static_cast<DirectionalLight*>(light_iter->second));

			// Check if it emits shadows
			if (*shadow_emitting_iter)
			{
				light.emitsShadows = 1;

				light.shadowMatrix = DirectX::XMMatrixTranspose(
					DirectX::XMLoadFloat4x4(&(*shadow_emitting_iter)->ShadowMatrix()));

				light.cascadeSplits.x = (*shadow_emitting_iter)->CascadeSplit(0);
				light.cascadeSplits.y = (*shadow_emitting_iter)->CascadeSplit(1);
				light.cascadeSplits.z = (*shadow_emitting_iter)->CascadeSplit(2);
				light.cascadeSplits.w = (*shadow_emitting_iter)->CascadeSplit(3);

				for (auto i = 0; i < NUM_SHADOW_CASCADES; i++)
				{
					light.cascadeOffsets[i] = (*shadow_emitting_iter)->CascadeOffset(i);
					light.cascadeScales[i] = (*shadow_emitting_iter)->CascadeScale(i);
				}

				light.staticOffsetBias = (*shadow_emitting_iter)->StaticOffsetBias();
				light.normalOffsetScaleBias = (*shadow_emitting_iter)->NormalOffsetScaleBias();
				light.usePlaneDepthBias = (*shadow_emitting_iter)->UsePlaneDepthBias();
				light.shadowIntensity = (*shadow_emitting_iter)->ShadowIntensity();

				submit_cascades = true;
			}
			else
				light.emitsShadows = 0;

			break;
		}

		case POINT_LIGHT:
			light.emitsShadows = 0;
			light.pointLight = *(static_cast<PointLight*>(light_iter->second));
			break;

		case SPOT_LIGHT:
			light.emitsShadows = 0;
			light.spotLight = *(static_cast<SpotLight*>(light_iter->second));
			break;
		}

		lightsToGpuOut.push_back(light);

		if (submit_cascades)
		{
			shadowMapCascades.push_back((*shadow_emitting_iter)->SmcResources().srv.Get());
		}
		else
			shadowMapCascades.push_back(nullptr);
	}
}
