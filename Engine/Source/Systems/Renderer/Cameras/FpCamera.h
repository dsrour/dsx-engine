/**  FpCamera.h
 *
 * Holds a first person camera class.
 */

#pragma once

#include "Systems/Renderer/Camera.h"

class FpCamera : public Camera
{
public:
    FpCamera(void) ;

    virtual
    ~FpCamera(void);

   /**
    * Move forward, backward        
    * 
    * @param Increment per sec.
    * @param Elapsed time since beginning of frame.
    */ 
    void
    Move(float const increment, float const frameTimeDelta = 1.0f);

   /**
    * Strafe left, right
    * 
    * @param Increment per sec.
    * @param Elapsed time since beginning of frame.
    */ 
    void
    Strafe(float const increment, float const frameTimeDelta = 1.0f);

   /**
    * Rotate from x-axis.
    * 
    * @param Degree of rotation per sec in radians.
    * @param Elapsed time since beginning of frame.
    */ 
    void
    Pitch(float const degree, float const frameTimeDelta = 1.0f);

   /**
    * Rotate from z-axis.
    * 
    * @param Degree of rotation per sec in radians.
    * @param Elapsed time since beginning of frame.
    */ 
    void
    Yaw(float const degree, float const frameTimeDelta = 1.0f);

   /**
    * Set position      
    *     
    * @param Position
    */ 
    void
    Position(DirectX::XMFLOAT4& pos);

    virtual void
    Update(void);

	DirectX::XMFLOAT4 const&
	RotationQuat(void) const {return mRot;}

	DirectX::XMFLOAT4 
	LookTo() const { return mLookTo; }

	/**
	 * Using this function basically does a manual update of the camera.
	 */
	void
	LookTo(DirectX::XMVECTOR& pos, DirectX::XMVECTOR& lookTo, DirectX::XMVECTOR& upDir);
    

protected:
      DirectX::XMFLOAT4 mLookTo;	  
	  DirectX::XMFLOAT4 mLookUp;
      DirectX::XMFLOAT4 mLookRight;     
       
      DirectX::XMFLOAT4 mRot; 
      
      bool mDirty;             
};