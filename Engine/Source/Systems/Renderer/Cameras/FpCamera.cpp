#include "Debug/Debug.h"
#include "Systems/Renderer/MathHelper.h"
#include "Systems/Renderer/Cameras/FpCamera.h"

FpCamera::FpCamera(void) : Camera()
{
    mLookTo = DirectX::XMFLOAT4(0.0f, 0.0f, 1.0f, 0.0f);
    mLookUp = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f);
    mLookRight = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f);    

    mRot = DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
    
    mDirty = true;
}

/*virtual*/
FpCamera::~FpCamera(void)
{
        
}

void
FpCamera::Move(float const increment, float const frameTimeDelta)
{   
    DirectX::XMVECTOR pos = XMLoadFloat4(&mPos); 
    DirectX::XMVECTOR look_at = XMLoadFloat4(&mLookTo);
    //DirectX::XMVECTOR rot = XMLoadFloat4(&mRot);
    look_at = DirectX::XMVector4Transform( look_at, DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&mRot)) );
    DirectX::XMVECTOR new_pos = AddVectors( pos, FactorVector( FactorVector( look_at, increment ), frameTimeDelta ) );
    XMStoreFloat4(&mPos, new_pos); 
               
    mDirty = true;
}

void
FpCamera::Strafe(float const increment, float const frameTimeDelta)
{   
    DirectX::XMVECTOR pos = XMLoadFloat4(&mPos);
    DirectX::XMVECTOR look_r = XMLoadFloat4(&mLookRight);
    //DirectX::XMVECTOR rot = XMLoadFloat4(&mRot);
    look_r = DirectX::XMVector4Transform( look_r, DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&mRot)) );
    DirectX::XMVECTOR new_pos = AddVectors( pos, FactorVector( FactorVector( look_r, increment ), frameTimeDelta ) );
    XMStoreFloat4(&mPos, new_pos); 

    mDirty = true;
}

void
FpCamera::Pitch(float const degree, float const frameTimeDelta)
{
    float deg = degree*frameTimeDelta;
    
    DirectX::XMVECTOR rot = XMLoadFloat4(&mRot);   

    DirectX::XMVECTOR look_r = XMLoadFloat4(&mLookRight);    
    look_r = DirectX::XMVector4Transform(look_r, DirectX::XMMatrixRotationQuaternion(rot));

    DirectX::XMVECTOR extra_rot = DirectX::XMQuaternionRotationAxis(look_r, deg);
    

    XMStoreFloat4( &mRot, DirectX::XMQuaternionMultiply(rot, extra_rot) );
    mDirty = true;
}

void
FpCamera::Yaw(float const degree, float const frameTimeDelta)
{
    float deg = degree*frameTimeDelta;
    DirectX::XMVECTOR extra_rot = DirectX::XMQuaternionRotationAxis(XMLoadFloat4(&mLookUp), deg);
    DirectX::XMVECTOR rot = XMLoadFloat4(&mRot);            
    
    XMStoreFloat4( &mRot, DirectX::XMQuaternionMultiply(rot, extra_rot) );
    mDirty = true;
}

/*virtual*/ void
FpCamera::Update(void)
{
    if (mDirty)
    {
        //DirectX::XMVECTOR pos = XMLoadFloat4(&mPos);   
        DirectX::XMVECTOR rot = XMLoadFloat4(&mRot);   
        DirectX::XMVECTOR look_to = {0.0f, 0.0f, 1.0f, 0.0f};
        DirectX::XMVECTOR look_up = {0.0f, 1.0f, 0.0f, 0.0f};

        look_up = DirectX::XMVector4Transform(look_up, DirectX::XMMatrixRotationQuaternion(rot));
        look_to = DirectX::XMVector4Transform(look_to, DirectX::XMMatrixRotationQuaternion(rot));

        ViewMatrix(DirectX::XMLoadFloat4(&mPos), look_to, look_up);
        mDirty = false;
    }
}

void 
FpCamera::Position( DirectX::XMFLOAT4& pos )
{
	DirectX::XMVECTOR new_pos = XMLoadFloat4(&pos); 			
	XMStoreFloat4(&mPos, new_pos); 

	mDirty = true;
}

void 
FpCamera::LookTo(DirectX::XMVECTOR& pos, DirectX::XMVECTOR& lookTo, DirectX::XMVECTOR& upDir)
{	
	ViewMatrix(pos, lookTo, upDir);
	mDirty = false;
}
