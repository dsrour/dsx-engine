/**  RenderStateManager.h
 *
 *   Holds render states.
 *   Note::
 *     - Raster states mostly deal with the rendering pipeline, so it's not very flexible.  
 *       I must come back here and manually add to the enum every time a new one is needed.
 *     - Sampler states on the other hand might differ a lot per Renderable, so it is flexible here.
 */

#pragma once

#include <map>
#include <d3d11.h>

class RenderStateManager
{
public:
    enum RasterizerStateDesc
    {
        Default,  
		NoCull,
		NoCullDepthClipDisabled,
		Wireframe,
    };

    RenderStateManager(ID3D11Device* const d3dDevice);      

    ~RenderStateManager(void);    
    
    ID3D11RasterizerState* const
    RasterizerState(RasterizerStateDesc const& rasterState);   
    
    ID3D11SamplerState* const
    GetOrCreateSamplerState(ID3D11Device* const d3dDevice, D3D11_SAMPLER_DESC const& stateDesc);
	
	ID3D11DepthStencilState* const
	GetOrCreateDepthStencilState(ID3D11Device* const d3dDevice, D3D11_DEPTH_STENCIL_DESC const& dsDesc);

	ID3D11BlendState* const
	GetOrCreateBlendState(ID3D11Device* const d3dDevice, D3D11_BLEND_DESC const& blendDesc);
                    
	ID3D11SamplerState* 
	DefaultSamplerState() const { return mDefaultSamplerState; }
  
private:
    struct SamplerDesc
    {
        unsigned int filter;
        unsigned int addressU;
        unsigned int addressV;
        unsigned int addressW;
        float        mipLodBias;
        unsigned int maxAnisotropy;
        unsigned int comparisonFunc;
        float borderColor[ 4 ];
        float minLOD;
        float maxLOD;
    };

	struct SamplerDescComparor
	{
		bool operator()(const SamplerDesc & left, const SamplerDesc & right) const
		{
			if (left.filter != right.filter)
				return left.filter < right.filter;
			if (left.addressU != right.addressU)
				return left.addressU < right.addressU;
			if (left.addressV != right.addressV)
				return left.addressV < right.addressV;
			if (left.addressW != right.addressW)
				return left.addressW < right.addressW;
			if (left.mipLodBias != right.mipLodBias)
				return left.mipLodBias < right.mipLodBias;
			if (left.maxAnisotropy != right.maxAnisotropy)
				return left.maxAnisotropy < right.maxAnisotropy;
			if (left.comparisonFunc != right.comparisonFunc)
				return left.comparisonFunc < right.comparisonFunc;
			for (auto i = 0u; i < 4; i++)
				if (left.borderColor[i] != right.borderColor[i])
					return left.borderColor[i] < right.borderColor[i];
			if (left.minLOD != right.minLOD)
				return left.minLOD < right.minLOD;
			
			return left.maxLOD < right.maxLOD;
		}
	};

	struct DepthStencilDesc 
	{
		BOOL                       depthEnable;
		D3D11_DEPTH_WRITE_MASK     depthWriteMask;
		D3D11_COMPARISON_FUNC      depthFunc;
		BOOL                       stencilEnable;
		UINT8                      stencilReadMask;
		UINT8                      stencilWriteMask;
		D3D11_DEPTH_STENCILOP_DESC frontFace;
		D3D11_DEPTH_STENCILOP_DESC backFace;
	};

	struct DepthStencilDescComparor
	{
		bool operator()(const DepthStencilDesc & left, const DepthStencilDesc & right) const
		{
			if (left.depthEnable != right.depthEnable)
				return left.depthEnable < right.depthEnable;
			if (left.depthWriteMask != right.depthWriteMask)
				return left.depthWriteMask < right.depthWriteMask;
			if (left.depthFunc != right.depthFunc)
				return left.depthFunc < right.depthFunc;
			if (left.stencilEnable != right.stencilEnable)
				return left.stencilEnable < right.stencilEnable;
			if (left.stencilReadMask != right.stencilReadMask)
				return left.stencilReadMask < right.stencilReadMask;
			if (left.stencilWriteMask != right.stencilWriteMask)
				return left.stencilWriteMask < right.stencilWriteMask;
			if (left.frontFace.StencilDepthFailOp != right.frontFace.StencilDepthFailOp)
				return left.frontFace.StencilDepthFailOp < right.frontFace.StencilDepthFailOp;
			if (left.frontFace.StencilFailOp != right.frontFace.StencilFailOp)
				return left.frontFace.StencilFailOp < right.frontFace.StencilFailOp;
			if (left.frontFace.StencilFunc != right.frontFace.StencilFunc)
				return left.frontFace.StencilFunc < right.frontFace.StencilFunc;
			if (left.frontFace.StencilPassOp != right.frontFace.StencilPassOp)
				return left.frontFace.StencilPassOp < right.frontFace.StencilPassOp;
			if (left.backFace.StencilDepthFailOp != right.backFace.StencilDepthFailOp)
				return left.backFace.StencilDepthFailOp < right.backFace.StencilDepthFailOp;
			if (left.backFace.StencilFailOp != right.backFace.StencilFailOp)
				return left.backFace.StencilFailOp < right.backFace.StencilFailOp;
			if (left.backFace.StencilFunc != right.backFace.StencilFunc)
				return left.backFace.StencilFunc < right.backFace.StencilFunc;
			return left.backFace.StencilPassOp < right.backFace.StencilPassOp;
		}
	};

	struct BlendDesc
	{
		BOOL							alphaToConverageEnable;
		BOOL							independentBlendEnable;
		D3D11_RENDER_TARGET_BLEND_DESC	renderTarget[8];
	};

	struct BlendDescComparor
	{
		bool operator()(const BlendDesc & left, const BlendDesc & right) const
		{			
			for (auto i = 0u; i < 8; i++)
			{
				if (left.renderTarget[i].BlendEnable != right.renderTarget[i].BlendEnable)
					return left.renderTarget[i].BlendEnable < right.renderTarget[i].BlendEnable;
				if (left.renderTarget[i].BlendOp != right.renderTarget[i].BlendOp)
					return left.renderTarget[i].BlendOp < right.renderTarget[i].BlendOp;
				if (left.renderTarget[i].BlendOpAlpha != right.renderTarget[i].BlendOpAlpha)
					return left.renderTarget[i].BlendOpAlpha < right.renderTarget[i].BlendOpAlpha;
				if (left.renderTarget[i].DestBlend != right.renderTarget[i].DestBlend)
					return left.renderTarget[i].DestBlend < right.renderTarget[i].DestBlend;
				if (left.renderTarget[i].DestBlendAlpha != right.renderTarget[i].DestBlendAlpha)
					return left.renderTarget[i].DestBlendAlpha < right.renderTarget[i].DestBlendAlpha;
				if (left.renderTarget[i].RenderTargetWriteMask != right.renderTarget[i].RenderTargetWriteMask)
					return left.renderTarget[i].RenderTargetWriteMask < right.renderTarget[i].RenderTargetWriteMask;
				if (left.renderTarget[i].SrcBlend != right.renderTarget[i].SrcBlend)
					return left.renderTarget[i].SrcBlend < right.renderTarget[i].SrcBlend;
				if (left.renderTarget[i].SrcBlendAlpha != right.renderTarget[i].SrcBlendAlpha)
					return left.renderTarget[i].SrcBlendAlpha < right.renderTarget[i].SrcBlendAlpha;
			}


			if (left.alphaToConverageEnable != right.alphaToConverageEnable)
				return left.alphaToConverageEnable < right.alphaToConverageEnable;
			return left.independentBlendEnable < right.independentBlendEnable;
		}
	};

	
    std::map<RasterizerStateDesc, ID3D11RasterizerState*>							mRasterStates;
    std::map<SamplerDesc, ID3D11SamplerState*, SamplerDescComparor>					mSamplerStates;
	std::map<DepthStencilDesc, ID3D11DepthStencilState*, DepthStencilDescComparor>  mDepthStencilStates;
	std::map<BlendDesc, ID3D11BlendState*, BlendDescComparor>						mBlendStates;

	ID3D11SamplerState* mDefaultSamplerState;
	
};
