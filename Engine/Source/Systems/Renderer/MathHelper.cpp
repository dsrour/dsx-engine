#include "MathHelper.h"

DirectX::XMMATRIX InverseTranspose(DirectX::CXMMATRIX mat)
{
    DirectX::XMMATRIX a_matrix = mat;
    a_matrix.r[3] = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

    DirectX::XMVECTOR det = XMMatrixDeterminant(a_matrix);
    return XMMatrixTranspose(XMMatrixInverse(&det, a_matrix));
}

DirectX::XMVECTOR 
AddVectors( DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2 )
{
	float x1 = DirectX::XMVectorGetX(v1);
	float x2 = DirectX::XMVectorGetX(v2);
	float y1 = DirectX::XMVectorGetY(v1);
	float y2 = DirectX::XMVectorGetY(v2);
	float z1 = DirectX::XMVectorGetZ(v1);
	float z2 = DirectX::XMVectorGetZ(v2);
	float w1 = DirectX::XMVectorGetW(v1);
	float w2 = DirectX::XMVectorGetW(v2);

	return DirectX::XMVectorSet(x1+x2, y1+y2, z1+z2, w1+w2);
}

DirectX::XMVECTOR 
SubtractVectors( DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2 )
{
	float x1 = DirectX::XMVectorGetX(v1);
	float x2 = DirectX::XMVectorGetX(v2);
	float y1 = DirectX::XMVectorGetY(v1);
	float y2 = DirectX::XMVectorGetY(v2);
	float z1 = DirectX::XMVectorGetZ(v1);
	float z2 = DirectX::XMVectorGetZ(v2);
	float w1 = DirectX::XMVectorGetW(v1);
	float w2 = DirectX::XMVectorGetW(v2);

	return DirectX::XMVectorSet(x1-x2, y1-y2, z1-z2, w1-w2);
}

DirectX::XMVECTOR 
MultiplyVectors( DirectX::XMVECTOR const& v1, DirectX::XMVECTOR const& v2 )
{
	float x1 = DirectX::XMVectorGetX(v1);
	float x2 = DirectX::XMVectorGetX(v2);
	float y1 = DirectX::XMVectorGetY(v1);
	float y2 = DirectX::XMVectorGetY(v2);
	float z1 = DirectX::XMVectorGetZ(v1);
	float z2 = DirectX::XMVectorGetZ(v2);
	float w1 = DirectX::XMVectorGetW(v1);
	float w2 = DirectX::XMVectorGetW(v2);

	return DirectX::XMVectorSet(x1*x2, y1*y2, z1*z2, w1*w2);
}

DirectX::XMVECTOR 
FactorVector( DirectX::XMVECTOR const& v1, float const& factor )
{
	float x1 = DirectX::XMVectorGetX(v1);
	float y1 = DirectX::XMVectorGetY(v1);
	float z1 = DirectX::XMVectorGetZ(v1);
	float w1 = DirectX::XMVectorGetW(v1);

	return DirectX::XMVectorSet(x1*factor, y1*factor, z1*factor, w1*factor);
}

bool 
IsEqual(DirectX::XMFLOAT4X4 const& m1, DirectX::XMFLOAT4X4 const& m2)
{
	for (auto i = 0u; i < 4; i++)
		for (auto j = 0u; j < 4; j++)
			if (m1(i,j) != m2(i, j))
				return false;
	return true;
}
