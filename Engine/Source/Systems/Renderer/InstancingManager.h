/**  InstancingManager.h
 *
 *   Manages instanced renderables.  
 * 	 Will create batches and ID3D11Buffer's to hold the entities.
 */

#pragma once

#include <set>
#include <map>
#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>
#include "Components/Renderable.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/ReflectionMapped.h"

class InstancingManager
{
public:
	struct InstancedData
	{
		DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4	color;
	};

	struct InstancedBatchesResources
	{
		std::map<unsigned int, ID3D11Buffer*>						instancedBuffer;		// BATCH ID -> Instanced Buffer
		std::map< unsigned int, std::shared_ptr<Renderable> >		batchRenderableRef;		// Reference to buffers	
		std::map< unsigned int, std::shared_ptr<DiffuseMapped> >	batchDiffuseMapRef;		// Reference to diffuse map	
		std::map< unsigned int, std::shared_ptr<NormalMapped> >		batchNormalMapRef;		// Reference to normal map	
		std::map< unsigned int, std::shared_ptr<ReflectionMapped> >	batchReflectionMapRef;	// Reference to reflection map	
		std::map<unsigned int, unsigned int>						bufferSize;				// How many instances fill up the buffer
		std::map<unsigned int, unsigned int>						bufferCapacity;			// Total capacity of the buffer
	};
	

    ~InstancingManager(void);
    
	/// DO NOT CALL MANUALLY! If buffers for the specified batch id in the renderable don't exist, renderable is ignored.
	void
	AddEntityToDynamicBatch(unsigned int const& entityId);

		
	/// Will free memory of buffer for batchId
	void
	FreeBatchBuffer(unsigned int const& batchId);

	/// Will ignore if batchId was already created.
	/// NOTE: that the renderer does frustum culling of entities belonging to dynamic batches.
	void
	CreateDynamicBatchBuffer(	ID3D11Device* device, unsigned int const& batchId,
								unsigned int const& capacity, std::shared_ptr<Renderable>& renderable, 
								std::shared_ptr<DiffuseMapped>& diffuse, std::shared_ptr<NormalMapped>& normal, std::shared_ptr<ReflectionMapped>& reflection);

	/// Will ignore if batchId was already created.
	/// NOTE: that the core renderer does NOT cull static batches...
	/// Culling of static batches should be handled manually.
	void
	CreateStaticBatchBuffer(	ID3D11Device* device, unsigned int const& batchId,
								std::shared_ptr<Renderable>& renderable, 
								std::shared_ptr<DiffuseMapped>& diffuse, 
								std::shared_ptr<NormalMapped>& normal,
								std::vector<InstancedData>& instancedData, 
								std::shared_ptr<ReflectionMapped>& reflection);

	/// Prepares all batches of instanced renderables so they can be rendered.
	void
	Update(ID3D11DeviceContext* d3dContext);

	InstancedBatchesResources&
	InstancedBatchesRsrcs(void) { return mBatches; }
		
	unsigned int const
	InstancedBufferStride(void) const { return sizeof(InstancedData); }

private:
	InstancedBatchesResources mBatches;

	std::map< unsigned int, std::vector<InstancedData> > mBatchInstancedData; // [batchId -> vector of InstancedData]

	std::set< unsigned int > mDynamicBatches;
};
