/**  ShadowSystem.h
 *
 *   Gets entities with ShadowEmitting component. 
 *   Used by rendering pipelines to generate shadow-maps.
 *
 *	A lot of the implementation is based on MJP's demo:
 *  http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/
 */

#pragma once

#include <wrl/client.h>
#include <d3d11.h>
#include <map>
#include <vector>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"
#include "Components/ShadowCasting.h"
#include "Components/Spatialized.h"
#include "Components/Renderable.h"
#include "System.h"

class LightEmitting;
class ShadowEmitting;
class Camera;

#define SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY 1

class ShadowSystem : public System
{
public:
	ShadowSystem(std::shared_ptr<EntityManager> entityManager);

	~ShadowSystem(void) {}
   
	void
	RunImplementation(std::set<unsigned int> const* family, double const /*currentTime*/);

	void
	InitializeResources();

	void
	UpdateClipDistances(ID3D11ShaderResourceView* const depthSrv);


private:	
	void
	GetDirectionalLightsFromFamily(std::set<unsigned int> const* family, std::set<unsigned int>& directionalLightsOut);

	void
	RenderDirectionalLightCascades(unsigned int const lightId, std::set<unsigned int> const& shadowCastingEntities);

	DirectX::XMMATRIX
	MakeDirectionalLightGlobalShadowMatrix(DirectX::XMFLOAT3 const& lightDirection);

	void
	RenderDepthOnlyNonInstanced(
		Camera* const shadowCamera,
		unsigned int const entityId, 
		Renderable* const renderable, 
		Spatialized* const spatialized);

	void
	RenderDepthAllInstanced(Camera* const shadowCamera);
		
	// This map stores the last time an entity with a ShadowEmitting entity has been updated.
	std::map<unsigned int, double> mLastUpdated;

	float mMinClip; // these are relative to the scene for the current frame (technically last frame)
	float mMaxClip; //     
	
	struct DepthReductionResources
	{
		// Buffers used to deduce min/max clip plane distances
		std::vector< Microsoft::WRL::ComPtr<ID3D11Texture2D> > tex2D;
		std::vector< Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> > uav;
		std::vector< Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> > srv;

		// Dispatch args to use for each reduction pass
		std::vector<unsigned int> dispX;
		std::vector<unsigned int> dispY;

		// Read-back buffers (we readback from previous frame and copy current for next)
		Microsoft::WRL::ComPtr<ID3D11Texture2D> stagingTex2D[SHADOW_SYSTEM_DEPTH_READBACK_FRAME_LATENCY+1];
	} mDepthReductionResources;

	Microsoft::WRL::ComPtr<ID3DBlob> mDepthReductionInitCsBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mDepthReductionInitCs;
	Microsoft::WRL::ComPtr<ID3DBlob> mDepthReductionCsBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mDepthReductionCs;

	struct ReductionCb
	{		
		DirectX::XMMATRIX projMat;
		float nearPlane;
		float farPlane;
		float pad[2];
	} mReductionConstantVariables;
	DirectX::ConstantBuffer< ReductionCb > mReductionConstants;

	unsigned int mCurrFrame;

	// This is a sub-system which is responsible for fetching any renderable entities that cast shadows
	class ShadowCastingFetcherSystem : public System
	{
	public:
		ShadowCastingFetcherSystem(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
		{
			// Set family req...
			std::set<unsigned int> fam_req;
			fam_req.insert(ShadowCasting::GetGuid());
			fam_req.insert(Spatialized::GetGuid());
			fam_req.insert(Renderable::GetGuid());
			SetFamilyRequirements(fam_req);
		}

		~ShadowCastingFetcherSystem(void) {}

		void
		RunImplementation(std::set<unsigned int> const* family, double const /*currentTime*/)
		{
			mShadowCastingEntities = *family;
		}

		std::set<unsigned int>&
		ShadowCastingEntities() { return mShadowCastingEntities; }

	private:
		std::set<unsigned int> mShadowCastingEntities;
	};
	std::shared_ptr<ShadowCastingFetcherSystem> mShadowCastingFetcherSys;
	unsigned int mShadowCastingFetcherSysId;


	Microsoft::WRL::ComPtr<ID3DBlob> mDepthOnlyVsBlob;
	Microsoft::WRL::ComPtr<ID3D11VertexShader> mDepthOnlyVs;
	Microsoft::WRL::ComPtr<ID3DBlob> mDepthOnlyInstancedVsBlob;
	Microsoft::WRL::ComPtr<ID3D11VertexShader> mDepthOnlyInstancedVs;

	struct DepthOnlyCb
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX viewProj;
	} mDepthOnlyConstantVariables;
	DirectX::ConstantBuffer< DepthOnlyCb > mDepthOnlyConstants;

	D3D11_BLEND_DESC mDepthOnlyBlendDesc;
	D3D11_DEPTH_STENCIL_DESC mDepthOnlyStencilDesc;
};