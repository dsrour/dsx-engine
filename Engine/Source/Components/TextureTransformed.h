/**  TextureTransformed.h
 *
 *   For entities that have texture transformation
 */

#pragma once

#include <DirectXMath.h>
#include "Component.hpp"

class TextureTransformed : public Component<TextureTransformed>
{
public:    
	TextureTransformed(void) { DirectX::XMStoreFloat4x4(&mXform, DirectX::XMMatrixIdentity()); }
	
	DirectX::XMFLOAT4X4&
	TextureTransformation(void) { return mXform; }
        
private:  
    DirectX::XMFLOAT4X4 mXform;
};