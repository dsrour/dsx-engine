#include "BaseApp.h"
#include "ParticleEmitting.h"

ParticleEmitting::ParticleEmitting(unsigned int numParticles, std::shared_ptr<D3dRenderer> renderer)
{
	mNumParticles = numParticles;

	// Create structured buffers, uavs, & srvs
	HRESULT hr;

	typedef struct
	{
		float posAndTime[4];
	} PosAndTimeBufferStruct;

	ZeroMemory(&mPosTimeBuffDesc, sizeof(mPosTimeBuffDesc));
	mPosTimeBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mPosTimeBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	mPosTimeBuffDesc.StructureByteStride = sizeof(PosAndTimeBufferStruct);
	mPosTimeBuffDesc.ByteWidth = mPosTimeBuffDesc.StructureByteStride * mNumParticles;
	mPosTimeBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mPosTimeBuffDesc.CPUAccessFlags = 0;	
	mPosTimeBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	hr = renderer->Device()->CreateBuffer(&mPosTimeBuffDesc, NULL, &mPosAndTimeBuffer);
	assert(SUCCEEDED(hr));

	ZeroMemory(&mPosTimeUavDesc, sizeof(mPosTimeUavDesc));
	mPosTimeUavDesc.Buffer.NumElements = mPosTimeBuffDesc.ByteWidth / mPosTimeBuffDesc.StructureByteStride;
	mPosTimeUavDesc.Format = DXGI_FORMAT_UNKNOWN;
	mPosTimeUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mPosTimeUavDesc.Buffer.FirstElement = 0;
	hr = renderer->Device()->CreateUnorderedAccessView((ID3D11Resource *)mPosAndTimeBuffer, &mPosTimeUavDesc, &mPosAndTimeUav);
	assert(SUCCEEDED(hr));

	ZeroMemory(&mPosTimeSrvDesc, sizeof(mPosTimeSrvDesc));
	mPosTimeSrvDesc.Buffer.ElementWidth = mPosTimeBuffDesc.StructureByteStride;
	mPosTimeSrvDesc.Buffer.FirstElement = mPosTimeUavDesc.Buffer.FirstElement;
	mPosTimeSrvDesc.Buffer.NumElements = mPosTimeUavDesc.Buffer.NumElements;
	mPosTimeSrvDesc.Format = DXGI_FORMAT_UNKNOWN;
	mPosTimeSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	hr = renderer->Device()->CreateShaderResourceView((ID3D11Resource *)mPosAndTimeBuffer, &mPosTimeSrvDesc, &mPosAndTimeSrv);



	typedef struct
	{
		float posAndTime[3];
	} DirBufferStruct;

	ZeroMemory(&mDirBuffDesc, sizeof(mDirBuffDesc));
	mDirBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mDirBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	mDirBuffDesc.StructureByteStride = sizeof(DirBufferStruct);
	mDirBuffDesc.ByteWidth = mDirBuffDesc.StructureByteStride * mNumParticles;
	mDirBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mDirBuffDesc.CPUAccessFlags = 0;
	mDirBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	hr = renderer->Device()->CreateBuffer(&mDirBuffDesc, NULL, &mDirBuffer);
	assert(SUCCEEDED(hr));

	ZeroMemory(&mDirUavDesc, sizeof(mDirUavDesc));
	mDirUavDesc.Buffer.NumElements = mDirBuffDesc.ByteWidth / mDirBuffDesc.StructureByteStride;
	mDirUavDesc.Format = DXGI_FORMAT_UNKNOWN;
	mDirUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mDirUavDesc.Buffer.FirstElement = 0;
	hr = renderer->Device()->CreateUnorderedAccessView((ID3D11Resource *)mDirBuffer, &mDirUavDesc, &mDirUav);
	assert(SUCCEEDED(hr));	
}

ParticleEmitting::~ParticleEmitting(void)
{
	mPosAndTimeBuffer->Release();
	mDirBuffer->Release();
	mPosAndTimeUav->Release();
	mPosAndTimeSrv->Release();
	mDirUav->Release();
}
