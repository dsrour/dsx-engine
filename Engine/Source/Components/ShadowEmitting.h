// For entities that generate shadows (eg. lights).

#pragma once

#include <windows.h>
#include <wrl/client.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Component.hpp"

class ShadowEmitting : public Component<ShadowEmitting>
{
public:
	ShadowEmitting(unsigned int const shadowMapSize = 2048)
	{
		// # of cascades is hard coded for now.
		// The reason is that it reduces the complexity of the rendering shader code.
		mNumCascades = NUM_SHADOW_CASCADES;
		mShadowMapSize = shadowMapSize;

		mStaticOffsetBias = 0.0005f;
		mNormalOffsetScaleBias = 0.f;
		mUsePlaneDepthBias = 0;
		mShadowIntensity = 1.f;

		Init();
	}

	DirectX::XMFLOAT4X4&
	ShadowMatrix() { return mShadowMatrix; }

	float&
	CascadeSplit(unsigned int const cascadeIndex) { return mCascadeSplits[cascadeIndex]; }

	DirectX::XMFLOAT4&
	CascadeOffset(unsigned int const cascadeIndex) { return mCascadeOffsets[cascadeIndex]; }

	DirectX::XMFLOAT4&
	CascadeScale(unsigned int const cascadeIndex) { return mCascadeScales[cascadeIndex]; }
			
private:
	void
	Init();

	unsigned int mNumCascades;
	unsigned int mShadowMapSize;
	
	struct ShadowMapResources
	{
		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView> dsv;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView> ro_dsv;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> srv;

		std::vector< Microsoft::WRL::ComPtr<ID3D11DepthStencilView> > slices;
	} mSmcResources;

	// Following are updated by the ShadowSystem class and used as constants within shaders
	DirectX::XMFLOAT4X4 mShadowMatrix;
	float mCascadeSplits[NUM_SHADOW_CASCADES];
	DirectX::XMFLOAT4 mCascadeOffsets[NUM_SHADOW_CASCADES];
	DirectX::XMFLOAT4 mCascadeScales[NUM_SHADOW_CASCADES];

	float mStaticOffsetBias;
	float mNormalOffsetScaleBias;
	int  mUsePlaneDepthBias;
	float mShadowIntensity;

public:
	unsigned int NumCascades() const { return mNumCascades; }
	unsigned int ShadowMapSize() const { return mShadowMapSize; }
	ShadowMapResources const& SmcResources() const { return mSmcResources; }	
	float StaticOffsetBias() const { return mStaticOffsetBias; }
	void StaticOffsetBias(float val) { mStaticOffsetBias = val; }
	float NormalOffsetScaleBias() const { return mNormalOffsetScaleBias; }
	void NormalOffsetScaleBias(float val) { mNormalOffsetScaleBias = val; }
	int UsePlaneDepthBias() const { return mUsePlaneDepthBias; }
	void UsePlaneDepthBias(int val) { mUsePlaneDepthBias = val; }
	float ShadowIntensity() const { return mShadowIntensity; }
	void ShadowIntensity(float val) { mShadowIntensity = val; }
};
