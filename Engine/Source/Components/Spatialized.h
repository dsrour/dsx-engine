/**  Spatialized.h
 *
 *   For entities that take up space.
 *   Has a transform and bound (size).
 */

#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>
#include <memory>
#include <list>
#include "Component.hpp"

class Spatialized : public Component<Spatialized>
{
public:    
    Spatialized(void);

    DirectX::XMFLOAT4X4 const& 
    LocalTransformation(void);   

	void
	LocalTransformation(DirectX::XMFLOAT4X4 const& xform);

	void
    LocalPosition(DirectX::XMFLOAT3 const& position);

    void
    LocalScale(DirectX::XMFLOAT3 const& scale);

	/// @param rot A quaternion of the rotation.
    void
    LocalRotation(DirectX::XMFLOAT4 const& rot);
        
    DirectX::XMFLOAT3&
    LocalPosition(void);

    DirectX::XMFLOAT3&
    LocalScale(void);

	/// @return A quaternion of the rotation.
    DirectX::XMFLOAT4&
    LocalRotation(void);

	bool const& 
	IsDirty(void) { return mDirty; }	
       
private:  
    DirectX::XMFLOAT3 mTranslation;
    DirectX::XMFLOAT4 mRotation;
    DirectX::XMFLOAT3 mScale;

    DirectX::XMFLOAT4X4  mLocalTransformation;       
	    
    bool mDirty; ///Tells us whether the local transform matrix needs to be remade due to changes to scale, trans., rot...
};