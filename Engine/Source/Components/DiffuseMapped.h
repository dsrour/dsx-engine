/// Used by entities that are rendered by GPU shaders that require a diffuse map.
/// NOTE:  Diffuse textures should be non-linear!

#pragma once

#include <string>
#include <d3d11.h>
#include "Component.hpp"

class DiffuseMapped : public Component<DiffuseMapped>
{
public:
	DiffuseMapped()
	{
		D3D11_SAMPLER_DESC desc;
		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.MipLODBias = 0.0f;
		desc.MaxAnisotropy = 1;
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		desc.BorderColor[0] = 0;
		desc.BorderColor[1] = 0;
		desc.BorderColor[2] = 0;
		desc.BorderColor[3] = 0;
		desc.MinLOD = 0;
		desc.MaxLOD = D3D11_FLOAT32_MAX;

		mDiffuseMapSamplerDesc = desc;
	}

    DiffuseMapped(std::string const& diffMapFile)
    {    
        D3D11_SAMPLER_DESC desc;
        desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.MipLODBias = 0.0f;
        desc.MaxAnisotropy = 1;
        desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
        desc.BorderColor[0] = 0;
        desc.BorderColor[1] = 0;
        desc.BorderColor[2] = 0;
        desc.BorderColor[3] = 0;
        desc.MinLOD = 0;
        desc.MaxLOD = D3D11_FLOAT32_MAX;

        mDiffuseMapFile = diffMapFile;
        mDiffuseMapSamplerDesc = desc;
    }

    DiffuseMapped(std::string const& diffMapFile, D3D11_SAMPLER_DESC const& diffMapSamplerDesc)
    {
        mDiffuseMapFile = diffMapFile;
        mDiffuseMapSamplerDesc = diffMapSamplerDesc;
    }

    /** @return File location string. */
    std::string const&
    DiffuseMapName(void) const { return mDiffuseMapFile; }

    /** @return Sampler desc of how to sample the diffused texture. */
    D3D11_SAMPLER_DESC const&
    DiffuseMapSamplerDesc(void) const { return mDiffuseMapSamplerDesc; }

    /** @param String of the file location. */
    void
    DiffuseMapName(std::string const& file) { mDiffuseMapFile = file; }

    /** @param Sampler desc of the diffused texture. */
    void
    DiffuseMapSamplerDesc(D3D11_SAMPLER_DESC const& samplerDesc) { mDiffuseMapSamplerDesc = samplerDesc; }
     
private:
    std::string         mDiffuseMapFile;
    D3D11_SAMPLER_DESC  mDiffuseMapSamplerDesc;
};