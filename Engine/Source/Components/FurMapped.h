/**  FurMapped.h
 *
 *   For entities that have fur.
 */

#pragma once

#include <string>
#include <vector>
#include <windows.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <random>
#include "Systems/Renderer/Renderer.h"
#include "Component.hpp"

class FurMapped : public Component<FurMapped>
{
public:    
	FurMapped();	
	FurMapped(uint32_t const randomSeed);
	~FurMapped();

	/*
	 * @params:
	 *  renderer:		renderer to use
	 *  texturesSize:	square texture 2d size (texturesSize x texturesSize)
	 *  numLayers:		number of layers
	 *  startDensity:	number of hair to start with at base
	 *  endDensity:		number of hair to end with at tip of strand
	 *  startAlpha:		alpha of hair to start with at base [0.f to 1.f]
	 *  endAlpha:		alpha of hair to end with at tip of strand [0.f to 1.f]
	 */
	void
	InitFurLayers(std::shared_ptr<D3dRenderer> renderer, unsigned int const texturesSize, unsigned int const numLayers,
					unsigned int const startDensity, unsigned int const endDensity,
					float const startAlpha, float const endAlpha);
       
	/// Releases all created resources
	void
	Reset();

// 	ID3D11Texture2D* const
// 	LayerTexture(unsigned int const layer) { return mLayers[layer]; }

	ID3D11ShaderResourceView* const
	LayerSrv(unsigned int const layer) { return mLayersSrvs[layer]; }

	float&
	FurLength() { return mFurLength; }

	DirectX::XMFLOAT3&
	Force() { return mForce; }


	unsigned int const
	NumLayers(void) const { return (unsigned int)mLayers.size(); }

	D3D11_SAMPLER_DESC const&
	SamplerDesc() const { return mSamplerDesc; }

	unsigned int const
	TexturesSize() const { return mTexturesSize; }

	unsigned int const
	StartDensity() const { return mStartDensity; }
	
	unsigned int const
	EndDensity() const { return mEndDensity; }

	float const
	StartAlpha() const { return mStartAlpha; }

	float const
	EndAlpha() const { return mEndAlpha; }

private:  
	D3D11_SAMPLER_DESC mSamplerDesc;	

	std::mt19937 mRng;
	uint32_t     mRndmSeedVal;

	std::vector< ID3D11Texture2D* >				mLayers;
	std::vector< ID3D11ShaderResourceView* >	mLayersSrvs;

	unsigned int mTexturesSize; 	
	unsigned int mStartDensity;
	unsigned int mEndDensity;
	float mStartAlpha;
	float mEndAlpha;

	DirectX::XMFLOAT3 mForce;
	
	float mFurLength;
};