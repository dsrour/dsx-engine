/// Used by entities that are rendered by GPU shaders that require a normal map.

#pragma once

#include <string>
#include <d3d11.h>
#include "Component.hpp"

class NormalMapped : public Component<NormalMapped>
{
public:
	NormalMapped()
	{
		D3D11_SAMPLER_DESC desc;
		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.MipLODBias = 0.0f;
		desc.MaxAnisotropy = 1;
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		desc.BorderColor[0] = 0;
		desc.BorderColor[1] = 0;
		desc.BorderColor[2] = 0;
		desc.BorderColor[3] = 0;
		desc.MinLOD = 0;
		desc.MaxLOD = D3D11_FLOAT32_MAX;
		mNormalMapSamplerDesc = desc;
	}

    NormalMapped(std::string const& normalMapFile)
    {    
        D3D11_SAMPLER_DESC desc;
        desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.MipLODBias = 0.0f;
        desc.MaxAnisotropy = 1;
        desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
        desc.BorderColor[0] = 0;
        desc.BorderColor[1] = 0;
        desc.BorderColor[2] = 0;
        desc.BorderColor[3] = 0;
        desc.MinLOD = 0;
        desc.MaxLOD = D3D11_FLOAT32_MAX;

        mNormalMapFile = normalMapFile;
        mNormalMapSamplerDesc = desc;
    }

    NormalMapped(std::string const& normalMapFile, D3D11_SAMPLER_DESC const& normalMapSamplerDesc)
    {
        mNormalMapFile = normalMapFile;
        mNormalMapSamplerDesc = normalMapSamplerDesc;
    }

    /** @return File location string. */
    std::string const&
    NormalMapName(void) const { return mNormalMapFile; }

    /** @return Sampler desc of how to sample the normal map. */
    D3D11_SAMPLER_DESC const&
    NormalMapSamplerDesc(void) const { return mNormalMapSamplerDesc; }

    /** @param String of the file location. */
    void
    NormalMapName(std::string const& file) { mNormalMapFile = file; }

    /** @param Sampler desc of the normal texture. */
    void
    NormalMapSamplerDesc(D3D11_SAMPLER_DESC const& samplerDesc) { mNormalMapSamplerDesc = samplerDesc; }
     
private:
    std::string         mNormalMapFile;
    D3D11_SAMPLER_DESC  mNormalMapSamplerDesc;
};