/**  Spatialized.cpp 
 */

#include <algorithm>
#include "Spatialized.h"

Spatialized::Spatialized(void)
{
    DirectX::XMStoreFloat4x4(&mLocalTransformation, DirectX::XMMatrixIdentity());
    mTranslation = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
    mScale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
    mRotation = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f);
    mDirty = true;    
}

DirectX::XMFLOAT4X4 const& 
Spatialized::LocalTransformation(void)
{
    if (mDirty)
    {
        mDirty = false;

        DirectX::XMMATRIX trans_mat = DirectX::XMMatrixTranslationFromVector(XMLoadFloat3(&mTranslation));
		DirectX::XMVECTOR rot_vec = XMLoadFloat4(&mRotation);
		if (1.f != mRotation.x )
		{
			rot_vec = DirectX::XMQuaternionNormalize(rot_vec);
			XMStoreFloat4(&mRotation, rot_vec);
		}
        DirectX::XMMATRIX rot_mat = DirectX::XMMatrixRotationQuaternion(rot_vec);
        DirectX::XMMATRIX scale_mat = DirectX::XMMatrixScalingFromVector(XMLoadFloat3(&mScale));

        DirectX::XMMATRIX new_xform = DirectX::XMMatrixIdentity();
        new_xform = XMMatrixMultiply(new_xform, scale_mat);
        new_xform = XMMatrixMultiply(new_xform, rot_mat);
        new_xform = XMMatrixMultiply(new_xform, trans_mat);

        XMStoreFloat4x4(&mLocalTransformation, new_xform);    
    }

    return mLocalTransformation;
}

void 
Spatialized::LocalTransformation(DirectX::XMFLOAT4X4 const& xform)
{	
	DirectX::XMVECTOR scale_tmp, rot_tmp, trans_tmp;
	DirectX::XMMatrixDecompose(&scale_tmp, &rot_tmp, &trans_tmp, DirectX::XMLoadFloat4x4(&xform));
	mRotation.x = DirectX::XMVectorGetX(rot_tmp);
	mRotation.y = DirectX::XMVectorGetY(rot_tmp);
	mRotation.z = DirectX::XMVectorGetZ(rot_tmp);
	mRotation.w = DirectX::XMVectorGetW(rot_tmp);
	mTranslation.x = DirectX::XMVectorGetX(trans_tmp);
	mTranslation.y = DirectX::XMVectorGetY(trans_tmp);
	mTranslation.z = DirectX::XMVectorGetZ(trans_tmp);
	mScale.x = DirectX::XMVectorGetX(scale_tmp);
	mScale.y = DirectX::XMVectorGetY(scale_tmp);
	mScale.z = DirectX::XMVectorGetZ(scale_tmp);
	mDirty = true;
}

void
Spatialized::LocalPosition(DirectX::XMFLOAT3 const& position)
{
    mTranslation = position;
    mDirty = true;
}

void
Spatialized::LocalScale(DirectX::XMFLOAT3 const& scale)
{
    mScale = scale;
    mDirty = true;
}

void
Spatialized::LocalRotation(DirectX::XMFLOAT4 const& rot)
{
    mRotation = rot; 
    mDirty = true;
}

DirectX::XMFLOAT3&
Spatialized::LocalPosition(void)
{    
	mDirty = true;
    return mTranslation;
}

DirectX::XMFLOAT3&
Spatialized::LocalScale(void)
{
	mDirty = true;
    return mScale;
}

DirectX::XMFLOAT4&
Spatialized::LocalRotation(void)
{
	mDirty = true;
    return mRotation;
}