#include <assert.h>
#include <algorithm>
#include "FurMapped.h"

FurMapped::FurMapped()
{
	mRndmSeedVal = 12321;
	mFurLength = 1.f;
	mForce = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

	D3D11_SAMPLER_DESC desc;
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.MipLODBias = 0.0f;
	desc.MaxAnisotropy = 1;
	desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	desc.BorderColor[0] = 0;
	desc.BorderColor[1] = 0;
	desc.BorderColor[2] = 0;
	desc.BorderColor[3] = 0;
	desc.MinLOD = 0;
	desc.MaxLOD = D3D11_FLOAT32_MAX;
		
	mSamplerDesc = desc;
}

FurMapped::FurMapped(uint32_t const randomSeed)
{
	mRndmSeedVal = randomSeed;
	mFurLength = 1.f;
	mForce = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

	D3D11_SAMPLER_DESC desc;
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.MipLODBias = 0.0f;
	desc.MaxAnisotropy = 1;
	desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	desc.BorderColor[0] = 0;
	desc.BorderColor[1] = 0;
	desc.BorderColor[2] = 0;
	desc.BorderColor[3] = 0;
	desc.MinLOD = 0;
	desc.MaxLOD = D3D11_FLOAT32_MAX;

	mSamplerDesc = desc;
}

FurMapped::~FurMapped()
{
	Reset();
}

void 
FurMapped::InitFurLayers(std::shared_ptr<D3dRenderer> renderer, unsigned int const texturesSize, unsigned int const numLayers,
						 unsigned int const startDensity, unsigned int const endDensity, 
						 float const startAlpha, float const endAlpha)
{	
	Reset();

	mTexturesSize = texturesSize;
	mStartDensity = startDensity;
	mEndDensity = endDensity;
	mStartAlpha = startAlpha;
	mEndAlpha = endAlpha;


	std::uniform_int_distribution<unsigned int> uint_dist(0, texturesSize-1);

	int density_step = ((std::max)(startDensity, endDensity) - (std::min)(startDensity, endDensity)) / numLayers;
	if (endDensity < startDensity)
		density_step *= -1;

	float alpha_step = ((std::max)(startAlpha, endAlpha) - (std::min)(startAlpha, endAlpha)) / (float)numLayers;
	if (endAlpha < startAlpha)
		alpha_step *= -1.f;

	for (unsigned int cur_layer = 0; cur_layer < numLayers; cur_layer++)
	{
		// Reseed so that the sequences are always the same for each layer.... (we want continuous strands of fur)
		mRng.seed(mRndmSeedVal);

		float cur_alpha = startAlpha + (cur_layer*alpha_step);
		int cur_density = startDensity + (cur_layer*density_step);

		std::vector<float> layer(texturesSize*texturesSize, 0.f);

		for (int cur_strand_num = 0; cur_strand_num < cur_density; cur_strand_num++)
		{
			unsigned int x = uint_dist(mRng);
			unsigned int y = uint_dist(mRng);

			layer[(y*texturesSize) + x] = cur_alpha;
		}
		
		// Create resources
		HRESULT hr;

		ID3D11Texture2D* texture = nullptr;
		D3D11_TEXTURE2D_DESC tex_desc;
		ZeroMemory(&tex_desc, sizeof(D3D11_TEXTURE2D_DESC));
		tex_desc.ArraySize = 1;
		tex_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		tex_desc.CPUAccessFlags = 0;
		tex_desc.Format = DXGI_FORMAT_R32_FLOAT;
		tex_desc.Height = texturesSize;
		tex_desc.MipLevels = 1;
		tex_desc.MiscFlags = 0;
		tex_desc.SampleDesc.Count = 1;
		tex_desc.SampleDesc.Quality = 0;
		tex_desc.Usage = D3D11_USAGE_DEFAULT;
		tex_desc.Width = texturesSize;

		D3D11_SUBRESOURCE_DATA texture_init_data;
		ZeroMemory(&texture_init_data, sizeof(D3D11_SUBRESOURCE_DATA));
		texture_init_data.pSysMem = &layer[0];
		texture_init_data.SysMemPitch = sizeof(float) * texturesSize;
		texture_init_data.SysMemSlicePitch = texture_init_data.SysMemPitch * texturesSize;
		
		hr = renderer->Device()->CreateTexture2D(&tex_desc, &texture_init_data, &texture);
		assert(SUCCEEDED(hr));
		mLayers.push_back(texture);

		ID3D11ShaderResourceView* srv;
		hr = renderer->Device()->CreateShaderResourceView(texture, NULL, &srv);
		assert(SUCCEEDED(hr));
		mLayersSrvs.push_back(srv);
	}
}

void 
FurMapped::Reset()
{
	for (auto texture : mLayers)
		if (texture)
			texture->Release();

	for (auto srv : mLayersSrvs)
		if (srv)
			srv->Release();

	mLayers.clear();
	mLayersSrvs.clear();
}

