/**  ParticleEmitting.h
 *
 *   For entities that emit particles.
 */

#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <assert.h>
#include "Systems/Renderer/Renderer.h"
#include "Component.hpp"

class ParticleEmitting : public Component<ParticleEmitting>
{
public:    
	ParticleEmitting(unsigned int numParticles, std::shared_ptr<D3dRenderer> renderer);	
	~ParticleEmitting(void);

	float SpawnTimeOffset() const { return mSpawnTimeOffset; }
	void SpawnTimeOffset(float val) { mSpawnTimeOffset = val; }

	DirectX::XMFLOAT3 SpawnDirection() const { return mSpawnDirection; }
	void SpawnDirection(DirectX::XMFLOAT3 val) { mSpawnDirection = val; }
        
	float RespawnDelay() const { return mRespawnDelay; }
	void RespawnDelay(float val) { mRespawnDelay = val; }

	float LifeTime() const { return lifeTime; }
	void LifeTime(float val) { lifeTime = val; }

	float Gravity() const { return gravity; }
	void Gravity(float val) { gravity = val; }

	DirectX::XMFLOAT3 ColorSpawn() const { return mColorSpawn; }
	void ColorSpawn(DirectX::XMFLOAT3 val) { mColorSpawn = val; }

	DirectX::XMFLOAT3 ColorDie() const { return mColorDie; }
	void ColorDie(DirectX::XMFLOAT3 val) { mColorDie = val; }

	float ParticleSize() const { return mParticleSize; }
	void ParticleSize(float val) { mParticleSize = val; }

	unsigned int NumParticles() const { return mNumParticles; }	

	ID3D11Buffer* const
	PositionAndTimeBuffer() { return mPosAndTimeBuffer; }

	ID3D11Buffer* const
	DirectionBuffer() {	return mDirBuffer; }

	ID3D11UnorderedAccessView* const
	PositionAndTimeUav() { return mPosAndTimeUav; }

	ID3D11ShaderResourceView* const
	PositionAndTimeSrv() { return mPosAndTimeSrv; }

	ID3D11UnorderedAccessView* const
	DirectionUav()	{ return mDirUav; }


private:  	
	float  mSpawnTimeOffset;			// Time difference between consecutive spawning particles.  The smaller this is, the more of a stream appearance there'll be.
	DirectX::XMFLOAT3 mSpawnDirection;	// Direction to spawn particles towards.
	float  mRespawnDelay;				// Death duration.			
	float  lifeTime;					// How long a particle lives before disappearing.
	float  gravity;						// Gravity.

	DirectX::XMFLOAT3   mColorSpawn;	// Spawn color
	DirectX::XMFLOAT3   mColorDie;		// Death color

	float				mParticleSize;  // Size of particle (billboard created will be mParticleSize X mParticleSize)	
	unsigned int		mNumParticles;  // Number of particles emitted.  Has to be a multiple of "GRP_DIM" in ParticlRenderer.fx.
										// Max value is (D3D11_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION *  GRP_DIM).		

	D3D11_BUFFER_DESC mPosTimeBuffDesc;
	D3D11_UNORDERED_ACCESS_VIEW_DESC mPosTimeUavDesc;

	D3D11_SHADER_RESOURCE_VIEW_DESC mPosTimeSrvDesc;

	D3D11_BUFFER_DESC mDirBuffDesc;
	D3D11_UNORDERED_ACCESS_VIEW_DESC mDirUavDesc;

	ID3D11Buffer* mPosAndTimeBuffer;
	ID3D11Buffer* mDirBuffer;
	ID3D11UnorderedAccessView* mPosAndTimeUav;
	ID3D11UnorderedAccessView* mDirUav;
	ID3D11ShaderResourceView* mPosAndTimeSrv;
};