/// Renderable entities emitting lights that are used in shader lighting pipeline.


#pragma once

#include <assert.h>
#include <DirectXMath.h>
#include "Component.hpp"

/// Type of light.
enum LightType
{
    DIRECTIONAL_LIGHT,
    POINT_LIGHT,
    SPOT_LIGHT,
};

/// Base light structure.
struct Light
{
    Light() { ZeroMemory(this, sizeof(this)); }
};

/// Directional light.
struct DirectionalLight : public Light
{    
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;
    DirectX::XMFLOAT3 direction;   
	float    lightSize;

	DirectionalLight() 
	{ 
		direction = DirectX::XMFLOAT3(0.f, 0.f, 1.f);
		ambient = diffuse = specular = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f); 
		lightSize = 0.f;
	}
};

/// Point light.
struct PointLight : public Light
{   
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;
    
    DirectX::XMFLOAT3 position;
    float    range;

    DirectX::XMFLOAT3 attenuation;    
    float    pad;

	PointLight() 
	{ 
		position = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		ambient = diffuse = specular = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f); 
		attenuation = DirectX::XMFLOAT3(0.f, 1.f, 0.f); 
		range = 500.f;
	}
};

/// Spot light.
struct SpotLight : public Light
{   
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;

    DirectX::XMFLOAT3 position;
    float    range;

    DirectX::XMFLOAT3 direction;
    float    spot;

    DirectX::XMFLOAT3 attenuation;
    float    pad;

	SpotLight() 
	{ 
		position = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		direction = DirectX::XMFLOAT3(0.f, 0.f, 1.f);
		ambient = diffuse = specular = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f); 
		attenuation = DirectX::XMFLOAT3(0.f, 1.f, 0.f); 
		range = 500.f;
		spot = 180.f;
	}
};

class LightEmitting : public Component<LightEmitting>
{
public:
    LightEmitting(void)  { mType = POINT_LIGHT; mLight = NULL; }

    ~LightEmitting(void) { if (mLight) delete mLight; }

    /** @return Light type. */
    LightType const&
    Type(void) const { return mType; }

    /** @return Pointer to light structure. */
    Light* const
    GetLight(void) const { return mLight; }

    /** @param pointLight A point light. */
    void
    SetPointLight(PointLight const& pointLight)
    {
		if (mLight && mType != POINT_LIGHT)
		{
			delete mLight;
			mLight = nullptr;
		}

		if (!mLight)
		{
			mLight = new PointLight;
			mType = POINT_LIGHT;
		}
	
        *(static_cast<PointLight*>(mLight)) = pointLight;       
    }

    /** @param directionalLight A directional light. */
    void
    SetDirectionalLight(DirectionalLight const& directionalLight)
    {
		if (mLight && mType != DIRECTIONAL_LIGHT)
		{
			delete mLight;
			mLight = nullptr;
		}
		
		if (!mLight)
		{
			mLight = new DirectionalLight;
			mType = DIRECTIONAL_LIGHT;
		}
		
		*(static_cast<DirectionalLight*>(mLight)) = directionalLight;
    }

    /** @param spotLight A spot light. */
    void
    SetSpotLight(SpotLight const& spotLight)
    {
		if (mLight && mType != SPOT_LIGHT)
		{
			delete mLight;
			mLight = nullptr;
		}

		if (!mLight)
		{
			mLight = new SpotLight;
			mType = SPOT_LIGHT;
		}

		*(static_cast<SpotLight*>(mLight)) = spotLight;
    }

protected:
    LightType  mType;
    Light*     mLight;
};