/// Used by entities that are rendered by GPU shaders that require a reflection map.
/// NOTE:  Reflection textures should be cubemaps and non-linear!

#pragma once

#include <string>
#include <d3d11.h>
#include "Component.hpp"

class ReflectionMapped : public Component<ReflectionMapped>
{
public:
	ReflectionMapped()
	{
		D3D11_SAMPLER_DESC desc;
		desc.Filter = D3D11_FILTER_ANISOTROPIC;
		desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		desc.MipLODBias = 0.0f;
		desc.MaxAnisotropy = 4;
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		desc.BorderColor[0] = 0;
		desc.BorderColor[1] = 0;
		desc.BorderColor[2] = 0;
		desc.BorderColor[3] = 0;
		desc.MinLOD = 0;
		desc.MaxLOD = D3D11_FLOAT32_MAX;
				
		mReflectionMapSamplerDesc = desc;
	}

    ReflectionMapped(std::string const& reflectionMapFile)
    {    
        D3D11_SAMPLER_DESC desc;
        desc.Filter = D3D11_FILTER_ANISOTROPIC;
        desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
        desc.MipLODBias = 0.0f;
        desc.MaxAnisotropy = 4;
        desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
        desc.BorderColor[0] = 0;
        desc.BorderColor[1] = 0;
        desc.BorderColor[2] = 0;
        desc.BorderColor[3] = 0;
        desc.MinLOD = 0;
        desc.MaxLOD = D3D11_FLOAT32_MAX;

        mReflectionMapFile = reflectionMapFile;
        mReflectionMapSamplerDesc = desc;
    }

    ReflectionMapped(std::string const& reflectionMapFile, D3D11_SAMPLER_DESC const& reflectionMapSamplerDesc)
    {
        mReflectionMapFile = reflectionMapFile;
        mReflectionMapSamplerDesc = reflectionMapSamplerDesc;
    }

    /** @return File location string. */
    std::string const&
    ReflectionMapName(void) const { return mReflectionMapFile; }

    /** @return Sampler desc of how to sample the diffused texture. */
    D3D11_SAMPLER_DESC const&
    ReflectionMapSamplerDesc(void) const { return mReflectionMapSamplerDesc; }

    /** @param String of the file location. */
    void
    ReflectionMapName(std::string const& file) { mReflectionMapFile = file; }

    /** @param Sampler desc of the diffused texture. */
    void
    ReflectionMapSamplerDesc(D3D11_SAMPLER_DESC const& samplerDesc) { mReflectionMapSamplerDesc = samplerDesc; }
     
private:
    std::string         mReflectionMapFile;
    D3D11_SAMPLER_DESC  mReflectionMapSamplerDesc;
};