#include "BaseApp.h"
#include "ShadowEmitting.h"

extern BaseApp* gApp;


void
ShadowEmitting::Init()
{
	//assert(mNumCascades > 1); // as to restrict to Texture2DArray

	auto const depth_typeless_format = DXGI_FORMAT_R32_TYPELESS;
	auto const dsv_format = DXGI_FORMAT_D32_FLOAT;
	auto const srv_format = DXGI_FORMAT_R32_FLOAT;

	auto device = gApp->Renderer()->Device();
	auto device_c = gApp->Renderer()->DeviceContext();

	D3D11_TEXTURE2D_DESC tex_desc;
	tex_desc.Width = mShadowMapSize;
	tex_desc.Height = mShadowMapSize;
	tex_desc.ArraySize = mNumCascades;
	tex_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	tex_desc.CPUAccessFlags = 0;
	tex_desc.Format = depth_typeless_format;
	tex_desc.MipLevels = 1;
	tex_desc.MiscFlags = 0;
	tex_desc.SampleDesc.Count = 1;
	tex_desc.SampleDesc.Quality = 0;
	tex_desc.Usage = D3D11_USAGE_DEFAULT;
	HRESULT hr = device->CreateTexture2D(&tex_desc, nullptr, mSmcResources.texture.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mSmcResources.slices.clear();

	for (auto i = 0u; i < mNumCascades; i++)
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView> ds_view;

		dsv_desc.Format = dsv_format;
		dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
		dsv_desc.Texture2DArray.ArraySize = 1;
		dsv_desc.Texture2DArray.FirstArraySlice = i;
		dsv_desc.Texture2DArray.MipSlice = 0;
		dsv_desc.Flags = 0;
		hr = device->CreateDepthStencilView(
			mSmcResources.texture.Get(),
			&dsv_desc,
			ds_view.ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		mSmcResources.slices.push_back(ds_view);

		// create read-only
		if (0 == i)
		{
			dsv_desc.Flags = D3D11_DSV_READ_ONLY_DEPTH;
			hr = device->CreateDepthStencilView(
				mSmcResources.texture.Get(),
				&dsv_desc,
				mSmcResources.ro_dsv.ReleaseAndGetAddressOf());
			assert(SUCCEEDED(hr));
		}
	}

	mSmcResources.dsv = mSmcResources.slices[0];

	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	srv_desc.Format = srv_format;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srv_desc.Texture2DArray.ArraySize = mNumCascades;
	srv_desc.Texture2DArray.FirstArraySlice = 0;
	srv_desc.Texture2DArray.MipLevels = 1;
	srv_desc.Texture2DArray.MostDetailedMip = 0;
	hr = device->CreateShaderResourceView(
		mSmcResources.texture.Get(),
		&srv_desc,
		mSmcResources.srv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
}
