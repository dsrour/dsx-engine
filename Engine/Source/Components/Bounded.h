/**  Bounded.h
 *
 *   For entities that take up space.
 *   Has a bounding volume.
 */

#pragma once

#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>
#include "Component.hpp"

class Bounded : public Component<Bounded>
{
public:    
	Bounded(void) {}

	Bounded( DirectX::BoundingBox const& aabb) { AabbBounds(aabb); }
	    
    DirectX::BoundingBox&
    AabbBounds(void) { return mAabbBounds; } 

    void
	AabbBounds(DirectX::BoundingBox const& aabb) { mAabbBounds = aabb; }
        
private:  
    DirectX::BoundingBox mAabbBounds;
};