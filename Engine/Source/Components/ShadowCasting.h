// For entities with renderables that cast shadows.

#pragma once

#include "Component.hpp"

class ShadowCasting : public Component<ShadowCasting>
{
public:
	ShadowCasting() { }
	~ShadowCasting() { }
};