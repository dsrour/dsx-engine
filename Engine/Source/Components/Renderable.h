/// Attributes of a renderable entity.
/** Holds attributes such as vertices, indices, and settings 
    regarding how the renderable should be rendered for the current 
    pipeline. */

#pragma once

#include <map>
#include <string>
#include <vector>
#include <d3d11.h>
#include <assert.h>
#include <DirectXMath.h>
#include "Systems/Renderer/InputLayoutManager.h"
#include "Component.hpp"

// LIGHTING MODELS.  THESE SHOULD REFLECT THE SAME DEFINES IN "Lighting.h" SHADER CODE
#define PHONG			0
#define COOK_TORRANCE	1

/// A material structure holding ambient diffuse specular and reflective attributes.
struct Material
{
	Material(void) { ZeroMemory(this, sizeof(this)); reflectionCoeff = 1.f; lightingModel = PHONG; }

    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular; // w = spec brightness (Phong), roughness (Cook-Torrance). [0, 1]
    DirectX::XMFLOAT4 reflect;
	float			  reflectionCoeff; // for Fresnel term in Cook-Torrance
	unsigned int	  lightingModel;
	DirectX::XMFLOAT2 pad;
};

/// An index buffer description structure.
struct IndexBufferDesc
{
    IndexBufferDesc(void)
    {
        indexBuffer = NULL;
        indexBufferFormat = DXGI_FORMAT_R32_UINT;
        indexBufferOffset = 0;                
        indexCount = 0;
    }

    ID3D11Buffer*   indexBuffer;	
    unsigned int    indexCount;
    DXGI_FORMAT     indexBufferFormat;
    unsigned int    indexBufferOffset;
}; 

/// An vertex buffer description structure.
struct VertexBufferDesc
{
    VertexBufferDesc(void)
    {
        vertexBuffer = NULL;                
        vertexCount = 0;
    }

    ID3D11Buffer*               vertexBuffer;
    unsigned int                stride;
    unsigned int                offset;    
    unsigned int                vertexCount;
}; 

class Renderable : public Component<Renderable>
{
public:
	////// METADATA ///////////////////////////////////////////////////////////////////////////////////
	struct IndexBufferMetadata
	{
		D3D11_BUFFER_DESC			indexBufferDesc;
		std::vector<unsigned int>	indexBuffer;
		unsigned int				numIndices;
	};

	struct GeometryBufferMetadata
	{
		D3D11_BUFFER_DESC	vertexBufferDesc;
		std::vector<GeometryInputLayout>	vertsBuffer;
		unsigned int		numVerts;
	};

	struct TextureBufferMetadata
	{
		D3D11_BUFFER_DESC		textureBufferDesc;
		std::vector<TextureInputLayout>	textureBuffer;
		unsigned int			numCoords;
	};

	struct Metadata
	{
		IndexBufferMetadata    indexMetadata;
		IndexBufferMetadata    adjacencyIndexMetadata;
		GeometryBufferMetadata geomMetadata;
		TextureBufferMetadata  textureMetadata;
	};
	///////////////////////////////////////////////////////////////////////////////////////////////////

	/// What the renderable represents.  This is so that the appropriate pipeline knows whether to process the entity or not.
    enum RenderableType
    {
        LIT,   
        FLAT,
		SKY, 
		POINT_CLOUD,
		PARTICLES,
		FUR,
    };

    Renderable(void) 
	{ 
		mRenderableType = LIT; 
		mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; 
		mInstancingBatch = -1; 
		mBackCulled = true; 
		mInstancedColor = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
		mStoreMetadata = false;
	}

    ~Renderable(void) 
    {
        std::map<InputLayoutManager::SubInputLayout, VertexBufferDesc>::iterator iter;
        for (iter = mVertexBuffers.begin(); iter != mVertexBuffers.end(); iter++)
        {
            if (iter->second.vertexBuffer)
                iter->second.vertexBuffer->Release();
        }

        if (mIndexBuffer.indexBuffer)
            mIndexBuffer.indexBuffer->Release();


		if (mAdjacencyIndexBuffer.indexBuffer)
			mAdjacencyIndexBuffer.indexBuffer->Release();
    }

    /** @return Reference to the index buffer description structure. */
    inline IndexBufferDesc&
    IndexBuffer(void);

    /** @return Reference to the vertex buffer description structure. */
    inline VertexBufferDesc&
    VertexBuffer(InputLayoutManager::SubInputLayout const& vertexBufferType);

    /** @return Pipeline settings.  Long int represent an enumeration of RenderablePipelineSettings. */
    inline unsigned int const&
    RenderableType(void) const;

    /** @param settings Pipeline settings. */
    inline void
    RenderableType(unsigned int const& settings);

	inline void
	AddVertexBuffer(InputLayoutManager::SubInputLayout const& type, VertexBufferDesc const& vertexBufferType);
		
	inline void
	PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY const& topology);

    /** @return The primitive topology.  Note that some pipelines might not support certain topologies. */
    inline D3D11_PRIMITIVE_TOPOLOGY const&
    PrimitiveTopology(void) const;

	inline bool const&
	BackCulled(void) const;

	inline void
	BackCulled(bool const& backCulled);

    /** @return Reference to the material attributes structure. */
    Material&
    MaterialProperties(void) { return mMaterial; }

    /** @param mat Material properties. */
    void
    MaterialProperties(Material const& mat) { mMaterial = mat; }

	void
	DynamicInstancedBatchId(unsigned int const& id) { mInstancingBatch = id; }

	void
	InstancedColor(DirectX::XMFLOAT4 const& color) { mInstancedColor = color; }

	int const&
	DynamicInstancedBatchId(void) const { return mInstancingBatch; }

	DirectX::XMFLOAT4 const&
	InstancedColor(void) const { return mInstancedColor; }

	bool const
	HasMetadata() const { return mStoreMetadata; }

	/// TODO:: The following setter is kinda hacky... what if client says it has metadata but it never was set?
	void
	HasMetadata(bool const hasMeta) { mStoreMetadata = hasMeta; }

	Metadata&
	GetMetadata() { return mMetadata; }
		    
protected:                    
	std::map<InputLayoutManager::SubInputLayout, VertexBufferDesc>  mVertexBuffers;
    IndexBufferDesc                                                 mIndexBuffer;
	IndexBufferDesc													mAdjacencyIndexBuffer;  // an index buffer with adjacency information... useful in geometry shader for  extracting volumes
		
    unsigned int                                                    mRenderableType;

    D3D11_PRIMITIVE_TOPOLOGY                                        mPrimitiveTopology;

    Material                                                        mMaterial;	

	bool															mBackCulled;

	int																mInstancingBatch; // if >= 0, renderer will add the belonging entity to the dynamic instancing batch of this id
	DirectX::XMFLOAT4												mInstancedColor; // used when geometry is instanced

	Metadata mMetadata;
	bool mStoreMetadata;
};

inline IndexBufferDesc&
Renderable::IndexBuffer(void)
{
    return mIndexBuffer;
}

inline VertexBufferDesc&
Renderable::VertexBuffer(InputLayoutManager::SubInputLayout const& vertexBufferType)
{
    std::map<InputLayoutManager::SubInputLayout,VertexBufferDesc>::iterator desc = mVertexBuffers.find(vertexBufferType);
    assert( desc != mVertexBuffers.end() );
    return desc->second;
}

inline void
Renderable::AddVertexBuffer(InputLayoutManager::SubInputLayout const& type, VertexBufferDesc const& vertexBufferDesc)
{
	mVertexBuffers[type] = vertexBufferDesc;
}

unsigned int const&
Renderable::RenderableType(void) const
{
    return mRenderableType;
}

inline void
Renderable::RenderableType(unsigned int const& type) 
{
    mRenderableType = type;
}

inline D3D11_PRIMITIVE_TOPOLOGY const&
Renderable::PrimitiveTopology(void) const
{
    return mPrimitiveTopology;
}

inline bool const&
Renderable::BackCulled(void) const
{
	return mBackCulled;
}

inline void
Renderable::BackCulled(bool const& backCulled)
{
	mBackCulled = backCulled;
}

inline void
Renderable::PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY const& topology)
{
	mPrimitiveTopology = topology;
}
