/**  SystemManager.cpp
 */

#include "GuidGenerator.h"
#include "Debug/Debug.h"
#include "SystemManager.h"

SystemManager::~SystemManager(void)
{
    // Memory will be auto cleaned if last ref of shared_ptr's in map.
    mSystems.clear();
}

unsigned int const
SystemManager::AddSystem(std::shared_ptr<System> system)
{
    unsigned int id = GuidGenerator::GenerateGuid();
    mSystems[id] = system;
    
    system->OnRegister();

    return id;
}

void
SystemManager::RemoveSystem(unsigned int const systemId)
{
    std::map< unsigned int, std::shared_ptr<System> >::iterator to_remove;
    to_remove = mSystems.find(systemId);

    if (mSystems.end() == to_remove)
    {
        OutputDebugMsg("SystemManager::RemoveSystem(uint), bad system id.\n");
        return;
    }

    to_remove->second->OnUnregister();

    // smart pointer will delete system if it's the last ref
    mSystems.erase(to_remove);
}

void
SystemManager::RunSystem(unsigned int const systemId, double const currentTime)
{
    std::map< unsigned int, std::shared_ptr<System> >::iterator to_run;
    to_run = mSystems.find(systemId);

    if (mSystems.end() == to_run)
    {
        OutputDebugMsg("SystemManager::RunSystem(uint), bad system id(" + to_string(systemId) + ").\n");
        return;
    }

    to_run->second->Run(currentTime);
}