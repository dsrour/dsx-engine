/**  SystemScheduler.h
 *
 *  Provides functionalities to schedule when to run a system.
 */

#ifndef _EVENT_MANAGER_
#define _EVENT_MANAGER_

#include <set>
#include <map>
#include "SystemManager.h"

class SystemScheduler 
{
public:
    SystemScheduler(std::shared_ptr<SystemManager> systemManager);

    void
    Advance(double const currentTime);

	/// @param timeStepSimulated if false, system is ran once, if true ran multiple times with time step simulation (ex. renderer only needs to render once even if late)
    void
    RunSystemEveryDelta(unsigned int const systemId, double const delta, bool const timeStepSimulated = false);

    void
    RunSystemInDelta(unsigned int const systemId, double const delta);

    void
    RunSystemNow(unsigned int const systemId, double const currentTime = 0.0);

    void
    StopSystem(unsigned int const systemId);
            
private:    
    double mLastTime;

    struct SystemTimeInfo
    {      
        double systemTime;
        double accumulator;
        double delta;
		bool   timeStepSimulated;
    };
    
    std::shared_ptr<SystemManager>          mSystemManager; 
    std::map<unsigned int, SystemTimeInfo>  mPeriodicalEvents;	
    std::map<unsigned int, double>          mUniqueEvents; // system->time to do event
};

#endif /*_EVENT_MANAGER_*/