#include <windows.h>
#include <set>
#include "StateManager.h"
#include "SceneEditor.h"
#include "Component.hpp"
#include "Scene/SceneObjectManager.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Scene/SceneLoaderWriter.h"
#include "Settings.h"
#include "EventNames.h"
#include "KeyboardActions.h"

ApplicationKeyboardActions::ApplicationKeyboardActions(
	std::shared_ptr<InputDeviceManager> inputDeviceManager, 
	std::shared_ptr<SceneObjectManager> soMngr,
	std::shared_ptr<StateManager> stateMngr) : InputDeviceActions(inputDeviceManager)
{        
	mSoMngr = soMngr;
	mStateMngr = stateMngr;
	mCamera = gApp->Renderer()->CurrentCamera();        		
}   

void
ApplicationKeyboardActions::OnStateChange()
{	
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if ( manager->Keyboard()->IsKeyDown(VK_ESCAPE) )
	{
		gApp->EndEngine();
	}

	// Save scene
	//if (manager->Keyboard()->IsKeyUp(VK_F6))
	//{		
	//	SceneLoaderWriter* slw = new SceneLoaderWriter;
	//	slw->WriteSceneToFile(LOAD_SAVE_PATH, mSoMngr);
	//	delete slw;
	//}

	// Load scene
	//if (manager->Keyboard()->IsKeyUp(VK_F9))
	//{
	//	SceneEditor* scene_editor = static_cast<SceneEditor*>(gApp);

	//	// Clear scene of scene objects				
	//	std::set<unsigned int> ids = mSoMngr->SceneObjectsIds();
	//	mSoMngr->RemoveSceneObjects(ids);

	//	scene_editor->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);

	//	// Lights to be created when loading scene
	//	std::set<unsigned int> light_entities;

	//	// Bounds of instancing batches that'll be created
	//	std::map<unsigned int, DirectX::BoundingBox> instancing_batches;

	//	SceneLoaderWriter* slw = new SceneLoaderWriter;
	//	slw->LoadSceneFromFile(LOAD_SAVE_PATH, mSoMngr, light_entities, instancing_batches);
	//	delete slw;			

	//	//// Reset light menu
	//	//scene_editor->GetLightMenu()->ResetFromEntities(light_entities);

	//	//// Reset instancing menu
	//	//scene_editor->GetInstancingMenu()->ManualReset(instancing_batches);

	//	//// Reset fog menu
	//	//scene_editor->GetFogMenu()->ResetFromRenderer();

	//	// Unselect everything
	//	scene_editor->SceneObjectMngr()->UnselectSceneObjects( scene_editor->SceneObjectMngr()->CurrentlySelectedSceneObjects() );		
	//}

	// Recompile shaders... debug on broski
	if (manager->Keyboard()->IsKeyUp(VK_F11))
	{
		for (std::list< std::shared_ptr<Pipeline> >::const_iterator iter = gApp->Renderer()->RenderPath().begin();
			iter != gApp->Renderer()->RenderPath().end();
			++iter)
			(*iter)->RecompileShaders();
	}

	// Toggle fps
	if (manager->Keyboard()->IsKeyUp(VK_HOME))
	{		
		static bool show_fps_on = false;	
		show_fps_on = !show_fps_on;
		
		mStateMngr->ToggleState(SHOW_FPS, show_fps_on);
	}

	// Incr/Decr nav speed
	if (manager->Keyboard()->IsKeyUp(VK_PRIOR))
	{
		auto orig_nav = static_cast<SceneEditor*>(gApp)->NavSpeedFactor();
		static_cast<SceneEditor*>(gApp)->NavSpeedFactor(orig_nav*2.f);
	}
	if (manager->Keyboard()->IsKeyUp(VK_NEXT))
	{
		auto orig_nav = static_cast<SceneEditor*>(gApp)->NavSpeedFactor();
		static_cast<SceneEditor*>(gApp)->NavSpeedFactor(orig_nav/2.f);
	}

	// Selection
	if (manager->Keyboard()->IsKeyUp('C'))
	{			
		if (manager->Keyboard()->IsKeyPressed(VK_SHIFT))
		{
			for (auto id : mSoMngr->SceneObjectsIds())
			{
				if (mSoMngr->SceneObjectRef(id)->IsVisible())
					mSoMngr->SelectSceneObject(id);
			}
		}
		else
			mSoMngr->UnselectSceneObjects(mSoMngr->CurrentlySelectedSceneObjects());

		static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
	}

	// Toggle in/out of relative cursor mode
	if (manager->Keyboard()->IsKeyUp('E')) 
	{
		static bool relative = false;
		relative = !relative;

		manager->Mouse()->ToggleRelativeMode(relative);		
	}

	DoEntityCreationActions(manager);

	// Depending on the current states... use specific keyboard actions	
	if (mStateMngr->IsStateOn(CAMERA_CONTROL))
		DoCameraControlActions(manager);		
}

void ApplicationKeyboardActions::DoCameraControlActions(std::shared_ptr<InputDeviceManager>& inputDeviceMngr)
{
	static double last_time = 0.0;
	double new_time = gApp->Timer().ElapsedTimeSecs();
	float delta = (float)new_time - (float)last_time;
	last_time = new_time;

	if (            
		inputDeviceMngr->Keyboard()->IsKeyDown('W') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('S') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('A') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('D') ||              
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_RIGHT) ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_UP)    ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_DOWN)  ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_LEFT) )  	     
	{
		last_time = gApp->Timer().ElapsedTimeSecs();
	}
	else
	{       
		float nav_factor = static_cast<SceneEditor*>(gApp)->NavSpeedFactor();
		float incr = 0.f;
		float orig_y_pos = mCamera->Position().y;
		if (inputDeviceMngr->Keyboard()->IsKeyPressed(VK_SHIFT))
			incr = 16.f + nav_factor * 4.f;
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('W') )
		{        
			static_cast<FpCamera*>(mCamera.get())->Move((8.0f*nav_factor)+incr, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('S') )
		{				
			static_cast<FpCamera*>(mCamera.get())->Move((-8.0f*nav_factor)-incr, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('A') )
		{        
			static_cast<FpCamera*>(mCamera.get())->Strafe((-8.0f*nav_factor)-incr, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('D') )
		{
			static_cast<FpCamera*>(mCamera.get())->Strafe((8.0f*nav_factor)+incr, delta);  
		}

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_RIGHT) )
		{
			static_cast<FpCamera*>(mCamera.get())->Yaw(8.0f/DirectX::XM_PI, delta);      
		}    

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_LEFT) )
		{
			static_cast<FpCamera*>(mCamera.get())->Yaw(-8.0f/DirectX::XM_PI, delta);      
		}   

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_UP) )
		{
			static_cast<FpCamera*>(mCamera.get())->Pitch(8.0f/DirectX::XM_PI, delta);      
		}    

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_DOWN) )
		{
			static_cast<FpCamera*>(mCamera.get())->Pitch(-8.0f/DirectX::XM_PI, delta);      
		}    

		if (inputDeviceMngr->Keyboard()->IsKeyPressed(VK_CONTROL))
		{
			auto pos = mCamera->Position();
			pos.y = orig_y_pos;
			static_cast<FpCamera*>(mCamera.get())->Position(pos);
		}
	}
}  

void 
ApplicationKeyboardActions::DoEntityCreationActions( std::shared_ptr<InputDeviceManager>& inputDeviceMngr )
{	
	if (inputDeviceMngr->Keyboard()->IsKeyDown(VK_ADD))
	{
		mSoMngr->AddSceneObject(std::shared_ptr<SceneObject>(new SceneObject));
		static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
	}
	else if (inputDeviceMngr->Keyboard()->IsKeyDown(VK_SUBTRACT))
	{
		std::set<unsigned int> to_delete = mSoMngr->CurrentlySelectedSceneObjects();
		mSoMngr->RemoveSceneObjects(to_delete);
		static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
	}

}
