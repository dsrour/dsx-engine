#include <sstream>
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "BaseApp.h"
#include "EventManager/EventManager.h"
#include "EventNames.h"
#include "Scene/SceneObjectManager.h"
#include "Components/LightEmitting.h"
#include "Components/ShadowEmitting.h"
#include "LightingMenu.h"

extern BaseApp* gApp;

TwBar* LightingMenu::mLightingMenuTwBar;
std::shared_ptr<EventManager> LightingMenu::mEventManager;
std::shared_ptr<SceneObjectManager> LightingMenu::mSoMngr;
std::shared_ptr<ForwardRenderer> LightingMenu::mFrwrdRenderer;
LightingMenu::EditLightMenuVars LightingMenu::mEditLightSubMenu;
bool LightingMenu::mLive = false;
int LightingMenu::mShadowMapSize = 2048;
int LightingMenu::mNumCascades = 4;
float LightingMenu::mStaticOffsetBias = 0.f;
float LightingMenu::mNormalOffsetScaleBias = 0.0005f;
int LightingMenu::mUsePlaneDepthBias = 0;
float LightingMenu::mShadowIntensity = 1.f; 


LightingMenu::LightingMenu(
	std::shared_ptr<ForwardRenderer> forwardRenderer, 
	std::shared_ptr<SceneObjectManager> soMngr, 
	std::shared_ptr<EventManager> eventManager)
{    
	mSoMngr = soMngr;
	mEventManager = eventManager;
	
	mFrwrdRenderer = forwardRenderer;

	
	mLightingMenuTwBar = TwNewBar("Lighting");
	
	TwAddButton(mLightingMenuTwBar, "AddLight", AddLights, NULL, " label='Add Lights' ");
	TwAddButton(mLightingMenuTwBar, "RemoveLight", RemoveLights, NULL, " label='Remove Lights' ");
	TwAddButton(mLightingMenuTwBar, "space1", NULL, NULL, " label=' ' ");
	TwAddVarRW(mLightingMenuTwBar, "live", TW_TYPE_BOOLCPP, &mLive, " label='Live' ");

	LightingMenu::Tw_Str_Defs defs = CreateTwStringDefs();
	TwEnumVal tw_ligh_type_enum[] = { 
		{ DIRECTIONAL_LIGHT, "Directional" },
		{ POINT_LIGHT, "Point" },
		{ SPOT_LIGHT, "Spot" } };
	TwType light_type_enum = TwDefineEnum("Light Type", tw_ligh_type_enum, 3);

	TwAddVarCB(mLightingMenuTwBar, defs.light_type.c_str(), light_type_enum, SetLightTypeCallback, GetLightTypeCallback, &mEditLightSubMenu.lightType, " label='Light Type' ");
	
	TwAddVarRW(mLightingMenuTwBar, defs.spot_amb.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.spotLight.ambR, " label=Ambient visible=false ");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_dif.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.spotLight.difR, " label=Diffuse visible=false ");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_spec.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.spotLight.specR, " label=Specular visible=false ");
	//TwAddVarRW(mLightingMenuTwBar, defs.spot_pos_x.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.posX, " label=PosX visible=false");
	//TwAddVarRW(mLightingMenuTwBar, defs.spot_pos_y.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.posY, " label=PosY visible=false");
	//TwAddVarRW(mLightingMenuTwBar, defs.spot_pos_z.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.posZ, " label=PosZ visible=false");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_range.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.range, " label=Range visible=false min=0");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_dir_x.c_str(), TW_TYPE_DIR3F, &mEditLightSubMenu.spotLight.dirX, " label=Dir visible=false");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_spot.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.spot, " label=Spot visible=false min=0");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_att_x.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.attX, " label='Constant Attenuation' visible=false min=0.0 step=0.025 precision=6");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_att_y.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.attY, " label='Linear Attenuation' visible=false min=0.0 step=0.025 precision=6");
	TwAddVarRW(mLightingMenuTwBar, defs.spot_att_z.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.spotLight.attZ, " label='Exp Attenuation' visible=false min=0.0 step=0.025 precision=6");

	TwAddVarRW(mLightingMenuTwBar, defs.pt_amb.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.pointLight.ambR, " label=Ambient");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_dif.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.pointLight.difR, " label=Diffuse");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_spec.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.pointLight.specR, " label=Specular");
	//TwAddVarRW(mLightingMenuTwBar, defs.pt_pos_x.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.posX, " label=PosX");
	//TwAddVarRW(mLightingMenuTwBar, defs.pt_pos_y.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.posY, " label=PosY");
	//TwAddVarRW(mLightingMenuTwBar, defs.pt_pos_z.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.posZ, " label=PosZ");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_range.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.range, " label=Range min=0");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_att_x.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.attX, " label='Constant Attenuation' min=0.0 step=0.025 precision=6");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_att_y.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.attY, " label='Linear Attenuation' min=0.0 step=0.025 precision=6");
	TwAddVarRW(mLightingMenuTwBar, defs.pt_att_z.c_str(), TW_TYPE_FLOAT, &mEditLightSubMenu.pointLight.attZ, " label='Exp Attenuation' min=0.0 step=0.025 precision=6");

	TwAddVarRW(mLightingMenuTwBar, defs.dir_amb.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.directionalLight.ambR, " label=Ambient visible=false");
	TwAddVarRW(mLightingMenuTwBar, defs.dir_dif.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.directionalLight.difR, " label=Diffuse visible=false");
	TwAddVarRW(mLightingMenuTwBar, defs.dir_spec.c_str(), TW_TYPE_COLOR4F, &mEditLightSubMenu.directionalLight.specR, " label=Specular visible=false");
	TwAddVarRW(mLightingMenuTwBar, defs.dir_dir_x.c_str(), TW_TYPE_DIR3F, &mEditLightSubMenu.directionalLight.dirX, " label=Dir visible=false");

	TwEnumVal tw_sm_size_enum[] = {
		{ 512, "512" },
		{ 1024, "1024" },
		{ 2048, "2048" } };
	TwType sm_size_enum = TwDefineEnum("ShadowMap Size", tw_sm_size_enum, 3);
	TwEnumVal tw_num_cascades_enum[] = {
		{ 2, "2" },
		{ 3, "3" },
		{ 4, "4" },
		{ 5, "5" }, 
		{ 6, "6" }, };
	TwType num_cascades_enum = TwDefineEnum("Num. Cascades", tw_num_cascades_enum, 5);
	TwAddButton(mLightingMenuTwBar, "space2", NULL, NULL, " label=' ' ");
	TwAddVarRW(mLightingMenuTwBar, "sm_size", sm_size_enum, &mShadowMapSize, " label='Shadow Map Size'");
	TwAddVarRO(mLightingMenuTwBar, "num_cascades", num_cascades_enum, &mNumCascades, " label='Num. Cascades'");
	TwAddButton(mLightingMenuTwBar, "AddSm", AddShadowCascades, NULL, " label='Add Shadow Cascades' ");
	TwAddButton(mLightingMenuTwBar, "RemoveSm", RemoveShadowCascades, NULL, " label='Remove Shadow Cascades' ");

	TwAddButton(mLightingMenuTwBar, "space3", NULL, NULL, " label=' ' ");
	TwAddButton(mLightingMenuTwBar, "biases", NULL, NULL, " label='SHADOW BIAS OFFSETS' ");
	TwAddVarRW(mLightingMenuTwBar, "staticBias", TW_TYPE_FLOAT, &mStaticOffsetBias, "label='Static' min=0.0 step=0.0005 precision=4");
	TwAddVarRW(mLightingMenuTwBar, "normalBias", TW_TYPE_FLOAT, &mNormalOffsetScaleBias, "label='Normal Scale' min=0.0 step=0.0005 precision=4");
	TwAddVarRW(mLightingMenuTwBar, "planeDepthBias", TW_TYPE_BOOLCPP, &mUsePlaneDepthBias, "label='Use Plane Depth'");
	TwAddButton(mLightingMenuTwBar, "space4", NULL, NULL, " label=' ' ");
	TwAddVarRW(mLightingMenuTwBar, "shadowIntensity", TW_TYPE_FLOAT, &mShadowIntensity, "label='Shadow Intensity' min=0.0 max=1.0 step=0.05 precision=2");




	//TwDefine(" Renderer iconified=true ");
	TwDefine(" 'Lighting' size='250 250' position='25 720' valueswidth=100 alpha=50 ");
	Reset();
}

LightingMenu::~LightingMenu(void)
{		
}

void 
LightingMenu::Advance()
{ 
	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	if (ids.empty())
		mLive = false;

	for (auto id : ids)
	{
		auto so = mSoMngr->SceneObjectRef(id);
		if (so->EmitsLight() && mLive)
		{
			auto type = mEditLightSubMenu.lightType;

			switch (type)
			{
			case DIRECTIONAL_LIGHT:
				so->SetLightEmitting(CreateDirectionalLightFromMenuVars(), DIRECTIONAL_LIGHT);
				break;
			case POINT_LIGHT:
				so->SetLightEmitting(CreatePointLightFromMenuVars(), POINT_LIGHT);
				break;
			case SPOT_LIGHT:
				so->SetLightEmitting(CreateSpotLightFromMenuVars(), SPOT_LIGHT);
				break;
			default:
				break;
			}
		}

		if (so->EmitsShadows())
		{
			if (so->EmitsShadows())
			{
				so->ShadowEmittingComponent()->StaticOffsetBias(mStaticOffsetBias);
				so->ShadowEmittingComponent()->NormalOffsetScaleBias(mNormalOffsetScaleBias);
				so->ShadowEmittingComponent()->UsePlaneDepthBias(mUsePlaneDepthBias);
				so->ShadowEmittingComponent()->ShadowIntensity(mShadowIntensity);
			}
		}
	}
}

void 
LightingMenu::Reset()
{
	Reset_();
}

void TW_CALL 
LightingMenu::AddLights(void* /*clientData*/)
{
	PointLight pl;
	pl.ambient = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	pl.diffuse = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	pl.specular = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	pl.range = 500.f;	

	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{
		auto so = mSoMngr->SceneObjectRef(id);
		so->SetLightEmitting(pl, POINT_LIGHT);
	}

	mLive = false;
	Reset_();
}

void TW_CALL 
LightingMenu::RemoveLights(void* /*clientData*/)
{
	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{
		auto so = mSoMngr->SceneObjectRef(id);
		so->RemoveLightEmitting();
	}

	mLive = false;
	Reset_();
}

void 
LightingMenu::OnEvent(std::string const& eventName)
{
	if (eventName == std::string(SO_SELECTION_CHANGE) ||
		eventName == std::string(SO_GEOMETRY_CHANGE))
		Reset();		
}

LightingMenu::Tw_Str_Defs
LightingMenu::CreateTwStringDefs()
{	
	LightingMenu::Tw_Str_Defs ret;
	
	ret.light_type = "LightType";

	ret.pt_amb = "PtAmb";
	ret.pt_dif = "PtDif";
	ret.pt_spec = "PtSpec";
	//ret.pt_pos_x = "PtPosX";
	//ret.pt_pos_y = "PtPosY";
	//ret.pt_pos_z = "PtPosZ";
	ret.pt_range = "PtRange";
	ret.pt_att_x = "PtAttX";
	ret.pt_att_y = "PtAttY";
	ret.pt_att_z = "PtAttZ";

	ret.spot_amb = "SpotAmb";
	ret.spot_dif = "SpotDif";
	ret.spot_spec = "SpotSpec";
	//ret.spot_pos_x = "SpotPosX";
	//ret.spot_pos_y = "SpotPosY";
	//ret.spot_pos_z = "SpotPosZ";
	ret.spot_range = "SpotRange";
	ret.spot_dir_x = "SpotDirX";
	ret.spot_dir_y = "SpotDirY";
	ret.spot_dir_z = "SpotDirZ";
	ret.spot_spot = "SpotSpot";
	ret.spot_att_x = "SpotAttX";
	ret.spot_att_y = "SpotAttY";
	ret.spot_att_z = "SpotAttZ";

	ret.dir_amb = "DirAmb";
	ret.dir_dif = "DirDif";
	ret.dir_spec = "DirSpec";
	ret.dir_dir_x = "DirDirX";
	ret.dir_dir_y = "DirDirY";
	ret.dir_dir_z = "DirDirZ";

	return ret;
}

void TW_CALL 
LightingMenu::SetLightTypeCallback(const void* value, void* /*clientData*/)
{
	// Update and show the appropriate variables
	LightType light_type = (LightType)(*(const unsigned int*)value);
	mEditLightSubMenu.lightType = light_type;

	ShowLightTypeVars(light_type);
}

void TW_CALL 
LightingMenu::GetLightTypeCallback(void* value, void* /*clientData*/)
{	
	*(unsigned int*)value = mEditLightSubMenu.lightType;
}

void 
LightingMenu::ShowLightTypeVars(LightType const& lightType)
{
	Tw_Str_Defs defs = CreateTwStringDefs();

	bool show_point = false;
	bool show_spot = false;
	bool show_dir = false;

	if (lightType == POINT_LIGHT)
		show_point = true;
	else if (lightType == SPOT_LIGHT)
		show_spot = true;
	else if (lightType == DIRECTIONAL_LIGHT)
		show_dir = true;

	if (show_point)
	{
		TwDefine((" Lighting/" + defs.pt_amb + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.pt_dif + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.pt_spec + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_x + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_y + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_z + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.pt_range + " visible=true min=0 ").c_str());
		TwDefine((" Lighting/" + defs.pt_att_x + " visible=true min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.pt_att_y + " visible=true min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.pt_att_z + " visible=true min=0.0 step=0.025 precision=6").c_str());
	}
	else
	{
		TwDefine((" Lighting/" + defs.pt_amb + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.pt_dif + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.pt_spec + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_x + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_y + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.pt_pos_z + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.pt_range + " visible=false min=0 ").c_str());
		TwDefine((" Lighting/" + defs.pt_att_x + " visible=false min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.pt_att_y + " visible=false min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.pt_att_z + " visible=false min=0.0 step=0.025 precision=6").c_str());
	}

	if (show_spot)
	{
		TwDefine((" Lighting/" + defs.spot_amb + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.spot_dif + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.spot_spec + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_x + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_y + " visible=true ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_z + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.spot_range + " visible=true min=0 ").c_str());
		TwDefine((" Lighting/" + defs.spot_dir_x + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.spot_spot + " visible=true  min=0").c_str());
		TwDefine((" Lighting/" + defs.spot_att_x + " visible=true min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.spot_att_y + " visible=true min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.spot_att_z + " visible=true min=0.0 step=0.025 precision=6").c_str());
	}
	else
	{
		TwDefine((" Lighting/" + defs.spot_amb + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.spot_dif + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.spot_spec + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_x + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_y + " visible=false ").c_str());
		//TwDefine((" Lighting/" + defs.spot_pos_z + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.spot_range + " visible=false min=0 ").c_str());
		TwDefine((" Lighting/" + defs.spot_dir_x + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.spot_spot + " visible=false  min=0").c_str());
		TwDefine((" Lighting/" + defs.spot_att_x + " visible=false min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.spot_att_y + " visible=false min=0.0 step=0.025 precision=6").c_str());
		TwDefine((" Lighting/" + defs.spot_att_z + " visible=false min=0.0 step=0.025 precision=6").c_str());
	}

	if (show_dir)
	{
		TwDefine((" Lighting/" + defs.dir_amb + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.dir_dif + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.dir_spec + " visible=true ").c_str());
		TwDefine((" Lighting/" + defs.dir_dir_x + " visible=true ").c_str());
	}
	else
	{
		TwDefine((" Lighting/" + defs.dir_amb + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.dir_dif + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.dir_spec + " visible=false ").c_str());
		TwDefine((" Lighting/" + defs.dir_dir_x + " visible=false ").c_str());
	}
}

void 
LightingMenu::Reset_()
{
	// Go through selected SO's, find the first that emits light and set variables

	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{
		auto so = mSoMngr->SceneObjectRef(id);
		if (so->EmitsLight())
		{
			auto type = so->LightEmittingComponent()->Type();
			auto p_light = so->LightEmittingComponent()->GetLight();

			switch (type)
			{
			case DIRECTIONAL_LIGHT:
			{
				DirectionalLight* dl = static_cast<DirectionalLight*>(p_light);
				mEditLightSubMenu.directionalLight.ambR = dl->ambient.x;
				mEditLightSubMenu.directionalLight.ambG = dl->ambient.y;
				mEditLightSubMenu.directionalLight.ambB = dl->ambient.z;
				mEditLightSubMenu.directionalLight.ambA = dl->ambient.w;
				mEditLightSubMenu.directionalLight.difR = dl->diffuse.x;
				mEditLightSubMenu.directionalLight.difG = dl->diffuse.y;
				mEditLightSubMenu.directionalLight.difB = dl->diffuse.z;
				mEditLightSubMenu.directionalLight.difA = dl->diffuse.w;
				mEditLightSubMenu.directionalLight.specR = dl->specular.x;
				mEditLightSubMenu.directionalLight.specG = dl->specular.y;
				mEditLightSubMenu.directionalLight.specB = dl->specular.z;
				mEditLightSubMenu.directionalLight.specA = dl->specular.w;
				mEditLightSubMenu.directionalLight.dirX = dl->direction.x;
				mEditLightSubMenu.directionalLight.dirY = dl->direction.y;
				mEditLightSubMenu.directionalLight.dirZ = -dl->direction.z;
				mEditLightSubMenu.lightType = DIRECTIONAL_LIGHT;
				break;
			}
			case POINT_LIGHT:
			{
				PointLight* pl = static_cast<PointLight*>(p_light);
				mEditLightSubMenu.pointLight.ambR = pl->ambient.x;
				mEditLightSubMenu.pointLight.ambG = pl->ambient.y;
				mEditLightSubMenu.pointLight.ambB = pl->ambient.z;
				mEditLightSubMenu.pointLight.ambA = pl->ambient.w;
				mEditLightSubMenu.pointLight.difR = pl->diffuse.x;
				mEditLightSubMenu.pointLight.difG = pl->diffuse.y;
				mEditLightSubMenu.pointLight.difB = pl->diffuse.z;
				mEditLightSubMenu.pointLight.difA = pl->diffuse.w;
				mEditLightSubMenu.pointLight.specR = pl->specular.x;
				mEditLightSubMenu.pointLight.specG = pl->specular.y;
				mEditLightSubMenu.pointLight.specB = pl->specular.z;
				mEditLightSubMenu.pointLight.specA = pl->specular.w;
				mEditLightSubMenu.pointLight.range = pl->range;
				mEditLightSubMenu.pointLight.attX = pl->attenuation.x;
				mEditLightSubMenu.pointLight.attY = pl->attenuation.y;
				mEditLightSubMenu.pointLight.attZ = pl->attenuation.z;
				//mEditLightSubMenu.pointLight.posX = pl->position.x;
				//mEditLightSubMenu.pointLight.posY = pl->position.y;
				//mEditLightSubMenu.pointLight.posZ = pl->position.z;
				mEditLightSubMenu.lightType = POINT_LIGHT;
				break;
			}
			case SPOT_LIGHT:
			{
				SpotLight* sl = static_cast<SpotLight*>(p_light);
				mEditLightSubMenu.spotLight.ambR = sl->ambient.x;
				mEditLightSubMenu.spotLight.ambG = sl->ambient.y;
				mEditLightSubMenu.spotLight.ambB = sl->ambient.z;
				mEditLightSubMenu.spotLight.ambA = sl->ambient.w;
				mEditLightSubMenu.spotLight.difR = sl->diffuse.x;
				mEditLightSubMenu.spotLight.difG = sl->diffuse.y;
				mEditLightSubMenu.spotLight.difB = sl->diffuse.z;
				mEditLightSubMenu.spotLight.difA = sl->diffuse.w;
				mEditLightSubMenu.spotLight.specR = sl->specular.x;
				mEditLightSubMenu.spotLight.specG = sl->specular.y;
				mEditLightSubMenu.spotLight.specB = sl->specular.z;
				mEditLightSubMenu.spotLight.specA = sl->specular.w;
				mEditLightSubMenu.spotLight.range = sl->range;
				mEditLightSubMenu.spotLight.attX = sl->attenuation.x;
				mEditLightSubMenu.spotLight.attY = sl->attenuation.y;
				mEditLightSubMenu.spotLight.attZ = sl->attenuation.z;
				mEditLightSubMenu.spotLight.dirX = sl->direction.x;
				mEditLightSubMenu.spotLight.dirY = sl->direction.y;
				mEditLightSubMenu.spotLight.dirZ = -sl->direction.z;
				mEditLightSubMenu.spotLight.spot = sl->spot;
				//mEditLightSubMenu.spotLight.posX = sl->position.x;
				//mEditLightSubMenu.spotLight.posY = sl->position.y;
				//mEditLightSubMenu.spotLight.posZ = sl->position.z;
				mEditLightSubMenu.lightType = SPOT_LIGHT;
				break;
			}
			default:
				break;
			}		
		}

		if (so->EmitsShadows())
		{
			mStaticOffsetBias = so->ShadowEmittingComponent()->StaticOffsetBias();
			mNormalOffsetScaleBias = so->ShadowEmittingComponent()->NormalOffsetScaleBias();
			mUsePlaneDepthBias = so->ShadowEmittingComponent()->UsePlaneDepthBias();
			mShadowIntensity = so->ShadowEmittingComponent()->ShadowIntensity();
		}

		break;
	}

	ShowLightTypeVars(mEditLightSubMenu.lightType);
}

PointLight 
LightingMenu::CreatePointLightFromMenuVars()
{
	PointLight pl;
	pl.ambient.x = mEditLightSubMenu.pointLight.ambR;
	pl.ambient.y = mEditLightSubMenu.pointLight.ambG;
	pl.ambient.z = mEditLightSubMenu.pointLight.ambB;
	pl.ambient.w = mEditLightSubMenu.pointLight.ambA;
	pl.diffuse.x = mEditLightSubMenu.pointLight.difR;
	pl.diffuse.y = mEditLightSubMenu.pointLight.difG;
	pl.diffuse.z = mEditLightSubMenu.pointLight.difB;
	pl.diffuse.w = mEditLightSubMenu.pointLight.difA;
	pl.specular.x = mEditLightSubMenu.pointLight.specR;
	pl.specular.y = mEditLightSubMenu.pointLight.specG;
	pl.specular.z = mEditLightSubMenu.pointLight.specB;
	pl.specular.w = mEditLightSubMenu.pointLight.specA;
	pl.range = mEditLightSubMenu.pointLight.range;
	pl.attenuation.x = mEditLightSubMenu.pointLight.attX;
	pl.attenuation.y = mEditLightSubMenu.pointLight.attY;
	pl.attenuation.z = mEditLightSubMenu.pointLight.attZ;
	//pl.position.x = mEditLightSubMenu.pointLight.posX;
	//pl.position.y = mEditLightSubMenu.pointLight.posY;
	//pl.position.z = mEditLightSubMenu.pointLight.posZ;
	return pl;
}

SpotLight 
LightingMenu::CreateSpotLightFromMenuVars()
{
	SpotLight sl;
	sl.ambient.x = mEditLightSubMenu.spotLight.ambR;
	sl.ambient.y = mEditLightSubMenu.spotLight.ambG;
	sl.ambient.z = mEditLightSubMenu.spotLight.ambB;
	sl.ambient.w = mEditLightSubMenu.spotLight.ambA;
	sl.diffuse.x = mEditLightSubMenu.spotLight.difR;
	sl.diffuse.y = mEditLightSubMenu.spotLight.difG;
	sl.diffuse.z = mEditLightSubMenu.spotLight.difB;
	sl.diffuse.w = mEditLightSubMenu.spotLight.difA;
	sl.specular.x = mEditLightSubMenu.spotLight.specR;
	sl.specular.y = mEditLightSubMenu.spotLight.specG;
	sl.specular.z = mEditLightSubMenu.spotLight.specB;
	sl.specular.w = mEditLightSubMenu.spotLight.specA;
	sl.range = mEditLightSubMenu.spotLight.range;
	sl.attenuation.x = mEditLightSubMenu.spotLight.attX;
	sl.attenuation.y = mEditLightSubMenu.spotLight.attY;
	sl.attenuation.z = mEditLightSubMenu.spotLight.attZ;
	sl.direction.x = mEditLightSubMenu.spotLight.dirX;
	sl.direction.y = mEditLightSubMenu.spotLight.dirY;
	sl.direction.z = -mEditLightSubMenu.spotLight.dirZ;
	sl.spot = mEditLightSubMenu.spotLight.spot;
	//sl.position.x = mEditLightSubMenu.spotLight.posX;
	//sl.position.y = mEditLightSubMenu.spotLight.posY;
	//sl.position.z = mEditLightSubMenu.spotLight.posZ;
	return sl;
}

DirectionalLight 
LightingMenu::CreateDirectionalLightFromMenuVars()
{
	DirectionalLight dl;
	dl.ambient.x = mEditLightSubMenu.directionalLight.ambR;
	dl.ambient.y = mEditLightSubMenu.directionalLight.ambG;
	dl.ambient.z = mEditLightSubMenu.directionalLight.ambB;
	dl.ambient.w = mEditLightSubMenu.directionalLight.ambA;
	dl.diffuse.x = mEditLightSubMenu.directionalLight.difR;
	dl.diffuse.y = mEditLightSubMenu.directionalLight.difG;
	dl.diffuse.z = mEditLightSubMenu.directionalLight.difB;
	dl.diffuse.w = mEditLightSubMenu.directionalLight.difA;
	dl.specular.x = mEditLightSubMenu.directionalLight.specR;
	dl.specular.y = mEditLightSubMenu.directionalLight.specG;
	dl.specular.z = mEditLightSubMenu.directionalLight.specB;
	dl.specular.w = mEditLightSubMenu.directionalLight.specA;
	dl.direction.x = mEditLightSubMenu.directionalLight.dirX;
	dl.direction.y = mEditLightSubMenu.directionalLight.dirY;
	dl.direction.z = -mEditLightSubMenu.directionalLight.dirZ;
	return dl;
}

void TW_CALL 
LightingMenu::AddShadowCascades(void* /*clientData*/)
{
	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{		
		auto so = mSoMngr->SceneObjectRef(id);
		if (so->EmitsLight())
			so->SetShadowEmitting(mShadowMapSize);
	}
}

void TW_CALL 
LightingMenu::RemoveShadowCascades(void* /*clientData*/)
{
	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{
		auto so = mSoMngr->SceneObjectRef(id);
		so->RemoveShadowEmitting();
	}
}






