#pragma once

#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "MenuManager/Menu.h"

class SceneObjectManager;

class SceneEditorMenu : public Menu
{
public:
	SceneEditorMenu(std::shared_ptr<SceneObjectManager> soMngr);
	~SceneEditorMenu(void);

	
	virtual void
	Reset();
		
private:
	static std::shared_ptr<SceneObjectManager> mSoMngr;

	static float	mNavSpeedFactor;	
	static bool		mShowBounds;
	static bool		mShowFloorGrid;
	static TwBar*   mSceneEditorMenuTwBar;

	static void TW_CALL 
	SetNavFactorCallback(const void* value, void* /*clientData*/);

	static void TW_CALL 
	GetNavFactorCallback(void* value, void* /*clientData*/); 
	
	static void TW_CALL 
	SetShowBoundsCallback(const void* value, void* /*clientData*/);

	static void TW_CALL 
	GetShowBoundsCallback(void* value, void* /*clientData*/); 

	static void TW_CALL 
	SetShowFloorCallback(const void* value, void* /*clientData*/);

	static void TW_CALL 
	GetShowFloorCallback(void* value, void* /*clientData*/);
};

