#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Scene/SceneObjectManager.h"
#include "MenuManager/Menu.h"

struct Material;
class Renderable;

struct MaterialMenuVars
{
	MaterialMenuVars()
	{
		ambR = ambG = ambB = 
		difR = difG = difB = 
		specR = specG = specB = 
		reflR = reflG = reflB = 
		instancedR = instancedG = instancedB = 0.5f;
		ambA = difA = reflA = instancedA = 1.f;
		specA = 0.5f;
		reflectionCoeff = 0.f;
		lightingModel = 0; // PHONG
	}

	float ambR, ambG, ambB, ambA;
	float difR, difG, difB, difA;
	float specR, specG, specB, specA;
	float reflR, reflG, reflB, reflA;
	float reflectionCoeff;
	unsigned int lightingModel;
	float instancedR, instancedG, instancedB, instancedA;
};

class RenderableComponentMenu : public Menu
{
public:
	RenderableComponentMenu(std::shared_ptr<SceneObjectManager> soManager);
	~RenderableComponentMenu();

	virtual void
	Reset();

	virtual void 
	Advance();

	virtual void
	OnEvent(std::string const& eventName);

private:
	Material
	CreateMaterialFromRenderable(std::shared_ptr<Renderable> const& renderable);

	static Material
	CreateMaterialFromMenuVars();
	
	static std::shared_ptr<SceneObjectManager> mSoManager;
	static TwBar*            mRenderableTwBar;
		
	// Primitive topology
	static void TW_CALL
	SetPrimTopologyToTris(void* /*clientData*/);
	static void TW_CALL
	SetPrimTopologyToPnts(void* /*clientData*/);

	// Toggle back culling
	static bool mBackCullToggle;
	static void TW_CALL
	ToggleBackCull(void* /*clientData*/);

	// Material
	static bool				 mLiveMaterial;
	static MaterialMenuVars  mMaterialMenuVars;
	static void TW_CALL
	ApplyMaterial(void* /*clientData*/);

	// Shadows
	static void TW_CALL
	EnableShadowCasting(void* /*clientData*/);

	static void TW_CALL
	DisableShadowCasting(void* /*clientData*/);

	static unsigned int mType;
};

