#include <sstream>
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "BaseApp.h"
#include "EventNames.h"
#include "RendererMenu.h"

extern BaseApp* gApp;

TwBar* RendererMenu::mRendererMenuTwBar;
Fog RendererMenu::mFog;

RendererMenu::RendererMenu(std::shared_ptr<ForwardRenderer> forwardRenderer)
{    
	mFrwrdRenderer = forwardRenderer;

		mRendererMenuTwBar = TwNewBar("Renderer");
	
	TwAddButton(mRendererMenuTwBar, "FOG", NULL, NULL, " label='FOG' ");
	TwAddVarRW(mRendererMenuTwBar, "Density", TW_TYPE_FLOAT, &(mFog.fogDensity), " label='Density' min=0.0 max = 1.0 step=0.025 precision=6");
	TwAddVarRW(mRendererMenuTwBar, "Start", TW_TYPE_FLOAT, &(mFog.fogLinStart), " min=0 label='Start' ");
	TwAddVarRW(mRendererMenuTwBar, "End", TW_TYPE_FLOAT, &(mFog.fogLinEnd), " min=0 label='End' ");
	TwAddVarRW(mRendererMenuTwBar, "LinearExponentType", TW_TYPE_INT32, &(mFog.fogType), " min=-1 label='Linear Exp. Type' ");
	TwAddVarRW(mRendererMenuTwBar, "Color", TW_TYPE_COLOR4F, &(mFog.fogColor), " label='Color' ");
			
	//TwDefine(" Renderer iconified=true ");
	TwDefine(" 'Renderer' size='250 135' position='25 575' valueswidth=100 alpha=50 ");

	Reset();
}

RendererMenu::~RendererMenu(void)
{		
}

void 
RendererMenu::Advance()
{ 
	// Update fog settings
	mFrwrdRenderer->FogSettings() = mFog;	
}

void 
RendererMenu::Reset()
{
	mFog = mFrwrdRenderer->FogSettings();
}

void 
RendererMenu::OnEvent(std::string const& eventName)
{
	if (eventName == std::string(SO_SELECTION_CHANGE) ||
		eventName == std::string(SO_GEOMETRY_CHANGE))
		Reset();
}
