#pragma once

#include <Windows.h>
#include <string>
#include <map>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "MenuManager/Menu.h"

struct Fog;
class ForwardRenderer;

class RendererMenu :
	public Menu
{
public:
	RendererMenu(std::shared_ptr<ForwardRenderer> forwardRenderer);

    virtual
    ~RendererMenu(void);
		            
    virtual void 
    Advance();

	void
	Reset(); 

	virtual void
	OnEvent(std::string const& eventName);
               
protected:   	
	static Fog				mFog;
	static TwBar*           mRendererMenuTwBar;
	
	std::shared_ptr<ForwardRenderer> mFrwrdRenderer;
};