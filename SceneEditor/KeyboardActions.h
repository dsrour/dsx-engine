#pragma once

#include <windows.h>
#include <DirectXMath.h>

#include "InputDeviceManager.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Components/Renderable.h"

class StateManager;
class SceneObjectManager;

class ApplicationKeyboardActions : public InputDeviceActions
{
public:	
	ApplicationKeyboardActions(
		std::shared_ptr<InputDeviceManager> inputDeviceManager, 
		std::shared_ptr<SceneObjectManager> soMngr,
		std::shared_ptr<StateManager> stateMngr);

    virtual void
    OnStateChange(void);
	
private:	
	void
	DoCameraControlActions(std::shared_ptr<InputDeviceManager>& inputDeviceMngr);

	void
	DoEntityCreationActions(std::shared_ptr<InputDeviceManager>& inputDeviceMngr);

    std::shared_ptr<Camera> mCamera;    
	std::shared_ptr<SceneObjectManager> mSoMngr;
	std::shared_ptr<StateManager> mStateMngr;
};


