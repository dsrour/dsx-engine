#include "Utils/CommonWinDialogs.h"
#include "EventNames.h"
#include "EventManager/EventManager.h"
#include "SceneObjectMenu.h"


TwBar* SceneObjectMenu::mSoTwBar;

std::shared_ptr<EventManager> SceneObjectMenu::mEventManager;
std::shared_ptr<SceneObjectManager> SceneObjectMenu::mSoManager;

SceneObjectMenu::SceneObjectMenu(std::shared_ptr<SceneObjectManager> soManager, std::shared_ptr<EventManager> eventManager)
{
	mSoManager = soManager;
	mEventManager = eventManager;

	mSoTwBar = TwNewBar("Scene Object");
	
	TwAddButton(mSoTwBar, "VISIBILITY", NULL, NULL, " label='VISIBILITY' ");
	TwAddButton(mSoTwBar, "Hide Selection", HideSelection, NULL, " label='Hide Selection' ");
	TwAddButton(mSoTwBar, "Show All", UnhideAll, NULL, " label='Show All' ");
	TwAddButton(mSoTwBar, "space1", NULL, NULL, " label=' ' ");
	TwAddButton(mSoTwBar, "TODO://SCRIPTING", NULL, NULL, " label='TODO://SCRIPTING' ");

	//TwDefine(" 'Scene Object' iconified=true ");
	TwDefine(" 'Scene Object' size='250 150' position='25 230' valueswidth=100 alpha=50 ");
}



SceneObjectMenu::~SceneObjectMenu()
{
}

void 
SceneObjectMenu::Reset()
{

}

void 
SceneObjectMenu::Advance()
{

}

void 
SceneObjectMenu::OnEvent(std::string const& eventName)
{

}

void
TW_CALL SceneObjectMenu::HideSelection(void* /*clientData*/)
{	
	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();
	for (std::set<unsigned int>::iterator iter = selected.begin();
		iter != selected.end();
		++iter)
	{
		mSoManager->SceneObjectRef(*iter)->SetVisibility(false);
		mSoManager->UnselectSceneObject(*iter);
	}
}

void
TW_CALL SceneObjectMenu::UnhideAll(void* /*clientData*/)
{	
	std::set<unsigned int> sos = mSoManager->SceneObjectsIds();
	for (std::set<unsigned int>::iterator iter = sos.begin();
		iter != sos.end();
		++iter)
	{
		mSoManager->SceneObjectRef(*iter)->SetVisibility(true);
	}
}


