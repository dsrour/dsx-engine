#pragma once

#include <Windows.h>
#include <string>
#include <map>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "MenuManager/Menu.h"

class ForwardRenderer;
class SceneObjectManager;
class EventManager;



class LightingMenu :
	public Menu
{
public:
	struct Tw_Str_Defs
	{
		std::string light_type;

		// Point
		std::string pt_amb;
		std::string pt_dif;
		std::string pt_spec;
		//std::string pt_pos_x;
		//std::string pt_pos_y;
		//std::string pt_pos_z;
		std::string pt_range;
		std::string pt_att_x;
		std::string pt_att_y;
		std::string pt_att_z;

		// Spot
		std::string spot_amb;
		std::string spot_dif;
		std::string spot_spec;
		//std::string spot_pos_x;
		//std::string spot_pos_y;
		//std::string spot_pos_z;
		std::string spot_range;
		std::string spot_dir_x;
		std::string spot_dir_y;
		std::string spot_dir_z;
		std::string spot_spot;
		std::string spot_att_x;
		std::string spot_att_y;
		std::string spot_att_z;

		// Directional
		std::string dir_amb;
		std::string dir_dif;
		std::string dir_spec;
		std::string dir_dir_x;
		std::string dir_dir_y;
		std::string dir_dir_z;
	};

	struct EditLightMenuVars
	{


		EditLightMenuVars() { ZeroMemory(this, sizeof(this)); }

		LightType lightType;

		struct Point
		{
			Point() 
			{ 
				ambR = ambG = ambB = ambA = 1.f;
				difR = difG = difB = difA = 1.f;
				specR = specG = specB = specA = 1.f;
				//posX = posY = posZ = 0.f;
				range = 500.f;
				attX = 0.f; attY = 1.f; attZ = 0.f;
			}

			float ambR, ambG, ambB, ambA;
			float difR, difG, difB, difA;
			float specR, specG, specB, specA;
			//float posX, posY, posZ;
			float range;
			float attX, attY, attZ;
		} pointLight;

		struct Spot
		{
			Spot() 
			{
				ambR = ambG = ambB = ambA = 1.f;
				difR = difG = difB = difA = 1.f;
				specR = specG = specB = specA = 1.f;
				//posX = posY = posZ = 0.f;
				dirX = 0.f; dirY = 0.f; dirZ = -1.f;
				spot = 180.f;
				range = 500.f;
				attX = 0.f; attY = 1.f; attZ = 0.f;
			}

			float ambR, ambG, ambB, ambA;
			float difR, difG, difB, difA;
			float specR, specG, specB, specA;
			//float posX, posY, posZ;
			float range;
			float dirX, dirY, dirZ;
			float spot;
			float attX, attY, attZ;
		} spotLight;

		struct Directional
		{
			Directional() 
			{
				ambR = ambG = ambB = ambA = 1.f;
				difR = difG = difB = difA = 1.f;
				specR = specG = specB = specA = 1.f;
				dirX = 0.f; dirY = 0.f; dirZ = -1.f;
			}

			float ambR, ambG, ambB, ambA;
			float difR, difG, difB, difA;
			float specR, specG, specB, specA;
			float dirX, dirY, dirZ;
		} directionalLight;
	};

	LightingMenu(
		std::shared_ptr<ForwardRenderer> forwardRenderer,
		std::shared_ptr<SceneObjectManager> soMngr, 
		std::shared_ptr<EventManager> eventManager);

    virtual
    ~LightingMenu(void);
	            
    virtual void 
    Advance();

	void
	Reset(); 	

private:  
	static void
	Reset_();

	static void
	ShowLightTypeVars(LightType const& lightType);

	static LightingMenu::Tw_Str_Defs
	CreateTwStringDefs();

	static void TW_CALL
	SetLightTypeCallback(const void* value, void* /*clientData*/);

	static void TW_CALL
	GetLightTypeCallback(void* value, void* /*clientData*/);

	virtual void
	OnEvent(std::string const& eventName);

	static void TW_CALL
	AddLights(void* /*clientData*/);

	static void TW_CALL
	RemoveLights(void* /*clientData*/);

	static PointLight
	CreatePointLightFromMenuVars();

	static SpotLight
	CreateSpotLightFromMenuVars();

	static DirectionalLight
	CreateDirectionalLightFromMenuVars();

	static void TW_CALL
	AddShadowCascades(void* /*clientData*/);

	static void TW_CALL
	RemoveShadowCascades(void* /*clientData*/);

	static TwBar*            mLightingMenuTwBar;	
	static bool mLive;
	static EditLightMenuVars mEditLightSubMenu;
	static std::shared_ptr<ForwardRenderer> mFrwrdRenderer;
	static std::shared_ptr<SceneObjectManager> mSoMngr;
	static std::shared_ptr<EventManager> mEventManager;

	static int mNumCascades;
	static int mShadowMapSize;

	static int   mUsePlaneDepthBias;
	static float mNormalOffsetScaleBias;
	static float mStaticOffsetBias;
	static float mShadowIntensity;
};