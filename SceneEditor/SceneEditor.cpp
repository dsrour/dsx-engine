#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "CommonRenderables.h"
#include "Settings.h"
#include "Components/Bounded.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"
#include "Systems/Renderer/Pipelines/FurRenderer/FurRenderer.h"
#include "PostProcessPipeline.h"
#include "EventNames.h"
#include "SceneEditor.h"

// Menus
#include "SceneEditorMenu.h"
#include "RenderableComponentMenu.h"
#include "RendererMenu.h"
#include "SceneObjectMenu.h"
#include "SpatialTransformationMenu.h"
#include "FileMenu.h"
#include "TexturingMenu.h"
#include "FurMenu.h"
#include "LightingMenu.h"
#define SCENE_EDITOR_MENU	"scene_editor_menu"
#define RENDERABLE_MENU		"renderable_menu"
#define RENDERER_MENU		"renderer_menu"
#define SCENE_OBJECT_MENU	"scene_object_menu"
#define SPATIAL_XFORM_MENU  "spatial_xform_menu"
#define IMPORT_EXPORT_MENU  "import_export_menu"
#define TEXTURING_MENU		"texturing_menu"
#define FUR_MENU			"fur_menu"
#define LIGHTING_MENU		"lighting_menu"

BaseApp* gApp = new SceneEditor;

SceneEditor::SceneEditor(void)
{    
	WinWidth(1888);
	WinHeight(992);

	// State mngr since some other objects depend on it 	
	mStateManager = std::make_shared<StateManager>();
	mStateManager->ToggleState(CAMERA_CONTROL, true);
	mStateManager->ToggleState(SPATIAL_TRANSFORM, true);
		
	mSceneObjectManager = std::make_shared<SceneObjectManager>();
	mEventManager = std::make_shared<EventManager>();

	mNavSpeedFactor = 10.f;

	mEventManager->SubscribeToEvent(SO_TRANSFORMED, this);
	mEventManager->SubscribeToEvent(SO_SELECTION_CHANGE, this);
}


SceneEditor::~SceneEditor(void)
{
	mEventManager->UnsubscribeToEvent(SO_TRANSFORMED, this);
	mEventManager->UnsubscribeToEvent(SO_SELECTION_CHANGE, this);
}

bool const 
SceneEditor::OnLoadApp( void )
{	
    std::string help_str = std::string("GLOBAL help=' \n") + 
                           "(+) \t New Entity \n" + 
                           "(-) \t Remove Selected Entities \n" +
                           "\n" +
						   "(HOME) \t Toggle FPS \n" +					   					   				  
						   "\n" +
                           "(F9) \t Load Scene \n" + 
                           "(F6) \t Save Scene \n" + 
						   "\n" +
						   "(F11) \t Recompile Shaders \n" + 
						   "\n" +
						   "(E) \t Toggle FPS Camera \n" + 
						   "(SHIFT+C) \t Select All \n" +
						   "(C) \t Clear Selection \n" + 
						   "\n" +
                           + "(SHIFT)+Action \t Speed up Action \n'";	                           
    TwDefine(help_str.c_str());

	ToggleFloorGridRenderableEntity(true); 


	// Set actions for keyboard + mouse
	mKeyboardActions = std::make_shared<ApplicationKeyboardActions>(InputDeviceMngr(), mSceneObjectManager, mStateManager);
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardActions);
	mMouseActions = std::make_shared<ApplicationMouseActions>(InputDeviceMngr(), mSceneObjectManager, mStateManager, mEventManager);
	InputDeviceMngr()->Mouse()->AddMouseActions(mMouseActions);
		
	// Set Render Path with a post-process pipeline
	std::shared_ptr<ForwardRenderer> forward_renderer(new ForwardRenderer(gApp->Renderer()));
	std::shared_ptr<FurRenderer> fur_renderer(new FurRenderer(gApp->Renderer()));
	std::shared_ptr<SkyRenderer> sky(new SkyRenderer(gApp->Renderer()));
	std::shared_ptr<PostProcessPipeline> post_proc_pipeline(new PostProcessPipeline(gApp->Renderer()));
	std::list< std::shared_ptr<Pipeline> > path; 
	path.push_back(sky); 
	path.push_back(forward_renderer); 
	path.push_back(fur_renderer);
	path.push_back(post_proc_pipeline); 
	gApp->Renderer()->RenderPath(path);

	// Move camera backwards off the center of the scene
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Position(DirectX::XMFLOAT4(0.f, 15.f, 0.f, 1.f));
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Move(-30.0f);
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Pitch(0.4f);

	mWidgetManager = std::make_shared<WidgetManager>(mSceneObjectManager, mStateManager, mEventManager);

	// Menu inits
	mMenuManager = std::make_shared<MenuManager>();
	std::shared_ptr<Menu> scene_editor_menu(new SceneEditorMenu(mSceneObjectManager));
	std::shared_ptr<Menu> renderable_menu(new RenderableComponentMenu(mSceneObjectManager));
	std::shared_ptr<Menu> renderer_menu(new RendererMenu(forward_renderer));
	std::shared_ptr<Menu> so_menu(new SceneObjectMenu(mSceneObjectManager, mEventManager));
	std::shared_ptr<Menu> xform_menu(new SpatialTransformationMenu(mSceneObjectManager));
	std::shared_ptr<Menu> imp_exp_menu(new FileMenu(mSceneObjectManager, mEventManager, forward_renderer));
	std::shared_ptr<Menu> texturing_menu(new TexturingMenu(mSceneObjectManager));
	std::shared_ptr<Menu> fur_menu(new FurMenu(mSceneObjectManager));
	std::shared_ptr<Menu> lighting_menu(new LightingMenu(forward_renderer, mSceneObjectManager, mEventManager));
	mMenuManager->AddMenu(SCENE_EDITOR_MENU, scene_editor_menu);
	mMenuManager->AddMenu(RENDERABLE_MENU, renderable_menu);
	mMenuManager->AddMenu(RENDERER_MENU, renderer_menu);
	mMenuManager->AddMenu(SCENE_OBJECT_MENU, so_menu);
	mMenuManager->AddMenu(SPATIAL_XFORM_MENU, xform_menu);
	mMenuManager->AddMenu(IMPORT_EXPORT_MENU, imp_exp_menu);
	mMenuManager->AddMenu(TEXTURING_MENU, texturing_menu);
	mMenuManager->AddMenu(FUR_MENU, fur_menu);
	mMenuManager->AddMenu(LIGHTING_MENU, lighting_menu);
		
    return true;
}

void 
SceneEditor::OnPreUpdate( void )
{
	mMenuManager->AdvanceMenus();
}

void 
SceneEditor::OnPostUpdate( void )
{		
	
}

void 
SceneEditor::OnUnloadApp( void )
{

}

void 
SceneEditor::ToggleFloorGridRenderableEntity( bool const& show )
{
	static int floor_grid_id = -1;

	if (floor_grid_id < 0 && show)
	{		
		int renderable_id = EntityMngr()->CreateEntity();                  
		std::shared_ptr<FloorGridRenderable> renderable_obj( new FloorGridRenderable(Renderer()->Device()) );   
		Material mat;	
		mat.diffuse = FLOOR_GRID_COLOR;
		renderable_obj->MaterialProperties(mat);
		EntityMngr()->AddComponentToEntity(renderable_id, renderable_obj);
		floor_grid_id = renderable_id;
	}
	else if (!show && floor_grid_id >= 0)
	{
		EntityMngr()->DestroyEntity(floor_grid_id);
		floor_grid_id = -1;
	}
}

void 
SceneEditor::OnEvent(std::string const& eventName, Metadata* const metadata /*= NULL*/)
{
	mMenuManager->BroadcastEvent(eventName);
}
