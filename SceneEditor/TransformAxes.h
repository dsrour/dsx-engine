#pragma once

#include "EventManager/EventManager.h"

class SceneObjectManager;
class StateManager;


/* A widget for transforming scene objects.
   Shows the x, y, z axes around the bounding box of an SO.
   User can interact with the axes to transform, rotate, SO. */

class TransformAxes : public EventSubscriber
{
public:
	TransformAxes(
		std::shared_ptr<SceneObjectManager> soMngr, 
		std::shared_ptr<StateManager> stateMngr,
		std::shared_ptr<EventManager> eventMngr);
	~TransformAxes(void);
	
	// Called by mouse input actions to transform so.
	// Note that this function affects all selected so's.
	bool const
	InteractionActions(int const& xIncr, int const& yIncr);

private:
	// Following is private because this widget will be based on selected SO... it doesn't have to be bounded by anyone else
	void
	BindTransformAxesToSO(unsigned int const& soId);
	void
	UnbindTransformAxes(void);


	virtual void
	OnEvent(std::string const& event, Metadata* const metadata = NULL);

	int mAxesIds[3]; // x, y, z		

	int mSoId;  // SO the widget was bounded to

	std::shared_ptr<SceneObjectManager> mSoMngr;
	std::shared_ptr<StateManager> mStateMngr;
	std::shared_ptr<EventManager> mEventMngr;
};

