#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <set>

#include "InputDeviceManager.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Components/Renderable.h"

class SceneObjectManager;
class StateManager;
class EventManager;

class ApplicationMouseActions : public InputDeviceActions
{
public:	
	ApplicationMouseActions(
		std::shared_ptr<InputDeviceManager> inputDeviceManager,
		std::shared_ptr<SceneObjectManager> soMngr,
		std::shared_ptr<StateManager> stateMngr,
		std::shared_ptr<EventManager> eventMngr);

	virtual void
	OnStateChange(void);

	
private:
	/// @return True if actions should be terminated upon return.
	bool const 
	DoSpatialTransformActions(std::shared_ptr<InputDeviceManager>& manager);

	void 
	CameraTransformActions( std::shared_ptr<InputDeviceManager>& manager );

	void 
	DoSelectionActions( std::shared_ptr<InputDeviceManager> manager );

	std::shared_ptr<Camera> mCamera;

	int mLastX, mLastY;

	std::shared_ptr<SceneObjectManager> mSoMngr;
	std::shared_ptr<StateManager> mStateMngr;
	std::shared_ptr<EventManager> mEventMngr;
};




