#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Scene/SceneObjectManager.h"
#include "MenuManager/Menu.h"

class SpatialTransformationMenu :
	public Menu
{
public:
	SpatialTransformationMenu(std::shared_ptr<SceneObjectManager> soManager);
	~SpatialTransformationMenu(void);

	virtual void
	Reset();

	virtual void
	Advance();

	virtual void
	OnEvent(std::string const& eventName);


private:
	static std::shared_ptr<SceneObjectManager> mSoManager;

	static TwBar*   mMenu;

	// Menu Vars
	static float mScale;
	static float mPosX, mPosY, mPosZ;
	static float mRotX, mRotY, mRotZ;

	static void TW_CALL
	ResetSelection(void* /*clientData*/);
};

