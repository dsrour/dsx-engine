#pragma once

#include <sstream> 
#include "Systems/Renderer/DirectXTK/Inc/SpriteFont.h"
#include "SceneEditor.h"
#include "Systems/Renderer/Pipeline.h"

extern std::wstring gResourcesDir;

class PostProcessPipeline : public Pipeline
{
public:
    PostProcessPipeline(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
	{		
		mSpriteBatch = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
		mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (gResourcesDir+L"Fonts\\font.spritefont").c_str());
	}
    
    virtual 
    ~PostProcessPipeline(void)
	{

		if (mSpriteFont)
			delete mSpriteFont;

		if (mSpriteBatch)
			delete mSpriteBatch;
	}
          
    virtual void
    MadeActive(void) {}

    virtual void
    MadeInactive(void) {}

    virtual void
    UpdatePipeline(void) {}

	virtual void
	RecompileShaders() { /*no shaders*/ }

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime)
	{
		if (static_cast<SceneEditor*>(gApp)->StateMngr()->IsStateOn(SHOW_FPS))
		{
			// Since this gets called every time we render... we just see how many frames we get per second
			static double elapsed = gApp->Timer().ElapsedTimeSecs();
			static unsigned int frames = 0;			
			frames++;
						
			static std::wstring fps_str = L"0 FPS";

			if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
			{
				std::wstringstream ss;   
				ss << frames; 
				fps_str = ss.str() + L" FPS";
				frames = 0;
				elapsed = gApp->Timer().ElapsedTimeSecs();
			}						
			mSpriteBatch->Begin();
			mSpriteFont->DrawString(mSpriteBatch, fps_str.c_str(), DirectX::XMFLOAT2(5.f, 5.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
			mSpriteBatch->End();
		}		
	}
        
private:
	DirectX::SpriteBatch*	  mSpriteBatch;
	DirectX::SpriteFont*	  mSpriteFont;   
};