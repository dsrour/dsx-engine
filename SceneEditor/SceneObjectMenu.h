#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Scene/SceneObjectManager.h"
#include "MenuManager/Menu.h"

class EventManager;

class SceneObjectMenu :
	public Menu
{ 
public:
	SceneObjectMenu(std::shared_ptr<SceneObjectManager> soManager, std::shared_ptr<EventManager> eventManager);
	~SceneObjectMenu();

	virtual void
	Reset();

	virtual void
	Advance();

	virtual void
	OnEvent(std::string const& eventName);

private:
	static std::shared_ptr<SceneObjectManager> mSoManager;
	static std::shared_ptr<EventManager> mEventManager;

	static TwBar*            mSoTwBar;

	static void TW_CALL
	CreateFromObj(void* /*clientData*/);

	static void TW_CALL
	HideSelection(void* /*clientData*/);

	static void TW_CALL
	UnhideAll(void* /*clientData*/);
};


