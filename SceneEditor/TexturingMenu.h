#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Scene/SceneObjectManager.h"
#include "MenuManager/Menu.h"

class EventManager;

class TexturingMenu :
	public Menu
{
public:
	TexturingMenu(std::shared_ptr<SceneObjectManager> soManager);
	~TexturingMenu(void);

	virtual void
	Reset();

	virtual void
	Advance();

	virtual void
	OnEvent(std::string const& eventName);

private:
	static void TW_CALL 
	AddDiffuse(void* /*clientData*/);

	static void TW_CALL 
	AddNormal(void* /*clientData*/);

	static void TW_CALL
	AddReflection(void* /*clientData*/);

	static void TW_CALL
	SetPointSampling(void* /*clientData*/);

	static void TW_CALL
	SetLinearSampling(void* /*clientData*/);

	static void TW_CALL
	SetAnisoSampling(void* /*clientData*/);

	static std::shared_ptr<SceneObjectManager> mSoManager;

	static float mU, mV;
	static bool mLive;

	static TwBar*            mTexturingMenuTwBar;
};

