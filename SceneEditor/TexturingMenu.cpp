#include <sstream>
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "SceneEditor.h"
#include "EventManager/EventManager.h"
#include "Utils/CommonWinDialogs.h"
#include "EventNames.h"
#include "Systems/Renderer/DirectXTK/Inc/CommonStates.h"
#include "TexturingMenu.h"

std::shared_ptr<SceneObjectManager> TexturingMenu::mSoManager;

bool TexturingMenu::mLive = false;
float TexturingMenu::mV = 1.f;
float TexturingMenu::mU = 1.f;

TwBar* TexturingMenu::mTexturingMenuTwBar = NULL;


TexturingMenu::TexturingMenu(std::shared_ptr<SceneObjectManager> soManager)
{
	mSoManager = soManager;	

	mTexturingMenuTwBar = TwNewBar("Texturing");
	TwAddButton(mTexturingMenuTwBar, "MAPS", NULL, NULL, " label='MAPS' ");
	TwAddButton(mTexturingMenuTwBar, "Diff", AddDiffuse, NULL, " label='Diffuse' "); 
	TwAddButton(mTexturingMenuTwBar, "Norm", AddNormal, NULL, " label='Normal' ");   
	TwAddButton(mTexturingMenuTwBar, "Refl", AddReflection, NULL, " label='Reflection' ");

	TwAddButton(mTexturingMenuTwBar, "space00", NULL, NULL, " label=' ' ");
	TwAddButton(mTexturingMenuTwBar, "SAMPLERS", NULL, NULL, " label='SAMPLERS' ");
	TwAddButton(mTexturingMenuTwBar, "Point Sampler", SetPointSampling, NULL, " label='Point Sampler' ");
	TwAddButton(mTexturingMenuTwBar, "Linear Sampler", SetLinearSampling, NULL, " label='Linear Sampler' ");
	TwAddButton(mTexturingMenuTwBar, "Aniso Sampler", SetAnisoSampling, NULL, " label='Aniso. Sampler' ");

	TwAddButton(mTexturingMenuTwBar, "space01", NULL, NULL, " label=' ' ");
	TwAddVarRW(mTexturingMenuTwBar, "live", TW_TYPE_BOOLCPP, &mLive, "label='Live'");
	TwAddButton(mTexturingMenuTwBar, "UV SCALE", NULL, NULL, " label='UV' ");
	TwAddVarRW(mTexturingMenuTwBar, "uScale", TW_TYPE_FLOAT, &mU, "  step=0.025 precision=3 label='U' ");
	TwAddVarRW(mTexturingMenuTwBar, "vScale", TW_TYPE_FLOAT, &mV, "  step=0.025 precision=3 label='V' ");
	
		//TwDefine(" Texturing iconified=true ");
	TwDefine(" 'Texturing' size='250 235' position='1628 385' valueswidth=100 alpha=50 ");
}


TexturingMenu::~TexturingMenu(void)
{

}

void 
TW_CALL TexturingMenu::AddDiffuse( void* /*clientData*/ )
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);			
	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();

	std::wstring file;
	if (!selected.empty())
		file = BasicFileOpen(L"*.dds");
	else
		return;

	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se && se->renderableComp)
		{	
			if (!file.empty())
			{
				basepath = std::wstring(file);
				auto found = basepath.find_last_of(L"/\\");
				basepath = basepath.substr(0, found + 1);
			}

			SceneObject::SetDiffuseMapFromFile(file, se);
		}			
	}		
}

void 
TW_CALL TexturingMenu::AddNormal( void* /*clientData*/ )
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);				
	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();	

	std::wstring file;
	if (!selected.empty())
		file = BasicFileOpen(L"*.dds");
	else
		return;

	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se && se->renderableComp)
		{			
			if (!file.empty())
			{
				basepath = std::wstring(file);
				auto found = basepath.find_last_of(L"/\\");
				basepath = basepath.substr(0, found + 1);
			}

			SceneObject::SetNormalMapFromFile(file, se);
		}			
	}		
}

void 
TexturingMenu::Reset()
{
	// Set UV texture scaling to the one of the first selected SO
	auto ids = mSoManager->CurrentlySelectedSceneObjects();
	if (ids.size() == 1)
	{
		auto id = *ids.begin();
		auto so = mSoManager->SceneObjectRef(id);

		if (so->HasGeometry())
		{
			auto se = so->SubEntityRefByIndex(0);
			auto xform = se->textureXformComp;
			if (xform)
			{
				mU = xform->TextureTransformation()._11;
				mV = xform->TextureTransformation()._22;
			}
		}
	}
}

void 
TexturingMenu::Advance()
{
	if (!mLive)
		return;

	auto ids = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : ids)
	{		
		auto so = mSoManager->SceneObjectRef(id);
		
		if (so->HasGeometry())
		{
			// Assume reflection map was added to subentity if it was already created
			if (!so->SubEntityRefByIndex(0)->textureXformComp)
			{
				so->SubEntityRefByIndex(0)->textureXformComp = std::make_shared<TextureTransformed>();
				gApp->EntityMngr()->AddComponentToEntity(so->SubEntityRefByIndex(0)->entityId, so->SubEntityRefByIndex(0)->textureXformComp);
			}
			
			DirectX::XMStoreFloat4x4(
				&so->SubEntityRefByIndex(0)->textureXformComp->TextureTransformation(),
				DirectX::XMMatrixScaling(mU, mV, 1.f)	);
		}
	}
}

void 
TexturingMenu::OnEvent(std::string const& eventName)
{
	if (eventName == SO_GEOMETRY_CHANGE ||
		eventName == SO_SELECTION_CHANGE)
	{
		Reset();
	}
}

void TW_CALL 
TexturingMenu::AddReflection(void* /*clientData*/)
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);
	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();
	
	std::wstring file;
	if (!selected.empty())
		file = BasicFileOpen(L"*.dds");
	else
		return;

	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se && se->renderableComp)
		{

			if (file.empty())
			{
				basepath = std::wstring(file);
				auto found = basepath.find_last_of(L"/\\");
				basepath = basepath.substr(0, found + 1);
			}

			SceneObject::SetReflectionMapFromFile(file, se);
		}
	}
}

void TW_CALL
TexturingMenu::SetPointSampling(void* /*clientData*/)
{	
	D3D11_SAMPLER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;

	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	desc.MaxAnisotropy = (gApp->Renderer()->Device()->GetFeatureLevel() > D3D_FEATURE_LEVEL_9_1) ? 16 : 2;

	desc.MaxLOD = FLT_MAX;
	desc.ComparisonFunc = D3D11_COMPARISON_NEVER;

	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se->diffuseMappedComp)		
			se->diffuseMappedComp->DiffuseMapSamplerDesc(desc);
		if (se->normalMappedComp)
			se->normalMappedComp->NormalMapSamplerDesc(desc);	
	}
}

void TW_CALL 
TexturingMenu::SetLinearSampling(void* /*clientData*/)
{
	D3D11_SAMPLER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	desc.MaxAnisotropy = (gApp->Renderer()->Device()->GetFeatureLevel() > D3D_FEATURE_LEVEL_9_1) ? 16 : 2;

	desc.MaxLOD = FLT_MAX;
	desc.ComparisonFunc = D3D11_COMPARISON_NEVER;

	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se->diffuseMappedComp)
			se->diffuseMappedComp->DiffuseMapSamplerDesc(desc);
		if (se->normalMappedComp)
			se->normalMappedComp->NormalMapSamplerDesc(desc);
	}
}

void TW_CALL 
TexturingMenu::SetAnisoSampling(void* /*clientData*/)
{
	D3D11_SAMPLER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Filter = D3D11_FILTER_ANISOTROPIC;

	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	desc.MaxAnisotropy = (gApp->Renderer()->Device()->GetFeatureLevel() > D3D_FEATURE_LEVEL_9_1) ? 16 : 2;

	desc.MaxLOD = FLT_MAX;
	desc.ComparisonFunc = D3D11_COMPARISON_NEVER;

	std::set<unsigned int> selected = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selected)
	{
		// Make sure a geometry exists
		SceneObject::SubEntity* se = mSoManager->SceneObjectRef(id)->SubEntityRefByIndex(0);
		if (se->diffuseMappedComp)
			se->diffuseMappedComp->DiffuseMapSamplerDesc(desc);
		if (se->normalMappedComp)
			se->normalMappedComp->NormalMapSamplerDesc(desc);
	}
}


