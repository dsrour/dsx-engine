#include <algorithm>
#include "Components/Spatialized.h"
#include "SceneEditor.h"
#include "Scene/SceneObjectManager.h"
#include "EventNames.h"
#include "SpatialTransformationMenu.h"

std::shared_ptr<SceneObjectManager> SpatialTransformationMenu::mSoManager;

TwBar*            SpatialTransformationMenu::mMenu = NULL;
float			  SpatialTransformationMenu::mScale;
float			  SpatialTransformationMenu::mPosX;
float			  SpatialTransformationMenu::mPosY;
float			  SpatialTransformationMenu::mPosZ;
float			  SpatialTransformationMenu::mRotX;
float			  SpatialTransformationMenu::mRotY;
float			  SpatialTransformationMenu::mRotZ;


SpatialTransformationMenu::SpatialTransformationMenu(std::shared_ptr<SceneObjectManager> soManager)
{
	mSoManager = soManager;

	mMenu = TwNewBar("Spatial Transformation");
	TwDefine(" 'Spatial Transformation' help='Right mouse button drag to scale.\nLeft mouse button translates by an axis.\nMiddle mouse drag rotates by an axis.\n'");
	//TwDefine(" 'Spatial Transformation' iconified=true ");
	TwDefine(" 'Spatial Transformation' size='250 175' position='25 390' ");
	TwDefine(" 'Spatial Transformation' refresh=0.2 ");
	TwAddVarRO(mMenu, "ST_posx", TW_TYPE_FLOAT, &mPosX, " label='Pos.X' ");
	TwAddVarRO(mMenu, "ST_posy", TW_TYPE_FLOAT, &mPosY, " label='Pos.Y' ");
	TwAddVarRO(mMenu, "ST_posz", TW_TYPE_FLOAT, &mPosZ, " label='Pos.Z' ");
	TwAddVarRO(mMenu, "ST_rotx", TW_TYPE_FLOAT, &mRotX, " label='Rot.X' ");
	TwAddVarRO(mMenu, "ST_roty", TW_TYPE_FLOAT, &mRotY, " label='Rot.Y' ");
	TwAddVarRO(mMenu, "ST_rotz", TW_TYPE_FLOAT, &mRotZ, " label='Rot.Z' ");
	TwAddVarRO(mMenu, "ST_scale", TW_TYPE_FLOAT, &mScale, " label='Scale' ");
	TwAddButton(mMenu, "space00", NULL, NULL, " label=' ' ");
	TwAddButton(mMenu, "Reset Selection", ResetSelection, NULL, " label='Reset Selection' ");
}


SpatialTransformationMenu::~SpatialTransformationMenu(void)
{

}

void 
SpatialTransformationMenu::Advance()
{
	// Refresh variables
	std::set<unsigned int> selected = static_cast<SceneEditor*>(gApp)->SceneObjectMngr()->CurrentlySelectedSceneObjects();
	
	if (selected.size() > 1 || selected.empty())
	{
		mPosX = mPosY = mPosZ = mRotX = mRotY = mRotZ = mScale = 0.;
	}
	else
	{
		int ent_id = *selected.begin();

		std::shared_ptr<IComponent> tmp_comp;
		gApp->EntityMngr()->GetComponentFromEntity(ent_id, Spatialized::GetGuid(), tmp_comp);
		
		if (tmp_comp)
		{
			Spatialized* spatialized = static_cast<Spatialized*>(tmp_comp.get());					
						
			DirectX::XMFLOAT3 pos = spatialized->LocalPosition();
			mPosX = pos.x;
			mPosY = pos.y;
			mPosZ = pos.z;
			
			DirectX::XMMATRIX curr_rot = DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&spatialized->LocalRotation()));
			DirectX::XMVECTOR scale_tmp, rot, trans_tmp;			
			DirectX::XMMatrixDecompose(&scale_tmp, &rot, &trans_tmp, curr_rot);
			mRotX = DirectX::XMVectorGetX(rot);
			mRotY = DirectX::XMVectorGetY(rot);
			mRotZ = DirectX::XMVectorGetZ(rot);

			DirectX::XMFLOAT3 scale = spatialized->LocalScale();
			mScale = (std::max)( scale.x, (std::max)(scale.y, scale.z) );
		}
	}
}

void
SpatialTransformationMenu::OnEvent(std::string const& eventName)
{
	if (eventName == SO_TRANSFORMED || eventName == SO_SELECTION_CHANGE)
	{	
		Advance();
	}
}

void 
SpatialTransformationMenu::Reset()
{

}

void TW_CALL 
SpatialTransformationMenu::ResetSelection(void* /*clientData*/)
{
	for (auto id : mSoManager->CurrentlySelectedSceneObjects())
	{
		mSoManager->SceneObjectRef(id)->SpatializedComponent()->LocalPosition(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
		mSoManager->SceneObjectRef(id)->SpatializedComponent()->LocalScale(DirectX::XMFLOAT3(1.f, 1.f, 1.f));
		mSoManager->SceneObjectRef(id)->SpatializedComponent()->LocalRotation(DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	}
}
