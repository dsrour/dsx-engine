#include "Debug/Debug.h"
#include "BaseApp.h"
#include "Components/Renderable.h"
#include "EventNames.h"
#include "RenderableComponentMenu.h"

extern BaseApp* gApp;

std::shared_ptr<SceneObjectManager> RenderableComponentMenu::mSoManager;
TwBar*           RenderableComponentMenu::mRenderableTwBar = nullptr;
MaterialMenuVars RenderableComponentMenu::mMaterialMenuVars;
bool RenderableComponentMenu::mLiveMaterial = false;
bool RenderableComponentMenu::mBackCullToggle = false;
unsigned int RenderableComponentMenu::mType = Renderable::LIT;


RenderableComponentMenu::RenderableComponentMenu(std::shared_ptr<SceneObjectManager> soManager)
{
	mSoManager = soManager;

	mRenderableTwBar = TwNewBar("Renderable");

	TwType light_models;
	light_models = TwDefineEnum("Lighting Model", NULL, 0);
	TwType types;
	types = TwDefineEnum("Type", NULL, 0);

	TwAddVarRW(mRenderableTwBar, "Type", types, &mType, 
		" label='Type' enum='0 {LIT}, 1 {FLAT}, 2 {SKY}, 3 {POINT_CLOUD}, 4 {PARTICLES}, 5 {FUR}' ");
	
	TwAddButton(mRenderableTwBar, "space0", NULL, NULL, " label=' ' ");
	TwAddButton(mRenderableTwBar, "GEOMETRY", NULL, NULL, " label='GEOMETRY' ");
	TwAddButton(mRenderableTwBar, "Toggle Back Cull", ToggleBackCull, NULL, " label='Toggle Back Cull' ");
	TwAddButton(mRenderableTwBar, "Triangle Primitive", SetPrimTopologyToTris, NULL, " label='Triangle Primitive' ");
	TwAddButton(mRenderableTwBar, "Point Primitive", SetPrimTopologyToPnts, NULL, " label='Point Primitive' ");
	TwAddButton(mRenderableTwBar, "space1", NULL, NULL, " label=' ' ");
	TwAddButton(mRenderableTwBar, "enable_sh_cast", EnableShadowCasting, NULL, " label='Enable Shadow Casting' ");
	TwAddButton(mRenderableTwBar, "disable_sh_cast", DisableShadowCasting, NULL, " label='Disable Shadow Casting' ");
	TwAddButton(mRenderableTwBar, "space2", NULL, NULL, " label=' ' ");
	TwAddButton(mRenderableTwBar, "space3", NULL, NULL, " label=' ' ");
	TwAddVarRW(mRenderableTwBar, "Live", TW_TYPE_BOOLCPP, &mLiveMaterial, " label='Live' ");
	TwAddButton(mRenderableTwBar, "MATERIAL", NULL, NULL, " label='MATERIAL' ");
	TwAddVarRW(mRenderableTwBar, "Ambient", TW_TYPE_COLOR4F, &mMaterialMenuVars.ambR, " label='Ambient' ");
	TwAddVarRW(mRenderableTwBar, "Diffuse", TW_TYPE_COLOR4F, &mMaterialMenuVars.difR, " label='Diffuse' ");
	TwAddVarRW(mRenderableTwBar, "Specular", TW_TYPE_COLOR4F, &mMaterialMenuVars.specR, " label='Specular' ");
	TwAddVarRW(mRenderableTwBar, "Reflection", TW_TYPE_COLOR4F, &mMaterialMenuVars.reflR, " label='Reflection' ");
	TwAddVarRW(mRenderableTwBar, "Instanced Color", TW_TYPE_COLOR4F, &mMaterialMenuVars.instancedR, " label='Instanced Color' ");
	TwAddVarRW(mRenderableTwBar, "Reflection Coeff", TW_TYPE_FLOAT, &mMaterialMenuVars.reflectionCoeff, " min=0.0 step=0.025 precision=6 label='Reflection Coeff.' ");	
	TwAddVarRW(mRenderableTwBar, "Lighting Model", light_models, &mMaterialMenuVars.lightingModel, " label='Lighting Model' enum='0 {PHONG}, 1 {COOK-TORRANCE}' ");	
	TwAddButton(mRenderableTwBar, "Apply Material", ApplyMaterial, NULL, " label='Apply Material' ");
		
	//TwDefine(" Renderable iconified=true ");
	TwDefine(" 'Renderable' size='250 365' position='1628 10' valueswidth=100 alpha=50 ");
}


RenderableComponentMenu::~RenderableComponentMenu()
{
}

void RenderableComponentMenu::Reset()
{
	//mLiveMaterial = false;
	mBackCullToggle = true;
		
	if (!mSoManager->CurrentlySelectedSceneObjects().empty())
	{
		auto id = *mSoManager->CurrentlySelectedSceneObjects().begin();
		auto so = mSoManager->SceneObjectRef(id);
		if (so->HasGeometry())
		{
			mBackCullToggle = so->SubEntityRefByIndex(0)->renderableComp->BackCulled();
			mType = so->SubEntityRefByIndex(0)->renderableComp->RenderableType();
		}
	}
} 

void 
RenderableComponentMenu::Advance()
{
	if (!mSoManager->CurrentlySelectedSceneObjects().empty())
	{
		auto id = *mSoManager->CurrentlySelectedSceneObjects().begin();
		auto so = mSoManager->SceneObjectRef(id);
		if (so->HasGeometry())
		{
			so->SubEntityRefByIndex(0)->renderableComp->RenderableType(mType);
		}
	}

	if (mLiveMaterial)
	{
		void* tmp = nullptr;
		ApplyMaterial(tmp);
	}
}

void TW_CALL 
RenderableComponentMenu::SetPrimTopologyToTris(void* /*clientData*/)
{
	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->SetPrimitiveToTris();
	}
}

void TW_CALL 
RenderableComponentMenu::SetPrimTopologyToPnts(void* /*clientData*/)
{
	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->SetPrimitiveToPoints();
	}
}

void TW_CALL 
RenderableComponentMenu::ToggleBackCull(void* /*clientData*/)
{
	mBackCullToggle = !mBackCullToggle;

	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->ToggleBackCulling(mBackCullToggle);		
	}
}

void TW_CALL 
RenderableComponentMenu::ApplyMaterial(void* /*clientData*/)
{	
	Material mat = CreateMaterialFromMenuVars();

	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->SetMaterial(mat);
	}
}

void 
RenderableComponentMenu::OnEvent(std::string const& eventName)
{
	if (eventName == std::string(SO_SELECTION_CHANGE))
	{
		Reset();
		// If something is selected, set to settings of the first one
		auto selection = mSoManager->CurrentlySelectedSceneObjects();
		if (!selection.empty())
		{
			int id = *selection.begin();	
			std::shared_ptr<SceneObject> so = mSoManager->SceneObjectRef(id);
			if (!so->HasGeometry())
				return;
			SceneObject::SubEntity* se = so->SubEntityRefByIndex(0);

			DirectX::XMFLOAT4 instanced_color;
			if (se->renderableComp)
			{
				Material mat = CreateMaterialFromRenderable(se->renderableComp);
				instanced_color = se->renderableComp->InstancedColor();
				mBackCullToggle = se->renderableComp->BackCulled();
			}
		}
	}
}

Material 
RenderableComponentMenu::CreateMaterialFromRenderable(std::shared_ptr<Renderable> const& renderable)
{
	Material mat;

	if (renderable)
	{ 
		mMaterialMenuVars.ambR = renderable->MaterialProperties().ambient.x;
		mMaterialMenuVars.ambG = renderable->MaterialProperties().ambient.y;
		mMaterialMenuVars.ambB = renderable->MaterialProperties().ambient.z;
		mMaterialMenuVars.ambA = renderable->MaterialProperties().ambient.w;
		mMaterialMenuVars.difR = renderable->MaterialProperties().diffuse.x;
		mMaterialMenuVars.difG = renderable->MaterialProperties().diffuse.y;
		mMaterialMenuVars.difB = renderable->MaterialProperties().diffuse.z;
		mMaterialMenuVars.difA = renderable->MaterialProperties().diffuse.w;
		mMaterialMenuVars.specR = renderable->MaterialProperties().specular.x;
		mMaterialMenuVars.specG = renderable->MaterialProperties().specular.y;
		mMaterialMenuVars.specB = renderable->MaterialProperties().specular.z;
		mMaterialMenuVars.specA = renderable->MaterialProperties().specular.w;
		mMaterialMenuVars.reflR = renderable->MaterialProperties().reflect.x;
		mMaterialMenuVars.reflG = renderable->MaterialProperties().reflect.y;
		mMaterialMenuVars.reflB = renderable->MaterialProperties().reflect.z;
		mMaterialMenuVars.reflA = renderable->MaterialProperties().reflect.w;
		mMaterialMenuVars.lightingModel = renderable->MaterialProperties().lightingModel;
		mMaterialMenuVars.reflectionCoeff = renderable->MaterialProperties().reflectionCoeff;
	}

	return mat;
}

Material 
RenderableComponentMenu::CreateMaterialFromMenuVars()
{
	Material mat;
	mat.ambient = DirectX::XMFLOAT4(mMaterialMenuVars.ambR, mMaterialMenuVars.ambG, mMaterialMenuVars.ambB, mMaterialMenuVars.ambA);
	mat.diffuse = DirectX::XMFLOAT4(mMaterialMenuVars.difR, mMaterialMenuVars.difG, mMaterialMenuVars.difB, mMaterialMenuVars.difA);
	mat.specular = DirectX::XMFLOAT4(mMaterialMenuVars.specR, mMaterialMenuVars.specG, mMaterialMenuVars.specB, mMaterialMenuVars.specA);
	mat.reflect = DirectX::XMFLOAT4(mMaterialMenuVars.reflR, mMaterialMenuVars.reflG, mMaterialMenuVars.reflB, mMaterialMenuVars.reflA);
	mat.lightingModel = mMaterialMenuVars.lightingModel;
	mat.reflectionCoeff = mMaterialMenuVars.reflectionCoeff;
	return mat;
}

void TW_CALL 
RenderableComponentMenu::EnableShadowCasting(void* /*clientData*/)
{
	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->SetShadowCasting(true);
	}
}

void TW_CALL 
RenderableComponentMenu::DisableShadowCasting(void* /*clientData*/)
{
	auto selection = mSoManager->CurrentlySelectedSceneObjects();
	for (auto id : selection)
	{
		mSoManager->SceneObjectRef(id)->SetShadowCasting(false);
	}
}

