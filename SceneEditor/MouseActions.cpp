#include <algorithm>
#include "Debug/Debug.h"
#include "Components/Spatialized.h"
#include "Intersections.h"
#include "SceneEditor.h"
#include "EventNames.h"
#include "MouseActions.h"

ApplicationMouseActions::ApplicationMouseActions(
	std::shared_ptr<InputDeviceManager> inputDeviceManager,
	std::shared_ptr<SceneObjectManager> soMngr,
	std::shared_ptr<StateManager> stateMngr,
	std::shared_ptr<EventManager> eventMngr) : InputDeviceActions(inputDeviceManager)
{
	mSoMngr = soMngr;
	mStateMngr = stateMngr;
	mEventMngr = eventMngr;   
	mCamera = gApp->Renderer()->CurrentCamera();   
	inputDeviceManager->Mouse()->ToggleRelativeMode(false);
} 

void 
ApplicationMouseActions::OnStateChange( void )
{
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;
		
	bool bail = false; // will tell us if we should stop doing actions

	if (mStateMngr->IsStateOn(SPATIAL_TRANSFORM))
	{
		bail = DoSpatialTransformActions(manager);
	}	

	if (manager->Mouse()->RelativeMode()  && !bail)
		CameraTransformActions(manager);
	{
		mLastX = manager->Mouse()->MouseX();
		mLastY = manager->Mouse()->MouseY();
	}


	// Following is scene objects selection logic
	if (!bail)
		DoSelectionActions(manager);			


}

bool const 
ApplicationMouseActions::DoSpatialTransformActions( std::shared_ptr<InputDeviceManager>& manager )
{
	bool ret = false;
		
	int x_incr = 0;
	int y_incr = 0;

	if (manager->Mouse()->RelativeMode())
	{
		x_incr = manager->Mouse()->MouseX();
		y_incr = manager->Mouse()->MouseY();
	}
	else
	{
		x_incr = manager->Mouse()->MouseX() - mLastX;
		y_incr = mLastY - manager->Mouse()->MouseY();
	}

	// SCALE
	// Get selections' spatial component
	std::set<unsigned int>::iterator iter;
	for (iter = mSoMngr->CurrentlySelectedSceneObjects().begin();
		 iter != mSoMngr->CurrentlySelectedSceneObjects().end();
		 iter++) 
	{
		std::shared_ptr<IComponent>  tmp_comp;
		gApp->EntityMngr()->GetComponentFromEntity(*iter, Spatialized::GetGuid(), tmp_comp);

		if (tmp_comp)
		{			
			Spatialized* spatialized = static_cast<Spatialized*>(tmp_comp.get());	
			
			if ( MouseInputDevice::PRESSED == manager->Mouse()->ButtonState(MouseInputDevice::RIGHT) )
			{
				float scale_increment = 0.005f; // by pixel	
				if (manager->Keyboard()->IsKeyPressed(VK_SHIFT))
					scale_increment = 0.05f;				

				DirectX::XMFLOAT3 scale_xyz = spatialized->LocalScale();
				float scale = (std::max)(scale_xyz.x, (std::max)(scale_xyz.y, scale_xyz.z) ); 
				scale += ((x_incr+y_incr) * scale_increment);
				if (scale < 0.001f)
					scale = 0.001f;
				spatialized->LocalScale(DirectX::XMFLOAT3(scale, scale, scale));

				ret = true;
			}
		}			
	}
		
	// Translate and Rotate are delegated to the transform axes widget
	if ( static_cast<SceneEditor*>(gApp)->WidgetMngr()->TransformAxesWidget()->InteractionActions(x_incr, y_incr) ) 
		ret = true;

	static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_TRANSFORMED, (Metadata* const)NULL);

	return ret;
}

void
ApplicationMouseActions::CameraTransformActions( std::shared_ptr<InputDeviceManager>& manager )
{
	/*static double last_time = 0.0;
	double new_time = gApp->Timer().ElapsedTimeSecs();
	float delta = (float)new_time - (float)last_time;
	last_time = new_time;*/
		
	float const sensitivity = 0.001f;
	float x = manager->Mouse()->MouseX()*sensitivity;
	float y = manager->Mouse()->MouseY()*sensitivity;

	//OutputDebugMsg(to_string(x)+", "+ to_string(y)+"\n");
	
	static_cast<FpCamera*>(mCamera.get())->Yaw(x);      	
	static_cast<FpCamera*>(mCamera.get())->Pitch(y);      
} 

void 
ApplicationMouseActions::DoSelectionActions( std::shared_ptr<InputDeviceManager> manager )
{
	//OutputDebugMsg("DoSelectionActions()\n");
	static std::set<unsigned int> picked;
	static int last_toggled = -1;

	if (    MouseInputDevice::CLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::MIDDLE) ||
			MouseInputDevice::DBLE_CLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
	{
		std::set<unsigned int> entities = mSoMngr->SceneObjectsIds();

		float dist = 1000.f;
		MousePick(entities, picked, dist);	

		if (!picked.empty())
		{
			std::set<unsigned int> entities = mSoMngr->SceneObjectsIds();
			float dist = 1000.f;
			MousePick(entities, picked, dist);		

			std::set<unsigned int> to_select;
			if (mSoMngr->SceneObjectRef(*picked.begin())->IsVisible())
			{
				to_select.insert(*picked.begin());
				mSoMngr->ToggleSceneObjects(to_select);

				last_toggled =  *picked.begin();

				//OutputDebugMsg("CLICK - " + to_string(*picked.begin()) + "\n");

				static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
			}
		}	
		//else if (last_toggled >= 0)
		//{
		//	mSoMngr->ToggleSceneObjectsSelection(last_toggled);

		//	mStateMngr->BroadcastStateChange(SO_SELECTION_CHANGE);
		//}
	}

	if ( MouseInputDevice::PRESSED == manager->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
	{		
		//OutputDebugMsg("PRESSED - picked.size() = " + to_string(picked.size()) + "\n");
		if (!picked.empty() && last_toggled >= 0) 
		{
			//OutputDebugMsg("PRESSED - last_toggled = " + to_string(last_toggled) + "\n");
			// Scrolling while holding will toggle other objects that were picked instead

			// Get iterator to prev and next toggles
			std::set<unsigned int>::iterator curr_iter = picked.find(last_toggled);
			std::set<unsigned int>::iterator next_iter = curr_iter; next_iter++;
			std::set<unsigned int>::iterator prev_iter = curr_iter;

			if (curr_iter == picked.begin())	
			{
				prev_iter = picked.end();
				--prev_iter;
			}
			else 
				prev_iter--;

			if (next_iter == picked.end())
				next_iter = picked.begin();


			if ( MouseInputDevice::WHEEL_UP == manager->Mouse()->WheelState() )
			{
				std::set<unsigned int> to_toggle;								
				to_toggle.insert(*curr_iter);
				mSoMngr->ToggleSceneObjects(to_toggle);


				to_toggle.clear();

				if (mSoMngr->SceneObjectRef(*next_iter)->IsVisible())
				{
					to_toggle.insert(*next_iter);
					mSoMngr->ToggleSceneObjects(to_toggle);
					
					//OutputDebugMsg("PRESSED WU - last_toggled = " + to_string(last_toggled) + "\n");

					static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
				}

				last_toggled =  *next_iter;
			}		
			else if ( MouseInputDevice::WHEEL_DOWN == manager->Mouse()->WheelState() )
			{
				std::set<unsigned int> to_toggle;								
				to_toggle.insert(*curr_iter);
				mSoMngr->ToggleSceneObjects(to_toggle);


				to_toggle.clear();

				if (mSoMngr->SceneObjectRef(*prev_iter)->IsVisible())
				{
					to_toggle.insert(*prev_iter);
					mSoMngr->ToggleSceneObjects(to_toggle);
					
					//OutputDebugMsg("PRESSED WD - last_toggled = " + to_string(last_toggled) + "\n");

					static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
				}
				last_toggled =  *prev_iter;
			}			
		}	
	}	

	if ( MouseInputDevice::UNCLICK == manager->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
	{
		//OutputDebugMsg("UNCLICK\n");
		picked.clear();
		last_toggled = -1;
	}
}
