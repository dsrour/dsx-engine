#include "Debug/Debug.h"
#include "SceneEditor.h"
#include "Utils/CommonWinDialogs.h"
#include "Scene/SceneObjectManager.h"
#include "Scene/SoExporter.h"
#include "EventManager/EventManager.h"
#include "EventNames.h"
#include "Scene/SoExporter.h"
#include "Utils/StringUtils.h"
#include "Scene/ObjImporter.h"
#include "Scene/SoImporter.h"
#include "Scene/ProtoUtils.h"
#include "Scene/SceneLoaderWriter.h"
#include "FileMenu.h"

extern BaseApp* gApp;

std::shared_ptr<EventManager> FileMenu::mEventManager;
std::shared_ptr<SceneObjectManager> FileMenu::mSoMngr;
std::shared_ptr<ForwardRenderer> FileMenu::mFrwrdRenderer;
TwBar* FileMenu::mFileMenuTwBar	= NULL;

FileMenu::FileMenu(
		std::shared_ptr<SceneObjectManager> soMngr, 
		std::shared_ptr<EventManager> eventManager,
		std::shared_ptr<ForwardRenderer> frwrdRenderer)
{
	mSoMngr = soMngr; 
	mEventManager = eventManager;
	mFrwrdRenderer = frwrdRenderer;

	mFileMenuTwBar = TwNewBar("File");

	TwAddButton(mFileMenuTwBar, "Scene", NULL, NULL, " label='SCENE' ");
	TwAddButton(mFileMenuTwBar, "load_scene", LoadScene, NULL, " label='Load' ");
	TwAddButton(mFileMenuTwBar, "save_scene", SaveScene, NULL, " label='Save' ");

	TwAddButton(mFileMenuTwBar, "space01", NULL, NULL, " label=' ' ");
	TwAddButton(mFileMenuTwBar, "IMPORT1", NULL, NULL, " label='IMPORT' ");
	TwAddButton(mFileMenuTwBar, "From .so1", ImportSo, NULL, " label='From .so' ");	
	TwAddButton(mFileMenuTwBar, "From .obj1", ImportObj, NULL, " label='From .obj' ");
	

	TwAddButton(mFileMenuTwBar, "space02", NULL, NULL, " label=' ' ");
	TwAddButton(mFileMenuTwBar, "EXPORT1", NULL, NULL, " label='EXPORT SELECTION' ");
	TwAddButton(mFileMenuTwBar, "To .so1", ExportSo, NULL, " label='As .so' ");
	
	//TwDefine(" Settings iconified=true ");
	TwDefine(" 'File' size='250 210' position='25 10' alpha=50 ");
}


FileMenu::~FileMenu(void)
{
	ShutDownPrototypeLibrary();
}

void 
FileMenu::Reset()
{

}

void TW_CALL 
FileMenu::ImportObj(void* /*clientData*/)
{	
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);

	auto files = BasicFileOpenMultiple(L"*.obj", basepath);

	for (auto file : files)
	{
		if (!file.empty())
		{
			// EACH FILE WILL IMPORT EACH GEOMETRY WITHIN EACH FILE TO THEIR OWN UNIQUE SCENE OBJECT.
			// THIS WILL ALLOW TO MODIFY EACH GEOM SEPARATELY BEFORE EXPORTING TO .so
			basepath = std::wstring(file);
			auto found = basepath.find_last_of(L"/\\");
			basepath = basepath.substr(0, found + 1);

			auto sos = ObjImporter::ImportFromFileMultiple(file, basepath, true);

			if (!sos.empty())
			{
				for (auto so : sos)
				{
					unsigned int id = so->EntityId();
					so->SetVisibility(true);
					mSoMngr->AddSceneObject(so);
					mSoMngr->SelectSceneObject(id);
				}

				// TODO: Move this outa the loop?
				mEventManager->BroadcastEvent(SO_GEOMETRY_CHANGE, (Metadata* const)NULL); // TODO:: AXES WIDGET expects a selected object... should get fixed
				mEventManager->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
			}
		}
	}
}

void TW_CALL 
FileMenu::ExportSo(void* /*clientData*/)
{		
	static std::wstring basepath = L"";

	// TODO:: output some type of msg that tells user it can't export unless there's a selection

	auto ids = mSoMngr->CurrentlySelectedSceneObjects();
	if (!ids.empty())
	{
		gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);
		std::wstring file = BasicFileSave(basepath);

		if (file.length() > 3)
		{
			basepath = std::wstring(file);
			auto found = basepath.find_last_of(L"/\\");
			basepath = basepath.substr(0, found + 1);

			// Add .so extension if it isn't there
			std::string to_compare(file.end() - 3, file.end());
			if (to_compare != ".so" && to_compare != ".SO")
				file += L".so"; 

			std::vector< std::shared_ptr<SceneObject> > out_sos;

			for (auto id : ids)
				out_sos.push_back(mSoMngr->SceneObjectRef(id));
						
			SoExporter::ExportToFile(std::string(file.begin(), file.end()), out_sos);
		}
	}
}

void TW_CALL
FileMenu::ImportSo(void* /*clientData*/)
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);


	auto files = BasicFileOpenMultiple(L"*.so", basepath);

	for (auto file : files)
	{
		if (!file.empty())
		{
			basepath = std::wstring(file);
			auto found = basepath.find_last_of(L"/\\");
			basepath = basepath.substr(0, found + 1);

			if (!file.empty())
			{
				std::shared_ptr<SceneObject> so = SoImporter::ImportFromFile(file, true);
				unsigned int id = so->EntityId();
				so->SetVisibility(true);
				mSoMngr->AddSceneObject(so);
				mSoMngr->SelectSceneObject(id);
			
				// TODO: Move this outa the loop?
				mEventManager->BroadcastEvent(SO_GEOMETRY_CHANGE, (Metadata* const)NULL); // TODO:: AXES WIDGET expects a selected object... should get fixed
				mEventManager->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
			}
		}
	}
}

void TW_CALL 
FileMenu::LoadScene(void* /*clientData*/)
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);


	auto file = BasicFileOpen(L"*.sc", basepath);

	if (!file.empty())
	{
		basepath = std::wstring(file);
		auto found = basepath.find_last_of(L"/\\");
		basepath = basepath.substr(0, found + 1);

		if (!file.empty())
		{
			SceneLoaderWriter::LoadSceneFromFile(std::string(file.begin(), file.end()), mSoMngr, mFrwrdRenderer, true);

			// TODO: Move this outa the loop?
			mEventManager->BroadcastEvent(SO_GEOMETRY_CHANGE, (Metadata* const)NULL); // TODO:: AXES WIDGET expects a selected object... should get fixed
			mEventManager->BroadcastEvent(SO_SELECTION_CHANGE, (Metadata* const)NULL);
		}
	}
}

void TW_CALL 
FileMenu::SaveScene(void* /*clientData*/)
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);
	std::wstring file = BasicFileSave(basepath);

	if (file.length() > 3)
	{
		basepath = std::wstring(file);
		auto found = basepath.find_last_of(L"/\\");
		basepath = basepath.substr(0, found + 1);

		// Add .so extension if it isn't there
		std::string to_compare(file.end() - 3, file.end());
		if (to_compare != ".sc" && to_compare != ".SC")
			file += L".sc";

		SceneLoaderWriter::WriteSceneToFile(std::string(file.begin(), file.end()), mSoMngr, mFrwrdRenderer);

	}
}
