#pragma once
#include "TransformAxes.h"

/* For now this class just holds the widget objects and provides them to others */
class WidgetManager
{
public:
	WidgetManager(
		std::shared_ptr<SceneObjectManager> soMngr,
		std::shared_ptr<StateManager> stateMngr,
		std::shared_ptr<EventManager> eventMngr)
	{
		mTransformAxes = new TransformAxes(soMngr, stateMngr, eventMngr);
	}

	~WidgetManager()
	{
		if (mTransformAxes)
			delete mTransformAxes;
	}

	TransformAxes* const
	TransformAxesWidget(void) const {return mTransformAxes;}

private:
	TransformAxes* mTransformAxes;
};