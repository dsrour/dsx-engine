#pragma once

#include <set>
#include "Components/Renderable.h"
#include "KeyboardActions.h"
#include "MouseActions.h"
#include "StateManager.h"
#include "Scene/SceneObjectManager.h"
#include "WidgetManager.h"
#include "MenuManager/MenuManager.h"
#include "EventManager/EventManager.h"

#include "BaseApp.h"

class ApplicationKeyboardActions;
class ApplicationMouseActions;

class SceneEditor : 
	public EventSubscriber,
    public BaseApp
{
public:
    SceneEditor(void);
    ~SceneEditor(void);

    bool const
    OnLoadApp(void);

    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);

    void
    OnUnloadApp(void);		

	std::shared_ptr<StateManager> const&
	StateMngr(void) const {return mStateManager;}

	std::shared_ptr<SceneObjectManager> const&
	SceneObjectMngr(void) const {return mSceneObjectManager;}

	std::shared_ptr<WidgetManager> const&
	WidgetMngr(void) const {return mWidgetManager;}

	float const& 
	NavSpeedFactor() const { return mNavSpeedFactor; }

	void 
	NavSpeedFactor(float const& val) { mNavSpeedFactor = val; mMenuManager->ResetMenus(); }

	void
	ToggleFloorGridRenderableEntity(bool const& show);

	std::shared_ptr<EventManager> const&
	EventMnger(void) const { return mEventManager; }
	
private: 
	virtual void
	OnEvent(std::string const& eventName, Metadata* const metadata = NULL);

	////////////////////////////////////////////////////////
	friend class ApplicationKeyboardActions; /// TODO:: super hacky! Expand state manager to broadcast metadata with the state change
	////////////////////////////////////////////////////////

	float	mNavSpeedFactor;

	std::shared_ptr<ApplicationKeyboardActions> mKeyboardActions;		
	std::shared_ptr<ApplicationMouseActions> mMouseActions;	

	std::shared_ptr<StateManager>			mStateManager;
	std::shared_ptr<SceneObjectManager>		mSceneObjectManager;
	std::shared_ptr<WidgetManager>			mWidgetManager;
	std::shared_ptr<EventManager>			mEventManager;
		
	// Menus
	std::shared_ptr<MenuManager> mMenuManager;	
};

