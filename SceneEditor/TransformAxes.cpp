#include "Settings.h"
#include "CommonRenderables.h"
#include "SceneEditor.h"
#include "Scene/SceneObjectManager.h"
#include "Intersections.h"
#include "EventNames.h"
#include "TransformAxes.h"

TransformAxes::TransformAxes(
	std::shared_ptr<SceneObjectManager> soMngr, 
	std::shared_ptr<StateManager> stateMngr,
	std::shared_ptr<EventManager> eventMngr)
{
	mSoMngr = soMngr;
	mStateMngr = stateMngr;
	mEventMngr = eventMngr;
	mAxesIds[0] = mAxesIds[1] = mAxesIds[2] = mSoId = -1;	
	mEventMngr->SubscribeToEvent(SO_GEOMETRY_CHANGE, this);
	mEventMngr->SubscribeToEvent(SO_SELECTION_CHANGE, this);
}


TransformAxes::~TransformAxes(void)
{
	UnbindTransformAxes();	
	mEventMngr->UnsubscribeToEvent(SO_GEOMETRY_CHANGE, this);
	mEventMngr->UnsubscribeToEvent(SO_SELECTION_CHANGE, this);
}

void 
TransformAxes::BindTransformAxesToSO( unsigned int const& soId )
{
	if (mSoMngr->CurrentlySelectedSceneObjects().empty())
		return;

	mSoId = soId;
		
	UnbindTransformAxes();
			
	std::vector<DirectX::XMFLOAT3> verts_pos;

	// X
	mAxesIds[0] = gApp->EntityMngr()->CreateEntity();                  
	std::shared_ptr<AxesRenderable> renderable_obj_x(new AxesRenderable(gApp->Renderer()->Device(), *mSoMngr->CurrentlySelectedSceneObjects().begin(), AxesRenderable::X_AXIS, &verts_pos));
	Material mat;	
	mat.diffuse = TRANSFORM_AXES_COLOR;
	renderable_obj_x->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[0], renderable_obj_x);
	DirectX::BoundingBox aabb;
	DirectX::BoundingBox::CreateFromPoints(aabb, verts_pos.size(), &verts_pos[0], sizeof(DirectX::XMFLOAT3));		
	std::shared_ptr<Bounded> bounded_x( new Bounded(aabb) );	
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[0], bounded_x);	
	verts_pos.clear();

	// Y
	mAxesIds[1] = gApp->EntityMngr()->CreateEntity();                  
	std::shared_ptr<AxesRenderable> renderable_obj_y(new AxesRenderable(gApp->Renderer()->Device(), *mSoMngr->CurrentlySelectedSceneObjects().begin(), AxesRenderable::Y_AXIS, &verts_pos));
	renderable_obj_y->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[1], renderable_obj_y);
	DirectX::BoundingBox::CreateFromPoints(aabb, verts_pos.size(), &verts_pos[0], sizeof(DirectX::XMFLOAT3));		
	std::shared_ptr<Bounded> bounded_y( new Bounded(aabb) );	
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[1], bounded_y);	
	verts_pos.clear();

	// Z
	mAxesIds[2] = gApp->EntityMngr()->CreateEntity();                  
	std::shared_ptr<AxesRenderable> renderable_obj_z(new AxesRenderable(gApp->Renderer()->Device(), *mSoMngr->CurrentlySelectedSceneObjects().begin(), AxesRenderable::Z_AXIS, &verts_pos));
	renderable_obj_z->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[2], renderable_obj_z);
	DirectX::BoundingBox::CreateFromPoints(aabb, verts_pos.size(), &verts_pos[0], sizeof(DirectX::XMFLOAT3));		
	std::shared_ptr<Bounded> bounded_z( new Bounded(aabb) );	
	gApp->EntityMngr()->AddComponentToEntity(mAxesIds[2], bounded_z);	

	std::shared_ptr<IComponent>  tmp_comp;
	gApp->EntityMngr()->GetComponentFromEntity(*mSoMngr->CurrentlySelectedSceneObjects().begin(), Spatialized::GetGuid(), tmp_comp);

	if (tmp_comp)
	{					
		gApp->EntityMngr()->AddComponentToEntity(mAxesIds[0], tmp_comp);		
		gApp->EntityMngr()->AddComponentToEntity(mAxesIds[1], tmp_comp);	
		gApp->EntityMngr()->AddComponentToEntity(mAxesIds[2], tmp_comp);	
	}	
}

void 
TransformAxes::UnbindTransformAxes()
{
	if (mAxesIds[0] >= 0)
	{
		gApp->EntityMngr()->DestroyEntity(mAxesIds[0]);
		gApp->EntityMngr()->DestroyEntity(mAxesIds[1]);
		gApp->EntityMngr()->DestroyEntity(mAxesIds[2]);
		mAxesIds[0] = -1;
		mAxesIds[1] = -1;
		mAxesIds[2] = -1;
		mSoId = -1;
	}
}

bool const 
TransformAxes::InteractionActions( int const& xIncr, int const& yIncr )
{
	bool ret = false;
	
	// Get selections' spatial component
	std::set<unsigned int>::iterator iter;
	for (iter = mSoMngr->CurrentlySelectedSceneObjects().begin();
		iter != mSoMngr->CurrentlySelectedSceneObjects().end();
		iter++) 
	{
		std::shared_ptr<IComponent>  tmp_comp;
		gApp->EntityMngr()->GetComponentFromEntity(*iter, Spatialized::GetGuid(), tmp_comp);

		if (tmp_comp)
		{	
			Spatialized* spatialized = static_cast<Spatialized*>(tmp_comp.get());	

			// TRANSLATE
			static bool orig_from_x_axis = false;
			static bool orig_from_y_axis = false;
			static bool orig_from_z_axis = false;

			if ( MouseInputDevice::CLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::LEFT) )
			{
				float dist = 100.f;
				if ( MousePick(mAxesIds[0], dist) )
					orig_from_x_axis = ret = true;
				if ( MousePick(mAxesIds[1], dist) )
					orig_from_y_axis = ret = true;
				if ( MousePick(mAxesIds[2], dist) )
					orig_from_z_axis = ret = true; 
			}
			else if ( MouseInputDevice::UNCLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::LEFT) )
			{
				orig_from_x_axis = false;
				orig_from_y_axis = false;
				orig_from_z_axis = false;
			}
			else if ( MouseInputDevice::PRESSED == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::LEFT) )
			{
				float shift_increment = 0.00033f; // by pixel	
				if (gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_SHIFT))
					shift_increment = 0.05f;				

				DirectX::XMFLOAT3 trsnlate(0.f, 0.f, 0.f);
				if (orig_from_x_axis)
					trsnlate.x = (xIncr+yIncr) * shift_increment;
				if (orig_from_y_axis)
					trsnlate.y = (xIncr+yIncr) * shift_increment;
				if (orig_from_z_axis)
					trsnlate.z = (xIncr+yIncr) * shift_increment;

				DirectX::XMMATRIX rot_mat = DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&spatialized->LocalRotation()));
				DirectX::XMVECTOR trs_vec = XMLoadFloat3(&trsnlate);
				DirectX::XMVECTOR rotated_trs = DirectX::XMVector4Transform(trs_vec, rot_mat);

				DirectX::XMFLOAT3 pos( spatialized->LocalPosition().x + DirectX::XMVectorGetX(rotated_trs), spatialized->LocalPosition().y +  DirectX::XMVectorGetY(rotated_trs), spatialized->LocalPosition().z +  DirectX::XMVectorGetZ(rotated_trs));
				spatialized->LocalPosition(pos);

				if (orig_from_x_axis || orig_from_y_axis || orig_from_z_axis)
					ret = true;
			}		

			// Rotate
			if ( MouseInputDevice::CLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
			{
				float dist = 100.f;
				if ( MousePick(mAxesIds[0], dist) )
					orig_from_x_axis = ret = true;
				if ( MousePick(mAxesIds[1], dist) )
					orig_from_y_axis = ret = true;
				if ( MousePick(mAxesIds[2], dist) )
					orig_from_z_axis = ret = true; 			
			}
			else if ( MouseInputDevice::UNCLICK == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
			{
				orig_from_x_axis = false;
				orig_from_y_axis = false;
				orig_from_z_axis = false;
			}
			else if ( MouseInputDevice::PRESSED == gApp->InputDeviceMngr()->Mouse()->ButtonState(MouseInputDevice::MIDDLE) )
			{
				float rot_increment = 0.005f; // by pixel	
				if (gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_SHIFT))
					rot_increment = 0.05f;			

				DirectX::XMFLOAT3 rot_incr(0.f, 0.f, 0.f);
				if (orig_from_x_axis)
					rot_incr.x = (xIncr+yIncr) * rot_increment;
				if (orig_from_y_axis)
					rot_incr.y = (xIncr+yIncr) * rot_increment;
				if (orig_from_z_axis)
					rot_incr.z = (xIncr+yIncr) * rot_increment;

				DirectX::XMVECTOR quat = XMLoadFloat4(&spatialized->LocalRotation());
				quat = DirectX::XMQuaternionMultiply(DirectX::XMQuaternionRotationRollPitchYaw(rot_incr.x, rot_incr.y, rot_incr.z), quat);				

				DirectX::XMFLOAT4 new_rot; DirectX::XMStoreFloat4(&new_rot, quat);
				spatialized->LocalRotation(new_rot);

				if (orig_from_x_axis || orig_from_y_axis || orig_from_z_axis)
					ret = true;
			}		
		}
	}
						
	
	return ret;
}

void 
TransformAxes::OnEvent(std::string const& event, Metadata* const metadata)
{
	if ( SO_SELECTION_CHANGE == event )
	{
		if (mSoMngr->CurrentlySelectedSceneObjects().empty())
			UnbindTransformAxes();
		else
		{
			// If first in selection has changed, then rebound to oldest in selection list
			if (mStateMngr->IsStateOn(SPATIAL_TRANSFORM))
			{			
				int oldest_so = *mSoMngr->CurrentlySelectedSceneObjects().begin();
				if (mSoId != oldest_so)
				{
					UnbindTransformAxes();
					mSoId = oldest_so;
					BindTransformAxesToSO(mSoId);
				}
			}
		}		
	}
	else if (SO_GEOMETRY_CHANGE == event)
	{
		int id = mSoId;
		UnbindTransformAxes();
		BindTransformAxesToSO(id);
		

	}	
}