#include <sstream>
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "SceneEditor.h"
#include "EventManager/EventManager.h"
#include "Utils/CommonWinDialogs.h"
#include "FurMenu.h"

std::shared_ptr<SceneObjectManager> FurMenu::mSoManager;

float				FurMenu::mFurLength = 1.f;
unsigned int		FurMenu::mTexturesSize = 64;
unsigned int		FurMenu::mNumLayers = 15;
unsigned int		FurMenu::mStartDensity = 30000;
unsigned int		FurMenu::mEndDensity = 15000;
float				FurMenu::mStartAlpha = 0.18f;
float				FurMenu::mEndAlpha = 0.01f;
float				FurMenu::mFurForce[3] = { 0.f, 0.f, 0.f };

TwBar* FurMenu::mFurMenuTwBar = NULL;


FurMenu::FurMenu(std::shared_ptr<SceneObjectManager> soManager)
{
	mSoManager = soManager;	

	mFurMenuTwBar = TwNewBar("Fur");

	TwAddVarRW(mFurMenuTwBar, "TextureSize", TW_TYPE_INT32, &mTexturesSize, "label='Texture Size' min=64");
	TwAddVarRW(mFurMenuTwBar, "NumLayers", TW_TYPE_INT32, &mNumLayers, "label='Num Layers' min=0");
	TwAddVarRW(mFurMenuTwBar, "StartDensity", TW_TYPE_INT32, &mStartDensity, "label='Start Density' min=0");
	TwAddVarRW(mFurMenuTwBar, "EndDensity", TW_TYPE_INT32, &mEndDensity, "label='End Density' min=0");
	TwAddVarRW(mFurMenuTwBar, "StartAlpha", TW_TYPE_FLOAT, &mStartAlpha, "label='Start Alpha' min=0.001 step=0.005 precision=3");
	TwAddVarRW(mFurMenuTwBar, "EndAlpha", TW_TYPE_FLOAT, &mEndAlpha, "label='End Alpha' min=0.001 step=0.005 precision=3");
	TwAddButton(mFurMenuTwBar, "Init", InitFurLayers, NULL, " label='Init Layers' ");
	
	TwAddButton(mFurMenuTwBar, "space1", NULL, NULL, " label=' ' ");
	TwAddButton(mFurMenuTwBar, "LIVE SETTINGS", NULL, NULL, " label='LIVE SETTINGS' ");
	TwAddVarRW(mFurMenuTwBar, "FurLength", TW_TYPE_FLOAT, &mFurLength, "label='Fur Length' min=0.001 step=0.025 precision=3");
	TwAddVarRW(mFurMenuTwBar, "FurForce", TW_TYPE_DIR3F, &mFurForce, "label = 'Fur Force'");
	
	//TwDefine(" Fur iconified=true ");
	TwDefine(" 'Fur' size='250 230' position='1628 630' valueswidth=100 alpha=50 ");
}


FurMenu::~FurMenu(void)
{

}



void 
FurMenu::Reset()
{
	auto ids = mSoManager->CurrentlySelectedSceneObjects();
	if (!ids.empty())
	{
		auto id = *ids.begin();
		auto so = mSoManager->SceneObjectRef(id);

		if (so->HasGeometry())
		{
			auto fur_comp = so->SubEntityRefByIndex(0)->furComp;
			if (fur_comp)
			{
				mFurLength = fur_comp->FurLength();
				mNumLayers = fur_comp->NumLayers();
				mFurForce[0] = fur_comp->Force().x;
				mFurForce[1] = fur_comp->Force().y;
				mFurForce[2] = fur_comp->Force().z;
			}
		}
	}
}

void 
FurMenu::Advance()
{
	auto ids = mSoManager->CurrentlySelectedSceneObjects();
	if (!ids.empty())
	{
		auto id = *ids.begin();
		auto so = mSoManager->SceneObjectRef(id);
		
		if (so->HasGeometry())
		{
			if (so->SubEntityRefByIndex(0)->furComp)
			{
				so->SubEntityRefByIndex(0)->furComp->FurLength() = mFurLength;
				so->SubEntityRefByIndex(0)->furComp->Force() = DirectX::XMFLOAT3(mFurForce[0], mFurForce[1], mFurForce[2]);
			}
		}
	}
}

void 
FurMenu::OnEvent(std::string const& eventName)
{

}

void TW_CALL 
FurMenu::InitFurLayers(void* /*clientData*/)
{
	auto ids = mSoManager->CurrentlySelectedSceneObjects();
	if (!ids.empty())
	{
		auto id = *ids.begin();
		auto so = mSoManager->SceneObjectRef(id);

		if (so->HasGeometry())
		{
			if (gApp)
			{

				if (!so->SubEntityRefByIndex(0)->furComp)
				{
					so->SubEntityRefByIndex(0)->furComp = std::make_shared<FurMapped>();
					gApp->EntityMngr()->AddComponentToEntity(so->SubEntityRefByIndex(0)->entityId, so->SubEntityRefByIndex(0)->furComp);
				}

				if (mNumLayers == 0) // then remove the component
				{
					so->SubEntityRefByIndex(0)->furComp = nullptr;
					gApp->EntityMngr()->RemoveComponentFromEntity(so->SubEntityRefByIndex(0)->entityId, FurMapped::GetGuid());
				}
				else
					so->SubEntityRefByIndex(0)->furComp->InitFurLayers(
						gApp->Renderer(),
						mTexturesSize,
						mNumLayers,
						mStartDensity,
						mEndDensity,
						mStartAlpha,
						mEndAlpha);
			}
		}
	}
}




