#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Scene/SceneObjectManager.h"
#include "MenuManager/Menu.h"

class EventManager;

class FurMenu :
	public Menu
{
public:
	FurMenu(std::shared_ptr<SceneObjectManager> soManager);
	~FurMenu(void);

	virtual void
	Reset();

	virtual void
	Advance();

private:
	virtual void
	OnEvent(std::string const& eventName);

	static void TW_CALL
	InitFurLayers(void* /*clientData*/);

	static std::shared_ptr<SceneObjectManager> mSoManager;

	static float			mFurLength;
	static unsigned int		mTexturesSize;
	static unsigned int		mNumLayers;
	static unsigned int		mStartDensity;
	static unsigned int		mEndDensity;
	static float			mStartAlpha;
	static float			mEndAlpha;
	static float			mFurForce[3];


	static TwBar*            mFurMenuTwBar;
};

