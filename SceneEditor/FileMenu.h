#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "MenuManager/Menu.h"

class SceneObjectManager;
class EventManager;

class FileMenu : public Menu
{
public:
	FileMenu(std::shared_ptr<SceneObjectManager> soMngr, 
			 std::shared_ptr<EventManager> eventManager,
			 std::shared_ptr<ForwardRenderer> frwrdRenderer);
	~FileMenu(void);

	
	virtual void
	Reset();
		
private:
	static std::shared_ptr<SceneObjectManager> mSoMngr;
	static std::shared_ptr<EventManager> mEventManager;
	static std::shared_ptr<ForwardRenderer> mFrwrdRenderer; // needed to export its settings to scene file
		
	static TwBar*   mFileMenuTwBar;

	static void TW_CALL
	LoadScene(void* /*clientData*/);

	static void TW_CALL
	SaveScene(void* /*clientData*/);
		
	static void TW_CALL
	ImportObj(void* /*clientData*/);
	static void TW_CALL
	ExportSo(void* /*clientData*/);
	static void TW_CALL 
	ImportSo(void* /*clientData*/);	
};

