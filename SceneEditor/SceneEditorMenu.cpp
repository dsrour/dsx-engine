#include "SceneEditor.h"
#include "Utils/CommonWinDialogs.h"
#include "Scene/SceneObjectManager.h"
#include "SceneEditorMenu.h"

extern BaseApp* gApp;


std::shared_ptr<SceneObjectManager> SceneEditorMenu::mSoMngr;
float  SceneEditorMenu::mNavSpeedFactor			= 10.f;
bool   SceneEditorMenu::mShowBounds				= true;
bool   SceneEditorMenu::mShowFloorGrid			= true;
TwBar* SceneEditorMenu::mSceneEditorMenuTwBar	= NULL;

SceneEditorMenu::SceneEditorMenu(std::shared_ptr<SceneObjectManager> soMngr)
{
	mSoMngr = soMngr; 

	mSceneEditorMenuTwBar = TwNewBar("Settings");
	TwAddVarCB(mSceneEditorMenuTwBar, "NavSpeedFactor", TW_TYPE_FLOAT, SetNavFactorCallback, GetNavFactorCallback, NULL, " min=1 step=0.1 label='Nav Speed Factor' ");   	   
	TwAddVarCB(mSceneEditorMenuTwBar, "ShowBounds", TW_TYPE_BOOLCPP, SetShowBoundsCallback, GetShowBoundsCallback, NULL, " label='Show Bounds' ");   
	TwAddVarCB(mSceneEditorMenuTwBar, "ShowFloor", TW_TYPE_BOOLCPP, SetShowFloorCallback, GetShowFloorCallback, NULL, " label='Show Floor Grid' ");   

	//TwDefine(" Settings iconified=true ");
	TwDefine(" 'Settings' size='250 100' position='1628 870' alpha=50 ");
}


SceneEditorMenu::~SceneEditorMenu(void)
{
}

void TW_CALL 
SceneEditorMenu::SetNavFactorCallback( const void* value, void* /*clientData*/ )
{
	mNavSpeedFactor = *(const float*)value; 
	static_cast<SceneEditor*>(gApp)->NavSpeedFactor(mNavSpeedFactor);
}

void TW_CALL 
SceneEditorMenu::GetNavFactorCallback( void* value, void* /*clientData*/ )
{
	*(float*)value = mNavSpeedFactor;
}

void TW_CALL 
SceneEditorMenu::SetShowBoundsCallback( const void* value, void* /*clientData*/ )
{
	mShowBounds = *(const bool*)value;

	std::set<unsigned int> selected = mSoMngr->SceneObjectsIds();	
	for (std::set<unsigned int>::iterator iter = selected.begin(); 
		iter != selected.end();
		++iter)
	{					
		mSoMngr->SceneObjectRef(*iter)->ToggleBoundBox(mShowBounds);
	}	
}

void TW_CALL
SceneEditorMenu::GetShowBoundsCallback( void* value, void* /*clientData*/ )
{
	 *(bool*)value = mShowBounds;
}

void TW_CALL 
SceneEditorMenu::SetShowFloorCallback( const void* value, void* /*clientData*/ )
{
	mShowFloorGrid = *(const bool*)value;
	static_cast<SceneEditor*>(gApp)->ToggleFloorGridRenderableEntity(mShowFloorGrid);
}

void TW_CALL 
SceneEditorMenu::GetShowFloorCallback( void* value, void* /*clientData*/ )
{
	 *(bool*)value = mShowFloorGrid;
}

void 
SceneEditorMenu::Reset()
{
	mNavSpeedFactor = static_cast<SceneEditor*>(gApp)->NavSpeedFactor();
}

