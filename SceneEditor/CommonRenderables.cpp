#include "SceneEditor.h"
#include "Components/Bounded.h"
#include "CommonRenderables.h"

FloorGridRenderable::FloorGridRenderable(ID3D11Device* d3dDevice)
{
	unsigned int const LENGTH = 21;

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> indices_to_gpu;
	for (unsigned int i = 0; i < LENGTH * 2 * 2 + 8; i++)
		indices_to_gpu.push_back(i);

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  

	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
		}
	};

	std::vector<Vertex> vertices;
	Vertex vert;

	float pos = -(LENGTH / 2.f);

	// CROSS SECTION
	vert.pos = DirectX::XMFLOAT3( -(LENGTH/2.f + (LENGTH/4.f)), 0.f, 0.f ); 
	vertices.push_back(vert);  

	vert.pos = DirectX::XMFLOAT3( (LENGTH/2.f + (LENGTH/4.f)), 0.f, 0.f ); 
	vertices.push_back(vert);      

	vert.pos = DirectX::XMFLOAT3( 0.f, 0.f, -(LENGTH/2.f + (LENGTH/4.f)) ); 
	vertices.push_back(vert);  

	vert.pos = DirectX::XMFLOAT3( 0.f, 0.f, (LENGTH/2.f + (LENGTH/4.f)) ); 
	vertices.push_back(vert);    
	///////////


	for (unsigned i = 0; i < LENGTH + 1; i++)
	{
		vert.pos = DirectX::XMFLOAT3( (float)(pos), 0.f, -(float)(LENGTH/2.f) ); 
		vertices.push_back(vert);  

		vert.pos = DirectX::XMFLOAT3( (float)(pos), 0.f, (float)(LENGTH/2.f) ); 
		vertices.push_back(vert);      

		vert.pos = DirectX::XMFLOAT3( -(float)(LENGTH/2.f), 0.f, (float)(pos) ); 
		vertices.push_back(vert);  

		vert.pos = DirectX::XMFLOAT3( (float)(LENGTH/2.f), 0.f, (float)(pos) ); 
		vertices.push_back(vert);    

		pos += 1.f;
	}

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
}

AxesRenderable::AxesRenderable( ID3D11Device* d3dDevice, unsigned int entId, AxisName const& axis, std::vector<DirectX::XMFLOAT3>* vertsOut )
{
	std::shared_ptr<IComponent>  tmp_comp;
	gApp->EntityMngr()->GetComponentFromEntity(entId, Bounded::GetGuid(), tmp_comp);

	if (tmp_comp)
	{			
		Bounded* bounded = static_cast<Bounded*>(tmp_comp.get());	


		// OFFSETS
		float x_off = bounded->AabbBounds().Extents.x + 0.25f;
		float y_off = bounded->AabbBounds().Extents.y + 0.25f;
		float z_off = bounded->AabbBounds().Extents.z + 0.25f;


		D3D11_BUFFER_DESC bd;
		D3D11_SUBRESOURCE_DATA InitData;

		std::vector<unsigned int> indices_to_gpu;
		for (unsigned int i = 0; i < 24; i++)
			indices_to_gpu.push_back(i);

		ZeroMemory( &bd, sizeof(bd) );
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;        
		ZeroMemory( &InitData, sizeof(InitData) );
		InitData.pSysMem = &indices_to_gpu[0];
		d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

		mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  

		struct Vertex
		{
			DirectX::XMFLOAT3 pos;
			DirectX::XMFLOAT3 norm;

			Vertex()
			{
				pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
				norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
			}
		};

		std::vector<Vertex> vertices;
		Vertex vert;

		float x_dim = bounded->AabbBounds().Extents.x;
		if (x_dim < 0.5f) x_dim = 0.5f;
		float y_dim = bounded->AabbBounds().Extents.y;
		if (y_dim < 0.5f) y_dim = 0.5f;
		float z_dim = bounded->AabbBounds().Extents.z;
		if (z_dim < 0.5f) z_dim = 0.5f;
		float lngth_x = x_dim / 1.5f;
		float lngth_y = y_dim / 1.5f;
		float lngth_z = z_dim / 1.5f;

		if (axis == X_AXIS)
		{						
			// Front face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f ); 					
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x,  
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Back face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f /8.f);
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x,  
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Right connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f,  
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off+lngth_x, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f,  
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Left connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f,  
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_off, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f,  
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
		}
		else if (axis == Y_AXIS)
		{			
			// Front face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Back face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f /8.f);
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Right connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/0.5f/8.f,  
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off+lngth_y, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Left connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/-0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f,  
											bounded->AabbBounds().Center.y + y_off, 
											bounded->AabbBounds().Center.z + z_dim/0.5f/8.f );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
		}

		else if (axis == Z_AXIS)
		{			
			// Front face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f,
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off);
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Back face
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f,
											bounded->AabbBounds().Center.z + z_off ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);      
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off ); 
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);    
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f /8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z);
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f,
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Right connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f,  
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(   bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off+lngth_z );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  

			// Left connections
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/0.5f/8.f,  
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.y + y_dim/-0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
			vert.pos = DirectX::XMFLOAT3(	bounded->AabbBounds().Center.x + x_dim/-0.5f/8.f,  
											bounded->AabbBounds().Center.y + y_dim/0.5f/8.f, 
											bounded->AabbBounds().Center.z + z_off );
			vertices.push_back(vert); if (vertsOut) vertsOut->push_back(vert.pos);  
		}

		
		








		ZeroMemory( &bd, sizeof(bd) );
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;    
		ZeroMemory( &InitData, sizeof(InitData) );
		InitData.pSysMem = &vertices[0];

		VertexBufferDesc vbuff_desc;
		d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

		vbuff_desc.stride = sizeof(Vertex);
		vbuff_desc.offset = 0;            
		vbuff_desc.vertexCount = (unsigned int)vertices.size();

		mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

		mRenderableType = FLAT;

		mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;

	}
}

