#pragma once
#pragma once

#pragma warning(disable:4244)

#include <Windows.h>
#include <set>
#include <liblas/header.hpp>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "PcSceneManager.h"

class PcSystemMenu
{
public:
	PcSystemMenu(PcSceneManager* const pcSceneMngr);
	~PcSystemMenu(void);

private:	
	static void TW_CALL
	AppendFrame(void* /*clientData*/);	

	static void TW_CALL
	ClearFrames(void* /*clientData*/);	

	static void TW_CALL
	ToggleBucketDbgBox(void* /*clientData*/);	

	static void TW_CALL
	ToggleBvhDbgBox(void* /*clientData*/);	

	static void TW_CALL
	ToggleSubPointCloudsAabbs(void* /*clientData*/);

	static void TW_CALL 
	SetTargetPntsCallback(const void* value, void*);

	static void TW_CALL 
	GetTargetPntsCallback(void* value, void*);

	static void TW_CALL 
	SetDistWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetDistWeightCallback(void* value, void*);

	static void TW_CALL 
	SetAreaWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetAreaWeightCallback(void* value, void*);

	static void TW_CALL 
	SetPntsWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetPntsWeightCallback(void* value, void*);

	static void TW_CALL 
	Init( void* /*clientData*/ );

	static void TW_CALL 
	SetNumBucketsCallback(const void* value, void*);
	
	static void TW_CALL 
	GetNumBucketsCallback(void* value, void*);
	
	static TwBar* mPcSysTwBar;
	static PcSceneManager* mPcSceneManager;

	static unsigned int mNumBuckets;

	// Lod stuff
	static unsigned int mTargetNumPnts;
	static float mDistWeight, mAreaWeight, mPointsWeight;
};