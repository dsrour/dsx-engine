#pragma once

#include <windows.h>
#include <DirectXMath.h>

#include "InputDeviceManager.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Components/Renderable.h"

#include "PcSceneManager.h"

class ApplicationKeyboardActions : public InputDeviceActions
{
public:	
	ApplicationKeyboardActions(std::shared_ptr<InputDeviceManager> inputDeviceManager, PcSceneManager* const pcSceneMngr);

    virtual void
    OnStateChange(void);
	
private:	
	void
	DoCameraControlActions(std::shared_ptr<InputDeviceManager>& inputDeviceMngr);

    std::shared_ptr<Camera> mCamera; 

	PcSceneManager* mPcSceneMngr;
};


