#pragma once

#include <map>
#include "Bvh.h"
#include "PcEntity.h"

bool
CompareSecondPairElementBvhData(std::pair<Bvh::BvhData*, float> first, std::pair<Bvh::BvhData*, float> second);

bool 
CompareSecondPairElementPcEntity( std::pair<PcEntity*, float> first, std::pair<PcEntity*, float> second );

bool 
CompareSecondPairElementUint( std::pair<unsigned int, float> first, std::pair<unsigned int, float> second );
