#pragma once

#include "Components/Renderable.h"
#include "BaseApp.h"

class PointCloudRenderable : public Renderable
{
public:  
	struct Point
	{
		DirectX::XMFLOAT3 xyz;
		DirectX::XMFLOAT3 color;
	};

	PointCloudRenderable(void);
	
	bool const
	IsLoaded(void) const {return mLoaded;}

	void
	Load(std::vector<PointCloudRenderable::Point> const& points);

	void
	SetNumberOfVisiblePoints(unsigned int const num, unsigned int const offset = 0)
	{
		std::map<InputLayoutManager::SubInputLayout, VertexBufferDesc>::iterator iter;
		for (iter = mVertexBuffers.begin();
			 iter != mVertexBuffers.end();
			 ++iter )
		{
			 iter->second.vertexCount = num;
			 iter->second.offset = sizeof(Point)*offset;
		}
	}

private:
	bool mLoaded;
};