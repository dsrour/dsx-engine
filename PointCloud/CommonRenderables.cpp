#include "BaseApp.h"
#include "PointCloudRenderable.h"
#include "CommonRenderables.h"

extern BaseApp* gApp;

BucketDebugRenderable::BucketDebugRenderable(float const& lngth, std::vector<DirectX::XMFLOAT3> centers, DirectX::XMFLOAT3 const& color)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	struct Vec
	{
		DirectX::XMFLOAT3 xyz;
		DirectX::XMFLOAT3 norm;

		Vec() { norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f); }
	};
	std::vector<Vec> vertices;

	float half_lngth = lngth / 2.f;

	for (unsigned int i = 0; i < centers.size(); i++)
	{
		Vec vert;

		DirectX::XMFLOAT3 center = centers[i];

		// Front face
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);

		// Back face
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);

		// Right connections
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);

		// Left connections
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z - half_lngth);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z + half_lngth);
		vertices.push_back(vert);
	}


	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vec)* vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	gApp->Renderer()->Device()->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));

	vbuff_desc.stride = sizeof(Vec);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;

	mMaterial.diffuse = DirectX::XMFLOAT4(color.x, color.y, color.z, 1.f);
}

GenericAabbDebugRenderable::GenericAabbDebugRenderable(std::vector<DirectX::BoundingBox> aabbs, DirectX::XMFLOAT3 const& color)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;


	struct Vec
	{
		DirectX::XMFLOAT3 xyz;
		DirectX::XMFLOAT3 norm;

		Vec() { norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f); }
	};
	std::vector<Vec> vertices;



	for (unsigned int i = 0; i < aabbs.size(); i++)
	{
		Vec vert;

		DirectX::XMFLOAT3 center = aabbs[i].Center;
		float half_lngth_x = aabbs[i].Extents.x;
		float half_lngth_y = aabbs[i].Extents.y;
		float half_lngth_z = aabbs[i].Extents.z;

		// Front face
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);

		// Back face
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);

		// Right connections
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x + half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);

		// Left connections
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y + half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z - half_lngth_z);
		vertices.push_back(vert);
		vert.xyz = DirectX::XMFLOAT3(center.x - half_lngth_x, center.y - half_lngth_y, center.z + half_lngth_z);
		vertices.push_back(vert);
	}


	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vec)* vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	gApp->Renderer()->Device()->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));

	vbuff_desc.stride = sizeof(Vec);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;

	mMaterial.diffuse = DirectX::XMFLOAT4(color.x, color.y, color.z, 1.f);
}

FilledBucketRenderable::FilledBucketRenderable(float const& lngth, std::vector<DirectX::XMFLOAT3> centers, DirectX::XMFLOAT3 const& color)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	struct Vec
	{
		DirectX::XMFLOAT3 xyz;
		DirectX::XMFLOAT3 norm;

		Vec() { norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f); }
	};
	std::vector<Vec> vertices;

	float half_lngth = lngth / 2.f;	

	for (unsigned int i = 0; i < centers.size(); i++)
	{
		Vec vert;

		DirectX::XMFLOAT3 center = centers[i];

		auto p1 = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z - half_lngth);
		auto p2 = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z - half_lngth);
		auto p3 = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z - half_lngth);
		auto p4 = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z - half_lngth);
		auto p5 = DirectX::XMFLOAT3(center.x - half_lngth, center.y + half_lngth, center.z + half_lngth);
		auto p6 = DirectX::XMFLOAT3(center.x + half_lngth, center.y + half_lngth, center.z + half_lngth);
		auto p7 = DirectX::XMFLOAT3(center.x - half_lngth, center.y - half_lngth, center.z + half_lngth);
		auto p8 = DirectX::XMFLOAT3(center.x + half_lngth, center.y - half_lngth, center.z + half_lngth);

		// Front Face (1-2-3-4)		
		vert.xyz = p1; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, -1.f); vertices.push_back(vert);
		vert.xyz = p2; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, -1.f); vertices.push_back(vert);
		vert.xyz = p3; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, -1.f); vertices.push_back(vert);
		vert.xyz = p4; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, -1.f); vertices.push_back(vert);
		// Right Face (2-6-4-8)		
		vert.xyz = p2; vert.norm = DirectX::XMFLOAT3(1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p6; vert.norm = DirectX::XMFLOAT3(1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p4; vert.norm = DirectX::XMFLOAT3(1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p8; vert.norm = DirectX::XMFLOAT3(1.f, 0.f, 0.f); vertices.push_back(vert);
		// Top Face (5-6-1-2)
		vert.xyz = p5; vert.norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p6; vert.norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p1; vert.norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p2; vert.norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f); vertices.push_back(vert);
		// Back Face (6-5-8-7)
		vert.xyz = p6; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, 1.f); vertices.push_back(vert);
		vert.xyz = p5; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, 1.f); vertices.push_back(vert);
		vert.xyz = p8; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, 1.f); vertices.push_back(vert);
		vert.xyz = p7; vert.norm = DirectX::XMFLOAT3(0.f, 0.f, 1.f); vertices.push_back(vert);
		// Left Face (5-1-7-3)
		vert.xyz = p5; vert.norm = DirectX::XMFLOAT3(-1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p1; vert.norm = DirectX::XMFLOAT3(-1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p7; vert.norm = DirectX::XMFLOAT3(-1.f, 0.f, 0.f); vertices.push_back(vert);
		vert.xyz = p3; vert.norm = DirectX::XMFLOAT3(-1.f, 0.f, 0.f); vertices.push_back(vert);
		// Bottom Face (3-4-7-8)
		vert.xyz = p3; vert.norm = DirectX::XMFLOAT3(0.f, -1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p4; vert.norm = DirectX::XMFLOAT3(0.f, -1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p7; vert.norm = DirectX::XMFLOAT3(0.f, -1.f, 0.f); vertices.push_back(vert);
		vert.xyz = p8; vert.norm = DirectX::XMFLOAT3(0.f, -1.f, 0.f); vertices.push_back(vert);
	}


	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vec)* vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	gApp->Renderer()->Device()->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));

	vbuff_desc.stride = sizeof(Vec);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	mMaterial.diffuse = DirectX::XMFLOAT4(color.x, color.y, color.z, 1.f);
}
