#include <assert.h>
#include <fstream>
#include <sstream>
#include "Debug/Debug.h"
#include "AnimationDataLoader.h"


AnimationDataLoader::AnimationDataLoader(void)
{
}


AnimationDataLoader::~AnimationDataLoader(void)
{
}

std::map< unsigned int, std::map<unsigned int, unsigned int> > 
AnimationDataLoader::LoadAnimationData( std::string const& file )
{
	std::map< unsigned int, std::map<unsigned int, unsigned int> > ret;

	std::ifstream infile(file);
	std::string line;

	int cur_frame = -1;

	while (std::getline(infile, line))
	{
		if ('f' == *line.begin())		
		{
			std::string frame_num_str( (++line.begin()), line.end() );
			std::istringstream iss(frame_num_str);			
			iss >> cur_frame;
			continue;
		}
		else if (cur_frame >= 0)
		{
			std::istringstream iss(line);
			unsigned int from, to;
			assert(iss >> from >> to);
			ret[cur_frame][from] = to;
			//OutputDebugMsg(to_string(cur_frame) + ": " + to_string(from) + " -> " + to_string(to) + "\n");
		}
	}

	return ret;
}
