#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "Settings.h"
#include "Components/Bounded.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"
#include "PointCloudPipeline.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "SceneEditor.h"

BaseApp* gApp = new SceneEditor;

SceneEditor::SceneEditor(void)
{    
	// Has to be divisible by 32 for post processing.     
	WinWidth(1888);
	WinHeight(992);       

	// State mngr since some other objects depend on it 	
	mStateManager = new StateManager();
	mStateManager->ToggleState(CAMERA_CONTROL, true);
	
	mEventManager = new EventManager();

	mNavSpeedFactor = 1.f;

	mSceneEditorMenu = NULL;
	mTemporalAnalysisMenu = NULL;
	mFileEditMenu = NULL;
	mAnimationMenu = NULL;
	mPcSysMenu = NULL;
}


SceneEditor::~SceneEditor(void)
{
	if (mSceneEditorMenu)
		delete mSceneEditorMenu;

	if (mTemporalAnalysisMenu)
		delete mTemporalAnalysisMenu;

	if (mFileEditMenu)
		delete mFileEditMenu;

	if (mAnimationMenu)
		delete mAnimationMenu;

	if (mPcSysMenu)
		delete mPcSysMenu;

	// State manager last because others depend on it
	if (mStateManager)
		delete mStateManager;

	if (mEventManager)
		delete mEventManager;
}

bool const 
SceneEditor::OnLoadApp( void )
{
    std::string help_str = std::string("GLOBAL help=' \n") + 
                           "(+) \t New Entity \n" + 
                           "(-) \t Remove Selected Entities \n" +
                           "\n" +
						   "(HOME) \t Toggle FPS \n" +					   					   				  
						   "\n" +
                           "(F9) \t Load Scene \n" + 
                           "(F6) \t Save Scene \n" + 
						   "\n" +
						   "(F11) \t Recompile Shaders \n" + 
						   "\n" +
						   "(E) \t Toggle FPS Camera \n" + 
						   "(C) \t Clear Selection \n" + 
						   "\n" +
                           + "(SHIFT)+Action \t Speed up Action \n'";	                           
    TwDefine(help_str.c_str());


	// PC Scene Manager... make it update at a freq of 60 updates/sec.... same as rendering
	mPcSceneMngr = std::make_shared<PcSceneManager>(mEventManager, TARGET_NUM_PNTS_PER_FRAME);
	unsigned int pc_scene_mngr_id = gApp->SystemMngr()->AddSystem(mPcSceneMngr);     
	double target = 1.0 / 60.0;
	gApp->SystemSchdlr()->StopSystem(pc_scene_mngr_id);
	gApp->SystemSchdlr()->RunSystemEveryDelta(pc_scene_mngr_id, target);

	// Set actions for keyboard + mouse
	mKeyboardActions = std::make_shared<ApplicationKeyboardActions>(InputDeviceMngr(), mPcSceneMngr.get());
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardActions);
	mMouseActions = std::make_shared<ApplicationMouseActions>(InputDeviceMngr());
	InputDeviceMngr()->Mouse()->AddMouseActions(mMouseActions);

	// Menu inits
	mPcSysMenu = new PcSystemMenu(mPcSceneMngr.get());
	mTemporalAnalysisMenu = new TemporalAnalysisMenu(mPcSceneMngr.get());
	mSceneEditorMenu = new SceneEditorMenu;
	mFileEditMenu = new PointCloudFileEditMenu;
	mAnimationMenu = new PointCloudAnimationMenu(mPcSceneMngr.get(), mEventManager);


	
	std::shared_ptr<PointCloudPipeline> pc_pipeline(new PointCloudPipeline(gApp->Renderer()));
	mPostProcPipeline = std::make_shared<PostProcessPipeline>(gApp->Renderer(), mEventManager);
	std::list< std::shared_ptr<Pipeline> > path; 

	std::shared_ptr<ForwardRenderer> fr_pipeline(new ForwardRenderer(gApp->Renderer()));
	 
	path.push_back(fr_pipeline);
	path.push_back(pc_pipeline); 
	path.push_back(mPostProcPipeline); 
	gApp->Renderer()->RenderPath(path);

	gApp->Renderer()->ClearColor(0.f, 0.f, 0.f, 1.f);

	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Position(DirectX::XMFLOAT4(0.f, 0.f, -15.f, 1.f));	
	Renderer()->CurrentCamera()->ProjMatrix(DirectX::XM_PIDIV4, (float)WinWidth(), (float)WinHeight(), 0.1f, 1000000.0f);    

    return true;
}

void 
SceneEditor::OnPreUpdate( void )
{
	mFileEditMenu->Update();
	mAnimationMenu->Update();
}

void 
SceneEditor::OnPostUpdate( void )
{		
	
}

void 
SceneEditor::OnUnloadApp( void )
{

}