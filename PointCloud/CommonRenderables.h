#pragma once

#include <vector>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>
#include "Components/Renderable.h"

class BucketDebugRenderable : public Renderable
{
public:
	BucketDebugRenderable(float const& lngth, std::vector<DirectX::XMFLOAT3> centers, DirectX::XMFLOAT3 const& color);
};

class GenericAabbDebugRenderable : public Renderable
{
public:
	GenericAabbDebugRenderable(std::vector<DirectX::BoundingBox> aabbs, DirectX::XMFLOAT3 const& color);
};


class FilledBucketRenderable : public Renderable
{
public:
	FilledBucketRenderable(float const& lngth, std::vector<DirectX::XMFLOAT3> centers, DirectX::XMFLOAT3 const& color);
};