#include <windows.h>
#include <set>
#include "StateManager.h"
#include "SceneEditor.h"
#include "StateManager.h"
#include "Component.hpp"
#include "Scene/SceneObjectManager.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Scene/SceneLoaderWriter.h"
#include "Settings.h"
#include "KeyboardActions.h"

ApplicationKeyboardActions::ApplicationKeyboardActions(std::shared_ptr<InputDeviceManager> inputDeviceManager, PcSceneManager* const pcSceneMngr) : InputDeviceActions(inputDeviceManager)       
{        
	mCamera = gApp->Renderer()->CurrentCamera();        
	mPcSceneMngr = pcSceneMngr;
}   

void
ApplicationKeyboardActions::OnStateChange()
{	
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if ( manager->Keyboard()->IsKeyDown(VK_ESCAPE) )
	{
		gApp->EndEngine();
	}

	// Recompile shaders... debug on broski
	if (manager->Keyboard()->IsKeyUp(VK_F11))
	{
		for (std::list< std::shared_ptr<Pipeline> >::const_iterator iter = gApp->Renderer()->RenderPath().begin();
			 iter != gApp->Renderer()->RenderPath().end();
			 ++iter)
			(*iter)->RecompileShaders();
	}

	// Toggle in/out of relative cursor mode
	if (manager->Keyboard()->IsKeyUp('E')) 
	{
		static bool relative = false;
		relative = !relative;

		if (relative)
			while(ShowCursor(false)>=0);
		else
			ShowCursor(true);

		manager->Mouse()->ToggleRelativeMode(relative);		
	}

	// Next frame
	if (manager->Keyboard()->IsKeyUp(VK_ADD)) 
	{
		mPcSceneMngr->CurrentFrame( mPcSceneMngr->CurrentFrame() + 1 );
	}

	// Previous frame
	if (manager->Keyboard()->IsKeyUp(VK_SUBTRACT)) 
	{
		int frame = mPcSceneMngr->CurrentFrame() - 1;
		if (frame < 0)
			mPcSceneMngr->CurrentFrame(0);
		else
			mPcSceneMngr->CurrentFrame(frame);
	}

	// All frames
	if (manager->Keyboard()->IsKeyUp(VK_MULTIPLY)) 
	{
		mPcSceneMngr->CurrentFrame(-1);
	}

	// Toggle play/playse
	if (manager->Keyboard()->IsKeyUp('P')) 
	{		
		mPcSceneMngr->ToggleAnimation();
	}

	// Depending on the current states... use specific keyboard actions	
	if (static_cast<SceneEditor*>(gApp)->StateMngr()->IsStateOn(CAMERA_CONTROL))	
		DoCameraControlActions(manager);		
}

void ApplicationKeyboardActions::DoCameraControlActions(std::shared_ptr<InputDeviceManager>& inputDeviceMngr)
{
	static double last_time = 0.0;
	double new_time = gApp->Timer().ElapsedTimeSecs();
	float delta = (float)new_time - (float)last_time;
	last_time = new_time;

	if (            
		inputDeviceMngr->Keyboard()->IsKeyDown('W') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('S') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('A') ||
		inputDeviceMngr->Keyboard()->IsKeyDown('D') ||              
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_RIGHT) ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_UP)    ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_DOWN)  ||
		inputDeviceMngr->Keyboard()->IsKeyDown(VK_LEFT) )  	     
	{
		last_time = gApp->Timer().ElapsedTimeSecs();
	}
	else
	{       
		float nav_factor = static_cast<SceneEditor*>(gApp)->NavSpeedFactor();
		float mult = 1.f;
		if (inputDeviceMngr->Keyboard()->IsKeyPressed(VK_SHIFT))
			mult = 5.f;
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('W') )
		{        
			static_cast<FpCamera*>(mCamera.get())->Move((8.0f*nav_factor)*mult, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('S') )
		{				
			static_cast<FpCamera*>(mCamera.get())->Move((-8.0f*nav_factor)*mult, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('A') )
		{        
			static_cast<FpCamera*>(mCamera.get())->Strafe((-8.0f*nav_factor)*mult, delta);  
		}
		if ( inputDeviceMngr->Keyboard()->IsKeyPressed('D') )
		{
			static_cast<FpCamera*>(mCamera.get())->Strafe((8.0f*nav_factor)*mult, delta);  
		}

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_RIGHT) )
		{
			static_cast<FpCamera*>(mCamera.get())->Yaw(8.0f/DirectX::XM_PI, delta);      
		}    

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_LEFT) )
		{
			static_cast<FpCamera*>(mCamera.get())->Yaw(-8.0f/DirectX::XM_PI, delta);      
		}   

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_UP) )
		{
			static_cast<FpCamera*>(mCamera.get())->Pitch(8.0f/DirectX::XM_PI, delta);      
		}    

		if ( inputDeviceMngr->Keyboard()->IsKeyPressed(VK_DOWN) )
		{
			static_cast<FpCamera*>(mCamera.get())->Pitch(-8.0f/DirectX::XM_PI, delta);      
		}    

	}
}  