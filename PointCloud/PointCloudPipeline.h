#pragma once

#include "PointCloudEffect.h"
#include "Systems/Renderer/Pipeline.h"

class PointCloudPipeline : public Pipeline
{
public:
	PointCloudPipeline(std::shared_ptr<D3dRenderer> renderer);    

	virtual 
	~PointCloudPipeline(void);

	virtual void
	MadeActive(void) {}

	virtual void
	MadeInactive(void) {}

	virtual void
	RecompileShaders();

	virtual void
	EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);

	
private:
	void 
	DrawPointCloud( Renderable* renderable, std::shared_ptr<D3dRenderer> renderer );

	std::shared_ptr<PointCloudEffect> mPointCloudEffect;  

	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
	D3D11_BLEND_DESC mBlendDesc;
	ID3D11Texture2D* mRtTexture;  // Has its own back buffer texture so it can do post processing on it
	ID3D11RenderTargetView* mRtv; // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	D3D11_SHADER_RESOURCE_VIEW_DESC mRtTextureSrvDesc;
	D3D11_SAMPLER_DESC				mSamplerDesc;
	ID3D11ShaderResourceView* mRtTextureSrv;

};

