#include <assert.h>
#include <fstream>
#include <sstream>
#include "Debug/Debug.h"
#include "AnimationMarkerLoader.h"


AnimationMarkerLoader::AnimationMarkerLoader(void)
{
}


AnimationMarkerLoader::~AnimationMarkerLoader(void)
{
}

std::set<double>
AnimationMarkerLoader::LoadMarkerData( std::string const& file )
{
	std::set<double> ret;

	std::ifstream infile(file);
	std::string line;

	while (std::getline(infile, line))
	{
		std::istringstream iss(line);
		double time_pos;
		assert(iss >> time_pos);
		ret.insert(time_pos);			
	}

	return ret;
}
