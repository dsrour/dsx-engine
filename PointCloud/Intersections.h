#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <set>
#include "StateManager.h"
#include "Scene/SceneObjectManager.h"
#include "BaseApp.h"

extern BaseApp* gApp;
extern StateManager* gStateMngr;
extern SceneObjectManager* gSceneObjectManager;

bool const
IntersectMouseRayEntity(unsigned int entityId, DirectX::XMVECTOR const& rayOrig, DirectX::XMVECTOR const& rayDir, float distance = 100.f);

// Uses current active camera and computed in view space
bool const 
MousePick(  int entityToIntersect, 								
			float& distance);

// Uses current active camera and computed in view space
void 
MousePick(  std::set<unsigned int>& entitiesToIntersect, 
			std::set<unsigned int>& pickedEntitiesOut,
			float& distance);