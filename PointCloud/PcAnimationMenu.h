#pragma once
#pragma once

#pragma warning(disable:4244)

#include <Windows.h>
#include <map>
#include <set>
#include "Components/Renderable.h"
#include "CommonRenderables.h"
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "EventManager/EventManager.h"
#include "PcSceneManager.h"

class PointCloudAnimationMenu
{
public:
	PointCloudAnimationMenu(PcSceneManager* const pcSceneMngr, EventManager* const eventMngr);
	~PointCloudAnimationMenu(void);

	void 
	Update(void);

private:	

	static void TW_CALL
	Clear(void* /*clientData*/);	
	
	static void TW_CALL
	Import(void* /*clientData*/);	

	static void TW_CALL
	Export(void* /*clientData*/);	

	static void TW_CALL
	ToggleAnimBounds(void* /*clientData*/);


	void
	ShowFromBucket(DirectX::XMFLOAT3 center);

	void
	ShowToBucket(DirectX::XMFLOAT3 center);

	void
	HideFromBucket(void);

	void
	HideToBucket(void);

	void
	ShowIntersectedBucketsNotification(DirectX::XMFLOAT3& color);

	static unsigned int  mAnimBoundsEntityId;
	static bool mShowAnimBounds;
	
	std::shared_ptr<FilledBucketRenderable> mFromBucketRenderable, mToBucketRenderable;
	int mFromBucketId, mToBucketId;

	static PcSceneManager* mPcSceneMngr;
	EventManager* mEventMngr;
	static TwBar*					mPointCloudMenuTwBar;
	static std::map< unsigned int,  std::map<unsigned int, unsigned int> > mAnimationData;	// frame -> from -> to
	static std::set<double> mMarkers;
	std::pair<int, int> mTmpPair;
};