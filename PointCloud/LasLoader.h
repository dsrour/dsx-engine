#pragma once

#pragma warning(disable:4244)

#include <string>
#include <vector>
#include <liblas/header.hpp>
#include <DirectXMath.h>
#include "PointCloudRenderable.h"

class LasLoader
{
public:
	static std::vector<PointCloudRenderable::Point>
	LoadPointCloud(std::string const& file, liblas::Header& hdrOut);
};

