#include <algorithm>
#include <assert.h>
#include "Debug/Debug.h"
#include "Timeline.h"

Timeline::Timeline(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
{
	Reset();
}


Timeline::~Timeline(void)
{
}

void 
Timeline::Reset( void )
{
	mPaused = true;
	mCurPos = 0.0;
	mPlaySpeedFactor = 1;
	mMarkers.clear();
	mLastTime = -1.0;
	mHasReachedEnd = false;
	mLastMarker = 0;
}

void Timeline::InsertMarker( double const timePosition )
{
	mMarkers.insert(timePosition);
}

void Timeline::RemoveMarker( double const timePosition )
{
	mMarkers.erase(timePosition);
}

unsigned int 
Timeline::GetNumMarkers( void ) const
{
	return (unsigned int)mMarkers.size();
}

void 
Timeline::SetPosition( double const pos )
{
	mHasReachedEnd = false;
	if (pos < 0.0)
		mCurPos = 0.0;
	else if (pos > *(--mMarkers.end()))
		{
			mCurPos = *(--mMarkers.end());
			mPaused = true;
			mHasReachedEnd = true;
		}
	else
		mCurPos = pos;
}

void 
Timeline::SetPosition( unsigned int const marker )
{
	if ( marker >= mMarkers.size() )
	{
		mCurPos = *(--mMarkers.end());
		mPaused = true;		
	}
	else
	{
		mHasReachedEnd = false;
		unsigned int cur = 0;
		for (auto pos : mMarkers)
		{
			if (cur == marker)
			{
				mCurPos = pos;
				break;
			}
			cur++;
		}
	}
}

void 
Timeline::Play( void )
{
	mPaused = false;
}

void 
Timeline::Pause( void )
{
	mPaused = true;
}

void 
Timeline::SetPlaySpeedFactor( unsigned int speedFactor )
{
	mPlaySpeedFactor = speedFactor;
}

double const 
Timeline::DeltaSinceLastMarker( void )
{
	auto markers =  GetInBetweenMarkers();
	return mCurPos - *(markers.a);
}

double const 
Timeline::DeltaUntilNextMarker( void )
{
	auto markers =  GetInBetweenMarkers();
	return *(markers.b) - mCurPos;
}

Timeline::InBetweenMarkers 
Timeline::GetInBetweenMarkers( void )
{
	assert(!mMarkers.empty());

	InBetweenMarkers ret;
	ret.a = mMarkers.begin();
	ret.b = mMarkers.begin();

	auto iter = mMarkers.begin();
	
	while (iter != mMarkers.end())
	{		
		if (*iter <= mCurPos)
			ret.a = iter;
		iter++;
	}

	iter = ret.a;
	iter++;

	if (iter == mMarkers.end())
		ret.b = ret.a;
	else
		ret.b = iter;

	return ret;
}

/*virtual*/ void 
Timeline::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{
	if (mMarkers.empty())
		return;

	double last_marker_pos = *(--mMarkers.end());

	if (mLastTime < 0 || mPaused)
	{
		mLastTime = currentTime;
		return;
	}

	if (mHasReachedEnd)
	{		
		return;
	}

	mCurPos += (currentTime-mLastTime);
			
	if (mCurPos > last_marker_pos)
	{
		mCurPos = last_marker_pos;
		mHasReachedEnd = true;
	}	

	// calculate last marker
	int pos = -1;
	for (auto iter : mMarkers)
	{		
		if (iter < mCurPos )
		{
			pos++;
		}		
	}
	mLastMarker = std::max(0, pos);	

	mLastTime = currentTime;
}

unsigned int const 
Timeline::LastMarker( void )
{
	return mLastMarker;
}


