#include <assert.h>
#include <algorithm>
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "SpatialGridPartition.h"
#include "EventsMetadata.h"
#include "StlUtils.h"
#include "Utils/ComponentFetches.h"
#include "PcSceneManager.h"

extern BaseApp* gApp;

PcSceneManager::PcSceneManager(EventManager* const eventMngr, unsigned int const targetNumPointsPerFrame) : System(gApp->EntityMngr())
{
	mTargetNumPoints = targetNumPointsPerFrame;
	mEventMngr = eventMngr;
	mTotalPoints = 0;
	mBvh = NULL;

	// Favor distant, small area, and large num points
	mDistanceLodWeight = 1.f;
	mAreaLodWeight = 3.f;
	mPointsLodWeight = 1.f;

	mIsInitiated = false;
	mShowingBuckets = false;
	mShowingSubPcsAabbs = false;
	mShowingBvh = false;
	mHideNonTemporalAffectedPoints = false;
	mCurrFrame = -1;

	mTmpAnalysis = new TemporalAnalysis(this);

	mTimeline = std::make_shared<Timeline>(gApp->EntityMngr());
	mTimelineId = gApp->SystemMngr()->AddSystem(mTimeline);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mTimelineId, 1/60);
}


PcSceneManager::~PcSceneManager(void)
{
	Reset();

	if (mTmpAnalysis)
		delete mTmpAnalysis;
}

void 
PcSceneManager::AppendPcFrame( std::vector<PointCloudRenderable::Point>& points, DirectX::XMFLOAT3 const& min, DirectX::XMFLOAT3 const& max )
{
	if (mIsInitiated)
	{
		Notification notification;
		notification.notification = L"The system has already been initiated.";
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);

		return;
	}

	mTotalPoints += (unsigned int)points.size();

	if (mAppendedPoints.empty())
	{
		mAppendedMin = min;
		mAppendedMax = max;	
	}
	else
	{
		if (min.x < mAppendedMin.x)
			mAppendedMin.x = min.x;
		if (min.y < mAppendedMin.y)
			mAppendedMin.y = min.y;
		if (min.z < mAppendedMin.z)
			mAppendedMin.z = min.z;

		if (max.x > mAppendedMax.x)
			mAppendedMax.x = max.x;
		if (max.y > mAppendedMax.y)
			mAppendedMax.y = max.y;
		if (max.z > mAppendedMax.z)
			mAppendedMax.z = max.z;	
	}

	mAppendedPoints.push_back(points);

	Notification notification;
	std::wstringstream ss;   
	ss << mAppendedPoints.size(); 
	notification.notification = ss.str() + L" frame(s) appended.";
	notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	notification.durationSec = 2.0;
	mEventMngr->BroadcastEvent("NOTIFICATION", &notification);

	BroadcastPointsStats(mTotalPoints, 0);
}

void
PcSceneManager::Init( unsigned int forceNumBucketsApprox )
{
	if (!mIsInitiated)
	{
		if (mAppendedPoints.empty())
		{
			Notification notification;
			notification.notification = L"There is no appended frame.";
			notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
			notification.durationSec = 2.0;
			mEventMngr->BroadcastEvent("NOTIFICATION", &notification);

			return;
		}

		// We want a few thousands of buckets tops to reduce draw calls... the following scheme will get a "decent" bucket size most of the time
		unsigned int /*const*/ NUM_BUCKETS = forceNumBucketsApprox/*1000*/; // TODO: this should scale somehow (by world size or by how many avg pnts per buckets, etc....)
		float volume = (mAppendedMax.x-mAppendedMin.x) * (mAppendedMax.y-mAppendedMin.y) * (mAppendedMax.z-mAppendedMin.z);
		//OutputDebugMsg("Volume = " + to_string(volume) + " \n");
		mBucketSize = std::powf(volume / NUM_BUCKETS, 1.f/3.f);

		std::unique_ptr<SpatialGridPartition> grid( new SpatialGridPartition(mAppendedMin, mAppendedMax, mBucketSize) );

		unsigned int total_frames = (unsigned int)mAppendedPoints.size();

		for (unsigned int i = 0; i < total_frames; i++)
		{
			Frame frm;
			mFrames.push_back(frm);

// 			unsigned int min_bucket_index = 999999999;
// 			unsigned int max_bucket_index = 0;

			// Start at back of vector and assign points to buckets while popping back the vector...
			// This will keep memory used at around the same amount as the beginning of this function
			while (!mAppendedPoints[i].empty())
			{
				PointCloudRenderable::Point pnt = mAppendedPoints[i].back();
				unsigned int bucket_index = grid->IndexFromXyz(pnt.xyz);

// 				if (bucket_index < min_bucket_index)
// 					min_bucket_index = bucket_index;
// 				if (bucket_index > max_bucket_index)
// 					max_bucket_index = bucket_index;

				mFrames[i].buckets[bucket_index].AddPoint(pnt);

				// TESTING GRID LOGIC HERE ////////////////////////
 				DirectX::XMFLOAT3 bucket_center = grid->CellCenterFromIndex(bucket_index);
				float half_bucket_size = (mBucketSize / 2.f);
				assert( pnt.xyz.x >= (bucket_center.x - half_bucket_size));
				assert( pnt.xyz.x <= (bucket_center.x + half_bucket_size));
				assert( pnt.xyz.y >= (bucket_center.y - half_bucket_size));
				assert( pnt.xyz.y <= (bucket_center.y + half_bucket_size));
				assert( pnt.xyz.z >= (bucket_center.z - half_bucket_size));
				assert( pnt.xyz.z <= (bucket_center.z + half_bucket_size));
				///////////////////////////////////////////////////

				mAppendedPoints[i].pop_back();
			}
		}
		FreeAppendedData();


 		// Create entities and give buckets aabb's so that we can insert them into the partitioning tree
 		std::vector<Bvh::BvhData*> bvh_input;

		for (unsigned int i = 0; i < total_frames; i++)
		{
			std::vector<DirectX::XMFLOAT3> bucket_centers;
			std::vector<DirectX::BoundingBox> subpc_aabb;

 			std::map<unsigned int, PcBucket>::iterator iter = mFrames[i].buckets.begin();
			for (; iter != mFrames[i].buckets.end(); ++iter)
		 	{
		 		DirectX::XMFLOAT3 bucket_center = grid->CellCenterFromIndex(iter->first);
		 		assert( bucket_center.x == bucket_center.x); // NAN CHECK
		 		assert( bucket_center.y == bucket_center.y); // NAN CHECK
		 		assert( bucket_center.z == bucket_center.z); // NAN CHECK
		 		bucket_centers.push_back(bucket_center);
		 
		 		// Assign a bound
		 		DirectX::XMFLOAT3 extents(mBucketSize/2.f, mBucketSize/2.f, mBucketSize/2.f);
		 		assert( extents.x >= 0.f); // EXTENTS VALIDITY CHECK
		 		assert( extents.y >= 0.f); // EXTENTS VALIDITY CHECK
		 		assert( extents.z >= 0.f); // EXTENTS VALIDITY CHECK
		 		DirectX::BoundingBox aabb( bucket_center, extents );
		 		iter->second.aabb = aabb;
		 
		 		bvh_input.push_back(&(iter->second));
				mBucketToFrameLookup[&(iter->second)] = i;
		 
		 		// reverse lookup used when intersecting frustum to spatial partition tree
		 		// we can then quickly retrieve the entity that needs attaching/detaching with the bucket index
		 		mFrames[i].bucketIndices[&(iter->second)] = iter->first;
		 
		 		// Actual Pc Entity
				std::random_shuffle ( iter->second.PointsRef().begin(), iter->second.PointsRef().end() ); // this should help reduce LOD gaps
		 		PcEntity* entity = new PcEntity(iter->second.PointsRef());
		 		mFrames[i].entities[iter->first] = entity;
				subpc_aabb.push_back(entity->Aabb());
		 	}

			// Sub point clouds aabbs debug
			std::shared_ptr<GenericAabbDebugRenderable> subpc_aabbs_rndrble(new GenericAabbDebugRenderable(subpc_aabb, DirectX::XMFLOAT3(1.f,1.f,0.f)));			
			unsigned int subpc_aabbs_entity_id = gApp->EntityMngr()->CreateEntity();
			mFrames[i].entitiesDebugAabbs = std::pair< unsigned int, std::shared_ptr<GenericAabbDebugRenderable> >(subpc_aabbs_entity_id, subpc_aabbs_rndrble);
		 
			// Bucket debug
			std::shared_ptr<BucketDebugRenderable> bucket_dbg_rndrble(  new BucketDebugRenderable( mBucketSize, 
																								   bucket_centers,
																								   DirectX::XMFLOAT3(1.f, 0.f, 0.f) )
																								  );

			unsigned int bucket_dbg_entity_id = gApp->EntityMngr()->CreateEntity();
			mFrames[i].bucketsDbgEntity = std::pair< unsigned int, std::shared_ptr<BucketDebugRenderable> >(bucket_dbg_entity_id, bucket_dbg_rndrble);
		}





		// Build partitioning tree
		assert(!mBvh);
		{          
			mBvh = new Bvh;
			mBvh->Build(bvh_input);
		}

		// Bvh debug entity
		std::vector<DirectX::BoundingBox> bvh_aabbs;
		mBvh->GetAllNodeBounds(bvh_aabbs);
		std::shared_ptr<GenericAabbDebugRenderable> bvh_dbg_rndrble(new GenericAabbDebugRenderable(bvh_aabbs, DirectX::XMFLOAT3(0.f, 1.f, 0.f)));
		unsigned int bvh_dbg_entity_id = gApp->EntityMngr()->CreateEntity();
		mBvhDbgEntity = std::pair< unsigned int, std::shared_ptr<GenericAabbDebugRenderable> >(bvh_dbg_entity_id, bvh_dbg_rndrble);

		mIsInitiated = true;

		mEventMngr->BroadcastEvent("PC_SYSTEM_INITIALIZED", (Metadata *const)NULL);
	}
	else
	{
		Notification notification;
		notification.notification = L"The system has already been initiated.";
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
	}
}

void 
PcSceneManager::Reset( void )
{
	// Remove renderables from
	for (unsigned int i = 0; i < mFrames.size(); i++)
	{
		std::map<unsigned int, PcEntity*>::iterator iter = mFrames[i].entities.begin();
		for (; iter != mFrames[i].entities.end(); ++iter)
		{
			delete iter->second;
		}	

		if (gApp) // since this func is being called from destructor... gotta make sure gApp hasn't been deleted due to exiting the app
		{
			mTmpAnalysis->CleanUp();

			gApp->EntityMngr()->DestroyEntity(mFrames[i].bucketsDbgEntity.first);
			gApp->EntityMngr()->DestroyEntity(mFrames[i].entitiesDebugAabbs.first);
			gApp->EntityMngr()->DestroyEntity(mBvhDbgEntity.first);
			BroadcastPointsStats(0, 0);
		}
	}

	std::vector< Frame >  empty_frms;
	std::swap( mFrames, empty_frms );
	mFrames.clear();

	if (mBvh)
	{
		delete mBvh;
		mBvh = NULL;
	}

	FreeAppendedData();

	mTotalPoints = 0;
	mRenderList.clear();

	mBucketToFrameLookup.clear();

	mAnimationData.clear();
	mTimeline->Reset();

	mIsInitiated = false;

	mShowingBuckets = false;
	mShowingSubPcsAabbs = false;
	mShowingBvh = false;

	mCurrFrame = -1;
}

void 
PcSceneManager::FreeAppendedData( void )
{
	for (unsigned int i = 0; i < mAppendedPoints.size(); i++)
	{
		std::vector<PointCloudRenderable::Point> empty;
		std::swap( mAppendedPoints[i], empty );
	}
	mAppendedPoints.clear();
}

void 
PcSceneManager::ToggleBucketBounds( bool const& on )
{
	static std::set<unsigned int> frames_showing;

	std::set<unsigned int>::iterator iter = frames_showing.begin();
	for (; iter != frames_showing.end(); ++iter)
		gApp->EntityMngr()->RemoveComponentFromEntity(mFrames[*iter].bucketsDbgEntity.first, mFrames[*iter].bucketsDbgEntity.second->GetComponentTypeGuid());

	frames_showing.clear();
	
	if (on)
	{
		if (mCurrFrame < 0)
		{
			for (unsigned int i = 0; i < mFrames.size(); i++)
			{
				gApp->EntityMngr()->AddComponentToEntity(mFrames[i].bucketsDbgEntity.first, mFrames[i].bucketsDbgEntity.second);
				frames_showing.insert(i);
			}
		}
		else
		{
			gApp->EntityMngr()->AddComponentToEntity(mFrames[mCurrFrame].bucketsDbgEntity.first, mFrames[mCurrFrame].bucketsDbgEntity.second);
			frames_showing.insert(mCurrFrame);
		}
	}
	
	mShowingBuckets = on;
}

void 
PcSceneManager::ToggleSubPointCloudsAabbs(bool const& on)
{
	static std::set<unsigned int> frames_showing;

	std::set<unsigned int>::iterator iter = frames_showing.begin();
	for (; iter != frames_showing.end(); ++iter)
		gApp->EntityMngr()->RemoveComponentFromEntity(mFrames[*iter].entitiesDebugAabbs.first, mFrames[*iter].entitiesDebugAabbs.second->GetComponentTypeGuid());

	frames_showing.clear();

	if (on)
	{
		if (mCurrFrame < 0)
		{
			for (unsigned int i = 0; i < mFrames.size(); i++)
			{
				gApp->EntityMngr()->AddComponentToEntity(mFrames[i].entitiesDebugAabbs.first, mFrames[i].entitiesDebugAabbs.second);
				frames_showing.insert(i);
			}
		}
		else
		{
			gApp->EntityMngr()->AddComponentToEntity(mFrames[mCurrFrame].entitiesDebugAabbs.first, mFrames[mCurrFrame].entitiesDebugAabbs.second);
			frames_showing.insert(mCurrFrame);
		}
	}

	mShowingSubPcsAabbs = on;
}


void 
PcSceneManager::ToggleAnalysisResults( bool const& on )
{
	if (mFrames.size() < 2)
		return;

	static std::set<unsigned int> frames_showing;

	std::set<unsigned int>::iterator iter = frames_showing.begin();
	for (; iter != frames_showing.end(); ++iter)
	{
		if (!mFrames[*iter].appearingBuckets.empty())
			gApp->EntityMngr()->RemoveComponentFromEntity(mFrames[*iter].appearingBucketsDbgEntity.first, mFrames[*iter].appearingBucketsDbgEntity.second->GetComponentTypeGuid());
		
		if (!mFrames[*iter].disappearingBuckets.empty())
			gApp->EntityMngr()->RemoveComponentFromEntity(mFrames[*iter].disappearingBucketsDbgEntity.first, mFrames[*iter].disappearingBucketsDbgEntity.second->GetComponentTypeGuid());
		
		if (!mFrames[*iter].changingBuckets.empty())
			gApp->EntityMngr()->RemoveComponentFromEntity(mFrames[*iter].changingBucketsDbgEntity.first, mFrames[*iter].changingBucketsDbgEntity.second->GetComponentTypeGuid());
	}

	frames_showing.clear();

	if (on)
	{
		if (mCurrFrame < 0)
		{
			for (unsigned int i = 0; i < mFrames.size() - 1; i++)
			{
				if (!mFrames[i].appearingBuckets.empty())
					gApp->EntityMngr()->AddComponentToEntity(mFrames[i].appearingBucketsDbgEntity.first, mFrames[i].appearingBucketsDbgEntity.second);
				
				if (!mFrames[i].disappearingBuckets.empty())
					gApp->EntityMngr()->AddComponentToEntity(mFrames[i].disappearingBucketsDbgEntity.first, mFrames[i].disappearingBucketsDbgEntity.second);
				
				if (!mFrames[i].changingBuckets.empty())
					gApp->EntityMngr()->AddComponentToEntity(mFrames[i].changingBucketsDbgEntity.first, mFrames[i].changingBucketsDbgEntity.second);
				
				frames_showing.insert(i);
			}
		}
		else
		{
			if (!mFrames[mCurrFrame].appearingBuckets.empty())
				gApp->EntityMngr()->AddComponentToEntity(mFrames[mCurrFrame].appearingBucketsDbgEntity.first, mFrames[mCurrFrame].appearingBucketsDbgEntity.second);
			
			if (!mFrames[mCurrFrame].disappearingBuckets.empty())
				gApp->EntityMngr()->AddComponentToEntity(mFrames[mCurrFrame].disappearingBucketsDbgEntity.first, mFrames[mCurrFrame].disappearingBucketsDbgEntity.second);
			
			if (!mFrames[mCurrFrame].changingBuckets.empty())
				gApp->EntityMngr()->AddComponentToEntity(mFrames[mCurrFrame].changingBucketsDbgEntity.first, mFrames[mCurrFrame].changingBucketsDbgEntity.second);
			
			frames_showing.insert(mCurrFrame);
		}
	}

	mShowingAnalysisResults = on;
}

void 
PcSceneManager::ToggleBvhBounds( bool const& on )
{
	if (mBvh)
	{
		if (on)
			gApp->EntityMngr()->AddComponentToEntity(mBvhDbgEntity.first, mBvhDbgEntity.second);
		else
			gApp->EntityMngr()->RemoveComponentFromEntity(mBvhDbgEntity.first, mBvhDbgEntity.second->GetComponentTypeGuid());
	}

	mShowingBvh = on;
}

void 
PcSceneManager::RunImplementation( std::set<unsigned int> const* /*family*/, double const /*currentTime*/ )
{	
	unsigned int num_pnts_rendered = 0;

 	if (mBvh)
 	{
		if (!mAnimationData.empty() && mCurrFrame >= 0)
		{
			// @ the start of every loop we just reset all modified xforms back to the origin.
			for (auto iter = mAnimationData[mCurrFrame].begin();
				iter != mAnimationData[mCurrFrame].end();
				++iter)
			{		
 				auto iter_tmp = mFrames[mCurrFrame].entities.find(iter->first);

				if (mFrames[mCurrFrame].entities.end() == iter_tmp)
				{
					Notification notification;
					notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
					notification.notification = L"Wrong animation data loaded.  Could not locate PcEntity.";
					notification.durationSec = 2.0;
					mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
					continue;
				}				

				PcEntity* entity = iter_tmp->second;
 				assert(entity);
				entity->SpatializedComponent()->LocalPosition(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
			}

			// Check if timeline went through a frame and update if needed
			if (mTimeline->IsPlaying())
			{
				if (mTimeline->HasReachedEnd() && mCurrFrame != (unsigned int)(mFrames.size() - 1))
				{
					CurrentFrame((unsigned int)(mFrames.size() - 1));
					mTimeline->Pause();
				}
				else if (!mTimeline->HasReachedEnd())
				{
					unsigned int last_frame = mTimeline->LastMarker();
					//OutputDebugMsg(to_string(last_frame)+"\n");
					if (mCurrFrame != last_frame)
						CurrentFrame(last_frame);
				}
			}
		}

		DirectX::XMMATRIX cam_view = DirectX::XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ViewMatrix());
		DirectX::XMVECTOR det_view = DirectX::XMMatrixDeterminant(cam_view);
		DirectX::XMMATRIX inv_view = DirectX::XMMatrixInverse(&det_view, cam_view);

		DirectX::XMMATRIX cam_proj = DirectX::XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ProjMatrix());

		DirectX::XMVECTOR scale, rot, trans;
		DirectX::XMMatrixDecompose(&scale, &rot, &trans, inv_view);
		float max_scale = (std::max)(DirectX::XMVectorGetX(scale), (std::max)(DirectX::XMVectorGetY(scale), DirectX::XMVectorGetZ(scale)));

		DirectX::BoundingFrustum frustum = gApp->Renderer()->CurrentCamera()->CameraFrustum();
		frustum.Transform(frustum, max_scale, rot, trans);

		// Detach old render_list
		for (unsigned int i = 0; i < mRenderList.size(); i++)
			mRenderList[i]->DetachRenderable();

		mRenderList.clear();
		
 		std::vector<Bvh::BvhData*> bvh_intersect;
		if (mTargetNumPoints > 0)
 			mBvh->Intersect(frustum, bvh_intersect);
		 
 		// Do finer intersection....  & filter out non-current frames
 		for (unsigned int i = 0; i < bvh_intersect.size(); i++)
 		{
			unsigned int frame = mBucketToFrameLookup[bvh_intersect[i]];

			// Is the bucket for the correct frame?
			if (mCurrFrame >= 0 && mCurrFrame != frame)
				continue;

			std::map<Bvh::BvhData*, unsigned int>::iterator iter = mFrames[frame].bucketIndices.find(bvh_intersect[i]);
 			if (  mFrames[frame].bucketIndices.end() != iter ) 
 			{
				unsigned int bucket_index = iter->second;


				// Should we hide non-affected points?
				if ( mHideNonTemporalAffectedPoints && mCurrFrame >= 0 )
				{
					auto appearing = mFrames[frame].appearingBuckets.find(bucket_index);
					auto disappearing = mFrames[frame].disappearingBuckets.find(bucket_index);
					auto changing = mFrames[frame].changingBuckets.find(bucket_index);
					
					if ( appearing == mFrames[frame].appearingBuckets.end() &&
						 disappearing == mFrames[frame].disappearingBuckets.end() &&
						 changing == mFrames[frame].changingBuckets.end() )
						 continue;
				}

				// All good... add to render list
  				PcEntity* entity = mFrames[frame].entities[ bucket_index ];				
				assert(entity);
 				if ( frustum.Contains(entity->Aabb()) != DirectX::DISJOINT )
 				{
 					mRenderList.push_back(entity);
 				}
 			}				
 		}
 
 		// Attach			
 		for (unsigned int i = 0; i < mRenderList.size(); i++)
 		{				
 			mRenderList[i]->AttachRenderable();
 			mRenderList[i]->SetNumPointsToRender(mRenderList[i]->TotalPoints());
 			num_pnts_rendered += mRenderList[i]->TotalPoints();
 		}		

		// Handle animation logic
		for (auto iter = mAnimationData[mCurrFrame].begin();
				  iter != mAnimationData[mCurrFrame].end();
				  ++iter )
		{			
			auto iter_tmp = mFrames[mCurrFrame].entities.find(iter->first);

			if (mFrames[mCurrFrame].entities.end() == iter_tmp)
			{
				Notification notification;
				notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
				notification.notification = L"Wrong animation data loaded.  Could not locate PcEntity.";
				notification.durationSec = 2.0;
				mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
				continue;
			}

			PcEntity* entity = iter_tmp->second;
			assert(entity);
			entity->SpatializedComponent()->LocalPosition(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
			

			// Set current pos
			PcBucket* from_bkt = &(mFrames[mCurrFrame].buckets[iter->first]);
			PcBucket* to_bkt   = &(mFrames[mCurrFrame+1].buckets[iter->second]);

			double since = mTimeline->DeltaSinceLastMarker();
			double until = mTimeline->DeltaUntilNextMarker();
			float percentage = (float)(since / (since + until));

			DirectX::XMFLOAT3 start_pos = from_bkt->aabb.Center;
			DirectX::XMFLOAT3 end_pos = to_bkt->aabb.Center;
			DirectX::XMFLOAT3 dir_dist = DirectX::XMFLOAT3(  (end_pos.x - start_pos.x)*percentage,
															 (end_pos.y - start_pos.y)*percentage,
															 (end_pos.z - start_pos.z)*percentage  );
			DirectX::XMFLOAT3 cur_pos = DirectX::XMFLOAT3(dir_dist.x, dir_dist.y, dir_dist.z);

			entity->SpatializedComponent()->LocalPosition(cur_pos);

			// Add to render list and render regardless if culled or not... Note this might be super inefficient if there is a lot of animated buckets.
			entity->AttachRenderable();
			mRenderList.push_back(entity);			
		}

 		// Have we passed our pnt target?  if so do LOD
  		if (num_pnts_rendered > mTargetNumPoints)
  		{
  			std::vector<float> lod_weights;
  			CalculateLodWeights(mRenderList, lod_weights);
  
  			// CalculateLodWeights orders render_list by distance
  			// Go back to front and prune by the weight
  			for (int i = (unsigned int)mRenderList.size()-1; i >= 0; i--)
  			{
  				unsigned int pnts = (unsigned int)(mRenderList[i]->TotalPoints() * lod_weights[i]);
  				mRenderList[i]->SetNumPointsToRender(pnts);
  
  				// Retrieve the difference
  				num_pnts_rendered -= mRenderList[i]->TotalPoints() - pnts;
  
  				// Break out as soon as we're under target
  				if (num_pnts_rendered <= mTargetNumPoints)
  					break;
  			}
  		}	
 	}

	BroadcastPointsStats(mTotalPoints, num_pnts_rendered);
	BroadcastAnimTimelineStats();
}

void
PcSceneManager::CalculateLodWeights(std::vector<PcEntity*>& entitiesIn, std::vector<float>& weightsOut)
{
	float min_dist, min_area, min_pnts;
	float max_dist, max_area, max_pnts;

	std::vector<float> dists_rslts, areas_rslts, pnts_rslts;
	dists_rslts.resize(entitiesIn.size());
	areas_rslts.resize(entitiesIn.size());
	pnts_rslts.resize(entitiesIn.size());

	OrderByDistance(entitiesIn, dists_rslts);

	std::vector<PcEntity*> area_ordered = entitiesIn;
	std::vector<PcEntity*> pnts_ordered = entitiesIn;
		
	OrderByScreenAabbArea(area_ordered, areas_rslts);
	OrderByTotalPoints(pnts_ordered, pnts_rslts);

	min_dist = dists_rslts[0]; max_dist = dists_rslts[entitiesIn.size()-1];
	min_area = areas_rslts[0]; max_area = areas_rslts[entitiesIn.size()-1];
	min_pnts = pnts_rslts[0];  max_pnts = pnts_rslts[entitiesIn.size()-1];

// 	OutputDebugMsg("Dist: " + to_string(min_dist) + "\t" + to_string(max_dist) + "\n");
// 	OutputDebugMsg("Area: " + to_string(min_area) + "\t" + to_string(max_area) + "\n");
// 	OutputDebugMsg("Pnts: " + to_string(min_pnts) + "\t" + to_string(max_pnts) + "\n\n");

	struct Positions
	{
		float dist;
		float area;
		float pnts;
	};
	std::map<PcEntity*, Positions> positions; 

	unsigned int num_entities = (unsigned int)entitiesIn.size();

	// Accumulate positions in array
	for (unsigned int i = 0; i < num_entities; i++)
	{
		positions[entitiesIn[i]].dist += i/num_entities;
		positions[area_ordered[i]].area += i/num_entities;
		positions[pnts_ordered[i]].pnts += i/num_entities;
	}

	// Figure out weights (this scheme could change.... potentially... even become dynamic)
	weightsOut.resize(num_entities);
	for (unsigned int i = 0; i < num_entities; i++)
	{
		float dist_rank = 1.f - ( (max_dist - dists_rslts[i]) / (max_dist - min_dist) );
		float area_rank = 1.f - ( (max_area - areas_rslts[i]) / (max_area - min_area) );
		float pnts_rank = 1.f - ( (max_pnts - pnts_rslts[i]) / (max_pnts - min_pnts) );

		weightsOut[i] = ((mDistanceLodWeight * dist_rank) + (mAreaLodWeight * area_rank) + (mPointsLodWeight * pnts_rank)) / (mDistanceLodWeight + mAreaLodWeight + mPointsLodWeight);
	}
}

void 
PcSceneManager::AttachBucketRenderable( unsigned int const& bucketIndex )
{
	for (unsigned int i = 0; i < mFrames.size(); i++)
		if (mFrames[i].entities[bucketIndex])
			mFrames[i].entities[bucketIndex]->AttachRenderable();
}

void 
PcSceneManager::DetachBucketRenderable( unsigned int const& bucketIndex )
{
	for (unsigned int i = 0; i < mFrames.size(); i++)
		if (mFrames[i].entities[bucketIndex])
			mFrames[i].entities[bucketIndex]->DetachRenderable();
}

void 
PcSceneManager::BroadcastPointsStats( unsigned int const total, unsigned int const rendered )
{
	PointStats stats;
	stats.totalNumPoints = total;
	stats.renderedNumPoints = rendered;	
	mEventMngr->BroadcastEvent("POINTS_STATS", &stats);
}

void 
PcSceneManager::OrderByDistance( std::vector<PcEntity*>& toOrder, std::vector<float>& distsOut )
{
	std::vector< std::pair<PcEntity*, float> > dists;
	dists.resize(toOrder.size());

	DirectX::XMFLOAT4 cur_pos = gApp->Renderer()->CurrentCamera()->Position();
	for (unsigned int i = 0; i < toOrder.size(); i++)
	{
		DirectX::XMFLOAT3 center = toOrder[i]->Aabb().Center;
		float dist = std::powf((center.x - cur_pos.x), 2.f) + std::powf((center.y - cur_pos.y), 2.f) + std::powf((center.z - cur_pos.z), 2.f);
		dists[i] = std::pair<PcEntity*, float>(toOrder[i], dist);
	}

	std::sort(dists.begin(), dists.end(), CompareSecondPairElementPcEntity);
	
	float prev_dist = -1.f;
	for (unsigned int i = 0; i < dists.size(); i++)
	{
		distsOut[i] = dists[i].second;

		assert(dists[i].second >= prev_dist);
		prev_dist = dists[i].second;
		toOrder[i] = dists[i].first;
	}
}

void 
PcSceneManager::OrderByScreenAabbArea( std::vector<PcEntity*>& toOrder, std::vector<float>& areasOut )
{
	std::vector< std::pair<PcEntity*, float> > areas;
	areas.resize(toOrder.size());

	for (unsigned int i = 0; i < toOrder.size(); i++)
	{
		float area = (float)CalculateScreenAabbArea(toOrder[i]->Aabb());
		areas[i] = std::pair<PcEntity*, float>(toOrder[i], area);
	}

	std::sort(areas.begin(), areas.end(), CompareSecondPairElementPcEntity);

	float prev_area = -1.f;
	for (unsigned int i = 0; i < areas.size(); i++)
	{
		areasOut[i] = areas[i].second;

		assert(areas[i].second >= prev_area);
		prev_area = areas[i].second;
		toOrder[i] = areas[i].first;
	}
}

void 
PcSceneManager::OrderByTotalPoints( std::vector<PcEntity*>& toOrder, std::vector<float>& totalPointsOut )
{
	std::vector< std::pair<PcEntity*, float> > pnts;
	pnts.resize(toOrder.size());

	for (unsigned int i = 0; i < toOrder.size(); i++)
	{
		float pnt = (float)toOrder[i]->TotalPoints();
		pnts[i] = std::pair<PcEntity*, float>(toOrder[i], pnt);
	}

	std::sort(pnts.begin(), pnts.end(), CompareSecondPairElementPcEntity);

	float prev_pnts = -1.f;
	for (unsigned int i = 0; i < pnts.size(); i++)
	{
		totalPointsOut[i] = pnts[i].second;

		assert(pnts[i].second >= prev_pnts);
		prev_pnts = pnts[i].second;
		toOrder[i] = pnts[i].first;
	}
}

unsigned int
PcSceneManager::CalculateScreenAabbArea( DirectX::BoundingBox const& aabb )
{
	DirectX::XMFLOAT3 pnts[8];

	pnts[0] = DirectX::XMFLOAT3(aabb.Center.x + aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z);

	pnts[1] = DirectX::XMFLOAT3(aabb.Center.x - aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z);

	pnts[2] = DirectX::XMFLOAT3( aabb.Center.x + aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z);

	pnts[3] = DirectX::XMFLOAT3( aabb.Center.x - aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z);

	pnts[4] = DirectX::XMFLOAT3( aabb.Center.x + aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z);

	pnts[5] = DirectX::XMFLOAT3( aabb.Center.x - aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z);

	pnts[6] = DirectX::XMFLOAT3( aabb.Center.x + aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z);

	pnts[7] = DirectX::XMFLOAT3( aabb.Center.x - aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z);

	DirectX::XMMATRIX proj =  XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ProjMatrix());
	DirectX::XMMATRIX view =  XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ViewMatrix());
	DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();

	DirectX::XMFLOAT3 scrn_pnts[8];
	DirectX::XMVector3ProjectStream(&scrn_pnts[0], sizeof(DirectX::XMFLOAT3), &pnts[0], sizeof(DirectX::XMFLOAT3), 8,
									0.f, 0.f, (float)gApp->WinWidth(), (float)gApp->WinHeight(), 0.f, 1.f, proj, view, world);

	std::set<float> xs;
	xs.insert( scrn_pnts[0].x );
	xs.insert( scrn_pnts[1].x );
	xs.insert( scrn_pnts[2].x );
	xs.insert( scrn_pnts[3].x );
	xs.insert( scrn_pnts[4].x );
	xs.insert( scrn_pnts[5].x );
	xs.insert( scrn_pnts[6].x );
	xs.insert( scrn_pnts[7].x );

	std::set<float> ys;
	ys.insert( scrn_pnts[0].y );
	ys.insert( scrn_pnts[1].y );
	ys.insert( scrn_pnts[2].y );
	ys.insert( scrn_pnts[3].y );
	ys.insert( scrn_pnts[4].y );
	ys.insert( scrn_pnts[5].y );
	ys.insert( scrn_pnts[6].y );
	ys.insert( scrn_pnts[7].y );

	float w = (std::max)( 1.f, ( (*(--xs.end())) - (*(xs.begin()))  ) );
	float h = (std::max)( 1.f, ( (*(--ys.end())) - (*(ys.begin()))  ) );

	//OutputDebugMsg("Area = "  + to_string(w*h) + "\n");

	return (unsigned int)(w * h);
}

void 
PcSceneManager::CurrentFrame( int const frame )
{
	Notification notification;
	std::wstringstream total_frames_str;   
	total_frames_str << mFrames.size(); 

	if (!mIsInitiated)
	{
		mCurrFrame = -1;  // insurance
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.notification = L"The system needs to be initiated.";
	}
	else if (frame < 0)
	{
		mCurrFrame = -1;
		notification.notification = L"All " + total_frames_str.str() + L" frame(s).";
		notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	}
	else
	{
		if (frame >= (int)mFrames.size())
			mCurrFrame = (int)mFrames.size() - 1;
		else
			mCurrFrame = frame;

		std::wstringstream curr_frame_str;   
		curr_frame_str << mCurrFrame+1; 
		notification.notification = L"Frame " + curr_frame_str.str() + L"/" + total_frames_str.str();
		notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	}

	ToggleBucketBounds(mShowingBuckets);
	ToggleSubPointCloudsAabbs(mShowingSubPcsAabbs);
	ToggleAnalysisResults(mShowingAnalysisResults);
		
	notification.durationSec = 2.0;
	mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
}

void 
PcSceneManager::IntersectMouseRayWithCurrentFrameBuckets( int& bucketIdOut, DirectX::XMFLOAT3& bucketCenterOut )
{
	if (mCurrFrame < 0)
	{
		bucketIdOut = -1;
		return;		
	}
		
	std::shared_ptr<InputDeviceManager> manager = gApp->InputDeviceMngr();
	DirectX::XMMATRIX proj_mat = DirectX::XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ProjMatrix());
	DirectX::XMFLOAT4X4 proj_v4x4; DirectX::XMStoreFloat4x4(&proj_v4x4 ,proj_mat);

	float vx;
	float vy;

	if (manager->Mouse()->RelativeMode())
	{
		vx = 0;
		vy = 0;
	}
	else
	{
		vx = (+2.0f * manager->Mouse()->MouseX() / gApp->WinWidth() - 1.0f) / proj_v4x4(0, 0);
		vy = (-2.0f * manager->Mouse()->MouseY() / gApp->WinHeight() + 1.0f) / proj_v4x4(1, 1);
	}


	// Ray in view space
	DirectX::XMVECTOR ray_orig = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	DirectX::XMVECTOR ray_dir = DirectX::XMVectorSet(vx, vy, 1.0f, 0.0f);

	float min_dist = 999999.9f;
	int closest_intersected_bucket = -1;
	
	// Check for intersection on passed in entities	
	for (auto iter = mFrames[mCurrFrame].buckets.begin();
		 iter != mFrames[mCurrFrame].buckets.end();
		 iter++)
	{ 

		DirectX::XMVECTOR orig, dir;
		DirectX::XMMATRIX view = XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ViewMatrix());
		DirectX::XMMATRIX inv_view = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(view), view);

		DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
		DirectX::XMMATRIX inv_world = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(world), world);

		DirectX::XMMATRIX to_local = DirectX::XMMatrixMultiply(inv_view, inv_world);

		orig = DirectX::XMVector3TransformCoord(ray_orig, to_local);
		dir = DirectX::XMVector3TransformNormal(ray_dir, to_local);
		dir = DirectX::XMVector3Normalize(dir);

		float dist = min_dist + 1.f;
		if (iter->second.aabb.Intersects(orig, dir, dist) != DirectX::DISJOINT)
			if (dist < min_dist)
			{
				closest_intersected_bucket = iter->first;
				bucketCenterOut = iter->second.aabb.Center;
			}
		
	}	

	bucketIdOut = closest_intersected_bucket;
}

void 
PcSceneManager::BroadcastAnimTimelineStats( void )
{
	AnimationTimelineStats stats;	
	stats.currentFrame = mCurrFrame+1;
	stats.totalFrames = (unsigned int)mFrames.size();
	stats.currentPos = mTimeline->CurrentPosition();
	stats.playing = mTimeline->IsPlaying();
	stats.playSpeedFactor = mTimeline->GetPlaySpeedFactor();
	mEventMngr->BroadcastEvent("TIMELINE_STATS", &stats);
}

void 
PcSceneManager::InitializeAnimation( std::map< unsigned int, std::map<unsigned int, unsigned int> > animData, 
									 std::set<double> markers )
{
	if (!mIsInitiated)
	{
		mCurrFrame = -1;  // insurance
		Notification notification;
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.notification = L"The system needs to be initiated.";
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
		return;
	}

	if (markers.size() != mFrames.size())
	{
		Notification notification;
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.notification = L"Wrong animation data loaded.";
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
		return;
	}

	mAnimationData = animData;

	for (auto iter = markers.begin(); iter != markers.end(); ++iter)
		mTimeline->InsertMarker(*iter);

	Notification notification;
	notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	notification.notification = L"Animation data initialized.";
	notification.durationSec = 2.0;
	mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
}

void 
PcSceneManager::ToggleAnimation( void )
{	
	if (!mIsInitiated)
	{
		mCurrFrame = -1;  // insurance
		Notification notification;
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.notification = L"The system needs to be initiated.";
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
		return;
	}
	if (mAnimationData.empty())
	{
		Notification notification;
		notification.notification = L"No animation data loaded.";
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.durationSec = 2.0;
		mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
		return;
	}

	if (!mTimeline->IsPlaying())
	{
		if (mCurrFrame == -1 || mTimeline->HasReachedEnd())
		{
			CurrentFrame(0);
			mTimeline->SetPosition(0.0);
		}

		// If last marker doesn't match current frame, set position
		unsigned int last_marker = mTimeline->LastMarker();
		if (last_marker != mCurrFrame && mCurrFrame >= 0)
		{
			CurrentFrame(mCurrFrame);
			mTimeline->SetPosition((unsigned int)mCurrFrame);
		}
		mTimeline->Play();
	}
	else
		mTimeline->Pause();
}

void 
PcSceneManager::ClearAnimation(void)
{
	// Reset xforms
	for (auto fr = mAnimationData.begin(); fr != mAnimationData.end(); ++ fr)
	for (auto iter = mAnimationData[fr->first].begin();
		iter != mAnimationData[fr->first].end();
			++iter)
		{
			auto iter_tmp = mFrames[fr->first].entities.find(iter->first);

			if (mFrames[fr->first].entities.end() == iter_tmp)							
				continue;

			PcEntity* entity = iter_tmp->second;
			assert(entity);
			entity->SpatializedComponent()->LocalPosition(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
		}

	mAnimationData.clear();
	mTimeline->Reset();

	Notification notification;
	notification.notification = L"Animation data cleared.";
	notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	notification.durationSec = 2.0;
	mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
}