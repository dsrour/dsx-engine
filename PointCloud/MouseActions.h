#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <set>

#include "InputDeviceManager.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Components/Renderable.h"

class ApplicationMouseActions : public InputDeviceActions
{
public:	
	ApplicationMouseActions(std::shared_ptr<InputDeviceManager> inputDeviceManager);

	virtual void
	OnStateChange(void);

	
private:
	void 
	CameraTransformActions( std::shared_ptr<InputDeviceManager>& manager );

	std::shared_ptr<Camera> mCamera;

	int mLastX, mLastY;
};




