#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class PointCloudEffect : public Effect
{
public:
	PointCloudEffect(std::shared_ptr<D3dRenderer> renderer);       
	~PointCloudEffect(void);

	virtual void
	Recompile() { if(mRendererRef) Init(mRendererRef, true); }

	// Shader vars          
	void
	UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat);

	void
	UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp); 

	void
	UpdatePcTextureSamplerVariable(ID3D11SamplerState* const sampler);

	void
	UpdatePcTextureVariable(ID3D11ShaderResourceView* const texture);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);
	
private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false);

	struct CbPerFrame
	{
		DirectX::XMMATRIX worldMat;
		DirectX::XMMATRIX vpMat;
	} mPerFrameVariables;
	DirectX::ConstantBuffer< CbPerFrame > mPerFrameCb;
		
	ID3DBlob* mPcVsBlob;
	ID3D11VertexShader* mPcVS;
	ID3DBlob* mPcPsBlob;
	ID3D11PixelShader* mPcPS;

	ID3DBlob* mResolveVsBlob;
	ID3D11VertexShader* mResolveVS;
	ID3DBlob* mResolvePsBlob;
	ID3D11PixelShader* mResolvePS;

	// Texture vars
	ID3D11SamplerState*           mPcTextureSampler;
	ID3D11ShaderResourceView*	  mPcTextureSrv;

	D3dRenderer* mRendererRef; /// Kept for recompile() func
};

