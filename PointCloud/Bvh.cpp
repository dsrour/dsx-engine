#include <assert.h>
#include <algorithm>
#include "StlUtils.h"
#include "Bvh.h"

Bvh::Bvh(void)
{
	mRoot = NULL;
	mMaxDataPerLeafNode = 10;
}

Bvh::Bvh( unsigned int const& maxDataPerLeafNode )
{
	mRoot = NULL;
	mMaxDataPerLeafNode = maxDataPerLeafNode;
}

Bvh::~Bvh(void)
{
	if (mRoot)
	{
		FreeBvh(mRoot);
		delete mRoot;
	}
}

void 
Bvh::Build( std::vector<BvhData*>& bvhData )
{
	assert(!bvhData.empty());

	mRoot = new BvhNode;
	mRoot->aabb = FindFittingAabb(bvhData);

	mRoot->data = std::vector<BvhData*>(bvhData.begin(), bvhData.end());

	BuildRecursively(mRoot);
	CleanNonLeafNodesRecursively(mRoot);
}

DirectX::BoundingBox 
Bvh::FindFittingAabb( std::vector<BvhData*>& bvhData )
{
	assert(!bvhData.empty());

	DirectX::BoundingBox best_fit(bvhData[0]->aabb);

	for (unsigned int i = 1; i < bvhData.size(); i++)
	{
		DirectX::BoundingBox tmp_1(best_fit);
		DirectX::BoundingBox tmp_2(bvhData[i]->aabb);
		DirectX::BoundingBox::CreateMerged(best_fit, tmp_1, tmp_2);
	}

	return best_fit;
}

void 
Bvh::BuildRecursively( BvhNode* root )
{
	// Leaf node?
	if (root->data.size() <= mMaxDataPerLeafNode)
		return;

	std::vector<BvhData*> ordered_x, ordered_y, ordered_z;
	SortByAxis(root->data, ordered_x, X);
	SortByAxis(root->data, ordered_y, Y);
	SortByAxis(root->data, ordered_z, Z);

	float cost_x, cost_y, cost_z;
	BvhNode left_x, left_y, left_z, right_x, right_y, right_z;

	FindBestSplitFromOrderedArray(root, ordered_x, cost_x, left_x, right_x);
	FindBestSplitFromOrderedArray(root, ordered_y, cost_y, left_y, right_y);
	FindBestSplitFromOrderedArray(root, ordered_z, cost_z, left_z, right_z);

	root->left = new BvhNode;
	root->right = new BvhNode;

	if ( cost_x <= cost_y && cost_x <= cost_z )
	{
		root->left->aabb = left_x.aabb;
		root->left->data = left_x.data;

		root->right->aabb = right_x.aabb;
		root->right->data = right_x.data;

	}
	else if ( cost_y <= cost_x && cost_y <= cost_z )
	{
		root->left->aabb = left_y.aabb;
		root->left->data = left_y.data;

		root->right->aabb = right_y.aabb;
		root->right->data = right_y.data;
	}
	else if ( cost_z <= cost_x && cost_z <= cost_y )
	{
		root->left->aabb = left_z.aabb;
		root->left->data = left_z.data;

		root->right->aabb = right_z.aabb;
		root->right->data = right_z.data;
	}

	BuildRecursively(root->left);
	BuildRecursively(root->right);
}

void 
Bvh::SortByAxis( std::vector<BvhData*>& inData, std::vector<BvhData*>& outData, Axis const& axis )
{
	assert(outData.empty());

	std::vector< std::pair<BvhData*, float> > sorted;

	for (  std::vector<BvhData*>::iterator iter = inData.begin();
		   iter != inData.end();
		   ++iter  )
	{
		DirectX::XMFLOAT3 center = (*iter)->aabb.Center;

		if (X == axis)
			sorted.push_back( std::pair<BvhData*, float>(*iter, center.x) );
		else if (Y == axis)
			sorted.push_back( std::pair<BvhData*, float>(*iter, center.y) );
		else if (Z == axis)
			sorted.push_back( std::pair<BvhData*, float>(*iter, center.z) );
	}

	std::sort(sorted.begin(), sorted.end(), CompareSecondPairElementBvhData);

	// Populate outData
	for (  std::vector< std::pair<BvhData*, float> >::iterator iter = sorted.begin();
		   iter != sorted.end();
		   ++iter  )
	{
		outData.push_back(iter->first);
	}
}

float 
Bvh::SahCost( BvhNode const* const root, BvhNode const* const tmpLeft, BvhNode const* const tmpRight )
{
	return SahCost(root, tmpLeft->aabb, tmpRight->aabb, (unsigned int)tmpLeft->data.size(), (unsigned int)tmpRight->data.size());
}

float Bvh::SahCost( BvhNode const* const root, DirectX::BoundingBox const& leftAabb, DirectX::BoundingBox const& rightAabb, int const& leftSize, int const& rightSize )
{
	float root_v_width = root->aabb.Extents.x * 2;
	float root_v_height = root->aabb.Extents.y * 2;
	float root_v_depth = root->aabb.Extents.z * 2;
	float sa_root = 2.f * (root_v_width * root_v_depth  +  root_v_width * root_v_height  +  root_v_depth * root_v_height);

	float lft_v_width = leftAabb.Extents.x * 2;
	float lft_v_height = leftAabb.Extents.y * 2;
	float lft_v_depth = leftAabb.Extents.z * 2;
	float sa_left = 2.f * (lft_v_width * lft_v_depth  +  lft_v_width * lft_v_height  +  lft_v_depth * lft_v_height);

	float rght_v_width = rightAabb.Extents.x * 2;
	float rght_v_height = rightAabb.Extents.y * 2;
	float rght_v_depth = rightAabb.Extents.z * 2;
	float sa_right = 2.f * (rght_v_width * rght_v_depth  +  rght_v_width * rght_v_height  +  rght_v_depth * rght_v_height);

	float p_left = sa_left / sa_root;
	float p_right = sa_right / sa_root;

	return ( (p_left * leftSize) + (p_right * rightSize) );
}

void 
Bvh::FindBestSplitFromOrderedArray( BvhNode* root, std::vector<BvhData*>& orderedArrayIn, float& min_cost_out, BvhNode& leftChildOut, BvhNode& rightChildOut )
{
	assert(!orderedArrayIn.empty());

	// Tmp arrays that'll store the current min, max bounds as we sweep through from both sides
	std::vector< DirectX::BoundingBox > tmp_right_sweep; tmp_right_sweep.resize( orderedArrayIn.size()-1 );
	std::vector< DirectX::BoundingBox > tmp_left_sweep;  tmp_left_sweep.resize( orderedArrayIn.size()-1 );

	// Sweep right
	tmp_right_sweep[0] = orderedArrayIn[0]->aabb;
	for ( unsigned int i = 1; i < orderedArrayIn.size()-1; i++ )
		DirectX::BoundingBox::CreateMerged(tmp_right_sweep[i], tmp_right_sweep[i-1], orderedArrayIn[i]->aabb);

	// Sweep left
	tmp_left_sweep[tmp_left_sweep.size()-1] = orderedArrayIn[ orderedArrayIn.size()-1 ]->aabb;
	for ( int i = (unsigned int)orderedArrayIn.size()-2; i > 0; i-- )
		DirectX::BoundingBox::CreateMerged(tmp_left_sweep[i-1], tmp_left_sweep[i], orderedArrayIn[i]->aabb);

	// Find best split
	leftChildOut.aabb = tmp_right_sweep[0];
	leftChildOut.data = std::vector<BvhData*>(orderedArrayIn.begin(), orderedArrayIn.begin()+1);

	rightChildOut.aabb = tmp_left_sweep[0];
	rightChildOut.data = std::vector<BvhData*>(orderedArrayIn.begin()+1, orderedArrayIn.end());

	min_cost_out = SahCost(root, &leftChildOut, &rightChildOut);

	// Search for split with minimum cost
	unsigned int num_data = (unsigned int)root->data.size();
	unsigned int min_i = 1;
	for (unsigned int i = 1; i < tmp_right_sweep.size(); i++)
	{
		float cost = SahCost(root, tmp_right_sweep[i], tmp_left_sweep[i], i+1, num_data-i-1);
		
		if (cost < min_cost_out)
		{
			min_cost_out = cost;
			min_i = i;
		}
	}

	// Set the BvhBoxes for the min cost split
	leftChildOut.aabb = tmp_right_sweep[min_i];
	leftChildOut.data = std::vector<BvhData*>(orderedArrayIn.begin(), orderedArrayIn.begin()+min_i+1);
	rightChildOut.aabb = tmp_left_sweep[min_i];
	rightChildOut.data = std::vector<BvhData*>(orderedArrayIn.begin()+min_i+1, orderedArrayIn.end());
}

void 
Bvh::FreeBvh( BvhNode* root )
{
	// If leaf, return
	if (!root->left && !root->right)
		return;

	if (root->left)
	{
		FreeBvh(root->left);
		delete root->left;
	}

	if (root->right)
	{
		FreeBvh(root->right);
		delete root->right;
	}
}

void 
Bvh::GetAllNodeBounds( std::vector<DirectX::BoundingBox>& aabbsOut )
{
	assert(aabbsOut.empty());

	if (mRoot) 
		GetAllBoundsRecursively(mRoot, aabbsOut);
}

void 
Bvh::GetAllBoundsRecursively( BvhNode* root, std::vector<DirectX::BoundingBox>& aabbsOut )
{
	aabbsOut.push_back(root->aabb);

	// If leaf, return
	if (!root->left && !root->right)
		return;

	if (root->left)
		GetAllBoundsRecursively(root->left, aabbsOut);

	if (root->right)
		GetAllBoundsRecursively(root->right, aabbsOut);
}

void 
Bvh::CleanNonLeafNodesRecursively( BvhNode* root )
{
	// If leaf, return
	if (!root->left && !root->right)
		return;
	else
	{
		// Clean data out
		std::vector<BvhData*> empty;
		root->data.clear();
		std::swap(root->data, empty);
	}

	if (root->left)
		CleanNonLeafNodesRecursively(root->left);

	if (root->right)
		CleanNonLeafNodesRecursively(root->right);
}

void
Bvh::Intersect( DirectX::BoundingFrustum const& frustum, std::vector<BvhData*>& intersectedOut )
{
	IntersectRecursively(mRoot, frustum, intersectedOut);
}

void 
Bvh::IntersectRecursively( BvhNode* root, DirectX::BoundingFrustum const& frustum, std::vector<BvhData*>& intersectedOut )
{
	// If leaf, populate intersectedOut
	if ( !root->left && !root->right )
	{
		for (unsigned int i = 0; i < root->data.size(); i++)
			intersectedOut.push_back(root->data[i]);
		return;
	}
	
	// Check intersection and recurse
	if ( frustum.Contains(root->left->aabb) != DirectX::DISJOINT )
		IntersectRecursively(root->left, frustum, intersectedOut);

	if ( frustum.Contains(root->right->aabb) != DirectX::DISJOINT )
		IntersectRecursively(root->right, frustum, intersectedOut);
}
