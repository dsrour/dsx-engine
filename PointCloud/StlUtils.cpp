#include "StlUtils.h"

bool 
CompareSecondPairElementBvhData( std::pair<Bvh::BvhData*, float> first, std::pair<Bvh::BvhData*, float> second )
{
	return first.second < second.second;
}

bool 
CompareSecondPairElementPcEntity( std::pair<PcEntity*, float> first, std::pair<PcEntity*, float> second )
{
	return first.second < second.second;
}

bool 
CompareSecondPairElementUint( std::pair<unsigned int, float> first, std::pair<unsigned int, float> second )
{
	return first.second < second.second;
}
