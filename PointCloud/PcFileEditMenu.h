#pragma once
#pragma once

#pragma warning(disable:4244)

#include <Windows.h>
#include <set>
#include <liblas/header.hpp>
#include "PointCloudRenderable.h"
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"

#define MAX_FRAMES 10

class PointCloudFileEditMenu
{
public:
	PointCloudFileEditMenu(void);
	~PointCloudFileEditMenu(void);

	void 
	Update(void);

private:	
	static void TW_CALL
	CreateCloud(void* /*clientData*/);	

	static void TW_CALL
	ClearClouds(void* /*clientData*/);	

	static void TW_CALL
	ExportClouds(void* /*clientData*/);
	
	static TwBar*					mPointCloudMenuTwBar;
	static std::set<unsigned int>   mCreatedPointCloudFrames;

	static bool mSelectedFrames[MAX_FRAMES];
	static unsigned int mSelectedFramesEntityId[MAX_FRAMES];

	static unsigned int mTotalFrames;

	static float mPosX;
	static float mPosY;
	static float mPosZ;
	static float mRotX;
	static float mRotY;
	static float mRotZ;

	static liblas::Header mLasHeader;

	static std::vector<PointCloudRenderable::Point> mFrames[MAX_FRAMES];
};