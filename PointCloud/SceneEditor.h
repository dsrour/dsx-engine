#pragma once

#include <set>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "Components/Renderable.h"
#include "KeyboardActions.h"
#include "MouseActions.h"
#include "StateManager.h"
#include "EventManager/EventManager.h"
#include "PostProcessPipeline.h" 
#include "PcSceneManager.h"

// Menus
#include "SceneEditorMenu.h"
#include "PcSystemMenu.h"
#include "PcFileEditMenu.h"
#include "PcAnimationMenu.h"
#include "TemporalAnalysisMenu.h"

#include "BaseApp.h"

class ApplicationKeyboardActions;
class ApplicationMouseActions;

class SceneEditor :
    public BaseApp
{
public:
    SceneEditor(void);
    ~SceneEditor(void);

    bool const
    OnLoadApp(void);

    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);

    void
    OnUnloadApp(void);		

	StateManager* const
	StateMngr(void) const {return mStateManager;}

	float const& 
	NavSpeedFactor() const { return mNavSpeedFactor; }

	void 
	NavSpeedFactor(float const& val) { mNavSpeedFactor = val; }

	EventManager* const
	EventMnger(void) const { return mEventManager; }

	PostProcessPipeline* const
	PostProcPipelineRef(void) { return mPostProcPipeline.get(); }

	
private:   
	float	mNavSpeedFactor;

	std::shared_ptr<ApplicationKeyboardActions> mKeyboardActions;		
	std::shared_ptr<ApplicationMouseActions> mMouseActions;	

	std::shared_ptr<PostProcessPipeline> mPostProcPipeline;

	StateManager*		mStateManager;
	EventManager*		mEventManager;
	std::shared_ptr<PcSceneManager>      mPcSceneMngr;
		
	// Menus
	SceneEditorMenu*	mSceneEditorMenu;	
	TemporalAnalysisMenu* mTemporalAnalysisMenu;
	PointCloudFileEditMenu* mFileEditMenu;
	PointCloudAnimationMenu* mAnimationMenu;
	PcSystemMenu* mPcSysMenu;
};

