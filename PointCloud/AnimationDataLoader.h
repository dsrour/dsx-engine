#pragma once
#include <map>
#include <string>

class AnimationDataLoader
{
public:
	AnimationDataLoader(void);
	~AnimationDataLoader(void);

	static std::map< unsigned int,  std::map<unsigned int, unsigned int> >
	LoadAnimationData(std::string const& file);
};

