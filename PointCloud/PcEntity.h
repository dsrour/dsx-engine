#pragma once

#include <vector>
#include "Components/Spatialized.h"
#include "PointCloudRenderable.h"

class PcEntity
{
public:
	PcEntity(std::vector<PointCloudRenderable::Point>& points);
	~PcEntity(void);

	void
	AttachRenderable(void);

	void
	DetachRenderable(void);	

	void
	SetNumPointsToRender(unsigned int const numPoints);

	DirectX::BoundingBox const&
	Aabb() const { return mAabb; }

	unsigned int const&
	TotalPoints(void) { return mTotalPoints; }

	std::shared_ptr<Spatialized>&
	SpatializedComponent(void) { return mSpatialized; }

private:
	void
	Init(std::vector<PointCloudRenderable::Point>& points);

	DirectX::BoundingBox mAabb;

	unsigned int mTotalPoints;
	
	unsigned int mEntityId;
	std::shared_ptr<PointCloudRenderable> mRenderable;
	std::shared_ptr<Spatialized> mSpatialized;

	bool mRenderableAttached;
};

