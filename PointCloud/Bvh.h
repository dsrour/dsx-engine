#pragma once
#include <string>
#include <vector>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>

class Bvh
{
public:
	struct BvhData
	{
		DirectX::BoundingBox aabb;
	};

	Bvh(void);
	Bvh(unsigned int const& maxDataPerLeafNode);
	~Bvh(void);

	void
	Intersect(DirectX::BoundingFrustum const& frustum, std::vector<BvhData*>& intersectedOut);

	unsigned int 
	MaxDataPerLeafNode() const { return mMaxDataPerLeafNode; }

	void
	Build( std::vector<BvhData*>& bvhData );

	void
	GetAllNodeBounds( std::vector<DirectX::BoundingBox>& aabbsOut );

private:
	enum Axis {X, Y, Z};

	struct BvhNode
	{	
		BvhNode() { left = right = NULL; }
			
		DirectX::BoundingBox aabb;
		BvhNode* left;  
		BvhNode* right;		
		std::vector<BvhData*> data; // this is used while building tree but will be empty if non leaf-node
	};

	void
	GetAllBoundsRecursively( BvhNode* root, std::vector<DirectX::BoundingBox>& aabbsOut );

	DirectX::BoundingBox
	FindFittingAabb( std::vector<BvhData*>& bvhData );

	void
	BuildRecursively( BvhNode* root );

	void 
	SortByAxis(std::vector<BvhData*>& inData, std::vector<BvhData*>& outData, Axis const& axis);

	void 
	FindBestSplitFromOrderedArray( BvhNode* root, std::vector<BvhData*>& orderedArrayIn, float& minCostOut, BvhNode& leftChildOut, BvhNode& rightChildOut );

	float 
	SahCost(BvhNode const* const root, BvhNode const* const tmpLeft, BvhNode const* const tmpRight);

	float 
	SahCost(BvhNode const* const root, DirectX::BoundingBox const& leftAabb, DirectX::BoundingBox const& rightAabb, int const& leftSize, int const& rightSize);

	void
	FreeBvh(BvhNode* root);

	void
	CleanNonLeafNodesRecursively( BvhNode* root );

	void
	IntersectRecursively(BvhNode* root, DirectX::BoundingFrustum const& frustum, std::vector<BvhData*>& intersectedOut);


	BvhNode* mRoot;
	unsigned int mMaxDataPerLeafNode;
};


