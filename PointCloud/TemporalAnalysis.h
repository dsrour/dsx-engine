#pragma once

#include <vector>
#include <ANN/ANN.h>

class PcSceneManager;
class Frame;

class TemporalAnalysis
{
public:
	TemporalAnalysis(PcSceneManager* pcSceneMngr);
	~TemporalAnalysis(void);

	void
	AnalyzeFrames(void);

	void
	PointsWeight(float const weight) { mPntsWeight = weight; }

	void
	AvgColorWeight(float const weight) { mAvgColorWeight = weight; } 

	void
	DensityWeight(float const weight) { mDensityWeight = weight; } 

	void
	Samples(unsigned int const samples) { mSamples = samples; }

	void
	RadiusSections(unsigned int const sections) { mRadiusSections = sections; }

	void
	MinRadius(float const minRadius) { mMinRadius = minRadius; }

	void
	MaxRadius(float const maxRadius) { mMaxRadius = maxRadius; }

	void 
	MaxNearestNeighborsReturn(unsigned int const maxNeighbors) { mMaxNeighbors = maxNeighbors; }

	void
	CleanUp(void);

private:
	struct PointFeatureHistory
	{
		DirectX::XMFLOAT3 origin;
		std::map<float, unsigned int> radiusToNumNeighbors;
		std::map<float, float> radiusToAvgDistance;
	};

	void
	GetBucketIndicesForFrame(unsigned int const frameNum, std::set<unsigned int>& indicesOut);

	void
	FindAppearingBuckets(std::set<unsigned int>& frameN, std::set<unsigned int>& frameNPlus1, std::set<unsigned int>& appearingBucketsOut);

	void
	FindDisappearingBuckets(std::set<unsigned int>& frameN, std::set<unsigned int>& frameNPlus1, std::set<unsigned int>& disappearingBucketsOut);

	void
	FindChangingBuckets(unsigned int const frameNNumber, std::set<unsigned int>& frameN, 
						unsigned int const frameNPlus1Number, std::set<unsigned int>& frameNPlus1, 
						std::set<unsigned int>& changingBucketsOut);

	void
	CreateDebugEntitiesForAppearingBuckets(unsigned int const frameNum);

	void
	CreateDebugEntitiesForDisappearingBuckets(unsigned int const frameNum);

	void
	CreateDebugEntitiesForChangingBuckets(unsigned int const frameNum);
	
	void 
	FeatureRecognition( void );

	ANNkd_tree*
	InitFeatureRecognitionKdStructures( ANNpointArray& points, unsigned int const frame );

	void
	OrderAffectedBucketsByDistance(unsigned int const frame, unsigned int const originBucket, std::vector<unsigned int>& orderedOut);

	/// Samples for points in the specified bucket and calculates a point feature history...
	void
	SamplePointFeatureHistory(  unsigned int const frame, unsigned int const bucket, 
								ANNpointArray const& points, ANNkd_tree* const kdTree, 
								ANNpoint& queryPnt, ANNidxArray& nearestNeighbors,
								ANNdistArray& sqrdDists, std::vector<PointFeatureHistory>& pntHistoryOut );

	PcSceneManager* mPcSceneManager;
	float mPntsWeight, mDensityWeight, mAvgColorWeight;
	unsigned int mSamples;
	unsigned int mRadiusSections;
	float mMinRadius, mMaxRadius;
	unsigned int mMaxNeighbors;
};

