#pragma once

#pragma warning(disable:4244)

#include <map>
#include "PointCloudRenderable.h"


class AnimationDataWriter
{
public:
	AnimationDataWriter() {}

	static void
	WriteAnimationData(std::string const& file, std::map< unsigned int,  std::map<unsigned int, unsigned int> > data);
};

