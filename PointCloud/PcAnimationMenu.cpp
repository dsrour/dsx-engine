#pragma warning(disable:4244)

#include <cstddef>

#include "Debug/Debug.h"
#include "BaseApp.h"
#include "EventsMetadata.h"
#include "Utils/CommonWinDialogs.h"

#include "AnimationDataWriter.h"
#include "AnimationDataLoader.h"
#include "AnimationMarkerWriter.h"
#include "AnimationMarkerLoader.h"

#include "PcAnimationMenu.h"

extern BaseApp* gApp;

TwBar* PointCloudAnimationMenu::mPointCloudMenuTwBar;
PcSceneManager* PointCloudAnimationMenu::mPcSceneMngr;
std::map< unsigned int,  std::map<unsigned int, unsigned int> > PointCloudAnimationMenu::mAnimationData;
std::set<double> PointCloudAnimationMenu::mMarkers;

unsigned int  PointCloudAnimationMenu::mAnimBoundsEntityId;
bool PointCloudAnimationMenu::mShowAnimBounds = false;

PointCloudAnimationMenu::PointCloudAnimationMenu(PcSceneManager* const pcSceneMngr, EventManager* const eventMngr)
{	
	mFromBucketId = mToBucketId = -1;
	mTmpPair.first = mTmpPair.second = -1;

	mPcSceneMngr = pcSceneMngr;
	mEventMngr = eventMngr;

	mAnimBoundsEntityId = gApp->EntityMngr()->CreateEntity();

	mPointCloudMenuTwBar = TwNewBar("Point Cloud Animation");
	TwAddButton(mPointCloudMenuTwBar, "Clear Animation Data", Clear, NULL, " label='Clear Animation Data' ");
	TwAddButton(mPointCloudMenuTwBar, "Import Animation Data", Import, NULL, " label='Import Animation Data' ");
	TwAddButton(mPointCloudMenuTwBar, "Export Animation Data", Export, NULL, " label='Export Animation Data' ");
	TwAddButton(mPointCloudMenuTwBar, "Toggle Animation Data Visuals", ToggleAnimBounds, NULL, " label='Toggle Animation Data Visuals' ");
		
	//TwDefine(" 'Point Cloud Animation' iconified=true ");
}


PointCloudAnimationMenu::~PointCloudAnimationMenu(void)
{
}

void 
TW_CALL PointCloudAnimationMenu::Clear( void* /*clientData*/ )
{		
	mAnimationData.clear();
	mMarkers.clear();
	mPcSceneMngr->ClearAnimation();
}

void 
PointCloudAnimationMenu::Update( void )
{	
	if (gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_F1) || 
		gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_F2))
	{
		int intersected_bucket_id;
		DirectX::XMFLOAT3 center;
		mPcSceneMngr->IntersectMouseRayWithCurrentFrameBuckets(intersected_bucket_id, center);

		if (intersected_bucket_id >= 0)
		{
			if ( gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_F1) )
			{
				mTmpPair.first = intersected_bucket_id;
				ShowFromBucket(center);
			}
			else if ( gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_F2) )
			{
				ShowToBucket(center);
				mTmpPair.second = intersected_bucket_id;
			}

			ShowIntersectedBucketsNotification( DirectX::XMFLOAT3(1.f, 1.f, 1.f) );
		}	
	}	

	if (gApp->InputDeviceMngr()->Keyboard()->IsKeyPressed(VK_F3))
	{
		int cur_frame = mPcSceneMngr->CurrentFrame();

		if (cur_frame >= 0 && mTmpPair.first >= 0 && mTmpPair.second >= 0)
		{			
			ShowIntersectedBucketsNotification( DirectX::XMFLOAT3(0.f, 1.f, 0.f) );
			mAnimationData[cur_frame][mTmpPair.first] = mTmpPair.second;
			mTmpPair.first = mTmpPair.second = -1;
			HideFromBucket();
			HideToBucket();
		}		
	}
}

void 
PointCloudAnimationMenu::ShowFromBucket( DirectX::XMFLOAT3 center )
{
	HideFromBucket();
	std::vector<DirectX::XMFLOAT3> centers; centers.push_back(center);
	mFromBucketRenderable = std::make_shared<FilledBucketRenderable>(mPcSceneMngr->BucketSize()-0.01f, centers, DirectX::XMFLOAT3(1.f, 1.f, 1.f));
	mFromBucketId = gApp->EntityMngr()->CreateEntity();
	gApp->EntityMngr()->AddComponentToEntity(mFromBucketId, mFromBucketRenderable);
}

void 
PointCloudAnimationMenu::ShowToBucket( DirectX::XMFLOAT3 center )
{
	HideToBucket();
	std::vector<DirectX::XMFLOAT3> centers; centers.push_back(center);
	mToBucketRenderable = std::make_shared<FilledBucketRenderable>(mPcSceneMngr->BucketSize() - 0.01f, centers, DirectX::XMFLOAT3(0.1f, 0.8f, 0.8f));
	mToBucketId = gApp->EntityMngr()->CreateEntity();
	gApp->EntityMngr()->AddComponentToEntity(mToBucketId, mToBucketRenderable);
}

void 
PointCloudAnimationMenu::HideFromBucket( void )
{
	if (mFromBucketId >= 0)
	{
		gApp->EntityMngr()->DestroyEntity(mFromBucketId);
		mFromBucketId = -1;
	}
}

void 
PointCloudAnimationMenu::HideToBucket( void )
{
	if (mToBucketId >= 0)
	{
		gApp->EntityMngr()->DestroyEntity(mToBucketId);
		mToBucketId = -1;
	}
}

void 
PointCloudAnimationMenu::ShowIntersectedBucketsNotification( DirectX::XMFLOAT3& color )
{
	std::wstring msg = L"Frame ";
	msg += std::to_wstring(mPcSceneMngr->CurrentFrame());
	msg += L": ";
	msg += std::to_wstring(mTmpPair.first);
	msg += L" -> ";
	msg += std::to_wstring(mTmpPair.second);

	Notification notification;
	notification.notification = msg;	
	notification.color = color;
	notification.durationSec = 2.0;
	mEventMngr->BroadcastEvent("NOTIFICATION", &notification);	
}

void 
TW_CALL PointCloudAnimationMenu::Export( void* /*clientData*/ )
{
	std::wstring file = BasicFileSave();
	std::size_t pos = file.find_last_of(L".");
	if ( pos != std::wstring::npos )
		file = file.substr(0, pos);

	AnimationDataWriter anim_wr;
	anim_wr.WriteAnimationData(std::string(file.begin(), file.end())+".anim", mAnimationData);

	// The marker data isn't customizable from the application itself.  It just saves them at constant intervals;
	double const time_interval = 3.0;
	
	double cur_time = 0.0;
	for (auto fr_iter = mAnimationData.begin(); fr_iter != mAnimationData.end(); ++fr_iter)
	{
		mMarkers.insert(cur_time);
		cur_time += time_interval;
	}
	mMarkers.insert(cur_time);

	AnimationMarkerWriter marker_wr;
	marker_wr.WriteMarkerData(std::string(file.begin(), file.end())+".markers", mMarkers);
}

void TW_CALL 
PointCloudAnimationMenu::Import( void* /*clientData*/ )
{
	std::wstring file = BasicFileOpen();
	std::size_t pos = file.find_last_of(L".");
	if ( pos != std::wstring::npos )
		file = file.substr(0, pos);

	AnimationDataLoader anim_ldr;
	mAnimationData = anim_ldr.LoadAnimationData( std::string(file.begin(), file.end())+".anim" );

	AnimationMarkerLoader marker_ldr;
	mMarkers = marker_ldr.LoadMarkerData( std::string(file.begin(), file.end())+".markers" );

	mPcSceneMngr->InitializeAnimation(mAnimationData, mMarkers);

	assert(mMarkers.size() == mAnimationData.size()+1);
}

void TW_CALL 
PointCloudAnimationMenu::ToggleAnimBounds(void* /*clientData*/)
{
	mShowAnimBounds = !mShowAnimBounds;

	if (mShowAnimBounds)
	{
		std::vector<DirectX::XMFLOAT3> centers;
		for (auto frame_iter = mAnimationData.begin(); frame_iter != mAnimationData.end(); ++frame_iter)
		{
			unsigned int cur_frame = frame_iter->first;

			for (auto buckets_iter = mAnimationData[cur_frame].begin(); buckets_iter != mAnimationData[cur_frame].end(); ++buckets_iter)
			{
				centers.push_back(mPcSceneMngr->GetBucket(cur_frame, buckets_iter->first).aabb.Center);
				centers.push_back(mPcSceneMngr->GetBucket(cur_frame+1, buckets_iter->second).aabb.Center);
			}
		}

		std::shared_ptr<BucketDebugRenderable> buckets(new BucketDebugRenderable(mPcSceneMngr->BucketSize() - 0.01f, centers, DirectX::XMFLOAT3(0.0f, 0.1f, 0.1f)));
		gApp->EntityMngr()->AddComponentToEntity(mAnimBoundsEntityId, buckets);
	}
	else
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(mAnimBoundsEntityId, BucketDebugRenderable::GetGuid());
	}
}
