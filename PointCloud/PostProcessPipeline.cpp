#include "Settings.h"
#include "PostProcessPipeline.h"

PostProcessPipeline::PostProcessPipeline( std::shared_ptr<D3dRenderer> renderer, EventManager* const eventMngr ) : Pipeline(renderer)
{
	mEventManager = eventMngr;
	mEventManager->SubscribeToEvent("POINTS_STATS", this);
	mEventManager->SubscribeToEvent("NOTIFICATION", this);
	mEventManager->SubscribeToEvent("TIMELINE_STATS", this);
	
	mFpsOn = mPntsStatsOn = mAnimTimelineOn = false;

	mPointStats.renderedNumPoints = mPointStats.totalNumPoints = 0;

	mSpriteBatch = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());

	std::wstring font_path = gResourcesDir + L"\\Fonts\\font.spritefont";
	mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (WCHAR*)font_path.c_str());
}

PostProcessPipeline::~PostProcessPipeline( void )
{
	if (mSpriteFont)
		delete mSpriteFont;

	if (mSpriteBatch)
		delete mSpriteBatch;
}

void 
PostProcessPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	if (mFpsOn)
	{
		// Since this gets called every time we render... we just see how many frames we get per second
		static double elapsed = gApp->Timer().ElapsedTimeSecs();
		static unsigned int frames = 0;			
		frames++;

		static std::wstring fps_str = L"0 FPS";

		if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
		{
			std::wstringstream ss;   
			ss << frames; 
			fps_str = ss.str() + L" FPS";
			frames = 0;
			elapsed = gApp->Timer().ElapsedTimeSecs();
		}						
		mSpriteBatch->Begin();
		mSpriteFont->DrawString(mSpriteBatch, fps_str.c_str(), DirectX::XMFLOAT2(5.f, 5.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
		mSpriteBatch->End();
	}

	if (mPntsStatsOn)
	{
		std::wstring str;

		std::wstringstream ss;   
		ss << mPointStats.totalNumPoints; 
		str = L"Total Points: " + ss.str() + L"\n";
		
		ss.clear();
		ss.str(L"");

		ss << mPointStats.renderedNumPoints; 
		str += L"Rendering:  " + ss.str();
					
		mSpriteBatch->Begin();
		mSpriteFont->DrawString(mSpriteBatch, str.c_str(), DirectX::XMFLOAT2(5.f, 30.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
		mSpriteBatch->End();
	}

	if (mAnimTimelineOn)
	{
		std::wstring str;
		if (mAnimStats.playing)
			str += L"Playing\n";
		else
			str += L"Paused\n";

		str += L"Frame: " + std::to_wstring(mAnimStats.currentFrame) + L"/" + std::to_wstring(mAnimStats.totalFrames) + L"\n";
		str += std::to_wstring(mAnimStats.playSpeedFactor) + L"X" + L"        " + std::to_wstring(mAnimStats.currentPos);
		

		mSpriteBatch->Begin();
		mSpriteFont->DrawString(mSpriteBatch, str.c_str(), DirectX::XMFLOAT2(5.f, 80.f), DirectX::XMVectorSet(0.75f,0.75f,0.75f,1.f));
		mSpriteBatch->End();
	}

	if (mCurrNotification.durationSec > 0.f)
	{
		mCurrNotification.durationSec -= (gApp->Timer().ElapsedTimeSecs() - mNewNotificationTime);
		mNewNotificationTime = gApp->Timer().ElapsedTimeSecs();

		mSpriteBatch->Begin();
		mSpriteFont->DrawString(mSpriteBatch, 
								mCurrNotification.notification.c_str(), 
								DirectX::XMFLOAT2(20.f, gApp->WinHeight() - 35.f), 
								DirectX::XMLoadFloat3(&mCurrNotification.color));
		mSpriteBatch->End();
	}
}

void 
PostProcessPipeline::OnEvent( std::string const& event, Metadata* const metadata )
{
	if ("POINTS_STATS" == event)
	{
		mPointStats.renderedNumPoints = static_cast<PointStats const* const>(metadata)->renderedNumPoints;
		mPointStats.totalNumPoints = static_cast<PointStats const* const>(metadata)->totalNumPoints;
	}

	if ("NOTIFICATION" == event)
	{
		mCurrNotification.color = static_cast<Notification const* const>(metadata)->color;
		mCurrNotification.durationSec = static_cast<Notification const* const>(metadata)->durationSec;
		mCurrNotification.notification = static_cast<Notification const* const>(metadata)->notification;
		mNewNotificationTime = gApp->Timer().ElapsedTimeSecs();
	}

	if("TIMELINE_STATS" == event)
	{
		mAnimStats.currentFrame = static_cast<AnimationTimelineStats const* const>(metadata)->currentFrame;
		mAnimStats.currentPos = static_cast<AnimationTimelineStats const* const>(metadata)->currentPos;		
		mAnimStats.playing = static_cast<AnimationTimelineStats const* const>(metadata)->playing;
		mAnimStats.playSpeedFactor = static_cast<AnimationTimelineStats const* const>(metadata)->playSpeedFactor;
		mAnimStats.totalFrames = static_cast<AnimationTimelineStats const* const>(metadata)->totalFrames;
	}
}
