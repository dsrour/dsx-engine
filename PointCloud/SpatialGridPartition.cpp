#include <assert.h>
#include "Debug/Debug.h"
#include "SpatialGridPartition.h"


SpatialGridPartition::SpatialGridPartition(DirectX::XMFLOAT3 const worldMin, DirectX::XMFLOAT3 const worldMax, float const gridCellSize)
	: mWorldMin(worldMin), mWorldMax(worldMax), mCellSize(gridCellSize)
{
	// Compute values we'll be needing
	mWorldWidth = mWorldMax.x - mWorldMin.x;
	mWorldHeight = mWorldMax.y - mWorldMin.y;
	mWorldDepth = mWorldMax.z - mWorldMin.z;

	mNumCellsX = (unsigned int)std::ceilf(mWorldWidth / mCellSize);
	mNumCellsY = (unsigned int)std::ceilf(mWorldHeight / mCellSize);
	mNumCellsZ = (unsigned int)std::ceilf(mWorldDepth / mCellSize);
}


SpatialGridPartition::~SpatialGridPartition(void)
{
}

unsigned int const
SpatialGridPartition::IndexFromXyz( DirectX::XMFLOAT3 const xyz )
{
	assert( xyz.x >= mWorldMin.x && xyz.x <= mWorldMax.x &&
		    xyz.y >= mWorldMin.y && xyz.y <= mWorldMax.y &&
			xyz.z >= mWorldMin.z && xyz.z <= mWorldMax.z );

	// xyz to ijk
	unsigned int i = (unsigned int)std::floorf( (xyz.x - mWorldMin.x) / mCellSize );
	unsigned int j = (unsigned int)std::floorf( (xyz.y - mWorldMin.y) / mCellSize );
	unsigned int k = (unsigned int)std::floorf( (xyz.z - mWorldMin.z) / mCellSize );

	// ijk to index
	unsigned int index = i + (j * mNumCellsX) + (k * mNumCellsX * mNumCellsY);
	
	return index;
}

DirectX::XMFLOAT3 const 
SpatialGridPartition::CellCenterFromIndex( unsigned int const index )
{
	// index to ijk
	unsigned int i = index % mNumCellsX;
	unsigned int j = ( index / mNumCellsX ) % mNumCellsY;
	unsigned int k = index / ( mNumCellsX * mNumCellsY );

	// center from ijk
	float half_size = mCellSize / 2.f;

	return DirectX::XMFLOAT3 (  mWorldMin.x + (i * mCellSize) + half_size,
								mWorldMin.y + (j * mCellSize) + half_size,
								mWorldMin.z + (k * mCellSize) + half_size );
}