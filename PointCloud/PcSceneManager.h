#pragma once

#include <vector>
#include <map>
#include "Components/Renderable.h"
#include "PointCloudRenderable.h"
#include "CommonRenderables.h"
#include "PcEntity.h"
#include "PcBucket.h"
#include "System.h"
#include "EventManager/EventManager.h"
#include "TemporalAnalysis.h"
#include "Timeline.h"
#include "Bvh.h"

class PcSceneManager : public System, public std::enable_shared_from_this<PcSceneManager>
{
	friend class TemporalAnalysis;  // This is a module that is only used by PcSceneManager and that is able to look at its data....
									// It is in its own class as to not clutter PcSceneManager...

public:
	PcSceneManager(EventManager* const eventMngr, unsigned int const targetNumPointsPerFrame);
	~PcSceneManager(void);

	void
	AppendPcFrame(std::vector<PointCloudRenderable::Point>& points, DirectX::XMFLOAT3 const& min, DirectX::XMFLOAT3 const& max);
		
	void
	Init(unsigned int forceNumBucketsApprox = 1000);

	void
	Reset(void);
		
	void
	ToggleBucketBounds(bool const& on);

	void
	ToggleSubPointCloudsAabbs(bool const& on);

	bool const
	ShowingBucketBounds(void) { return mShowingBuckets; }

	void
	ToggleBvhBounds(bool const& on);

	bool const
	ShowingBvhBounds(void) { return mShowingBvh; }

	bool const
	ShowingSubPcsAabbs(void) { return mShowingSubPcsAabbs; }

	int const
	CurrentFrame(void) const {return mCurrFrame;}

	void
	CurrentFrame(int const frame);

	unsigned int const
	TotalFrames(void) const { return (unsigned int)mFrames.size(); }

	float const
	BucketSize(void) const { return mBucketSize; }

	void 
	IntersectMouseRayWithCurrentFrameBuckets( int& bucketIdOut, DirectX::XMFLOAT3& bucketCenterOut );

	void
	InitializeAnimation(std::map< unsigned int,  std::map<unsigned int, unsigned int> > animData,
						std::set<double> markers);

	void
	ClearAnimation(void);

	PcBucket
	GetBucket(unsigned int const frame, unsigned int const bucketId) { return mFrames[frame].buckets[bucketId]; }


	// LOD Getters/Setters /////////////////////////////////////////
	float const
	PointsLodWeight() const { return mPointsLodWeight; }

	void 
	PointsLodWeight(float const val) { mPointsLodWeight = val; }

	float const
	AreaLodWeight() const { return mAreaLodWeight; }

	void 
	AreaLodWeight(float const val) { mAreaLodWeight = val; }

	float const
	DistanceLodWeight() const { return mDistanceLodWeight; }

	void 
	DistanceLodWeight(float const val) { mDistanceLodWeight = val; }

	unsigned int const  
	TargetNumPoints() const { return mTargetNumPoints; }

	void 
	TargetNumPoints(unsigned int const val) { mTargetNumPoints = val; }

	void
	ToggleAnimation(void);
	////////////////////////////////////////////////////////////////

	// Tmp Analysis ////////////////////////////////////////////////
	void
	AnalysisPointsWeight(float const weight) { mTmpAnalysis->PointsWeight(weight); }

	void
	AnalysisAvgColorWeight(float const weight) { mTmpAnalysis->AvgColorWeight(weight); }

	void
	AnalysisDensityWeight(float const weight) { mTmpAnalysis->DensityWeight(weight); }

	void
	AnalysisSamplesPerBucket(unsigned int const samples) { mTmpAnalysis->Samples(samples); }

	void
	AnalysisRadiusSections(unsigned int const sections) { mTmpAnalysis->RadiusSections(sections); }

	void
	AnalysisMinRadius(float const minRadius) { mTmpAnalysis->MinRadius(minRadius); }

	void
	AnalysisMaxRadius(float const maxRadius) { mTmpAnalysis->MaxRadius(maxRadius); }

	void
	AnalysisMaxNearestNeighborsReturn(unsigned int const maxNeighbors) { mTmpAnalysis->MaxNearestNeighborsReturn(maxNeighbors); }

	void
	TemporalAnalyze(void) { mShowingAnalysisResults = false; mTmpAnalysis->AnalyzeFrames(); }

	void
	ToggleAnalysisResults(bool const& on);

	bool const
	ShowingAnalysisResults(void) { return mShowingAnalysisResults; }

	bool const
	HidingNonAffectedTemporalPoints(void) { return mHideNonTemporalAffectedPoints; }

	void
	ToggleNonAffectedTemporalPoints(bool const hide) { mHideNonTemporalAffectedPoints = hide; }
	////////////////////////////////////////////////////////////////

protected:
	virtual void
	RunImplementation(std::set<unsigned int> const* /*family*/, double const /*currentTime*/);

private:
	void
	AttachBucketRenderable(unsigned int const& bucketIndex);

	void
	DetachBucketRenderable(unsigned int const& bucketIndex);

	void 
	BroadcastPointsStats(unsigned int const  total, unsigned int const rendered);

	void 
	BroadcastAnimTimelineStats(void);

	void
	OrderByDistance(std::vector<PcEntity*>& toOrder, std::vector<float>& distsOut);

	void
	OrderByScreenAabbArea(std::vector<PcEntity*>& toOrder, std::vector<float>& areasOut);

	void
	OrderByTotalPoints(std::vector<PcEntity*>& toOrder, std::vector<float>& totalPointsOut);

	unsigned int
	CalculateScreenAabbArea( DirectX::BoundingBox const& aabb );

	void
	CalculateLodWeights(std::vector<PcEntity*>& entitiesIn, std::vector<float>& weightsOut);

	void
	FreeAppendedData(void);	
	
	struct Frame
	{		
		std::map<unsigned int, PcBucket>  buckets;  // index to bucket
		std::map<Bvh::BvhData*, unsigned int> bucketIndices; // reverse lookup
		std::map<unsigned int, PcEntity*> entities; // each bucket has an entity that visually represents the point cloud within that bucket

		// Analysis data structures... these represent what happens in the next frame
		std::set<unsigned int> appearingBuckets, disappearingBuckets, changingBuckets;

		// Debugging entities... key is entity id
		std::pair<unsigned int, std::shared_ptr<GenericAabbDebugRenderable> > entitiesDebugAabbs;	   // aabbs of the sub point clouds
		std::pair<unsigned int, std::shared_ptr<BucketDebugRenderable> > bucketsDbgEntity;			   // buckets used by this frame
		std::pair<unsigned int, std::shared_ptr<BucketDebugRenderable> > appearingBucketsDbgEntity;    // buckets that will appear in the next frame
		std::pair<unsigned int, std::shared_ptr<BucketDebugRenderable> > disappearingBucketsDbgEntity; // buckets that will disappear in the next frame
		std::pair<unsigned int, std::shared_ptr<BucketDebugRenderable> > changingBucketsDbgEntity;	   // buckets that will change in the next frame
	};

	
	
	EventManager* mEventMngr;

	std::vector<PcEntity*> mRenderList;

	std::vector< Frame > mFrames;  
	std::map<Bvh::BvhData*, unsigned int> mBucketToFrameLookup;

	std::map< unsigned int,  std::map<unsigned int, unsigned int> > mAnimationData;	// frame -> from -> to
	std::shared_ptr<Timeline> mTimeline;
	unsigned int mTimelineId;

	unsigned int mTotalPoints;

	unsigned int mTargetNumPoints;
	float mDistanceLodWeight, mAreaLodWeight, mPointsLodWeight;

	// Appended data //////////////////////////////////////////////////////////////////////////
	std::vector< std::vector<PointCloudRenderable::Point> > mAppendedPoints;
	DirectX::XMFLOAT3 mAppendedMin, mAppendedMax;
	///////////////////////////////////////////////////////////////////////////////////////////

	float mBucketSize;

	Bvh* mBvh;
	std::pair<unsigned int, std::shared_ptr<GenericAabbDebugRenderable> > mBvhDbgEntity;

	bool mIsInitiated;

	int mCurrFrame;  // -1 means show all

	bool mShowingBuckets, mShowingSubPcsAabbs, mShowingBvh, mShowingAnalysisResults;

	bool mHideNonTemporalAffectedPoints;

	TemporalAnalysis* mTmpAnalysis;
};

