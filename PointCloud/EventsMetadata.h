#include <DirectXMath.h>
#include "EventManager/EventManager.h"

// Displays point statistics
struct PointStats : public Metadata
{
	unsigned int totalNumPoints;
	unsigned int renderedNumPoints;
};

// A timed notification message
struct Notification : public Metadata
{
	std::wstring		notification;
	DirectX::XMFLOAT3	color;
	double				durationSec;

	Notification() : durationSec(0.0) {}
};

// Animation playback time line
struct AnimationTimelineStats : public Metadata
{
	bool				playing;
	unsigned int		currentFrame;
	unsigned int		totalFrames;
	double				currentPos;
	unsigned int		playSpeedFactor;
	
	AnimationTimelineStats() : playing(false), currentFrame(0), totalFrames(0), currentPos(0.0), playSpeedFactor(1) {}
};