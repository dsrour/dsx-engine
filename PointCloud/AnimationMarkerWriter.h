#pragma once

#pragma warning(disable:4244)

#include <set>
#include "PointCloudRenderable.h"


class AnimationMarkerWriter
{
public:
	AnimationMarkerWriter() {}

	// Writes position of markers in time.
	static void
	WriteMarkerData(std::string const& file, std::set<double> data);
};

