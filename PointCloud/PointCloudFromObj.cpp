#include <string>
#include <vector>
#include "BaseApp.h"
#include "Scene/SceneObject.h"
#include "Scene/ObjRenderable/tiny_obj_loader.h"

#include "PointCloudFromObj.h"

extern BaseApp* gApp;

std::vector<PointCloudRenderable::Point> 
PointCloudFromObjFile( std::string const& file )
{
	std::string basepath(file);
	auto found = basepath.find_last_of("/\\");
	basepath = basepath.substr(0, found + 1);

	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, file.c_str(), basepath.c_str());
	assert(err.empty());
	
	std::vector<DirectX::XMFLOAT3> verts;
	for (unsigned int i = 0; i < shapes[0].mesh.positions.size() / 3; i++)
		verts.push_back(DirectX::XMFLOAT3(
		shapes[0].mesh.positions[3 * i + 0],
		shapes[0].mesh.positions[3 * i + 1],
		shapes[0].mesh.positions[3 * i + 2]));

	std::vector<PointCloudRenderable::Point>  ret;
	for (auto vert = verts.begin(); vert != verts.end(); ++vert)
	{
		PointCloudRenderable::Point pnt;
		pnt.xyz = *vert;
		pnt.color = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
		ret.push_back(pnt);
	}

	return ret;
}

std::vector<PointCloudRenderable::Point>
PointCloudFromObjFile( std::string const& file, float& minXOut, float& minYOut, float& minZOut, float& maxXOut, float& maxYOut, float& maxZOut )
{
	std::string basepath(file);
	auto found = basepath.find_last_of("/\\");
	basepath = basepath.substr(0, found + 1);

	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, file.c_str(), basepath.c_str());
	assert(err.empty());

	std::vector<DirectX::XMFLOAT3> verts;
	for (unsigned int i = 0; i < shapes[0].mesh.positions.size() / 3; i++)
		verts.push_back(DirectX::XMFLOAT3(
		shapes[0].mesh.positions[3 * i + 0],
		shapes[0].mesh.positions[3 * i + 1],
		shapes[0].mesh.positions[3 * i + 2]));

	float max_x = 0.f;
	float max_y = 0.f;
	float max_z = 0.f;
	float min_x = 0.f;
	float min_y = 0.f;
	float min_z = 0.f;

	std::vector<PointCloudRenderable::Point>  ret;
	for (auto vert = verts.begin(); vert != verts.end(); ++vert)
	{
		if (vert == verts.begin())
		{
			min_x = max_x = vert->x;
			min_y = max_y = vert->y;
			min_z = max_z = vert->z;
		}
		else
		{
			if (vert->x < min_x) min_x = vert->x;
			if (vert->y < min_y) min_y = vert->y;
			if (vert->z < min_z) min_z = vert->z;

			if (vert->x > max_x) max_x = vert->x;
			if (vert->y > max_y) max_y = vert->y;
			if (vert->z > max_z) max_z = vert->z;
		}		

		PointCloudRenderable::Point pnt;
		pnt.xyz = *vert;
		pnt.color = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
		ret.push_back(pnt);
	}

	minXOut = min_x;
	minYOut = min_y;
	minZOut = min_z;
	maxXOut = max_x;
	maxYOut = max_y;
	maxZOut = max_z;

	return ret;
}
