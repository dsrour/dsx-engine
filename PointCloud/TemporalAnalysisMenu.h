#pragma once
#pragma once

#pragma warning(disable:4244)

#include <Windows.h>
#include <set>
#include <liblas/header.hpp>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "PcSceneManager.h"
#include "EventManager/EventManager.h"

class TemporalAnalysisMenu : public EventSubscriber
{
public:
	TemporalAnalysisMenu(PcSceneManager* const pcSceneMngr);
	~TemporalAnalysisMenu(void);

private:
	virtual void
	OnEvent(std::string const& event, Metadata const* const metadata);

	static void TW_CALL
	ToggleAffectedBucketsDbg(void* /*clientData*/);	

	static void TW_CALL
	ToggleNonAffectedPoints(void* /*clientData*/);	

	static void TW_CALL 
	SetAvgColorWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetAvgColorWeightCallback(void* value, void*);

	static void TW_CALL 
	SetDensityWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetDensityWeightCallback(void* value, void*);

	static void TW_CALL 
	SetPntsWeightCallback(const void* value, void*);

	static void TW_CALL 
	GetPntsWeightCallback(void* value, void*);

	static void TW_CALL 
	SetSamplesPerBucketCallback(const void* value, void*);

	static void TW_CALL 
	GetSamplesPerBucketCallback(void* value, void*);

	static void TW_CALL 
	SetMinRadiusCallback(const void* value, void*);

	static void TW_CALL 
	GetMinRadiusCallback(void* value, void*);

	static void TW_CALL 
	SetMaxRadiusCallback(const void* value, void*);

	static void TW_CALL 
	GetMaxRadiusCallback(void* value, void*);

	static void TW_CALL 
	SetRadiusSectionsCallback(const void* value, void*);

	static void TW_CALL 
	GetRadiusSectionsCallback(void* value, void*);

	static void TW_CALL 
	SetMaxNearestNeighborsReturnCallback(const void* value, void*);

	static void TW_CALL 
	GetMaxNearestNeighborsReturnCallback(void* value, void*);

	static void TW_CALL 
	Analyze( void* /*clientData*/ );

	static TwBar* mTmpAnalysisTwBar;
	static PcSceneManager* mPcSceneManager;

	// Analysis variables
	static float mAvgColorWeight, mDensityWeight, mPointsWeight;
	static unsigned int mSamplesPerBucket;
	static float mMinRadius, mMaxRadius;
	static unsigned int mRadiusSections;
	static unsigned int mMaxNearestNeighborsReturn;
};