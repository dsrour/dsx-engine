#pragma once
#include <set>
#include <string>

class AnimationMarkerLoader
{
public:
	AnimationMarkerLoader(void);
	~AnimationMarkerLoader(void);

	static std::set<double>
	LoadMarkerData(std::string const& file);
};

