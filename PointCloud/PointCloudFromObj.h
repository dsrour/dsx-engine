#pragma once

#include <vector>
#include "PointCloudRenderable.h"

std::vector<PointCloudRenderable::Point>
PointCloudFromObjFile(std::string const& file);

std::vector<PointCloudRenderable::Point>
PointCloudFromObjFile(std::string const& file, 
					  float& minXOut, float& minYOut, float& minZOut,
                      float& maxXOut, float& maxYOut, float& maxZOut );