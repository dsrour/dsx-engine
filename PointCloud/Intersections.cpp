#include "Debug/Debug.h"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Intersections.h"

bool const
IntersectMouseRayEntity(unsigned int entityId, DirectX::XMVECTOR const& rayOrig, DirectX::XMVECTOR const& rayDir, float distance)
{
	std::shared_ptr<IComponent> spatialized_tmp_comp, bounded_tmp_comp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Spatialized::GetGuid(), spatialized_tmp_comp);
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Bounded::GetGuid(), bounded_tmp_comp);
	
	if (spatialized_tmp_comp && bounded_tmp_comp)
	{
		Spatialized* spatialized = static_cast<Spatialized*>(spatialized_tmp_comp.get());					
		Bounded* bounded = static_cast<Bounded*>(bounded_tmp_comp.get());					

		DirectX::XMVECTOR orig, dir;
		DirectX::XMMATRIX view = XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ViewMatrix());
		DirectX::XMMATRIX inv_view = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(view), view);

		DirectX::XMMATRIX world = XMLoadFloat4x4(&spatialized->LocalTransformation());
		DirectX::XMMATRIX inv_world = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(world), world);

		DirectX::XMMATRIX to_local = DirectX::XMMatrixMultiply(inv_view, inv_world);

		orig = DirectX::XMVector3TransformCoord(rayOrig, to_local);
		dir = DirectX::XMVector3TransformNormal(rayDir, to_local);
		dir = DirectX::XMVector3Normalize(dir);

		if (bounded->AabbBounds().Intersects(orig, dir, distance) != DirectX::DISJOINT)
			return true;
	}

	return false;
}

bool const 
MousePick( int entityToIntersect, float& distance )
{
	std::shared_ptr<InputDeviceManager> manager = gApp->InputDeviceMngr();

	// Duplicate code below... should clean up
	DirectX::XMMATRIX proj_mat = DirectX::XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ProjMatrix());
	DirectX::XMFLOAT4X4 proj_v4x4; DirectX::XMStoreFloat4x4(&proj_v4x4 ,proj_mat);

	float vx;
	float vy;

	if (manager->Mouse()->RelativeMode())
	{
		vx = 0;
		vy = 0;	
	}
	else
	{
		vx = (+2.0f * manager->Mouse()->MouseX() / gApp->WinWidth() - 1.0f) / proj_v4x4(0, 0);
		vy = (-2.0f * manager->Mouse()->MouseY() / gApp->WinHeight() + 1.0f) / proj_v4x4(1, 1);
	}

	//OutputDebugMsg(to_string(vx)+" "+to_string(vy)+"\n");

	// Ray in view space
	DirectX::XMVECTOR ray_orig = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	DirectX::XMVECTOR ray_dir = DirectX::XMVectorSet(vx, vy, 1.0f, 0.0f);

	return IntersectMouseRayEntity(entityToIntersect, ray_orig, ray_dir, distance);
}

void 
MousePick( std::set<unsigned int>& entitiesToIntersect, std::set<unsigned int>& pickedEntitiesOut, float& distance )
{
	std::shared_ptr<InputDeviceManager> manager = gApp->InputDeviceMngr();
	DirectX::XMMATRIX proj_mat = DirectX::XMLoadFloat4x4(&gApp->Renderer()->CurrentCamera()->ProjMatrix());
	DirectX::XMFLOAT4X4 proj_v4x4; DirectX::XMStoreFloat4x4(&proj_v4x4 ,proj_mat);

	float vx;
	float vy;

	if (manager->Mouse()->RelativeMode())
	{
		vx = 0;
		vy = 0;
	}
	else
	{
		vx = (+2.0f * manager->Mouse()->MouseX() / gApp->WinWidth() - 1.0f) / proj_v4x4(0, 0);
		vy = (-2.0f * manager->Mouse()->MouseY() / gApp->WinHeight() + 1.0f) / proj_v4x4(1, 1);
	}
	

	// Ray in view space
	DirectX::XMVECTOR ray_orig = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	DirectX::XMVECTOR ray_dir = DirectX::XMVectorSet(vx, vy, 1.0f, 0.0f);

	// Check for intersection on passed in entities	
	for (std::set<unsigned int>::iterator iter = entitiesToIntersect.begin();
		iter != entitiesToIntersect.end();
		iter++)
	{
		if (IntersectMouseRayEntity(*iter, ray_orig, ray_dir, distance))
			pickedEntitiesOut.insert(*iter);
	}	
}
