#pragma warning(disable:4244)

#include <fstream>  
#include "LasWriter.h"

void 
LasWriter::WritePointCloud( std::string const& file, std::vector< std::vector<PointCloudRenderable::Point>* > pcFramesOut, std::vector<DirectX::XMFLOAT4X4>& transformsIn, liblas::Header hdr )
{
	std::ofstream ofs;
	if (!liblas::Create(ofs, file.c_str()))
	{
		assert(false);
	}

	liblas::Writer writer(ofs, hdr);

	for (unsigned int i = 0; i < pcFramesOut.size(); i++)
	{
		std::vector<PointCloudRenderable::Point>::iterator iter = (*(pcFramesOut[i])).begin();
		for ( ; iter != (*(pcFramesOut[i])).end(); ++iter )
		{
			DirectX::XMFLOAT3 pos(iter->xyz.x, iter->xyz.y, iter->xyz.z);
			DirectX::XMVECTOR vec_pos = DirectX::XMVector4Transform( DirectX::XMVectorSet(iter->xyz.x, iter->xyz.y, iter->xyz.z, 1.f), DirectX::XMLoadFloat4x4(&transformsIn[i]) );
			DirectX::XMStoreFloat3(&pos, vec_pos);

			liblas::Point point(&hdr);
			point.SetCoordinates(pos.x, pos.y, pos.z);
			point.SetColor( liblas::Color(uint32_t(iter->color.x*255), uint32_t(iter->color.y*255), uint32_t(iter->color.z*255)) );

			writer.WritePoint(point);
		}
	}
}
