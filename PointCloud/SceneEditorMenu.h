#pragma once

#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"

class SceneEditorMenu
{
public:
	SceneEditorMenu(void);
	~SceneEditorMenu(void);

private:
	static bool		mToggleFps;
	static bool		mTogglePntsStats;
	static bool		mToggleAnimTimelineStats;
	static float	mNavSpeedFactor;	
	static TwBar*   mSceneEditorMenuTwBar;

	static void TW_CALL 
	SetNavFactorCallback(const void* value, void* /*clientData*/);

	static void TW_CALL 
	GetNavFactorCallback(void* value, void* /*clientData*/); 

	static void TW_CALL
	ToggleFps(void* /*clientData*/);	

	static void TW_CALL
	TogglePointsStats(void* /*clientData*/);

	static void TW_CALL
	ToggleAnimTimelineStats(void* /*clientData*/);
};

