#pragma once

#include <vector>
#include "PointCloudRenderable.h"
#include "Bvh.h"

class PcBucket : public Bvh::BvhData
{
public:
	PcBucket(void);
	~PcBucket(void);

	void
	AddPoint(PointCloudRenderable::Point const& pnt);

	unsigned int
	TotalPoints(void) { return (unsigned int)mPoints.size(); }

	std::vector<PointCloudRenderable::Point>&
	PointsRef(void) {return mPoints;}

private:
	std::vector<PointCloudRenderable::Point> mPoints;
};

