#pragma warning(disable:4244)

#include <iostream>
#include <fstream>
#include "AnimationDataWriter.h"

void 
AnimationDataWriter::WriteAnimationData( std::string const& file, std::map< unsigned int,  std::map<unsigned int, unsigned int> > data )
{
	std::ofstream f;
	f.open (file);

	for (auto fr_iter = data.begin(); fr_iter != data.end(); ++fr_iter)
	{
		f << "f" + std::to_string(fr_iter->first) + "\n";

		for (auto from_to_iter = fr_iter->second.begin(); from_to_iter != fr_iter->second.end(); ++from_to_iter)
		{
			f << std::to_string(from_to_iter->first) + " " + std::to_string(from_to_iter->second) + "\n";
		}
	}

	f.close();
}
