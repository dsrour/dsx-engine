#include <algorithm>
#include <sstream>
#include <stdlib.h>   
#include <time.h>
#include "BaseApp.h"
#include "PcSceneManager.h"
#include "SpatialGridPartition.h"
#include "StlUtils.h"
#include "EventsMetadata.h"
#include "TemporalAnalysis.h"

extern BaseApp* gApp;

TemporalAnalysis::TemporalAnalysis(PcSceneManager* pcSceneMngr)
{
	srand( (unsigned int)time(NULL) );
	mPcSceneManager = pcSceneMngr;
}


TemporalAnalysis::~TemporalAnalysis(void)
{
}

void 
TemporalAnalysis::AnalyzeFrames(void)
{
	if ( mPcSceneManager->mFrames.size() < 2)
		return;

	CleanUp();

	if (0.f == mPntsWeight && 0.f == mDensityWeight && 0.f == mAvgColorWeight)
		return;


	double start_time = gApp->Timer().ElapsedTimeSecs();

	/*  PRUNING:
	 *	- First step is to prune unchanging buckets from one frame
	 *    to the next using the member variable weights...
	 *    
	 *    There are 3 scenarios:
	 *       1)  A bucket is present in frame n+1 but not in frame n.
	 *		 2)  A bucket is present in frame n   but not in frame n+1.
	 *		 3)  A bucket is present in both frames n & n+1 but its content has changed 
	 *			 more than the threshold defined by the weights.
	 */
	for (unsigned int i = 0; i < mPcSceneManager->mFrames.size() - 1; i++)
	{
		std::set<unsigned int> frame_i_buckets;
		std::set<unsigned int> frame_i_plus_1_buckets;

		GetBucketIndicesForFrame(i, frame_i_buckets);
		GetBucketIndicesForFrame(i+1, frame_i_plus_1_buckets);

		// Scenario 1
		FindAppearingBuckets(frame_i_buckets, frame_i_plus_1_buckets, mPcSceneManager->mFrames[i].appearingBuckets);

		// Scenario 2
		FindDisappearingBuckets(frame_i_buckets, frame_i_plus_1_buckets, mPcSceneManager->mFrames[i].disappearingBuckets);

		// Scenario 3
		FindChangingBuckets(i, frame_i_buckets, i+1, frame_i_plus_1_buckets, mPcSceneManager->mFrames[i].changingBuckets);

		// Create debug entities 
		CreateDebugEntitiesForAppearingBuckets(i);
		CreateDebugEntitiesForDisappearingBuckets(i);
		CreateDebugEntitiesForChangingBuckets(i);
	}


	/*  FEATURE RECOGNITION:
	 *	- Second step is to figure out feature points and creating AABB's that will 
	 *    hold points that need to be animated.
	 */
	FeatureRecognition();


	double analysis_time = gApp->Timer().ElapsedTimeSecs() - start_time;
	std::wstringstream time_str;   
	time_str << analysis_time; 
	Notification notification;
	notification.notification = L"Analysis Time: " + time_str.str();
	notification.color = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	notification.durationSec = 5.0;
	mPcSceneManager->mEventMngr->BroadcastEvent("NOTIFICATION", &notification);
}

void 
TemporalAnalysis::GetBucketIndicesForFrame( unsigned int const frameNum, std::set<unsigned int>& indicesOut )
{
	std::map<unsigned int, PcBucket>::iterator iter = mPcSceneManager->mFrames[frameNum].buckets.begin();
	for (; iter != mPcSceneManager->mFrames[frameNum].buckets.end(); ++iter)
	{
		indicesOut.insert(iter->first);
	}
}

void 
TemporalAnalysis::FindAppearingBuckets( std::set<unsigned int>& frameN, std::set<unsigned int>& frameNPlus1, std::set<unsigned int>& appearingBucketsOut )
{
	std::set<unsigned int>::iterator iter = frameNPlus1.begin();
	for (; iter != frameNPlus1.end(); ++iter)
	{
		if (frameN.end() == frameN.find(*iter))
			appearingBucketsOut.insert(*iter);
	}
}

void 
TemporalAnalysis::FindDisappearingBuckets( std::set<unsigned int>& frameN, std::set<unsigned int>& frameNPlus1, std::set<unsigned int>& disappearingBucketsOut )
{
	std::set<unsigned int>::iterator iter = frameN.begin();
	for (; iter != frameN.end(); ++iter)
	{
		if (frameNPlus1.end() == frameNPlus1.find(*iter))
			disappearingBucketsOut.insert(*iter);
	}
}

void 
TemporalAnalysis::FindChangingBuckets( unsigned int const frameNNumber, std::set<unsigned int>& frameN, 
								       unsigned int const frameNPlus1Number, std::set<unsigned int>& frameNPlus1, 
									   std::set<unsigned int>& changingBucketsOut )
{
	if ( 0.f == mPntsWeight &&
		 0.f == mDensityWeight &&
		 0.f == mAvgColorWeight )
		return;

	// Need the intersection of the 2 frames first
	std::set<unsigned int> intersection;
	std::set_intersection(frameN.begin(), frameN.end(), frameNPlus1.begin(), frameNPlus1.end(), 
						  std::inserter(intersection, intersection.end()));

	// See if threshold is met and add to changingBucketsOut
	std::set<unsigned int>::iterator iter = intersection.begin();

	for (; iter != intersection.end(); ++iter)
	{
		bool pnts_diff_above_threshold = false;
		bool density_diff_above_threshold = false;
		bool avg_color_above_threshold = false;

		if (0.f == mPntsWeight)
			pnts_diff_above_threshold = true;
		else
		{
			unsigned int frame_i_pnts = (unsigned int)mPcSceneManager->mFrames[frameNNumber].buckets[*iter].PointsRef().size();
			unsigned int frame_i_plus_1_pnts = (unsigned int)mPcSceneManager->mFrames[frameNPlus1Number].buckets[*iter].PointsRef().size();

			float plus_minus = frame_i_pnts * mPntsWeight;

			if (frame_i_plus_1_pnts <= frame_i_pnts - plus_minus ||
				frame_i_plus_1_pnts >= frame_i_pnts + plus_minus  )
				pnts_diff_above_threshold = true;
		}

		if (0.f == mAvgColorWeight)
			avg_color_above_threshold = true;
		else
		{
			// TODO:: might wanna do a better scheme than just a regular RGB distance calculation
			// Look into other color spaces and DelatE for noticeable changes between the two avg colors
			
			std::vector<PointCloudRenderable::Point>::iterator pnts_iter;

			float frame_i_avg_r = 0.f;
			float frame_i_avg_g = 0.f;
			float frame_i_avg_b = 0.f;

			if (!mPcSceneManager->mFrames[frameNNumber].buckets[*iter].PointsRef().empty())
			{
				for (pnts_iter = mPcSceneManager->mFrames[frameNNumber].buckets[*iter].PointsRef().begin();
					pnts_iter != mPcSceneManager->mFrames[frameNNumber].buckets[*iter].PointsRef().end();
					++pnts_iter)
				{
					frame_i_avg_r += pnts_iter->color.x;
					frame_i_avg_g += pnts_iter->color.y;
					frame_i_avg_b += pnts_iter->color.z;
				}

				unsigned int pnts_size = (unsigned int)mPcSceneManager->mFrames[frameNNumber].buckets[*iter].PointsRef().size();
				frame_i_avg_r /= pnts_size;
				frame_i_avg_g /= pnts_size;
				frame_i_avg_b /= pnts_size;
			}


			
			float frame_i_plus_1_avg_r = 0.f;
			float frame_i_plus_1_avg_g = 0.f;
			float frame_i_plus_1_avg_b = 0.f;

			if (!mPcSceneManager->mFrames[frameNPlus1Number].buckets[*iter].PointsRef().empty())
			{
				for (pnts_iter = mPcSceneManager->mFrames[frameNPlus1Number].buckets[*iter].PointsRef().begin();
					pnts_iter != mPcSceneManager->mFrames[frameNPlus1Number].buckets[*iter].PointsRef().end();
					++pnts_iter)
				{
					frame_i_plus_1_avg_r += pnts_iter->color.x;
					frame_i_plus_1_avg_g += pnts_iter->color.y;
					frame_i_plus_1_avg_b += pnts_iter->color.z;
				}

				unsigned int pnts_size = (unsigned int)mPcSceneManager->mFrames[frameNPlus1Number].buckets[*iter].PointsRef().size();
				frame_i_plus_1_avg_r /= pnts_size;
				frame_i_plus_1_avg_g /= pnts_size;
				frame_i_plus_1_avg_b /= pnts_size;
			}
			


			float frame_i_dist = frame_i_avg_r * frame_i_avg_r +
				                 frame_i_avg_g * frame_i_avg_g +
								 frame_i_avg_b * frame_i_avg_b;

			float frame_i_plus_1_dist = frame_i_plus_1_avg_r * frame_i_plus_1_avg_r +
										frame_i_plus_1_avg_g * frame_i_plus_1_avg_g +
										frame_i_plus_1_avg_b * frame_i_plus_1_avg_b;

			float plus_minus = frame_i_dist * mAvgColorWeight;

			if (frame_i_plus_1_dist <= frame_i_dist - plus_minus ||
				frame_i_plus_1_dist >= frame_i_dist + plus_minus  )
				avg_color_above_threshold = true;
		}

		if (0.f == mDensityWeight)
			density_diff_above_threshold = true;
		else
		{
			float bucket_size = mPcSceneManager->mBucketSize;
			float bucket_volume = bucket_size * bucket_size * bucket_size;

			DirectX::BoundingBox frame_i_entity_aabb = mPcSceneManager->mFrames[frameNNumber].entities[*iter]->Aabb();
			float frame_i_volume = (frame_i_entity_aabb.Extents.x * 2.f) * (frame_i_entity_aabb.Extents.y * 2.f) * (frame_i_entity_aabb.Extents.z * 2.f);

			DirectX::BoundingBox frame_i_plus_1_entity_aabb = mPcSceneManager->mFrames[frameNPlus1Number].entities[*iter]->Aabb();
			float frame_i_plus_1_volume = (frame_i_plus_1_entity_aabb.Extents.x * 2.f) * (frame_i_plus_1_entity_aabb.Extents.y * 2.f) * (frame_i_plus_1_entity_aabb.Extents.z * 2.f);

			float frame_i_density = frame_i_volume / bucket_volume;
			float frame_i_plus_1_density = frame_i_plus_1_volume / bucket_volume;

			float plus_minus = frame_i_density * mDensityWeight;

			if (frame_i_plus_1_density <= frame_i_density - plus_minus ||
				frame_i_plus_1_density >= frame_i_density + plus_minus  )
				density_diff_above_threshold = true;
		}

		if (pnts_diff_above_threshold && avg_color_above_threshold && density_diff_above_threshold)
			changingBucketsOut.insert(*iter);
	}
}

void 
TemporalAnalysis::CleanUp( void )
{
	for (unsigned int i = 0; i < mPcSceneManager->mFrames.size() - 1; i++)
	{
		if (!mPcSceneManager->mFrames[i].appearingBuckets.empty())
		{
			gApp->EntityMngr()->DestroyEntity(mPcSceneManager->mFrames[i].appearingBucketsDbgEntity.first);
			mPcSceneManager->mFrames[i].appearingBuckets.clear();
		}

		if (!mPcSceneManager->mFrames[i].disappearingBuckets.empty())
		{
			gApp->EntityMngr()->DestroyEntity(mPcSceneManager->mFrames[i].disappearingBucketsDbgEntity.first);
			mPcSceneManager->mFrames[i].disappearingBuckets.clear();
		}

		if (!mPcSceneManager->mFrames[i].changingBuckets.empty())
		{
			gApp->EntityMngr()->DestroyEntity(mPcSceneManager->mFrames[i].changingBucketsDbgEntity.first);
			mPcSceneManager->mFrames[i].changingBuckets.clear();
		}
	}
}

void 
TemporalAnalysis::CreateDebugEntitiesForAppearingBuckets( unsigned int const frameNum )
{
	if (mPcSceneManager->mFrames[frameNum].appearingBuckets.empty())
		return;

	std::unique_ptr<SpatialGridPartition> grid( new SpatialGridPartition(mPcSceneManager->mAppendedMin, 
																		 mPcSceneManager->mAppendedMax, 
																		 mPcSceneManager->mBucketSize) );
	std::vector<DirectX::XMFLOAT3> bucket_centers;

	std::set<unsigned int>::iterator iter = mPcSceneManager->mFrames[frameNum].appearingBuckets.begin();
	for (; iter != mPcSceneManager->mFrames[frameNum].appearingBuckets.end(); ++iter)
		bucket_centers.push_back(grid->CellCenterFromIndex(*iter));

	std::shared_ptr<BucketDebugRenderable> bucket_dbg_rndrble(  new BucketDebugRenderable( mPcSceneManager->mBucketSize, 
																						   bucket_centers,
																						   DirectX::XMFLOAT3(0.f, 1.f, 0.f) )
																			  );
		
	unsigned int bucket_dbg_entity_id = gApp->EntityMngr()->CreateEntity();
	mPcSceneManager->mFrames[frameNum].appearingBucketsDbgEntity = std::pair< unsigned int, std::shared_ptr<BucketDebugRenderable> >(bucket_dbg_entity_id, bucket_dbg_rndrble);
}

void 
TemporalAnalysis::CreateDebugEntitiesForDisappearingBuckets( unsigned int const frameNum )
{
	if (mPcSceneManager->mFrames[frameNum].disappearingBuckets.empty())
		return;

	std::unique_ptr<SpatialGridPartition> grid( new SpatialGridPartition(mPcSceneManager->mAppendedMin, 
																		 mPcSceneManager->mAppendedMax, 
																		 mPcSceneManager->mBucketSize) );
	std::vector<DirectX::XMFLOAT3> bucket_centers;

	std::set<unsigned int>::iterator iter = mPcSceneManager->mFrames[frameNum].disappearingBuckets.begin();
	for (; iter != mPcSceneManager->mFrames[frameNum].disappearingBuckets.end(); ++iter)
		bucket_centers.push_back(grid->CellCenterFromIndex(*iter));

	std::shared_ptr<BucketDebugRenderable> bucket_dbg_rndrble(  new BucketDebugRenderable(  mPcSceneManager->mBucketSize, 
																							bucket_centers,
																							DirectX::XMFLOAT3(1.f, 0.f, 0.f) )
																			 );

	unsigned int bucket_dbg_entity_id = gApp->EntityMngr()->CreateEntity();
	mPcSceneManager->mFrames[frameNum].disappearingBucketsDbgEntity = std::pair< unsigned int, std::shared_ptr<BucketDebugRenderable> >(bucket_dbg_entity_id, bucket_dbg_rndrble);
}

void 
TemporalAnalysis::CreateDebugEntitiesForChangingBuckets( unsigned int const frameNum )
{
	if (mPcSceneManager->mFrames[frameNum].changingBuckets.empty())
		return;

	std::unique_ptr<SpatialGridPartition> grid( new SpatialGridPartition(mPcSceneManager->mAppendedMin, 
																		 mPcSceneManager->mAppendedMax, 
																	     mPcSceneManager->mBucketSize) );
	std::vector<DirectX::XMFLOAT3> bucket_centers;

	std::set<unsigned int>::iterator iter = mPcSceneManager->mFrames[frameNum].changingBuckets.begin();
	for (; iter != mPcSceneManager->mFrames[frameNum].changingBuckets.end(); ++iter)
		bucket_centers.push_back(grid->CellCenterFromIndex(*iter));

	std::shared_ptr<BucketDebugRenderable> bucket_dbg_rndrble(  new BucketDebugRenderable(  mPcSceneManager->mBucketSize, 
																							bucket_centers,
																							DirectX::XMFLOAT3(0.f, 0.f, 1.f) )
																			 );

	unsigned int bucket_dbg_entity_id = gApp->EntityMngr()->CreateEntity();
	mPcSceneManager->mFrames[frameNum].changingBucketsDbgEntity = std::pair< unsigned int, std::shared_ptr<BucketDebugRenderable> >(bucket_dbg_entity_id, bucket_dbg_rndrble);
}

void 
TemporalAnalysis::FeatureRecognition( void )
{
	ANNpoint query_pnt;
	ANNidxArray nn = nullptr;
	ANNdistArray sqr_dsts = nullptr;

	query_pnt = annAllocPt(3);
	nn = new ANNidx[mMaxNeighbors];
	sqr_dsts = new ANNdist[mMaxNeighbors];



	for (unsigned int i = 0; i < mPcSceneManager->mFrames.size() - 1; i++)
	{
		ANNkd_tree* frame_n_tree = nullptr;
		ANNkd_tree* frame_n_plus_1_tree = nullptr;
		ANNpointArray frame_n_points = nullptr;
		ANNpointArray frame_n_plus_1_points = nullptr;

		/////////INIT/////////////////////////////////////////////////////////////////////////
		// Frame (N) init
		frame_n_tree = InitFeatureRecognitionKdStructures(frame_n_points, i);

		if (nullptr == frame_n_tree)
			continue;

		// Frame (N+1) init
		frame_n_plus_1_tree = InitFeatureRecognitionKdStructures(frame_n_plus_1_points, i+1);

		if (nullptr == frame_n_plus_1_tree)
		{
			annDeallocPts(frame_n_points);
			delete frame_n_tree;
			continue;
		}
		//////////////////////////////////////////////////////////////////////////////////////
		

		std::set<unsigned int> frame_n_buckets;
		frame_n_buckets.insert(mPcSceneManager->mFrames[i].disappearingBuckets.begin(), mPcSceneManager->mFrames[i].disappearingBuckets.end());
		frame_n_buckets.insert(mPcSceneManager->mFrames[i].changingBuckets.begin(), mPcSceneManager->mFrames[i].changingBuckets.end());


		for ( std::set<unsigned int>::iterator iter = frame_n_buckets.begin();
			  iter != frame_n_buckets.end();
			  ++iter )
		{	
			// POINT FEATURES TO COMPARE TO
			std::vector<PointFeatureHistory> pnt_feature_history;
			SamplePointFeatureHistory(i, *iter, frame_n_points, frame_n_tree, query_pnt, nn, sqr_dsts, pnt_feature_history); 

// 			std::vector<unsigned int> frame_n_plus_1_buckets;
// 			OrderAffectedBucketsByDistance(i+1, *iter, frame_n_plus_1_buckets); // Is this really needed?  Are we gonna bail out early?
// 			
// 			for (unsigned int n = 0; n < frame_n_plus_1_buckets.size(); n++)
// 			{
// 				// Continue if we are looking at the same bucket in both frames
// 				if (*iter == frame_n_plus_1_buckets[n])
// 					continue;
// 			}
 		}
		



		// Free memory
		annDeallocPts(frame_n_points);
        annDeallocPts(frame_n_plus_1_points);
		delete frame_n_tree;
        delete frame_n_plus_1_tree;
	}


	delete [] nn;
	delete [] sqr_dsts; 
	annDeallocPt(query_pnt);

	annClose(); // Free all shared memory by the ann library
}

ANNkd_tree*
TemporalAnalysis::InitFeatureRecognitionKdStructures( ANNpointArray& points, unsigned int const frame )
{
	std::set<unsigned int>::iterator bucket_iter;

	unsigned int num_points = 0;

// 	for (bucket_iter =  mPcSceneManager->mFrames[frame].appearingBuckets.begin();
// 		bucket_iter !=  mPcSceneManager->mFrames[frame].appearingBuckets.end();
// 		++bucket_iter)
// 		num_points += mPcSceneManager->mFrames[frame].buckets[*bucket_iter].TotalPoints();

	for (bucket_iter =  mPcSceneManager->mFrames[frame].disappearingBuckets.begin();
		bucket_iter !=  mPcSceneManager->mFrames[frame].disappearingBuckets.end();
		++bucket_iter)
		num_points += mPcSceneManager->mFrames[frame].buckets[*bucket_iter].TotalPoints();

	for (bucket_iter =  mPcSceneManager->mFrames[frame].changingBuckets.begin();
		bucket_iter !=  mPcSceneManager->mFrames[frame].changingBuckets.end();
		++bucket_iter)
		num_points += mPcSceneManager->mFrames[frame].buckets[*bucket_iter].TotalPoints();

	if (0 == num_points)
		return nullptr;

	points = annAllocPts(num_points, 3);

	unsigned int index = 0;
	std::vector<PointCloudRenderable::Point>::iterator pnt_iter;

// 	for (bucket_iter =  mPcSceneManager->mFrames[frame].appearingBuckets.begin();
// 		bucket_iter !=  mPcSceneManager->mFrames[frame].appearingBuckets.end();
// 		++bucket_iter)
// 	{
// 		for (pnt_iter = mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().begin();
// 			 pnt_iter != mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().end();
// 			 ++pnt_iter)
// 		{
// 			points[index][0] = (*pnt_iter).xyz.x;
// 			points[index][1] = (*pnt_iter).xyz.y;
// 			points[index][2] = (*pnt_iter).xyz.z;
// 
// 			index++;
// 		}		
// 	}

	for (bucket_iter =  mPcSceneManager->mFrames[frame].disappearingBuckets.begin();
		bucket_iter !=  mPcSceneManager->mFrames[frame].disappearingBuckets.end();
		++bucket_iter)
	{
		for (pnt_iter = mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().begin();
			 pnt_iter != mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().end();
			 ++pnt_iter)
		{
			points[index][0] = (*pnt_iter).xyz.x;
			points[index][1] = (*pnt_iter).xyz.y;
			points[index][2] = (*pnt_iter).xyz.z;

			index++;
		}		
	}

	for (bucket_iter =  mPcSceneManager->mFrames[frame].changingBuckets.begin();
		bucket_iter !=  mPcSceneManager->mFrames[frame].changingBuckets.end();
		++bucket_iter)
	{
		for (pnt_iter = mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().begin();
			 pnt_iter != mPcSceneManager->mFrames[frame].buckets[*bucket_iter].PointsRef().end();
			 ++pnt_iter)
		{
			points[index][0] = (*pnt_iter).xyz.x;
			points[index][1] = (*pnt_iter).xyz.y;
			points[index][2] = (*pnt_iter).xyz.z;

			index++;
		}		
	}

	return new ANNkd_tree(points, num_points, 3); 
}

void 
TemporalAnalysis::OrderAffectedBucketsByDistance( unsigned int const frame, unsigned int const originBucket, std::vector<unsigned int>& orderedOut )
{
	orderedOut.clear();
	

	std::unique_ptr<SpatialGridPartition> grid( new SpatialGridPartition(mPcSceneManager->mAppendedMin, 
																		 mPcSceneManager->mAppendedMax, 
																		 mPcSceneManager->mBucketSize) );
	DirectX::XMFLOAT3 origin_bucket_pos = grid->CellCenterFromIndex(originBucket);
	std::set<unsigned int>::iterator bucket_iter;
	std::vector< std::pair<unsigned int, float> > tmp;



// 	for (bucket_iter =  mPcSceneManager->mFrames[frame].appearingBuckets.begin();
// 	     bucket_iter !=  mPcSceneManager->mFrames[frame].appearingBuckets.end();
// 	 	 ++bucket_iter)
// 	{
// 		DirectX::XMFLOAT3 pos = grid->CellCenterFromIndex(*bucket_iter);
// 		float dist_sqrd = std::powf(pos.x - origin_bucket_pos.x, 2) + std::powf(pos.y - origin_bucket_pos.y, 2) + std::powf(pos.z - origin_bucket_pos.z, 2);
// 		tmp.push_back( std::pair<unsigned int, float>(*bucket_iter, dist_sqrd) );
// 	}

	for (bucket_iter =  mPcSceneManager->mFrames[frame].disappearingBuckets.begin();
		 bucket_iter !=  mPcSceneManager->mFrames[frame].disappearingBuckets.end();
		 ++bucket_iter)
	{
		DirectX::XMFLOAT3 pos = grid->CellCenterFromIndex(*bucket_iter);
		float dist_sqrd = std::powf(pos.x - origin_bucket_pos.x, 2) + std::powf(pos.y - origin_bucket_pos.y, 2) + std::powf(pos.z - origin_bucket_pos.z, 2);
		tmp.push_back( std::pair<unsigned int, float>(*bucket_iter, dist_sqrd) );
	}

	for (bucket_iter =  mPcSceneManager->mFrames[frame].changingBuckets.begin();
		 bucket_iter !=  mPcSceneManager->mFrames[frame].changingBuckets.end();
		 ++bucket_iter)
	{
		DirectX::XMFLOAT3 pos = grid->CellCenterFromIndex(*bucket_iter);
		float dist_sqrd = std::powf(pos.x - origin_bucket_pos.x, 2) + std::powf(pos.y - origin_bucket_pos.y, 2) + std::powf(pos.z - origin_bucket_pos.z, 2);
		tmp.push_back( std::pair<unsigned int, float>(*bucket_iter, dist_sqrd) );
	}

	std::sort(tmp.begin(), tmp.end(), CompareSecondPairElementUint);

	for (unsigned int i = 0; i < tmp.size(); i++)
		orderedOut.push_back(tmp[i].first);
}

void 
TemporalAnalysis::SamplePointFeatureHistory( unsigned int const frame, unsigned int const bucket, 
											 ANNpointArray const& points, ANNkd_tree* const kdTree, 
											 ANNpoint& queryPnt, ANNidxArray& nearestNeighbors,
											 ANNdistArray& sqrdDists, std::vector<PointFeatureHistory>& pntHistoryOut )
{
	double rad_increase = (double)(mMaxRadius - mMinRadius) / (mRadiusSections - 1);

	pntHistoryOut.resize(mSamples);

	std::set<unsigned int> already_sampled;

	// Total points in specified bucket for specified frame
	unsigned int total_points = mPcSceneManager->mFrames[frame].buckets[bucket].TotalPoints();

	for (unsigned int curr_sample = 0; curr_sample < mSamples; curr_sample++)
	{
		// Is there remaining points we can sample?
		if (already_sampled.size() >= total_points)
			break;

		// Ensure there are points
		if (total_points - 1 <= 0)
			continue;

		// Point to sample ////////////
		unsigned int query_point_index = rand() % (total_points - 1);
		while ( already_sampled.find(query_point_index) != already_sampled.end() ) // already sampled.... find another point
		{
			++query_point_index;
			if (query_point_index >= total_points)
				query_point_index = 0;
		}
		already_sampled.insert(query_point_index);

		for (unsigned int curr_rad_section = 0; curr_rad_section < mRadiusSections; curr_rad_section++)
		{
			float rad = (mMinRadius) + (float)(curr_rad_section * rad_increase);
			ANNdist sq_rad = std::pow((double)rad, 2);
			queryPnt[0] = mPcSceneManager->mFrames[frame].buckets[bucket].PointsRef()[query_point_index].xyz.x;
			queryPnt[1] = mPcSceneManager->mFrames[frame].buckets[bucket].PointsRef()[query_point_index].xyz.y;
			queryPnt[2] = mPcSceneManager->mFrames[frame].buckets[bucket].PointsRef()[query_point_index].xyz.z;

			unsigned int amnt_n_n = (std::min)(mMaxNeighbors, (unsigned int)kdTree->annkFRSearch(queryPnt, sq_rad, 0));			

			kdTree->annkFRSearch(queryPnt, sq_rad, amnt_n_n, nearestNeighbors, sqrdDists);


			// Calculate features ////////////////////////////////////////////////////////////////////////////////////////////////////////
			double avg_dist = 0.0;
			unsigned int cur_index = 0;
			while ( ANN_DIST_INF != sqrdDists[cur_index] && cur_index < amnt_n_n )
			{
				avg_dist += sqrdDists[cur_index++];
			}
			if (0 == cur_index)
				avg_dist = 0.0;
			else
				avg_dist /= cur_index;

			PointFeatureHistory pnt_feat;
			pnt_feat.origin = DirectX::XMFLOAT3((float)queryPnt[0], (float)queryPnt[1], (float)queryPnt[2]);
			pnt_feat.radiusToAvgDistance[rad] = (float)avg_dist;
			pnt_feat.radiusToNumNeighbors[rad] = amnt_n_n;

			pntHistoryOut[curr_sample] = pnt_feat;
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
		}
	}
}
