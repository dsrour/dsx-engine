#pragma warning(disable:4244)

#include "Debug/Debug.h"
#include "SceneEditor.h"
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "Components/Spatialized.h"
#include "PointCloudRenderable.h"
#include "Utils/ComponentFetches.h"
#include "Utils/CommonWinDialogs.h"
#include "LasWriter.h"
#include "LasLoader.h"
#include "BaseApp.h"
#include "TemporalAnalysisMenu.h"
 
extern BaseApp* gApp;
 
TwBar* TemporalAnalysisMenu::mTmpAnalysisTwBar = NULL;
PcSceneManager* TemporalAnalysisMenu::mPcSceneManager = NULL;
float TemporalAnalysisMenu::mAvgColorWeight;
float TemporalAnalysisMenu::mDensityWeight;
float TemporalAnalysisMenu::mPointsWeight;
unsigned int TemporalAnalysisMenu::mSamplesPerBucket;
float TemporalAnalysisMenu::mMinRadius;
float TemporalAnalysisMenu::mMaxRadius;
unsigned int TemporalAnalysisMenu::mRadiusSections;
unsigned int TemporalAnalysisMenu::mMaxNearestNeighborsReturn;

TemporalAnalysisMenu::TemporalAnalysisMenu(PcSceneManager* const pcSceneMngr)
{
	mPcSceneManager = pcSceneMngr;

	mPointsWeight = 0.03f;
	mDensityWeight = 0.016f;
	mAvgColorWeight = 0.02f;
	mSamplesPerBucket = 10;
	mRadiusSections = 3;
	mMinRadius = 0.001f;
	mMaxRadius = 2.f;
	mMaxNearestNeighborsReturn = 1000;
	 
	mPcSceneManager->AnalysisAvgColorWeight(mAvgColorWeight);
	mPcSceneManager->AnalysisDensityWeight(mDensityWeight);
	mPcSceneManager->AnalysisPointsWeight(mPointsWeight);
	mPcSceneManager->AnalysisSamplesPerBucket(mSamplesPerBucket);
	mPcSceneManager->AnalysisRadiusSections(mRadiusSections);
	mPcSceneManager->AnalysisMinRadius(mMinRadius);
	mPcSceneManager->AnalysisMaxRadius(mMaxRadius);
	mPcSceneManager->AnalysisMaxNearestNeighborsReturn(mMaxNearestNeighborsReturn);

	mTmpAnalysisTwBar = TwNewBar("Temporal Analysis");

	TwAddButton(mTmpAnalysisTwBar, "WEIGHTS", NULL, NULL, " label='WEIGHTS' ");
	TwAddVarCB(mTmpAnalysisTwBar, "Num. Points Weight", TW_TYPE_FLOAT, SetPntsWeightCallback, GetPntsWeightCallback, &mPointsWeight, " label='Num. Points Weight' step=0.001 min=0.0 max=1.0");
	TwAddVarCB(mTmpAnalysisTwBar, "Density Weight", TW_TYPE_FLOAT, SetDensityWeightCallback, GetDensityWeightCallback, &mDensityWeight, " label='Density Weight' step=0.001 min=0.0 max=1.0");
	TwAddVarCB(mTmpAnalysisTwBar, "Avg. Color Weight", TW_TYPE_FLOAT, SetAvgColorWeightCallback, GetAvgColorWeightCallback, &mAvgColorWeight, " label='Avg. Color Weight' step=0.001 min=0.0 max=1.0");
	TwAddButton(mTmpAnalysisTwBar, "SAMPLING", NULL, NULL, " label='SAMPLING' ");
	TwAddVarCB(mTmpAnalysisTwBar, "Samples Per Bucket", TW_TYPE_UINT32, SetSamplesPerBucketCallback, GetSamplesPerBucketCallback, &mSamplesPerBucket, " label='Samples Per Bucket' step=1 min=1");
	TwAddVarCB(mTmpAnalysisTwBar, "Min. Radius", TW_TYPE_FLOAT, SetMinRadiusCallback, GetMinRadiusCallback, &mMinRadius, " label='Min. Radius' step=0.001 min=0.001");
	TwAddVarCB(mTmpAnalysisTwBar, "Max. Radius", TW_TYPE_FLOAT, SetMaxRadiusCallback, GetMaxRadiusCallback, &mMaxRadius, " label='Max. Radius' step=0.001 min=0.004");
	TwAddVarCB(mTmpAnalysisTwBar, "Radius Sections", TW_TYPE_UINT32, SetRadiusSectionsCallback, GetRadiusSectionsCallback, &mSamplesPerBucket, " label='Radius Sections' step=1 min=1");
	TwAddVarCB(mTmpAnalysisTwBar, "Max Nearest Neighbors Return", TW_TYPE_UINT32, SetMaxNearestNeighborsReturnCallback, GetMaxNearestNeighborsReturnCallback, &mMaxNearestNeighborsReturn, " label='Max Nearest Neighbors Return' step=1 min=1");
 	TwAddButton(mTmpAnalysisTwBar, "---------", NULL, NULL, " label='---------' ");
	TwAddButton(mTmpAnalysisTwBar, "Analyze", Analyze, NULL, " label='Analyze' ");
	TwAddButton(mTmpAnalysisTwBar, "Toggle Affected Buckets", ToggleAffectedBucketsDbg, NULL, " label='Toggle Affected Buckets' ");
	TwAddButton(mTmpAnalysisTwBar, "Toggle Non-Affected Points", ToggleNonAffectedPoints, NULL, " label='Toggle Non-Affected Points' ");

	TwDefine(" 'Temporal Analysis' iconified=true ");
	TwDefine(" 'Temporal Analysis' size='260 320'  ");

	static_cast<SceneEditor*>(gApp)->EventMnger()->SubscribeToEvent("PC_SYSTEM_INITIALIZED", this);
}
 
 
TemporalAnalysisMenu::~TemporalAnalysisMenu(void)
{
}
 

void TW_CALL 
TemporalAnalysisMenu::ToggleAffectedBucketsDbg( void* /*clientData*/ )
{
 	mPcSceneManager->ToggleAnalysisResults(!mPcSceneManager->ShowingAnalysisResults());
}
 
void TW_CALL 
TemporalAnalysisMenu::SetAvgColorWeightCallback(const void* value, void*)
{
 	mAvgColorWeight = *(const float*)value;
 	mPcSceneManager->AnalysisAvgColorWeight(mAvgColorWeight);
}
 
void TW_CALL 
TemporalAnalysisMenu::GetAvgColorWeightCallback(void* value, void*)
{
 	*(float*)value = mAvgColorWeight;
}
 
void TW_CALL 
TemporalAnalysisMenu::SetDensityWeightCallback( const void* value, void* )
{
 	mDensityWeight = *(const float*)value;
 	mPcSceneManager->AnalysisDensityWeight(mDensityWeight);
}
 
void TW_CALL 
TemporalAnalysisMenu::GetDensityWeightCallback( void* value, void* )
{
 	*(float*)value = mDensityWeight;
}
 
void TW_CALL 
TemporalAnalysisMenu::SetPntsWeightCallback( const void* value, void* )
{
 	mPointsWeight = *(const float*)value;
 	mPcSceneManager->AnalysisPointsWeight(mPointsWeight);
}
 
void TW_CALL 
TemporalAnalysisMenu::GetPntsWeightCallback( void* value, void* )
{
 	*(float*)value = mPointsWeight;
}
 
void TW_CALL 
TemporalAnalysisMenu::Analyze( void* /*clientData*/ )
{
 	mPcSceneManager->TemporalAnalyze();
}

void TW_CALL 
TemporalAnalysisMenu::ToggleNonAffectedPoints( void* /*clientData*/ )
{
	mPcSceneManager->ToggleNonAffectedTemporalPoints( !mPcSceneManager->HidingNonAffectedTemporalPoints() );
}

void TW_CALL 
TemporalAnalysisMenu::SetSamplesPerBucketCallback( const void* value, void* )
{
	mSamplesPerBucket = *(const unsigned int*)value;
	mPcSceneManager->AnalysisSamplesPerBucket(mSamplesPerBucket);
}

void TW_CALL 
TemporalAnalysisMenu::GetSamplesPerBucketCallback( void* value, void* )
{
	*(unsigned int*)value = mSamplesPerBucket;
}

void TW_CALL 
TemporalAnalysisMenu::SetMinRadiusCallback( const void* value, void* )
{
	mMinRadius = *(const float*)value;

	if (mMinRadius > mMaxRadius - 0.003f)
		mMaxRadius = mMinRadius + 0.003f;

	mPcSceneManager->AnalysisMinRadius(mMinRadius);
}

void TW_CALL 
TemporalAnalysisMenu::GetMinRadiusCallback( void* value, void* )
{
	*(float*)value = mMinRadius;
}

void TW_CALL 
TemporalAnalysisMenu::SetMaxRadiusCallback( const void* value, void* )
{
	mMaxRadius = *(const float*)value;

	if (mMaxRadius < mMinRadius + 0.003f)
		mMinRadius = mMaxRadius - 0.003f;

	mPcSceneManager->AnalysisMaxRadius(mMaxRadius);
}

void TW_CALL 
TemporalAnalysisMenu::GetMaxRadiusCallback( void* value, void* )
{
	*(float*)value = mMaxRadius;
}

void TW_CALL 
TemporalAnalysisMenu::SetRadiusSectionsCallback( const void* value, void* )
{
	mRadiusSections = *(const unsigned int*)value;
	mPcSceneManager->AnalysisRadiusSections(mRadiusSections);
}

void TW_CALL 
TemporalAnalysisMenu::GetRadiusSectionsCallback( void* value, void* )
{
	*(unsigned int*)value = mRadiusSections;
}

void 
TemporalAnalysisMenu::OnEvent( std::string const& event, Metadata const* const metadata )
{
	if (event == "PC_SYSTEM_INITIALIZED")
	{
		mMinRadius = mPcSceneManager->BucketSize() / 8.f;
		mMaxRadius = mPcSceneManager->BucketSize();

		mPcSceneManager->AnalysisMinRadius(mMinRadius);
		mPcSceneManager->AnalysisMaxRadius(mMaxRadius);
	}
}

void TW_CALL 
TemporalAnalysisMenu::SetMaxNearestNeighborsReturnCallback( const void* value, void* )
{
	mMaxNearestNeighborsReturn = *(const unsigned int*)value;
	mPcSceneManager->AnalysisMaxNearestNeighborsReturn(mMaxNearestNeighborsReturn);
}

void TW_CALL 
TemporalAnalysisMenu::GetMaxNearestNeighborsReturnCallback( void* value, void* )
{
	*(unsigned int*)value = mMaxNearestNeighborsReturn;
}


