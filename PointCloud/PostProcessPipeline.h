#pragma once

#include <sstream> 
#include "Systems/Renderer/DirectXTK/Inc/SpriteFont.h"
#include "BaseApp.h"
#include "Systems/Renderer/Pipeline.h"
#include "EventsMetadata.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;

class PostProcessPipeline : public Pipeline, public EventSubscriber
{
public:
    PostProcessPipeline(std::shared_ptr<D3dRenderer> renderer, EventManager* const eventMngr);
    
    virtual 
    ~PostProcessPipeline(void);
          
    virtual void
    MadeActive(void) {}

    virtual void
    MadeInactive(void) {}

    virtual void
    UpdatePipeline(void) {}

	virtual void
	RecompileShaders() { /*no shaders*/ }

	void
	ToggleFps(bool const on) { mFpsOn = on; }

	void
	TogglePointsStats(bool const on) { mPntsStatsOn = on; }

	void
	ToggleAnimationTimelineStats(bool const on) { mAnimTimelineOn = on; }

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);
        
private:
	virtual void
	OnEvent(std::string const& event, Metadata* const metadata);

	EventManager* mEventManager;

	DirectX::SpriteBatch*	  mSpriteBatch;
	DirectX::SpriteFont*	  mSpriteFont;  

	bool mFpsOn;
	bool mPntsStatsOn;
	bool mAnimTimelineOn;

	PointStats mPointStats;
	Notification mCurrNotification;
	AnimationTimelineStats mAnimStats;
	double mNewNotificationTime;
};