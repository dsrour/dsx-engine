
#pragma warning(disable:4244)

#include <assert.h>
#include <liblas/point.hpp>
#include <liblas/reader.hpp>
#include <fstream>  // std::ifstream
#include <liblas/liblas.hpp>
#include "Debug/Debug.h"
#include "LasLoader.h"

std::vector<PointCloudRenderable::Point>
LasLoader::LoadPointCloud(std::string const& file, liblas::Header& hdrOut)
{
	std::vector<PointCloudRenderable::Point> ret;

	std::ifstream ifs;
	if ( !liblas::Open(ifs, file.c_str()) )
	{
		OutputDebugMsg("Couldn't open " + to_string(file) + "\n");
		return ret;
	}

	try
	{
		liblas::Reader reader(ifs);

		hdrOut = reader.GetHeader();
		ret.reserve(reader.GetHeader().GetPointRecordsCount());

		OutputDebugMsg("Loading " + to_string(reader.GetHeader().GetPointRecordsCount()) + " points...\n");

		while (reader.ReadNextPoint())
		{
			liblas::Point const& p = reader.GetPoint();

			PointCloudRenderable::Point pnt;

			pnt.xyz.x = (float)p.GetX();
			pnt.xyz.y = (float)p.GetY();
			pnt.xyz.z = (float)p.GetZ();
			pnt.color.x = p.GetColor().GetRed() / 255.f;
			pnt.color.y = p.GetColor().GetGreen() / 255.f;
			pnt.color.z = p.GetColor().GetBlue() / 255.f;

			//OutputDebugMsg( to_string(pnt.xyz.x) + "\t" + to_string(pnt.xyz.y) + "\t" + to_string(pnt.xyz.z) + "\n");

			ret.push_back(pnt);
		}

		// Do another pass through and find min max
		float max_x = 0.f;
		float max_y = 0.f;
		float max_z = 0.f;
		float min_x = 0.f;
		float min_y = 0.f;
		float min_z = 0.f;

		for (unsigned int i = 0; i < ret.size(); i++)
		{
			if (0 == i)
			{
				min_x = max_x = ret[i].xyz.x;
				min_y = max_y = ret[i].xyz.y;
				min_z = max_z = ret[i].xyz.z;
			}
			else
			{
				if (ret[i].xyz.x < min_x) min_x = ret[i].xyz.x;
				if (ret[i].xyz.y < min_y) min_y = ret[i].xyz.y;
				if (ret[i].xyz.z < min_z) min_z = ret[i].xyz.z;

				if (ret[i].xyz.x > max_x) max_x = ret[i].xyz.x;
				if (ret[i].xyz.y > max_y) max_y = ret[i].xyz.y;
				if (ret[i].xyz.z > max_z) max_z = ret[i].xyz.z;
			}
		}

		float mid_x = min_x + ( (max_x - min_x) / 2.f );
		float mid_y = min_y + ( (max_y - min_y) / 2.f );
		float mid_z = min_z + ( (max_z - min_z) / 2.f );

		// Last pass to translate
		for (unsigned int i = 0; i < ret.size(); i++)
		{
			ret[i].xyz.x -= mid_x;
			ret[i].xyz.y -= mid_y;
			ret[i].xyz.z -= mid_z;
		}

		// adjust min max by translate
		min_x -= mid_x;
		min_y -= mid_y;
		min_z -= mid_z;
		max_x -= mid_x;
		max_y -= mid_y;
		max_z -= mid_z;

		//OutputDebugMsg( "Min point: " + to_string(min_x) + "\t" + to_string(min_y) + "\t" + to_string(min_z) + "\n");
		//OutputDebugMsg( "Max point: " + to_string(max_x) + "\t" + to_string(max_y) + "\t" + to_string(max_z) + "\n");
		//OutputDebugMsg( "Mid range: " + to_string(mid_x) + "\t" + to_string(mid_y) + "\t" + to_string(mid_z) + "\n");

		// Save the correct mid max in the header
		hdrOut.SetMin(min_x, min_y, min_z);
		hdrOut.SetMax(max_x, max_y, max_z);
	}
	catch (std::exception const& e)
	{
		OutputDebugMsg(to_string(e.what()) + "\n");
	}

	return ret;
}
