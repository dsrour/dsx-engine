#pragma once

#pragma warning(disable:4244)

#include <string>
#include <vector>
#include <DirectXMath.h>
#include <liblas/liblas.hpp>
#include "PointCloudRenderable.h"


class LasWriter
{
public:
	static void
	WritePointCloud(std::string const& file, std::vector< std::vector<PointCloudRenderable::Point>* > pcFramesOut, std::vector<DirectX::XMFLOAT4X4>& transformsIn, liblas::Header hdr);
};

