#pragma warning(disable:4244)

#include <algorithm>
#include "Debug/Debug.h"
#include "SceneEditor.h"
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "Components/Spatialized.h"
#include "Utils/ComponentFetches.h"
#include "Utils/CommonWinDialogs.h"
#include "BaseApp.h"

#include "LasLoader.h"
#include "LasWriter.h"
#include "PointCloudFromObj.h"

#include "PcFileEditMenu.h"

extern BaseApp* gApp;


TwBar*					  PointCloudFileEditMenu::mPointCloudMenuTwBar = NULL;
std::set<unsigned int>    PointCloudFileEditMenu::mCreatedPointCloudFrames;
bool					  PointCloudFileEditMenu::mSelectedFrames[MAX_FRAMES];
unsigned int			  PointCloudFileEditMenu::mSelectedFramesEntityId[MAX_FRAMES];
unsigned int			  PointCloudFileEditMenu::mTotalFrames;
std::vector<PointCloudRenderable::Point> PointCloudFileEditMenu::mFrames[MAX_FRAMES];

float			  PointCloudFileEditMenu::mPosX;
float			  PointCloudFileEditMenu::mPosY;
float			  PointCloudFileEditMenu::mPosZ;
float			  PointCloudFileEditMenu::mRotX;
float			  PointCloudFileEditMenu::mRotY;
float			  PointCloudFileEditMenu::mRotZ;

liblas::Header    PointCloudFileEditMenu::mLasHeader;

PointCloudFileEditMenu::PointCloudFileEditMenu(void)
{
	mTotalFrames = 0;

	for (unsigned int i = 0; i < MAX_FRAMES; i++)
		mSelectedFrames[i] = false;

	mPointCloudMenuTwBar = TwNewBar("Point Cloud File Edit");
	TwAddButton(mPointCloudMenuTwBar, "Add Point Cloud", CreateCloud, NULL, " label='Add Point Cloud' ");
	TwAddButton(mPointCloudMenuTwBar, "Clear Clouds", ClearClouds, NULL, " label='Clear Clouds' ");
	TwAddButton(mPointCloudMenuTwBar, "Export", ExportClouds, NULL, " label='Export' ");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_posx", TW_TYPE_FLOAT, &mPosX, " label='Pos.X' step=0.1");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_posy", TW_TYPE_FLOAT, &mPosY, " label='Pos.Y' step=0.1");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_posz", TW_TYPE_FLOAT, &mPosZ, " label='Pos.Z' step=0.1");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_rotx", TW_TYPE_FLOAT, &mRotX, " label='Rot.X' step=0.1");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_roty", TW_TYPE_FLOAT, &mRotY, " label='Rot.Y' step=0.1");
	TwAddVarRW(mPointCloudMenuTwBar, "ST_rotz", TW_TYPE_FLOAT, &mRotZ, " label='Rot.Z' step=0.1");
	
	mPosX = mPosY = mPosZ = mRotX = mRotY = mRotZ = 0.f;

	TwDefine(" 'Point Cloud File Edit' iconified=true ");
}


PointCloudFileEditMenu::~PointCloudFileEditMenu(void)
{
}

void 
TW_CALL PointCloudFileEditMenu::CreateCloud( void* /*clientData*/ )
{		
	std::wstring wfile = BasicFileOpen();
	std::string file;
	if (wfile.empty())
		return;
	file.assign(wfile.begin(), wfile.end());

	std::vector<PointCloudRenderable::Point> pnts;
		
	std::string low_case_path = file;
	std::transform(low_case_path.begin(), low_case_path.end(), low_case_path.begin(), ::tolower);	
	if ( low_case_path.find(".las") != std::string::npos )
			pnts = LasLoader::LoadPointCloud(file, mLasHeader);
	else if ( low_case_path.find(".obj") != std::string::npos )
		pnts = PointCloudFromObjFile(file);

	if (pnts.empty())
	{
		Notification notification;
		notification.notification = L"Could not load " + wfile;
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.durationSec = 2.0;
		static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent("NOTIFICATION", &notification);

		return;
	}

	OutputDebugMsg(to_string(pnts.size()) + " points loaded.\n");
	
	unsigned int entity_id = gApp->EntityMngr()->CreateEntity();

	std::shared_ptr<PointCloudRenderable> pc_renderable(new PointCloudRenderable);
	pc_renderable->Load(pnts);
	gApp->EntityMngr()->AddComponentToEntity(entity_id, pc_renderable);	

	std::shared_ptr<Spatialized> spatialized(new Spatialized);	
	gApp->EntityMngr()->AddComponentToEntity(entity_id, spatialized);	

	mCreatedPointCloudFrames.insert(entity_id);

	unsigned int frame = mTotalFrames++;

	assert(mTotalFrames < MAX_FRAMES);

	std::ostringstream frame_str;	
	frame_str << frame;

	TwAddVarRW(mPointCloudMenuTwBar, frame_str.str().c_str(), TW_TYPE_BOOLCPP, &mSelectedFrames[frame], std::string(" label='" + frame_str.str() + "'").c_str());
	mSelectedFramesEntityId[frame] = entity_id;
	mFrames[frame] = pnts;
}

void 
TW_CALL PointCloudFileEditMenu::ClearClouds( void* /*clientData*/ )
{
	for ( std::set<unsigned int> ::iterator iter = mCreatedPointCloudFrames.begin();
		  iter != mCreatedPointCloudFrames.end();
		  ++iter )
	{
		gApp->EntityMngr()->DestroyEntity(*iter);
	}

	mCreatedPointCloudFrames.clear();

	for (unsigned int i = 0; i < mTotalFrames; i++)
	{
		mFrames[i].clear();
		mSelectedFrames[i] = false;

		std::ostringstream frame_str;
		frame_str << i;
		TwRemoveVar( mPointCloudMenuTwBar, frame_str.str().c_str() );		
	}

	mTotalFrames = 0;
}

void 
PointCloudFileEditMenu::Update()
{
	// If material menu is not up and displaying, forget about it...
	int iconified = 0;
	int visible = 0;
	TwGetParam(mPointCloudMenuTwBar, NULL, "iconified", TW_PARAM_INT32, 1, &iconified);
	TwGetParam(mPointCloudMenuTwBar, NULL, "visible", TW_PARAM_INT32, 1, &visible);
	if (iconified || !visible)
		return;

	for (unsigned int i = 0; i < mTotalFrames; i++)
	{
		if (mSelectedFrames[i])
		{
			Spatialized* spatial = SpatializedFromEntity(mSelectedFramesEntityId[i]);
			assert(spatial);

			DirectX::XMFLOAT3 new_pos = spatial->LocalPosition();
			new_pos.x += mPosX;
			new_pos.y += mPosY;
			new_pos.z += mPosZ;
			spatial->LocalPosition(new_pos);
		
			DirectX::XMVECTOR quat = XMLoadFloat4(&spatial->LocalRotation());
			quat = DirectX::XMQuaternionMultiply(
				DirectX::XMQuaternionRotationRollPitchYaw(DirectX::XMConvertToRadians(mRotX), DirectX::XMConvertToRadians(mRotY), DirectX::XMConvertToRadians(mRotZ)), 
				quat
				);				
			DirectX::XMFLOAT4 new_rot; DirectX::XMStoreFloat4(&new_rot, quat);
			spatial->LocalRotation(new_rot);
		}
	}

	mPosX = mPosY = mPosZ = mRotX = mRotY = mRotZ = 0.f;
}

void TW_CALL 
PointCloudFileEditMenu::ExportClouds( void* /*clientData*/ )
{
	std::vector< std::vector<PointCloudRenderable::Point>* > pcs;
	std::vector<DirectX::XMFLOAT4X4> transforms;

	for (unsigned int i = 0; i < mTotalFrames; i++)
	{
		if (mSelectedFrames[i])
		{
			Spatialized* spatial = SpatializedFromEntity(mSelectedFramesEntityId[i]);
			assert(spatial);

			transforms.push_back(spatial->LocalTransformation());

			Renderable* renderable = RenderableFromEntity(mSelectedFramesEntityId[i]);
			assert(renderable);

			pcs.push_back(&mFrames[i]);
		}
	}

	std::wstring wfile = BasicFileSave();
	if (wfile.empty())
		return;
	std::string file;
	file.assign(wfile.begin(), wfile.end());

	LasWriter::WritePointCloud(file, pcs, transforms, mLasHeader);
}
