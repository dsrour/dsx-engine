#pragma once

#include <set>
#include <map>

/// Consistent states
enum SceneEditorState
{	
	CAMERA_CONTROL,			// can control camera	
	SPATIAL_TRANSFORM,		// can scale, rotate, translate selections			
};

class StateManager
{
public:
	StateManager() {}

	void
	ToggleState(SceneEditorState const& state, bool const& on) { if(on) mCurrStates.insert(state); else mCurrStates.erase(state); }

	bool const
	IsStateOn(SceneEditorState state)
	{
		if (mCurrStates.find(state) != mCurrStates.end())
			return true;
		return false;
	}

	
private:
	std::set<int>										mCurrStates;
};