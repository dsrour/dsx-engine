#pragma warning(disable:4244)

#include "Debug/Debug.h"
#include "BaseApp.h"
#include "LasLoader.h"
#include "PointCloudRenderable.h"

extern BaseApp* gApp;

PointCloudRenderable::PointCloudRenderable( )
{
	mLoaded = false;
}

void 
PointCloudRenderable::Load( std::vector<Point> const& points )
{
	if (points.empty())
		return;

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	//std::vector<unsigned int> indices_to_gpu;
	//for (unsigned int i = 0; i < mPointCloud.size(); i++)
	//	indices_to_gpu.push_back(i);

	//ZeroMemory( &bd, sizeof(bd) );
	//bd.Usage = D3D11_USAGE_DEFAULT;
	//bd.ByteWidth = sizeof(unsigned int) * indices_to_gpu.size();
	//bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//bd.CPUAccessFlags = 0;        
	//ZeroMemory( &InitData, sizeof(InitData) );
	//InitData.pSysMem = &indices_to_gpu[0];
	//gApp->Renderer()->Device()->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	//mIndexBuffer.indexCount = indices_to_gpu.size();  



	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Point) * points.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &points[0];

	VertexBufferDesc vbuff_desc;
	gApp->Renderer()->Device()->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Point);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)points.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

	mRenderableType = POINT_CLOUD;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;

	mLoaded = true;
}
