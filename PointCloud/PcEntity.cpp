#include <set>
#include <vector>
#include <algorithm>
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "PcEntity.h"

extern BaseApp* gApp;

PcEntity::PcEntity(std::vector<PointCloudRenderable::Point>& points)
{
	mRenderableAttached = false;

	std::vector<DirectX::XMFLOAT3> xyz;
	for (unsigned int i = 0; i < points.size(); i++)
	{
		xyz.push_back(points[i].xyz);
	}
	DirectX::BoundingBox::CreateFromPoints(mAabb, xyz.size(), &xyz[0], sizeof(DirectX::XMFLOAT3));
		
	Init(points);
}


PcEntity::~PcEntity(void)
{
	if (gApp)
		gApp->EntityMngr()->DestroyEntity(mEntityId);
}


void 
PcEntity::AttachRenderable( void )
{
	if (!mRenderableAttached && mRenderable)
	{
		gApp->EntityMngr()->AddComponentToEntity(mEntityId, mRenderable);
		mRenderableAttached = true;
	}
}

void 
PcEntity::DetachRenderable( void )
{
	if (mRenderableAttached && mRenderable)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, mRenderable->GetGuid());
		mRenderableAttached = false;
	}
}

void 
PcEntity::Init( std::vector<PointCloudRenderable::Point>& points )
{
	mTotalPoints = (unsigned int)points.size();
	mEntityId = gApp->EntityMngr()->CreateEntity();
	mRenderable = std::make_shared<PointCloudRenderable>();
	mRenderable->Load(points);
	mSpatialized = std::make_shared<Spatialized>();	
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mSpatialized);
}

void
PcEntity::SetNumPointsToRender(unsigned int const numPoints)
{
	assert(numPoints <= mTotalPoints);
	unsigned int offset = 0;//mTotalPoints - numPoints;
	mRenderable->SetNumberOfVisiblePoints( numPoints, offset );
}