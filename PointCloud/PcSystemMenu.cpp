#pragma warning(disable:4244)

#include "Debug/Debug.h"
#include "SceneEditor.h"
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "Components/Spatialized.h"
#include "PointCloudRenderable.h"
#include "Utils/ComponentFetches.h"
#include "Utils/CommonWinDialogs.h"
#include "BaseApp.h"

#include "LasLoader.h"
#include "LasWriter.h"
#include "PointCloudFromObj.h"

#include "PcSystemMenu.h"

extern BaseApp* gApp;

TwBar* PcSystemMenu::mPcSysTwBar = NULL;
PcSceneManager* PcSystemMenu::mPcSceneManager = NULL;
float PcSystemMenu::mDistWeight;
float PcSystemMenu::mAreaWeight;
float PcSystemMenu::mPointsWeight;
unsigned int PcSystemMenu::mTargetNumPnts;
unsigned int PcSystemMenu::mNumBuckets = 100;

PcSystemMenu::PcSystemMenu(PcSceneManager* const pcSceneMngr)
{
	mPcSysTwBar = TwNewBar("Point Cloud System");
	TwAddButton(mPcSysTwBar, "Append Frame", AppendFrame, NULL, " label='Append Frame' ");
	TwAddVarCB(mPcSysTwBar, "# Buckets", TW_TYPE_UINT32, SetNumBucketsCallback, GetNumBucketsCallback, &mNumBuckets, " label='# Buckets' step=1 min=1");
	TwAddButton(mPcSysTwBar, "Init", Init, NULL, " label='Init' ");
	TwAddButton(mPcSysTwBar, "Clear Frames", ClearFrames, NULL, " label='Clear Frames' ");
	TwAddButton(mPcSysTwBar, "Toggle Buckets", ToggleBucketDbgBox, NULL, " label='Toggle Buckets' ");
	TwAddButton(mPcSysTwBar, "Toggle BVH", ToggleBvhDbgBox, NULL, " label='Toggle BVH' ");
	TwAddButton(mPcSysTwBar, "Toggle SubPCs Aabbs", ToggleSubPointCloudsAabbs, NULL, " label='Toggle Sub-PCs Aabbs' ");

	TwAddButton(mPcSysTwBar, "---------", NULL, NULL, " label='---------' ");
	TwAddButton(mPcSysTwBar, "LOD", NULL, NULL, " label='LOD' ");	
	TwAddVarCB(mPcSysTwBar, "Target Points", TW_TYPE_UINT32, SetTargetPntsCallback, GetTargetPntsCallback, &mTargetNumPnts, " label='Target Points' step=50 min=0");
	TwAddVarCB(mPcSysTwBar, "Dist Weight", TW_TYPE_FLOAT, SetDistWeightCallback, GetDistWeightCallback, &mDistWeight, " label='Distance Weight' step=0.05 min=0.1 ");
	TwAddVarCB(mPcSysTwBar, "Area Weight", TW_TYPE_FLOAT, SetAreaWeightCallback, GetAreaWeightCallback, &mAreaWeight, " label='Area Weight' step=0.05 min=0.1 ");
	TwAddVarCB(mPcSysTwBar, "Points Weight", TW_TYPE_FLOAT, SetPntsWeightCallback, GetPntsWeightCallback, &mPointsWeight, " label='Points Weight' step=0.05 min=0.1 ");

	mPcSceneManager = pcSceneMngr;

	mDistWeight = pcSceneMngr->DistanceLodWeight();
	mAreaWeight = pcSceneMngr->AreaLodWeight();
	mPointsWeight = pcSceneMngr->PointsLodWeight();
	mTargetNumPnts = pcSceneMngr->TargetNumPoints();	
}


PcSystemMenu::~PcSystemMenu(void)
{
}

void 
TW_CALL PcSystemMenu::AppendFrame( void* /*clientData*/ )
{		
	std::wstring wfile = BasicFileOpen();
	if (wfile.empty())
		return;

	std::string file;
	file.assign(wfile.begin(), wfile.end());

	std::vector<PointCloudRenderable::Point> pnts;
	float min_x, min_y, min_z, max_x, max_y, max_z;
	
	std::string low_case_path = file;
	std::transform(low_case_path.begin(), low_case_path.end(), low_case_path.begin(), ::tolower);	
	if ( low_case_path.find(".las") != std::string::npos )	
	{
		liblas::Header hdr;
		pnts = LasLoader::LoadPointCloud(file, hdr);	
		min_x = (float)hdr.GetMinX();
		min_y = (float)hdr.GetMinY();
		min_z = (float)hdr.GetMinZ();
		max_x = (float)hdr.GetMaxX();
		max_y = (float)hdr.GetMaxY();
		max_z = (float)hdr.GetMaxZ();

	}
	else if ( low_case_path.find(".obj") != std::string::npos )
		pnts = PointCloudFromObjFile(file, min_x, min_y, min_z, max_x, max_y, max_z);

	if (pnts.empty())
	{
		Notification notification;
		notification.notification = L"Could not load " + wfile;
		notification.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		notification.durationSec = 2.0;
		static_cast<SceneEditor*>(gApp)->EventMnger()->BroadcastEvent("NOTIFICATION", &notification);

		return;
	}

	mPcSceneManager->AppendPcFrame( pnts, 
									DirectX::XMFLOAT3( min_x, min_y, min_z ),
									DirectX::XMFLOAT3( max_x, max_y, max_z ) );
}

void 
TW_CALL PcSystemMenu::ClearFrames( void* /*clientData*/ )
{
	mPcSceneManager->Reset();
}

void TW_CALL 
PcSystemMenu::ToggleBucketDbgBox( void* /*clientData*/ )
{
	mPcSceneManager->ToggleBucketBounds(!mPcSceneManager->ShowingBucketBounds());
}

void TW_CALL 
PcSystemMenu::ToggleBvhDbgBox( void* /*clientData*/ )
{
	mPcSceneManager->ToggleBvhBounds(!mPcSceneManager->ShowingBvhBounds());
}

void TW_CALL 
PcSystemMenu::SetTargetPntsCallback(const void* value, void*)
{
	mTargetNumPnts = *(const unsigned int*)value;
	mPcSceneManager->TargetNumPoints(mTargetNumPnts);
}

void TW_CALL 
PcSystemMenu::GetTargetPntsCallback(void* value, void*)
{
	*(unsigned int*)value = mTargetNumPnts;
}

void TW_CALL 
PcSystemMenu::SetDistWeightCallback( const void* value, void* )
{
	mDistWeight = *(const float*)value;
	mPcSceneManager->DistanceLodWeight(mDistWeight);
}

void TW_CALL 
PcSystemMenu::GetDistWeightCallback( void* value, void* )
{
	*(float*)value = mDistWeight;
}

void TW_CALL 
PcSystemMenu::SetAreaWeightCallback( const void* value, void* )
{
	mAreaWeight = *(const float*)value;
	mPcSceneManager->AreaLodWeight(mAreaWeight);
}

void TW_CALL 
PcSystemMenu::GetAreaWeightCallback( void* value, void* )
{
	*(float*)value = mAreaWeight;
}

void TW_CALL 
PcSystemMenu::SetPntsWeightCallback( const void* value, void* )
{
	mPointsWeight = *(const float*)value;
	mPcSceneManager->PointsLodWeight(mPointsWeight);
}

void TW_CALL 
PcSystemMenu::GetPntsWeightCallback( void* value, void* )
{
	*(float*)value = mPointsWeight;
}

void TW_CALL 
PcSystemMenu::Init( void* /*clientData*/ )
{
	mPcSceneManager->Init(mNumBuckets);
}

void TW_CALL 
PcSystemMenu::SetNumBucketsCallback(const void* value, void*)
{
	mNumBuckets = *(const unsigned int*)value;
}

void TW_CALL 
PcSystemMenu::GetNumBucketsCallback(void* value, void*)
{
	*(unsigned int*)value = mNumBuckets;
}

void TW_CALL 
PcSystemMenu::ToggleSubPointCloudsAabbs(void* /*clientData*/)
{
	mPcSceneManager->ToggleSubPointCloudsAabbs(!mPcSceneManager->ShowingSubPcsAabbs());
}
