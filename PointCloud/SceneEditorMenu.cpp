#include "SceneEditor.h"
#include "SceneEditorMenu.h"

extern BaseApp* gApp;

bool   SceneEditorMenu::mToggleFps				= false;
bool   SceneEditorMenu::mTogglePntsStats		= false;
bool   SceneEditorMenu::mToggleAnimTimelineStats= false;
float  SceneEditorMenu::mNavSpeedFactor			= 1.f;
TwBar* SceneEditorMenu::mSceneEditorMenuTwBar	= NULL;

SceneEditorMenu::SceneEditorMenu(void)
{
	mSceneEditorMenuTwBar = TwNewBar("Settings");
	TwAddVarCB(mSceneEditorMenuTwBar, "NavSpeedFactor", TW_TYPE_FLOAT, SetNavFactorCallback, GetNavFactorCallback, NULL, " min=1 step=0.1 label='Nav Speed Factor' ");   	   
	TwAddButton(mSceneEditorMenuTwBar, "Toggle FPS", ToggleFps, NULL, " label='Toggle FPS' ");
	TwAddButton(mSceneEditorMenuTwBar, "Toggle Points Stats", TogglePointsStats, NULL, " label='Toggle Points Stats' ");
	TwAddButton(mSceneEditorMenuTwBar, "Toggle Animation Timeline Stats", ToggleAnimTimelineStats, NULL, " label='Toggle Animation Timeline Stats' ");

	TwDefine(" Settings iconified=true ");

	static_cast<SceneEditor*>(gApp)->NavSpeedFactor(mNavSpeedFactor);
}


SceneEditorMenu::~SceneEditorMenu(void)
{
}

void TW_CALL 
SceneEditorMenu::SetNavFactorCallback( const void* value, void* /*clientData*/ )
{
	mNavSpeedFactor = *(const float*)value; 
	static_cast<SceneEditor*>(gApp)->NavSpeedFactor(mNavSpeedFactor);
}

void TW_CALL 
SceneEditorMenu::GetNavFactorCallback( void* value, void* /*clientData*/ )
{
	*(float*)value = mNavSpeedFactor;
}

void TW_CALL 
SceneEditorMenu::ToggleFps( void* /*clientData*/ )
{
	mToggleFps = !mToggleFps;

	static_cast<SceneEditor*>(gApp)->PostProcPipelineRef()->ToggleFps(mToggleFps);
}

void TW_CALL 
SceneEditorMenu::TogglePointsStats( void* /*clientData*/ )
{
	mTogglePntsStats = !mTogglePntsStats;

	static_cast<SceneEditor*>(gApp)->PostProcPipelineRef()->TogglePointsStats(mTogglePntsStats);
}

void TW_CALL 
SceneEditorMenu::ToggleAnimTimelineStats( void* /*clientData*/ )
{
	mToggleAnimTimelineStats = !mToggleAnimTimelineStats;

	static_cast<SceneEditor*>(gApp)->PostProcPipelineRef()->ToggleAnimationTimelineStats(mToggleAnimTimelineStats);
}
