#pragma warning(disable:4244)

#include <iostream>
#include <fstream>
#include "AnimationMarkerWriter.h"


void 
AnimationMarkerWriter::WriteMarkerData( std::string const& file, std::set<double> data )
{
	std::ofstream f;
	f.open (file);

	for (auto fr_iter = data.begin(); fr_iter != data.end(); ++fr_iter)
	{
		f << std::to_string(*fr_iter) + "\n";		
	}

	f.close();
}
