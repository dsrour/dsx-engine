#pragma once

#include <set>
#include <System.h>

/*  Deals with timing logic.
 *  A marker can be created per frame at specific points in time.
 *  This class can then simulate time and be queried for information such as
 *  time remaining since last marker or until the next marker hits.
 *
 *  This is a system and should be used with the SystemScheduler to set the frequency
 *  at which it is to be ran.
 */

class Timeline : public System, public std::enable_shared_from_this<Timeline>
{
public:
	Timeline(std::shared_ptr<EntityManager> entityManager);
	~Timeline(void);

	void
	InsertMarker(double const timePosition);

	void
	RemoveMarker(double const timePosition);

	unsigned int
	GetNumMarkers(void) const;

	// Clears everything and resets the whole class to defaults.
	void
	Reset(void);

	void
	SetPosition(double const pos);

	void
	SetPosition(unsigned int const marker);

	void
	Play(void);

	void
	Pause(void);

	void
	SetPlaySpeedFactor(unsigned int speedFactor);

	unsigned int const
	GetPlaySpeedFactor(void) const { return mPlaySpeedFactor; }

	double const
	DeltaSinceLastMarker(void);

	double const
	DeltaUntilNextMarker(void);

	double const
	CurrentPosition(void) const { return mCurPos; }

	unsigned int const
	LastMarker(void);

	bool const
	HasReachedEnd(void) const { return mHasReachedEnd; }

	virtual void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

	bool const
	IsPlaying(void) { return !mPaused; }

private:
	struct InBetweenMarkers
	{
		std::set<double>::iterator a;
		std::set<double>::iterator b;
	};
	InBetweenMarkers
	GetInBetweenMarkers(void);

	bool mPaused;
	double mCurPos;
	unsigned int mPlaySpeedFactor;
	unsigned int mLastMarker;
	std::set<double> mMarkers;
	double mLastTime;
	bool mHasReachedEnd;
};

