#include "Debug/Debug.h"
#include "Components/Spatialized.h"
#include "Intersections.h"
#include "SceneEditor.h"
#include "MouseActions.h"

ApplicationMouseActions::ApplicationMouseActions(std::shared_ptr<InputDeviceManager> inputDeviceManager) : InputDeviceActions(inputDeviceManager)       
{        
	mCamera = gApp->Renderer()->CurrentCamera();   
	inputDeviceManager->Mouse()->ToggleRelativeMode(false);
} 

void 
ApplicationMouseActions::OnStateChange( void )
{
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if (manager->Mouse()->RelativeMode())
		CameraTransformActions(manager);
	{
		mLastX = manager->Mouse()->MouseX();
		mLastY = manager->Mouse()->MouseY();
	}
}

void
ApplicationMouseActions::CameraTransformActions( std::shared_ptr<InputDeviceManager>& manager )
{
	/*static double last_time = 0.0;
	double new_time = gApp->Timer().ElapsedTimeSecs();
	float delta = (float)new_time - (float)last_time;
	last_time = new_time;*/
		
	float const sensitivity = 0.001f;
	float x = manager->Mouse()->MouseX()*sensitivity;
	float y = manager->Mouse()->MouseY()*sensitivity;

	//OutputDebugMsg(to_string(x)+", "+ to_string(y)+"\n");
	
	static_cast<FpCamera*>(mCamera.get())->Yaw(x);      	
	static_cast<FpCamera*>(mCamera.get())->Pitch(y);      
} 
