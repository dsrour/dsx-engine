#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Components/Spatialized.h"
#include "PointCloudPipeline.h"

extern BaseApp* gApp;

PointCloudPipeline::PointCloudPipeline(std::shared_ptr<D3dRenderer> renderer) : Pipeline(renderer)
{
	mPointCloudEffect = std::make_shared<PointCloudEffect>(renderer);	

	// Create state descs
	ZeroMemory(&mDepthStencilDesc, sizeof(mDepthStencilDesc));
	mDepthStencilDesc.DepthEnable = false;
	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	mDepthStencilDesc.StencilEnable = true;
	mDepthStencilDesc.StencilReadMask = 0xFF;
	mDepthStencilDesc.StencilWriteMask = 0xFF;
	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	ZeroMemory(&mBlendDesc, sizeof(mBlendDesc));
	mBlendDesc.AlphaToCoverageEnable = false;
	mBlendDesc.IndependentBlendEnable = false;
	mBlendDesc.RenderTarget[0].BlendEnable = true;
	mBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	mBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	mBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	// Create resources
	D3D11_TEXTURE2D_DESC bb_desc;
	ZeroMemory(&bb_desc, sizeof(bb_desc));
	bb_desc.Width = gApp->WinWidth();
	bb_desc.Height = gApp->WinHeight();
	bb_desc.MipLevels = 1;
	bb_desc.ArraySize = 1;
	bb_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	bb_desc.SampleDesc.Count = 1;
	bb_desc.Usage = D3D11_USAGE_DEFAULT;
	bb_desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	bb_desc.CPUAccessFlags = 0;
	bb_desc.MiscFlags = 0;

	HRESULT hr;
	hr = renderer->Device()->CreateTexture2D(&bb_desc, NULL, &mRtTexture);
	assert(SUCCEEDED(hr));

	D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
	ZeroMemory(&rtv_desc, sizeof(rtv_desc));
	rtv_desc.Format = bb_desc.Format;
	rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtv_desc.Texture2D.MipSlice = 0;

	hr = renderer->Device()->CreateRenderTargetView(mRtTexture, &rtv_desc, &mRtv);
	assert(SUCCEEDED(hr));

	ZeroMemory(&mRtTextureSrvDesc, sizeof(mRtTextureSrvDesc));
	mRtTextureSrvDesc.Format = bb_desc.Format;
	mRtTextureSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	mRtTextureSrvDesc.Texture2D.MostDetailedMip = 0;
	mRtTextureSrvDesc.Texture2D.MipLevels = 1;
	hr = renderer->Device()->CreateShaderResourceView(mRtTexture, &mRtTextureSrvDesc, &mRtTextureSrv);
	assert(SUCCEEDED(hr));

	ZeroMemory(&mSamplerDesc, sizeof(mSamplerDesc));
	mSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	mSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	mSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	mSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	mSamplerDesc.MipLODBias = 0.0f;
	mSamplerDesc.MaxAnisotropy = 1;
	mSamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	mSamplerDesc.BorderColor[0] = 0;
	mSamplerDesc.BorderColor[1] = 0;
	mSamplerDesc.BorderColor[2] = 0;
	mSamplerDesc.BorderColor[3] = 0;
	mSamplerDesc.MinLOD = 0;
	mSamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
}


PointCloudPipeline::~PointCloudPipeline(void)
{	
	if (mRtTexture)
	{
		mRtTexture->Release();
		mRtTexture = nullptr;
	}

	if (mRtv)
	{
		mRtv->Release();
		mRtv = nullptr;
	}

	if (mRtTextureSrv)
	{
		mRtTextureSrv->Release();
		mRtTextureSrv = nullptr;
	}	
}

void 
PointCloudPipeline::RecompileShaders()
{
	if (mPointCloudEffect) 
	{
		MadeInactive();
		mPointCloudEffect->Recompile();
		MadeActive();
	}
}

void 
PointCloudPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;    
		
	float clr_color[4] = { 0.f, 0.f, 0.f, 1.f };
	renderer->DeviceContext()->ClearRenderTargetView(mRtv, &clr_color[0]);
	renderer->DeviceContext()->OMSetRenderTargets(1, &mRtv, renderer->DepthStencilView());


	// Set default blend state
	renderer->DeviceContext()->OMSetBlendState(NULL, NULL, 0xffffffff);

	// Set default depth stencil state
	renderer->DeviceContext()->OMSetDepthStencilState(NULL, NULL);

	// Default raster state
	ID3D11RasterizerState* default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::Default);
	renderer->DeviceContext()->RSSetState(default_raster);

	std::shared_ptr<Camera> camera = renderer->CurrentCamera();

	// Update View Projection matrix variable
	DirectX::XMMATRIX view =  XMLoadFloat4x4(&(camera->ViewMatrix()));
	DirectX::XMMATRIX proj =  XMLoadFloat4x4(&(camera->ProjMatrix()));
	DirectX::XMMATRIX vp =  view * proj;
	mPointCloudEffect->UpdateViewProjectionMatrixVariable(vp); 

	// Iterate through families
	std::set<unsigned int> renderable_types_to_process;
	renderable_types_to_process.insert(Renderable::POINT_CLOUD);
	
	for (std::set<unsigned int>::iterator type_iter = renderable_types_to_process.begin();
		type_iter != renderable_types_to_process.end();
		++type_iter)
	{
		std::set<unsigned int>::const_iterator fam_iter = familyByRenderableType[*type_iter].begin();
		for (fam_iter; fam_iter != familyByRenderableType[*type_iter].end(); ++fam_iter)
		{
			// Get renderable component
			std::shared_ptr<IComponent> tmp_comp;			

			// Check for renderable to draw
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), tmp_comp);                
			if (tmp_comp)
			{
				Renderable* renderable = static_cast<Renderable*>(tmp_comp.get());	

				// Update world mat from Spatialized component
				DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
				gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), tmp_comp);     
				if (tmp_comp)
				{
					Spatialized* spatial_comp = static_cast<Spatialized*>(tmp_comp.get());
					world = DirectX::XMLoadFloat4x4(&spatial_comp->LocalTransformation());
				}
				mPointCloudEffect->UpdateWorldMatrixVariable(world);

				if (Renderable::POINT_CLOUD == renderable->RenderableType())
					DrawPointCloud(renderable, renderer);
			}
		}    
	}
	
	// Do post process

	// Resolve	
	renderer->SetBackBufferRenderTarget(); // Use default back buffer as render target

	// Set raster state
	ID3D11RasterizerState* raster_state = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
	renderer->DeviceContext()->RSSetState(raster_state);

	// Set blend state
	renderer->DeviceContext()->OMSetBlendState(renderer->StateMngr()->GetOrCreateBlendState(renderer->Device(), mBlendDesc), NULL, 0xffffffff);

	// Set depth stencil state
	renderer->DeviceContext()->OMSetDepthStencilState(renderer->StateMngr()->GetOrCreateDepthStencilState(renderer->Device(), mDepthStencilDesc), 1);

	// Set primitive topology to point list
	renderer->DeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Texture
	mPointCloudEffect->UpdatePcTextureSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), mSamplerDesc));
	mPointCloudEffect->UpdatePcTextureVariable(mRtTextureSrv);

	renderer->DeviceContext()->IASetInputLayout(nullptr);
	renderer->DeviceContext()->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);

	// Apply
	mPointCloudEffect->ApplyTechnique(POINT_CLOUD_RESOLVE_TECH, renderer->DeviceContext());

	renderer->DeviceContext()->Draw(3, NULL);

	mPointCloudEffect->ClearTechnique(POINT_CLOUD_RESOLVE_TECH, renderer->DeviceContext());
}

void 
PointCloudPipeline::DrawPointCloud( Renderable* renderable, std::shared_ptr<D3dRenderer> renderer )
{
	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

	renderer->DeviceContext()->IASetInputLayout( mPointCloudEffect->InputLayout(POINT_CLOUD_TECH) );
	input_layout_desc = mPointCloudEffect->InputLayoutDescription(POINT_CLOUD_TECH);
	assert( !input_layout_desc.empty() );

	renderer->DeviceContext()->IASetPrimitiveTopology(renderable->PrimitiveTopology());

	// Go through needed subinputs and accumulate vertex buffers
	std::vector<ID3D11Buffer*> buffers;
	std::vector<unsigned int> strides;
	std::vector<unsigned int> offsets;

	unsigned int vertex_count = 0;

	std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();            
	for ( sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++ )
	{
		buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
		strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
		offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);  
		
		if (vertex_count == 0)
			vertex_count = renderable->VertexBuffer(*sub_input_iter).vertexCount;
		else if (vertex_count != renderable->VertexBuffer(*sub_input_iter).vertexCount)
		{
			OutputDebugMsg("PointCloudPipeline::DrawPointCloud: renderable has mismatching vertex count in its sub-vertex buffer!\n");
			assert(false);
		}
	}
	renderer->DeviceContext()->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

	// Apply
	mPointCloudEffect->ApplyTechnique(POINT_CLOUD_TECH, renderer->DeviceContext());				

	renderer->DeviceContext()->Draw(vertex_count, 0);

	mPointCloudEffect->ClearTechnique(POINT_CLOUD_TECH, renderer->DeviceContext());
}
