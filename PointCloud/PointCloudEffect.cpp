#include <string>
#include <d3dcompiler.h>
#include "PointCloudEffect.h"

extern std::wstring gEngineRootDir;

PointCloudEffect::PointCloudEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mPcVsBlob = nullptr;
	mPcVS = nullptr;
	mPcPsBlob = nullptr;
	mPcPS = nullptr;
	mResolveVsBlob = nullptr;
	mResolveVS = nullptr;
	mResolvePsBlob = nullptr;
	mResolvePS = nullptr;

	mPerFrameCb.Create(renderer->Device());

	ZeroMemory(&mPerFrameVariables, sizeof(CbPerFrame));

	mPcTextureSampler = nullptr;
	mPcTextureSrv = nullptr;
	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}


PointCloudEffect::~PointCloudEffect(void)
{
	if (mPcVsBlob)
		mPcVsBlob->Release();
	if (mPcVS)
		mPcVS->Release();
	if (mPcPsBlob)
		mPcPsBlob->Release();
	if (mPcPS)
		mPcPS->Release();

	if (mResolveVsBlob)
		mResolveVsBlob->Release();
	if (mResolveVS)
		mResolveVS->Release();
	if (mResolvePsBlob)
		mResolvePsBlob->Release();
	if (mResolvePS)
		mResolvePS->Release();
}

void 
PointCloudEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	cso_name = std::wstring(POINT_CLOUD_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mPcVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mPcVsBlob->GetBufferPointer(),
		mPcVsBlob->GetBufferSize(),
		NULL,
		&mPcVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(POINT_CLOUD_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mPcPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mPcPsBlob->GetBufferPointer(),
		mPcPsBlob->GetBufferSize(),
		NULL,
		&mPcPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(POINT_CLOUD_RESOLVE_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mResolveVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mResolveVsBlob->GetBufferPointer(),
		mResolveVsBlob->GetBufferSize(),
		NULL,
		&mResolveVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(POINT_CLOUD_RESOLVE_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mResolvePsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mResolvePsBlob->GetBufferPointer(),
		mResolvePsBlob->GetBufferSize(),
		NULL,
		&mResolvePS);
	assert(SUCCEEDED(hr));

	{	
		// Create input layout descs
 		std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

		input_layout_desc.push_back(InputLayoutManager::GEOMETRY);  
		mInputLayoutDescs[POINT_CLOUD_TECH] = input_layout_desc;

		mInputLayouts[POINT_CLOUD_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mPcVsBlob, renderer->Device());
	}
}

void 
PointCloudEffect::UpdateWorldMatrixVariable( DirectX::XMMATRIX& worldMat )
{
	mPerFrameVariables.worldMat = DirectX::XMMatrixTranspose(worldMat);
}

void 
PointCloudEffect::UpdateViewProjectionMatrixVariable( DirectX::XMMATRIX& vp )
{
	mPerFrameVariables.vpMat = DirectX::XMMatrixTranspose(vp);
}

void 
PointCloudEffect::UpdatePcTextureSamplerVariable(ID3D11SamplerState* const sampler)
{
	mPcTextureSampler = sampler;
}

void 
PointCloudEffect::UpdatePcTextureVariable(ID3D11ShaderResourceView* const texture)
{
	mPcTextureSrv = texture;
}

void 
PointCloudEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (POINT_CLOUD_TECH == techName)
	{
		mPerFrameCb.SetData(deviceContext, mPerFrameVariables);

		auto cb = mPerFrameCb.GetBuffer();
		deviceContext->VSSetConstantBuffers(0, 1, &cb);

		deviceContext->VSSetShader(mPcVS, nullptr, 0);
		deviceContext->PSSetShader(mPcPS, nullptr, 0);
	}
	else if (POINT_CLOUD_RESOLVE_TECH == techName)
	{
		deviceContext->PSSetShaderResources(0, 1, &mPcTextureSrv);
		deviceContext->PSSetSamplers(0, 1, &mPcTextureSampler);

		deviceContext->VSSetShader(mResolveVS, nullptr, 0);
		deviceContext->PSSetShader(mResolvePS, nullptr, 0);
	}
}

void 
PointCloudEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;
	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11SamplerState* ns = nullptr;

	if (POINT_CLOUD_RESOLVE_TECH == techName)
	{
		deviceContext->VSSetConstantBuffers(0, 1, &nb);
		deviceContext->VSSetShader(nullptr, nullptr, 0);

		deviceContext->PSSetShaderResources(0, 1, &nsrv);
		deviceContext->PSSetSamplers(0, 1, &ns);
		deviceContext->PSSetShader(nullptr, nullptr, 0);
	}
}
