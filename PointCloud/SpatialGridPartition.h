#pragma once

#include <DirectXMath.h>

class SpatialGridPartition
{
public:
	SpatialGridPartition(DirectX::XMFLOAT3 const worldMin, DirectX::XMFLOAT3 const worldMax, float const gridCellSize);
	~SpatialGridPartition(void);

	DirectX::XMFLOAT3 const& 
	WorldMin() const { return mWorldMin; }

	DirectX::XMFLOAT3 const& 
	WorldMax() const { return mWorldMax; }

	float const& 
	CellSize() const { return mCellSize; }
	
	unsigned int const
	IndexFromXyz(DirectX::XMFLOAT3 const xyz);

	DirectX::XMFLOAT3 const
	CellCenterFromIndex(unsigned int const index);

private:
	DirectX::XMFLOAT3 mWorldMin;
	DirectX::XMFLOAT3 mWorldMax;
	float mCellSize;
	
	float mWorldWidth;
	float mWorldHeight;
	float mWorldDepth;
	unsigned int mNumCellsX;
	unsigned int mNumCellsY;
	unsigned int mNumCellsZ;
};

