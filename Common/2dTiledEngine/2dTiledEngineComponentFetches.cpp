#include "BaseApp.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"

extern BaseApp* gApp;

Velocity* 
VelocityComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Velocity::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Velocity*>(tmp.get());
	}

	return nullptr;
}

Acceleration* 
AccelerationComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Acceleration::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Acceleration*>(tmp.get());
	}

	return nullptr;
}

Mass* 
MassComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Mass::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Mass*>(tmp.get());
	}

	return nullptr;
}

Elasticity* 
ElasticityComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Elasticity::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Elasticity*>(tmp.get());
	}

	return nullptr;
}

Friction* 
FrictionComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Friction::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Friction*>(tmp.get());
	}

	return nullptr;
}

Health* 
HealthComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Health::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Health*>(tmp.get());
	}

	return nullptr;
}

Damageable* 
DamageableComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Damageable::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<Damageable*>(tmp.get());
	}

	return nullptr;
}

OnMovingPlatform*
OnMovingPlatformComponent( unsigned int const entityId )
{
	std::shared_ptr<IComponent> tmp;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, OnMovingPlatform::GetGuid(), tmp);
	if (tmp)
	{
		return static_cast<OnMovingPlatform*>(tmp.get());
	}

	return nullptr;
}

