#pragma once

#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/Acceleration.h"
#include "2dTiledEngine/Components/Mass.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Friction.h"
#include "2dTiledEngine/Components/Health.h"
#include "2dTiledEngine/Components/Damageable.h"
#include "2dTiledEngine/Components/OnMovingPlatform.h"


Velocity*
VelocityComponent(unsigned int const entityId);

Acceleration*
AccelerationComponent(unsigned int const entityId);

Mass*
MassComponent(unsigned int const entityId);

Elasticity*
ElasticityComponent(unsigned int const entityId);

Friction*
FrictionComponent(unsigned int const entityId);

Health*
HealthComponent(unsigned int const entityId);

Damageable*
DamageableComponent(unsigned int const entityId);

OnMovingPlatform*
OnMovingPlatformComponent(unsigned int const entityId);