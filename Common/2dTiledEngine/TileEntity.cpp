#include "EntityManager.h"
#include "BaseApp.h"
#include "2dTiledEngine/TileEntity.h"

extern BaseApp* gApp;

TileEntity::TileEntity(void)
{
	unsigned int id = gApp->EntityMngr()->CreateEntity();

	mBoundedComp = std::make_shared<Bounded>();
	mSpatializedComp = std::make_shared<Spatialized>();
	mTiledEntityComp = std::make_shared<TiledEntity>(id, false);

	gApp->EntityMngr()->AddComponentToEntity(id, mBoundedComp);
	gApp->EntityMngr()->AddComponentToEntity(id, mSpatializedComp);
	gApp->EntityMngr()->AddComponentToEntity(id, mTiledEntityComp);
}


TileEntity::~TileEntity(void)
{
	if (gApp)
		gApp->EntityMngr()->DestroyEntity(mTiledEntityComp->TiledEntityId());
}
