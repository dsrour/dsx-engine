#pragma once

#include "2dTiledEngine/Level.h"
#include "System.h"

class TileMovementSystem : public System, public std::enable_shared_from_this<TileMovementSystem>
{
public:
	TileMovementSystem(std::shared_ptr<EntityManager> entityManager, Level* const level);
	~TileMovementSystem(void);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

private:
	double mLastRan;  
	Level* mLevel;
};

