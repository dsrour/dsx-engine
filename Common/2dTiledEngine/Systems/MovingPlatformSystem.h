#pragma once

#include "2dTiledEngine/Level.h"
#include "System.h"

class MovingPlatformSystem : public System, public std::enable_shared_from_this<MovingPlatformSystem>
{
public:
	MovingPlatformSystem(std::shared_ptr<EntityManager> entityManager, Level* const level);
	~MovingPlatformSystem(void);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

private:
	double mLastRan;  
	Level* mLevel;
};

