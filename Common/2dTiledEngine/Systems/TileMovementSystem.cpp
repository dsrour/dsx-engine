#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/TiledEntity.h"
#include "2dTiledEngine/Components/Acceleration.h"
#include "2dTiledEngine/Level.h"
#include "Components/Spatialized.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Utils/MathUtils.h"
#include "TileMovementSystem.h"

extern BaseApp* gApp;

TileMovementSystem::TileMovementSystem(std::shared_ptr<EntityManager> entityManager,  Level* const level) : System(entityManager)
{
	// Set family req
	std::set<unsigned int> fam_req;
	fam_req.insert(Velocity::GetGuid());      
	fam_req.insert(Spatialized::GetGuid());   
	fam_req.insert(TiledEntity::GetGuid());   
	SetFamilyRequirements(fam_req);    

	mLastRan = 0.0;
	mLevel = level;
}


TileMovementSystem::~TileMovementSystem(void)
{
}

void 
TileMovementSystem::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	float const AUTODESTRUCT_MAX_VEL = 300.f; // If an entity moves that fast during a time step... it is assumed rogue and deleted

	if (0.0 == mLastRan)
	{
		mLastRan = (float)(gApp->Timer().ElapsedTimeSecs());
		return;
	}

	MovingTilesMetadata moving_tiles;

	std::set<unsigned int>::const_iterator fam_iter = family->begin();
	for (fam_iter; fam_iter!= family->end(); ++fam_iter)
	{   			
		//////////////////////////////////////////////////////
		float delta = (float)(gApp->Timer().ElapsedTimeSecs() - mLastRan);
		assert(delta > 0.f);
		//////////////////////////////////////////////////////

		std::shared_ptr<IComponent> velocity_tmp_comp, tiled_tmp_comp, accel_tmp_comp, spatial_tmp_comp;

		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Velocity::GetGuid(), velocity_tmp_comp);
		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, TiledEntity::GetGuid(), tiled_tmp_comp);
		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Acceleration::GetGuid(), accel_tmp_comp);
		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), spatial_tmp_comp);

		 if (velocity_tmp_comp && tiled_tmp_comp)
		 {
			 Acceleration* accel_comp = nullptr;
			 if (accel_tmp_comp)
				 accel_comp = static_cast<Acceleration*>(accel_tmp_comp.get());

			 Velocity* velocity = static_cast<Velocity*>(velocity_tmp_comp.get());
			 TiledEntity* tiled_entity = static_cast<TiledEntity*>(tiled_tmp_comp.get());
			 Spatialized* spatialized = static_cast<Spatialized*>(spatial_tmp_comp.get());

			 if ( std::fabsf(velocity->VelocityVector().x) > AUTODESTRUCT_MAX_VEL ||
				  std::fabsf(velocity->VelocityVector().y) > AUTODESTRUCT_MAX_VEL )
			 {
				 mLevel->RemoveTileEntity(tiled_entity->TiledEntityId());
				 continue;
			 }

			 // Apply accel if there is some
			 DirectX::XMFLOAT3 accel(0.f, 0.f, 0.f);			 
			 if (accel_comp)			 				 
				 accel = accel_comp->AccelerationVector();				 

			 DirectX::XMFLOAT2 pos_start(spatialized->LocalPosition().x, spatialized->LocalPosition().y);
			 DirectX::XMFLOAT2 pos_end = pos_start;
			 			 
			 // Velocity vector represents tile units
			 delta *= (float)mLevel->TileSize(); 
			 pos_end.x += ( delta * velocity->VelocityVector().x ) + (0.5f * accel.x * delta * delta);
			 pos_end.y += ( delta * velocity->VelocityVector().y ) + (0.5f * accel.y * delta * delta);

			 // Update velocity
			 DirectX::XMFLOAT3 final_vel = velocity->VelocityVector();
			 final_vel.x += accel.x * delta;
			 final_vel.y += accel.y * delta;
			 velocity->VelocityVector(final_vel);

			 float sqrd_dist = std::powf(pos_end.x - pos_start.x, 2.f) + std::powf(pos_end.y - pos_start.y, 2.f);		

			 if ( sqrd_dist > 0.f )
			 {
				 //spatialized->LocalPosition(pos_end);
				 moving_tiles.startPos[tiled_entity->TiledEntityId()] = pos_start;
				 moving_tiles.endPos[tiled_entity->TiledEntityId()] = pos_end;
			 }
		 }		 
	}	

	// Any moving tiles?
	if ( !moving_tiles.startPos.empty() )
	{
		// Broadcast a move event			
		mLevel->EventMngr()->BroadcastEvent(MOVING_TILES_EVENT, &moving_tiles);
	}		

	mLastRan = (float)(gApp->Timer().ElapsedTimeSecs());
}
