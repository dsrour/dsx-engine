#include "BaseApp.h"
#include "MovingPlatformSystem.h"
#include "2dTiledEngine/Components/TiledEntity.h"
#include "Components/Spatialized.h"
#include "Debug/Debug.h"
#include "Utils/MathUtils.h"
#include "2dTiledEngine/Components/OnMovingPlatform.h"


extern BaseApp* gApp;

MovingPlatformSystem::MovingPlatformSystem(std::shared_ptr<EntityManager> entityManager,  Level* const level) : System(entityManager)
{
	// Set family req
	std::set<unsigned int> fam_req;
	fam_req.insert(Spatialized::GetGuid());      	
	fam_req.insert(TiledEntity::GetGuid());   
	fam_req.insert(OnMovingPlatform::GetGuid());   
	SetFamilyRequirements(fam_req);    

	mLastRan = 0.0;
	mLevel = level;
}


MovingPlatformSystem::~MovingPlatformSystem(void)
{
}

void 
MovingPlatformSystem::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	if (0.0 == mLastRan)
	{
		mLastRan = (float)(gApp->Timer().ElapsedTimeSecs());
		return;
	}

	//////////////////////////////////////////////////////
	float delta = (float)(gApp->Timer().ElapsedTimeSecs() - mLastRan);
	assert(delta > 0.f);
	//////////////////////////////////////////////////////

	MovingTilesMetadata moving_tiles;

	std::set<unsigned int>::const_iterator fam_iter = family->begin();
	for (fam_iter; fam_iter!= family->end(); fam_iter++)
	{   			
		 std::shared_ptr<IComponent> tiled_tmp_comp, on_mving_pltfrm_tmp_comp, spatial_tmp_comp;

		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, TiledEntity::GetGuid(), tiled_tmp_comp);
		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, OnMovingPlatform::GetGuid(), on_mving_pltfrm_tmp_comp);
		 gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), spatial_tmp_comp);

		 if (on_mving_pltfrm_tmp_comp && tiled_tmp_comp)
		 {
			 OnMovingPlatform* on_platform = static_cast<OnMovingPlatform*>(on_mving_pltfrm_tmp_comp.get());
			 TiledEntity* tiled_entity = static_cast<TiledEntity*>(tiled_tmp_comp.get());
			 Spatialized* spatialized = static_cast<Spatialized*>(spatial_tmp_comp.get());

			 if (on_platform->MovingPlatformId() < 0)
				 continue;
			 			 
			 DirectX::XMFLOAT3 pos_end_vec3 = mLevel->Entity(on_platform->MovingPlatformId())->SpatializedComponent()->LocalPosition();
			 DirectX::XMFLOAT2 pos_end(pos_end_vec3.x+on_platform->Offset().x, pos_end_vec3.y+on_platform->Offset().y-2.f);
			 DirectX::XMFLOAT2 pos_start = pos_end;
			 			 
			 //float sqrd_dist = std::powf(pos_end.x - pos_start.x, 2.f) + std::powf(pos_end.y - pos_start.y, 2.f);		

			 //if ( sqrd_dist > 0.f )
			 {
				 //spatialized->LocalPosition(pos_end);
				 moving_tiles.startPos[tiled_entity->TiledEntityId()] = pos_start;
				 moving_tiles.endPos[tiled_entity->TiledEntityId()] = pos_end;
			 }
		 }			 
	}	

	// Any moving tiles?
	if ( !moving_tiles.startPos.empty() )
	{
		// Broadcast a move event			
		mLevel->EventMngr()->BroadcastEvent(MOVING_TILES_EVENT, &moving_tiles);
	}		

	mLastRan = (float)(gApp->Timer().ElapsedTimeSecs());
}
