#include <set>
#include <list>
#include "Utils/MathUtils.h"
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/TileCollisionResolver.h"

/* The following implementations are largely based on this blog entry:
 * http://gamedev.tutsplus.com/tutorials/implementation/create-custom-2d-physics-engine-aabb-circle-impulse-resolution/
 */


extern BaseApp* gApp;

TileCollisionResolver::TileCollisionResolver(Level* const level)
{
	mCollisionSlop = 0.f;
	mCollisionCorrectionSlack = 0.1f;
	mTrajectoryStepDelta = 0.2f;
	mLevel = level;
	mLevel->EventMngr()->SubscribeToEvent(MOVING_TILES_EVENT, this);
}


TileCollisionResolver::~TileCollisionResolver(void)
{
}

void 
TileCollisionResolver::OnEvent( std::string const& event, Metadata* const metadata )
{
	if (MOVING_TILES_EVENT == event)
	{
		MovingTilesMetadata* const tile_move = static_cast<MovingTilesMetadata* const>(metadata);
		SimulateMovement(tile_move);
	}
}

void 
TileCollisionResolver::SimulateMovement( MovingTilesMetadata* const movingTiles )
{
	//OutputDebugMsg("START\n");
	std::map<unsigned int, DirectX::XMFLOAT2> current_positions = movingTiles->startPos;
	std::map<unsigned int, DirectX::XMFLOAT2> end_positions = movingTiles->endPos;
		
	std::map<unsigned int, ResolvedAxes> resolved_collisions; // for entity's x & y axis

	std::map< std::pair<int, int>, CollisionInfo > to_callbak;

	do
	{
		std::set<unsigned int> ended_trajectory; // Records which entities are done moving

		// step through the trajectory of all moving entities and update grid position
		for ( auto moving_iter = current_positions.begin();
			  moving_iter != current_positions.end();
			  ++moving_iter )
		{
			unsigned int const id = moving_iter->first;

			DirectX::XMFLOAT2 new_pos = moving_iter->second;

			if ( StepThroughTrajectory(id, resolved_collisions[id], moving_iter->second, movingTiles->startPos[id], end_positions[id], new_pos) )
				ended_trajectory.insert(id);

			moving_iter->second = new_pos;	
			mLevel->Entity(id)->SpatializedComponent()->LocalPosition( DirectX::XMFLOAT3(new_pos.x, new_pos.y, 0.f) );
			UpdateGridPosition(id, new_pos);			
		}

		//OutputDebugMsg("Advanced trajectories\n");

		std::map<unsigned int, std::vector<CollisionInfo> > collisions;
		
		unsigned int iteration = 0;
		unsigned int const MAX_ITERATIONS = 5;

 		do 
 		{  			
			collisions.clear();

			// Check for collisions and resolve them
			DetectCollisions(current_positions, collisions);		

			//if (0 == iteration)
				for (auto iter = collisions.begin(); iter != collisions.end(); ++iter)
					for (auto col_iter = iter->second.begin(); col_iter != iter->second.end(); ++col_iter)
					{						
						std::pair<int, int> pair(col_iter->movingEntityId, col_iter->collidingEntityId);
						if ( to_callbak.end() == to_callbak.find(pair) )
							to_callbak[pair] = *col_iter;					
					}

		} while ( ResolveCollisions(current_positions, end_positions, collisions, resolved_collisions) && iteration++ < MAX_ITERATIONS );
	
		// Remove resolved entities
		auto res_iter = resolved_collisions.begin();
		while ( res_iter != resolved_collisions.end() )	
		{
			if (res_iter->second.x && res_iter->second.y)
			{
				//OutputDebugMsg(to_string(res_iter->first) + "  resolved\n");
				current_positions.erase(res_iter->first);	
				resolved_collisions.erase(res_iter++);
			}
			else
			{
				++res_iter;
			}
		}
		
		// Remove done moving entities
		for (auto ended_traj_iter = ended_trajectory.begin();
			 ended_traj_iter != ended_trajectory.end();
			 ++ended_traj_iter )		
		{
// 			OutputDebugMsg(to_string(*ended_traj_iter) + "  done traveling\tcurrent: " +
// 				to_string(current_positions[*ended_traj_iter].x) + ", " +
// 				to_string(current_positions[*ended_traj_iter].y) + "\t end: " +
// 				to_string(end_positions[*ended_traj_iter].x) + ", " +
// 				to_string(end_positions[*ended_traj_iter].y) + "\n");
			current_positions.erase(*ended_traj_iter);			
		}

	} while ( !current_positions.empty() );

	for ( auto iter = to_callbak.begin();
		  iter != to_callbak.end();
		  ++iter )
	{
		//OutputDebugMsg(to_string(iter->second.movingEntityId)+" -> "+to_string(iter->second.collidingEntityId)+"\n");	

		TileEntity* moving = mLevel->Entity(iter->second.movingEntityId);
		TileEntity* colliding = mLevel->Entity(iter->second.collidingEntityId);
				
		moving->PreCollision(mLevel, iter->second);	
		colliding->PreCollision(mLevel, iter->second);	

		if (moving->IsElasticRigidBody() && colliding->IsElasticRigidBody())			
			ElasticCollisionResolution(mLevel, iter->second);

		moving->OnCollision(mLevel, iter->second);	
		colliding->OnCollision(mLevel, iter->second);	

		moving->PostCollision(mLevel, iter->second);	
		colliding->PostCollision(mLevel, iter->second);					
	}

	//OutputDebugMsg("END\n");
}

bool const 
TileCollisionResolver::TileToTileIntersection(  DirectX::XMFLOAT2& movingTileMin, DirectX::XMFLOAT2& movingTileMax, 
												DirectX::XMFLOAT2& collidingTileMin, DirectX::XMFLOAT2& collidingTileMax, 							
												DirectX::XMFLOAT2& normalOut, float& penetrationOut )
{	
	DirectX::XMFLOAT2 moving_tile_half_extent( (movingTileMax.x - movingTileMin.x) / 2.f, (movingTileMax.y - movingTileMin.y) / 2.f );
	DirectX::XMFLOAT2 colliding_tile_half_extent( (collidingTileMax.x - collidingTileMin.x) / 2.f, (collidingTileMax.y - collidingTileMin.y) / 2.f );

	DirectX::XMFLOAT2 moving_tile_center(movingTileMin.x + moving_tile_half_extent.x, movingTileMin.y + moving_tile_half_extent.y);
	DirectX::XMFLOAT2 colliding_tile_center(collidingTileMin.x + colliding_tile_half_extent.x, collidingTileMin.y + colliding_tile_half_extent.y);

	DirectX::XMFLOAT2 n = DirectX::XMFLOAT2(moving_tile_center.x - colliding_tile_center.x, moving_tile_center.y - colliding_tile_center.y);

	float x_overlap = moving_tile_half_extent.x + colliding_tile_half_extent.x - std::fabsf(n.x);
	if ( std::fabsf(x_overlap) < mCollisionSlop )
		x_overlap = 0.f;

	penetrationOut = 0.f;

	if(x_overlap > 0.f)
	{
		float y_overlap = moving_tile_half_extent.y + colliding_tile_half_extent.y - std::fabsf(n.y);
		if ( std::fabsf(y_overlap) < mCollisionSlop )
			y_overlap = 0.f;
		
		if(y_overlap > 0.f)
		{
			if(x_overlap < y_overlap)
			{
				if(n.x < 0.f)
					normalOut = DirectX::XMFLOAT2( -1.f, 0.f );
				else
					normalOut = DirectX::XMFLOAT2( 1.f, 0.f );

				penetrationOut = x_overlap;
			}
			else
			{
				if(n.y < 0.f)
					normalOut = DirectX::XMFLOAT2( 0.f, -1.f );
				else
					normalOut = DirectX::XMFLOAT2( 0.f, 1.f );

				penetrationOut = y_overlap;				
			}

			return true;
		}
	}
	

	return false;
}

void 
TileCollisionResolver::Separate( CollisionInfo& collisionInfo, DirectX::XMFLOAT2& movingNewPos, DirectX::XMFLOAT2& collidingNewPos )
{
	Mass* moving_mass_comp = MassComponent(collisionInfo.movingEntityId);
	Mass* colliding_mass_comp = MassComponent(collisionInfo.collidingEntityId);

	float moving_inv_mass = 0.f;
	float colliding_inv_mass = 0.f;

	if (moving_mass_comp)
		moving_inv_mass = moving_mass_comp->InverseMass();

	if (colliding_mass_comp)
		colliding_inv_mass = colliding_mass_comp->InverseMass();

	float mass_sum = moving_inv_mass + colliding_inv_mass;


	// Position adjustment	
	float correction = collisionInfo.penetration + mCollisionCorrectionSlack;
	
	movingNewPos = collisionInfo.collisionPosition;
	DirectX::XMFLOAT3 colliding_pos = mLevel->Entity(collisionInfo.collidingEntityId)->SpatializedComponent()->LocalPosition();
	collidingNewPos = DirectX::XMFLOAT2(colliding_pos.x, colliding_pos.y);

	// Separate objects depending on their mass.
	{
		float moving_factor = 1.f;
		float colliding_factor = 0.f;		
		
		if (mass_sum > 0.f)
		{
			moving_factor = moving_inv_mass / mass_sum;
			colliding_factor = colliding_inv_mass / mass_sum;
		}

		movingNewPos.x += correction * collisionInfo.normal.x * moving_factor;
		movingNewPos.y += correction * collisionInfo.normal.y * moving_factor;
		collidingNewPos.x -= correction * collisionInfo.normal.x * colliding_factor;
		collidingNewPos.y -= correction * collisionInfo.normal.y * colliding_factor;	
	}
}

bool 
TileCollisionResolver::StepThroughTrajectory( unsigned int const& id, ResolvedAxes const& resolvedAxes, DirectX::XMFLOAT2 const& current, 
									          DirectX::XMFLOAT2 const& start, DirectX::XMFLOAT2 const& end, 
											  DirectX::XMFLOAT2& newPosOut )
{
	// Movement information//////////////////////////////
	// Check horizontal direction
	bool neutral_x = false, 		 
		right_dir = false;

	float run = end.x - start.x;

	if ( IsEqual(run, 0.f) )
		neutral_x = true;
	else if (run > 0.f)
		right_dir = true;

	// Check vertical direction
	bool neutral_y = false, 		 
		up_dir = false;

	float rise = end.y - start.y;

	if ( IsEqual(rise, 0.f) )
		neutral_y = true;
	else if (rise > 0.f)
		up_dir = true;
	/////////////////////////////////////////////////////

	newPosOut = current;

	if( neutral_x && neutral_y )
		return true;

	// Step through the trajectory
	if (neutral_x)
	{
		if (resolvedAxes.y)
			return true;
		
		if (up_dir)
			newPosOut.y = current.y + mTrajectoryStepDelta;
		else
			newPosOut.y = current.y - mTrajectoryStepDelta;
	}		
	else
	{
		if (neutral_y && resolvedAxes.x)
			return true;

		assert( std::fabsf(run) > FLOAT_EPSILON );		
		float m = rise / run;
		float b = current.y - (m * current.x);

		if ( std::fabsf(run) > std::fabsf(rise) )
		{
			if (right_dir)
				newPosOut.x = current.x + mTrajectoryStepDelta;
			else
				newPosOut.x = current.x - mTrajectoryStepDelta;

			newPosOut.y = (m * newPosOut.x) + b;
		}
		else
		{
			if (up_dir)
				newPosOut.y = current.y + mTrajectoryStepDelta;
			else
				newPosOut.y = current.y - mTrajectoryStepDelta;

			if ( std::fabsf(m) < FLOAT_EPSILON )
				newPosOut.x = current.x;
			else
				newPosOut.x = (newPosOut.y - b) / m; 
		}

		if (resolvedAxes.x)
		{
			newPosOut.x = current.x;
			if (std::fabsf(rise) < 0.1f)
				return true;
		}
		if (resolvedAxes.y)
		{
			newPosOut.y = current.y;
			if (std::fabsf(run) < 0.1f)
				return true;
		}
	}

	bool exceeded = false;
	
	if (!neutral_x)
	{
		if (right_dir)
		{
			if (newPosOut.x >= end.x)
			{
				exceeded = true;

// 				OutputDebugMsg(to_string(id) + " has exceeded it's end pos RIGHT... " +
// 							   to_string(newPosOut.x) + "  >  " + to_string(end.x) + "\n");

				newPosOut.x = end.x;
			}
		}
		else
		{
			if (newPosOut.x <= end.x)
			{
				exceeded = true;

// 				OutputDebugMsg(to_string(id) + " has exceeded it's end pos LEFT... " +
// 							   to_string(newPosOut.x) + "  <  " + to_string(end.x) + "\n");

				newPosOut.x = end.x;
			}
		}
	}

	if (!neutral_y)
	{
		if (up_dir)
		{
			if (newPosOut.y >= end.y)
			{
				exceeded = true;

// 				OutputDebugMsg(to_string(id) + " has exceeded it's end pos UP... " +
// 							   to_string(newPosOut.y) + "  >  " + to_string(end.y) + "\n");

				newPosOut.y = end.y;
			}
		}
		else
		{
			if (newPosOut.y <= end.y)
			{
				exceeded = true;

// 				OutputDebugMsg(to_string(id) + " has exceeded it's end pos DOWN... " +
// 							   to_string(newPosOut.y) + "  <  " + to_string(end.y) + "\n");

				newPosOut.y = end.y;
			}
		}
	}

	return exceeded;
}

void 
TileCollisionResolver::UpdateGridPosition( unsigned int const& id, DirectX::XMFLOAT2 const& newPos )
{	
	for (auto iter = mLevel->mEntitiesGridPositions[id].begin();
		iter != mLevel->mEntitiesGridPositions[id].end();
		++iter)
		mLevel->mLevelGrid[*iter].erase(id);
	mLevel->mEntitiesGridPositions.erase(id);

	// the four points of the moving entity
	DirectX::XMFLOAT2 bl1 = newPos;
	DirectX::XMFLOAT2 tr1 = DirectX::XMFLOAT2( bl1.x + (mLevel->mTileEntities[id]->BoundedComponent()->AabbBounds().Extents.x * 2.f),
											   bl1.y + (mLevel->mTileEntities[id]->BoundedComponent()->AabbBounds().Extents.y * 2.f) );

	std::pair<int, int> coverage_start, coverage_end;
	coverage_start = mLevel->WorldToGridPosition(bl1);
	coverage_end = mLevel->WorldToGridPosition( tr1 );
	for (int i = coverage_start.first; i <= coverage_end.first; i++)
		for (int j = coverage_start.second; j <= coverage_end.second; j++)
		{
			std::pair<int, int> grid_pos(i, j);
			mLevel->mLevelGrid[grid_pos].insert(id);
			mLevel->mEntitiesGridPositions[id].insert(grid_pos);
		}
}

void 
TileCollisionResolver::DetectCollisions(std::map<unsigned int, DirectX::XMFLOAT2>& currentPositions, std::map<unsigned int, std::vector<CollisionInfo> >& collisionsOut)
{	
	for (auto moving_iter = currentPositions.begin();
		moving_iter != currentPositions.end();
		++moving_iter)
	{
		unsigned int const id = moving_iter->first;

		if (!mLevel->Entity(id))
			continue;

		// the four points of the entity we are testing against
		// the  extreme points of the moving entity
		DirectX::XMFLOAT2 bl1 = moving_iter->second;
		DirectX::XMFLOAT2 tr1 = DirectX::XMFLOAT2( bl1.x + (mLevel->mTileEntities[id]->BoundedComponent()->AabbBounds().Extents.x * 2.f),
												   bl1.y + (mLevel->mTileEntities[id]->BoundedComponent()->AabbBounds().Extents.y * 2.f) );

		// All tiled entities that are in grid tiles that span bl1 tr1
		std::set< unsigned int > entities_to_check;
		mLevel->EntitiesInTilesSpanningFromQuad(bl1, tr1, entities_to_check);
						
		// Go through the entities in this tile and check for intersection
		for (auto check_against_iter = entities_to_check.begin();
			check_against_iter != entities_to_check.end();
			++check_against_iter)
		{
			if (*check_against_iter == id)  // moving object can't collide with itself
				continue;

			if (!mLevel->Entity(*check_against_iter))
				continue;

			// the four points of the entity we are testing against
			DirectX::XMFLOAT2 bl2 = 
				DirectX::XMFLOAT2( mLevel->mTileEntities[*check_against_iter]->SpatializedComponent()->LocalPosition().x,
								   mLevel->mTileEntities[*check_against_iter]->SpatializedComponent()->LocalPosition().y );

			DirectX::XMFLOAT2 tr2 = DirectX::XMFLOAT2( bl2.x + ( mLevel->mTileEntities[*check_against_iter]->BoundedComponent()->AabbBounds().Extents.x * 2.f),
				bl2.y + ( mLevel->mTileEntities[*check_against_iter]->BoundedComponent()->AabbBounds().Extents.y * 2.f) );

			DirectX::XMFLOAT2 normal;
			float penetration;			

			if ( TileToTileIntersection( bl1, tr1, bl2, tr2, normal, penetration ) )
			{	
				// Let colliding entities know they have collided... allow slop
				CollisionInfo collision_info = { id, *check_against_iter, penetration, normal, moving_iter->second };
				collisionsOut[id].push_back(collision_info);				
			}									
		}			
	}	
}

bool 
TileCollisionResolver::ResolveCollisions( std::map<unsigned int, DirectX::XMFLOAT2>& currentPositions, std::map<unsigned int, DirectX::XMFLOAT2>& endPositions, 
	                                      std::map<unsigned int, std::vector<CollisionInfo> >& collisionsIn, std::map<unsigned int, ResolvedAxes>& resolvedOut )
{
	std::map<unsigned int, std::list< DirectX::XMFLOAT2 > > separation_positions;
	std::map<unsigned int, DirectX::XMFLOAT2 > collision_point;

	std::set<unsigned int> resolved;

	// If there was collisions, resolve them
	for ( auto moving_iter = collisionsIn.begin();
		  moving_iter != collisionsIn.end();
		  ++moving_iter )
	{
		for (auto collision_iter = moving_iter->second.begin();
			collision_iter != moving_iter->second.end();
			++collision_iter)
		{
			CollisionInfo col_info = (*collision_iter);			

			collision_point[moving_iter->first] = collision_iter->collisionPosition;	

			if (!mLevel->Entity(collision_iter->movingEntityId) ||
				!mLevel->Entity(collision_iter->collidingEntityId))
				continue;

			if ( mLevel->Entity(collision_iter->movingEntityId)->IsObstacle() &&
				 mLevel->Entity(collision_iter->collidingEntityId)->IsObstacle() )
			{
				DirectX::XMFLOAT2 moving_pos_sep, colliding_pos_sep;				
				Separate( col_info, moving_pos_sep, colliding_pos_sep );

				separation_positions[moving_iter->first].push_back(moving_pos_sep);													
				resolved.insert(moving_iter->first);		
												
				separation_positions[col_info.collidingEntityId].push_back(colliding_pos_sep);		
				resolved.insert(col_info.collidingEntityId);	

				if (col_info.normal.x)
				{
					resolvedOut[moving_iter->first].x = true;

					if ( currentPositions.end() != currentPositions.find(collision_iter->collidingEntityId) )
						resolvedOut[collision_iter->collidingEntityId].x = true;
				}
				if (col_info.normal.y)
				{
					resolvedOut[moving_iter->first].y = true;

					if ( currentPositions.end() != currentPositions.find(collision_iter->collidingEntityId) )
						resolvedOut[collision_iter->collidingEntityId].y = true;
				}
			}	
		}
	}

	// Calculate final positions
	for ( auto iter = resolved.begin();
		  iter != resolved.end();
		  ++iter )
	{	
		int id = *iter;


		float avg_pos_x = 0.f;
		float avg_pos_y = 0.f;

		for ( auto pos_iter = separation_positions[*iter].begin();
			  pos_iter != separation_positions[*iter].end(); 
			  ++pos_iter )
		{
			avg_pos_x += pos_iter->x;
			avg_pos_y += pos_iter->y;
		}

		avg_pos_x /= (float)separation_positions[*iter].size();
		avg_pos_y /= (float)separation_positions[*iter].size();

		if ( currentPositions.end() != currentPositions.find(*iter) )
			currentPositions[*iter] = DirectX::XMFLOAT2(avg_pos_x, avg_pos_y);							
		mLevel->Entity(*iter)->SpatializedComponent()->LocalPosition( DirectX::XMFLOAT3(avg_pos_x, avg_pos_y, 0.f) );
		UpdateGridPosition( *iter, DirectX::XMFLOAT2(avg_pos_x, avg_pos_y) );
	}	

	return !resolved.empty();
}

void 
TileCollisionResolver::ElasticCollisionResolution( Level* const level, CollisionInfo const& collisionInfo )
{
	/* based on: http://elancev.name/oliver/2D%20polygon.htm */

	unsigned int entity_id = collisionInfo.movingEntityId;
	unsigned int other_id = collisionInfo.collidingEntityId;

	Mass* mass_comp = MassComponent(entity_id);
	Mass* other_mass_comp = MassComponent(other_id);

	float inv_mass = 0.f;
	float other_inv_mass = 0.f;

	if (mass_comp)
		inv_mass = mass_comp->InverseMass();

	if (other_mass_comp)
		other_inv_mass = other_mass_comp->InverseMass();


	Velocity* vel_comp = VelocityComponent(entity_id);
	Velocity* other_vel_comp = VelocityComponent(other_id);

	float vel[2] = {0.f, 0.f};
	float other_vel[2] = {0.f, 0.f};

	if (vel_comp)
	{
		vel[0] = vel_comp->VelocityVector().x;
		vel[1] = vel_comp->VelocityVector().y;
	}

	if (other_vel_comp)
	{
		other_vel[0] = other_vel_comp->VelocityVector().x;
		other_vel[1] = other_vel_comp->VelocityVector().y;
	}

	// Get avg Elasticity and friction of colliding entities
	Elasticity* elasticity_comp = ElasticityComponent(entity_id);
	Elasticity* other_elasticity_comp = ElasticityComponent(other_id);
	Friction* friction_comp = FrictionComponent(entity_id);
	Friction* other_friction_comp = FrictionComponent(other_id);

	float elastic_value = 0.f;
	float friction_value = 0.f;
	float other_elastic_value = 0.f;
	float other_friction_value = 0.f;

	if (elasticity_comp)
		elastic_value += elasticity_comp->ElasticityValue();

	if (friction_comp)
		friction_value += friction_comp->FrictionValue();

	if (other_elasticity_comp)
		other_elastic_value += other_elasticity_comp->ElasticityValue();

	if (other_friction_comp)
		other_friction_value += other_friction_comp->FrictionValue();



	// The normal and tangent vec
	float v[2] = { vel[0] - other_vel[0],
				   vel[1] - other_vel[1] };


	float v_dot_n = ( (v[0] *  collisionInfo.normal.x) + (v[1] *  collisionInfo.normal.y) );
	if (v_dot_n > -0.1f) 
		return;

	DirectX::XMFLOAT2 vn(v_dot_n *  collisionInfo.normal.x, v_dot_n *  collisionInfo.normal.y);
	DirectX::XMFLOAT2 vt(v[0] - vn.x, v[1] - vn.y);

	// Velocity response
	DirectX::XMFLOAT2 resp_vel;
	resp_vel.x = vt.x * -(friction_value) + vn.x * -(1 + elastic_value);
	resp_vel.y = vt.y * -(friction_value) + vn.y * -(1 + elastic_value) ;

	DirectX::XMFLOAT2 other_resp_vel;
	other_resp_vel.x = vt.x * -(other_friction_value) + vn.x * -(1 + other_elastic_value);
	other_resp_vel.y = vt.y * -(other_friction_value) + vn.y * -(1 + other_elastic_value) ;


	float mass_sum = inv_mass + other_inv_mass;
	DirectX::XMFLOAT3 final_vel(vel[0], vel[1], 0.f);
	DirectX::XMFLOAT3 other_final_vel(other_vel[0], other_vel[1], 0.f);

	if (mass_sum > 0.f)
	{
		final_vel.x += resp_vel.x * inv_mass / mass_sum;
		final_vel.y += resp_vel.y * inv_mass / mass_sum;

		other_final_vel.x -= other_resp_vel.x * other_inv_mass / mass_sum;
		other_final_vel.y -= other_resp_vel.y * other_inv_mass / mass_sum;
	}
	else
	{
		final_vel.x += resp_vel.x * 0.5f;
		final_vel.y += resp_vel.y * 0.5f;

		other_final_vel.x -= other_resp_vel.x * 0.5f;
		other_final_vel.y -= other_resp_vel.y * 0.5f;
	}

	if ( std::fabsf(final_vel.x) < 0.1f )
		final_vel.x = 0.f;
	if ( std::fabsf(final_vel.y) < 0.1f )
		final_vel.y = 0.f;

	if ( std::fabsf(other_final_vel.x) < 0.1f )
		other_final_vel.x = 0.f;
	if ( std::fabsf(other_final_vel.y) < 0.1f )
		other_final_vel.y = 0.f;

	if (vel_comp && mLevel->Entity(entity_id)->IsElasticRigidBody())
		vel_comp->VelocityVector(final_vel);	

	if (other_vel_comp && mLevel->Entity(other_id)->IsElasticRigidBody())
		other_vel_comp->VelocityVector(other_final_vel);

// 	 	OutputDebugMsg(to_string(entity_id) + "orig vel = " + 				   
// 	 				to_string(vel[0]) + ", " + 
// 	 				to_string(vel[1]) + "other vel = " + 				   
// 	 				to_string(other_vel[0]) + ", " + 
// 	 				to_string(other_vel[1]) + "final vel = " + 				   
// 	 				to_string(final_vel.x) + ", " + 
// 	 				to_string(final_vel.y) + " normal = " + 				   
// 	 				to_string(collisionInfo.normal.x) + ", " + 
// 	 				to_string(collisionInfo.normal.y) + "  v_dot_n = " +
// 	 				to_string(v_dot_n) + "\n");
}
