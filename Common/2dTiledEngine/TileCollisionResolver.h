#pragma once

#include <vector>
#include <map> 
#include "EventManager/EventManager.h"

class Level;
struct CollisionInfo;

class TileCollisionResolver : EventSubscriber
{
public:
	TileCollisionResolver(Level* const level);
	~TileCollisionResolver(void);

private:
	struct ResolvedAxes {bool x, y; ResolvedAxes(){x = y = false;} };

	bool const
	TileToTileIntersection( DirectX::XMFLOAT2& movingTileMin, DirectX::XMFLOAT2& movingTileMax, 
							DirectX::XMFLOAT2& collidingTileMin, DirectX::XMFLOAT2& collidingTileMax, 							
							DirectX::XMFLOAT2& normalOut, float& penetrationOut);

	void
	SimulateMovement(MovingTilesMetadata* const movingTiles);

	virtual void
	OnEvent(std::string const& event, Metadata* const metadata);

	void 
	Separate( CollisionInfo& collisionInfo, DirectX::XMFLOAT2& movingNewPos, DirectX::XMFLOAT2& collidingNewPos );

	/// Returns true if newPosOut has exceeded end
	bool 
	StepThroughTrajectory(unsigned int const& id, ResolvedAxes const& resolvedAxes, DirectX::XMFLOAT2 const& current, 
						  DirectX::XMFLOAT2 const& start, DirectX::XMFLOAT2 const& end, 
	                      DirectX::XMFLOAT2& newPosOut);

	void
	UpdateGridPosition(unsigned int const& id, DirectX::XMFLOAT2 const& newPos);

	void
	DetectCollisions(std::map<unsigned int, DirectX::XMFLOAT2>& currentPositions, std::map<unsigned int, std::vector<CollisionInfo> >& collisionsOut);

	bool
	ResolveCollisions(std::map<unsigned int, DirectX::XMFLOAT2>& currentPositions, std::map<unsigned int, DirectX::XMFLOAT2>& endPositions, 
	                  std::map<unsigned int, std::vector<CollisionInfo> >& collisionsIn, std::map<unsigned int, ResolvedAxes>& resolvedOut);

	void
	ElasticCollisionResolution(Level* const level, CollisionInfo const& collisionInfo);

	Level* mLevel;

	float mCollisionCorrectionSlack, mCollisionSlop, mTrajectoryStepDelta;
};

