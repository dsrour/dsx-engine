#pragma once

#include <set>
#include <DirectXMath.h>
#include "Components/Bounded.h"
#include "Components/Spatialized.h"
#include "2dTiledEngine/Components/TiledEntity.h"

class Level;
struct MovingTilesMetadata;

/// BASE TILE ENTITY CLASS

struct CollisionInfo
{
	unsigned int movingEntityId;
	unsigned int collidingEntityId;
	float penetration;
	DirectX::XMFLOAT2 normal;
	DirectX::XMFLOAT2 collisionPosition; /// where it collided prior position resolution
};

class TileEntity
{
public:
	TileEntity(void);
	~TileEntity(void);

	std::shared_ptr<Bounded> const&
	BoundedComponent(void) const { return mBoundedComp; }

	std::shared_ptr<Spatialized> const&
	SpatializedComponent(void) const { return mSpatializedComp; }

	virtual void 
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size ) = 0;

	virtual void
	OnLevelRemoval( Level* const level ) = 0;

	unsigned int const
	EntityId(void) { return mTiledEntityComp->TiledEntityId(); }

	bool const
	IsObstacle(void) { return mTiledEntityComp->IsObstacle(); }

	bool const
	IsElasticRigidBody(void) { return mTiledEntityComp->IsElasticRigidBody(); }

	virtual void
	PreCollision(Level* const level, CollisionInfo const& collisionInfo) {}

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo) {}

	virtual void
	PostCollision(Level* const level, CollisionInfo const& collisionInfo) {}
	
protected:
	std::shared_ptr<Bounded> mBoundedComp;				// Used for collision
	std::shared_ptr<Spatialized> mSpatializedComp;		// Used for positioning
	std::shared_ptr<TiledEntity> mTiledEntityComp;		// Used to store entity id and for other systems to know that this is a "tiled" entity
};

