#include "BaseApp.h"
#include "2dTiledEngine/Systems/TileMovementSystem.h"
#include "2dTiledEngine/Systems/MovingPlatformSystem.h"
#include "SpriteAnimationSystem/SpriteAnimation.h"
#include "Utils/MathUtils.h"
#include "Level.h"

extern BaseApp* gApp;

Level::Level( unsigned int const tileSize, std::pair<int, int> const levelSize, std::shared_ptr<EventManager> eventMngr )
{
	mTileSize = tileSize;
	mLevelSize = levelSize;
		
	// Event manager
	mEventManager = eventMngr;

	// Level's collision resolver friend class (acts as a module)
	mCollisionResolver = new TileCollisionResolver(this);

	// Systems
	double target = 1.0 / 60.0;	
	
	std::shared_ptr<TileMovementSystem> tile_mov_sys( new TileMovementSystem(gApp->EntityMngr(), this) );
	mTileMovementSysId = gApp->SystemMngr()->AddSystem(tile_mov_sys);	
	gApp->SystemSchdlr()->StopSystem(mTileMovementSysId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mTileMovementSysId, target);

	std::shared_ptr<MovingPlatformSystem> mvng_platform_sys( new MovingPlatformSystem(gApp->EntityMngr(), this) );
	mMovingPlatformSystemId = gApp->SystemMngr()->AddSystem(mvng_platform_sys);	
	gApp->SystemSchdlr()->StopSystem(mMovingPlatformSystemId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mMovingPlatformSystemId, target);

	std::shared_ptr<SpriteAnimationSystem> sprt_anim__sys( new SpriteAnimationSystem(gApp->EntityMngr()) );
	mSpriteAnimationSystemId = gApp->SystemMngr()->AddSystem(sprt_anim__sys);	
	gApp->SystemSchdlr()->StopSystem(mSpriteAnimationSystemId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mSpriteAnimationSystemId, target);
}


Level::~Level(void)
{
	// Stop and remove systems
	gApp->SystemSchdlr()->StopSystem(mTileMovementSysId);
	gApp->SystemMngr()->RemoveSystem(mTileMovementSysId);

	gApp->SystemSchdlr()->StopSystem(mMovingPlatformSystemId);
	gApp->SystemMngr()->RemoveSystem(mMovingPlatformSystemId);

	gApp->SystemSchdlr()->StopSystem(mSpriteAnimationSystemId);
	gApp->SystemMngr()->RemoveSystem(mSpriteAnimationSystemId);

	// Call OnRemove for all tiles
	for ( auto iter = mTileEntities.begin();
		  iter != mTileEntities.end();
		  ++iter )
		  iter->second->OnLevelRemoval(this);

	if (mCollisionResolver)
		delete mCollisionResolver;
}

DirectX::XMFLOAT3 const
Level::GridToWorldPosition( std::pair<int, int> const gridPos )
{
	float x = (float)((mTileSize) * gridPos.first) - (gApp->WinWidth() / 2) ;
	float y = (float)((mTileSize) * gridPos.second) - (gApp->WinHeight() / 2);
	float z = 0.f;

	return DirectX::XMFLOAT3(x, y, z);
}

std::pair<int, int> const 
Level::WorldToGridPosition( DirectX::XMFLOAT2 const worldPos )
{
	int x = (int)( (worldPos.x + (gApp->WinWidth() / 2)) / (mTileSize) );
	int y = (int)( (worldPos.y + (gApp->WinHeight() / 2)) / (mTileSize) );

	//assert(x >= 0 && y >= 0);

	return std::pair<int, int>((unsigned int)x, (unsigned int)y);
}

void 
Level::InsertTileEntity( DirectX::XMFLOAT2 const& position, 
						 DirectX::XMFLOAT2 const& size,
						 std::shared_ptr<TileEntity> tileEntity )
{
	// Make sure the entity is valid
	assert(tileEntity);

	// Store the tile
	unsigned int id = tileEntity->EntityId();
	mTileEntities[id] = tileEntity;

	// Tell tile entity it's insertion positions
	tileEntity->OnLevelInsert(this, position, size);

	// @ this point, the entity should have init'ed itself and its bounds	
	// We can now figure out which tiles in the grid it belongs to
	std::pair<int, int> start, end;
	EntityGridCoverage(tileEntity.get(), start, end);

	std::set< std::pair<int, int> > grid_positions;

	for (int x = start.first; x <= end.first; x++)
		for (int y = start.second; y <= end.second; y++)
		{
			std::pair<int, int> grid_pos(x, y);
			grid_positions.insert(grid_pos);

			mLevelGrid[grid_pos].insert(id);
		}

	mEntitiesGridPositions[id] = grid_positions;
}

void 
Level::RemoveTileEntity( unsigned int const tileEntityId )
{
	// Make sure there is an actual tile
	if(mTileEntities.find(tileEntityId) != mTileEntities.end())
		mEntitiesToRemove.insert(tileEntityId);
}

void 
Level::EntityGridCoverage( TileEntity* const tileEntity, 
						   std::pair<int, int>& startRangeOut, 
						   std::pair<int, int>& endRangeOut )
{
	assert(nullptr != tileEntity);

	DirectX::BoundingBox aabb = tileEntity->BoundedComponent()->AabbBounds();
	DirectX::XMFLOAT3 pos = tileEntity->SpatializedComponent()->LocalPosition();

	DirectX::XMFLOAT2 center = DirectX::XMFLOAT2(aabb.Center.x, aabb.Center.y);
	DirectX::XMFLOAT2 extents = DirectX::XMFLOAT2(aabb.Extents.x, aabb.Extents.y);

	DirectX::XMFLOAT2 bottom_left_corner = DirectX::XMFLOAT2(pos.x, pos.y);
	DirectX::XMFLOAT2 top_right_corner = DirectX::XMFLOAT2( bottom_left_corner.x + aabb.Center.x + aabb.Extents.x,
															bottom_left_corner.y + aabb.Center.y + aabb.Extents.y);



	startRangeOut = WorldToGridPosition(bottom_left_corner);
	endRangeOut   = WorldToGridPosition(top_right_corner);
}

void 
Level::EntitiesInTilesSpanningFromQuad(DirectX::XMFLOAT2& bl1, DirectX::XMFLOAT2& tr1, std::set< unsigned int >& entitiesOut)
{	 
	auto range_start = WorldToGridPosition(bl1);
	auto range_end = WorldToGridPosition(tr1);

	for (int i = range_start.first; i <= range_end.first; i++)
		for (int j = range_start.second; j <= range_end.second; j++)
		{
			std::pair<int, int> grid_pos(i, j);
			entitiesOut.insert( mLevelGrid[grid_pos].begin(), mLevelGrid[grid_pos].end() );
		}
}

void 
Level::PostFrame( void )
{
	// Remove entities if needed
	for (auto rmv_iter = mEntitiesToRemove.begin();
		 rmv_iter != mEntitiesToRemove.end();
		 ++rmv_iter )
	{
		std::set< std::pair<int, int> >::iterator iter;
		for (iter = mEntitiesGridPositions[*rmv_iter].begin();
			iter != mEntitiesGridPositions[*rmv_iter].end();
			++iter)
			mLevelGrid[*iter].erase(*rmv_iter);

		mEntitiesGridPositions.erase(*rmv_iter);

		mTileEntities[*rmv_iter]->OnLevelRemoval(this);

		// Unstore the tile
		mTileEntities.erase(*rmv_iter);
	}
	mEntitiesToRemove.clear();
}



