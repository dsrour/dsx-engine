#pragma once

#include <map>
#include <set>
#include "2dTiledEngine/TileEntity.h"
#include "2dTiledEngine/TileCollisionResolver.h"
#include "EventManager/EventManager.h"

#define MOVING_TILES_EVENT "MOVING_TILES_EVENT"

struct MovingTilesMetadata : public Metadata
{	
	std::map<unsigned int, DirectX::XMFLOAT2> startPos; // tiled entity id -> start pos
	std::map<unsigned int, DirectX::XMFLOAT2> endPos;	// tiled entity id -> end pos
};

typedef std::map< std::pair<int, int>, std::set<unsigned int> > LEVEL_GRID;

class Level
{
public:	
	Level( unsigned int const tileSize, std::pair<int, int> const levelSize, std::shared_ptr<EventManager> eventMngr );
	~Level(void);

	/*  @param  Grid position of tile
	 *  @return Bottom left corner of specified tile in world space
	 */ 
	DirectX::XMFLOAT3 const
	GridToWorldPosition( std::pair<int, int> const gridPos );

	/*  @param  World Position
	 *  @return Associated Grid Position
	 */ 
	std::pair<int, int> const
	WorldToGridPosition( DirectX::XMFLOAT2 const worldPos );

	/*  @param  Entity
	 *  @param  Grid start
	 *  @param  Grid end
	 */
	void
	EntityGridCoverage( TileEntity* const tileEntity, 
						std::pair<int, int>& startRangeOut, 
						std::pair<int, int>& endRangeOut );

	unsigned int const
	TileSize(void) { return mTileSize; }

	std::pair<int, int> const
	LevelSize(void) { return mLevelSize; }

	void
	InsertTileEntity( DirectX::XMFLOAT2 const& position, 
					  DirectX::XMFLOAT2 const& size, 
					  std::shared_ptr<TileEntity> tileEntity );

	void
	RemoveTileEntity(unsigned int const tileEntityId);

	EventManager* const
	EventMngr(void) const { return mEventManager.get(); }

	TileEntity* const
	Entity(unsigned int const id) { if (mTileEntities.find(id) != mTileEntities.end()) return mTileEntities[id].get(); else return NULL; }

	void
	PostFrame(void);

private:
	friend TileCollisionResolver;

	void
	EntitiesInTilesSpanningFromQuad(DirectX::XMFLOAT2& bl1, DirectX::XMFLOAT2& tr1, std::set< unsigned int >& entitiesOut);
		

	unsigned int mTileSize;   // in pixels
	std::pair<int, int> mLevelSize;  // in terms of number of tiles

	LEVEL_GRID mLevelGrid;
	std::map< unsigned int, std::set< std::pair<int, int> > > mEntitiesGridPositions;

	std::map<unsigned int, std::shared_ptr<TileEntity> > mTileEntities;

	unsigned int mTileMovementSysId, mMovingPlatformSystemId, mSpriteAnimationSystemId;
	std::shared_ptr<EventManager> mEventManager;
		
	TileCollisionResolver* mCollisionResolver;

	std::set<unsigned int> mEntitiesToRemove;
};

