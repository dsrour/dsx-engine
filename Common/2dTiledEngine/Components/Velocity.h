/**  Velocity.h
 *
 *   For entities that are moving.
 */

#pragma once

#include <DirectXMath.h>
#include "Component.hpp"

class Velocity : public Component<Velocity>
{
public:    
	Velocity(void) { mVelocityVector = DirectX::XMFLOAT3(0.f, 0.f, 0.f); }

	Velocity( DirectX::XMFLOAT3 const& velocity) { VelocityVector(velocity); }
	    
    DirectX::XMFLOAT3&
    VelocityVector(void) { return mVelocityVector; } 

    void
	VelocityVector(DirectX::XMFLOAT3 const& velocity) { mVelocityVector = velocity; }
        
private:  
    DirectX::XMFLOAT3 mVelocityVector;
};