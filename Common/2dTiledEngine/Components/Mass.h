/**  Mass.h
 *
 *   For entities that are moving with mass.
 */

#pragma once

#pragma warning( disable : 4723)


#include <DirectXMath.h>
#include "Component.hpp"

class Mass : public Component<Mass>
{
public:    
	Mass(void) {mInvMass = 0.f;}

	Mass( float const& mass) { MassFactor(mass); }
	    
    float const&
    InverseMass(void) const { return mInvMass; } 

    void
	MassFactor(float const& mass) { if (std::fabsf(mass) < 0.01) mInvMass = 0.f; else mInvMass = 1.f/mass; }
        
private:  
    float mInvMass;
};