/**  Friction.h
 *
 *   For entities that are colliding.
 */

#pragma once

#include "Component.hpp"

class Friction : public Component<Friction>
{
public:    
	Friction(void) {mFriction = 0.f;}

	Friction( float const& friction) { FrictionValue(friction); }
	    
    float const&
    FrictionValue(void) const { return mFriction; } 

    void
	FrictionValue(float const& friction) { mFriction = friction; }
        
private:  
    float mFriction;
};