/**  TiledEntity.h
 *
 *   Represents a tiled entity
 */

#pragma once

#include "Component.hpp"

class TiledEntity : public Component<TiledEntity>
{
public:    
	TiledEntity( unsigned int const& id, bool const& isObstacle, bool const& isElasticRigidBody = true) 
	{ 
		mEntityId = id; 
		mIsObstacle = isObstacle; 
		mIsElasticRigidBody = true;
	}
	    
    unsigned int const&
    TiledEntityId(void) const { return mEntityId; } 

	bool const
	IsObstacle(void) const { return mIsObstacle; }

	void
	IsObstacle(bool const isObstacle) { mIsObstacle = isObstacle; }

	bool const
	IsElasticRigidBody(void) const { return mIsElasticRigidBody; }

	void
	IsElasticRigidBody(bool const isElasticRigidBody) { mIsElasticRigidBody = isElasticRigidBody; }

private:  
    unsigned int mEntityId;	
	bool mIsObstacle;
	bool mIsElasticRigidBody;
};