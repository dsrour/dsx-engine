/**  OnMovingPlatform.h
 *
 *   For entities that are standing on a moving platform. 
 */

#pragma once

#include <DirectXMath.h>
#include "Component.hpp"

class OnMovingPlatform : public Component<OnMovingPlatform>
{
public:    
	OnMovingPlatform(void) { mTiledPlatformId = -1; mOffset = DirectX::XMFLOAT2(0.f, 0.f); }
		    
    int&
    MovingPlatformId(void) { return mTiledPlatformId; } 

	DirectX::XMFLOAT2&
	Offset(void) { return mOffset; }
        
private:  
   int mTiledPlatformId;
   DirectX::XMFLOAT2 mOffset;
};