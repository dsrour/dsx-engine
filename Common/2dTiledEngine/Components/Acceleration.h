/**  Acceleration.h
 *
 *   For entities that are moving.
 */

#pragma once

#include <DirectXMath.h>
#include "Component.hpp"

class Acceleration : public Component<Acceleration>
{
public:    
	Acceleration(void) { mAccelerationVector = DirectX::XMFLOAT3(0.f, 0.f, 0.f); }

	Acceleration( DirectX::XMFLOAT3 const& acceleration) { AccelerationVector(acceleration); }
	    
    DirectX::XMFLOAT3&
    AccelerationVector(void) { return mAccelerationVector; } 

    void
	AccelerationVector(DirectX::XMFLOAT3 const& acceleration) { mAccelerationVector = acceleration; }

private:  
    DirectX::XMFLOAT3 mAccelerationVector;
};