#pragma once

#include "Component.hpp"

class Health : public Component<Health>
{
public:    
	Health(void) {mHealth = 0;}

    int&
    CurrentHealth(void) { return mHealth; } 
        
private:  
	int mHealth;
};