#pragma once

#include "Component.hpp"

class Damageable : public Component<Damageable>
{
public:    
	Damageable(void) { mInflictedDamage = 0; mHitNormal[0] = mHitNormal[1] = 0; }

    unsigned int&
    InflictedDamage(void) { return mInflictedDamage; } 

	int*
	HitNormal(void) { return mHitNormal; }
        
private:  
	unsigned int mInflictedDamage;
	int mHitNormal[2];
};