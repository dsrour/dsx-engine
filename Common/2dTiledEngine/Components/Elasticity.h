/**  Elasticity.h
 *
 *   For entities that are colliding.
 */

#pragma once

#include "Component.hpp"

class Elasticity : public Component<Elasticity>
{
public:    
	Elasticity(void) {mElasticity = 0.f;}

	Elasticity( float const& elasticity) { ElasticityValue(elasticity); }
	    
    float const&
    ElasticityValue(void) const { return mElasticity; } 

    void
	ElasticityValue(float const& elasticity) { mElasticity = elasticity; }
        
private:  
    float mElasticity;
};