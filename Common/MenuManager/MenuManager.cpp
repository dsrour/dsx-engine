#include "MenuManager.h"


MenuManager::MenuManager()
{
}


MenuManager::~MenuManager()
{
}

void 
MenuManager::AddMenu(std::string const& name, std::shared_ptr<Menu> menu)
{
	mMenus[name] = menu;
}

void 
MenuManager::RemoveMenu(std::string const& name)
{
	mMenus.erase(name);
}

void MenuManager::AdvanceMenus()
{
	for (auto iter : mMenus)
		iter.second->Advance();
}

void 
MenuManager::ResetMenus()
{
	for (auto iter : mMenus)
		iter.second->Reset();
}

void 
MenuManager::BroadcastEvent(std::string const& eventName)
{
	for (auto iter : mMenus)
		iter.second->OnEvent(eventName);
}
