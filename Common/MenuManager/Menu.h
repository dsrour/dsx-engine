#pragma once

#include <string>

class Menu
{
public:
	Menu();
	~Menu();

	// Return state back to defaults, release all resources.
	virtual void
	Reset() = 0;

	virtual void
	OnEvent(std::string const& eventName) {}

	virtual void
	Advance() {}

};

