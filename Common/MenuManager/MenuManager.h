#pragma once

#include <string>
#include <memory>
#include <map>
#include "MenuManager/Menu.h"

class MenuManager
{
public:
	MenuManager();
	~MenuManager();

	void
	AddMenu(std::string const& name, std::shared_ptr<Menu> menu);

	void
	RemoveMenu(std::string const& name);
	
	void 
	AdvanceMenus();

	void
	BroadcastEvent(std::string const& eventName);

	void
	ResetMenus();

private:
	std::map<std::string, std::shared_ptr<Menu> > mMenus;
};

