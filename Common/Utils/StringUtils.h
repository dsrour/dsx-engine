#pragma once

#include <DirectXMath.h>
#include <string>
#include <sstream>

std::string
XmlTagFloat4(std::string const& tag, DirectX::XMFLOAT4 const& float4);

std::string
XmlTagFloat3(std::string const& tag, DirectX::XMFLOAT3 const& float3);

std::string
XmlTagFloat2(std::string const& tag, DirectX::XMFLOAT2 const& float2);

std::string
XmlTagFloat(std::string const& tag, float const& val);

std::string
XmlTagInt(std::string const& tag, int const& val);

std::string
XmlTagStr(std::string const& tag, std::string const& strVal);


/// in: "File\\In\\Here\\file.exe"    out: "File\\In\\Here\\"
std::string
ExtractPath(std::string const& fileLocation);

/// in: "File\\In\\Here\\file.exe"    out: "file.exe"
std::string
ExtractFileFromPath(std::string const& fileLocation);
