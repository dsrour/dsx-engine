#include "Utils/MathUtils.h"


float 
RoundFloat( float const& value )
{
	float ret = value;

	if (ret > 0.f)
		ret += 0.5f;
	else
		ret -= 0.5f;

	int rounded = static_cast<int>(ret);

	return (float)rounded;
}

DirectX::XMFLOAT3 
RoundFloat3( DirectX::XMFLOAT3 const& vec )
{
	return DirectX::XMFLOAT3( RoundFloat(vec.x), RoundFloat(vec.y), RoundFloat(vec.z) );
}

DirectX::XMFLOAT2 
RoundFloat2( DirectX::XMFLOAT3 const& vec )
{
	return DirectX::XMFLOAT2( RoundFloat(vec.x), RoundFloat(vec.y) );
}

DirectX::XMFLOAT2 
RoundFloat2( DirectX::XMFLOAT2 const& vec )
{
	return DirectX::XMFLOAT2( RoundFloat(vec.x), RoundFloat(vec.y) );
}

bool const 
IsEqual( float const& x, float const& y )
{		
	return std::abs(x - y) <= FLOAT_EPSILON * std::abs(x);		
}

bool const 
IsEqual( DirectX::XMFLOAT2 const& x, DirectX::XMFLOAT2 const& y )
{		
	return IsEqual(x.x, y.x) && IsEqual(x.y, y.y);		
}

bool const 
IsEqual( DirectX::XMFLOAT3 const& x, DirectX::XMFLOAT3 const& y )
{		
	return IsEqual(x.x, y.x) && IsEqual(x.y, y.y) && IsEqual(x.z, y.z);		
}

