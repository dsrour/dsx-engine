#pragma once
#include <windows.h>
#include <shobjidl.h> 
#include <string>
#include <vector>
/// Commonly used dialogs such as load, save, etc...

std::wstring 
BasicFileOpen();

std::wstring
BasicFileOpen(std::wstring const& fileExtension, std::wstring const& path = L"");

std::vector<std::wstring>
BasicFileOpenMultiple(std::wstring const& fileExtension, std::wstring const& path = L"");

std::wstring
BasicFileSave(std::wstring const& path = L"");