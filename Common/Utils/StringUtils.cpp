#include "Utils/StringUtils.h"

std::string 
XmlTagFloat4(std::string const& tag, DirectX::XMFLOAT4 const& float4)
{
	std::string ret;
	std::ostringstream buff;
	buff.precision(10);

	ret += "<" + tag + "X>";
	buff << float4.x;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "X>";

	ret += "<" + tag + "Y>";
	buff << float4.y;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "Y>";

	ret += "<" + tag + "Z>";
	buff << float4.z;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "Z>";

	ret += "<" + tag + "W>";
	buff << float4.w;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "W>";

	return ret;
}

std::string 
XmlTagFloat3(std::string const& tag, DirectX::XMFLOAT3 const& float3)
{
	std::string ret;
	std::ostringstream buff;
	buff.precision(10);

	ret += "<" + tag + "X>";
	buff << float3.x;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "X>";

	ret += "<" + tag + "Y>";
	buff << float3.y;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "Y>";

	ret += "<" + tag + "Z>";
	buff << float3.z;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "Z>";

	return ret;
}

std::string
XmlTagFloat2(std::string const& tag, DirectX::XMFLOAT2 const& float2)
{
	std::string ret;
	std::ostringstream buff;
	buff.precision(10);

	ret += "<" + tag + "X>";
	buff << float2.x;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "X>";

	ret += "<" + tag + "Y>";
	buff << float2.y;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + "Y>";	

	return ret;
}

std::string
XmlTagFloat(std::string const& tag, float const& val)
{
	std::string ret;
	std::ostringstream buff;
	buff.precision(10);

	ret += "<" + tag + ">";
	buff << val;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + ">";

	return ret;
}

std::string
XmlTagInt(std::string const& tag, int const& val)
{
	std::string ret;
	std::ostringstream buff;
	buff.precision(10);

	ret += "<" + tag + ">";
	buff << val;
	ret += buff.str();
	buff.str("");
	ret += "</" + tag + ">";

	return ret;
}

std::string 
XmlTagStr(std::string const& tag, std::string const& strVal)
{
	std::string ret;
	
	ret += "<" + tag + ">";
	ret += strVal;	
	ret += "</" + tag + ">";

	return ret;
}

std::string 
ExtractPath(std::string const& fileLocation)
{
	std::string ret;
	auto found = fileLocation.find_last_of("/\\");
	if (found != std::string::npos)
		ret = fileLocation.substr(0, found + 1);
	return ret;
}

std::string ExtractFileFromPath(std::string const& fileLocation)
{
	std::string ret;
	auto found = fileLocation.find_last_of("/\\");
	if (found != std::string::npos)
		ret = fileLocation.substr(found + 1, std::string::npos);
	return ret;
}

