#pragma once

#include <DirectXMath.h>

#define FLOAT_EPSILON 0.00001f

float 
RoundFloat(float const& value);

DirectX::XMFLOAT3
RoundFloat3(DirectX::XMFLOAT3 const& vec);

DirectX::XMFLOAT2
RoundFloat2(DirectX::XMFLOAT2 const& vec);

DirectX::XMFLOAT2
RoundFloat2(DirectX::XMFLOAT3 const& vec);

bool const
IsEqual(float const& x, float const& y);

bool const 
IsEqual( DirectX::XMFLOAT2 const& x, DirectX::XMFLOAT2 const& y );

bool const 
IsEqual( DirectX::XMFLOAT3 const& x, DirectX::XMFLOAT3 const& y );
