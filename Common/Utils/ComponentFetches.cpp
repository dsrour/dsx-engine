#include "BaseApp.h"
#include "ComponentFetches.h"

extern BaseApp* gApp;

Renderable* const
RenderableFromEntity( unsigned int const& entityId )
{
	std::shared_ptr<IComponent> tmp_comp;
	Renderable* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Renderable::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<Renderable*>(tmp_comp.get());

	return comp;
}

Spatialized* const
SpatializedFromEntity( unsigned int const& entityId )
{
	std::shared_ptr<IComponent> tmp_comp;
	Spatialized* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Spatialized::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<Spatialized*>(tmp_comp.get());

	return comp;
}

DiffuseMapped* const
DiffuseMappedFromEntity( unsigned int const& entityId )
{
	std::shared_ptr<IComponent> tmp_comp;
	DiffuseMapped* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, DiffuseMapped::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<DiffuseMapped*>(tmp_comp.get());

	return comp;
}

NormalMapped* const NormalMappedFromEntity( unsigned int const& entityId )
{
	std::shared_ptr<IComponent> tmp_comp;
	NormalMapped* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, NormalMapped::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<NormalMapped*>(tmp_comp.get());

	return comp;
}

LightEmitting* const 
LightEmittingFromEntity( unsigned int const& entityId )
{
	std::shared_ptr<IComponent> tmp_comp;
	LightEmitting* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, LightEmitting::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<LightEmitting*>(tmp_comp.get());

	return comp;
}

FurMapped* const 
FurMappedFromEntity(unsigned int const& entityId)
{
	std::shared_ptr<IComponent> tmp_comp;
	FurMapped* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, FurMapped::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<FurMapped*>(tmp_comp.get());

	return comp;
}

ReflectionMapped* const
ReflectionMappedFromEntity(unsigned int const& entityId)
{
	std::shared_ptr<IComponent> tmp_comp;
	ReflectionMapped* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, ReflectionMapped::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<ReflectionMapped*>(tmp_comp.get());

	return comp;
}

Bounded* const 
BoundedFromEntity(unsigned int const& entityId)
{
	std::shared_ptr<IComponent> tmp_comp;
	Bounded* comp = nullptr;
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Bounded::GetGuid(), tmp_comp);
	if (tmp_comp)
		comp = static_cast<Bounded*>(tmp_comp.get());

	return comp;
}
