#include <windows.h>
#include "Utils/IOUtils.h"

void 
RemoveFromDirectories(std::vector<std::string>& directories)
{
	wchar_t dirs[10000];
	unsigned int iter = 0;

	for (auto dir : directories)
	{
		std::wstring wstr_dir(dir.begin(), dir.end());

		for (auto wch : wstr_dir)
			dirs[iter++] = wch;

		dirs[iter++] = L'\0';
	}
	dirs[iter++] = L'\0';

	SHFILEOPSTRUCT stSHFileOpStruct = { 0 };
	stSHFileOpStruct.wFunc = FO_DELETE;
	stSHFileOpStruct.pFrom = dirs;
	stSHFileOpStruct.pTo = NULL;
	stSHFileOpStruct.fFlags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NOCONFIRMMKDIR | FOF_SILENT;
	stSHFileOpStruct.fAnyOperationsAborted = FALSE;
	int nFileDeleteOprnRet = SHFileOperation(&stSHFileOpStruct);
}

