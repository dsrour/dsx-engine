#pragma once
/// Convenient methods to get specific components

// Components //
#include "Components/Bounded.h"
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/LightEmitting.h"
#include "Components/FurMapped.h"
#include "Components/ReflectionMapped.h"
////////////////


Renderable* const
RenderableFromEntity(unsigned int const& entityId);

Spatialized* const 
SpatializedFromEntity( unsigned int const& entityId );

DiffuseMapped* const 
DiffuseMappedFromEntity( unsigned int const& entityId );

NormalMapped* const 
NormalMappedFromEntity( unsigned int const& entityId );

LightEmitting* const 
LightEmittingFromEntity( unsigned int const& entityId );

FurMapped* const
FurMappedFromEntity(unsigned int const& entityId);

ReflectionMapped* const
ReflectionMappedFromEntity(unsigned int const& entityId);

Bounded* const
BoundedFromEntity(unsigned int const& entityId);