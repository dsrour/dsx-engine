#include "Utils/CommonWinDialogs.h"

std::wstring 
BasicFileOpen()
{
	std::wstring ret;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | 
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, 
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					// Display the file name to the user.
					if (SUCCEEDED(hr))
					{
						//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
						ret = pszFilePath;
						CoTaskMemFree(pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}		
	return ret;
}

std::wstring 
BasicFileOpen(std::wstring const& fileExtension, std::wstring const& path)
{
	std::wstring ret;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{		
		COMDLG_FILTERSPEC aFileTypes[] = { { fileExtension.c_str(), fileExtension.c_str() } };

		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));		

		if (SUCCEEDED(hr))
		{
			pFileOpen->SetFileTypes(1, aFileTypes);

			if (SUCCEEDED(hr))
			{
				if (L"" != path)
				{
					IShellItem* item;
					hr = SHCreateItemFromParsingName(
						path.c_str(),
						0,
						IID_IShellItem,
						reinterpret_cast<void**>(&item));

					hr = pFileOpen->SetFolder(item);
				}

				// Show the Open dialog box.
				hr = pFileOpen->Show(NULL);

				// Get the file name from the dialog box.
				if (SUCCEEDED(hr))
				{
					IShellItem *pItem;
					hr = pFileOpen->GetResult(&pItem);
					if (SUCCEEDED(hr))
					{
						PWSTR pszFilePath;
						hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

						// Display the file name to the user.
						if (SUCCEEDED(hr))
						{
							//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
							ret = pszFilePath;
							CoTaskMemFree(pszFilePath);
						}
						pItem->Release();
					}
				}
				pFileOpen->Release();
			}
		}
		CoUninitialize();
	}
	return ret;
}

std::wstring
BasicFileSave(std::wstring const& path)
{
	std::wstring ret;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileSave;

		// Create the FileSaveDialog object.
		hr = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_ALL,
			IID_IFileSaveDialog, reinterpret_cast<void**>(&pFileSave));

		if (SUCCEEDED(hr))
		{
			if (L"" != path)
			{
				IShellItem* item;
				hr = SHCreateItemFromParsingName(
					path.c_str(),
					0,
					IID_IShellItem,
					reinterpret_cast<void**>(&item));

				hr = pFileSave->SetFolder(item);
			}

			// Show the Open dialog box.
			hr = pFileSave->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileSave->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					// Display the file name to the user.
					if (SUCCEEDED(hr))
					{
						//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
						ret = pszFilePath;
						CoTaskMemFree(pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileSave->Release();
		}
		CoUninitialize();
	}
	return ret;
}

std::vector<std::wstring> 
BasicFileOpenMultiple(std::wstring const& fileExtension, std::wstring const& path)
{
	std::vector<std::wstring> ret;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		COMDLG_FILTERSPEC aFileTypes[] = { { fileExtension.c_str(), fileExtension.c_str() } };

		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Allow multi-selection in the common file dialog.
			DWORD dwOptions;
			hr = pFileOpen->GetOptions(&dwOptions);
			if (SUCCEEDED(hr))
			{
				hr = pFileOpen->SetOptions(dwOptions | FOS_ALLOWMULTISELECT);
			}

			pFileOpen->SetFileTypes(1, aFileTypes);

			if (SUCCEEDED(hr))
			{
				if (L"" != path)
				{
					IShellItem* item;
					hr = SHCreateItemFromParsingName(
							path.c_str(),
							0,
							IID_IShellItem,
							reinterpret_cast<void**>(&item));

					hr = pFileOpen->SetFolder(item);
				}

				// Show the Open dialog box.
				hr = pFileOpen->Show(NULL);

				// Get the file name from the dialog box.
				if (SUCCEEDED(hr))
				{
					IShellItemArray* pItems;
					hr = pFileOpen->GetResults(&pItems);
					if (SUCCEEDED(hr))
					{
						DWORD dwItemCount = 0;
						hr = pItems->GetCount(&dwItemCount);
						if (SUCCEEDED(hr))
						{
							for (DWORD dwItem = 0; dwItem < dwItemCount; dwItem++)
							{
								IShellItem* pItem;
								pItems->GetItemAt(dwItem, &pItem);
								
								PWSTR pszFilePath;
								hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

								// Display the file name to the user.
								if (SUCCEEDED(hr))
								{
									//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
									ret.push_back(pszFilePath);
									CoTaskMemFree(pszFilePath);
								}
							}
						}
						pItems->Release();
					}
				}
				pFileOpen->Release();
			}
		}
		CoUninitialize();
	}
	return ret;
}


