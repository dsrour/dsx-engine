#include "ScanDefines.h"

RWBuffer<int> Input		: register (u0);
RWBuffer<int> Output	: register (u1);
RWBuffer<int> BlockSums : register (u2);

cbuffer cbConstants : register(b0)
{
	int				  numElements;
	int			      secondPass;
	int2			  pad;
};

groupshared uint gTmp[NUM_ELEMENTS_PER_GROUP];


[numthreads(NUM_ELEMENTS_PER_GROUP / 2, 1, 1)]
void Scan(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	int index = (Gid.x * NUM_ELEMENTS_PER_GROUP) + (2 * GI);

	// I employ the Blelloch scan as explained here: http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html 

	if (index < numElements)
	{
		if (!secondPass)
		{
			gTmp[2 * GI] = Input[index];
			gTmp[2 * GI + 1] = Input[index + 1];
		}
		else
		{
			gTmp[2 * GI] = BlockSums[index];
			gTmp[2 * GI + 1] = BlockSums[index + 1];
		}
	}
	else
	{
		gTmp[2 * GI] = 0;
		gTmp[2 * GI + 1] = 0;
	}

	uint offset = 1;	
	for (uint d = NUM_ELEMENTS_PER_GROUP >> 1; d > 0; d >>= 1)
	{
		GroupMemoryBarrierWithGroupSync();
		if (GI < d)
		{
			uint ai = offset*(2 * GI + 1) - 1;
			uint bi = offset*(2 * GI + 2) - 1;

			gTmp[bi] += gTmp[ai];
		}

		offset *= 2;
	}

	if (GI == 0)
	{
		if (!secondPass)
		{
			BlockSums[Gid.x] = gTmp[NUM_ELEMENTS_PER_GROUP - 1];
		}
		gTmp[NUM_ELEMENTS_PER_GROUP - 1] = 0;
		//else
		//	gTmp[NUM_ELEMENTS_PER_GROUP - 1] = 0;
	}
		
	for (uint d2 = 1; d2 < NUM_ELEMENTS_PER_GROUP; d2 *= 2)
	{
		offset >>= 1;
		GroupMemoryBarrierWithGroupSync();

		if (GI < d2)
		{
			uint ai = offset*(2 * GI + 1) - 1;
			uint bi = offset*(2 * GI + 2) - 1;

			float t = gTmp[ai];
			gTmp[ai] = gTmp[bi];
			gTmp[bi] += t;
		}
	}

	GroupMemoryBarrierWithGroupSync();
	
	if (index < numElements)
	{
		if (!secondPass)
		{
			Output[index] = gTmp[2 * GI];
			Output[index + 1] = gTmp[2 * GI + 1];
		}
		else
		{
			BlockSums[index] = gTmp[2 * GI];
			BlockSums[index + 1] = gTmp[2 * GI + 1];
		}
	}
}