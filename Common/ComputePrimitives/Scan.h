#pragma once

#include <windows.h>
#include <d3d11_2.h>
#include <memory>
#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

// UAVS passed to the primitive must be SINT typed buffers.
// For now this primitive only does a prefix sum. 
// TODO:: extend primitive so that it can do other operations (mul, or, and, xor, min, max, etc...)
 
class ScanPrimitive
{
public:
	ScanPrimitive(unsigned int const& maxNumElements, ID3D11DeviceContext* const devContext, ID3D11Device* const dev);

	~ScanPrimitive();

	bool
	Scan(unsigned int const& numElements, ID3D11UnorderedAccessView* const in, ID3D11UnorderedAccessView* const out);
	
private:	
	unsigned int mMaxNumElements;

	ID3D11DeviceContext* mDevContextRef;

	struct D3DResources
	{
		D3D11_BUFFER_DESC mBlockSumsBuffDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC mBlockSumsUavDesc;
		ID3D11Buffer* mBlockSumsBuff;
		ID3D11UnorderedAccessView* mBlockSumsUav;
	} mD3dResources; 

	struct CbInitConstants
	{		
		int numElements;
		int secondPass;
		int pad[2];
	} mCbVars;
	DirectX::ConstantBuffer< CbInitConstants > mCb;

	ID3DBlob* mScanCsBlob;
	ID3D11ComputeShader* mScanCs;
	ID3DBlob* mAddSumsCsBlob;
	ID3D11ComputeShader* mAddSumsCs;
};
