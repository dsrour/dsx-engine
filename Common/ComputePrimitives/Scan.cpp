#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "ScanDefines.h"
#include "Scan.h"

ScanPrimitive::ScanPrimitive(unsigned int const& maxNumElements, ID3D11DeviceContext* const devContext, ID3D11Device* const dev)
{
	ZeroMemory(&mD3dResources, sizeof(mD3dResources));
	mMaxNumElements = maxNumElements;
	mDevContextRef = devContext;

	mCb.Create(dev);
	ZeroMemory(&mCbVars, sizeof(mCbVars));

	// The intermediate buffer has as many elements as the amount of blocks launched for the scan
	unsigned int const num_blocks_sum_elements = ((maxNumElements + (maxNumElements % NUM_ELEMENTS_PER_GROUP)) / NUM_ELEMENTS_PER_GROUP) + 1;
	assert(num_blocks_sum_elements < NUM_ELEMENTS_PER_GROUP);

	mD3dResources.mBlockSumsBuffDesc.ByteWidth = (UINT)(sizeof(int) * num_blocks_sum_elements);
	mD3dResources.mBlockSumsBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mD3dResources.mBlockSumsBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
	mD3dResources.mBlockSumsBuffDesc.CPUAccessFlags = 0;
	mD3dResources.mBlockSumsBuffDesc.MiscFlags = 0;
	mD3dResources.mBlockSumsBuffDesc.StructureByteStride = (UINT)(sizeof(int));
	HRESULT hr = dev->CreateBuffer(&mD3dResources.mBlockSumsBuffDesc, NULL, &mD3dResources.mBlockSumsBuff);
	assert(SUCCEEDED(hr));

	mD3dResources.mBlockSumsUavDesc.Format = DXGI_FORMAT_R32_SINT;
	mD3dResources.mBlockSumsUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mD3dResources.mBlockSumsUavDesc.Buffer.FirstElement = 0;
	mD3dResources.mBlockSumsUavDesc.Buffer.Flags = 0;
	mD3dResources.mBlockSumsUavDesc.Buffer.NumElements = num_blocks_sum_elements;
	hr = dev->CreateUnorderedAccessView(mD3dResources.mBlockSumsBuff, &mD3dResources.mBlockSumsUavDesc, &mD3dResources.mBlockSumsUav);
	assert(SUCCEEDED(hr));

	std::wstring cso_name;
	
	cso_name = std::wstring(L"Scan_CS.cso");
	hr = D3DReadFileToBlob(cso_name.c_str(), &mScanCsBlob);
	assert(SUCCEEDED(hr));
	hr = dev->CreateComputeShader(
		mScanCsBlob->GetBufferPointer(),
		mScanCsBlob->GetBufferSize(),
		NULL,
		&mScanCs);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(L"Scan_AddSums_CS.cso");
	hr = D3DReadFileToBlob(cso_name.c_str(), &mAddSumsCsBlob);
	assert(SUCCEEDED(hr));
	hr = dev->CreateComputeShader(
		mAddSumsCsBlob->GetBufferPointer(),
		mAddSumsCsBlob->GetBufferSize(),
		NULL,
		&mAddSumsCs);
	assert(SUCCEEDED(hr));
}

ScanPrimitive::~ScanPrimitive()
{
	if (mScanCsBlob)
		mScanCsBlob->Release();
	if (mScanCs)
		mScanCs->Release();

	if (mAddSumsCsBlob)
		mAddSumsCsBlob->Release();
	if (mAddSumsCs)
		mAddSumsCs->Release();

	if (mD3dResources.mBlockSumsUav)
		mD3dResources.mBlockSumsUav->Release();
	if (mD3dResources.mBlockSumsBuff)
		mD3dResources.mBlockSumsBuff->Release();
}

bool 
ScanPrimitive::Scan(unsigned int const& numElements, ID3D11UnorderedAccessView* const in, ID3D11UnorderedAccessView* const out)
{
	if (numElements < mMaxNumElements ||
		nullptr == in ||
		nullptr == out ||
		nullptr == mDevContextRef)
		return false;

	{
		mCbVars.numElements = numElements;
		mCbVars.secondPass = 0;
		mCb.SetData(mDevContextRef, mCbVars);
		auto cb = mCb.GetBuffer();
		mDevContextRef->CSSetConstantBuffers(0, 1, &cb);
	}
		
	unsigned int init_cnts[] = { 0, 0, 0 };
	{
		ID3D11UnorderedAccessView* uavs[] = { in, out, mD3dResources.mBlockSumsUav };
		mDevContextRef->CSSetUnorderedAccessViews(0, 3, uavs, init_cnts);
	}	
	mDevContextRef->CSSetShader(mScanCs, nullptr, 0);
	auto first_pass_num_dispatches = (UINT)std::ceil(numElements / (float)NUM_ELEMENTS_PER_GROUP);
	mDevContextRef->Dispatch(first_pass_num_dispatches, 1, 1);


	{
		mCbVars.numElements = first_pass_num_dispatches;
		mCbVars.secondPass = 1;
		mCb.SetData(mDevContextRef, mCbVars);
		auto cb = mCb.GetBuffer();
		mDevContextRef->CSSetConstantBuffers(0, 1, &cb);
	}
	auto second_pass_num_dispatches = (UINT)std::ceil(mCbVars.numElements / (float)NUM_ELEMENTS_PER_GROUP);
	mDevContextRef->Dispatch(second_pass_num_dispatches, 1, 1);


	{
		mCbVars.numElements = numElements;
		mCbVars.secondPass = false;
		mCb.SetData(mDevContextRef, mCbVars);
		auto cb = mCb.GetBuffer();
		mDevContextRef->CSSetConstantBuffers(0, 1, &cb);
	}
	mDevContextRef->CSSetShader(mAddSumsCs, nullptr, 0);
	mDevContextRef->Dispatch(first_pass_num_dispatches, 1, 1);

	// Unset
	ID3D11Buffer* nb = nullptr;
	mDevContextRef->CSSetConstantBuffers(0, 1, &nb);
	{
		ID3D11UnorderedAccessView* uavs[] = { nullptr, nullptr, nullptr };
		mDevContextRef->CSSetUnorderedAccessViews(0, 3, uavs, init_cnts);
	}
	mDevContextRef->CSSetShader(nullptr, nullptr, 0);

	mDevContextRef->CSSetShader(nullptr, nullptr, 0);

	return true;
}
