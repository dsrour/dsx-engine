#include "ScanDefines.h"

RWBuffer<int> Input		: register (u0);
RWBuffer<int> Output	: register (u1);
RWBuffer<int> BlockSums : register (u2);

cbuffer cbConstants : register(b0)
{
	int				  numElements;
	int			      secondPass;
	int2			  pad;
};

groupshared int gGroupSum;

[numthreads(NUM_ELEMENTS_PER_GROUP / 2, 1, 1)]
void ScanAddSums(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
	if (0 == GI)
		gGroupSum = BlockSums[Gid.x];

	AllMemoryBarrierWithGroupSync();

	//if (Gid.x > 0)
	{
		int index = (Gid.x * NUM_ELEMENTS_PER_GROUP) + (2 * GI);
		//if (index < NUM_ELEMENTS_PER_GROUP)
		{
			Output[index] += gGroupSum;
			Output[index + 1] += gGroupSum;
		}
	}
}