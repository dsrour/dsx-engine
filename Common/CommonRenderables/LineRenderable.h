#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <vector>
#include "Components/Renderable.h"

class LineRenderable : public Renderable
{
public:    
	LineRenderable(ID3D11Device* d3dDevice, std::vector<DirectX::XMFLOAT3>& verts);
};