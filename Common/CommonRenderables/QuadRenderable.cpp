#include <vector>
#include "QuadRenderable.h"

QuadRenderable::QuadRenderable(ID3D11Device* d3dDevice, DirectX::XMFLOAT2& start, DirectX::XMFLOAT2& end)
{
	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	};

	std::vector<Vertex> verts;
	Vertex vert;
	vert.norm = DirectX::XMFLOAT3(0.f, 0.f, -1.f);

	vert.pos = DirectX::XMFLOAT3(start.x, start.y, 0.f);
	verts.push_back( vert );
	vert.pos = DirectX::XMFLOAT3(start.x, end.y, 0.f);
	verts.push_back( vert );
	vert.pos = DirectX::XMFLOAT3(end.x, end.y, 0.f);
	verts.push_back( vert );
	vert.pos = DirectX::XMFLOAT3(end.x, end.y, 0.f); 
	verts.push_back( vert );
	vert.pos = DirectX::XMFLOAT3(end.x, start.y, 0.f);
	verts.push_back( vert );
	vert.pos = DirectX::XMFLOAT3(start.x, start.y, 0.f);
	verts.push_back( vert );


	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> indices_to_gpu;
	for (unsigned int i = 0; i < (unsigned int)verts.size(); i++)
		indices_to_gpu.push_back(i);

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  
		
	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex) * verts.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &verts[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)verts.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    



	struct Texture
	{
		DirectX::XMFLOAT2 texCoords;
		DirectX::XMFLOAT3 tangent;

		Texture()
		{
			texCoords  = DirectX::XMFLOAT2(0.f, 0.f);
			tangent = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	};       

	std::vector< Texture > texture_data;
		
	{   
		DirectX::XMFLOAT2 coords;			
		Texture texture;
		texture.texCoords = DirectX::XMFLOAT2(0.f, 1.f); 
		texture.tangent = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		texture_data.push_back( texture );
	}
	{   
		DirectX::XMFLOAT2 coords;			
		Texture texture;
		texture.texCoords = DirectX::XMFLOAT2(0.f, 0.f); 
		texture.tangent = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		texture_data.push_back( texture );
	}
	{   
		DirectX::XMFLOAT2 coords;			
		Texture texture;
		texture.texCoords = DirectX::XMFLOAT2(1.f, 0.f); 
		texture.tangent = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		texture_data.push_back( texture );
		texture_data.push_back( texture );
	}
	{   
		DirectX::XMFLOAT2 coords;			
		Texture texture;
		texture.texCoords = DirectX::XMFLOAT2(1.f, 1.f); 
		texture.tangent = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		texture_data.push_back( texture );
	}
	{   
		DirectX::XMFLOAT2 coords;			
		Texture texture;
		texture.texCoords = DirectX::XMFLOAT2(0.f, 1.f); 
		texture.tangent = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		texture_data.push_back( texture );
	}

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Texture) * texture_data.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &texture_data[0];

	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Texture);
	vbuff_desc.offset = 0;                
	vbuff_desc.vertexCount = 4;

	mVertexBuffers[InputLayoutManager::TEXTURE] = vbuff_desc;	

	mRenderableType = LIT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
}