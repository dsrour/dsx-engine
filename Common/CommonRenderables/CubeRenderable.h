#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include "Components/Renderable.h"

class CubeRenderable : public Renderable
{
public:
	CubeRenderable(ID3D11Device* d3dDevice, float const& extents = 0.5f, bool const& lines = false);
};