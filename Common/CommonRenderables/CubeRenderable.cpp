#include <vector>
#include "CubeRenderable.h"

CubeRenderable::CubeRenderable(ID3D11Device* d3dDevice, float const& extents /*= 0.5f*/, bool const& lines /* = false*/)
{
	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	};

	std::vector<Vertex> unit_cube_verts(8);
	unit_cube_verts[0].pos = DirectX::XMFLOAT3(-0.5f, 0.5f, -0.5f);
	unit_cube_verts[1].pos = DirectX::XMFLOAT3(0.5f, 0.5f, -0.5f);
	unit_cube_verts[2].pos = DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f);
	unit_cube_verts[3].pos = DirectX::XMFLOAT3(-0.5f, 0.5f, 0.5f);
	unit_cube_verts[4].pos = DirectX::XMFLOAT3(-0.5f, -0.5f, -0.5f);
	unit_cube_verts[5].pos = DirectX::XMFLOAT3(0.5f, -0.5f, -0.5f);
	unit_cube_verts[6].pos = DirectX::XMFLOAT3(0.5f, -0.5f, 0.5f);
	unit_cube_verts[7].pos = DirectX::XMFLOAT3(-0.5f, -0.5f, 0.5f);

	std::vector<unsigned int> unit_cube_inds;
	if (lines)
	{
		unit_cube_inds = std::vector < unsigned int >
		{
			0,1,1,2,2,3,3,0,
			4,5,5,6,6,7,7,4,
			3,7,2,6,1,5,0,4
		};

		mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
	}
	else
	{
		unit_cube_inds = std::vector<unsigned int>
		{
			3, 1, 0,
			2, 1, 3,

			6, 4, 5,
			7, 4, 6,

			3, 4, 7,
			0, 4, 3,

			1, 6, 5,
			2, 6, 1,

			0, 5, 4,
			1, 5, 0,

			2, 7, 6,
			3, 7, 2
		};

		mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	}
	

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int)* unit_cube_inds.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &unit_cube_inds[0];
	d3dDevice->CreateBuffer(&bd, &InitData, &(mIndexBuffer.indexBuffer));
	mIndexBuffer.indexCount = (unsigned int)unit_cube_inds.size();

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex)* unit_cube_verts.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &unit_cube_verts[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = (unsigned int)unit_cube_verts.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;
	
	mRenderableType = LIT;
}
