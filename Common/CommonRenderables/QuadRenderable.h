#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include "Components/Renderable.h"

class QuadRenderable : public Renderable
{
public:    
	QuadRenderable(ID3D11Device* d3dDevice, DirectX::XMFLOAT2& start, DirectX::XMFLOAT2& end);
};