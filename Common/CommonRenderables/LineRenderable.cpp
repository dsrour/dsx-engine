#include <vector>
#include "LineRenderable.h"

LineRenderable::LineRenderable( ID3D11Device* d3dDevice, std::vector<DirectX::XMFLOAT3>& verts )
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> indices_to_gpu;
	for (unsigned int i = 0; i < (unsigned int)verts.size(); i++)
		indices_to_gpu.push_back(i);

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  
		
	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(DirectX::XMFLOAT3) * verts.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &verts[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(DirectX::XMFLOAT3);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)verts.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
}
