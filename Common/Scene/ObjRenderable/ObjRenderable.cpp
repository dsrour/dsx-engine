#include <assert.h>
#include <map>
#include <set>
#include <sstream>
#include "Systems/Renderer/MathHelper.h"
#include "Debug/Debug.h"
#include "Systems/Renderer/DirectXMesh/DirectXMesh/DirectXMesh.h"
#include "ObjRenderable.h"

bool const
ObjRenderable::Create(ID3D11Device* d3dDevice, tinyobj::shape_t& shape, std::vector<DirectX::XMFLOAT3>* vertsOut, bool const storeMetadata/* = false*/)
{
	mStoreMetadata = storeMetadata;
	bool ret = CreateRenderable(d3dDevice, shape, vertsOut);	
	return ret;
}

bool 
ObjRenderable::CreateRenderable(ID3D11Device* d3dDevice, tinyobj::shape_t& shape, std::vector<DirectX::XMFLOAT3>* vertsOut /*= NULL*/)
{
	// Gather statistics
	unsigned int num_indices = (unsigned int)shape.mesh.indices.size();
	assert(num_indices % 3 == 0);
	unsigned int num_tris = num_indices / 3;
	unsigned int num_verts = (unsigned int)shape.mesh.positions.size() / 3;
	unsigned int num_norms = (unsigned int)shape.mesh.normals.size() / 3;
	unsigned int num_tcoords = (unsigned int)shape.mesh.texcoords.size() / 2;
	if (num_norms > 0)
	{
		if (num_norms != num_verts)
		{
			OutputDebugMsg("Non-unified normals!\n");
			return false;
		}
	}
	if (num_tcoords > 0)
	{
		if (num_tcoords != num_verts)
		{
			OutputDebugMsg("Texture coordinates do not match the number of vertices!\n");
			return false;
		}
	}

	if (num_verts == 0 || num_indices == 0)
	{
		OutputDebugMsg("No geometry found!\n");
		return false;
	}
	
	// Convert from flat arrays to DirectXMath formats
	std::vector<unsigned int>      indices = shape.mesh.indices;
	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT3> normals;
	std::vector<DirectX::XMFLOAT2> texcoords;
	std::vector<DirectX::XMFLOAT3> tangents;

	for (size_t v = 0; v < num_verts; v++)
	{
		positions.push_back(DirectX::XMFLOAT3(shape.mesh.positions[3 * v + 0],
			shape.mesh.positions[3 * v + 1],
			shape.mesh.positions[3 * v + 2]));

		if (num_norms > 0)
			normals.push_back(DirectX::XMFLOAT3(shape.mesh.normals[3 * v + 0],
			shape.mesh.normals[3 * v + 1],
			shape.mesh.normals[3 * v + 2]));
	}

	if (num_tcoords > 0)
	{
		for (size_t t = 0; t < num_tcoords; t++)
		{
			texcoords.push_back(DirectX::XMFLOAT2(  1.f - shape.mesh.texcoords[2 * t + 0],
													1.f - shape.mesh.texcoords[2 * t + 1]));
			tangents.push_back(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
		}

		// Accumulate tangents (based on http://ogldev.atspace.co.uk/www/tutorial26/tutorial26.html)
		for (unsigned int i = 0; i < num_indices; i += 3)
		{
			DirectX::XMFLOAT3 vert0 = positions[indices[i + 0]];
			DirectX::XMFLOAT3 vert1 = positions[indices[i + 1]];
			DirectX::XMFLOAT3 vert2 = positions[indices[i + 2]];

			DirectX::XMFLOAT2 tc0 = texcoords[indices[i + 0]];
			DirectX::XMFLOAT2 tc1 = texcoords[indices[i + 1]];
			DirectX::XMFLOAT2 tc2 = texcoords[indices[i + 2]];

			DirectX::XMFLOAT3 edge1 = 
				DirectX::XMFLOAT3(
				vert1.x - vert0.x,
				vert1.y - vert0.y,
				vert1.z - vert0.z);

			DirectX::XMFLOAT3 edge2 = 
				DirectX::XMFLOAT3(
				vert2.x - vert0.x,
				vert2.y - vert0.y,
				vert2.z - vert0.z);

			float delta_U1 = tc1.x - tc0.x;
			float delta_V1 = tc1.y - tc0.y;
			float delta_U2 = tc2.x - tc0.x;
			float delta_V2 = tc2.y - tc0.y;

			float div = (delta_U1 * delta_V2 - delta_U2 * delta_V1);
			float f = 0.f;
			if (div > 0.001f || div < -0.001f)
				f = 1.f / div;

			DirectX::XMFLOAT3 tan;
			tan.x = f * (delta_V2 * edge1.x - delta_V1 * edge2.x);
			tan.y = f * (delta_V2 * edge1.y - delta_V1 * edge2.y);
			tan.z = f * (delta_V2 * edge1.z - delta_V1 * edge2.z);

			tangents[indices[i + 0]].x += tan.x;
			tangents[indices[i + 0]].y += tan.y;
			tangents[indices[i + 0]].z += tan.z;
			tangents[indices[i + 1]].x += tan.x;
			tangents[indices[i + 1]].y += tan.y;
			tangents[indices[i + 1]].z += tan.z;
			tangents[indices[i + 2]].x += tan.x;
			tangents[indices[i + 2]].y += tan.y;
			tangents[indices[i + 2]].z += tan.z;
		}

		// Normalize tangents
		for (unsigned int i = 0; i < tangents.size(); i++)
		{
			DirectX::XMVECTOR tan = XMLoadFloat3(&tangents[i]);
			tan = DirectX::XMVector3Normalize(tan);
			DirectX::XMFLOAT3 final_tan; DirectX::XMStoreFloat3(&final_tan, tan);
			tangents[i] = final_tan;
		}
	}

	assert((unsigned int)indices.size() == num_indices);
	assert((unsigned int)positions.size() == num_verts);
	if (num_norms > 0)
		assert((unsigned int)normals.size() == num_norms);
	if (num_tcoords > 0)
	{
		assert((unsigned int)texcoords.size() == num_tcoords);
		assert((unsigned int)tangents.size() == num_tcoords);
	}

	if (vertsOut)
	{
		vertsOut->clear();
		*vertsOut = positions;
	}




	// Create buffers
	

	std::vector<GeometryInputLayout> vertices;
	for (size_t v = 0; v < num_verts; v++)
	{
		GeometryInputLayout vert;
		vert.pos = positions[v];
		if (num_norms > 0)
			vert.norm = normals[v];		
		vertices.push_back(vert);
	}


	

	std::vector<TextureInputLayout> tcoords;
	if (num_tcoords > 0)
	{
		for (size_t t = 0; t < num_tcoords; t++)
		{
			TextureInputLayout tex;
			tex.texCoords = texcoords[t];
			tex.tangent = tangents[t];
			tcoords.push_back(tex);
		}
	}
	else // Make an empty buffer if no texture count
	{
		for (size_t v = 0; v < num_verts; v++)
		{
			TextureInputLayout tex;
			tcoords.push_back(tex);
		}
	}


	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int)* num_indices);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	if (mStoreMetadata)
	{
		mMetadata.indexMetadata.indexBuffer = std::vector<unsigned int>(indices.begin(), indices.end());
		mMetadata.indexMetadata.numIndices = num_indices;
		mMetadata.indexMetadata.indexBufferDesc = bd;
	}

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &indices[0];
	d3dDevice->CreateBuffer(&bd, &InitData, &(mIndexBuffer.indexBuffer));
	mIndexBuffer.indexCount = num_indices;



	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(GeometryInputLayout)* num_verts);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	if (mStoreMetadata)
	{
		mMetadata.geomMetadata.vertsBuffer = std::vector<GeometryInputLayout>(vertices.begin(), vertices.end());
		mMetadata.geomMetadata.numVerts = num_verts;
		mMetadata.geomMetadata.vertexBufferDesc = bd;
	}

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];
	VertexBufferDesc vbuff_desc;
	vbuff_desc.stride = sizeof(GeometryInputLayout);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = num_verts;
	d3dDevice->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));
	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;




	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(TextureInputLayout)* tcoords.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	if (mStoreMetadata)
 	{
		mMetadata.textureMetadata.textureBuffer = std::vector<TextureInputLayout>(tcoords.begin(), tcoords.end());
		mMetadata.textureMetadata.numCoords = (unsigned int)tcoords.size();
		mMetadata.textureMetadata.textureBufferDesc = bd;
	}


	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &tcoords[0];
	vbuff_desc.stride = sizeof(TextureInputLayout);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = (unsigned int)tcoords.size();
	d3dDevice->CreateBuffer(&bd, &InitData, &(vbuff_desc.vertexBuffer));
	mVertexBuffers[InputLayoutManager::TEXTURE] = vbuff_desc;


	// Set to lit if normals, flat otherwise
	num_norms > 0 ? mRenderableType = LIT : mRenderableType = FLAT;
	

	// Apply material
	ApplyMaterial(shape);

	
	// Create adjacency info index buffer
	std::vector<unsigned int>      adjacency(num_indices, 0);
	HRESULT hr = GenerateAdjacencyAndPointReps(&(indices[0]), num_tris, &(positions[0]), positions.size(), 0.f, nullptr, &(adjacency[0]));
	assert(SUCCEEDED(hr));

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int)* num_indices);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	if (mStoreMetadata)
	{
		mMetadata.adjacencyIndexMetadata.indexBuffer = std::vector<unsigned int>(adjacency.begin(), adjacency.end());
		mMetadata.adjacencyIndexMetadata.numIndices = num_indices;
		mMetadata.adjacencyIndexMetadata.indexBufferDesc = bd;
	}

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &adjacency[0];
	d3dDevice->CreateBuffer(&bd, &InitData, &(mAdjacencyIndexBuffer.indexBuffer));
	mIndexBuffer.indexCount = num_indices;



	return true;
}

void 
ObjRenderable::ApplyMaterial(tinyobj::shape_t& shape)
{
	Material mat;

	mat.ambient = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f);
	mat.diffuse = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f);
	mat.specular = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 0.f);	
	
	if (!shape.material.name.empty())
	{
		mat.ambient = DirectX::XMFLOAT4(
			shape.material.ambient[0],
			shape.material.ambient[1],
			shape.material.ambient[2],
			1.f);
		mat.diffuse = DirectX::XMFLOAT4(
			shape.material.diffuse[0],
			shape.material.diffuse[1],
			shape.material.diffuse[2],
			1.f);
		mat.specular = DirectX::XMFLOAT4(
			shape.material.specular[0],
			shape.material.specular[1],
			shape.material.specular[2],
			shape.material.shininess);
		if (shape.material.unknown_parameter["Lm"] == "PHONG")
			mat.lightingModel = PHONG;
		else if (shape.material.unknown_parameter["Lm"] == "CT")
			mat.lightingModel = COOK_TORRANCE;
		mat.reflectionCoeff = (float)std::atof(shape.material.unknown_parameter["Rc"].c_str());
		std::istringstream ss(shape.material.unknown_parameter["Rf"]);
		ss >> mat.reflect.x;
		ss >> mat.reflect.y;
		ss >> mat.reflect.z; 
		mat.reflect.z = 1.f;		
	}

	MaterialProperties(mat);		
}
