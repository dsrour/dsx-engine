#pragma once

#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>
#include <string>
#include "Components/Renderable.h"

#pragma warning(push)
#pragma warning(disable:4996) //4996 for _CRT_SECURE_NO_WARNINGS equivalent
#include "tiny_obj_loader.h"
#pragma warning(pop)

class ObjRenderable : public Renderable
{
public:    
	/* This function will create a Renderable component from a wavefront obj "shape_t" structure (found in tiny_obj_loader.h).
	* Note that the geometry must include normals that are unified (or shading will look flat).
	* The geometry may include texture coords if mapping will be used on the renderable.
	*
	* The material will also be set appropriately.
	*/
	bool const
	Create(ID3D11Device* d3dDevice, tinyobj::shape_t& shape, std::vector<DirectX::XMFLOAT3>* vertsOut = NULL, bool const storeMetadata = false);	

private:
	bool
	CreateRenderable(ID3D11Device* d3dDevice, tinyobj::shape_t& shape, std::vector<DirectX::XMFLOAT3>* vertsOut = NULL);
	
	void 
	ApplyMaterial(tinyobj::shape_t& shape);	
};
