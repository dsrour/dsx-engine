#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include <string>
#include "Components/Renderable.h"
#include "Scene/ObjRenderable/ObjRenderable.old/ObjLoader/objLoader.h"

class ObjRenderable : public Renderable
{
public:    
	/// @param vertsOut Will contain vertices' positions if a vector pointer is passed (if a vector is passed in, it will be cleared).
	bool const
    LoadObj(char* path, ID3D11Device* d3dDevice, std::vector<DirectX::XMFLOAT3>* vertsOut = NULL);

	std::string const
	Filename(void) {return mFilename;}
protected:
	std::string mFilename;  // path of opened obj
};
