#include <assert.h>
#include <map>
#include <set>
#include "Systems/Renderer/MathHelper.h"
#include "Debug/Debug.h"
#include "ObjRenderable.h"

bool const
ObjRenderable::LoadObj(char* path, ID3D11Device* d3dDevice, std::vector<DirectX::XMFLOAT3>* vertsOut)
{
	//OutputDebugMsg("Load file " + std::string(path) + "\n");
	objLoader* obj_data = new objLoader();
	if ( !obj_data->load(path) )
	{
		delete obj_data;
		return false;
	}

	//OutputDebugMsg("Load indices\n");
		
	mFilename = path;

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> v_indices;
	std::vector<unsigned int> vt_indices;

	//INDICES////////////////////////////////////////////////
	std::map< unsigned int, std::set<unsigned int> > obj_index_to_gpu_indices;
	std::vector<unsigned int> indices_to_gpu;
	for (int i = 0; i < obj_data->faceCount; i++)
	{
		v_indices.push_back( (unsigned int)(obj_data->faceList[i]->vertex_index[0]) );
		v_indices.push_back( (unsigned int)(obj_data->faceList[i]->vertex_index[1]) );
		v_indices.push_back( (unsigned int)(obj_data->faceList[i]->vertex_index[2]) );

		vt_indices.push_back( (unsigned int)(obj_data->faceList[i]->texture_index[0]) );
		vt_indices.push_back( (unsigned int)(obj_data->faceList[i]->texture_index[1]) );
		vt_indices.push_back( (unsigned int)(obj_data->faceList[i]->texture_index[2]) );

		indices_to_gpu.push_back(i*3+0);
		indices_to_gpu.push_back(i*3+1);
		indices_to_gpu.push_back(i*3+2);

		obj_index_to_gpu_indices[(unsigned int)(obj_data->faceList[i]->vertex_index[0])].insert(i*3+0);
		obj_index_to_gpu_indices[(unsigned int)(obj_data->faceList[i]->vertex_index[1])].insert(i*3+1);
		obj_index_to_gpu_indices[(unsigned int)(obj_data->faceList[i]->vertex_index[2])].insert(i*3+2);
	}        

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * v_indices.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );    

	mIndexBuffer.indexCount = (unsigned int)v_indices.size();  
	/////////////////////////////////////////////////////////////

	//OutputDebugMsg("Load Vertices\n");
	//GEOMETRY///////////////////////////////////////////////////
	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	};
	std::vector<DirectX::XMFLOAT3> v_tangents;

	if (vertsOut && vertsOut->size() > 0)
		vertsOut->clear();

	std::vector<Vertex> vertices;
	// Push back positions
	for (std::vector<unsigned int>::iterator iter = v_indices.begin(); iter != v_indices.end(); iter++)
	{
		Vertex vert;
		vert.pos = DirectX::XMFLOAT3( (float)(obj_data->vertexList[*iter]->e[0]), (float)(obj_data->vertexList[*iter]->e[1]), (float)(obj_data->vertexList[*iter]->e[2]) );

		if (vertsOut)
			vertsOut->push_back(vert.pos);

		vertices.push_back(vert);    

		v_tangents.push_back(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
	}

	//OutputDebugMsg("Load Normals+Tangents\n");
	// Calculate normals (by avg)
	if (0 != v_indices.size() % 3)
		return false; // Obj isn't made up of tri's            

	for (unsigned int i = 0; i < v_indices.size() / 3; i++)
	{
		// indices of ith tri
		unsigned int i0, i1, i2;
		i0 = i*3+0;
		i1 = i*3+1;
		i2 = i*3+2;
				
		// vertices of ith tri
		Vertex v0, v1, v2;
		v0 = vertices[i0];
		v1 = vertices[i1];
		v2 = vertices[i2];

		// positions
		DirectX::XMVECTOR p0 = XMLoadFloat3(&v0.pos);
		DirectX::XMVECTOR p1 = XMLoadFloat3(&v1.pos);
		DirectX::XMVECTOR p2 = XMLoadFloat3(&v2.pos);

		// face normal
		DirectX::XMVECTOR e0 = SubtractVectors(p1, p0);
		DirectX::XMVECTOR e1 = SubtractVectors(p2, p0);        
		DirectX::XMVECTOR xmv3_face_normal = DirectX::XMVector3Cross(e0, e1);

		DirectX::XMVECTOR tri_tangent;

		if (obj_data->textureCount > 0)
		{
			float u0 = ((float)obj_data->textureList[vt_indices[i1]]->e[0] - (float)obj_data->textureList[vt_indices[i0]]->e[0]);
			float v0 = ((float)obj_data->textureList[vt_indices[i1]]->e[1] - (float)obj_data->textureList[vt_indices[i0]]->e[1]);

			float u1 = ((float)obj_data->textureList[vt_indices[i2]]->e[0] - (float)obj_data->textureList[vt_indices[i0]]->e[0]);
			float v1 = ((float)obj_data->textureList[vt_indices[i2]]->e[1] - (float)obj_data->textureList[vt_indices[i0]]->e[1]);

			float e0x = DirectX::XMVectorGetX(e0);
			float e0y = DirectX::XMVectorGetY(e0);
			float e0z = DirectX::XMVectorGetZ(e0);

			float e1x = DirectX::XMVectorGetX(e1);
			float e1y = DirectX::XMVectorGetY(e1);
			float e1z = DirectX::XMVectorGetZ(e1);

			float inv = 1.0f / (u0*v1 -  v0*u1);

			float tan_x = inv*(v1*e0x - v0*e1x);
			float tan_y = inv*(v1*e0y - v0*e1y);
			float tan_z = inv*(v1*e0z - v0*e1z);  

			tri_tangent = DirectX::XMLoadFloat3(&DirectX::XMFLOAT3(tan_x, tan_y, tan_z));        
		}

		// accumulate for all neighboring vertices
		for (unsigned int j = 0; j < 3; j++)
		{
			for (std::set<unsigned int>::iterator iter = obj_index_to_gpu_indices[v_indices[i*3+j]].begin(); iter !=obj_index_to_gpu_indices[v_indices[i*3+j]].end(); iter++)
			{              
				DirectX::XMVECTOR xmv3_n = AddVectors(DirectX::XMLoadFloat3(&vertices[*iter].norm), xmv3_face_normal);            
				DirectX::XMFLOAT3 n; DirectX::XMStoreFloat3(&n, xmv3_n);                        
				vertices[*iter].norm = n;  

				if (obj_data->textureCount > 0)
				{
					DirectX::XMVECTOR xmv3_t = AddVectors(XMLoadFloat3(&v_tangents[*iter]), tri_tangent); 
					DirectX::XMFLOAT3 t; DirectX::XMStoreFloat3(&t, xmv3_t);          
					v_tangents[*iter] = t;
				}            
			}
		}
	}

	//OutputDebugMsg("Normalize Normals and Tangents\n");
	// run through vertices & normalize
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		DirectX::XMVECTOR normal = DirectX::XMLoadFloat3( &(vertices[i].norm) );        
		normal = DirectX::XMVector3Normalize(normal);        
		DirectX::XMFLOAT3 final_norm; DirectX::XMStoreFloat3(&final_norm, normal);
		vertices[i].norm = final_norm;
		//OutputDebugMsg(to_string(vertices[i].pos.x)+" "+to_string(vertices[i].pos.y)+" "+to_string(vertices[i].pos.z)+"\n");
		//OutputDebugMsg(to_string(vertices[i].norm.x)+" "+to_string(vertices[i].norm.y)+" "+to_string(vertices[i].norm.z)+"\n\n");

		if (obj_data->textureCount > 0)
		{
			DirectX::XMVECTOR tan = XMLoadFloat3(&v_tangents[i]);        
			tan = DirectX::XMVector3Normalize(tan);        
			DirectX::XMFLOAT3 final_tan; DirectX::XMStoreFloat3(&final_tan, tan);
			v_tangents[i] = final_tan;
		}
	}

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = obj_data->vertexCount;

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;
	/////////////////////////////////////////////////////////////
	//OutputDebugMsg("Load Text Coords\n");
	//TEX COORDS///////////////////////////////////////////////// 
	struct Texture
	{
		DirectX::XMFLOAT2 texCoords;
		DirectX::XMFLOAT3 tangent;

		Texture()
		{
			texCoords  = DirectX::XMFLOAT2(0.f, 0.f);
			tangent = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	};       

	std::vector< Texture > texture_data;

	if (obj_data->textureCount > 0)
	{
		for (std::vector<unsigned int>::iterator iter = vt_indices.begin(); iter != vt_indices.end(); iter++)
		{   
			DirectX::XMFLOAT2 coords;
			coords = DirectX::XMFLOAT2( (float)obj_data->textureList[*iter]->e[0], (float)obj_data->textureList[*iter]->e[1] );  
			Texture texture;
			texture.texCoords = coords;       
			texture.tangent = v_tangents[*iter];

			texture_data.push_back( texture );
		}
	}

	if (texture_data.empty()) // Make an empty buffer if no texture count
	{
		for (std::vector<unsigned int>::iterator iter = vt_indices.begin(); iter != vt_indices.end(); iter++)
		{   
			Texture tex;                 
			texture_data.push_back( tex );
		}
	}

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Texture) * texture_data.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &texture_data[0];

	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Texture);
	vbuff_desc.offset = 0;                
	vbuff_desc.vertexCount = obj_data->vertexCount;

	mVertexBuffers[InputLayoutManager::TEXTURE] = vbuff_desc;	       
	/////////////////////////////////////////////////////////////

	delete(obj_data);
	//OutputDebugMsg("Done\n");
	return true;
}