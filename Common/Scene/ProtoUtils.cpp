#include "Scene/ProtoUtils.h"

SoProto::BufferDesc* 
CreateProtoBufferDesc(D3D11_BUFFER_DESC const& bufferDesc)
{
	SoProto::BufferDesc* ret = new SoProto::BufferDesc;
	ret->set_bindflags(bufferDesc.BindFlags);
	ret->set_bytewidth(bufferDesc.ByteWidth);
	ret->set_cpuaccessflags(bufferDesc.CPUAccessFlags);
	ret->set_miscflags(bufferDesc.MiscFlags);
	ret->set_structurebytestride(bufferDesc.StructureByteStride);
	ret->set_usage(bufferDesc.Usage);
	return ret;
}

SoProto::SamplerDesc* 
CreateProtoSamplerDesc(D3D11_SAMPLER_DESC const& bufferDesc)
{
	SoProto::SamplerDesc* ret = new SoProto::SamplerDesc;
	ret->set_addressu(bufferDesc.AddressU);
	ret->set_addressv(bufferDesc.AddressV);
	ret->set_addressw(bufferDesc.AddressW);
	ret->set_bordercolor0(bufferDesc.BorderColor[0]);
	ret->set_bordercolor1(bufferDesc.BorderColor[1]);
	ret->set_bordercolor2(bufferDesc.BorderColor[2]);
	ret->set_bordercolor3(bufferDesc.BorderColor[3]);
	ret->set_comparefunc(bufferDesc.ComparisonFunc);
	ret->set_filter(bufferDesc.Filter);
	ret->set_maxaniso(bufferDesc.MaxAnisotropy);
	ret->set_maxlod(bufferDesc.MaxLOD);
	ret->set_minlod(bufferDesc.MinLOD);
	ret->set_miplodbias(bufferDesc.MipLODBias);
	return ret;
}

SoProto::Vec3* 
CreateProtoVec3(DirectX::XMFLOAT3 const& vec)
{
	SoProto::Vec3* ret = new SoProto::Vec3;
	ret->set_x(vec.x);
	ret->set_y(vec.y);
	ret->set_z(vec.z);
	return ret;
}

SoProto::Vec2* 
CreateProtoVec2(DirectX::XMFLOAT2 const& vec)
{
	SoProto::Vec2* ret = new SoProto::Vec2;
	ret->set_x(vec.x);
	ret->set_y(vec.y);
	return ret;
}

SoProto::Mat4x4*
CreateProtoMat4x4(DirectX::XMFLOAT4X4 const& mat)
{
	SoProto::Mat4x4* ret = new SoProto::Mat4x4;
	ret->set__11(mat._11);
	ret->set__12(mat._12);
	ret->set__13(mat._13);
	ret->set__14(mat._14);
	ret->set__21(mat._21);
	ret->set__22(mat._22);
	ret->set__23(mat._23);
	ret->set__24(mat._24);
	ret->set__31(mat._31);
	ret->set__32(mat._32);
	ret->set__33(mat._33);
	ret->set__34(mat._34);
	ret->set__41(mat._41);
	ret->set__42(mat._42);
	ret->set__43(mat._43);
	ret->set__44(mat._44);
	return ret;
}

void 
ShutDownPrototypeLibrary()
{
	google::protobuf::ShutdownProtobufLibrary();
}

D3D11_BUFFER_DESC 
CreateD3D11BufferDesc(SoProto::BufferDesc const& desc)
{
	D3D11_BUFFER_DESC ret;
	ret.BindFlags = desc.bindflags();
	ret.ByteWidth = desc.bytewidth();
	ret.CPUAccessFlags = desc.cpuaccessflags();
	ret.MiscFlags = desc.miscflags();
	ret.StructureByteStride = desc.structurebytestride();
	ret.Usage = (D3D11_USAGE)desc.usage();
	return ret;
}

D3D11_SAMPLER_DESC 
CreateD3D11SamplerDesc(SoProto::SamplerDesc const& desc)
{
	D3D11_SAMPLER_DESC ret;
	ret.AddressU = (D3D11_TEXTURE_ADDRESS_MODE)desc.addressu();
	ret.AddressV = (D3D11_TEXTURE_ADDRESS_MODE)desc.addressv();
	ret.AddressW = (D3D11_TEXTURE_ADDRESS_MODE)desc.addressw();
	ret.BorderColor[0] = desc.bordercolor0();
	ret.BorderColor[1] = desc.bordercolor1();
	ret.BorderColor[2] = desc.bordercolor2();
	ret.BorderColor[3] = desc.bordercolor3();
	ret.ComparisonFunc = (D3D11_COMPARISON_FUNC)desc.comparefunc();
	ret.Filter = (D3D11_FILTER)desc.filter();
	ret.MaxAnisotropy = desc.maxaniso();
	ret.MaxLOD = desc.maxlod();
	ret.MinLOD = desc.minlod();
	ret.MipLODBias = desc.miplodbias();
	return ret;
}

DirectX::XMFLOAT3 
CreateXMFloat3(SoProto::Vec3 const& vec)
{
	DirectX::XMFLOAT3 ret;
	ret.x = vec.x();
	ret.y = vec.y(); 
	ret.z = vec.z();
	return ret;
}

DirectX::XMFLOAT2 
CreateXMFloat2(SoProto::Vec2 const& vec)
{
	DirectX::XMFLOAT2 ret;
	ret.x = vec.x();
	ret.y = vec.y();
	return ret;
}

DirectX::XMFLOAT4X4 
CreateXMFloat4X4(SoProto::Mat4x4 const& mat)
{
	DirectX::XMFLOAT4X4 ret;
	ret._11 = mat._11();
	ret._12 = mat._12();
	ret._13 = mat._13();
	ret._14 = mat._14();
	ret._21 = mat._21();
	ret._22 = mat._22();
	ret._23 = mat._23();
	ret._24 = mat._24();
	ret._31 = mat._31();
	ret._32 = mat._32();
	ret._33 = mat._33();
	ret._34 = mat._34();
	ret._41 = mat._41();
	ret._42 = mat._42();
	ret._43 = mat._43();
	ret._44 = mat._44();
	return ret;
}



