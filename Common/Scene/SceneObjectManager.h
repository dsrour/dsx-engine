#pragma once
#include <map>
#include <set>
#include <memory>
#include "Components/Spatialized.h"
#include "Scene/SceneObject.h"
#include "BaseApp.h"

extern BaseApp* gApp;

class SceneObjectManager
{
public:
	SceneObjectManager(void);
	~SceneObjectManager(void);

	void
	AddSceneObject(std::shared_ptr<SceneObject>& sObj);

	void
	AddSceneObjects(std::vector< std::shared_ptr<SceneObject> >& sObjs);

	void
	RemoveSceneObject(unsigned int const sObjId);

	void
	RemoveSceneObjects(std::set<unsigned int>& sObjIds);

	// Selection
	void
	SelectSceneObjects(std::set<unsigned int> selection);
	void
	SelectSceneObject(unsigned int const& id);
	void
	UnselectSceneObjects(std::set<unsigned int> selection);
	void
	UnselectSceneObject(unsigned int const& id);
	void
	ToggleSceneObjects(std::set<unsigned int> selection);
	void
	ToggleSceneObject(unsigned int const& id);
	/////////////
	
	// Getters
	std::set<unsigned int> const&
	CurrentlySelectedSceneObjects() const { return mSelectedSceneObjectsIds; }

	std::set<unsigned int> const&
	SceneObjectsIds() const { return mSceneObjectsIds; }

	/// Returns a reference to a scene object.  Do not delete memory!
	std::shared_ptr<SceneObject> const&
	SceneObjectRef(unsigned int const& id)
	{		
		return mSceneObjects[id];
	}

	bool const
	IsSceneObjectSelected(unsigned int const entId) 
	{ 
		bool ret = (mSelectedSceneObjectsIds.find(entId) != mSelectedSceneObjectsIds.end()) ? true : false;
		return ret;
	}

private:		
	void 
	ChangeSceneObjectBoundBoxColor(unsigned int const& id, DirectX::XMFLOAT4 const& color) {mSceneObjects[id]->BoundingRenderableColor(color);}	

	std::map< unsigned int, std::shared_ptr<SceneObject> > mSceneObjects;
	std::set<unsigned int> mSceneObjectsIds; // keeping this around to make refactoring easier  TODO:: remove that shit
	std::set<unsigned int> mSelectedSceneObjectsIds;
};

