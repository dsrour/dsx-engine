#include <assert.h>
#include <algorithm>
#include "Debug/Debug.h"
#include "Scene/SceneObject.h"
#include "BaseApp.h"
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "Scene/ObjImporter.h"

extern BaseApp* gApp;

std::shared_ptr<SceneObject> 
ObjImporter::ImportFromFileSingle(
	std::wstring& fileName, 
	std::wstring& materialPath /*= std::wstring(L"")*/,
	bool const storeMetadata)
{
	std::shared_ptr<SceneObject> so(new SceneObject(storeMetadata));

	std::string file_name_str = to_string(fileName);
	std::string mat_path_str = to_string(materialPath);
	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, file_name_str.c_str(), mat_path_str.c_str());
	assert(err.empty());

	// Go through each shapes
	for (auto s_iter = shapes.begin(); s_iter != shapes.end(); ++s_iter)
	{
		AddSubEntityFromObjShapeToSo(so, *s_iter, file_name_str, mat_path_str, storeMetadata);
	}

	return so;
}

std::vector< std::shared_ptr<SceneObject> > 
ObjImporter::ImportFromFileMultiple(
	std::wstring& fileName, 
	std::wstring& materialPath /*= std::wstring(L"")*/,
	bool const storeMetadata)
{
	std::vector< std::shared_ptr<SceneObject> > sos;

	std::string file_name_str = to_string(fileName);
	std::string mat_path_str = to_string(materialPath);
	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, file_name_str.c_str(), mat_path_str.c_str());
	assert(err.empty());

	// Go through each shapes
	if (!shapes.empty())
	{
		for (auto s_iter = shapes.begin(); s_iter != shapes.end(); ++s_iter)
		{
			std::shared_ptr<SceneObject> so(new SceneObject(storeMetadata));
			AddSubEntityFromObjShapeToSo(so, *s_iter, file_name_str, mat_path_str, storeMetadata);
			sos.push_back(so);
		}
	}

	return sos;
}

void
ObjImporter::AddSubEntityFromObjShapeToSo(
	std::shared_ptr<SceneObject>& so,
	tinyobj::shape_t& shape,
	std::string& fileName,
	std::string& mtlPath,
	bool const storeMetadata)
{
	SceneObject::SubEntity se;
		
	se.entityId = gApp->EntityMngr()->CreateEntity();
	se.name = shape.name;

	se.renderableComp = std::make_shared<ObjRenderable>();
	std::vector<DirectX::XMFLOAT3> verts_pos;
	if (!static_cast<ObjRenderable*>(se.renderableComp.get())->Create(gApp->Renderer()->Device(), shape, &verts_pos, storeMetadata))
	{
		OutputDebugMsg("SceneObject: Can't CreateRenderablesFromObjFile, bad file passed in: \n " + fileName + "\n");
		return;
	}

	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.renderableComp);

	// Assign bounds & spatialized to sub-entity and update overall SceneObject bounds
	DirectX::BoundingBox aabb;
	DirectX::BoundingBox::CreateFromPoints(aabb, verts_pos.size(), &verts_pos[0], sizeof(DirectX::XMFLOAT3));
	se.boundedComp = std::make_shared<Bounded>(aabb);
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.boundedComp);
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, so->SpatializedComponent());

	// Is a diffuse map mentioned in the material?
	if (!(shape.material.diffuse_texname.empty()))
	{
		// NOTE:: assuming materialPath is in the same directory as the .obj model file
		std::string diff_map =
			mtlPath +
			shape.material.diffuse_texname;

		se.diffuseMappedComp = std::make_shared<DiffuseMapped>(diff_map);
		gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.diffuseMappedComp);
	}

	// Append SubEntity
	so->AddSubEntity(se);
}
