#pragma once

#define TIXML_USE_TICPP

#include <string>
#include <vector>
#include <memory>
#include "ticpp/ticpp.h"

class SceneObject;

class SoImporter
{
public:	
	/// Creates a single SO from a .so file.
	static std::shared_ptr<SceneObject>
	ImportFromFile(std::wstring& file, bool const storeMetadata = false);	

private:	
	static void
	ParseAndAddSubEntityToSo(
		std::shared_ptr<SceneObject>& so,
		ticpp::Iterator<ticpp::Element>& parent,
		std::string const& rootPath,
		std::string const& fileName,
		bool const storeMetadata = false);

	static void 
	ParseAndCreateRenderableAndBoundedComponents(		
		ticpp::Iterator<ticpp::Element>& parent, 
		std::string const& rootPath,
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<Renderable>& rOut, 
		std::shared_ptr<Bounded>& bOut,
		bool const storeMetadata /*= false*/ );

	static void
	ParseAndCreateDiffuseMappedComponent(
		std::string const& diffMapName,
		std::string const& rootPath,
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<DiffuseMapped>& dOut);

	static void
	ParseAndCreateNormalMappedComponent(
		std::string const& mapName,
		std::string const& rootPath,
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<NormalMapped>& nOut);

	static void
	ParseAndCreateReflectionMappedComponent(
		std::string const& mapName,
		std::string const& rootPath,
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<ReflectionMapped>& rOut);

	static void
	ParseAndAddTexXformComponentToSe(
		SceneObject::SubEntity& se,
		std::string const& rootPath,
		std::string const& fileName );

	static void
	ParseAndSetSpatializedInSo(
		std::shared_ptr<SceneObject>& so,
		ticpp::Iterator<ticpp::Element>& parent);

	static void
	ParseAndAddLightEmittingToSo(
		std::shared_ptr<SceneObject>& so,
		ticpp::Iterator<ticpp::Element>& parent);

	static void
	ParseAndAddShadowEmittingToSo(
		std::shared_ptr<SceneObject>& so,
		ticpp::Iterator<ticpp::Element>& parent);

	static void
	ParseAndAddFurComponentToSe(
		SceneObject::SubEntity& se,
		ticpp::Iterator<ticpp::Element>& parent);

	static void
	ParseAndAddShadowCastingComponentToSe(
		SceneObject::SubEntity& se,
		ticpp::Iterator<ticpp::Element>& parent);
};
