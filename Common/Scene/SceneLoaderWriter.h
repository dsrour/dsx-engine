#pragma once

#define TIXML_USE_TICPP

#include <vector>
#include "Scene/SceneObjectManager.h"
#include "Scene/SoImporter.h"
#include "Scene/SoExporter.h"
#include "SceneObject.h"
#include "ticpp/ticpp.h"

class SceneLoaderWriter
{
public:
	static bool const
	LoadSceneFromFile(
		std::string const& fileName,
		std::shared_ptr<SceneObjectManager> const& sceneObjManager,
		std::shared_ptr<ForwardRenderer> const& frwrdRenderer,
		bool const storeMetadata = false);

	static void 
	WriteSceneToFile(
		std::string const& fileName, 
		std::shared_ptr<SceneObjectManager> const& sceneObjManager,
		std::shared_ptr<ForwardRenderer> const& frwrdRenderer);

private:
	static void
	ParseAndSetFogToForwardRenderer(
		ticpp::Iterator<ticpp::Element>& parent,
		std::shared_ptr<ForwardRenderer> const& frwrdRenderer);
};

