#pragma once

#include <string>
#include <vector>
#include <memory>

#include "ObjRenderable/tiny_obj_loader.h"

class SceneObject;

class ObjImporter
{
public:	
	/// Creates a single SO from a Wavefront .obj file.
	static std::shared_ptr<SceneObject>
	ImportFromFileSingle(
		std::wstring& fileName, 
		std::wstring& materialPath = std::wstring(L""),
		bool const storeMetadata = false);

	/// Creates an SO for each geometry found in a Wavefront .obj file.
	static std::vector< std::shared_ptr<SceneObject> >
	ImportFromFileMultiple(
		std::wstring& fileName, 
		std::wstring& materialPath = std::wstring(L""),
		bool const storeMetadata = false);

private:	
	static void
	AddSubEntityFromObjShapeToSo(
		std::shared_ptr<SceneObject>& so, 
		tinyobj::shape_t& shape, 
		std::string& fileName, 
		std::string& mtlPath,
		bool const storeMetadata = false);
};

