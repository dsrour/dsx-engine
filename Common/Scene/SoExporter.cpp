#include <assert.h>
#include <fstream>
#include <iostream>
#include <d3d11.h>
#include "Debug/Debug.h"
#include "Scene/SceneObject.h"
#include "Utils/StringUtils.h"
#include "Utils/IOUtils.h"
#include "Scene/ProtoUtils.h"
#include "Scene/SoExporter.h"

// Proto buffers
#pragma warning( push ) 
#pragma warning( disable : 4267 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4996 )
#include "Scene/SceneObject.pb.h"
#pragma warning( pop ) 


void
SoExporter::ExportToFile(std::string const& file, std::vector< std::shared_ptr<SceneObject> >& sceneObjects)
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	std::string root_path = ExtractPath(file);	
	std::string file_name = ExtractFileFromPath(file);
	std::string renderable_dir = root_path + "Data\\";
	std::string maps_dir = root_path + "Maps\\";

	// Remove files
	std::vector<std::string> to_remove{ 
		renderable_dir + file_name + "*.il",
		root_path + "Maps\\" + file_name + "*diff_sampler.desc" ,
		root_path + "Maps\\" + file_name + "*norm_sampler.desc" ,
		root_path + "Maps\\" + file_name + "*refl_sampler.desc" ,
		root_path + "Maps\\" + file_name + "*tex_xform.mat" };	
	RemoveFromDirectories(to_remove);
	
	// Create directories
	CreateDirectory(
		std::wstring(renderable_dir.begin(), renderable_dir.end()).c_str(),
		NULL);
	CreateDirectory(
		std::wstring(maps_dir.begin(), maps_dir.end()).c_str(),
		NULL);

	// Get a unique xform
	DirectX::XMFLOAT4X4 xform = GetUniqueTransformMatrix(sceneObjects);

	// Get a unique light emitting and shadow emitting
	std::shared_ptr<LightEmitting> light_emitting = GetUniqueLightEmitting(sceneObjects);
	std::shared_ptr<ShadowEmitting> shadow_emitting = GetUniqueShadowEmitting(sceneObjects);


	std::string buffer;

	// First we need xml header tag
	buffer = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n\n";

	// Create an <SO> tag and record objects
	buffer += "<SceneObject>\n";

	// Export light & spatialized
	ExportTransformMatrix(buffer, xform);
	ExportLightEmitting(buffer, light_emitting); 
	ExportShadowEmitting(buffer, shadow_emitting);

	// Iterate over all SOs
	for (auto so : sceneObjects)
	{
		for (unsigned int i = 0; i < so->NumSubEntities(); i++)
		{ 
			auto se = so->SubEntityRefByIndex(i);
			buffer += "<SubEntity>\n";

			// Make the name unique
			se->name.erase(
				std::remove_if(se->name.begin(), se->name.end(), &isdigit),
				se->name.end());
			se->name = se->name + to_string(se->entityId);

			buffer += XmlTagStr("Name", se->name) + "\n";

			if (se->renderableComp)
				ExportRenderable(buffer, root_path, file_name, se->name, se->renderableComp);
			
			if (se->diffuseMappedComp)
				ExportDiffMap(buffer, root_path, file_name, se->name, se->diffuseMappedComp);
			
			if (se->normalMappedComp)
				ExportNormalMap(buffer, root_path, file_name, se->name, se->normalMappedComp);
			
			if (se->reflectionMappedComp)
				ExportReflectionMap(buffer, root_path, file_name, se->name, se->reflectionMappedComp);
			
			if (se->textureXformComp)
				ExportTexTransformed(buffer, root_path, file_name, se->name, se->textureXformComp);
			
			if (se->furComp)
				ExportFurMapped(buffer, se->furComp);

			if (se->shadowCastingComp)
				ExportShadowCasting(buffer, se->shadowCastingComp);
			
			buffer += "</SubEntity>\n";
		}
	}

	buffer += "</SceneObject>\n";

	std::ofstream o_stream(file);
	assert(o_stream.good());
	o_stream << buffer;
	o_stream.close();
}

void 
SoExporter::ExportRenderable(
	std::string& buffer, 
	std::string const& exportRootPath, 
	std::string const& fileName,
	std::string const& seName, 
	std::shared_ptr<Renderable> const& renderable)
{
	if (!renderable)
		return;

	if (!renderable->HasMetadata())
	{
		OutputDebugMsg("SoExporter::ExportRenderable: Can't export.  Renderable has no metadata saved.\n");
		return;
	}



	buffer += "<Renderable>\n";

	buffer += XmlTagInt("Type", renderable->RenderableType()) + "\n";

	Material mat = renderable->MaterialProperties();
	buffer += XmlTagFloat4("Amb", mat.ambient) + "\n";
	buffer += XmlTagFloat4("Diff", mat.diffuse) + "\n";
	buffer += XmlTagFloat4("Spec", mat.specular) + "\n";
	buffer += XmlTagFloat4("Refl", mat.reflect) + "\n";
	buffer += XmlTagFloat("ReflCoeff", mat.reflectionCoeff) + "\n";
	buffer += XmlTagInt("LModel", mat.lightingModel) + "\n";

	buffer += XmlTagInt("NumVerts", renderable->GetMetadata().geomMetadata.numVerts) + "\n";
	buffer += XmlTagInt("NumCoords", renderable->GetMetadata().textureMetadata.numCoords) + "\n";
	buffer += XmlTagInt("NumInd", renderable->GetMetadata().indexMetadata.numIndices) + "\n";

	buffer += XmlTagInt("BCull", renderable->BackCulled()) + "\n";

	std::string renderable_dir = exportRootPath + "Data\\";


	// Set up serialization //////////////////////////////////////////////////////////////////////////////////////////////
	SoProto::GeometryBufferMetadata geom_out;
	SoProto::IndexBufferMetadata    index_out;
	SoProto::TextureBufferMetadata  tex_out;

	{			
		geom_out.set_numverts(renderable->GetMetadata().geomMetadata.numVerts);
		geom_out.set_allocated_desc(CreateProtoBufferDesc(renderable->GetMetadata().geomMetadata.vertexBufferDesc));
		for (auto vert : renderable->GetMetadata().geomMetadata.vertsBuffer)
		{
			SoProto::GeometryInputLayout* geom_il = geom_out.add_geombuff();
			geom_il->set_allocated_pos(CreateProtoVec3(vert.pos));
			geom_il->set_allocated_norm(CreateProtoVec3(vert.norm));
		}
	}

	{		
		index_out.set_numindicies(renderable->GetMetadata().indexMetadata.numIndices);
		index_out.set_allocated_desc(CreateProtoBufferDesc(renderable->GetMetadata().indexMetadata.indexBufferDesc));
		for (auto vert : renderable->GetMetadata().indexMetadata.indexBuffer)		
			index_out.add_indexbuff(vert);		
	}

	{		
		tex_out.set_numcoords(renderable->GetMetadata().textureMetadata.numCoords);
		tex_out.set_allocated_desc(CreateProtoBufferDesc(renderable->GetMetadata().textureMetadata.textureBufferDesc));
		for (auto tex : renderable->GetMetadata().textureMetadata.textureBuffer)
		{
			SoProto::TextureInputLayout* tex_il = tex_out.add_texbuff();
			tex_il->set_allocated_texcoords(CreateProtoVec2(tex.texCoords));
			tex_il->set_allocated_tangent(CreateProtoVec3(tex.tangent));			
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Export
	std::ofstream of(renderable_dir + fileName + seName + "geom.il", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(geom_out.SerializeToOstream(&of));
	of.close();
	of.flush();

	of.open(renderable_dir + fileName + seName + "index.il", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(index_out.SerializeToOstream(&of));
	of.close();
	of.flush();

	of.open(renderable_dir + fileName + seName + "tex.il", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(tex_out.SerializeToOstream(&of));
	of.close();
	of.flush();


	buffer += "</Renderable>\n";
}

void 
SoExporter::ExportDiffMap(
	std::string& buffer, 
	std::string const& exportRootPath, 
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<DiffuseMapped> const& diffMap)
{
	if (!diffMap)
		return;

	std::string map_comes_from = diffMap->DiffuseMapName();
	std::string map_name = ExtractFileFromPath(map_comes_from);
	std::string export_to = exportRootPath + "Maps//" + map_name;

	CopyFile(
		std::wstring(map_comes_from.begin(), map_comes_from.end()).c_str(),
		std::wstring(export_to.begin(), export_to.end()).c_str(),
		FALSE);

	// Save sampler desc
	SoProto::DiffuseMapped proto_out;
	proto_out.set_allocated_desc(CreateProtoSamplerDesc(diffMap->DiffuseMapSamplerDesc()));
	std::ofstream of(exportRootPath + "Maps\\" + fileName + seName + "diff_sampler.desc", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(proto_out.SerializeToOstream(&of));
	of.close();
	of.flush();

	buffer += XmlTagStr("DiffMap", map_name) + "\n";
}

void 
SoExporter::ExportNormalMap(
	std::string& buffer, 
	std::string const& exportRootPath, 
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<NormalMapped> const& normalMap)
{
	if (!normalMap)
		return;

	std::string map_comes_from = normalMap->NormalMapName();
	std::string map_name = ExtractFileFromPath(map_comes_from);
	std::string export_to = exportRootPath + "Maps//" + map_name;

	CopyFile(
		std::wstring(map_comes_from.begin(), map_comes_from.end()).c_str(),
		std::wstring(export_to.begin(), export_to.end()).c_str(),
		FALSE);
		
	// Save sampler desc	
	SoProto::NormalMapped proto_out;
	proto_out.set_allocated_desc(CreateProtoSamplerDesc(normalMap->NormalMapSamplerDesc()));
	std::ofstream of(exportRootPath + "Maps\\" + fileName + seName + "norm_sampler.desc", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(proto_out.SerializeToOstream(&of));
	of.close();
	of.flush();

	buffer += XmlTagStr("NormMap", map_name) + "\n";
}

void 
SoExporter::ExportReflectionMap(
	std::string& buffer, 
	std::string const& exportRootPath, 
	std::string const& fileName,
	std::string const& seName, 
	std::shared_ptr<ReflectionMapped> const& reflectionMap)
{
	if (!reflectionMap)
		return;

	std::string map_comes_from = reflectionMap->ReflectionMapName();
	std::string map_name = ExtractFileFromPath(map_comes_from);
	std::string export_to = exportRootPath + "Maps//" + map_name;

	CopyFile(
		std::wstring(map_comes_from.begin(), map_comes_from.end()).c_str(),
		std::wstring(export_to.begin(), export_to.end()).c_str(),
		FALSE);

	// Save sampler desc	
	SoProto::NormalMapped proto_out;
	proto_out.set_allocated_desc(CreateProtoSamplerDesc(reflectionMap->ReflectionMapSamplerDesc()));
	std::ofstream of(exportRootPath + "Maps\\" + fileName + seName + "refl_sampler.desc", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(proto_out.SerializeToOstream(&of));
	of.close();
	of.flush();

	buffer += XmlTagStr("ReflMap", map_name) + "\n";
}

void 
SoExporter::ExportTexTransformed(
	std::string& buffer, 
	std::string const& exportRootPath, 
	std::string const& fileName,
	std::string const& seName, 
	std::shared_ptr<TextureTransformed> const& tt)
{
	if (!tt)
		return;

	// Save sampler desc
	SoProto::TextureTransformed proto_out;
	proto_out.set_allocated_xform(CreateProtoMat4x4(tt->TextureTransformation()));
	std::ofstream of(exportRootPath + "Maps\\" + fileName + seName + "tex_xform.mat", std::ofstream::binary | std::ofstream::out | std::ofstream::trunc);
	assert(of.good());
	assert(proto_out.SerializeToOstream(&of));
	of.close();
	of.flush();
}

void 
SoExporter::ExportLightEmitting(
	std::string& buffer, 	
	std::shared_ptr<LightEmitting> const& light)
{
	if (!light)
		return;

	auto p_light = light->GetLight();

	buffer += "<LightEmitting>\n";
	
	buffer += XmlTagInt("LightType", light->Type()) + "\n";
	switch (light->Type())
	{		
	case DIRECTIONAL_LIGHT:
	{
		DirectionalLight* dl = static_cast<DirectionalLight*>(p_light);
		buffer += XmlTagFloat4("Amb", dl->ambient) + "\n";
		buffer += XmlTagFloat4("Diff", dl->diffuse) + "\n";
		buffer += XmlTagFloat4("Spec", dl->specular) + "\n";
		buffer += XmlTagFloat3("Dir", dl->direction) + "\n";
		break;
	}
	case POINT_LIGHT:
	{
		PointLight* pl = static_cast<PointLight*>(p_light);
		buffer += XmlTagFloat4("Amb", pl->ambient) + "\n";
		buffer += XmlTagFloat4("Diff", pl->diffuse) + "\n";
		buffer += XmlTagFloat4("Spec", pl->specular) + "\n";
		buffer += XmlTagFloat3("Pos", pl->position) + "\n";
		buffer += XmlTagFloat("Range", pl->range) + "\n";
		buffer += XmlTagFloat3("Att", pl->attenuation) + "\n";
		break;
	}
	case SPOT_LIGHT:
	{
		SpotLight* sl = static_cast<SpotLight*>(p_light);
		buffer += XmlTagFloat4("Amb", sl->ambient) + "\n";
		buffer += XmlTagFloat4("Diff", sl->diffuse) + "\n";
		buffer += XmlTagFloat4("Spec", sl->specular) + "\n";
		buffer += XmlTagFloat3("Pos", sl->position) + "\n";
		buffer += XmlTagFloat("Range", sl->range) + "\n";
		buffer += XmlTagFloat3("Dir", sl->direction) + "\n";
		buffer += XmlTagFloat("Spot", sl->spot) + "\n";
		buffer += XmlTagFloat3("Att", sl->attenuation) + "\n";
		break;
	}
	default:
		break;
	}


	buffer += "</LightEmitting>\n";
}

DirectX::XMFLOAT4X4 
SoExporter::GetUniqueTransformMatrix(std::vector< std::shared_ptr<SceneObject> >& sceneObjects)
{
	DirectX::XMFLOAT4X4 ret;
	unsigned int num_different_mats = 0;
	
	for (auto so : sceneObjects)
	{
		if (num_different_mats == 0)
		{
			ret = so->SpatializedComponent()->LocalTransformation();
			num_different_mats++;
		}
		else
		{
			if (so->SpatializedComponent()->LocalTransformation()._11 != ret._11 ||
				so->SpatializedComponent()->LocalTransformation()._12 != ret._12 ||
				so->SpatializedComponent()->LocalTransformation()._13 != ret._13 ||
				so->SpatializedComponent()->LocalTransformation()._14 != ret._14 ||
				so->SpatializedComponent()->LocalTransformation()._21 != ret._21 ||
				so->SpatializedComponent()->LocalTransformation()._22 != ret._22 ||
				so->SpatializedComponent()->LocalTransformation()._23 != ret._23 ||
				so->SpatializedComponent()->LocalTransformation()._24 != ret._24 ||
				so->SpatializedComponent()->LocalTransformation()._31 != ret._31 ||
				so->SpatializedComponent()->LocalTransformation()._32 != ret._32 ||
				so->SpatializedComponent()->LocalTransformation()._33 != ret._33 ||
				so->SpatializedComponent()->LocalTransformation()._34 != ret._34 ||
				so->SpatializedComponent()->LocalTransformation()._41 != ret._41 ||
				so->SpatializedComponent()->LocalTransformation()._42 != ret._42 ||
				so->SpatializedComponent()->LocalTransformation()._43 != ret._43 ||
				so->SpatializedComponent()->LocalTransformation()._44 != ret._44)
				num_different_mats++;
		}
	}

	assert(num_different_mats != 0);
	if (num_different_mats > 1)
		OutputDebugMsg("SoExporter: Found multiple non-unique spatialized components.  Only 1 unique spatialized component will be exported.\n");

	return ret;
}

std::shared_ptr<LightEmitting> 
SoExporter::GetUniqueLightEmitting(std::vector< std::shared_ptr<SceneObject> >& sceneObjects)
{
	std::shared_ptr<LightEmitting> ret = nullptr;
	unsigned int num_lights = 0;

	for (auto so : sceneObjects)
	{
		if (so->EmitsLight())
		{
			num_lights++;
			ret = so->LightEmittingComponent();
		}
	}

	if (num_lights > 1)
		OutputDebugMsg("SoExporter: Found multiple lights.  Only 1 unique light emitting component will be exported.\n");

	return ret;
}

std::shared_ptr<ShadowEmitting>
SoExporter::GetUniqueShadowEmitting(std::vector< std::shared_ptr<SceneObject> >& sceneObjects)
{
	std::shared_ptr<ShadowEmitting> ret = nullptr;
	unsigned int num = 0;

	for (auto so : sceneObjects)
	{
		if (so->EmitsShadows())
		{
			num++;
			ret = so->ShadowEmittingComponent();
		}
	}

	if (num > 1)
		OutputDebugMsg("SoExporter: Found multiple shadow emitting components.  Only 1 unique shadow emitting component will be exported.\n");

	return ret;

}

void 
SoExporter::ExportTransformMatrix(
	std::string& buffer, 	
	DirectX::XMFLOAT4X4 const& mat)
{

	buffer += "<Spatialized>\n";

	buffer += XmlTagFloat("_11", mat._11) + "\n";
	buffer += XmlTagFloat("_12", mat._12) + "\n";
	buffer += XmlTagFloat("_13", mat._13) + "\n";
	buffer += XmlTagFloat("_14", mat._14) + "\n";
	
	buffer += XmlTagFloat("_21", mat._21) + "\n";
	buffer += XmlTagFloat("_22", mat._22) + "\n";
	buffer += XmlTagFloat("_23", mat._23) + "\n";
	buffer += XmlTagFloat("_24", mat._24) + "\n";

	buffer += XmlTagFloat("_31", mat._31) + "\n";
	buffer += XmlTagFloat("_32", mat._32) + "\n";
	buffer += XmlTagFloat("_33", mat._33) + "\n";
	buffer += XmlTagFloat("_34", mat._34) + "\n";

	buffer += XmlTagFloat("_41", mat._41) + "\n";
	buffer += XmlTagFloat("_42", mat._42) + "\n";
	buffer += XmlTagFloat("_43", mat._43) + "\n";
	buffer += XmlTagFloat("_44", mat._44) + "\n";
	
	buffer += "</Spatialized>\n";
}

void 
SoExporter::ExportFurMapped(
	std::string& buffer, 	
	std::shared_ptr<FurMapped> const& furMapped)
{
	if (!furMapped)
		return;

	buffer += "<FurMapped>\n";

	buffer += XmlTagInt("TextureSize", furMapped->TexturesSize()) + "\n";
	buffer += XmlTagInt("NumLayers", furMapped->NumLayers()) + "\n";
	buffer += XmlTagInt("StartDensity", furMapped->StartDensity()) + "\n";
	buffer += XmlTagInt("EndDensity", furMapped->EndDensity()) + "\n";
	buffer += XmlTagFloat("StartAlpha", furMapped->StartAlpha()) + "\n";
	buffer += XmlTagFloat("EndAlpha", furMapped->EndAlpha()) + "\n";
	buffer += XmlTagFloat("length", furMapped->FurLength()) + "\n";
	buffer += XmlTagFloat3("Force", furMapped->Force()) + "\n";

	buffer += "</FurMapped>\n";
}

void 
SoExporter::ExportShadowEmitting(std::string& buffer, std::shared_ptr<ShadowEmitting> const& shadowEmitting)
{
	if (!shadowEmitting)
		return;

	buffer += "<ShadowEmitting>\n";
		
	buffer += XmlTagInt("Size", shadowEmitting->ShadowMapSize()) + "\n";
	buffer += XmlTagFloat("StaticBias", shadowEmitting->StaticOffsetBias()) + "\n";
	buffer += XmlTagFloat("NormalScaleBias", shadowEmitting->NormalOffsetScaleBias()) + "\n";
	buffer += XmlTagInt("UsePlaneDepthBias", shadowEmitting->UsePlaneDepthBias()) + "\n";
	buffer += XmlTagFloat("ShadowIntensity", shadowEmitting->ShadowIntensity()) + "\n";

	buffer += "</ShadowEmitting>\n";
}

void 
SoExporter::ExportShadowCasting(std::string& buffer, std::shared_ptr<ShadowCasting> const& shadowCasting)
{
	if (!shadowCasting)
		return;

	buffer += "<ShadowCasting>\n";
	buffer += "</ShadowCasting>\n";
}




