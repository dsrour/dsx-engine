#pragma once

#include <windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

#pragma warning( push ) 
#pragma warning( disable : 4267 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4996 )
#include "Scene/SceneObject.pb.h"
#pragma warning( pop ) 

SoProto::BufferDesc*
CreateProtoBufferDesc(D3D11_BUFFER_DESC const& bufferDesc);

SoProto::SamplerDesc*
CreateProtoSamplerDesc(D3D11_SAMPLER_DESC const& bufferDesc);

SoProto::Vec3*
CreateProtoVec3(DirectX::XMFLOAT3 const& vec);

SoProto::Vec2*
CreateProtoVec2(DirectX::XMFLOAT2 const& vec);

SoProto::Mat4x4*
CreateProtoMat4x4(DirectX::XMFLOAT4X4 const& mat);


D3D11_BUFFER_DESC
CreateD3D11BufferDesc(SoProto::BufferDesc const& desc);

D3D11_SAMPLER_DESC
CreateD3D11SamplerDesc(SoProto::SamplerDesc const& desc);

DirectX::XMFLOAT3
CreateXMFloat3(SoProto::Vec3 const& vec);

DirectX::XMFLOAT2
CreateXMFloat2(SoProto::Vec2 const& vec);

DirectX::XMFLOAT4X4
CreateXMFloat4X4(SoProto::Mat4x4 const& mat);


/// LIBRARY IS UNUSABLE ONCE THIS IS CALLED!!!!!
void
ShutDownPrototypeLibrary();