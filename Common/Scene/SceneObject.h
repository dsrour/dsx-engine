#pragma once

#include <vector>

// Components //
#include "Components/Bounded.h"
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/ReflectionMapped.h"
#include "Components/LightEmitting.h"
#include "Components/TextureTransformed.h"
#include "Components/FurMapped.h"
#include "Components/ParticleEmitting.h"
#include "Components/LightEmitting.h"
#include "Components/ShadowEmitting.h"
#include "Components/ShadowCasting.h"
////////////////

class BoundingBoxRenderable : public Renderable
{
public:    
	BoundingBoxRenderable(ID3D11Device* d3dDevice);
	BoundingBoxRenderable(ID3D11Device* d3dDevice, DirectX::BoundingBox const& aabb);
};

class SceneObject
{
public:
	struct SubEntity
	{
		std::string							name;
		unsigned int						entityId;
		std::shared_ptr<Bounded>			boundedComp;
		std::shared_ptr<Renderable>			renderableComp;
		std::shared_ptr<DiffuseMapped>		diffuseMappedComp;
		std::shared_ptr<NormalMapped>		normalMappedComp;
		std::shared_ptr<ReflectionMapped>	reflectionMappedComp;
		std::shared_ptr<TextureTransformed>	textureXformComp;
		std::shared_ptr<FurMapped>			furComp;
		std::shared_ptr<ParticleEmitting>	particleEmittingComp;		
		std::shared_ptr<ShadowCasting>		shadowCastingComp;		
	};

	/// @param storeMetaData tells the SO if it should store metadata needed for saving SO to a file.
	SceneObject(bool const storeMetadata = false);
	~SceneObject(void);

	// QUERIES      //////////////////////////////////////////////////
	unsigned int const
	EntityId(void) const;

	unsigned int const
	NumSubEntities(void) const;
		
	SubEntity* const
	SubEntityRefByIndex(unsigned int const index);

	SubEntity* const
	SubEntityRefByName(std::string const& name);

	SubEntity* const
	SubEntityRefByEntityId(unsigned int const id);

	std::shared_ptr<Spatialized> const&
	SpatializedComponent();

	std::shared_ptr<LightEmitting> const&
	LightEmittingComponent();

	std::shared_ptr<ShadowEmitting> const&
	ShadowEmittingComponent();

	// Checks if any SubEntities have a renderable component set.
	bool const
	HasGeometry(void) const;

	bool const&
	IsVisible(void) const;

	bool const
	EmitsLight() const;

	bool const
	EmitsShadows() const;
	//////////////////////////////////////////////////////////////////


	// BOUNDING BOX //////////////////////////////////////////////////
	void
	BoundingRenderableColor(DirectX::XMFLOAT4 const& color);

	void
	ToggleBoundBox(bool const& show);

	void
	SetBounds(DirectX::BoundingBox const& aabb);
	//////////////////////////////////////////////////////////////////


	// SUB-ENTITIES MANIPULATION /////////////////////////////////////
	// NOTE: SubEntities are appended to mSubEntities vector.

	void
	SetVisibility(bool const& visible);

	
	void
	AddSubEntity(SubEntity const& se);

	/* Sets diff map to a SubEntity using a file.
	 * Only .dds files supported right now.
	 * Empty file name removes the map component.
	 */
	static void
	SetDiffuseMapFromFile(std::wstring& fileName, SubEntity* const subEntity);

	/* Set normal map to a SubEntity using a file.
	 * Only .dds files supported right now.
	 * Empty file name removes the map component.
	 */
	static void
	SetNormalMapFromFile(std::wstring& fileName, SubEntity* const subEntity);

	/* Set reflection map to a SubEntity using a file.
	 * Only .dds files supported right now.
	 * Empty file name removes the map component.
	 */
	static void
	SetReflectionMapFromFile(std::wstring& fileName, SubEntity* const subEntity);

	void
	SetShadowCasting(bool const shadowCasting);
	
	void 
	CreateInstancedRenderable(unsigned int const& batchId, DirectX::BoundingBox const& aabb, std::string const& name = "instanced");
		
	// If sceneEntityIndex == -1, it'll append a new sub entity... otherwise it'll replace the renderable component.
	void
	CreateQuad(DirectX::XMFLOAT2& min, DirectX::XMFLOAT2& max, int const sceneEntityIndex = -1, std::string const& name = "quad");
	
	// Toggles back culling for all renderables
	void
	ToggleBackCulling(bool const backCullOn);

	// Set material to all renderables
	void
	SetMaterial(Material& mat);

	void
	SetPrimitiveToPoints(void);

	void
	SetPrimitiveToTris(void);

	void
	SetLightEmitting(Light const& light, LightType const& lightType);

	void
	RemoveLightEmitting(void);

	void
	SetShadowEmitting(unsigned int const shadowMapSize);

	void
	RemoveShadowEmitting(void);

	/// Removes all sub-entities and reset bounds to unit length.
	void
	Reset();
	//////////////////////////////////////////////////////////////////	

protected:	
	unsigned int mEntityId; 

	// Bounds for debugging ///////////////////////////////
	unsigned int					mSoBoundsId;
	std::shared_ptr<Renderable>		mSoBoundsRenderable;
	std::shared_ptr<Bounded>		mBoundedComp;	
	std::shared_ptr<Spatialized>	mSpatializedComp;
	///////////////////////////////////////////////////////

	std::shared_ptr<LightEmitting>	mLightEmittingComp; // Do i emit light?
	std::shared_ptr<ShadowEmitting>	mShadowEmittingComp; // Do i emit shadows?

	// Will it be renderered?
	bool	mVisible;

	std::vector<SubEntity> mSubEntities;

	bool mStoreMetadata;
};

