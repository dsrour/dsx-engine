#include <algorithm>
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "CommonRenderables/QuadRenderable.h"
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "SceneObject.h"

extern BaseApp* gApp;

BoundingBoxRenderable::BoundingBoxRenderable(ID3D11Device* d3dDevice)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> indices_to_gpu;
	for (unsigned int i = 0; i < 24; i++)
		indices_to_gpu.push_back(i);

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  

	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
		}
	};

	std::vector<Vertex> vertices;
	Vertex vert;

	// Front face
	vert.pos = DirectX::XMFLOAT3( -0.5f, 0.5f, 0.5f ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  0.5f, 0.5f, 0.5f );
	vertices.push_back(vert);    
	vert.pos = DirectX::XMFLOAT3( -0.5f, -0.5f, 0.5f ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f, 0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f, 0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f,  0.5f, 0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f,-0.5f, 0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f, 0.5f, 0.5f );
	vertices.push_back(vert);

	// Back face
	vert.pos = DirectX::XMFLOAT3( -0.5f, 0.5f, -0.5f ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  0.5f, 0.5f, -0.5f );
	vertices.push_back(vert);    
	vert.pos = DirectX::XMFLOAT3( -0.5f, -0.5f, -0.5f ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f,  0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f,-0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f, 0.5f, -0.5f );
	vertices.push_back(vert);

	// Right connections
	vert.pos = DirectX::XMFLOAT3(  0.5f, 0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f, 0.5f,  0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  0.5f, -0.5f,  0.5f );
	vertices.push_back(vert);

	// Left connections
	vert.pos = DirectX::XMFLOAT3(  -0.5f, 0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f, 0.5f,  0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f, -0.5f, -0.5f );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  -0.5f, -0.5f,  0.5f );
	vertices.push_back(vert);


	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
}

BoundingBoxRenderable::BoundingBoxRenderable(ID3D11Device* d3dDevice, DirectX::BoundingBox const& aabb)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	std::vector<unsigned int> indices_to_gpu;
	for (unsigned int i = 0; i < 24; i++)
		indices_to_gpu.push_back(i);

	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(unsigned int) * indices_to_gpu.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;        
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &indices_to_gpu[0];
	d3dDevice->CreateBuffer( &bd, &InitData, &(mIndexBuffer.indexBuffer) );      

	mIndexBuffer.indexCount = (unsigned int)indices_to_gpu.size();  

	struct Vertex
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;

		Vertex()
		{
			pos  = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
			norm = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
		}
	};

	std::vector<Vertex> vertices;
	Vertex vert;

	// Front face
	vert.pos = DirectX::XMFLOAT3( aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y, aabb.Center.z+aabb.Extents.z ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);    
	vert.pos = DirectX::XMFLOAT3( aabb.Center.x-aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z+aabb.Extents.z ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y-aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y, aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);

	// Back face
	vert.pos = DirectX::XMFLOAT3( aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);    
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x,  aabb.Center.y-aabb.Extents.y,   aabb.Center.z-aabb.Extents.z ); 
	vertices.push_back(vert);  
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);

	// Right connections
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x+aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);

	// Left connections
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y+aabb.Extents.y,  aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z-aabb.Extents.z );
	vertices.push_back(vert);
	vert.pos = DirectX::XMFLOAT3(  aabb.Center.x-aabb.Extents.x, aabb.Center.y-aabb.Extents.y,  aabb.Center.z+aabb.Extents.z );
	vertices.push_back(vert);


	ZeroMemory( &bd, sizeof(bd) );
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;    
	ZeroMemory( &InitData, sizeof(InitData) );
	InitData.pSysMem = &vertices[0];

	VertexBufferDesc vbuff_desc;
	d3dDevice->CreateBuffer( &bd, &InitData, &(vbuff_desc.vertexBuffer) );

	vbuff_desc.stride = sizeof(Vertex);
	vbuff_desc.offset = 0;            
	vbuff_desc.vertexCount = (unsigned int)vertices.size();

	mVertexBuffers[InputLayoutManager::GEOMETRY] = vbuff_desc;	    

	mRenderableType = FLAT;

	mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
}

SceneObject::SceneObject(bool const storeMetadata)
{
	mStoreMetadata = storeMetadata;
	mVisible = true;

	mEntityId = gApp->EntityMngr()->CreateEntity();                  

	DirectX::BoundingBox aabb(DirectX::XMFLOAT3(0.f, 0.f, 0.f), DirectX::XMFLOAT3(0.5f,0.5f,0.5f));		
	mBoundedComp = std::make_shared<Bounded>(aabb);	
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mBoundedComp);

	mSpatializedComp = std::make_shared<Spatialized>( Spatialized() );	
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mSpatializedComp);

	mSoBoundsId = gApp->EntityMngr()->CreateEntity();    	
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mBoundedComp);
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSpatializedComp);
	mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>( gApp->Renderer()->Device() );		
	BoundingRenderableColor(DirectX::XMFLOAT4(0.8f, 0.8f, 0.8f, 1.f));			
	ToggleBoundBox(true);
}


SceneObject::~SceneObject(void)
{	
	if (gApp == nullptr)
		return;
	Reset();
	gApp->EntityMngr()->DestroyEntity(mSoBoundsId);
	gApp->EntityMngr()->DestroyEntity(mEntityId);	
}

unsigned int const 
SceneObject::EntityId(void) const
{
	return mEntityId;
}

unsigned int const
SceneObject::NumSubEntities(void) const
{
	return (unsigned int)mSubEntities.size();
}

SceneObject::SubEntity* const
SceneObject::SubEntityRefByIndex(unsigned int const index)
{
	if (index >= NumSubEntities())
		return nullptr;
	
	return &mSubEntities[index];
}

SceneObject::SubEntity* const
SceneObject::SubEntityRefByName(std::string const& name)
{
	SubEntity* ret = nullptr;
	
	for (auto iter = mSubEntities.begin(); iter != mSubEntities.end(); ++iter)
		if (iter->name == name)
		{
			ret = &(*iter);
			break;
		}

	return ret;
}

SceneObject::SubEntity* const
SceneObject::SubEntityRefByEntityId(unsigned int const id)
{
	SubEntity* ret = nullptr;

	for (auto iter = mSubEntities.begin(); iter != mSubEntities.end(); ++iter)
		if (iter->entityId == id)
		{
			ret = &(*iter);
			break;
		}

	return ret;
}

bool const 
SceneObject::HasGeometry(void) const
{
	for (auto iter = mSubEntities.begin(); iter != mSubEntities.end(); ++iter)
		if (iter->renderableComp)
			return true;

	return false;
}

void 
SceneObject::SetVisibility( bool const& visible )
{
	mVisible = visible;

	for (auto iter = mSubEntities.begin(); iter != mSubEntities.end(); ++iter)
	{
		if (!visible && iter->renderableComp)
			gApp->EntityMngr()->RemoveComponentFromEntity(iter->entityId, iter->renderableComp->GetComponentTypeGuid());
		else if (iter->renderableComp)
			gApp->EntityMngr()->AddComponentToEntity(iter->entityId, iter->renderableComp);
	}
}

bool const&
SceneObject::IsVisible(void) const
{
	return mVisible;
}

void 
SceneObject::BoundingRenderableColor( DirectX::XMFLOAT4 const& color )
{
	Material mat;
	mat.diffuse = color;
	mSoBoundsRenderable->MaterialProperties(mat);
}

void
SceneObject::ToggleBoundBox(bool const& show)
{
	show ? gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable) : 
		   gApp->EntityMngr()->RemoveComponentFromEntity(mSoBoundsId, Renderable::GetGuid());
}

void 
SceneObject::SetDiffuseMapFromFile(std::wstring& fileName, SubEntity* const subEntity)
{
	assert(subEntity);
	
	// Remove diffuse if there is one
	if (subEntity->diffuseMappedComp)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(subEntity->entityId, DiffuseMapped::GetGuid());
		subEntity->diffuseMappedComp = nullptr;
	}

	if (fileName.empty())
		return;

	subEntity->diffuseMappedComp = std::make_shared<DiffuseMapped>(DiffuseMapped((char*)to_string(fileName).c_str()));

	gApp->EntityMngr()->AddComponentToEntity(subEntity->entityId, subEntity->diffuseMappedComp);
}

void 
SceneObject::SetNormalMapFromFile(std::wstring& fileName, SubEntity* const subEntity)
{
	assert(subEntity);

	// Remove normal if there is one
	if (subEntity->normalMappedComp)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(subEntity->entityId, NormalMapped::GetGuid());
		subEntity->normalMappedComp = nullptr;
	}

	if (fileName.empty())
		return;

	subEntity->normalMappedComp = std::make_shared<NormalMapped>(NormalMapped((char*)to_string(fileName).c_str()));

	gApp->EntityMngr()->AddComponentToEntity(subEntity->entityId, subEntity->normalMappedComp);
}

void 
SceneObject::CreateInstancedRenderable(unsigned int const& batchId, DirectX::BoundingBox const& aabb, std::string const& name)
{
	SubEntity se;
	se.entityId = gApp->EntityMngr()->CreateEntity();
	se.name = name;

	se.renderableComp = std::make_shared<ObjRenderable>();
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.renderableComp);

	// Assign bounds to sub-entity and update overall SceneObject bounds
	se.boundedComp = std::make_shared<Bounded>(aabb);
	DirectX::BoundingBox::CreateMerged(mBoundedComp->AabbBounds(), mBoundedComp->AabbBounds(), aabb);
	se.boundedComp = std::make_shared<Bounded>(aabb);
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.boundedComp);
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, mSpatializedComp);

	Material mat;

	// Delete old bound renderable from EntityManager and give it a new one with new size		
	if (mSoBoundsRenderable)
	{
		mat = mSoBoundsRenderable->MaterialProperties();
		gApp->EntityMngr()->RemoveComponentFromEntity(mSoBoundsId, Renderable::GetGuid());
	}
	mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>(gApp->Renderer()->Device(), aabb);
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable);
	mSoBoundsRenderable->MaterialProperties(mat);

	// Assign batch id to renderable component.
	se.renderableComp->DynamicInstancedBatchId(batchId);	

	// Append SubEntity
	mSubEntities.push_back(se);
	
	SetVisibility(mVisible);
}

void
SceneObject::CreateQuad(DirectX::XMFLOAT2& min, DirectX::XMFLOAT2& max, int const sceneEntityIndex, std::string const& name)
{
	DirectX::BoundingBox aabb;
	aabb.Extents = DirectX::XMFLOAT3(max.x - min.x, max.y - min.y, 0.0001f);

	if (sceneEntityIndex == -1)
	{
		SubEntity se;
		se.entityId = gApp->EntityMngr()->CreateEntity();
		se.name = name;

		se.renderableComp = std::make_shared<QuadRenderable>(gApp->Renderer()->Device(), min, max);
		se.renderableComp->BackCulled(false);
		gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.renderableComp);
	
		se.boundedComp = std::make_shared<Bounded>(aabb);
		gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.boundedComp);
		gApp->EntityMngr()->AddComponentToEntity(se.entityId, mSpatializedComp);

		// Append SubEntity
		mSubEntities.push_back(se);

		SetVisibility(mVisible);
	}
	else
	{
		if (mSubEntities.size() > sceneEntityIndex)
		{
			auto se = mSubEntities[sceneEntityIndex];
			if (se.renderableComp)
			{
				gApp->EntityMngr()->RemoveComponentFromEntity(se.entityId, Renderable::GetGuid());
				se.renderableComp = nullptr;
				se.renderableComp = std::make_shared<QuadRenderable>(gApp->Renderer()->Device(), min, max);
				se.renderableComp->BackCulled(false);
				gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.renderableComp);
								
				se.boundedComp->AabbBounds(aabb);								
			}
		}
	}
}

void 
SceneObject::Reset()
{	
	for (auto iter = mSubEntities.begin(); iter != mSubEntities.end(); ++iter)	
		gApp->EntityMngr()->DestroyEntity(iter->entityId);
	mSubEntities.clear();
	
	DirectX::BoundingBox aabb(DirectX::XMFLOAT3(0.f, 0.f, 0.f), DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
	mBoundedComp->AabbBounds(aabb);		
	BoundingRenderableColor(DirectX::XMFLOAT4(0.8f, 0.8f, 0.8f, 1.f));
	ToggleBoundBox(true);
}

void 
SceneObject::SetReflectionMapFromFile(std::wstring& fileName, SubEntity* const subEntity)
{
	assert(subEntity);

	// Remove if there is one
	if (subEntity->reflectionMappedComp)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(subEntity->entityId, ReflectionMapped::GetGuid());
		subEntity->reflectionMappedComp = nullptr;		
	}

	if (fileName.empty())	
		return;	

	subEntity->reflectionMappedComp = std::make_shared<ReflectionMapped>((char*)to_string(fileName).c_str());

	gApp->EntityMngr()->AddComponentToEntity(subEntity->entityId, subEntity->reflectionMappedComp);
}

void 
SceneObject::ToggleBackCulling(bool const backCullOn)
{
	for (auto se : mSubEntities)
	{
		if (se.renderableComp)
		{
			se.renderableComp->BackCulled(backCullOn);
		}
	}
}

void 
SceneObject::SetMaterial(Material& mat)
{
	for (auto se : mSubEntities)
	{
		if (se.renderableComp)
		{
			se.renderableComp->MaterialProperties(mat);
		}
	}
}

void 
SceneObject::SetPrimitiveToPoints(void)
{
	for (auto se : mSubEntities)
	{
		if (se.renderableComp)
		{
			se.renderableComp->PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		}
	}
}

void 
SceneObject::SetPrimitiveToTris(void)
{
	for (auto se : mSubEntities)
	{
		if (se.renderableComp)
		{
			se.renderableComp->PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		}
	}
}

std::shared_ptr<Spatialized> const& 
SceneObject::SpatializedComponent()
{
	return mSpatializedComp;
}

void
SceneObject::SetBounds(DirectX::BoundingBox const& aabb)
{
	mBoundedComp->AabbBounds(aabb);
}

void 
SceneObject::AddSubEntity(SubEntity const& se)
{
	// Update bounds if needed
	if (se.boundedComp)
	{
		auto aabb = se.boundedComp->AabbBounds();
		if (mSubEntities.size() > 0)
		{
			float min_x = (std::min)(
				(mBoundedComp->AabbBounds().Center.x - mBoundedComp->AabbBounds().Extents.x),
				(aabb.Center.x - aabb.Extents.x));
			float min_y = (std::min)(
				(mBoundedComp->AabbBounds().Center.y - mBoundedComp->AabbBounds().Extents.y),
				(aabb.Center.y - aabb.Extents.y));
			float min_z = (std::min)(
				(mBoundedComp->AabbBounds().Center.z - mBoundedComp->AabbBounds().Extents.z),
				(aabb.Center.z - aabb.Extents.z));
			float max_x = (std::max)(
				(mBoundedComp->AabbBounds().Center.x + mBoundedComp->AabbBounds().Extents.x),
				(aabb.Center.x + aabb.Extents.x));
			float max_y = (std::max)(
				(mBoundedComp->AabbBounds().Center.y + mBoundedComp->AabbBounds().Extents.y),
				(aabb.Center.y + aabb.Extents.y));
			float max_z = (std::max)(
				(mBoundedComp->AabbBounds().Center.z + mBoundedComp->AabbBounds().Extents.z),
				(aabb.Center.z + aabb.Extents.z));

			DirectX::XMFLOAT3 center = DirectX::XMFLOAT3(
				((max_x - min_x) / 2.f) + min_x,
				((max_y - min_y) / 2.f) + min_y,
				((max_z - min_z) / 2.f) + min_z);
			DirectX::XMFLOAT3 extents = DirectX::XMFLOAT3(
				(max_x - min_x) / 2.f,
				(max_y - min_y) / 2.f,
				(max_z - min_z) / 2.f);

			mBoundedComp->AabbBounds().Extents = extents;
			mBoundedComp->AabbBounds().Center = center;
		}
		else
			mBoundedComp->AabbBounds() = aabb;

		// Delete old bound renderable from EntityManager and give it a new one with new size		
		Material mat;
		if (mSoBoundsRenderable)
		{
			mat = mSoBoundsRenderable->MaterialProperties();
			gApp->EntityMngr()->RemoveComponentFromEntity(mSoBoundsId, Renderable::GetGuid());
		}
		mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>(gApp->Renderer()->Device(), mBoundedComp->AabbBounds());
		gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable);
		mSoBoundsRenderable->MaterialProperties(mat);
	}	
		
	mSubEntities.push_back(se);
}

void 
SceneObject::SetLightEmitting(Light const& light, LightType const& lightType)
{
	if (!mLightEmittingComp)
	{
		mLightEmittingComp = std::make_shared<LightEmitting>();
		gApp->EntityMngr()->AddComponentToEntity(mEntityId, mLightEmittingComp);
	}

	// Apply light
	if (POINT_LIGHT == lightType)
		mLightEmittingComp->SetPointLight(static_cast<PointLight const&>(light));
	else if (SPOT_LIGHT == lightType)
		mLightEmittingComp->SetSpotLight(static_cast<SpotLight const&>(light));
	else if (DIRECTIONAL_LIGHT == lightType)
		mLightEmittingComp->SetDirectionalLight(static_cast<DirectionalLight const&>(light));
}

void 
SceneObject::RemoveLightEmitting(void)
{
	if (mLightEmittingComp)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, LightEmitting::GetGuid());
		mLightEmittingComp = nullptr;
	}
}

bool 
const SceneObject::EmitsLight() const
{
	if (mLightEmittingComp)
		return true;
	return false;
}

bool const
SceneObject::EmitsShadows() const
{
	if (mShadowEmittingComp)
		return true;
	return false;
}

std::shared_ptr<LightEmitting> const& 
SceneObject::LightEmittingComponent()
{
	return mLightEmittingComp;
}

std::shared_ptr<ShadowEmitting> const&
SceneObject::ShadowEmittingComponent()
{
	return mShadowEmittingComp;
}


void 
SceneObject::SetShadowEmitting(unsigned int const shadowMapSize)
{
	RemoveShadowEmitting();
	mShadowEmittingComp = std::make_shared<ShadowEmitting>(shadowMapSize);
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mShadowEmittingComp);
}

void 
SceneObject::RemoveShadowEmitting(void)
{
	if (mShadowEmittingComp)
	{
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, ShadowEmitting::GetGuid());
		mShadowEmittingComp = nullptr;
	}
}

void 
SceneObject::SetShadowCasting(bool const shadowCasting)
{
	for (auto& se : mSubEntities)
	{
		if (se.shadowCastingComp)
		{
			gApp->EntityMngr()->RemoveComponentFromEntity(se.entityId, ShadowCasting::GetGuid());
			se.shadowCastingComp = nullptr;
		}

		if (shadowCasting)
		{
			se.shadowCastingComp = std::make_shared<ShadowCasting>();
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.shadowCastingComp);			
		}
	}
}





