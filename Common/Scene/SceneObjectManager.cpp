#include <windows.h>
#include <DirectXMath.h>
#include "Debug/Debug.h"
#include "BaseApp.h"
#include "Components/Bounded.h"
#include "Scene/ObjRenderable/ObjRenderable.h"
#include "SceneObjectManager.h"

extern BaseApp* gApp;

#define ENTITY_BOUNDS_COLOR						DirectX::XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f)
#define ENTITY_BOUNDS_COLOR_SELECTED			DirectX::XMFLOAT4(0.3f, 1.0f, 0.3f, 1.0f)

SceneObjectManager::SceneObjectManager(void)
{
}


SceneObjectManager::~SceneObjectManager(void)
{

}

void 
SceneObjectManager::ToggleSceneObjects( std::set<unsigned int> selection )
{
	std::set<unsigned int>::iterator iter = selection.begin();
	DirectX::XMFLOAT4 color;

	for (; iter != selection.end(); iter++)
	{
		std::set<unsigned int>::iterator to_find = mSelectedSceneObjectsIds.find(*iter);
		if (mSelectedSceneObjectsIds.end() != to_find)
		{
			color = ENTITY_BOUNDS_COLOR;
			ChangeSceneObjectBoundBoxColor(*iter, color);
			mSelectedSceneObjectsIds.erase(*iter);
			
		}
		else
		{
			mSelectedSceneObjectsIds.insert(*iter);
			color = ENTITY_BOUNDS_COLOR_SELECTED;
			ChangeSceneObjectBoundBoxColor(*iter, color);
		}
	}	
}

void 
SceneObjectManager::UnselectSceneObjects( std::set<unsigned int> selection )
{
	std::set<unsigned int>::iterator iter = selection.begin();

	for (; iter!= selection.end(); iter++)
	{
		mSelectedSceneObjectsIds.erase(*iter);
		DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR;
		ChangeSceneObjectBoundBoxColor(*iter, color);
	}	
}

void
SceneObjectManager::SelectSceneObjects( std::set<unsigned int> selection )
{
	std::set<unsigned int>::iterator iter = selection.begin();

	for (; iter!= selection.end(); iter++)
	{
		mSelectedSceneObjectsIds.insert(*iter);
		DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR_SELECTED;
		ChangeSceneObjectBoundBoxColor(*iter, color);
	}
}

void
SceneObjectManager::SelectSceneObject( unsigned int const& id )
{
	mSelectedSceneObjectsIds.insert(id);
	DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR_SELECTED;
	ChangeSceneObjectBoundBoxColor(id, color);
}

void 
SceneObjectManager::ToggleSceneObject( unsigned int const& id )
{
	DirectX::XMFLOAT4 color;
		
	std::set<unsigned int>::iterator to_find = mSelectedSceneObjectsIds.find(id);
	if (mSelectedSceneObjectsIds.end() != to_find)
	{
		color = ENTITY_BOUNDS_COLOR;
		ChangeSceneObjectBoundBoxColor(id, color);
		mSelectedSceneObjectsIds.erase(id);

	}
	else
	{
		mSelectedSceneObjectsIds.insert(id);
		color = ENTITY_BOUNDS_COLOR_SELECTED;
		ChangeSceneObjectBoundBoxColor(id, color);
	}
}

void 
SceneObjectManager::UnselectSceneObject( unsigned int const& id )
{		
	mSelectedSceneObjectsIds.erase(id);
	DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR;
	ChangeSceneObjectBoundBoxColor(id, color);
}

void 
SceneObjectManager::AddSceneObject(std::shared_ptr<SceneObject>& sObj)
{
	DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR;
	sObj->BoundingRenderableColor(color);
	mSceneObjects[sObj->EntityId()] = sObj;
	mSceneObjectsIds.insert(sObj->EntityId());
}

void 
SceneObjectManager::AddSceneObjects(std::vector< std::shared_ptr<SceneObject> >& sObjs)
{
	for (auto so : sObjs)
		AddSceneObject(so);
}

void 
SceneObjectManager::RemoveSceneObject(unsigned int const sObjId)
{
	UnselectSceneObject(sObjId);
	mSceneObjectsIds.erase(sObjId);
	mSceneObjects.erase(sObjId);
}

void 
SceneObjectManager::RemoveSceneObjects(std::set<unsigned int>& sObjIds)
{	
	for (auto id : sObjIds)	
		RemoveSceneObject(id);			
}

/*
unsigned int const
SceneObjectManager::CreateSceneObject(bool const storeMetadata)
{
	// Set bounds colors
	DirectX::XMFLOAT4 color = ENTITY_BOUNDS_COLOR;
	SceneObject* so = new SceneObject(storeMetadata);
	so->BoundingRenderableColor(color);
	mSceneObjects[so->EntityId()] = so;
	mSceneObjectsIds.insert(so->EntityId());

	//SelectSceneObject(so->EntityId());

	return so->EntityId();
}

void
SceneObjectManager::DestroySceneObjects(std::set<unsigned int>& ids)
{
	UnselectSceneObject(ids);
	for (std::set<unsigned int>::iterator iter = ids.begin(); iter != ids.end(); iter++)
	{
		UnselectSceneObject(*iter);
		mSceneObjectsIds.erase(*iter);
		delete mSceneObjects[*iter];
		mSceneObjects.erase(*iter);
	}

}
*/