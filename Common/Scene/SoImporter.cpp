#include <assert.h>
#include <fstream>
#include <iostream>
#include <d3d11.h>
#include "Debug/Debug.h"
#include "Scene/SceneObject.h"
#include "Utils/ComponentFetches.h"
#include "Utils/StringUtils.h"
#include "BaseApp.h"
#include "Scene/ProtoUtils.h"
#include "Scene/SoImporter.h"

// Proto buffers
#pragma warning( push ) 
#pragma warning( disable : 4267 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4996 )
#include "Scene/SceneObject.pb.h"
#pragma warning( pop ) 

extern BaseApp* gApp;

std::shared_ptr<SceneObject> 
SoImporter::ImportFromFile(
	std::wstring& file, 
	bool const storeMetadata /*= false*/ )
{
	std::shared_ptr<SceneObject> so(new SceneObject());

	std::string path_and_name = to_string(file);
	std::string file_name = ExtractFileFromPath(path_and_name);
	std::string file_root_path = ExtractPath(path_and_name);

	ticpp::Document doc(path_and_name);
	
	// watch out for exceptions
	try { doc.LoadFile(); }
	catch(ticpp::Exception& ex)
	{
		ex.what(); //suppress VS warning
		OutputDebugMsg("Could not load scene object from: " + file_name);
		return nullptr;
	}
	
	ticpp::Iterator<ticpp::Element> parent;

	for (parent = parent.begin(doc.FirstChildElement()); parent != parent.end(); ++parent)
	{
		std::string str_name;
		parent->GetValue(&str_name);
		//OutputDebugMsg(str_name+"\n");

		if ("Spatialized" == str_name)
			ParseAndSetSpatializedInSo(so, parent);

		if ("LightEmitting" == str_name)
			ParseAndAddLightEmittingToSo(so, parent);

		if ("ShadowEmitting" == str_name)
			ParseAndAddShadowEmittingToSo(so, parent);

		if ("SubEntity" == str_name)
			ParseAndAddSubEntityToSo(so, parent, file_root_path, file_name, storeMetadata);
	}

	return so;
}

void 
SoImporter::ParseAndAddSubEntityToSo(
	std::shared_ptr<SceneObject>& so, 
	ticpp::Iterator<ticpp::Element>& parent, 
	std::string const& rootPath,
	std::string const& fileName,
	bool const storeMetadata /*= false*/)
{
	if (parent->FirstChildElement() == NULL)
		return;

	SceneObject::SubEntity se;
	se.entityId = gApp->EntityMngr()->CreateEntity();
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, so->SpatializedComponent());

	// We first need to set the name of the SE
	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		if ("Name" == value)
		{
			std::string name = child->GetText(); // A name should always be present!
			se.name = name;
			break;
		}
	}
		
	child = parent->FirstChildElement();
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		//OutputDebugMsg(value+": ");			
		//OutputDebugMsg(child->GetText()+"\n");	
		
		if ("Renderable" == value)
		{	
			ParseAndCreateRenderableAndBoundedComponents(child, rootPath, fileName, se.name, se.renderableComp, se.boundedComp, storeMetadata);
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.renderableComp);
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.boundedComp);
		}

		if ("DiffMap" == value)
		{
			std::string name = child->GetText();			
			ParseAndCreateDiffuseMappedComponent(name, rootPath, fileName, se.name, se.diffuseMappedComp);
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.diffuseMappedComp);
		}

		if ("NormMap" == value)
		{
			std::string name = child->GetText();
			ParseAndCreateNormalMappedComponent(name, rootPath, fileName, se.name, se.normalMappedComp);
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.normalMappedComp);
		}

		if ("ReflMap" == value)
		{
			std::string name = child->GetText();
			ParseAndCreateReflectionMappedComponent(name, rootPath, fileName, se.name, se.reflectionMappedComp);
			gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.reflectionMappedComp);
		}

		if ("FurMapped" == value)
		{			
			ParseAndAddFurComponentToSe(se, child);
		}

		if ("ShadowCasting" == value)
		{
			ParseAndAddShadowCastingComponentToSe(se, child);
		}
	}

	// Texture xform
	ParseAndAddTexXformComponentToSe(se, rootPath, fileName);

	so->AddSubEntity(se);
}

void SoImporter::ParseAndCreateRenderableAndBoundedComponents(
	ticpp::Iterator<ticpp::Element>& parent,
	std::string const& rootPath,
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<Renderable>& rOut, 
	std::shared_ptr<Bounded>& bOut,
	bool const storeMetadata /*= false*/)
{
	if (parent->FirstChildElement() == NULL)
		return;

	rOut = std::make_shared<Renderable>();

	unsigned int num_verts = 0;
	unsigned int num_tcoords = 0;
	unsigned int num_inds = 0;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		//OutputDebugMsg(value + ":");
		//OutputDebugMsg(child->GetText() + "\n");

		if ("Type" == value)
			rOut->RenderableType(std::stoi(child->GetText()));
		else if ("AmbX" == value)
			rOut->MaterialProperties().ambient.x = std::stof(child->GetText());
		else if ("AmbY" == value)
			rOut->MaterialProperties().ambient.y = std::stof(child->GetText());
		else if ("AmbZ" == value)
			rOut->MaterialProperties().ambient.z = std::stof(child->GetText());
		else if ("AmbW" == value)
			rOut->MaterialProperties().ambient.w = std::stof(child->GetText());
		else if ("DiffX" == value)
			rOut->MaterialProperties().diffuse.x = std::stof(child->GetText());
		else if ("DiffY" == value)
			rOut->MaterialProperties().diffuse.y = std::stof(child->GetText());
		else if ("DiffZ" == value)
			rOut->MaterialProperties().diffuse.z = std::stof(child->GetText());
		else if ("DiffW" == value)
			rOut->MaterialProperties().diffuse.w = std::stof(child->GetText());
		else if ("SpecX" == value)
			rOut->MaterialProperties().specular.x = std::stof(child->GetText());
		else if ("SpecY" == value)
			rOut->MaterialProperties().specular.y = std::stof(child->GetText());
		else if ("SpecZ" == value)
			rOut->MaterialProperties().specular.z = std::stof(child->GetText());
		else if ("SpecW" == value)
			rOut->MaterialProperties().specular.w = std::stof(child->GetText());
		else if ("ReflX" == value)
			rOut->MaterialProperties().reflect.x = std::stof(child->GetText());
		else if ("ReflY" == value)
			rOut->MaterialProperties().reflect.y = std::stof(child->GetText());
		else if ("ReflZ" == value)
			rOut->MaterialProperties().reflect.z = std::stof(child->GetText());
		else if ("ReflW" == value)
			rOut->MaterialProperties().reflect.w = std::stof(child->GetText());
		else if ("ReflCoeff" == value)
			rOut->MaterialProperties().reflectionCoeff = std::stof(child->GetText());
		else if ("LModel" == value)
			rOut->MaterialProperties().lightingModel = std::stoi(child->GetText());
		else if ("NumVerts" == value)
			num_verts = std::stoi(child->GetText());
		else if ("NumCoords" == value)
			num_tcoords = std::stoi(child->GetText());
		else if ("NumInd" == value)
			num_inds = std::stoi(child->GetText());
		else if ("BCull" == value)
			rOut->BackCulled(std::stoi(child->GetText()) || false); // bin op to suppress warning warning C4800
	}


	// deserialize buffer data
	std::string renderable_dir = rootPath + "Data\\";
		
	SoProto::IndexBufferMetadata    index_in;
	SoProto::GeometryBufferMetadata geom_in;
	SoProto::TextureBufferMetadata  tex_in;

	std::ifstream fi(renderable_dir + fileName + seName + "index.il", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(index_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + renderable_dir + fileName + seName + "index.il" + "\n");
	fi.close();
	
	fi.open(renderable_dir + fileName + seName + "geom.il", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(geom_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + renderable_dir + fileName + seName + "geom.il" + "\n");
	fi.close();

	fi.open(renderable_dir + fileName + seName + "tex.il", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(tex_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + renderable_dir + fileName + seName + "tex.il" + "\n");
	fi.close();

	
	std::vector<unsigned int> index_buffer(num_inds);
	std::vector<GeometryInputLayout> vertex_buff(num_verts);
	std::vector<TextureInputLayout> tex_buff(num_verts);
	for (unsigned int i = 0; i < num_inds; i++)
		index_buffer[i] = index_in.indexbuff(i);
	for (unsigned int i = 0; i < num_verts; i++)
	{
		auto cur_geom = geom_in.geombuff(i);
		auto cur_tex = tex_in.texbuff(i);
		vertex_buff[i].pos = CreateXMFloat3(cur_geom.pos());
		vertex_buff[i].norm = CreateXMFloat3(cur_geom.norm());
		tex_buff[i].texCoords = CreateXMFloat2(cur_tex.texcoords());
		tex_buff[i].tangent = CreateXMFloat3(cur_tex.tangent());
	}

	auto index_desc = CreateD3D11BufferDesc(index_in.desc());
	auto geom_desc = CreateD3D11BufferDesc(geom_in.desc());
	auto tex_desc = CreateD3D11BufferDesc(tex_in.desc());

	// Should we keep metadata?
	if (storeMetadata)
	{
		rOut->GetMetadata().indexMetadata.indexBuffer = index_buffer;
		rOut->GetMetadata().indexMetadata.numIndices = num_inds;
		rOut->GetMetadata().indexMetadata.indexBufferDesc = index_desc;

		rOut->GetMetadata().geomMetadata.vertsBuffer = vertex_buff;
		rOut->GetMetadata().geomMetadata.numVerts = num_verts;
		rOut->GetMetadata().geomMetadata.vertexBufferDesc = geom_desc;

		rOut->GetMetadata().textureMetadata.textureBuffer = tex_buff;
		rOut->GetMetadata().textureMetadata.numCoords = num_tcoords;
		rOut->GetMetadata().textureMetadata.textureBufferDesc = tex_desc;

		rOut->HasMetadata(true);
	}

	// Calculate bounds
	std::vector<DirectX::XMFLOAT3> v_positions;
	for (auto iter : rOut->GetMetadata().geomMetadata.vertsBuffer)
		v_positions.push_back(iter.pos);
	DirectX::BoundingBox aabb;
	DirectX::BoundingBox::CreateFromPoints(aabb, v_positions.size(), &v_positions[0], sizeof(DirectX::XMFLOAT3));
	bOut = std::make_shared<Bounded>(aabb);

	// create buffers ///////////////////////////////////////////////////////////	
	D3D11_SUBRESOURCE_DATA InitData;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &index_buffer[0];
	gApp->Renderer()->Device()->CreateBuffer(&index_desc, &InitData, &(rOut->IndexBuffer().indexBuffer));
	rOut->IndexBuffer().indexCount = num_inds;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertex_buff[0];
	VertexBufferDesc vbuff_desc;
	vbuff_desc.stride = sizeof(GeometryInputLayout);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = num_verts;	
	gApp->Renderer()->Device()->CreateBuffer(&geom_desc, &InitData, &(vbuff_desc.vertexBuffer));
	rOut->AddVertexBuffer(InputLayoutManager::GEOMETRY, vbuff_desc);

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &tex_buff[0];
	vbuff_desc.stride = sizeof(TextureInputLayout);
	vbuff_desc.offset = 0;
	vbuff_desc.vertexCount = num_verts;
	gApp->Renderer()->Device()->CreateBuffer(&tex_desc, &InitData, &(vbuff_desc.vertexBuffer));
	rOut->AddVertexBuffer(InputLayoutManager::TEXTURE, vbuff_desc);

	return;
}

void 
SoImporter::ParseAndCreateDiffuseMappedComponent(
	std::string const& diffMapName,
	std::string const& rootPath,
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<DiffuseMapped>& dOut)
{
	dOut = std::make_shared<DiffuseMapped>();
	
	std::string dir = rootPath + "Maps\\";

	SoProto::DiffuseMapped diff_in;
	std::ifstream fi(dir + fileName + seName + "diff_sampler.desc", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(diff_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + dir + fileName + seName + "diff_sampler.desc" + "\n");
	fi.close();

	dOut->DiffuseMapName(dir+diffMapName);
	dOut->DiffuseMapSamplerDesc(CreateD3D11SamplerDesc(diff_in.desc()));
}

void 
SoImporter::ParseAndAddTexXformComponentToSe(
	SceneObject::SubEntity& se, 
	std::string const& rootPath, 
	std::string const& fileName)
{
	std::string dir = rootPath + "Maps\\";

	std::ifstream fi(dir + fileName + se.name + "tex_xform.mat", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
	{
		SoProto::TextureTransformed xform_in;
		assert(xform_in.ParseFromIstream(&fi));

		se.textureXformComp = std::make_shared<TextureTransformed>();
		se.textureXformComp->TextureTransformation() = CreateXMFloat4X4(xform_in.xform());

		gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.textureXformComp);
	}
}

void 
SoImporter::ParseAndCreateNormalMappedComponent(
	std::string const& mapName,
	std::string const& rootPath,
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<NormalMapped>& nOut)
{
	nOut = std::make_shared<NormalMapped>();

	std::string dir = rootPath + "Maps\\";

	SoProto::NormalMapped norm_in;
	std::ifstream fi(dir + fileName + seName + "norm_sampler.desc", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(norm_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + dir + fileName + seName + "norm_sampler.desc" + "\n");
	fi.close();

	nOut->NormalMapName(dir + mapName);
	nOut->NormalMapSamplerDesc(CreateD3D11SamplerDesc(norm_in.desc()));
}

void 
SoImporter::ParseAndCreateReflectionMappedComponent(
	std::string const& mapName,
	std::string const& rootPath,
	std::string const& fileName,
	std::string const& seName,
	std::shared_ptr<ReflectionMapped>& rOut)
{
	rOut = std::make_shared<ReflectionMapped>();

	std::string dir = rootPath + "Maps\\";

	SoProto::ReflectionMapped refl_in;
	std::ifstream fi(dir + fileName + seName + "refl_sampler.desc", std::ifstream::in | std::ifstream::binary);
	if (fi.is_open() && fi.good())
		assert(refl_in.ParseFromIstream(&fi));
	else
		OutputDebugMsg("Couldn't open " + dir + fileName + seName + "refl_sampler.desc" + "\n");
	fi.close();

	rOut->ReflectionMapName(dir + mapName);
	rOut->ReflectionMapSamplerDesc(CreateD3D11SamplerDesc(refl_in.desc()));
}

void 
SoImporter::ParseAndSetSpatializedInSo(
	std::shared_ptr<SceneObject>& so, 
	ticpp::Iterator<ticpp::Element>& parent)
{
	if (parent->FirstChildElement() == NULL)
		return;

	DirectX::XMFLOAT4X4 mat;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		//OutputDebugMsg(value + ":");
		//OutputDebugMsg(child->GetText() + "\n");

		if ("_11" == value)
			mat._11 = std::stof(child->GetText());
		else if ("_12" == value)
			mat._12 = std::stof(child->GetText());
		else if ("_13" == value)
			mat._13 = std::stof(child->GetText());
		else if ("_14" == value)
			mat._14 = std::stof(child->GetText());

		if ("_21" == value)
			mat._21 = std::stof(child->GetText());
		else if ("_22" == value)
			mat._22 = std::stof(child->GetText());
		else if ("_23" == value)
			mat._23 = std::stof(child->GetText());
		else if ("_24" == value)
			mat._24 = std::stof(child->GetText());

		if ("_31" == value)
			mat._31 = std::stof(child->GetText());
		else if ("_32" == value)
			mat._32 = std::stof(child->GetText());
		else if ("_33" == value)
			mat._33 = std::stof(child->GetText());
		else if ("_34" == value)
			mat._34 = std::stof(child->GetText());

		if ("_41" == value)
			mat._41 = std::stof(child->GetText());
		else if ("_42" == value)
			mat._42 = std::stof(child->GetText());
		else if ("_43" == value)
			mat._43 = std::stof(child->GetText());
		else if ("_44" == value)
			mat._44 = std::stof(child->GetText());
	}

	so->SpatializedComponent()->LocalTransformation(mat);	
}

void 
SoImporter::ParseAndAddLightEmittingToSo(
	std::shared_ptr<SceneObject>& so, 
	ticpp::Iterator<ticpp::Element>& parent)
{
	if (parent->FirstChildElement() == NULL)
		return;
	
	LightType l_type;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);

		if ("LightType" == value)
		{
			l_type = (LightType)std::stoi(child->GetText());
			break;
		}
	}

	child = parent->FirstChildElement();
	switch (l_type)
	{
	case DIRECTIONAL_LIGHT:
	{
		DirectionalLight dl;
		for (child; child != child.end(); child++)
		{
			std::string value; child->GetValue(&value);

			if ("AmbX" == value)
				dl.ambient.x = std::stof(child->GetText());
			else if ("AmbY" == value)
				dl.ambient.y = std::stof(child->GetText());
			else if ("AmbZ" == value)
				dl.ambient.z = std::stof(child->GetText());
			else if ("AmbW" == value)
				dl.ambient.w = std::stof(child->GetText());

			else if ("DiffX" == value)
				dl.diffuse.x = std::stof(child->GetText());
			else if ("DiffY" == value)
				dl.diffuse.y = std::stof(child->GetText());
			else if ("DiffZ" == value)
				dl.diffuse.z = std::stof(child->GetText());
			else if ("DiffW" == value)
				dl.diffuse.w = std::stof(child->GetText());

			else if ("SpecX" == value)
				dl.specular.x = std::stof(child->GetText());
			else if ("SpecY" == value)
				dl.specular.y = std::stof(child->GetText());
			else if ("SpecZ" == value)
				dl.specular.z = std::stof(child->GetText());
			else if ("SpecW" == value)
				dl.specular.w = std::stof(child->GetText());

			else if ("DirX" == value)
				dl.direction.x = std::stof(child->GetText());
			else if ("DirY" == value)
				dl.direction.y = std::stof(child->GetText());
			else if ("DirZ" == value)
				dl.direction.z = std::stof(child->GetText());			
		}
		so->SetLightEmitting(dl, DIRECTIONAL_LIGHT);
		break;
	}
	case POINT_LIGHT:
	{
		PointLight pl;
		for (child; child != child.end(); child++)
		{
			std::string value; child->GetValue(&value);

			if ("AmbX" == value)
				pl.ambient.x = std::stof(child->GetText());
			else if ("AmbY" == value)
				pl.ambient.y = std::stof(child->GetText());
			else if ("AmbZ" == value)
				pl.ambient.z = std::stof(child->GetText());
			else if ("AmbW" == value)
				pl.ambient.w = std::stof(child->GetText());

			else if ("DiffX" == value)
				pl.diffuse.x = std::stof(child->GetText());
			else if ("DiffY" == value)
				pl.diffuse.y = std::stof(child->GetText());
			else if ("DiffZ" == value)
				pl.diffuse.z = std::stof(child->GetText());
			else if ("DiffW" == value)
				pl.diffuse.w = std::stof(child->GetText());

			else if ("SpecX" == value)
				pl.specular.x = std::stof(child->GetText());
			else if ("SpecY" == value)
				pl.specular.y = std::stof(child->GetText());
			else if ("SpecZ" == value)
				pl.specular.z = std::stof(child->GetText());
			else if ("SpecW" == value)
				pl.specular.w = std::stof(child->GetText());

			else if ("PosX" == value)
				pl.position.x = std::stof(child->GetText());
			else if ("PosY" == value)
				pl.position.y = std::stof(child->GetText());
			else if ("PosZ" == value)
				pl.position.z = std::stof(child->GetText());

			else if ("Range" == value)
				pl.range = std::stof(child->GetText());

			else if ("AttX" == value)
				pl.attenuation.x = std::stof(child->GetText());
			else if ("AttY" == value)
				pl.attenuation.y = std::stof(child->GetText());
			else if ("AttZ" == value)
				pl.attenuation.z = std::stof(child->GetText());
		}
		so->SetLightEmitting(pl, POINT_LIGHT);
		break;
	}
	case SPOT_LIGHT:
	{
		SpotLight sl;
		for (child; child != child.end(); child++)
		{
			std::string value; child->GetValue(&value);

			if ("AmbX" == value)
				sl.ambient.x = std::stof(child->GetText());
			else if ("AmbY" == value)
				sl.ambient.y = std::stof(child->GetText());
			else if ("AmbZ" == value)
				sl.ambient.z = std::stof(child->GetText());
			else if ("AmbW" == value)
				sl.ambient.w = std::stof(child->GetText());

			else if ("DiffX" == value)
				sl.diffuse.x = std::stof(child->GetText());
			else if ("DiffY" == value)
				sl.diffuse.y = std::stof(child->GetText());
			else if ("DiffZ" == value)
				sl.diffuse.z = std::stof(child->GetText());
			else if ("DiffW" == value)
				sl.diffuse.w = std::stof(child->GetText());

			else if ("SpecX" == value)
				sl.specular.x = std::stof(child->GetText());
			else if ("SpecY" == value)
				sl.specular.y = std::stof(child->GetText());
			else if ("SpecZ" == value)
				sl.specular.z = std::stof(child->GetText());
			else if ("SpecW" == value)
				sl.specular.w = std::stof(child->GetText());

			else if ("PosX" == value)
				sl.position.x = std::stof(child->GetText());
			else if ("PosY" == value)
				sl.position.y = std::stof(child->GetText());
			else if ("PosZ" == value)
				sl.position.z = std::stof(child->GetText());

			else if ("Range" == value)
				sl.range = std::stof(child->GetText());

			else if ("AttX" == value)
				sl.attenuation.x = std::stof(child->GetText());
			else if ("AttY" == value)
				sl.attenuation.y = std::stof(child->GetText());
			else if ("AttZ" == value)
				sl.attenuation.z = std::stof(child->GetText());

			else if ("DirX" == value)
				sl.direction.x = std::stof(child->GetText());
			else if ("DirY" == value)
				sl.direction.y = std::stof(child->GetText());
			else if ("DirZ" == value)
				sl.direction.z = std::stof(child->GetText());

			else if ("Spot" == value)
				sl.spot = std::stof(child->GetText());
		}
		so->SetLightEmitting(sl, SPOT_LIGHT);
		break;
	}
	default:
		break;
	}
}

void 
SoImporter::ParseAndAddFurComponentToSe(
	SceneObject::SubEntity& se, 
	ticpp::Iterator<ticpp::Element>& parent)
{
	if (parent->FirstChildElement() == NULL)
		return;

	se.furComp = std::make_shared<FurMapped>();
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.furComp);

	unsigned int tex_size, num_layers, start_density, end_density;
	float length, start_alpha, end_alpha;
	DirectX::XMFLOAT3 force;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		//OutputDebugMsg(value + ":");
		//OutputDebugMsg(child->GetText() + "\n");

		if ("TextureSize" == value)
			tex_size = std::stoi(child->GetText());
		else if ("NumLayers" == value)
			num_layers = std::stoi(child->GetText());
		else if ("StartDensity" == value)
			start_density = std::stoi(child->GetText());
		else if ("EndDensity" == value)
			end_density = std::stoi(child->GetText());
		else if ("length" == value)
			length = std::stof(child->GetText());
		else if ("StartAlpha" == value)
			start_alpha = std::stof(child->GetText());
		else if ("EndAlpha" == value)
			end_alpha = std::stof(child->GetText());
		else if ("ForceX" == value)
			force.x = std::stof(child->GetText());
		else if ("ForceY" == value)
			force.y = std::stof(child->GetText());
		else if ("ForceZ" == value)
			force.z = std::stof(child->GetText());
	}


	se.furComp->InitFurLayers(gApp->Renderer(), tex_size, num_layers, start_density, end_density, start_alpha, end_alpha);
	se.furComp->FurLength() = length;
	se.furComp->Force() = force;
}

void 
SoImporter::ParseAndAddShadowEmittingToSo(std::shared_ptr<SceneObject>& so, ticpp::Iterator<ticpp::Element>& parent)
{
	if (parent->FirstChildElement() == NULL)
		return;
	
	float static_bias = 0.f; 
	float normal_bias = 0.f;
	int use_plane_depth = 0;
	float shadow_intensity = 1.f;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);

		

		if ("Size" == value)
		{
			auto size = std::stoi(child->GetText());
			so->SetShadowEmitting(size);
		}
		else if ("StaticBias" == value)
		{
			static_bias = std::stof(child->GetText());			
		}
		else if ("NormalScaleBias" == value)
		{
			normal_bias = std::stof(child->GetText());
		}
		else if ("UsePlaneDepthBias" == value)
		{
			use_plane_depth = std::stoi(child->GetText());
		}
		else if ("ShadowIntensity" == value)
		{
			shadow_intensity = std::stof(child->GetText());
		}
	}	

	auto shadow_emitting = so->ShadowEmittingComponent();
	shadow_emitting->StaticOffsetBias(static_bias);
	shadow_emitting->NormalOffsetScaleBias(normal_bias);
	shadow_emitting->UsePlaneDepthBias(use_plane_depth);
	shadow_emitting->ShadowIntensity(shadow_intensity);
}

void 
SoImporter::ParseAndAddShadowCastingComponentToSe(SceneObject::SubEntity& se, ticpp::Iterator<ticpp::Element>& parent)
{
	se.shadowCastingComp = std::make_shared<ShadowCasting>();
	gApp->EntityMngr()->AddComponentToEntity(se.entityId, se.shadowCastingComp);
}
