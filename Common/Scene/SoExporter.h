#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <string>
#include <vector>
#include <memory>

// Components
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/ReflectionMapped.h"
#include "Components/TextureTransformed.h"
#include "Components/FurMapped.h"
#include "Components/ShadowEmitting.h"

class SceneObject;

class SoExporter
{
public:	
	static void
	ExportToFile(
		std::string const& file,
		std::vector< std::shared_ptr<SceneObject> >& sceneObjects);

private:
	static DirectX::XMFLOAT4X4
	GetUniqueTransformMatrix(std::vector< std::shared_ptr<SceneObject> >& sceneObjects);

	static std::shared_ptr<LightEmitting> 
	GetUniqueLightEmitting(std::vector< std::shared_ptr<SceneObject> >& sceneObjects);

	static std::shared_ptr<ShadowEmitting>
	GetUniqueShadowEmitting(std::vector< std::shared_ptr<SceneObject> >& sceneObjects);

	static void
	ExportRenderable(
		std::string& buffer,
		std::string const& exportRootPath,
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<Renderable> const& renderable);

	static void
	ExportTexTransformed(
		std::string& buffer, 
		std::string const& exportRootPath, 
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<TextureTransformed> const& tt);

	static void
	ExportDiffMap(
		std::string& buffer, 
		std::string const& exportRootPath, 
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<DiffuseMapped> const& diffMap);

	static void
	ExportNormalMap(
		std::string& buffer, 
		std::string const& exportRootPath, 
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<NormalMapped> const& normalMap);

	static void
	ExportReflectionMap(
		std::string& buffer, 
		std::string const& exportRootPath, 
		std::string const& fileName,
		std::string const& seName,
		std::shared_ptr<ReflectionMapped> const& reflectionMap);

	static void
	ExportFurMapped(
		std::string& buffer,		
		std::shared_ptr<FurMapped> const& furMapped);

	static void
	ExportLightEmitting(
		std::string& buffer,			
		std::shared_ptr<LightEmitting> const& light);

	static void
	ExportShadowEmitting(
		std::string& buffer,
		std::shared_ptr<ShadowEmitting> const& shadowEmitting);

	static void
	ExportTransformMatrix(
		std::string& buffer,		
		DirectX::XMFLOAT4X4 const& mat);

	static void
	ExportShadowCasting(
		std::string& buffer,
		std::shared_ptr<ShadowCasting> const& shadowCasting);
};

