#include <assert.h>
#include <fstream>
#include <iostream>
#include <DirectXMath.h>
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Debug/Debug.h"
#include "Utils/StringUtils.h"
#include "Utils/IOUtils.h"
#include "BaseApp.h"
#include "SceneLoaderWriter.h"

extern BaseApp* gApp;

bool const 
SceneLoaderWriter::LoadSceneFromFile(
	std::string const& file,
	std::shared_ptr<SceneObjectManager> const& sceneObjManager,
	std::shared_ptr<ForwardRenderer> const& frwrdRenderer,
	bool const storeMetadata)
{
	std::string root_path = ExtractPath(file);
	std::string assets_dir = root_path + "Assets\\";

	ticpp::Document doc(file);

	// watch out for exceptions
	try { doc.LoadFile(); }
	catch (ticpp::Exception& ex)
	{
		ex.what(); //suppress VS warning
		OutputDebugMsg("Could not load scene from: " + file);
		return false;
	}

	ticpp::Iterator<ticpp::Element> parent;

	for (parent = parent.begin(doc.FirstChildElement()); parent != parent.end(); ++parent)
	{
		std::string str_name;
		parent->GetValue(&str_name);
		//OutputDebugMsg(str_name+"\n");

		if ("SO" == str_name)
		{
			std::string so_file_name; parent->GetText(&so_file_name);
			std::string so_file = assets_dir + so_file_name;
			sceneObjManager->AddSceneObject(
				SoImporter::ImportFromFile(std::wstring(so_file.begin(), so_file.end()), storeMetadata));
		}

		if ("Fog" == str_name && frwrdRenderer)
			ParseAndSetFogToForwardRenderer(parent, frwrdRenderer);
	}

	return true;
}

void 
SceneLoaderWriter::WriteSceneToFile(std::string const& file, 
									std::shared_ptr<SceneObjectManager> const& sceneObjManager,
									std::shared_ptr<ForwardRenderer> const& frwrdRenderer)
{
	std::string root_path = ExtractPath(file);
	std::string assets_dir = root_path + "Assets\\";
	
	std::string buffer;		
	buffer = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n\n";
	buffer += "<Scene>\n";
	
	// Remove directores
	std::vector<std::string> dirs_to_clean{ 
		assets_dir + "*.so",
		assets_dir + "\\Maps\\*.desc", 
		assets_dir + "\\Maps\\*.mat",
		assets_dir + "\\Data\\*.il" };
	RemoveFromDirectories(dirs_to_clean);
	
	// Create directories
	CreateDirectory(
		std::wstring(assets_dir.begin(), assets_dir.end()).c_str(),
		NULL);

	auto ids = sceneObjManager->SceneObjectsIds();
	
	for (auto id : ids)
	{
		std::string so_file = std::to_string(id) + ".so";

		buffer += "<SO>" + so_file + "</SO>\n";

		auto so = sceneObjManager->SceneObjectRef(id);
		std::vector< std::shared_ptr<SceneObject> > so_out;
		so_out.push_back(so);

		SoExporter::ExportToFile(assets_dir+so_file, so_out);
	}

		
	// Eventually, other rendering pipeline settings might need to be exported. ////////	

	// Fog
	buffer += "\n<Fog>\n";
	buffer += XmlTagFloat("Density", frwrdRenderer->FogSettings().fogDensity) + "\n";
	buffer += XmlTagFloat("LinSart", frwrdRenderer->FogSettings().fogLinStart) + "\n";
	buffer += XmlTagFloat("LinEnd", frwrdRenderer->FogSettings().fogLinEnd) + "\n";
	buffer += XmlTagInt("Type", frwrdRenderer->FogSettings().fogType) + "\n";
	buffer += XmlTagFloat4("Color", frwrdRenderer->FogSettings().fogColor) + "\n";
	buffer += "</Fog>\n";

	buffer += "</Scene>\n";

	std::ofstream o_stream(file);
	assert(o_stream.good());
	o_stream << buffer;
	o_stream.close();
}

void 
SceneLoaderWriter::ParseAndSetFogToForwardRenderer(
	ticpp::Iterator<ticpp::Element>& parent, 
	std::shared_ptr<ForwardRenderer> const& frwrdRenderer)
{
	if (parent->FirstChildElement() == NULL)
		return;

	ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
	for (child; child != child.end(); child++)
	{
		std::string value; child->GetValue(&value);
		//OutputDebugMsg(value + ":");
		//OutputDebugMsg(child->GetText() + "\n");

		if ("Density" == value)
			frwrdRenderer->FogSettings().fogDensity = std::stof(child->GetText());
		else if ("LinSart" == value)
			frwrdRenderer->FogSettings().fogLinStart = std::stof(child->GetText());
		else if ("LinEnd" == value)
			frwrdRenderer->FogSettings().fogLinEnd = std::stof(child->GetText());
		else if ("Type" == value)
			frwrdRenderer->FogSettings().fogType = std::stoi(child->GetText());
		else if ("ColorX" == value)
			frwrdRenderer->FogSettings().fogColor.x = std::stof(child->GetText());
		else if ("ColorY" == value)
			frwrdRenderer->FogSettings().fogColor.y = std::stof(child->GetText());
		else if ("ColorZ" == value)
			frwrdRenderer->FogSettings().fogColor.z = std::stof(child->GetText());
		else if ("ColorW" == value)
			frwrdRenderer->FogSettings().fogColor.w = std::stof(child->GetText());
	}
}
