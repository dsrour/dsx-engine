#pragma once

#include <string>
#include <map>
#include <set> 

/// Metadata can be passed with event
struct Metadata
{ };

/// An EventSubscriber class that has been registered with the state manager can be notified about specific state changes.
class EventSubscriber
{
	friend class EventManager;
private:
	virtual void
	OnEvent(std::string const& event, Metadata const* const metadata) {};

	virtual void
	OnEvent(std::string const& event, Metadata* const metadata) {};
};

class EventManager
{
public:
	EventManager() {}
	
	void
	SubscribeToEvent(std::string const& event, EventSubscriber* const subscriber) { mEventSubscribers[event].insert(subscriber); }

	void
	UnsubscribeToEvent(std::string const& event, EventSubscriber* const subscriber) { mEventSubscribers[event].erase(subscriber); }

	void
	BroadcastEvent(std::string const& event, Metadata const* const metadata = NULL) 
	{
		// Broadcast
		for (std::set<EventSubscriber*>::iterator iter = mEventSubscribers[event].begin(); iter != mEventSubscribers[event].end(); iter++)
			(*iter)->OnEvent(event, metadata);
	}

	void
	BroadcastEvent(std::string const& event, Metadata* const metadata = NULL) 
	{
		// Broadcast
		for (std::set<EventSubscriber*>::iterator iter = mEventSubscribers[event].begin(); iter != mEventSubscribers[event].end(); iter++)
			(*iter)->OnEvent(event, metadata);
	}

private:	
	std::map< std::string, std::set<EventSubscriber*> >	mEventSubscribers;
};