#pragma once

#define TIXML_USE_TICPP
#include "ticpp/ticpp.h"
#include <string>
#include <map>

struct SpriteAnimationInfo
{
	std::string diffusePath;
	std::string normalPath;
	unsigned int width;
	unsigned int height;
	float offsetX;
	float offsetY;
	unsigned int  cols;
	unsigned int rows;
	float fps;
	float hitBoxWidth;
	float hitBoxHeight;
	unsigned int frameStart[2];
	unsigned int frameEnd[2];
	bool loop;
};

std::map< std::string, SpriteAnimationInfo >
LoadAnimationsInfoFromFile(std::string const& path);

