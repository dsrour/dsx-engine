#pragma once

// This class basically transforms a texture transform matrix to match the tile within a tile sheet

#include "System.h"

class SpriteAnimationSystem : public System, public std::enable_shared_from_this<SpriteAnimationSystem>
{
public:
	SpriteAnimationSystem(std::shared_ptr<EntityManager> entityManager);
	~SpriteAnimationSystem(void);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

private: 
};

