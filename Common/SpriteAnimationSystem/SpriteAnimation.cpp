#include <windows.h>
#include <DirectXMath.h>
#include "Components/TextureTransformed.h"
#include "SpriteAnimationSystem/SpriteAnimated.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "SpriteAnimationSystem/SpriteAnimation.h"


extern BaseApp* gApp;

SpriteAnimationSystem::SpriteAnimationSystem(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
{
	// Set family req
	std::set<unsigned int> fam_req;
	fam_req.insert(TextureTransformed::GetGuid());      
	fam_req.insert(SpriteAnimated::GetGuid());   
	SetFamilyRequirements(fam_req);    
}


SpriteAnimationSystem::~SpriteAnimationSystem(void)
{
}

void 
SpriteAnimationSystem::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	double cur_time = gApp->Timer().ElapsedTimeSecs();

	std::set<unsigned int>::const_iterator fam_iter = family->begin();
	for (fam_iter; fam_iter!= family->end(); ++fam_iter)
	{   			
		

		std::shared_ptr<IComponent> xform_tmp_comp, animated_tmp_comp;

		gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, TextureTransformed::GetGuid(), xform_tmp_comp);
		gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, SpriteAnimated::GetGuid(), animated_tmp_comp);

		if (xform_tmp_comp && animated_tmp_comp)
		{			
			TextureTransformed* xform = static_cast<TextureTransformed*>(xform_tmp_comp.get());
			SpriteAnimated* animated = static_cast<SpriteAnimated*>(animated_tmp_comp.get());		

			if (!animated->mPlay)
				continue;

			if (animated->mFps == 0.0 ||
				animated->mSpriteSheetCols == 0 ||
				animated->mSpriteSheetRows == 0)
				continue;

			// Set coord
			float sprite_x =  (1.f/(float)animated->mSpriteSheetCols);
			float sprite_y =  (1.f/(float)animated->mSpriteSheetRows);

			DirectX::XMMATRIX coord = DirectX::XMMatrixIdentity();

			DirectX::XMMATRIX scale = DirectX::XMMatrixScaling( sprite_x, sprite_y, 0.f );
			DirectX::XMMATRIX trans = DirectX::XMMatrixTranslation( sprite_x*(float)animated->mCurrentFrame[0], sprite_y*(float)animated->mCurrentFrame[1], 0.f);						
			coord = DirectX::XMMatrixMultiply(coord, scale);
			coord = DirectX::XMMatrixMultiply(coord, trans);

			DirectX::XMStoreFloat4x4(&xform->TextureTransformation(), coord);
									
			// Advance frame if needed
			double delta = cur_time - animated->mLastFrameTime;
			if (delta > animated->mFps)
			{
				animated->mLastFrameTime = cur_time;

				// Check if we finished the animation
				if ( animated->mCurrentFrame[0] == animated->mEndFrame[0] &&
				     animated->mCurrentFrame[1] == animated->mEndFrame[1] )
				{
					if (!animated->mLoop)
						animated->mPlay = false;
					else
					{
						animated->mCurrentFrame[0] = animated->mStartFrame[0];
						animated->mCurrentFrame[1] = animated->mStartFrame[1];
					}

					continue;
				}
				else
				{
					animated->mCurrentFrame[0]++;				

					if (animated->mCurrentFrame[0] >= animated->mSpriteSheetCols)
					{
						animated->mCurrentFrame[0] = 0;
						animated->mCurrentFrame[1]++;
					}

					if (animated->mCurrentFrame[1] >= animated->mSpriteSheetRows)
						animated->mCurrentFrame[1] = 0;
				}				
			}
		}		 
	}		
}
