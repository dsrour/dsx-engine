#include <string>
#include "BaseApp.h"
#include "SpriteAnimationSystem/SpriteEntity.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;

SpriteEntity::SpriteEntity( void )
{
	mXMirrored = false; 

	ToggleBoundBox(false);
	InitSprite(100, 100);
}

SpriteEntity::~SpriteEntity( void )
{

}

void 
SpriteEntity::InitSprite( unsigned int w, unsigned int h )
{
	// Remove renderable first if it exists
	if (HasGeometry())
		CreateQuad(DirectX::XMFLOAT2(-((float)w / 2.f), 0.f), DirectX::XMFLOAT2((float)w / 2.f, (float)h), 0);
	else
		CreateQuad( DirectX::XMFLOAT2(-((float)w / 2.f), 0.f), DirectX::XMFLOAT2( (float)w / 2.f, (float)h ) );
	
	mSpriteId = SubEntityRefByIndex(0)->entityId;
	SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::LIT);
	
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	mat.ambient = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	mat.specular = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 0.f);	
	SubEntityRefByIndex(0)->renderableComp->MaterialProperties(mat);
}

void 
SpriteEntity::AnimateSprite( std::string const& animationName )
{
	auto iter = mAnimations.find(animationName);

	assert(iter != mAnimations.end());
		
	if (iter->second.diffusePath != "")
	{
		std::wstring wpath = gResourcesDir + L"Platformer\\";
		std::string path(wpath.begin(), wpath.end());
		path +=  iter->second.diffusePath;

		if (!SubEntityRefByIndex(0)->diffuseMappedComp)
		{
			SubEntityRefByIndex(0)->diffuseMappedComp = std::make_shared<DiffuseMapped>(path);
			gApp->EntityMngr()->AddComponentToEntity(mSpriteId, SubEntityRefByIndex(0)->diffuseMappedComp);
		}
		else
			SubEntityRefByIndex(0)->diffuseMappedComp->DiffuseMapName(path);
	}

	if (iter->second.normalPath != "")
	{
		std::wstring wpath = gResourcesDir + L"Platformer\\";
		std::string path(wpath.begin(), wpath.end());
		path +=  iter->second.normalPath;

		if (!SubEntityRefByIndex(0)->normalMappedComp)
		{
			SubEntityRefByIndex(0)->normalMappedComp = std::make_shared<NormalMapped>(path);
			gApp->EntityMngr()->AddComponentToEntity(mSpriteId, SubEntityRefByIndex(0)->normalMappedComp);
		}
		else
			SubEntityRefByIndex(0)->normalMappedComp->NormalMapName(path);
	}
	
	

	if (SubEntityRefByIndex(0)->textureXformComp == NULL)
	{		
		SubEntityRefByIndex(0)->textureXformComp = std::make_shared<TextureTransformed>();
		gApp->EntityMngr()->AddComponentToEntity(mSpriteId, SubEntityRefByIndex(0)->textureXformComp);
	}



	if (mSpriteAnimatedComp == NULL)
	{		
		mSpriteAnimatedComp = std::make_shared<SpriteAnimated>( SpriteAnimated() );
		gApp->EntityMngr()->AddComponentToEntity(mSpriteId, mSpriteAnimatedComp);
	}


	mSpriteAnimatedComp->mSpriteSheetRows = iter->second.rows;
	mSpriteAnimatedComp->mSpriteSheetCols = iter->second.cols;
	mSpriteAnimatedComp->mCurrentFrame[0] = iter->second.frameStart[0];
	mSpriteAnimatedComp->mCurrentFrame[1] = iter->second.frameStart[1];
	mSpriteAnimatedComp->mStartFrame[0] = iter->second.frameStart[0];
	mSpriteAnimatedComp->mStartFrame[1] = iter->second.frameStart[1];
	mSpriteAnimatedComp->mEndFrame[0] = iter->second.frameEnd[0];
	mSpriteAnimatedComp->mEndFrame[1] = iter->second.frameEnd[1];
	mSpriteAnimatedComp->mFps = iter->second.fps;
	mSpriteAnimatedComp->mLoop = iter->second.loop;
	mSpriteAnimatedComp->mPlay = true;

	// Scale to appropriate size
	if (mXMirrored && mSpatializedComp)
		mSpatializedComp->LocalScale().x = -1.f;
	else if (mSpatializedComp)
		mSpatializedComp->LocalScale().x = 1.f;

	if (mCurrentAnim != animationName)
	{
		InitSprite(iter->second.width, iter->second.height); // This is potentially going to be a hot path depending on how frequently entities change animations
		mCurrentAnim = animationName;
	}	
}

void 
SpriteEntity::MirrorX( bool const mirror )
{
	mXMirrored = mirror;

	if (mirror && mSpatializedComp)
		mSpatializedComp->LocalScale().x = -1.f;
	else if (mSpatializedComp)
		mSpatializedComp->LocalScale().x = 1.f;
}

void 
SpriteEntity::LoadAnimations( std::map< std::string, SpriteAnimationInfo >& animsInfo )
{
	mAnimations = animsInfo;	
}

DirectX::XMFLOAT2 const 
SpriteEntity::CurrentAnimationOffsets( void )
{
	DirectX::XMFLOAT2 ret(0.f, 0.f);

	auto iter = mAnimations.find(mCurrentAnim);
	if ( iter != mAnimations.end() )
	{
		ret.x = iter->second.offsetX;
		ret.y = iter->second.offsetY;
	}

	return ret;
}

DirectX::XMFLOAT2 const 
SpriteEntity::CurrentAnimationHitBoxSize( void )
{
	DirectX::XMFLOAT2 ret(0.f, 0.f);

	auto iter = mAnimations.find(mCurrentAnim);
	if ( iter != mAnimations.end() )
	{
		ret.x = iter->second.hitBoxWidth;
		ret.y = iter->second.hitBoxHeight;	
	}

	return ret;
}



