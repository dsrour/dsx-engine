#include "Debug/Debug.h"
#include "SpriteAnimationLoader.h"

std::map< std::string, SpriteAnimationInfo >
LoadAnimationsInfoFromFile( std::string const& path )
{
	std::map< std::string, SpriteAnimationInfo > anims;

	ticpp::Document doc(path);

	// watch out for exceptions
	try
	{		
		doc.LoadFile();
	}
	catch(ticpp::Exception& ex)
	{
		ex.what(); //suppress VS warning
		OutputDebugMsg("Could not load sprite animation: " + path + "\n");
		return anims;
	}	

	// parsing time
	ticpp::Iterator<ticpp::Element> parent;
	for(parent = parent.begin(doc.FirstChildElement()); parent != parent.end(); parent++)
	{
		std::string str_name;
		parent->GetValue(&str_name);
		//OutputDebugMsg(str_name+"\n");

		if ("animation" == str_name)
		{
			SpriteAnimationInfo info;
			std::string name;

			if (parent->FirstChildElement() != NULL)
			{
				ticpp::Iterator<ticpp::Element> child(parent->FirstChildElement());
				for (child; child != child.end(); child++)
				{
					std::string value; child->GetValue(&value);
					//OutputDebugMsg(value+": ");			
					//OutputDebugMsg(child->GetText()+"\n");		

					if ("diffuse" == value)
					{						
						info.diffusePath = child->GetText();			
					}	
					else if ("normal" == value)
					{						
						info.normalPath = child->GetText();			
					}	
					else if ("name" == value)
					{						
						name = child->GetText();			
					}	
					else if ("width" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.width = val;											
					}	
					else if ("height" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.height = val;											
					}	
					else if ("offset_x" == value)
					{
						std::stringstream stream(child->GetText());
						float val;
						stream >> val; 
						info.offsetX = val;											
					}	
					else if ("offset_y" == value)
					{
						std::stringstream stream(child->GetText());
						float val;
						stream >> val; 
						info.offsetY = val;											
					}	
					else if ("cols" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.cols = val;											
					}	
					else if ("rows" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.rows = val;											
					}	
					else if ("fps" == value)
					{
						std::stringstream stream(child->GetText());
						float val;
						stream >> val; 
						info.fps = val;											
					}	
					else if ("hit_w" == value)
					{
						std::stringstream stream(child->GetText());
						float val;
						stream >> val; 
						info.hitBoxWidth = val;											
					}	
					else if ("hit_h" == value)
					{
						std::stringstream stream(child->GetText());
						float val;
						stream >> val; 
						info.hitBoxHeight = val;											
					}	
					else if ("frame_start_x" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.frameStart[0] = val;											
					}	
					else if ("frame_start_y" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.frameStart[1] = val;											
					}	
					else if ("frame_end_x" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.frameEnd[0] = val;											
					}	
					else if ("frame_end_y" == value)
					{
						std::stringstream stream(child->GetText());
						unsigned int val;
						stream >> val; 
						info.frameEnd[1] = val;											
					}	
					else if ("loop" == value)
					{
						std::stringstream stream(child->GetText());
						bool val;
						stream >> val; 
						info.loop = val;											
					}	
				}
			}

			anims[name] = info;
		}
	}

	return anims;
}

