#pragma once

/*
 * This is an actual entity (it derives SceneObject).  
 * TODO:  THIS IS SUPER HACKY!!!!!  IT RELIED ON A SINGLE
 *	      SUB-ENTITY.... IF EVER.... REVISIT HOW TO DRAW SPRITES 
 *	      WITHOUT USING AN SO.  ALSO, CLEAN UP OR GET RID OF THE 
 *        ASSOCIATED SPATIALIZED COMPONENT!!!!!
 */
  
#include <DirectXMath.h>
#include "Scene/SceneObject.h"

#include "Components/TextureTransformed.h"
#include "2dTiledEngine/Components/TiledEntity.h"
#include "SpriteAnimationSystem/SpriteAnimated.h"
#include "SpriteAnimationSystem/SpriteAnimationLoader.h"


class Level;
class Spatialized;
struct MovingTilesMetadata;

class SpriteEntity : public SceneObject
{
public:
	SpriteEntity(void);
	~SpriteEntity(void);

	void
	LoadAnimations(std::map< std::string, SpriteAnimationInfo >& animsInfo);

	void
	AnimateSprite( std::string const& animationName );

	void
	MirrorX(bool const mirror);

	DirectX::XMFLOAT2 const
	CurrentAnimationOffsets(void);

	DirectX::XMFLOAT2 const
	CurrentAnimationHitBoxSize(void);

	unsigned int const
	SpriteEntityId() const { return mSpriteId; }
		
protected:
	void
	InitSprite(unsigned int w, unsigned int h);

	std::shared_ptr<SpriteAnimated> mSpriteAnimatedComp;
	std::map< std::string, SpriteAnimationInfo > mAnimations;
	bool mXMirrored;
	std::string mCurrentAnim;

	unsigned int mSpriteId;
};

