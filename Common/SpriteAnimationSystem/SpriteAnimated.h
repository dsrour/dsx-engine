#pragma once

#include "Component.hpp"

class SpriteAnimated : public Component<SpriteAnimated>
{
public:    
	SpriteAnimated(void) 
	{
		mSpriteSheetRows = 0;
		mSpriteSheetCols = 0;

		mLoop = false;
		mPlay = false;

		double mFps = 1.0/60.0;

		mCurrentFrame[0] = 0;
		mCurrentFrame[1] = 0;

		mLastFrameTime = 0.0;

		mStartFrame[0] = 0;
		mStartFrame[1] = 0;

		mEndFrame[0] = 0;
		mEndFrame[1] = 0;
	}
	 
	int mSpriteSheetRows;
	int mSpriteSheetCols;

	bool mLoop;
	bool mPlay;

	double mFps;
	double mLastFrameTime;

	int mCurrentFrame[2];	
	int mEndFrame[2];
	int mStartFrame[2];
};