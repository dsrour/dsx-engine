#include <windows.h>
#include <shobjidl.h> 

#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Scene/SceneObject.h"
#include "Scene/ObjRenderable/objRenderable.h"
#include "PostProcessPipeline.h"
#include "Scene/ObjImporter.h"
#include "MaterialsApp.h"

// Systems
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/FurRenderer/FurRenderer.h"
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"

// Components
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Components/DiffuseMapped.h"
#include "Components/ReflectionMapped.h"
#include "Components/FurMapped.h"

float				MaterialsApp::mLightDirection[3] = { 0.25f, -0.43f, -0.87f };
MaterialMenuVars	MaterialsApp::mMaterialMenuVars;
float				MaterialsApp::mFurLength = 1.126f;
unsigned int		MaterialsApp::mTexturesSize = 64;
unsigned int		MaterialsApp::mNumLayers = 30;
unsigned int		MaterialsApp::mStartDensity =  30000;
unsigned int		MaterialsApp::mEndDensity =    15000;
float				MaterialsApp::mStartAlpha = 0.18f;
float				MaterialsApp::mEndAlpha = 0.01f;
float				MaterialsApp::mFurForce[3] = { 0.08f, -1.54f, 0.032f };
float				MaterialsApp::mReflectFactor = 0.06f;
float				MaterialsApp::mRoughness = 0.25f;
float				MaterialsApp::mReflectionCoeff = 2.f;

BaseApp* gApp = new MaterialsApp;
extern std::wstring gResourcesDir;

TwBar*									MaterialsApp::mMenu = NULL;
std::shared_ptr<SceneObject>			MaterialsApp::mSceneObjectBunny = nullptr;
std::shared_ptr<SceneObject>			MaterialsApp::mSceneObjectTpot = nullptr;
std::shared_ptr<SceneObject>			MaterialsApp::mSceneObjectFurBunny = nullptr;
std::shared_ptr<SceneObject>			MaterialsApp::mSceneObjectFurTpot = nullptr;
std::shared_ptr<FurMapped>				MaterialsApp::mFurComponent = nullptr;
std::shared_ptr<ReflectionMapped>		MaterialsApp::mReflectionComponent = nullptr;
bool									MaterialsApp::mMetalToggled = false;
bool									MaterialsApp::mShowingFur = false;
bool									MaterialsApp::mShowingTeapot = false;

MaterialsApp::MaterialsApp() 
{
    WinWidth(1888);
    WinHeight(992);
}

MaterialsApp::~MaterialsApp()
{
}

bool const
MaterialsApp::OnLoadApp()
{		
	TitleName(L"Fur & Metal Materials Demo");

    Renderer()->ClearColor(0.f, 0.f, 0.f, 1.0f);

	// Setup the rendering pipeline
	std::shared_ptr<ForwardRenderer> frwrd_rndrer(new ForwardRenderer(gApp->Renderer()));	
	std::shared_ptr<FurRenderer> fur_rndrer(new FurRenderer(gApp->Renderer()));
	std::shared_ptr<SkyRenderer> sky(new SkyRenderer(gApp->Renderer()));
	std::shared_ptr<PostProcessPipeline> post_proc(new PostProcessPipeline(gApp->Renderer())); // to display fps

	std::list< std::shared_ptr<Pipeline> > path;
	path.push_back(sky); // since the fur blends with what was drawn before, the sky box should render first
	path.push_back(frwrd_rndrer);
	path.push_back(fur_rndrer);
	path.push_back(post_proc);		
	gApp->Renderer()->RenderPath(path);
	
	// Add a directional light
	unsigned int light_id = gApp->EntityMngr()->CreateEntity();
	mLightEmitter = std::make_shared<LightEmitting>();
	DirectionalLight light;
	light.ambient = DirectX::XMFLOAT4(0.05f, 0.05f, 0.05f, 1.f);
	light.diffuse = DirectX::XMFLOAT4(0.75f, 0.75f, 0.75f, 1.f);
	light.specular = DirectX::XMFLOAT4(0.95f, 0.95f, 0.95f, 1.f);
	light.direction = DirectX::XMFLOAT3(0.f, 0.f, -1.f);
	mLightEmitter->SetDirectionalLight(light);
	gApp->EntityMngr()->AddComponentToEntity(light_id, mLightEmitter);

	// Move camera backwards off the center of the scene
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Position(DirectX::XMFLOAT4(0.f, 15.f, 0.f, 1.f));
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Move(-30.0f); 		
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Pitch(0.4f);

	// Create Entities & needed components//////////////////////////////////////	
	std::wstring box_model = gResourcesDir + L"Materials\\skybox.obj";
	std::wstring env_map = gResourcesDir + L"Materials\\sunsetcube1024.dds";
	static std::shared_ptr<SceneObject> skybox = ObjImporter::ImportFromFileSingle(box_model);
	skybox->SetDiffuseMapFromFile(env_map, skybox->SubEntityRefByIndex(0));
	skybox->ToggleBoundBox(false);
	skybox->SetVisibility(true);
	skybox->SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::SKY);
	mReflectionComponent = std::make_shared<ReflectionMapped>(to_string(env_map));

	mSceneObjectBunny = ObjImporter::ImportFromFileSingle(gResourcesDir + L"Materials\\bunny.obj");
	mSceneObjectBunny->ToggleBoundBox(false);
	mSceneObjectBunny->SetVisibility(true);	
	mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->BackCulled(false);
	mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::LIT);
	mSceneObjectTpot = ObjImporter::ImportFromFileSingle(gResourcesDir + L"Materials\\teapot.obj");
	mSceneObjectTpot->ToggleBoundBox(false);
	mSceneObjectTpot->SetVisibility(false);	
	mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->BackCulled(false);
	mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::LIT);
	mSceneObjectTpot->SpatializedComponent()->LocalScale(DirectX::XMFLOAT3(0.25f, 0.25f, 0.25f));
	
	mSceneObjectFurBunny = ObjImporter::ImportFromFileSingle(gResourcesDir + L"Materials\\bunny.obj");
	mSceneObjectFurBunny->ToggleBoundBox(false);
	mSceneObjectFurBunny->SetVisibility(false);
	mSceneObjectFurBunny->SetDiffuseMapFromFile(gResourcesDir + L"Materials\\FurTextures\\puma.dds", mSceneObjectFurBunny->SubEntityRefByIndex(0));

	mSceneObjectFurTpot = ObjImporter::ImportFromFileSingle(gResourcesDir + L"Materials\\teapot.obj");
	mSceneObjectFurTpot->ToggleBoundBox(false);
	mSceneObjectFurTpot->SetVisibility(false);
	mSceneObjectFurTpot->SetDiffuseMapFromFile(gResourcesDir + L"Materials\\FurTextures\\puma.dds", mSceneObjectFurTpot->SubEntityRefByIndex(0));
	mSceneObjectFurTpot->SpatializedComponent()->LocalScale(DirectX::XMFLOAT3(0.25f, 0.25f, 0.25f));

	Material mat;
	mat.ambient = DirectX::XMFLOAT4(0.15f, 0.15f, 0.15f, 1.0f);
	mat.diffuse = DirectX::XMFLOAT4(0.95f, 0.95f, 0.95f, 1.0f);
	mat.specular = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 0.f);		
	mSceneObjectFurBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties(mat);
	mSceneObjectFurBunny->SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::FUR);
	mSceneObjectFurTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties(mat);
	mSceneObjectFurTpot->SubEntityRefByIndex(0)->renderableComp->RenderableType(Renderable::FUR);

	mFurComponent = std::make_shared<FurMapped>();
	void* tmp = nullptr;
	InitFurLayers(tmp);
	mFurComponent->FurLength() = 0.5f;

	gApp->EntityMngr()->AddComponentToEntity(mSceneObjectFurBunny->SubEntityRefByIndex(0)->entityId, mFurComponent);
	gApp->EntityMngr()->AddComponentToEntity(mSceneObjectFurTpot->SubEntityRefByIndex(0)->entityId, mFurComponent);

	mMetalToggled = false;
	
	// Setup menu /////////////////////////////////////////////////////////////////////////////
	mMenu = TwNewBar("SETTINGS");
	TwAddButton(mMenu, "MODEL | LIGHT | MATERIAL", NULL, NULL, "");
	TwAddButton(mMenu, "ToggleModel", ToggleModel, NULL, " label='Bunny / Teapot Toggle' ");
	TwAddVarRW(mMenu, "LightDirection", TW_TYPE_DIR3F, &mLightDirection, "label = 'Light Direction'");
	TwAddVarRW(mMenu, "mat_amb", TW_TYPE_COLOR4F, &mMaterialMenuVars.ambR, (" label=Ambient visible=true "));
	TwAddVarRW(mMenu, "mat_diff", TW_TYPE_COLOR4F, &mMaterialMenuVars.difR, (" label=Diffuse visible=true "));
	TwAddVarRW(mMenu, "mat_spec", TW_TYPE_COLOR4F, &mMaterialMenuVars.specR, (" label=Specular visible=true "));
	TwAddButton(mMenu, "space0", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "space1", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "FUR", NULL, NULL, "");
	TwAddButton(mMenu, "ToggleFur", ToggleFur, NULL, " label='Toggle Fur Material' ");
	TwAddButton(mMenu, "SetDiffTex", SetFurDiffuseTexture, NULL, " label='Set Fur Diffuse Texture' ");
	TwAddVarRW(mMenu, "TextureSize", TW_TYPE_INT32, &mTexturesSize, "label='Texture Size' min=64");
	TwAddVarRW(mMenu, "NumLayers", TW_TYPE_INT32, &mNumLayers, "label='Num Layers' min=0");
	TwAddVarRW(mMenu, "StartDensity", TW_TYPE_INT32, &mStartDensity, "label='Start Density' min=0");
	TwAddVarRW(mMenu, "EndDensity", TW_TYPE_INT32, &mEndDensity, "label='End Density' min=0");
	TwAddVarRW(mMenu, "StartAlpha", TW_TYPE_FLOAT, &mStartAlpha, "label='Start Alpha' min=0.001 step=0.005 precision=3");
	TwAddVarRW(mMenu, "EndAlpha", TW_TYPE_FLOAT, &mEndAlpha, "label='End Alpha' min=0.001 step=0.005 precision=3");
	TwAddButton(mMenu, "Init", InitFurLayers, NULL, " label='Init Layers' ");
	TwAddVarRW(mMenu, "FurLength", TW_TYPE_FLOAT, &mFurLength, "label='Fur Length' min=0.001 step=0.025 precision=3");
	TwAddVarRW(mMenu, "FurForce", TW_TYPE_DIR3F, &mFurForce, "label = 'Fur Force'");
	TwAddButton(mMenu, "space2", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "space3", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "METAL", NULL, NULL, "");
	TwAddButton(mMenu, "ToggleMetal", ToggleMetal , NULL, " label='Toggle Metal Material' ");
	TwAddVarRW(mMenu, "ReflectFactor", TW_TYPE_FLOAT, &mReflectFactor, "label='Reflect Factor' min=0.0 max=1.0 step=0.05");
	TwAddVarRW(mMenu, "Roughness", TW_TYPE_FLOAT, &mRoughness, "label='Roughness' min=0.0 step=0.05");
	TwAddVarRW(mMenu, "ReflCoeff", TW_TYPE_FLOAT, &mReflectionCoeff, "label='Reflection Coefficient' min=0.0 step=0.05");

	TwDefine(" 'SETTINGS' size='350 650' valueswidth=150 alpha=200 ");
	///////////////////////////////////////////////////////////////////////////////////////////

	// User input
	mKeyboardSystem = std::make_shared<ApplicationKeyboardActions>(Renderer()->CurrentCamera(), InputDeviceMngr());
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardSystem);
	mMouseActions = std::make_shared<ApplicationMouseActions>(InputDeviceMngr());
	InputDeviceMngr()->Mouse()->AddMouseActions(mMouseActions);

    return true;
}

    
void
MaterialsApp::OnPreUpdate()
{        

}

void
MaterialsApp::OnPostUpdate()
{           
	DirectX::XMFLOAT3 spawn_dir = (DirectX::XMFLOAT3)mLightDirection; spawn_dir.z *= -1.f;
	static_cast<DirectionalLight*>(mLightEmitter->GetLight())->direction = spawn_dir;

	mFurComponent->FurLength() = mFurLength;
	mFurComponent->Force() = DirectX::XMFLOAT3(mFurForce[0], mFurForce[1], mFurForce[2]);

	mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().ambient = DirectX::XMFLOAT4(mMaterialMenuVars.ambR, mMaterialMenuVars.ambG, mMaterialMenuVars.ambB, mMaterialMenuVars.ambA);
	mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().diffuse = DirectX::XMFLOAT4(mMaterialMenuVars.difR, mMaterialMenuVars.difG, mMaterialMenuVars.difB, mMaterialMenuVars.difA);
	mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().specular = DirectX::XMFLOAT4(mMaterialMenuVars.specR, mMaterialMenuVars.specG, mMaterialMenuVars.specB, mMaterialMenuVars.specA);
	mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().ambient = DirectX::XMFLOAT4(mMaterialMenuVars.ambR, mMaterialMenuVars.ambG, mMaterialMenuVars.ambB, mMaterialMenuVars.ambA);
	mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().diffuse = DirectX::XMFLOAT4(mMaterialMenuVars.difR, mMaterialMenuVars.difG, mMaterialMenuVars.difB, mMaterialMenuVars.difA);
	mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().specular = DirectX::XMFLOAT4(mMaterialMenuVars.specR, mMaterialMenuVars.specG, mMaterialMenuVars.specB, mMaterialMenuVars.specA);

	if (mMetalToggled)
	{
		mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflect = DirectX::XMFLOAT4(mReflectFactor, mReflectFactor, mReflectFactor, 1.f);
		mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().specular.w = mRoughness;
		mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflectionCoeff = mReflectionCoeff;
		mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflect = DirectX::XMFLOAT4(mReflectFactor, mReflectFactor, mReflectFactor, 1.f);
		mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().specular.w = mRoughness;
		mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflectionCoeff = mReflectionCoeff;
	}
}
    
void
MaterialsApp::OnUnloadApp()
{

}

void TW_CALL 
MaterialsApp::InitFurLayers(void* /*clientData*/)
{
	if (gApp)
		mFurComponent->InitFurLayers(gApp->Renderer(), mTexturesSize, mNumLayers, mStartDensity, mEndDensity, mStartAlpha, mEndAlpha);
}

void TW_CALL 
MaterialsApp::ToggleFur(void* /*clientData*/)
{
	if (gApp)
	{		
		mShowingFur = !mShowingFur;

		if (!mShowingFur)
		{
			mSceneObjectFurBunny->SetVisibility(false);
			mSceneObjectFurTpot->SetVisibility(false);
		}
		else
		{
			if (mShowingTeapot)
				mSceneObjectFurTpot->SetVisibility(true);
			else
				mSceneObjectFurBunny->SetVisibility(true);
		}
	}
}

void TW_CALL 
MaterialsApp::ToggleMetal(void* /*clientData*/)
{
	if (gApp)
	{
		mMetalToggled = !mMetalToggled;
		if (!mMetalToggled) // remove metal
		{
			mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflect = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 0.f);
			gApp->EntityMngr()->RemoveComponentFromEntity(mSceneObjectBunny->SubEntityRefByIndex(0)->entityId, ReflectionMapped::GetGuid());
			mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().lightingModel = PHONG;

			mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().reflect = DirectX::XMFLOAT4(0.f, 0.f, 0.f, 0.f);
			gApp->EntityMngr()->RemoveComponentFromEntity(mSceneObjectTpot->SubEntityRefByIndex(0)->entityId, ReflectionMapped::GetGuid());
			mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().lightingModel = PHONG;
		}
		else // add metal
		{	
			gApp->EntityMngr()->AddComponentToEntity(mSceneObjectBunny->SubEntityRefByIndex(0)->entityId, mReflectionComponent);
			mSceneObjectBunny->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().lightingModel = COOK_TORRANCE;

			gApp->EntityMngr()->AddComponentToEntity(mSceneObjectTpot->SubEntityRefByIndex(0)->entityId, mReflectionComponent);
			mSceneObjectTpot->SubEntityRefByIndex(0)->renderableComp->MaterialProperties().lightingModel = COOK_TORRANCE;
		}
	}
}

std::wstring 
MaterialsApp::OpenFileDialog()
{
	std::wstring ret;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					// Display the file name to the user.
					if (SUCCEEDED(hr))
					{
						//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
						ret = pszFilePath;
						CoTaskMemFree(pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}
	return ret;
}

void TW_CALL 
MaterialsApp::SetFurDiffuseTexture(void* /*clientData*/)
{
	std::wstring file = OpenFileDialog();
	mSceneObjectFurBunny->SetDiffuseMapFromFile(file, mSceneObjectFurBunny->SubEntityRefByIndex(0));
	mSceneObjectFurTpot->SetDiffuseMapFromFile(file, mSceneObjectFurTpot->SubEntityRefByIndex(0));
}

void TW_CALL 
MaterialsApp::ToggleModel(void* /*clientData*/)
{
	mShowingFur = false;
	mSceneObjectFurBunny->SetVisibility(false);
	mSceneObjectFurTpot->SetVisibility(false);

	mShowingTeapot = !mShowingTeapot;	

	std::wstring mode_file_name;
	if (mShowingTeapot)
	{		
		mSceneObjectBunny->SetVisibility(false);
		mSceneObjectTpot->SetVisibility(true);
	}
	else
	{
		mSceneObjectBunny->SetVisibility(true);
		mSceneObjectTpot->SetVisibility(false);
	}
}
