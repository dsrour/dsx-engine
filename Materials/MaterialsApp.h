#pragma once

#include <memory>
#include <string>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "KeyboardActions.h"
#include "MouseActions.h"
#include "BaseApp.h"

class LightEmitting;
class FurMapped;
class ReflectionMapped;

struct MaterialMenuVars
{
	MaterialMenuVars()
	{
		ambR = ambG = ambB = 12/255.f;
		difR = difG = difB = 25/255.f;		
		specR = specG = specB = 45/255.f;
		specA = 160/255.f;
		ambA = difA = 1.f;		
	}

	float ambR, ambG, ambB, ambA;
	float difR, difG, difB, difA;
	float specR, specG, specB, specA;
};

class MaterialsApp : public BaseApp
{
public:
    MaterialsApp(void);

    virtual
    ~MaterialsApp(void);

    bool const
    OnLoadApp(void);
    
    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);
        
    void
    OnUnloadApp(void);
   
private: 
	static std::wstring
	OpenFileDialog();

	static void TW_CALL
	InitFurLayers(void* /*clientData*/);

	static void TW_CALL
	SetFurDiffuseTexture(void* /*clientData*/);

	static void TW_CALL
	ToggleFur(void* /*clientData*/);

	static void TW_CALL
	ToggleMetal(void* /*clientData*/);

	static void TW_CALL
	ToggleModel(void* /*clientData*/);

	std::shared_ptr<ApplicationKeyboardActions> mKeyboardSystem;
	std::shared_ptr<ApplicationMouseActions>	mMouseActions;

	std::shared_ptr<LightEmitting> mLightEmitter;
	static std::shared_ptr<SceneObject> mSceneObjectBunny;
	static std::shared_ptr<SceneObject> mSceneObjectTpot;

	// Fur
	static std::shared_ptr<SceneObject> mSceneObjectFurBunny;
	static std::shared_ptr<SceneObject> mSceneObjectFurTpot;
	static std::shared_ptr<FurMapped>	mFurComponent;

	// Metal reflection
	static std::shared_ptr<ReflectionMapped>	 mReflectionComponent;

	static bool mMetalToggled;
	static bool mShowingFur;
	static bool mShowingTeapot;

	// Menu vars ///////////////////////////////////////////////////
	static TwBar*           mMenu;
	static float			mLightDirection[3];	
	static MaterialMenuVars mMaterialMenuVars;
	static float			mFurLength;
	static unsigned int		mTexturesSize;
	static unsigned int		mNumLayers;
	static unsigned int		mStartDensity;
	static unsigned int		mEndDensity;
	static float			mStartAlpha;
	static float			mEndAlpha;
	static float			mFurForce[3];
	static float			mReflectFactor;
	static float			mRoughness;
	static float			mReflectionCoeff;
	////////////////////////////////////////////////////////////////
};

