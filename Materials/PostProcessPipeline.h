#pragma once

#include "Systems/Renderer/DirectXTK/Inc/SpriteFont.h"
#include "Systems/Renderer/Pipeline.h"


class PostProcessPipeline : public Pipeline
{
public:
	PostProcessPipeline(std::shared_ptr<D3dRenderer> renderer);
    
    virtual 
    ~PostProcessPipeline(void);
          
    virtual void
    MadeActive(void) {}

    virtual void
    MadeInactive(void) {}

	virtual void
	RecompileShaders() { /*no shaders*/ }

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);
        
private:	
	DirectX::SpriteFont*	  mSpriteFont;   
	DirectX::SpriteBatch*	  mFpsSprite;
};