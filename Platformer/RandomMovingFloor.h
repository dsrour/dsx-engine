#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Mass.h"
#include "2dTiledEngine/TileEntity.h"

class RandomMovingFloor : public TileEntity
{
public:
	RandomMovingFloor();
	~RandomMovingFloor(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	void
	SetVelocity(DirectX::XMFLOAT3 const& velocity);

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);	
		
private:
	DirectX::XMFLOAT3
	RandomVelocity(float min, float max);

	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<Velocity> mVelocity;
	std::shared_ptr<Elasticity> mElasticity;
	std::shared_ptr<Mass> mMass;

	float mWidth, mHeight;

	float mMaxSpeed;

	DirectX::XMFLOAT2 mLastNormal;
};

