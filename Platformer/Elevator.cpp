#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "CommonCollisionResolutions.h"
#include "Utils/MathUtils.h"
#include "Elevator.h"

extern BaseApp* gApp;

Elevator::Elevator(std::shared_ptr<EntityManager> entityManager, double const waitTime, 
				   float const heightTravel, float const velocity) : System(entityManager)
{	
	mWaitTime = waitTime;
	mHeightTravel = std::fabsf(heightTravel);
	if (heightTravel > 0.f) 
		mGoesUp = true;
	else 
		mGoesUp = false;
	mVelocity = std::fabsf(velocity);
	mArrivalTime = -123.21; // any negative value
}


Elevator::~Elevator(void)
{
	if (gApp)
	{
		// Stop and remove systems
		gApp->SystemSchdlr()->StopSystem(mSysId);
		gApp->SystemMngr()->RemoveSystem(mSysId);
	}	
}

void 
Elevator::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	std::shared_ptr<System> me(shared_from_this());
	mSysId = gApp->SystemMngr()->AddSystem(me);
	double target = 1.0 / 30.0;
	gApp->SystemSchdlr()->StopSystem(mSysId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mSysId, target);

	mWidth = size.x;
	mHeight = size.y;


	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	mStartPosY = world_pos.y;
	if (mGoesUp)
		mEndPosY = mStartPosY + mHeightTravel;
	else
		mEndPosY = mStartPosY - mHeightTravel;
	
	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mWidth/2.f), (mHeight/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mWidth/2.f), (mHeight/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, (mHeight/3.f), 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, (mHeight/3.f), 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, (mHeight/2.f), 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, (mHeight/2.f), 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight-(mHeight/3.f), 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight-(mHeight/3.f), 0.f ) );


	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(0.f, 1.f, 0.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);

	// Velocity + mElasticity
	mElasticity = std::make_shared<Elasticity>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mElasticity);
	
	mVelocityComp = std::make_shared<Velocity>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocityComp);	

	// Mass
	mMass = std::make_shared<Mass>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mMass);
		
	mTiledEntityComp->IsObstacle(true);
	mTiledEntityComp->IsElasticRigidBody(true);

	mLastNormal = DirectX::XMFLOAT2(0.f, 0.f);
}



void 
Elevator::OnLevelRemoval( Level* const level )
{

}


/*virtual*/ void
Elevator::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	MovingPlatformResolution(EntityId(), level, collisionInfo, mSpatializedComp->LocalPosition());

	if (collisionInfo.collidingEntityId == EntityId() && collisionInfo.normal.y == 1) // I'm being collided on by something on top of me
	{
		TileEntity* moving_entity = level->Entity(collisionInfo.movingEntityId);
		Health* moving_health = HealthComponent(collisionInfo.movingEntityId);	

		if ( IsEqual(mSpatializedComp->LocalPosition().y, mStartPosY) && 
			collisionInfo.normal.y == 1 && 
			moving_entity->IsObstacle() && 
			moving_health )
		{
			if (mGoesUp)
				mVelocityComp->VelocityVector().y = mVelocity;
			else 
				mVelocityComp->VelocityVector().y = -mVelocity;

			mGoingToDest = true;
		}
	}
}

void 
Elevator::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	if (mVelocityComp->VelocityVector().y != 0.f) 
	{
		if ( mGoingToDest )
		{
			float height_travelled;
			if (mGoesUp)
				height_travelled = mSpatializedComp->LocalPosition().y - mStartPosY;
			else
				height_travelled = mStartPosY - mSpatializedComp->LocalPosition().y;

			if (height_travelled >= mHeightTravel)
			{
				mVelocityComp->VelocityVector().y = 0.f;
				mArrivalTime = gApp->Timer().ElapsedTimeSecs();
				mGoingToDest = false;
				mSpatializedComp->LocalPosition().y = mEndPosY;
			}			
 		}
		else 
		{
			float height_travelled;
			if (mGoesUp)
				height_travelled = mEndPosY - mSpatializedComp->LocalPosition().y;
			else
				height_travelled = mSpatializedComp->LocalPosition().y - mEndPosY;

			if (height_travelled >= mHeightTravel)
			{
				mVelocityComp->VelocityVector().y = 0.f;
				mGoingToDest = true;
				mSpatializedComp->LocalPosition().y = mStartPosY;
			}			
		}
	}
	else
	{
		if ( gApp->Timer().ElapsedTimeSecs() - mArrivalTime > mWaitTime && !mGoingToDest )
		{
			mArrivalTime = -123.21f;

			if (mGoesUp)
				mVelocityComp->VelocityVector().y = -mVelocity;
			else 
				mVelocityComp->VelocityVector().y = mVelocity;	
		}
	}
		
}

