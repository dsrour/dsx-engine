#include "Player.h"
#include "PlayerInputs.h"


PlayerKeyboardInputs::PlayerKeyboardInputs(std::shared_ptr<InputDeviceManager> inputDeviceManager, Player* const player) : InputDeviceActions(inputDeviceManager)							  
{
	mPlayer = player;
}

/*virtual*/ void
PlayerKeyboardInputs::OnStateChange(void)
{
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if ( manager->Keyboard()->IsKeyDown(VK_ESCAPE) )
	{
		gApp->EndEngine();
	}

	
	
	// Movement
	int dir[2] = {0, 0} ;

	if ( manager->Keyboard()->IsKeyPressed('D') && !manager->Keyboard()->IsKeyPressed('A') )
		dir[0] = 1;
	else if ( !manager->Keyboard()->IsKeyPressed('D') && manager->Keyboard()->IsKeyPressed('A') )
		dir[0] = -1;
	else 	
		dir[0] = 0;
	
	

	if ( manager->Keyboard()->IsKeyPressed('W') )
	{
		dir[1] = 1;
	}
	
	mPlayer->UpdateInputs( dir, manager->Keyboard()->IsKeyPressed(VK_SHIFT) );
}  

PlayerMouseInputs::PlayerMouseInputs( std::shared_ptr<InputDeviceManager> inputDeviceManager, Player* const player ) : InputDeviceActions(inputDeviceManager)		
{
	mPlayer = player;
}

void 
PlayerMouseInputs::OnStateChange( void )
{
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if ( MouseInputDevice::CLICK == manager->Mouse()->ButtonState(MouseInputDevice::LEFT) ||
		 MouseInputDevice::DBLE_CLICK == manager->Mouse()->ButtonState(MouseInputDevice::LEFT) )
	{
		mPlayer->Attack(manager->Mouse()->MouseX(), manager->Mouse()->MouseY());
	}
}
