#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/TileEntity.h"
#include "System.h"

class PlayerHealthTrigger : public TileEntity
{
public:
	PlayerHealthTrigger(int const healthModifier, double const timeDelta);
	~PlayerHealthTrigger(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PostCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PreCollision( Level* const level, CollisionInfo const& collisionInfo );
		
private:
	std::shared_ptr<LineRenderable> mRenderableComponent;
	int mHealthModifier;
	double mTimeDelta;

	double mLastTime;

	int mPlayerId;
	DirectX::XMFLOAT3 mPlayerVel;
};

