#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Mass.h"
#include "2dTiledEngine/Components/Acceleration.h"
#include "2dTiledEngine/Components/Friction.h"
#include "2dTiledEngine/TileEntity.h"
#include "SpriteAnimationSystem/SpriteEntity.h"
#include "System.h"

class GenericTile : public TileEntity
{
public:
	GenericTile(void);
	~GenericTile(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);

	std::shared_ptr<Velocity>& 
	VelocityComponent()  { return mVelocity; }

	std::shared_ptr<Acceleration>& 
	AccelerationComponent()  { return mAccel; }

	std::shared_ptr<Elasticity>& 
	ElasticityComponent()  { return mElasticity; }

	std::shared_ptr<Friction>& 
	FrictionComponent()  { return mFriction; }

	std::shared_ptr<Mass>& 
	MassComponent()  { return mMass; }

	void
	SetSprite(std::shared_ptr<SpriteEntity> sprite);
		
private:
	std::shared_ptr<SpriteEntity> mSprite;

	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<Velocity> mVelocity;
	std::shared_ptr<Acceleration> mAccel;
	std::shared_ptr<Elasticity> mElasticity;
	std::shared_ptr<Friction> mFriction;
	std::shared_ptr<Mass> mMass;

	float mWidth, mHeight;
};

