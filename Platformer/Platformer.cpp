#include <ctime>
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "PostProcessPipeline.h"
#include "Platformer.h"

BaseApp* gApp = new Platformer;

Platformer::Platformer(void)
{
	srand((unsigned)time(0));

	WinWidth(1888);
	WinHeight(992);

	mEventMngr = std::make_shared<EventManager>();
}


Platformer::~Platformer(void)
{
}











//////////////////////////////////////////////
#include "2dTiledEngine/Level.h"
#include "LevelLoader.h"

bool const 
Platformer::OnLoadApp( void )
{
	// Set Render Path with a post-process pipeline
	// Set Render Path with a post-process pipeline
	std::shared_ptr<ForwardRenderer> forward_renderer(new ForwardRenderer(gApp->Renderer()));
	std::shared_ptr<PostProcessPipeline> post_proc_pipeline(new PostProcessPipeline(gApp->Renderer(), mEventMngr));
	std::list< std::shared_ptr<Pipeline> > path; 
	path.push_back(forward_renderer); 
	path.push_back(post_proc_pipeline); 
	gApp->Renderer()->RenderPath(path);

	//gApp->Renderer()->ClearColor(0.f, 0.f, 1.f, 1.f);

	// Orthogonal view, move a unit back as the game level is @ z = 0.f;
	Renderer()->CurrentCamera()->ProjMatrix(DirectX::XM_PI, (float)WinWidth(), (float)WinHeight(), 0.1f, 1000.0f, false);   
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Position(DirectX::XMFLOAT4(0.f, 0.f, -1.f, 1.f));	

	// Count down timer	
	double const count_down_timer_target = 1.0/30.0;
	mCountDownTimer = std::make_shared<CountDownTimer>(gApp->EntityMngr(), mEventMngr);
	mCountDownTimerSysId = gApp->SystemMngr()->AddSystem(mCountDownTimer);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mCountDownTimerSysId, count_down_timer_target);

	// Static scene object manager
	mStaticSceneObjManager = std::make_shared<SceneObjectManager>();

	// SpatializedAssociationSystem
	double const sp_assoc_sys_target = 1.0/62.0;
	mSpatializedAssociationSystem = std::make_shared<SpatializedAssociationSystem>(gApp->EntityMngr());
	mSpatializedAssociationSysId = gApp->SystemMngr()->AddSystem(mSpatializedAssociationSystem);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mSpatializedAssociationSysId, sp_assoc_sys_target);

	// Create level	
	mLevel = std::make_shared<Level>( 40, std::pair<unsigned int, unsigned int>(500, 60), mEventMngr );
	std::wstring wpath = gResourcesDir + L"\\Platformer\\test_level.xml";
	std::string lvl_path(wpath.begin(), wpath.end());
	LevelLoader* loader = new LevelLoader(mLevel.get(), mEventMngr);
	loader->LoadLevelFromFile(lvl_path, mStaticSceneObjManager);
	delete loader;

	mCountDownTimer->Start(30.0);

	return true;
}
//////////////////////////////////////////////


void 
Platformer::OnPreUpdate( void )
{

}

void 
Platformer::OnPostUpdate( void )
{
	mLevel->PostFrame();
}

void 
Platformer::OnUnloadApp( void )
{

}
