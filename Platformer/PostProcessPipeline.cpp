#include <sstream> 
#include <iostream>
#include <iomanip>
#include "EventMetadata.h"
#include "PostProcessPipeline.h"


PostProcessPipeline::PostProcessPipeline( std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EventManager> eventMngr ) : Pipeline(renderer)
{
	mEventManager = eventMngr;
	mEventManager->SubscribeToEvent(gTimeRemainingEventName, this);
	mEventManager->SubscribeToEvent(gPlayerHealthEventName, this);

	mFpsSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mTimerSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mPlayerHealthSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());

	mTimerValue = 0.0;
	std::wstring font_path = gResourcesDir + L"Fonts\\font.spritefont";
	mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (WCHAR*)font_path.c_str());
}

PostProcessPipeline::~PostProcessPipeline( void )
{
	mEventManager->UnsubscribeToEvent(gTimeRemainingEventName, this);
	mEventManager->UnsubscribeToEvent(gPlayerHealthEventName, this);

	if (mSpriteFont)
		delete mSpriteFont;

	if (mFpsSprite)
		delete mFpsSprite;

	if (mTimerSprite)
		delete mTimerSprite;

	if (mPlayerHealthSprite)
		delete mPlayerHealthSprite;
}

void 
PostProcessPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	// FPS
	{
		// Since this gets called every time we render... we just see how many frames we get per second
		static double elapsed = gApp->Timer().ElapsedTimeSecs();
		static unsigned int frames = 0;			
		frames++;
					
		static std::wstring fps_str;

		if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
		{
			std::wstringstream ss;   
			ss << frames; 
			fps_str = ss.str() + L" FPS";
			frames = 0;
			elapsed = gApp->Timer().ElapsedTimeSecs();
		}						
		mFpsSprite->Begin();
		mSpriteFont->DrawString(mFpsSprite, fps_str.c_str(), DirectX::XMFLOAT2(25.f, gApp->WinHeight() - 30.f), DirectX::XMVectorSet(1.f,0.f,0.f,1.f));
		mFpsSprite->End();
	}	

	// TIMER
	{
		std::wstringstream ss;   
		ss << mTimerValue; 
		std::wstring time_str;

		auto color = DirectX::XMVectorSet(0.75f,0.75f,0.75f,1.f);

		if (mTimerValue <= 0.0)
		{
			time_str = L"KABOOM BITCH!";
			color = DirectX::XMVectorSet(1.f,0.01f,0.01f,1.f);
		}
		else
			time_str = ss.str();		

		mTimerSprite->Begin();
		mSpriteFont->DrawString(mTimerSprite, time_str.c_str(), DirectX::XMFLOAT2(25.f, 10.f), color);
		mTimerSprite->End();
	}

	// PLAYER HEALTH
	{
		std::wstringstream ss;   
		ss << mPlayerHealth; 
		std::wstring health_str;

		auto color = DirectX::XMVectorSet(0.f,0.34f,0.f,1.f);

		if (mPlayerHealth <= 0)
		{
			health_str = L"You are terminated :(";
			color = DirectX::XMVectorSet(1.f,0.f,0.f,1.f);
		}
		else if (mPlayerHealth <= 25)
		{
			health_str = ss.str() + L"%";	
			color = DirectX::XMVectorSet(1.f,0.02f,0.f,1.f);
		}
		else
			health_str = ss.str() + L"%";	

		mPlayerHealthSprite->Begin();
		mSpriteFont->DrawString(mPlayerHealthSprite, health_str.c_str(), DirectX::XMFLOAT2(25.f, 32.5f), color);
		mPlayerHealthSprite->End();
	}
}

void 
PostProcessPipeline::OnEvent( std::string const& event, Metadata* const metadata )
{
	if (event == gTimeRemainingEventName)
	{
		mTimerValue = static_cast<CountDownTimerMetadata*>(metadata)->timeLeft;
	}
	if (event == gPlayerHealthEventName)
	{
		mPlayerHealth = static_cast<PlayerHealthMetadata*>(metadata)->playerHealth;
	}
	
}
