#pragma once

#include <string>
#include "EventManager/EventManager.h"

std::string const gTimeRemainingEventName = "remainingTime";
struct CountDownTimerMetadata : public Metadata
{
	double timeLeft;
};

std::string const gPlayerHealthEventName = "remainingPlayerHealth";
struct PlayerHealthMetadata : public Metadata
{
	int playerHealth;
};