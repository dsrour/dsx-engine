#pragma once

#include "EventManager/EventManager.h"
#include "System.h"

class CountDownTimer : public System, public std::enable_shared_from_this<CountDownTimer>
{
public:
	CountDownTimer(std::shared_ptr<EntityManager> entityManager, std::shared_ptr<EventManager> eventMngr);
	~CountDownTimer(void);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

	void
	Start(double const startTime) { mTimeLeft = startTime; mStarted = true; }

private:
	std::shared_ptr<EventManager> mEventManager;
	
	double mTimeLeft;
	bool mStarted;
};

