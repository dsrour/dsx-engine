#include "BackgroundSprite.h"

BackgroundSprite::BackgroundSprite( float const depth )
{	
	mDepth = depth;
}

BackgroundSprite::~BackgroundSprite( void )
{

}

void 
BackgroundSprite::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, mDepth);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (size.x/2.f), (size.y/2.f), mDepth );
	DirectX::XMFLOAT3 extents( (size.x/2.f), (size.y/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	mTiledEntityComp->IsObstacle(false);
	mTiledEntityComp->IsElasticRigidBody(false);
}

void 
BackgroundSprite::OnLevelRemoval( Level* const level )
{

}

void 
BackgroundSprite::OnCollision( Level* const level, CollisionInfo const& collisionInfo )
{

}

void 
BackgroundSprite::PostCollision( Level* const level, CollisionInfo const& collisionInfo )
{

}

void 
BackgroundSprite::PreCollision( Level* const level, CollisionInfo const& collisionInfo )
{

}
