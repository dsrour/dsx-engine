#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Utils/MathUtils.h"
#include "Player.h"
#include "CommonCollisionResolutions.h"
#include "Projectile.h"

extern BaseApp* gApp;

Projectile::Projectile(std::shared_ptr<EntityManager> entityManager, float size[],  unsigned int owner) : System(entityManager)
{
	mSize[0] = size[0]; mSize[1] = size[1];
	mProjectileOwner = owner;

	mActive = false;

	mOwnerVel = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mSize[0]/2.f), (mSize[1]/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mSize[0]/2.f), (mSize[1]/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mSize[0], 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mSize[0], 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mSize[0], mSize[1], 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mSize[0], mSize[1], 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mSize[1], 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mSize[1], 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );	

	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(0.88f, 0.88f, 0.88f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	

	// Velocity
	mVelocityComp = std::make_shared<Velocity>();	
		
	mTiledEntityComp->IsObstacle(false);
}


Projectile::~Projectile(void)
{
	if (gApp)
	{
		// Stop and remove systems
		gApp->SystemSchdlr()->StopSystem(mSysId);
		gApp->SystemMngr()->RemoveSystem(mSysId);
	}
}

void 
Projectile::Activate( Level* const level, float startPos[], float velocity[], float maxTravelDist, unsigned int damageToInflict )
{
	mStartPos[0] = startPos[0]; mStartPos[1] = startPos[1];
	mVelocity[0] = velocity[0]; mVelocity[1] = velocity[1];
	mMaxTravelDist = maxTravelDist;
	mDamageToInflict = damageToInflict;
	mLevelRef = level;

	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(startPos[0], startPos[1], 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Velocity
	mVelocityComp->VelocityVector(DirectX::XMFLOAT3(velocity[0], velocity[1], 0.f));

	std::shared_ptr<TileEntity> me(shared_from_this());
	level->InsertTileEntity(DirectX::XMFLOAT2(), DirectX::XMFLOAT2(), me);

	mActive = true;	
}

void 
Projectile::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	std::shared_ptr<Projectile> me(shared_from_this());
	mSysId = gApp->SystemMngr()->AddSystem(me);
	double target = 1.0 / 60.0;
	gApp->SystemSchdlr()->StopSystem(mSysId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mSysId, target);

	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocityComp);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);
}



void 
Projectile::OnLevelRemoval( Level* const level )
{
	gApp->EntityMngr()->RemoveComponentFromEntity(EntityId(), mRenderableComponent->GetComponentTypeGuid());
	gApp->EntityMngr()->RemoveComponentFromEntity(EntityId(), mVelocityComp->GetComponentTypeGuid());
}


/*virtual*/ void
Projectile::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	if (collisionInfo.movingEntityId == mProjectileOwner ||
		collisionInfo.collidingEntityId == mProjectileOwner)
		return;

	unsigned int id = EntityId();
	if (collisionInfo.movingEntityId == id)
		id = collisionInfo.collidingEntityId;

	Damageable* damageable = DamageableComponent(id);

	if (damageable)
	{
		unsigned int DAMAGE_TO_INFLICT = 1;
		damageable->InflictedDamage() = DAMAGE_TO_INFLICT;

		if (id == collisionInfo.movingEntityId)
		{
			damageable->HitNormal()[0] = (int)collisionInfo.normal.x;
			damageable->HitNormal()[1] = (int)collisionInfo.normal.y;
		}
		else
		{
			damageable->HitNormal()[0] = -(int)collisionInfo.normal.x;
			damageable->HitNormal()[1] = -(int)collisionInfo.normal.y;
		}	

		mActive = false;

		std::shared_ptr<TileEntity> me(shared_from_this());
		mLevelRef->RemoveTileEntity(EntityId());		
	}

	else if (level->Entity(id)->IsObstacle())
	{
		mActive = false;
		std::shared_ptr<TileEntity> me(shared_from_this());
		mLevelRef->RemoveTileEntity(EntityId());		
	}
}

void 
Projectile::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{
	if (!mActive)
		return;

	float dist_sqrd = std::powf(mSpatializedComp->LocalPosition().x-mStartPos[0], 2.f) +
		              std::powf(mSpatializedComp->LocalPosition().y-mStartPos[1], 2.f);

	if (dist_sqrd >= mMaxTravelDist*mMaxTravelDist)
	{
		mLevelRef->RemoveTileEntity(EntityId());
		mActive = false;
	}
}

void 
Projectile::PreCollision( Level* const level, CollisionInfo const& collisionInfo )
{
	Velocity* vel = VelocityComponent(mProjectileOwner);

	if (vel &&
		( collisionInfo.collidingEntityId == mProjectileOwner || 
		  collisionInfo.movingEntityId == mProjectileOwner ) )	
		mOwnerVel = vel->VelocityVector();
}

void 
Projectile::PostCollision( Level* const level, CollisionInfo const& collisionInfo )
{
	Velocity* vel = VelocityComponent(mProjectileOwner);

	if (vel &&
		( collisionInfo.collidingEntityId == mProjectileOwner || 
		collisionInfo.movingEntityId == mProjectileOwner ) )	
		vel->VelocityVector(mOwnerVel);
}



