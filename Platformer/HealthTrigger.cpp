#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Player.h"
#include "HealthTrigger.h"

extern BaseApp* gApp;

PlayerHealthTrigger::PlayerHealthTrigger(int const healthModifier, double const timeDelta)
{
	mPlayerId = -1;
	mTimeDelta = timeDelta;
	mHealthModifier = healthModifier;
	mLastTime = gApp->Timer().ElapsedTimeSecs();
}


PlayerHealthTrigger::~PlayerHealthTrigger(void)
{

}

void 
PlayerHealthTrigger::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{	
	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (size.x/2.f), (size.y/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (size.x/2.f), (size.y/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( size.x, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( size.x, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( size.x, size.y, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( size.x, size.y, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, size.y, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, size.y, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	if (mHealthModifier >= 0)
		mat.diffuse = DirectX::XMFLOAT4(0.f, 0.f, 1.f, 1.f);
	else
		mat.diffuse = DirectX::XMFLOAT4(1.f, 0.f, 0.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);

	mTiledEntityComp->IsObstacle(false);
	mTiledEntityComp->IsElasticRigidBody(false);
}



void 
PlayerHealthTrigger::OnLevelRemoval( Level* const level )
{

}


/*virtual*/ void
PlayerHealthTrigger::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	double cur_time = gApp->Timer().ElapsedTimeSecs();

	if (mPlayerId >= 0 && cur_time - mLastTime >= mTimeDelta)
	{
		HealthComponent(mPlayerId)->CurrentHealth() += mHealthModifier;
		mLastTime = cur_time;
	}	
}

void 
PlayerHealthTrigger::PreCollision( Level* const level, CollisionInfo const& collisionInfo )
{
	unsigned int other_id = collisionInfo.collidingEntityId;
	if (other_id == EntityId())
		other_id = collisionInfo.movingEntityId;

	Player* player = dynamic_cast<Player*>(level->Entity(other_id));

	if (player)
		mPlayerId = other_id;
	else
		mPlayerId = -1;

	if (mPlayerId >= 0)
	{
		Velocity* vel = VelocityComponent(mPlayerId);
		mPlayerVel = vel->VelocityVector();
	}	
}

void 
PlayerHealthTrigger::PostCollision( Level* const level, CollisionInfo const& collisionInfo )
{
	if (mPlayerId >= 0)
	{
		Velocity* vel = VelocityComponent(mPlayerId);
		vel->VelocityVector(mPlayerVel);
	}
}