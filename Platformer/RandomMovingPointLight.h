#pragma once

#include <random>
#include "CommonRenderables/LineRenderable.h"
#include "Components/LightEmitting.h"
#include "2dTiledEngine/Components/Acceleration.h"
#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/TileEntity.h"

class RandomMovingPointLight : public TileEntity
{
public:
	RandomMovingPointLight(float const radiusSqr);
	~RandomMovingPointLight(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);	
		
private:
	DirectX::XMFLOAT4 
	RandomColor(void);

	DirectX::XMFLOAT3 
	RandomAcceleration(void);

	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<LightEmitting> mLight;

	float mRadiusSqr;

	static std::mt19937 mRdmEng;
	static uint32_t mSeedVal;	

	DirectX::XMFLOAT3 mOriginalPos;

	DirectX::XMFLOAT3 mCurrentDestinationOffset;
	std::shared_ptr<Acceleration> mAccelComp;
	std::shared_ptr<Velocity> mVelocity;
};

