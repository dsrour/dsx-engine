#pragma once

/*
 * This system will adjust the associated entity's spatialized component to 
 * match the one of the current entity that has a SpatializedAssociated component.
 *
 * Useful for keeping sprites aligned with actual TileEntity.
 */

#include "System.h"

class SpatializedAssociationSystem : public System, public std::enable_shared_from_this<SpatializedAssociationSystem>
{
public:
	SpatializedAssociationSystem(std::shared_ptr<EntityManager> entityManager);
	~SpatializedAssociationSystem(void);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);
	
private:
};

