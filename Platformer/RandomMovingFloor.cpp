#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Utils/MathUtils.h"
#include "CommonCollisionResolutions.h"
#include "RandomMovingFloor.h"

extern BaseApp* gApp;

RandomMovingFloor::RandomMovingFloor()
{
	mMaxSpeed = 3.5f;
}


RandomMovingFloor::~RandomMovingFloor(void)
{

}

void 
RandomMovingFloor::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	mWidth = size.x;
	mHeight = size.y;


	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mWidth/2.f), (mHeight/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mWidth/2.f), (mHeight/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(0.f, 1.f, 0.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);

	// Velocity + mElasticity
	mElasticity = std::make_shared<Elasticity>(3.0f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mElasticity);
	
	mVelocity = std::make_shared<Velocity>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocity);
	
	mVelocity->VelocityVector(RandomVelocity(-mMaxSpeed, mMaxSpeed));

	// Mass
	mMass = std::make_shared<Mass>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mMass);

	// Make it an obstacle
	mTiledEntityComp->IsObstacle(true);

	mTiledEntityComp->IsElasticRigidBody(false);

	mLastNormal = DirectX::XMFLOAT2(0.f, 0.f);
}



void 
RandomMovingFloor::OnLevelRemoval( Level* const level )
{

}


/*virtual*/ void
RandomMovingFloor::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	MovingPlatformResolution(EntityId(), level, collisionInfo, mSpatializedComp->LocalPosition());

	unsigned int other_id = collisionInfo.collidingEntityId;
	if (other_id == EntityId())
		other_id = collisionInfo.movingEntityId;

	TileEntity* colliding_entity = level->Entity(other_id);
	Health* colliding_health = HealthComponent(other_id);

	if ( !colliding_health && colliding_entity )
	{
		mVelocity->VelocityVector(RandomVelocity(-mMaxSpeed, mMaxSpeed));		
		mLastNormal = collisionInfo.normal;
	}
}

DirectX::XMFLOAT3 
RandomMovingFloor::RandomVelocity( float min, float max )
{
	return DirectX::XMFLOAT3( min + (float)rand()/((float)RAND_MAX/(max-min) ),
							  min + (float)rand()/((float)RAND_MAX/(max-min) ), 0.f );
}
