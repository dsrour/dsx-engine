#include "BaseApp.h"
#include "EventMetadata.h"
#include "CountDownTimer.h"

extern BaseApp* gApp;

CountDownTimer::CountDownTimer(std::shared_ptr<EntityManager> entityManager, std::shared_ptr<EventManager> eventMngr) : System(entityManager)
{
	mEventManager = eventMngr;
	mStarted = false;
	mTimeLeft = 0.0;	
}


CountDownTimer::~CountDownTimer(void)
{
}

void 
CountDownTimer::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	if (mStarted)
	{
		double static last_time = gApp->Timer().ElapsedTimeSecs();		
		double curr_time = gApp->Timer().ElapsedTimeSecs();

		mTimeLeft -= (curr_time - last_time);
		last_time = curr_time;

		if (mTimeLeft < 0.0)
		{
			mTimeLeft = 0.0;
			mStarted = false;
		}

		CountDownTimerMetadata meta;
		meta.timeLeft = mTimeLeft;
		mEventManager->BroadcastEvent(gTimeRemainingEventName, &meta);
	}
}
