#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include "InputDeviceManager.h"
#include "BaseApp.h"

extern BaseApp* gApp;
class Player;

class PlayerKeyboardInputs : public InputDeviceActions
{
public:
	PlayerKeyboardInputs(std::shared_ptr<InputDeviceManager> inputDeviceManager, Player* const player);

	
	virtual void
	OnStateChange(void);


	
private:
	Player* mPlayer;
};

class PlayerMouseInputs : public InputDeviceActions
{
public:	
	PlayerMouseInputs(std::shared_ptr<InputDeviceManager> inputDeviceManager, Player* const player);

	virtual void
	OnStateChange(void);


private:
	Player* mPlayer;
};
