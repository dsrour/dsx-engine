#include "BaseApp.h"
#include "EventMetadata.h"
#include "SpatializedAssociated.h"
#include "Components/Spatialized.h"
#include "SpatializedAssociationSystem.h"

extern BaseApp* gApp;

SpatializedAssociationSystem::SpatializedAssociationSystem(std::shared_ptr<EntityManager> entityManager) : System(entityManager)
{
	// Set family req
	std::set<unsigned int> fam_req;
	fam_req.insert(SpatializedAssociated::GetGuid());
	fam_req.insert(Spatialized::GetGuid());   
	SetFamilyRequirements(fam_req);    
}


SpatializedAssociationSystem::~SpatializedAssociationSystem(void)
{
}

void 
SpatializedAssociationSystem::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	std::set<unsigned int>::const_iterator fam_iter = family->begin();
	for (fam_iter; fam_iter!= family->end(); fam_iter++)
	{   			
		std::shared_ptr<IComponent> sp_assoc_comp, spatial_comp;

		gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, SpatializedAssociated::GetGuid(), sp_assoc_comp);
		gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Spatialized::GetGuid(), spatial_comp);

		if (sp_assoc_comp && spatial_comp)
		{
			SpatializedAssociated* assoc = static_cast<SpatializedAssociated*>(sp_assoc_comp.get());

			int associate_id = assoc->AssociatedEntity();			

			Spatialized* master_spatialized = static_cast<Spatialized*>(spatial_comp.get());

			std::shared_ptr<IComponent> tmp_comp;
			Spatialized* slave_spatialized = NULL;
			gApp->EntityMngr()->GetComponentFromEntity(associate_id, Spatialized::GetGuid(), tmp_comp);
			if (!tmp_comp)
				continue;

			slave_spatialized = static_cast<Spatialized*>(tmp_comp.get());			

			if (slave_spatialized)
			{
				slave_spatialized->LocalPosition( master_spatialized->LocalPosition() );
				slave_spatialized->LocalPosition().x += assoc->OffsetX();
				slave_spatialized->LocalPosition().y += assoc->OffsetY();
			}
		}			 
	}	
}
