#include "Debug/Debug.h"
#include "BaseApp.h"

#include "SpriteAnimationSystem/SpriteAnimationLoader.h"
#include "SpatializedAssociated.h"

// The tile entities
#include "Floor.h"
#include "Player.h"
#include "GenericTile.h"
#include "Elevator.h"
#include "HealthTrigger.h"
#include "BackgroundSprite.h"
#include "RandomMovingPointLight.h"

#include "Scene/SceneLoaderWriter.h"
#include "LevelLoader.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;

void 
LevelLoader::LoadLevelFromFile( std::string const& path, std::shared_ptr<SceneObjectManager> const& staticSceneObjManager )
{
	ticpp::Document doc(path);

	// watch out for exceptions
	try
	{		
		doc.LoadFile();
	}
	catch(ticpp::Exception& ex)
	{
		ex.what(); //suppress VS warning
		OutputDebugMsg("Could not load level file: " + path + "\n");
		return;
	}	

	ticpp::Element* level_element = doc.FirstChildElement();
	if (!level_element)
	{
		OutputDebugMsg("Wrong level file format! \n");
		return;
	}

	// Static scene
	ticpp::Iterator<ticpp::Element> properties_iter("CustomProperties");
	for(properties_iter = properties_iter.begin(level_element); properties_iter != properties_iter.end(); properties_iter++)
	{
		ticpp::Iterator<ticpp::Element> property_iter("Property");
		for(property_iter = property_iter.begin(properties_iter->ToElement()); property_iter != property_iter.end(); property_iter++)
		{
			ticpp::Element* static_scene = property_iter.Get();
			std::string name;
			name = property_iter->GetAttribute("Name");
			if ("staticScene" == name)
			{
				std::string type = property_iter->GetAttribute("Type");
				if ("string" != type)
					continue;

				std::string scene_path;
				scene_path = property_iter->FirstChildElement()->GetText();				

				//OutputDebugMsg(scene_path + "\n");

				std::wstring wfile(scene_path.begin(), scene_path.end());
				std::wstring wpath = gResourcesDir + L"\\Platformer\\" + wfile;
				std::string path(wpath.begin(), wpath.end());

				//SceneLoaderWriter scene_loader;
				//std::set<unsigned int> lights;
				//std::map<unsigned int, DirectX::BoundingBox> instances;
				//scene_loader.LoadSceneFromFile(path, staticSceneObjManager, lights, instances);

			}
		}
	}

	// Layers
	ticpp::Iterator<ticpp::Element> layers_iter("Layers");
	for(layers_iter = layers_iter.begin(level_element); layers_iter != layers_iter.end(); layers_iter++)
	{
		ticpp::Iterator<ticpp::Element> layer_iter("Layer");
		for(layer_iter = layer_iter.begin(layers_iter->ToElement()); layer_iter != layer_iter.end(); layer_iter++)
		{
			ticpp::Element* layer_element = layer_iter.Get();
			std::string name;
			name = layer_element->GetAttribute("Name");
			if ("main" == name)
				LoadMainLayer(layer_element);
			else if ("lighting" == name)
				;// LoadLightingLayer(layer_element);
			else // then it's a background offset layer
				LoadOffsetLayer(layer_element);
		}
	}

}

void 
LevelLoader::LoadMainLayer( ticpp::Element* parent )
{
	ticpp::Iterator<ticpp::Element> items_iter("Items");
	for(items_iter = items_iter.begin(parent); items_iter != items_iter.end(); items_iter++)
	{
		ticpp::Iterator<ticpp::Element> item_iter("Item");
		for(item_iter = item_iter.begin(items_iter->ToElement()); item_iter != item_iter.end(); item_iter++)
		{
			ticpp::Element* item_element = item_iter.Get();
			if (item_element)
			{
				std::string name;
				name = item_element->GetAttribute("Name");
				// OutputDebugMsg(name + "\n");
				ItemInfo item_info = GetItemInfo(item_element);

				// Insure the item has a "type" property
				auto type_iter = item_info.customProperties.find("type");
				if (type_iter == item_info.customProperties.end())
				{
					OutputDebugMsg("Item '" + name + "' is missing a 'type' property.\n");
					continue;
				}

				// Depending on the type, call the appropriate function to insert the specific entity
				if ("floor" == type_iter->second)
					InsertFloorEntity(item_info);
				else if ("player" == type_iter->second)
					InsertPlayerEntity(item_info);
				else if ("test" == type_iter->second)
					InsertTestEntity(item_info);
				else if ("elevator" == type_iter->second)
					InsertElevatorEntity(item_info);
				else if ("playerHealthTrigger" == type_iter->second)
					InsertPlayerHealthTriggerEntity(item_info);
			}
		}
	}
}

LevelLoader::ItemInfo 
LevelLoader::GetItemInfo( ticpp::Element* item )
{
	ItemInfo info;
	info.position = info.size = DirectX::XMFLOAT2(0.f, 0.f);

	// Dimensions
	ticpp::Element* width_element = item->FirstChildElement("Width", false);
	if (width_element)
	{		
		std::stringstream steam(width_element->GetText());
		steam >> info.size.x; 

		//OutputDebugMsg("Width = " + to_string(info.size.x) + "\n");
	}
	ticpp::Element* height_element = item->FirstChildElement("Height", false);
	if (height_element)
	{		
		std::stringstream steam(height_element->GetText());
		steam >> info.size.y; 

		//OutputDebugMsg("Height = " + to_string(info.size.y) + "\n");
	}

	// Position
	ticpp::Element* position_element = item->FirstChildElement("Position", false);
	if (position_element)
	{
		ticpp::Element* x_element = position_element->FirstChildElement("X", false);
		ticpp::Element* y_element = position_element->FirstChildElement("Y", false);

		if (x_element && y_element)
		{
			std::stringstream stream_x(x_element->GetText());
			stream_x >> info.position.x; 

			std::stringstream stream_y(y_element->GetText());
			stream_y >> info.position.y; 
			
			info.position.y = -1.f * (info.position.y + info.size.y);
			info.position.x -= gApp->WinWidth() / 2.f;
			info.position.y -= gApp->WinHeight() / 2.f;

			//OutputDebugMsg("Pos = " + to_string(info.position.x) + ", " + to_string(info.position.y) + "\n");
		}
	}

	// Properties
	ticpp::Element* properties_element = item->FirstChildElement("CustomProperties", false);
	if (properties_element)
	{
		ticpp::Iterator<ticpp::Element> property_iter("Property");
		for(property_iter = property_iter.begin(properties_element); property_iter != property_iter.end(); property_iter++)
		{
			ticpp::Element* property_element = property_iter.Get();
			if (property_element)
			{
				// We insure that it was inputed as a "string" type within the level editor... anything else is ignored
				// This way all casting is done from within code instead of the level editor
				std::string type;
				type = property_element->GetAttribute("Type");
				if ("string" != type)
					continue;

				std::string name, value;
				name = property_element->GetAttribute("Name");
				value = property_element->FirstChildElement()->GetText();
				
				//OutputDebugMsg(name + " = " + value + "\n");

				info.customProperties[name] = value;
			}
		}
	}
	

	return info;
}

void 
LevelLoader::InsertFloorEntity( ItemInfo& info )
{
	std::shared_ptr<TileEntity> tile(new Floor);

	auto iter = info.customProperties.find("staticSprite");
	if ( iter != info.customProperties.end() )
	{
		static_cast<Floor*>(tile.get())->SetSprite( CreateSprite(tile->EntityId(), iter->second) );
	}

	mLevel->InsertTileEntity(info.position, info.size, tile);
}

void 
LevelLoader::InsertPlayerEntity( ItemInfo& info )
{
	std::shared_ptr<TileEntity> tile(new Player(gApp->EntityMngr(), mEventMngr));
	mLevel->InsertTileEntity(info.position, info.size, tile);
}

void 
LevelLoader::InsertTestEntity( ItemInfo& info )
{	
	std::shared_ptr<TileEntity> tile(new GenericTile());

	auto iter = info.customProperties.end();

	iter = info.customProperties.find("elasticity");
	if ( iter != info.customProperties.end() )
	{
		float elasticity = 0.f;
		std::stringstream value(iter->second);
		value >> elasticity; 
		static_cast<GenericTile*>(tile.get())->ElasticityComponent()->ElasticityValue(elasticity);
	}

	iter = info.customProperties.find("friction");
	if ( iter != info.customProperties.end() )
	{
		float friction = 0.f;
		std::stringstream value(iter->second);
		value >> friction; 
		static_cast<GenericTile*>(tile.get())->FrictionComponent()->FrictionValue(friction);
	}

	iter = info.customProperties.find("gravity");
	if ( iter != info.customProperties.end() )
	{
		float gravity = 0.f;
		std::stringstream value(iter->second);
		value >> gravity; 
		static_cast<GenericTile*>(tile.get())->AccelerationComponent()->AccelerationVector(DirectX::XMFLOAT3(0.f, gravity, 0.f));
	}

	iter = info.customProperties.find("mass");
	if ( iter != info.customProperties.end() )
	{
		float mass = 0.f;
		std::stringstream value(iter->second);
		value >> mass; 
		static_cast<GenericTile*>(tile.get())->MassComponent()->MassFactor(mass);
	}

	iter = info.customProperties.find("staticSprite");
	if ( iter != info.customProperties.end() )
	{
		static_cast<GenericTile*>(tile.get())->SetSprite( CreateSprite(tile->EntityId(), iter->second) );
	}

	mLevel->InsertTileEntity(info.position, info.size, tile);
}

void 
LevelLoader::InsertElevatorEntity( ItemInfo& info )
{
	float wait = 0.f;
	float height = 0.f;
	float vel = 0.f;

	auto iter = info.customProperties.end();

	iter = info.customProperties.find("waitTime");
	if ( iter != info.customProperties.end() )
	{	
		std::stringstream value(iter->second);
		value >> wait; 		
	}

	iter = info.customProperties.find("heightTravel");
	if ( iter != info.customProperties.end() )
	{	
		std::stringstream value(iter->second);
		value >> height; 		
	}

	iter = info.customProperties.find("velocity");
	if ( iter != info.customProperties.end() )
	{	
		std::stringstream value(iter->second);
		value >> vel; 		
	}

	std::shared_ptr<TileEntity> tile(new Elevator(gApp->EntityMngr(), wait, height, vel));
	mLevel->InsertTileEntity(info.position, info.size, tile);
}

void 
LevelLoader::InsertPlayerHealthTriggerEntity( ItemInfo& info )
{
	int health_modifier = 0;
	double time_delta = 0;

	auto iter = info.customProperties.end();

	iter = info.customProperties.find("healthModifier");
	if ( iter != info.customProperties.end() )
	{	
		std::stringstream value(iter->second);
		value >> health_modifier; 		
	}

	iter = info.customProperties.find("timeDelta");
	if ( iter != info.customProperties.end() )
	{	
		std::stringstream value(iter->second);
		value >> time_delta; 		
	}

	std::shared_ptr<TileEntity> tile(new PlayerHealthTrigger(health_modifier, time_delta));
	mLevel->InsertTileEntity(info.position, info.size, tile);
}

std::shared_ptr<SpriteEntity> 
LevelLoader::CreateSprite( unsigned int const tileEntityId, std::string const& fileName )
{
	std::wstring wfile(fileName.begin(), fileName.end());
	std::wstring wpath = gResourcesDir + L"\\Platformer\\" + wfile;
	std::string path(wpath.begin(), wpath.end());

	std::shared_ptr<SpriteEntity> sprite( new SpriteEntity );
	
	sprite->LoadAnimations( LoadAnimationsInfoFromFile(path) );
	sprite->AnimateSprite("static");

	mStaticSprites[path].push_back(sprite->SpriteEntityId());

	std::shared_ptr<SpatializedAssociated> sprite_spatial_associated = std::make_shared<SpatializedAssociated>(sprite->SpriteEntityId());
	DirectX::XMFLOAT2 sprite_offset = sprite->CurrentAnimationOffsets();
	sprite_spatial_associated->OffsetX() = sprite_offset.x;
	sprite_spatial_associated->OffsetY() = sprite_offset.y;
	gApp->EntityMngr()->AddComponentToEntity(tileEntityId, sprite_spatial_associated);

	return sprite;
}

void 
LevelLoader::LoadOffsetLayer( ticpp::Element* parent )
{
	ticpp::Iterator<ticpp::Element> items_iter("Items");
	for(items_iter = items_iter.begin(parent); items_iter != items_iter.end(); items_iter++)
	{
		ticpp::Iterator<ticpp::Element> item_iter("Item");
		for(item_iter = item_iter.begin(items_iter->ToElement()); item_iter != item_iter.end(); item_iter++)
		{
			ticpp::Element* item_element = item_iter.Get();
			if (item_element)
			{
				std::string name;
				name = item_element->GetAttribute("Name");
				// OutputDebugMsg(name + "\n");
				ItemInfo item_info = GetItemInfo(item_element);

				// Insure the item has a "depth" property
				auto type_iter = item_info.customProperties.find("depth");
				if (type_iter == item_info.customProperties.end())
				{
					OutputDebugMsg("Item '" + name + "' is missing a 'depth' property.\n");
					continue;
				}

				// Insure the item has a "staticSprite" property
				type_iter = item_info.customProperties.find("staticSprite");
				if (type_iter == item_info.customProperties.end())
				{
					OutputDebugMsg("Item '" + name + "' is missing a 'staticSprite' property.\n");
					continue;
				}

				// Depending on the type, call the appropriate function to insert the specific entity
				InsertOffsetLayer(item_info);
			}
		}
	}
}

void 
LevelLoader::InsertOffsetLayer( ItemInfo& info )
{
	float depth = 1.f;

	auto iter = info.customProperties.find("depth");
	if ( iter != info.customProperties.end() )
	{		
		std::stringstream value(iter->second);
		value >> depth; 		
	}	

	std::shared_ptr<TileEntity> tile(new BackgroundSprite(depth));

	iter = info.customProperties.find("staticSprite");
	if ( iter != info.customProperties.end() )
	{
		static_cast<BackgroundSprite*>(tile.get())->SetSprite( CreateSprite(tile->EntityId(), iter->second) );
	}

	mLevel->InsertTileEntity(info.position, info.size, tile);
}

void 
LevelLoader::LoadLightingLayer( ticpp::Element* parent )
{
	ticpp::Iterator<ticpp::Element> items_iter("Items");
	for(items_iter = items_iter.begin(parent); items_iter != items_iter.end(); items_iter++)
	{
		ticpp::Iterator<ticpp::Element> item_iter("Item");
		for(item_iter = item_iter.begin(items_iter->ToElement()); item_iter != item_iter.end(); item_iter++)
		{
			ticpp::Element* item_element = item_iter.Get();
			if (item_element)
			{
				std::string name;
				name = item_element->GetAttribute("Name");
				// OutputDebugMsg(name + "\n");
				ItemInfo item_info = GetItemInfo(item_element);

				// Insure the item has a "type" property
				auto type_iter = item_info.customProperties.find("type");
				if (type_iter == item_info.customProperties.end())
				{
					OutputDebugMsg("Item '" + name + "' is missing a 'depth' property.\n");
					continue;
				}
				
				// Depending on the type, call the appropriate function to insert the specific entity
				if ("randomPoint" == type_iter->second)
					InsertRandomPointLight(item_info);				
			}
		}
	}
}

void 
LevelLoader::InsertRandomPointLight( ItemInfo& info )
{
	float radius_sqr = 0.f;

	auto iter = info.customProperties.find("radiusSqr");
	if ( iter != info.customProperties.end() )
	{		
		std::stringstream value(iter->second);
		value >> radius_sqr; 		
	}	

	std::shared_ptr<TileEntity> tile(new RandomMovingPointLight(radius_sqr));
	
	mLevel->InsertTileEntity(info.position, info.size, tile);
}
