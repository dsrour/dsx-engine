#pragma once

#include "SpriteAnimationSystem/SpriteEntity.h"
#include "2dTiledEngine/TileEntity.h"
#include "System.h"

class BackgroundSprite : public TileEntity
{
public:
	BackgroundSprite(float const depth);
	~BackgroundSprite(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PostCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PreCollision( Level* const level, CollisionInfo const& collisionInfo );

	void
	SetSprite(std::shared_ptr<SpriteEntity> sprite) { mSprite = sprite; }
		
private:
	std::shared_ptr<SpriteEntity> mSprite;	
	float mDepth;
};

