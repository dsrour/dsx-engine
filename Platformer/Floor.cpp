#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Utils/MathUtils.h"
#include "Floor.h"

extern BaseApp* gApp;

Floor::Floor(void)
{
}


Floor::~Floor(void)
{
}

/*virtual*/ void 
Floor::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{	
	mWidth = size.x;
	mHeight = size.y;


	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mWidth/2.f), (mHeight/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mWidth/2.f), (mHeight/2.f), 0.001f);
 	DirectX::BoundingBox aabb(center, extents);
 	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
// 	std::vector<DirectX::XMFLOAT3> verts;
// 	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
// 	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
// 	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
// 	Material mat;
// 	mat.diffuse = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
// 	mRenderableComponent->MaterialProperties(mat);
// 	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);

	// Make the entity an obstacle so that other objects can't pass through it
	mTiledEntityComp->IsObstacle(true);
}

/*virtual*/ void 
Floor::OnLevelRemoval( Level* const level )
{

}

/*virtual*/ void 
Floor::OnCollision( Level* const level, CollisionInfo const& collisionInfo )
{	
	
}

void Floor::SetSprite( std::shared_ptr<SpriteEntity> sprite )
{
	mSprite = sprite;
}
