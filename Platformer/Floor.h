#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/TileEntity.h"
#include "SpriteAnimationSystem/SpriteEntity.h"

class Floor : public TileEntity
{
public:
	Floor(void);
	~Floor(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);

	void
	SetSprite(std::shared_ptr<SpriteEntity> sprite);

private:
	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<SpriteEntity> mSprite;

	float mWidth, mHeight;
};

