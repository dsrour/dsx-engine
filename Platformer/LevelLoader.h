#pragma once

#define TIXML_USE_TICPP
#include <map>
#include <vector>
#include <string>
#include <DirectXMath.h>
#include "ticpp/ticpp.h"
#include "EventManager/EventManager.h"
#include "2dTiledEngine/Level.h"

class SpriteEntity;
class SceneObjectManager;

class LevelLoader
{
public:
	LevelLoader(Level* const level, std::shared_ptr<EventManager> eventManager) { mLevel = level; mEventMngr = eventManager; }
	~LevelLoader(void) {}

	void
	LoadLevelFromFile(std::string const& path, std::shared_ptr<SceneObjectManager> const& staticSceneObjManager);
	
private:
	struct ItemInfo
	{
		DirectX::XMFLOAT2 size;
		DirectX::XMFLOAT2 position;
		std::map<std::string, std::string> customProperties;
	};

	void 
	LoadMainLayer( ticpp::Element* parent );

	ItemInfo 
	GetItemInfo( ticpp::Element* item );

	void
	InsertFloorEntity(ItemInfo& info);

	void
	InsertPlayerEntity(ItemInfo& info);

	void
	InsertTestEntity(ItemInfo& info);

	void
	InsertElevatorEntity(ItemInfo& info);

	void
	InsertPlayerHealthTriggerEntity(ItemInfo& info);

	std::shared_ptr<SpriteEntity>
	CreateSprite(unsigned int const tileEntityId, std::string const& fileName);

	void 
	LoadOffsetLayer( ticpp::Element* parent );

	void 
	LoadLightingLayer( ticpp::Element* parent );

	void 
	InsertOffsetLayer( ItemInfo& info );

	void
	InsertRandomPointLight(ItemInfo& info);

	Level* mLevel;

	std::shared_ptr<EventManager> mEventMngr;

	std::map< std::string, std::vector<unsigned int> > mStaticSprites; // Used to instantialize duplicates
};

