#pragma once

#include "BaseApp.h"
#include "EventManager/EventManager.h"
#include "CountDownTimer.h"
#include "SpatializedAssociationSystem.h"
#include "Scene/SceneObjectManager.h"

class Level;

class Platformer : public BaseApp
{
public:
	Platformer(void);
	~Platformer(void);

	bool const
	OnLoadApp(void);

	void
	OnPreUpdate(void);

	void
	OnPostUpdate(void);

	void
	OnUnloadApp(void);		

private:
	std::shared_ptr<EventManager> mEventMngr;
	std::shared_ptr<Level> mLevel;
	std::shared_ptr<CountDownTimer> mCountDownTimer; int mCountDownTimerSysId;
	std::shared_ptr<SpatializedAssociationSystem> mSpatializedAssociationSystem; int mSpatializedAssociationSysId;
	std::shared_ptr<SceneObjectManager> mStaticSceneObjManager;
	
};

