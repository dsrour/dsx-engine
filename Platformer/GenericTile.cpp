#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Utils/MathUtils.h"
#include "CommonCollisionResolutions.h"
#include "GenericTile.h"

extern BaseApp* gApp;

GenericTile::GenericTile()
{
	mElasticity = std::make_shared<Elasticity>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mElasticity);

	mFriction = std::make_shared<Friction>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mFriction);

	mVelocity = std::make_shared<Velocity>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocity);

	mAccel = std::make_shared<Acceleration>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mAccel);

	mMass = std::make_shared<Mass>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mMass);

	// Make it an obstacle
	mTiledEntityComp->IsObstacle(true);
}


GenericTile::~GenericTile(void)
{

}

void 
GenericTile::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{	
	mWidth = size.x;
	mHeight = size.y;


	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mWidth/2.f), (mHeight/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mWidth/2.f), (mHeight/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(0.f, 24.f, 8.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);
}



void 
GenericTile::OnLevelRemoval( Level* const level )
{

}


/*virtual*/ void
GenericTile::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	MovingPlatformResolution(EntityId(), level, collisionInfo, mSpatializedComp->LocalPosition());
}

void 
GenericTile::SetSprite( std::shared_ptr<SpriteEntity> sprite )
{
	mSprite = sprite;	
}
