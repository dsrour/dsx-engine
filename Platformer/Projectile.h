#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/TileEntity.h"
#include "System.h"

class Projectile : public TileEntity, public System, public std::enable_shared_from_this<Projectile>
{
public:
	Projectile(std::shared_ptr<EntityManager> entityManager, float size[],  unsigned int owner);
	~Projectile(void);

	void
	Activate(Level* const level, float startPos[], float velocity[], float maxTravelDist, unsigned int damageToInflict);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PreCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	PostCollision(Level* const level, CollisionInfo const& collisionInfo);

	virtual void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);
		
private:	
	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<Velocity> mVelocityComp;

	DirectX::XMFLOAT3 mOwnerVel;

	unsigned int mSysId;

	float mSize[2];
	float mStartPos[2];	
	float mVelocity[2];
	float mMaxTravelDist;
	unsigned int mProjectileOwner;
	bool mActive;
	unsigned int mDamageToInflict;

	Level* mLevelRef; // hopefully this stays the same always?
};

