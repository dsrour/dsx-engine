#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/Level.h"
#include "RandomMovingPointLight.h"

extern BaseApp* gApp;

std::mt19937 RandomMovingPointLight::mRdmEng;
uint32_t RandomMovingPointLight::mSeedVal = GuidGenerator::GenerateGuid();	

RandomMovingPointLight::RandomMovingPointLight(float const radiusSqr)
{
	mLight = std::make_shared<LightEmitting>();
	PointLight pnt_light;
	pnt_light.range = 100;
	pnt_light.diffuse  = RandomColor();
	pnt_light.ambient  = RandomColor();
	pnt_light.specular = RandomColor();
	mLight->SetPointLight(pnt_light);

	mRadiusSqr = radiusSqr;	
}

RandomMovingPointLight::~RandomMovingPointLight( void )
{

}

void 
RandomMovingPointLight::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	float tile_size = (float)level->TileSize();

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (tile_size/2.f), (tile_size/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (tile_size/2.f), (tile_size/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Velocity
	mVelocity = std::make_shared<Velocity>();
	mVelocity->VelocityVector(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocity);

	// Accel
	mAccelComp = std::make_shared<Acceleration>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mAccelComp);
	mAccelComp->AccelerationVector(DirectX::XMFLOAT3(0.f, 0.f, 0.f));

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( tile_size, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( tile_size, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( tile_size, tile_size, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( tile_size, tile_size, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, tile_size, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, tile_size, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(0.f, 1.f, 0.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);


	// Position light
	world_pos.x += (tile_size/2.f);
	world_pos.y += (tile_size/2.f);
	static_cast<PointLight*>(mLight->GetLight())->position = world_pos;
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mLight);

	mOriginalPos = world_pos;

	mTiledEntityComp->IsElasticRigidBody(false);
	mTiledEntityComp->IsElasticRigidBody(false);
}

void 
RandomMovingPointLight::OnLevelRemoval( Level* const level )
{
	gApp->EntityMngr()->RemoveComponentFromEntity(EntityId(), LightEmitting::GetGuid());
}

void 
RandomMovingPointLight::OnCollision( Level* const level, CollisionInfo const& collisionInfo )
{

}

DirectX::XMFLOAT4 
RandomMovingPointLight::RandomColor( void )
{
	std::uniform_int_distribution<unsigned int> rndm_distr(1, 100); 

	float const min = 1.f;
	float const max = 1.f;
	
	return DirectX::XMFLOAT4( 1.f / rndm_distr(mRdmEng), 
							  1.f / rndm_distr(mRdmEng), 
							  1.f / rndm_distr(mRdmEng),
							  1.f );
}

DirectX::XMFLOAT3
RandomMovingPointLight::RandomAcceleration( void )
{
	std::uniform_int_distribution<unsigned int> rndm_distr(1, 10); 

	float const min = 1.f;
	float const max = 1.f;

	return DirectX::XMFLOAT3( 1.f / rndm_distr(mRdmEng), 
		                      1.f / rndm_distr(mRdmEng), 
							  1.f / rndm_distr(mRdmEng) );
}