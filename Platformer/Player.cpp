#include <algorithm>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "ofpennereasing-master/PennerEasing/Expo.h"
#include "Utils/MathUtils.h"
#include "CommonCollisionResolutions.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Mass.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "SpriteAnimationSystem/SpriteAnimationLoader.h"
#include "EventMetadata.h"
#include "Player.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;

TwBar* Player::mPlayerTwBar = NULL;

// Walking mechanics ///
float Player::mAccel, Player::mDecel; 
float Player::mRun, Player::mSprint;      
float Player::mGravity;
float Player::mJumpEndVel, Player::mJumpTime, Player::mJumpHeight;    
///////////////////////

Player::Player(std::shared_ptr<EntityManager> entityManager, std::shared_ptr<EventManager> eventManager) : System(entityManager)
{
	mEventManager = eventManager;
	mState[0] = mState[1] = IDLE;	
	mInputDir[0] = 0; mInputDir[1] = 0; 

	mProjectileCount = 0;

	mAccel = 5.f;
	mDecel = 5.f;
	mRun = 6.4f;
	mSprint = 18.5f;
	mGravity = -2.7f;
	mJumpEndVel = 24.0f;
	mJumpTime = 0.22f;
	mJumpHeight = 102.3f;

	mIsOnFloor = true;
	mIsStunned = false;

	float projectile_size[2] = {15.f, 15.f};

	for (unsigned int i = 0; i < PLAYER_NUM_PROJECTILES; i++)
		mProjectiles[i] = std::make_shared<Projectile>(gApp->EntityMngr(), projectile_size, EntityId());

	ClearCollidingHistory();
}


Player::~Player(void)
{

}

void 
Player::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{
	mLevel = level;
	
 	std::shared_ptr<System> me(shared_from_this());
	mSysId = gApp->SystemMngr()->AddSystem(me);
	double target = 1.0 / 60.0;
	gApp->SystemSchdlr()->StopSystem(mSysId);
	gApp->SystemSchdlr()->RunSystemEveryDelta(mSysId, target);

	mPlayerTwBar = TwNewBar("Player Mechanics");
	TwDefine(" 'Player Mechanics' iconified=true ");  

	TwAddVarRW(mPlayerTwBar, "accel", TW_TYPE_FLOAT, &mAccel, " label='Acceleration' step=0.05 min=0.0");
	TwAddVarRW(mPlayerTwBar, "decel", TW_TYPE_FLOAT, &mDecel, " label='Deceleration' step=0.05 min=0.0");
	TwAddVarRW(mPlayerTwBar, "runspeed", TW_TYPE_FLOAT, &mRun, " label='Run Velocity' step=0.05 min=0.0");
	TwAddVarRW(mPlayerTwBar, "sprintspeed", TW_TYPE_FLOAT, &mSprint, " label='Sprint Velocity' step=0.05 min=0.0");	
	TwAddVarRW(mPlayerTwBar, "gravity", TW_TYPE_FLOAT, &mGravity, " label='Gravity' step=0.1");
	TwAddVarRW(mPlayerTwBar, "jumpendvel", TW_TYPE_FLOAT, &mJumpEndVel, " label='Jump End Velocity' step=0.1");
	TwAddVarRW(mPlayerTwBar, "jumptime", TW_TYPE_FLOAT, &mJumpTime, " label='Jump Time' step=0.05 min=0.05");
	TwAddVarRW(mPlayerTwBar, "jumpheight", TW_TYPE_FLOAT, &mJumpHeight, " label='Jump Height' step=0.05 min=0.0");
	

	// Create the actual sprite to animate the player
	std::wstring wpath = gResourcesDir + L"Platformer\\player_anims.xml";
	std::string path(wpath.begin(), wpath.end());
	mSprite = std::make_shared<SpriteEntity>();
	mSprite->LoadAnimations(LoadAnimationsInfoFromFile(path));
	mSprite->AnimateSprite("idle");
	mCurrentAnim = "idle";
	mSpriteSpatialAssociated = std::make_shared<SpatializedAssociated>(mSprite->EntityId());
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mSpriteSpatialAssociated);

	float width  = mSprite->CurrentAnimationHitBoxSize().x;
	float height = mSprite->CurrentAnimationHitBoxSize().y;
	
	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);		
	mSpatializedComp->LocalScale().x = width;
	mSpatializedComp->LocalScale().y = height;

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (width/2.f), (height/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (width/2.f), (height/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the outline renderable for debugging collisions
	DirectX::XMFLOAT2 start( 0.f, 0.f );	
	DirectX::XMFLOAT2 end( 1.f, 1.f );	
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3(start.x, start.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(start.x, end.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(start.x, end.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(end.x, end.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(end.x, end.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(end.x, start.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(end.x, start.y, 0.f) );
	verts.push_back( DirectX::XMFLOAT3(start.x, start.y, 0.f) );
	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);


	// Init inputs
	mKeyboardInputs = std::make_shared<PlayerKeyboardInputs>(gApp->InputDeviceMngr(), this);
	gApp->InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardInputs);
	mMouseInputs = std::make_shared<PlayerMouseInputs>(gApp->InputDeviceMngr(), this);
	gApp->InputDeviceMngr()->Mouse()->AddMouseActions(mMouseInputs);

	// Velocity
	mVelocity = std::make_shared<Velocity>();
	mVelocity->VelocityVector(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocity);

	// Accel
	mAccelComp = std::make_shared<Acceleration>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mAccelComp);
	mAccelComp->AccelerationVector(DirectX::XMFLOAT3(0.f, mGravity, 0.f));

	// Friction
	mFriction = std::make_shared<Friction>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mFriction);

	// Elasticity
	mElasticity = std::make_shared<Elasticity>(0.1f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mElasticity);

	// Moving Platform component
	mOnMovingPlatform = std::make_shared<OnMovingPlatform>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mOnMovingPlatform);

	// Health 
	mHealth = std::make_shared<Health>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mHealth);
	unsigned int const START_HEALTH = 100;
	mHealth->CurrentHealth() = START_HEALTH;
	mLastHealth = START_HEALTH;
	PlayerHealthMetadata meta;
	meta.playerHealth = START_HEALTH;
	mEventManager->BroadcastEvent(gPlayerHealthEventName, &meta);

	// Damageable
	mDamageable = std::make_shared<Damageable>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mDamageable);

	// Mass
	mMass = std::make_shared<Mass>(50.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mMass);

	// Other vars
	mTiledEntityComp->IsObstacle(true);
	mLastNormal = DirectX::XMFLOAT2(0.f, 0.f);
	mLevelSize = level->LevelSize();
	mTileSize = level->TileSize();
}

void 
Player::OnLevelRemoval( Level* const level )
{
	if (gApp)
	{
		// Stop and remove systems
		gApp->SystemSchdlr()->StopSystem(mSysId);
		gApp->SystemMngr()->RemoveSystem(mSysId);

		// Remove inputs
		gApp->InputDeviceMngr()->Keyboard()->RemoveKeyboardActions();	
		gApp->InputDeviceMngr()->Mouse()->RemoveMouseActions();
	}	
}

void 
Player::SetVelocity( DirectX::XMFLOAT3 const& velocity )
{
	if (mVelocity)
		mVelocity->VelocityVector(velocity);
}

void 
Player::UpdateInputs(int const(& dir)[2], bool const sprint)
{	
	State state[] = {mState[0], mState[1]};

	// X-Dir Set State
	if (0 == mInputDir[0] && 0 != dir[0]) 
	{
		if (!sprint)
		{
			state[0] = RUN;					
			//OutputDebugMsg("IDLE -> WALK\n");
		}
		else
		{
			state[0] = SPRINT;
			//OutputDebugMsg("IDLE -> RUN\n");
		}
	}
	else if (0 != mInputDir[0]) // There was movement in the x dir 
	{
		if ( 0 == dir[0] )
		{
			// but now there is none
			state[0] = IDLE;
			//OutputDebugMsg("WALK/RUN -> IDLE\n");
		}
		else
		{
			if (!sprint)
			{
				state[0] = RUN;			
				//OutputDebugMsg("IDLE -> WALK\n");
			}
			else
			{
				state[0] = SPRINT;
				//OutputDebugMsg("IDLE -> RUN\n");
			}
		}		
	}

	// Y-Dir
	if (1 == dir[1] && mInputDir[1] != 1 && mIsOnFloor)
	{
		mJumpStartTime = gApp->Timer().ElapsedTimeSecs();
		state[1] = JUMP;
		mAccelComp->AccelerationVector().y = 0.f;
		mIsOnFloor = false;		
		mJumpOriginY = mSpatializedComp->LocalPosition().y;		
	}	
	else if (1 == mInputDir[1] && 0 == dir[1] && mVelocity->VelocityVector().y > 0.f)
	{
		mVelocity->VelocityVector().y = 0.f;
		mAccelComp->AccelerationVector().y = mGravity;
		state[1] = IDLE;
	}


	if (dir[0] || dir[1])
	{
		mOnMovingPlatform->MovingPlatformId() = -1;
		mOnMovingPlatform->Offset() = DirectX::XMFLOAT2(0.f, 0.f);
	}	
	else
	{
		mAccelComp->AccelerationVector().x = 0.f;
		mVelocity->VelocityVector().x = 0.f;
	}	

	mInputDir[0] = dir[0];
	mInputDir[1] = dir[1];
	mState[0] = state[0];
	mState[1] = state[1];
}

void 
Player::RunImplementation( std::set<unsigned int> const* family, double const currentTime )
{	
	if (!mIsStunned)
	{
		// Y
		if (mState[1] == JUMP)
		{
			if (mSpatializedComp->LocalPosition().y - mJumpOriginY >= mJumpHeight)		
				mState[1] = IDLE;
			else
			{
				float time_elapsed = (float)(gApp->Timer().ElapsedTimeSecs() - mJumpStartTime);
				mVelocity->VelocityVector().y = Expo::easeOut(time_elapsed, 0.f, mJumpEndVel, (float)mJumpTime);
				//OutputDebugMsg("Jump Current Velocity = " + to_string(mVelocity->VelocityVector().y) + "\n");
			}
		}
		else
			mAccelComp->AccelerationVector().y = mGravity;


		// X
		if (mState[0] == RUN && std::fabsf(mVelocity->VelocityVector().x) > mRun)
		{
			mVelocity->VelocityVector().x = mRun * mInputDir[0];
		}
		else if (mState[0] == SPRINT && std::fabsf(mVelocity->VelocityVector().x) > mSprint)
		{
			mVelocity->VelocityVector().x = mSprint * mInputDir[0];			
		}
		else if (mState[0] == IDLE)
		{	
			if (mVelocity->VelocityVector().x > 0.f)
				mInputDir[0] = 1;
			else if (mVelocity->VelocityVector().x < 0.f)
				mInputDir[0] = -1;

			// If there's velocity but no acceleration... Time to decel
			if ( IsEqual(mAccelComp->AccelerationVector().x, 0.f) && !IsEqual(mVelocity->VelocityVector().x, 0.f) )			 
			{
				mAccelComp->AccelerationVector().x = mDecel * -mInputDir[0];				
			}
			else if ( mAccelComp->AccelerationVector().x < 0.f && mVelocity->VelocityVector().x < 0.f ||
					  mAccelComp->AccelerationVector().x > 0.f && mVelocity->VelocityVector().x > 0.f )
			{
				// Are we done decelerating?
				mVelocity->VelocityVector().x = 0.f;	
				mAccelComp->AccelerationVector().x = 0.f;
			}		
		}	
		else
			mAccelComp->AccelerationVector().x = mAccel * mInputDir[0];
	}
	else if (mIsStunned && gApp->Timer().ElapsedTimeSecs() >= mStunEndTime)
	{
		mIsStunned = false;
		mVelocity->VelocityVector() = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	}
			
	CheckForDamage();

	SetSpriteAnimation();

	CenterCamera();

	if (mOnMovingPlatform->MovingPlatformId() >= 0)
		if (!SearchCollidingHistory(mOnMovingPlatform->MovingPlatformId()))
		{
			mOnMovingPlatform->MovingPlatformId() = -1;
			mOnMovingPlatform->Offset() = DirectX::XMFLOAT2(0.f, 0.f);
		}
	ClearCollidingHistory();

// 	OutputDebugMsg(to_string(mVelocity->VelocityVector().x) + ", " +
// 				   to_string(mVelocity->VelocityVector().y) + "\n");
}

/*virtual*/ void
Player::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{		
//  	OutputDebugMsg(to_string(collisionInfo.normal.x)+", " + to_string(collisionInfo.normal.y));
//  	OutputDebugMsg("Player" + to_string(collisionInfo.penetration) + "\n");

	DirectX::XMFLOAT2 my_normal = collisionInfo.normal;
	unsigned int other_id = collisionInfo.collidingEntityId;
	if (other_id == EntityId())
	{
		other_id = collisionInfo.movingEntityId;
		my_normal.x *= -1.f;
		my_normal.y *= -1.f;
	}

	AddToCollidingHistory(other_id);

	if (collisionInfo.movingEntityId != EntityId() || mIsStunned)
		return;	
	
	TileEntity* other_entity = level->Entity(other_id);

	// Check if hitting floor
	if (my_normal.y == 1 && other_entity->IsObstacle()) 
	{
		mIsOnFloor = true;	
	}
	else if (my_normal.y == -1 && other_entity->IsObstacle()
		     && mState[1] == JUMP) 
	{
		float const HEAD_BUTT_VEL = 18.f;

		// If colliding entity has velocity give it an impulse as if making it bounce of top of player
		Velocity* vel = VelocityComponent(other_id);
		if (vel)
			vel->VelocityVector().y += HEAD_BUTT_VEL;
		else
			mState[1] = IDLE;

		//mIsOnFloor = false;	
	}

	if (my_normal.x && other_entity->IsObstacle())
 		mOnMovingPlatform->MovingPlatformId() = -1;


	mLastNormal = my_normal;

// 	OutputDebugMsg(to_string(mVelocity->VelocityVector().x) + ", " +
// 				   to_string(mVelocity->VelocityVector().y) + "\n");
}

void 
Player::CenterCamera(void)
{
	DirectX::XMFLOAT4 cam_pos = gApp->Renderer()->CurrentCamera()->Position();
	cam_pos.x = (std::max)(0.f, mSpatializedComp->LocalPosition().x);
	cam_pos.y = (std::max)(0.f, mSpatializedComp->LocalPosition().y);

	float max_x = ((mLevelSize.first+1) * mTileSize) - gApp->WinWidth() + 1.0f;
	float max_y = ((mLevelSize.second+1) * mTileSize) - gApp->WinHeight() + 1.0f;
	
 	cam_pos.x = (std::min)(cam_pos.x, max_x);
 	cam_pos.y = (std::min)(cam_pos.y, max_y);

	static_cast<FpCamera*>(gApp->Renderer()->CurrentCamera().get())->Position(cam_pos);
}

void 
Player::CheckForDamage( void )
{
	if (!mIsStunned && mDamageable->InflictedDamage() > 0)
	{
		mHealth->CurrentHealth() -= mDamageable->InflictedDamage();
		mDamageable->InflictedDamage() = 0;
		mIsStunned = true;

		// Stunned... move towards the normal of the hit
		float STUNN_VELOCITY = 33.f;
		double STUNN_TIME_DELTA = 0.05f;
		mStunEndTime = gApp->Timer().ElapsedTimeSecs() + STUNN_TIME_DELTA;
		mVelocity->VelocityVector().x = mDamageable->HitNormal()[0] *  STUNN_VELOCITY; 
		mVelocity->VelocityVector().y = mDamageable->HitNormal()[1] *  STUNN_VELOCITY; 
		mVelocity->VelocityVector().z = 0.f;
				
		mOnMovingPlatform->MovingPlatformId() = -1;
		mState[0] = mState[1] = IDLE;	
	}

	int const MAX_HEALTH = 150;
	int const MIN_HEALTH = 0;

	if (mHealth->CurrentHealth() > MAX_HEALTH)
		mHealth->CurrentHealth() = MAX_HEALTH;
	else if (mHealth->CurrentHealth() < MIN_HEALTH)
		mHealth->CurrentHealth() = MIN_HEALTH;


	if (mHealth->CurrentHealth() != mLastHealth )
	{	
		PlayerHealthMetadata meta;
		meta.playerHealth = mHealth->CurrentHealth();
		mLastHealth = mHealth->CurrentHealth();
		mEventManager->BroadcastEvent(gPlayerHealthEventName, &meta);	
	}
}

void 
Player::Attack( int const mouseX, int const mouseY )
{
	DirectX::XMFLOAT2 start_pos( mSpatializedComp->LocalPosition().x + mBoundedComp->AabbBounds().Extents.x,
							     mSpatializedComp->LocalPosition().y + mBoundedComp->AabbBounds().Extents.y);


	DirectX::XMFLOAT4 cam_pos = gApp->Renderer()->CurrentCamera()->Position();
	DirectX::XMVECTOR vec = DirectX::XMVectorSet( (float)(mouseX - (int)(gApp->WinWidth()/2) + cam_pos.x) - start_pos.x,
												  (float)((int)(gApp->WinHeight()/2) - mouseY + cam_pos.y) - start_pos.y,
												  0.f,
												  0.f);

	vec = DirectX::XMVector2Normalize(vec);
	DirectX::XMFLOAT2 dir;
	DirectX::XMStoreFloat2(&dir, vec);

	float startPos[2] = {start_pos.x /*+ (dir.x * (mBoundedComp->AabbBounds().Extents.x + 30.f))*/, 
		                 start_pos.y /*+ (dir.y * (mBoundedComp->AabbBounds().Extents.y + 30.f))*/};
	float velocity[2] = {dir.x * 50.f, dir.y * 50.f}; 
	float maxTravelDist = 1500.f;
	unsigned int damageToInflict = 1;

	if (mProjectileCount == PLAYER_NUM_PROJECTILES)
		mProjectileCount = 0;
	mProjectiles[mProjectileCount++]->Activate(mLevel, startPos, velocity, maxTravelDist, damageToInflict);

	//OutputDebugMsg( to_string(mouseX - (int)(gApp->WinWidth()/2) + cam_pos.x) + "\t" + to_string((int)(gApp->WinWidth()/2) - mouseY + cam_pos.y) + "\n" );
	//OutputDebugMsg( to_string(dir.x) + "\t" + to_string(dir.y) + "\n" );
}

void 
Player::SetSpriteAnimation( void )
{	
	float width  = mSprite->CurrentAnimationHitBoxSize().x;
	float height = mSprite->CurrentAnimationHitBoxSize().y;

	static bool jumping = false;

	if (mState[1] == JUMP && mCurrentAnim != "jump" && !jumping)
	{
		mCurrentAnim = "jump";
		mSprite->AnimateSprite("jump");
		jumping = true;
	}

	// Change to correct anim
	if (mIsOnFloor)
	{
		if (mState[0] == IDLE && mCurrentAnim != "idle")
		{
			mCurrentAnim = "idle";
			mSprite->AnimateSprite("idle");
		}
		else if (mState[0] == RUN && mCurrentAnim != "walk")
		{
			mCurrentAnim = "walk";
			mSprite->AnimateSprite("walk");
		}
		else if (mState[0] == SPRINT && mCurrentAnim != "run")
		{
			mCurrentAnim = "run";
			mSprite->AnimateSprite("run");
		}	

		jumping = false;
	}
	
	// Modify the spatialized component
	mSpatializedComp->LocalScale().x = width;
	mSpatializedComp->LocalScale().y = height;

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (width/2.f), (height/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (width/2.f), (height/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Set offset of sprite	to the SpatializedAssociated component
	DirectX::XMFLOAT2 sprite_offset = mSprite->CurrentAnimationOffsets();
	mSpriteSpatialAssociated->OffsetX() = sprite_offset.x;
	mSpriteSpatialAssociated->OffsetY() = sprite_offset.y;

	if ( mAccelComp->AccelerationVector().x > 0.f &&
		 mVelocity->VelocityVector().x > 0.f && 
		 mInputDir[0] != -1 )	
		mSprite->MirrorX(false);	
	else if ( mAccelComp->AccelerationVector().x < 0.f &&
		      mVelocity->VelocityVector().x < 0.f && 
			  mInputDir[0] != 1 )	
		mSprite->MirrorX(true);
}
