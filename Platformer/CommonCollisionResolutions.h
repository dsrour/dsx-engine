#pragma once

class Level;

void
MovingPlatformResolution(unsigned int const entityId, Level* const level, CollisionInfo const& collisionInfo, DirectX::XMFLOAT3 const& localPos);
