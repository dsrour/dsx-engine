// Used by SpatializedAssociated system.

#pragma once

#include <DirectXMath.h>
#include "Component.hpp"

class SpatializedAssociated : public Component<SpatializedAssociated>
{
public:    
	SpatializedAssociated(int const associatedEntityId) 
	{ 
		mAssociatedEntityId = associatedEntityId; 
		mOffsetX = mOffsetY = 0.f;
	}

	int&
	AssociatedEntity(void) { return mAssociatedEntityId; } 

	float&
	OffsetX(void) { return mOffsetX; }

	float&
	OffsetY(void) { return mOffsetY; }


private:  
	int mAssociatedEntityId;
	float mOffsetX;
	float mOffsetY;
};