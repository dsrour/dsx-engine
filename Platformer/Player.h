#pragma once

#include <set>
#include "System.h"
#include "EventManager/EventManager.h"

#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"

#include "PlayerInputs.h"
#include "Projectile.h"

#include "Scene/SceneObject.h"
#include "CommonRenderables/LineRenderable.h"
#include "SpriteAnimationSystem/SpriteEntity.h"

#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/Acceleration.h"
#include "2dTiledEngine/Components/Friction.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Health.h"
#include "2dTiledEngine/Components/Mass.h"
#include "2dTiledEngine/TileEntity.h"
#include "2dTiledEngine/Components/OnMovingPlatform.h"
#include "2dTiledEngine/Components/Damageable.h"

#include "SpatializedAssociated.h"

#define PLAYER_NUM_PROJECTILES 10
#define PLAYER_COLLIDING_HISTORY 10


class Player : public TileEntity, public System, public std::enable_shared_from_this<Player>
{
public:
	enum State
	{
		IDLE,
		RUN,
		SPRINT,
		JUMP,		
	};

	Player(std::shared_ptr<EntityManager> entityManager, std::shared_ptr<EventManager> eventManager);
	~Player(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );
		
	void
	UpdateInputs(int const(& dir)[2], bool const sprint);

	void
	Attack(int const mouseX, int const mouseY);

	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

private:
	void
	SetVelocity(DirectX::XMFLOAT3 const& velocity);

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);
	
	void 
	CenterCamera(void);

	void
	CheckForDamage(void);

	void
	SetSpriteAnimation(void);

	void
	ClearCollidingHistory() 
	{
		for (unsigned int i = 0; i < PLAYER_COLLIDING_HISTORY; i++)
			mCollidingHistory[i] = -1; 
	}

	bool
	SearchCollidingHistory(unsigned int const entityId)
	{
		for (unsigned int i = 0; i < PLAYER_COLLIDING_HISTORY; i++)
			if (mCollidingHistory[i] == entityId)
				return true; 

		return false;
	}

	void
	AddToCollidingHistory(unsigned int const entityId)
	{		
		for (int i = 0; i < PLAYER_COLLIDING_HISTORY; i++)
			if (mCollidingHistory[i] == -1)
			{
				mCollidingHistory[i] = entityId;
				return;
			}		
	}

	Level* mLevel; // hope this never changes!
	
	unsigned int mSysId;

	std::shared_ptr<EventManager> mEventManager;

	DirectX::XMFLOAT2 mLastNormal;
	float mJumpOriginY;
	double mJumpStartTime;
	bool mIsOnFloor;
	std::pair<unsigned int, unsigned int> mLevelSize;
	unsigned int mTileSize;

	std::shared_ptr<LineRenderable> mRenderableComponent;	
	std::shared_ptr<SpriteEntity> mSprite;
	std::shared_ptr<SpatializedAssociated> mSpriteSpatialAssociated;
	std::shared_ptr<PlayerKeyboardInputs> mKeyboardInputs;
	std::shared_ptr<PlayerMouseInputs> mMouseInputs;
	std::shared_ptr<Acceleration> mAccelComp;
	std::shared_ptr<Velocity> mVelocity;
	std::shared_ptr<Friction> mFriction;
	std::shared_ptr<Elasticity> mElasticity;
	std::shared_ptr<OnMovingPlatform> mOnMovingPlatform;
	std::shared_ptr<Health> mHealth;
	std::shared_ptr<Damageable> mDamageable;
	std::shared_ptr<Mass> mMass;

	std::shared_ptr<Projectile> mProjectiles[PLAYER_NUM_PROJECTILES];
	unsigned int mProjectileCount;

	int mInputDir[2];
	State mState[2]; // for x and y dir

	static TwBar* mPlayerTwBar; 

	int mLastHealth;
	bool mIsStunned;
	double mStunEndTime;

	std::string mCurrentAnim;

	int mCollidingHistory[10]; // PLAYER_COLLIDING_HISTORY last collided entities since last RunImplementation() callback
	
	/////////////////////
	static float mAccel, mDecel;
	static float mRun, mSprint;
	static float mJumpEndVel, mJumpTime, mJumpHeight;
	static float mGravity;
	/////////////////////
};

