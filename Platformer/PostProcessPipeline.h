#pragma once

#include "Systems/Renderer/DirectXTK/Inc/SpriteFont.h"
#include "Systems/Renderer/Pipeline.h"
#include "EventManager/EventManager.h"
#include "BaseApp.h"
#include "Platformer.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;

class PostProcessPipeline : public Pipeline, public EventSubscriber
{
public:
	PostProcessPipeline(std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EventManager> eventMngr);
    
    virtual 
    ~PostProcessPipeline(void);
          
    virtual void
    MadeActive(void) {}

    virtual void
    MadeInactive(void) {}

    virtual void
    UpdatePipeline(void) {}

	virtual void
	RecompileShaders() { /*no shaders*/ }

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);

private:	
	virtual void
	OnEvent(std::string const& event, Metadata* const metadata);

	std::shared_ptr<EventManager> mEventManager;

	DirectX::SpriteFont*	  mSpriteFont; 

	DirectX::SpriteBatch*	  mFpsSprite;
	DirectX::SpriteBatch*	  mTimerSprite;
	DirectX::SpriteBatch*	  mPlayerHealthSprite;

	double mTimerValue;   
	int mPlayerHealth;
};