#include <ctime>
#include "2dTiledEngine/Level.h"
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Utils/MathUtils.h"
#include "Player.h"
#include "CommonCollisionResolutions.h"
#include "RandomMovingEnemy.h"

extern BaseApp* gApp;

RandomMovingEnemy::RandomMovingEnemy(void)
{
	
}


RandomMovingEnemy::~RandomMovingEnemy(void)
{
}

void 
RandomMovingEnemy::OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size )
{	
	mWidth = size.x;
	mHeight = size.y;


	// Modify the spatialized component
	DirectX::XMFLOAT3 world_pos(position.x, position.y, 0.f);
	mSpatializedComp->LocalPosition(world_pos);

	// Modify the bounded component
	DirectX::XMFLOAT3 center( (mWidth/2.f), (mHeight/2.f), 0.f );
	DirectX::XMFLOAT3 extents( (mWidth/2.f), (mHeight/2.f), 0.001f);
	DirectX::BoundingBox aabb(center, extents);
	mBoundedComp->AabbBounds(aabb);

	// Create the renderable
	std::vector<DirectX::XMFLOAT3> verts;
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, 0.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, mHeight, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f, 0.f, 0.f ) );

	float eye_size = mWidth / 3.f;
	verts.push_back( DirectX::XMFLOAT3( 0.f + eye_size/3.f, mHeight - mHeight/3.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f + eye_size/3.f + eye_size, mHeight - mHeight/3.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth - eye_size/3.f, mHeight - mHeight/3.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( mWidth - eye_size/3.f - eye_size, mHeight - mHeight/3.f, 0.f ) );

	float mouth_size = mWidth / 2.f;
	verts.push_back( DirectX::XMFLOAT3( 0.f + mouth_size/2.f, 0.f+mHeight/3.f, 0.f ) );
	verts.push_back( DirectX::XMFLOAT3( 0.f + mouth_size/2.f + mouth_size, 0.f+mHeight/3.f, 0.f ) );


	mRenderableComponent = std::make_shared<LineRenderable>(gApp->Renderer()->Device(), verts);
	Material mat;
	mat.diffuse = DirectX::XMFLOAT4(1.f, 0.f, 1.f, 1.f);
	mRenderableComponent->MaterialProperties(mat);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mRenderableComponent);

	// Velocity + mElasticity
	mElasticity = std::make_shared<Elasticity>(3.0f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mElasticity);
	
	mVelocity = std::make_shared<Velocity>();
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mVelocity);
	
	mVelocity->VelocityVector(RandomVelocity(-4.f, 4.f));

	// Mass
	mMass = std::make_shared<Mass>(0.f);
	gApp->EntityMngr()->AddComponentToEntity(EntityId(), mMass);

	// Make it an obstacle
	mTiledEntityComp->IsObstacle(true);

	mLastNormal = DirectX::XMFLOAT2(0.f, 0.f);
}



void 
RandomMovingEnemy::OnLevelRemoval( Level* const level )
{

}


/*virtual*/ void
RandomMovingEnemy::OnCollision(Level* const level, CollisionInfo const& collisionInfo) 
{
	unsigned int id = EntityId();
	if (collisionInfo.movingEntityId == id)
		id = collisionInfo.collidingEntityId;

	TileEntity* moving_entity = level->Entity(id);	
	Damageable* moving_damageable = DamageableComponent(id);

	if (moving_entity && moving_damageable)
	{
		unsigned int DAMAGE_TO_INFLICT = 1;
		moving_damageable->InflictedDamage() = DAMAGE_TO_INFLICT;

		if (id == collisionInfo.movingEntityId)
		{
			moving_damageable->HitNormal()[0] = (int)collisionInfo.normal.x;
			moving_damageable->HitNormal()[1] = (int)collisionInfo.normal.y;
		}
		else
		{
			moving_damageable->HitNormal()[0] = -(int)collisionInfo.normal.x;
			moving_damageable->HitNormal()[1] = -(int)collisionInfo.normal.y;
		}
		
	}
	else
		mVelocity->VelocityVector(RandomVelocity(-4.f, 4.f));
}

DirectX::XMFLOAT3 
RandomMovingEnemy::RandomVelocity( float min, float max )
{
	return DirectX::XMFLOAT3( min + (float)rand()/((float)RAND_MAX/(max-min) ),
							  min + (float)rand()/((float)RAND_MAX/(max-min) ), 0.f );
}

