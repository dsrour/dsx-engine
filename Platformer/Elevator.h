#pragma once

#include "CommonRenderables/LineRenderable.h"
#include "2dTiledEngine/Components/Velocity.h"
#include "2dTiledEngine/Components/Elasticity.h"
#include "2dTiledEngine/Components/Mass.h"
#include "2dTiledEngine/TileEntity.h"
#include "System.h"

class Elevator : public TileEntity, public System, public std::enable_shared_from_this<Elevator>
{
public:
	Elevator(std::shared_ptr<EntityManager> entityManager, double const waitTime, 
		     float const heightTravel, float const velocity);
	~Elevator(void);

	virtual void
	OnLevelInsert( Level* const level, DirectX::XMFLOAT2 const& position, DirectX::XMFLOAT2 const& size );

	virtual void
	OnLevelRemoval( Level* const level );

	virtual void
	OnCollision(Level* const level, CollisionInfo const& collisionInfo);
	
	void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);
		
private:
	std::shared_ptr<LineRenderable> mRenderableComponent;
	std::shared_ptr<Velocity> mVelocityComp;
	std::shared_ptr<Elasticity> mElasticity;
	std::shared_ptr<Mass> mMass;

	float mWidth, mHeight;

	unsigned int mSysId;

	bool mGoesUp;
	bool mGoingToDest;
	
	DirectX::XMFLOAT2 mLastNormal;

	double mWaitTime;	
	float mStartPosY, mEndPosY;
	float mHeightTravel;
	float mVelocity;
	double mArrivalTime;
};

