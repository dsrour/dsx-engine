#include "2dTiledEngine/2dTiledEngineComponentFetches.h"
#include "Utils/MathUtils.h"
#include "Debug/Debug.h"
#include "2dTiledEngine/Level.h"
#include "CommonCollisionResolutions.h"

void 
MovingPlatformResolution( unsigned int const entityId, Level* const level, CollisionInfo const& collisionInfo, DirectX::XMFLOAT3 const& localPos )
{
	if (collisionInfo.movingEntityId == entityId && collisionInfo.normal.y == -1)  // Mving pltfrm is moving and pushed down
	{		
		OnMovingPlatform* mv_pltfrm = OnMovingPlatformComponent(collisionInfo.collidingEntityId);
		if (mv_pltfrm && mv_pltfrm->MovingPlatformId() != entityId)
		{			
			TileEntity* entity = level->Entity(collisionInfo.collidingEntityId);
			mv_pltfrm->MovingPlatformId() = collisionInfo.movingEntityId;
			mv_pltfrm->Offset() = DirectX::XMFLOAT2( entity->SpatializedComponent()->LocalPosition().x - localPos.x,
												     entity->SpatializedComponent()->LocalPosition().y - localPos.y + 2.f );									
		}
	}
	else if (collisionInfo.collidingEntityId == entityId && collisionInfo.normal.y == 1) // Mving pltfrm is collided by something on top of it
	{
		TileEntity* moving_entity = level->Entity(collisionInfo.movingEntityId);

		OnMovingPlatform* mv_pltfrm = OnMovingPlatformComponent(collisionInfo.movingEntityId);
		if (mv_pltfrm && mv_pltfrm->MovingPlatformId() != entityId)
		{			
			TileEntity* entity = level->Entity(collisionInfo.movingEntityId);
			mv_pltfrm->MovingPlatformId() = collisionInfo.collidingEntityId;
			mv_pltfrm->Offset() = DirectX::XMFLOAT2( entity->SpatializedComponent()->LocalPosition().x - localPos.x,
													 entity->SpatializedComponent()->LocalPosition().y - localPos.y + 2.f );												
		}	
	}
	else
	{
		OnMovingPlatform* mv_pltfrm = OnMovingPlatformComponent(collisionInfo.movingEntityId);
		if (mv_pltfrm)
			mv_pltfrm->MovingPlatformId() = -1;
	}
}
