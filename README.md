This repository contains a C++ component-entity system engine with a real-time renderer.  
The repository also contains many projects that are dependent on the "DSX" engine.  

Projects:  
"Materials": a simple demo that shows off the fur and physically based rendering capabilities of the engine.  
"MazeRace": a simple demo game that demonstrates the usage of the component-entity system as well as the instancing abilities of the renderer.  
"Particles": a GPU based particle renderer.  
"Platformer": a simple platformer demo with a self-made physics engine and sprite based renderer.  
"PointCloud": massive point cloud renderer and analysis school research project.  
"VoxelLighting": ongoing research project of a hybrid forward/ray-tracing renderer.  The name is somewhat misleading since the project started as a voxel renderer. It is now a hybrid renderer in which rays are spawned on demand to enhance rasterization. The current work deals with ray-tracing penumbras of shadows. Regular shadow-mapping is used and the edge of shadows are detected. Following this, the penumbra size is estimated for each edge pixel. The edges are morphological dilation to fill the penumbras. From there, a certain amount of shadow rays are spawned to calculate soft-shadows.
   
   
The whole project is a VS2013 solution.   
You'll need Windows 8.1 for DirectX 11.2.  
The only project that requires external dependencies is the "PointCloud" project for which you'll need the LAS & Boost library.

![mat1.jpg](https://bitbucket.org/repo/GXBdez/images/673711937-mat1.jpg)
![mat2.jpg](https://bitbucket.org/repo/GXBdez/images/540110427-mat2.jpg)
![mat3.jpg](https://bitbucket.org/repo/GXBdez/images/4004819732-mat3.jpg)
![maze_race.jpg](https://bitbucket.org/repo/GXBdez/images/2243562435-maze_race.jpg)
![particles.jpg](https://bitbucket.org/repo/GXBdez/images/1233187852-particles.jpg)
![pnts.jpg](https://bitbucket.org/repo/GXBdez/images/2100662972-pnts.jpg)
![pnts2.jpg](https://bitbucket.org/repo/GXBdez/images/3809178949-pnts2.jpg)