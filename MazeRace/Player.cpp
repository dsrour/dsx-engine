#include "Component.hpp"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Utils/ComponentFetches.h"
#include "BaseApp.h"
#include "MazeRace.h"
#include "Player.h"

extern std::wstring gResourcesDir;
extern BaseApp* gApp;

Player::Player(Level* const currentLevel)
{
	mCurrentLevel = currentLevel;
	mDefaultColor = DirectX::XMFLOAT4(0.03f, 0.03f, 0.99f, 1.f);
	mPenalizedColor = DirectX::XMFLOAT4(0.99f, 0.03f, 0.03f, 1.f);
	mPlayerSo = NULL;
	mPenalized = false;
	InitPlayer();
}


Player::~Player(void)
{
	if (mPlayerSo)
		delete mPlayerSo;

	gApp->EntityMngr()->DestroyEntity(mLightId);
}

void 
Player::InitPlayer( void )
{
	mPlayerSo = new SceneObject;
	std::wstring obstacle_model = gResourcesDir + L"MazeRace\\player.obj";
	mPlayerSo->SetGeometryFromFile(obstacle_model, false);	

	Spatialized* spatialized = SpatializedFromEntity(mPlayerSo->EntityId());

	PositionPlayer(mCurrentLevel->StartPos());	

	mPlayerSo->ToggleBoundBox(false);

	mLightId = gApp->EntityMngr()->CreateEntity();                  
	std::shared_ptr<LightEmitting> light_emmiter( new LightEmitting() );            	
	gApp->EntityMngr()->AddComponentToEntity(mLightId, light_emmiter);

	SetPlayerColor(mDefaultColor);
}

DirectX::XMFLOAT3 
Player::CurrentMazePosToWorldPos( void )
{
	DirectX::XMFLOAT3 pos;
	pos.x = (MAZE_TILE_SIZE * mCurrentMazePosition.first) - (MAZE_TILE_SIZE * (mCurrentLevel->MazeSize()-1))/2; 
	pos.y = (MAZE_TILE_SIZE *  mCurrentMazePosition.second) - (MAZE_TILE_SIZE * (mCurrentLevel->MazeSize()-1))/2; 
	pos.z = 100.f; //TODO:: calculate this depending on size of maze so that it fits the screen nicely
	return pos;
}

void 
Player::MoveNorth( void )
{
	if (mPenalized)
	{
		mPenalized = false;
		SetPlayerColor(mDefaultColor);
		return;
	}

	if (mCurrentMazePosition.second == mCurrentLevel->MazeSize()-1)
		return;

	std::pair<unsigned int, unsigned int> next_pos(mCurrentMazePosition.first, mCurrentMazePosition.second + 1);

	if (mCurrentLevel->IsPositionAnObstacle(next_pos))
	{
		mPenalized = true;
		SetPlayerColor(mPenalizedColor);
		return;
	}

	PositionPlayer(next_pos);
}

void 
Player::MoveSouth( void )
{
	if (mPenalized)
	{
		mPenalized = false;
		SetPlayerColor(mDefaultColor);
		return;
	}

	if (mCurrentMazePosition.second == 0)
		return;

	std::pair<unsigned int, unsigned int> next_pos(mCurrentMazePosition.first, mCurrentMazePosition.second - 1);

	if (mCurrentLevel->IsPositionAnObstacle(next_pos))
	{
		mPenalized = true;
		SetPlayerColor(mPenalizedColor);
		return;
	}

	PositionPlayer(next_pos);
}

void 
Player::MoveWest( void )
{
	if (mPenalized)
	{
		mPenalized = false;
		SetPlayerColor(mDefaultColor);
		return;
	}

	if (mCurrentMazePosition.first == 0)
		return;

	std::pair<unsigned int, unsigned int> next_pos(mCurrentMazePosition.first - 1, mCurrentMazePosition.second);

	if (mCurrentLevel->IsPositionAnObstacle(next_pos))
	{
		mPenalized = true;
		SetPlayerColor(mPenalizedColor);
		return;
	}

	PositionPlayer(next_pos);
}

void 
Player::MoveEast( void )
{
	if (mPenalized)
	{
		mPenalized = false;
		SetPlayerColor(mDefaultColor);
		return;
	}

	if (mCurrentMazePosition.first == mCurrentLevel->MazeSize()-1)
		return;

	std::pair<unsigned int, unsigned int> next_pos(mCurrentMazePosition.first + 1, mCurrentMazePosition.second);

	if (mCurrentLevel->IsPositionAnObstacle(next_pos))	
	{
		mPenalized = true;
		SetPlayerColor(mPenalizedColor);
		return;
	}

	PositionPlayer(next_pos);
}

void 
Player::PositionPlayer( std::pair<unsigned int, unsigned int> const& newMazePos )
{
	mCurrentMazePosition = newMazePos;

	std::shared_ptr<IComponent> tmp_comp;

	// Move light
	LightEmitting* comp = NULL;
	gApp->EntityMngr()->GetComponentFromEntity(mLightId, LightEmitting::GetGuid(), tmp_comp);
	if (tmp_comp)
	{
		comp = static_cast<LightEmitting*>(tmp_comp.get());
		static_cast<PointLight*>(comp->GetLight())->position = CurrentMazePosToWorldPos();
	}

	// Move model
	SpatializedFromEntity(mPlayerSo->EntityId())->LocalPosition(CurrentMazePosToWorldPos());

	// Leave trail
	mCurrentLevel->MarkTrail(mCurrentMazePosition);

	// Did we reach the end?
	if (newMazePos == mCurrentLevel->GoalPos())
		static_cast<MazeRace*>(gApp)->SetGameState(LEVEL_END_WIN);
}

void
Player::SetPlayerColor( DirectX::XMFLOAT4 const& color )
{
	Material mat;
	mat.ambient = color;
	mat.diffuse = color;
	mat.specular = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	mPlayerSo->SetMaterial(mat);	

	LightEmitting* emitter = LightEmittingFromEntity(mLightId);
	PointLight light;
	light.ambient = color;										
	light.diffuse = color;		
	light.specular = color;		
	light.position = CurrentMazePosToWorldPos();	 
	light.range = MAZE_TILE_SIZE + 2.5f;
	emitter->SetPointLight(light); 
}
