#pragma once

#include <memory>
#include "KeyboardActions.h"
#include "Level.h"
#include "Player.h"
#include "BaseApp.h"
#include "PostProcessPipeline.h"

enum GameState
{
	LEVEL_START,
	LEVEL_PLAY,
	LEVEL_END_WIN,
	LEVEL_END_LOSE,
};

class MazeRace : public BaseApp
{
public:
    MazeRace(void);

    virtual
    ~MazeRace(void);

    bool const
    OnLoadApp(void);
    
    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);
        
    void
    OnUnloadApp(void);

	GameState const&
	CurrentGameState(void) const {return mGameState;}

	void
	SetGameState(GameState const& state) {mGameState = state;}

	unsigned int const&
	LevelDifficulty(void) const { return mCurrentDifficulty; }

	float const&
	TimeRemaining(void) const { return mTimeRemaining; }
   
private:   
	void
	InitNewLevel(void);

	void
	ProceduralMazeCreation(MAZE_GRID& grid, std::pair<unsigned int,unsigned int>& start, std::pair<unsigned int,unsigned int>& goal, unsigned int gridSize);
	
	int 
	Roll(int min, int max);

    std::shared_ptr<ApplicationKeyboardActions> mKeyboardSystem;
	GameState mGameState;

	Level* mCurrentLevel;
	Player* mPlayer;

	unsigned int mCurrentDifficulty;
	float mTimeRemaining;
};

