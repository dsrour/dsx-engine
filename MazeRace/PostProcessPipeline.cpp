#include <sstream> 
#include <iostream>
#include <iomanip>
#include "PostProcessPipeline.h"


PostProcessPipeline::PostProcessPipeline( std::shared_ptr<D3dRenderer> renderer ) : Pipeline(renderer)
{
	mFpsSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mLevelSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mTimeSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mMsgSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());

	std::wstring font_path = gResourcesDir + L"Fonts\\font.spritefont";
	mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (WCHAR*)font_path.c_str());
}

PostProcessPipeline::~PostProcessPipeline( void )
{
	if (mSpriteFont)
		delete mSpriteFont;

	if (mFpsSprite)
		delete mFpsSprite;
	if (mLevelSprite)
		delete mLevelSprite;
	if (mTimeSprite)
		delete mTimeSprite;
	if (mMsgSprite)
		delete mMsgSprite;
}

void PostProcessPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	// FPS
	{
		// Since this gets called every time we render... we just see how many frames we get per second
		static double elapsed = gApp->Timer().ElapsedTimeSecs();
		static unsigned int frames = 0;			
		frames++;
					
		static std::wstring fps_str;

		if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
		{
			std::wstringstream ss;   
			ss << frames; 
			fps_str = ss.str() + L" FPS";
			frames = 0;
			elapsed = gApp->Timer().ElapsedTimeSecs();
		}						
		mFpsSprite->Begin();
		mSpriteFont->DrawString(mFpsSprite, fps_str.c_str(), DirectX::XMFLOAT2(25.f, gApp->WinHeight() - 30.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
		mFpsSprite->End();
	}		

	// Level #
	{
		std::wstringstream ss;
		ss << static_cast<MazeRace*>(gApp)->LevelDifficulty();
		std::wstring str = L"Level: " + ss.str();
		mLevelSprite->Begin();
		mSpriteFont->DrawString(mLevelSprite, str.c_str(), DirectX::XMFLOAT2(5.f, 5.f), DirectX::XMVectorSet(0.75f,0.75f,0.65f,1.f));
		mLevelSprite->End();
	}

	// Timer
	{
		std::wstringstream ss;		
		ss << std::setw(5) << std::showpoint << std::fixed << std::setprecision(2);
		ss << static_cast<MazeRace*>(gApp)->TimeRemaining();
		std::wstring str = ss.str();		
		mTimeSprite->Begin();
		mSpriteFont->DrawString(mTimeSprite, str.c_str(), DirectX::XMFLOAT2((gApp->WinWidth()/2.f) - 30.f, 5.f), DirectX::XMVectorSet(0.75f,0.75f,0.65f,1.f));
		mTimeSprite->End();
	}

	// Game Over msg
	if (LEVEL_END_LOSE == static_cast<MazeRace*>(gApp)->CurrentGameState())
	{		
		std::wstring str = L" GAME OVER\nPRESS ENTER";		
		mMsgSprite->Begin();
		mSpriteFont->DrawString(mMsgSprite, str.c_str(), DirectX::XMFLOAT2((gApp->WinWidth()/2.f) - 130.f, (gApp->WinHeight()/2.f) - 20), DirectX::XMVectorSet(1.f,0.f,0.f,1.f),
			                    0.f, DirectX::XMFLOAT2(0.f, 0.f), DirectX::XMFLOAT2(1.75f, 1.75f) );
		mMsgSprite->End();
	}

	if (LEVEL_END_WIN == static_cast<MazeRace*>(gApp)->CurrentGameState())
	{		
		std::wstring str = L" LEVEL CLEAR\nPRESS ENTER";		
		mMsgSprite->Begin();
		mSpriteFont->DrawString(mMsgSprite, str.c_str(), DirectX::XMFLOAT2((gApp->WinWidth()/2.f) - 130.f, (gApp->WinHeight()/2.f) - 20), DirectX::XMVectorSet(1.f,0.f,0.f,1.f),
			0.f, DirectX::XMFLOAT2(0.f, 0.f), DirectX::XMFLOAT2(1.75f, 1.75f) );
		mMsgSprite->End();
	}
}
