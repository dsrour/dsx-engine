#pragma once

#include "Systems/Renderer/Cameras/FpCamera.h"
#include "InputDeviceManager.h"
#include "Player.h"

class ApplicationKeyboardActions : public InputDeviceActions
{
public:
    ApplicationKeyboardActions(std::shared_ptr<Camera> camera, std::shared_ptr<InputDeviceManager> inputDeviceManager);    

    virtual void
    OnStateChange(void);   

	void
	SetPlayerToControl(Player* const player);

private:
    std::shared_ptr<Camera> mCamera;
    double mLastTime;   
	Player* mPlayer;
};