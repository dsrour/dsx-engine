#pragma once

#include <DirectXMath.h>
#include <map>
#include "Level.h"
#include "SceneObject.h"


class Player
{
public:
	Player(Level* const currentLevel);
	~Player(void);

	void
	MoveNorth(void);

	void
	MoveSouth(void);

	void
	MoveEast(void);

	void
	MoveWest(void);

private:
	void
	InitPlayer(void);

	DirectX::XMFLOAT3
	CurrentMazePosToWorldPos(void);

	void
	PositionPlayer(std::pair<unsigned int, unsigned int> const& newMazePos);

	void
	SetPlayerColor(DirectX::XMFLOAT4 const& color);

	DirectX::XMFLOAT4 mDefaultColor;
	DirectX::XMFLOAT4 mPenalizedColor;
	std::pair<unsigned int, unsigned int> mCurrentMazePosition;
	Level* mCurrentLevel;

	SceneObject* mPlayerSo;
	unsigned int mLightId;

	bool mPenalized;
};

