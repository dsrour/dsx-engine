#pragma once

// Components //
#include "Components/Bounded.h"
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/LightEmitting.h"
////////////////

class SceneObject
{
public:
	SceneObject(void);
	~SceneObject(void);

	/// Changes the bounding box renderable's color.
	void
	BoundingRenderableColor(DirectX::XMFLOAT4 const& color);

	/* Set geometry using a 3d model file.
	 *
	 * Only obj files supported right now.
	 */
	void 
	SetGeometryFromFile(std::wstring& fileName, bool const& lit = true);

	/* Set diff map using a file.
	 *
	 * Only .dds files supported right now.
	 */
	void 
	SetDiffuseMapFromFile(std::wstring& fileName);

	/// Shows / Hides bound box renderable.
	void
	ToggleBoundBox(bool const& show);

	unsigned int const
	EntityId(void) const { return mEntityId; }

	bool const
	HasGeometry(void) const { return mRenderableComp ? true : false; }

	void
	SetMaterial(Material& mat);

	/* Set normal map using a file.
	 *
	 * Only .dds files supported right now.
	 */
	void 
	SetNormalMapFromFile( std::wstring& fileName );

	std::shared_ptr<Renderable>
	GetRenderable(void) { return mRenderableComp; }

	void 
	MakeGeometryInstanced(unsigned int const& batchId, DirectX::XMFLOAT4 const& instancedColor, std::vector<DirectX::XMFLOAT3>& verts);

private:	
	unsigned int mEntityId;

	// Bounds for debugging
	unsigned int mSoBoundsId;
	std::shared_ptr<Renderable> mSoBoundsRenderable;


	// Components
	std::shared_ptr<Bounded>		mBoundedComp;		
	std::shared_ptr<Renderable>		mRenderableComp;		
	std::shared_ptr<Spatialized>	mSpatializedComp;		
	std::shared_ptr<DiffuseMapped>	mDiffuseMappedComp;		
	std::shared_ptr<NormalMapped>	mNormalMappedComp;		
};

