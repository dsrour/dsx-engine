#pragma once

#include <vector>
#include "SceneObject.h"

#define MAZE_GRID std::vector< std::vector<bool> >  // true means [X, Y] is an obstacle
#define MAZE_TILE_SIZE	10.f

class Level
{
public:
	Level(MAZE_GRID& obstacles, std::pair<unsigned int, unsigned int>& start, std::pair<unsigned int, unsigned int>& goal);
	~Level(void);

	std::pair<unsigned int, unsigned int> const&
	StartPos(void) const {return mStart;}

	std::pair<unsigned int, unsigned int> const&
	GoalPos(void) const {return mGoal;}

	unsigned int
	MazeSize(void) { return (unsigned int)mGrid[0].size(); }

	bool const
	IsPositionAnObstacle(std::pair<unsigned int, unsigned int> const& pos);

	void
	MarkTrail(std::pair<unsigned int, unsigned int> const& pos);

private:
	void
	InitSceneObjects();

	MAZE_GRID mGrid;
	std::pair<unsigned int, unsigned int> mStart;
	std::pair<unsigned int, unsigned int> mGoal;

	std::map< std::pair<unsigned int, unsigned int>, SceneObject* > mSceneObjects;
};

