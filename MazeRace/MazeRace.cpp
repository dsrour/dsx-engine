#include <cstdlib>
#include <ctime>
#include <stack>
#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"
#include "PostProcessPipeline.h"

#include "MazeRace.h"

BaseApp* gApp = new MazeRace;

MazeRace::MazeRace() 
{
    WinWidth(1888);
    WinHeight(992);
	mCurrentLevel = NULL;
	mPlayer = NULL;
	mCurrentDifficulty = 1;
	mTimeRemaining = 0.f;

	// Random num gen
	srand((unsigned int)time(NULL));
}

MazeRace::~MazeRace()
{
	if (mPlayer)
		delete mPlayer;

    if (mCurrentLevel)
		delete mCurrentLevel;	
}

bool const
MazeRace::OnLoadApp()
{
    OutputDebugMsg("In OnLoadApp()\n");
    TitleName(L"MAZE RACE");

    Renderer()->ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Add a directional light
	unsigned int light_id = gApp->EntityMngr()->CreateEntity();                  
	std::shared_ptr<LightEmitting> light_emmiter( new LightEmitting() );            
	DirectionalLight light;
	light.ambient = DirectX::XMFLOAT4( 0.08f, 0.08f, 0.08f, 1.f );										
	light.diffuse = DirectX::XMFLOAT4( 0.08f, 0.08f, 0.08f, 1.f );		
	light.specular = DirectX::XMFLOAT4( 0.16f, 0.16f, 0.16f, 1.f );		
	light.direction  = DirectX::XMFLOAT3( 0.f, 0.f, 1.f );		 
	light_emmiter->SetDirectionalLight(light);    
	gApp->EntityMngr()->AddComponentToEntity(light_id, light_emmiter);

	// Set initial state
	mGameState = LEVEL_END_LOSE;

	// User input via keyboard
	mKeyboardSystem = std::make_shared<ApplicationKeyboardActions>(Renderer()->CurrentCamera(), InputDeviceMngr());
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardSystem);

	// Set Render Path with a post-process pipeline
	std::shared_ptr<ForwardRenderer> forward_renderer(new ForwardRenderer(gApp->Renderer()));
	std::shared_ptr<SkyRenderer> sky(new SkyRenderer(gApp->Renderer()));
	std::shared_ptr<PostProcessPipeline> post_proc_pipeline(new PostProcessPipeline(gApp->Renderer()));
	std::list< std::shared_ptr<Pipeline> > path; 
	path.push_back(forward_renderer); 
	path.push_back(sky); 
	path.push_back(post_proc_pipeline); 
	gApp->Renderer()->RenderPath(path);

    return true;
}

    
void
MazeRace::OnPreUpdate()
{        
	static bool advance_level = false;
	if (LEVEL_END_WIN == mGameState)
	{
		advance_level = true;				
	}

	if (LEVEL_START == mGameState)
	{
		if (advance_level)
		{			
			mCurrentDifficulty++;
		}
		InitNewLevel();
		
		if (advance_level)
		{
			mTimeRemaining += mCurrentDifficulty;			
		}		
	}

	if (LEVEL_PLAY == mGameState)
	{
		// Lower timer
		static double last_time = Timer().ElapsedTimeSecs();
		
		if (advance_level)
		{
			last_time =  (float)Timer().ElapsedTimeSecs();
			advance_level = false;
		}
		else
		{
			mTimeRemaining -= (float)( Timer().ElapsedTimeSecs() - last_time );
			last_time =  (float)Timer().ElapsedTimeSecs();
		}

		if (mTimeRemaining <= 0.f)
		{
			mTimeRemaining = 0.f;
			mCurrentDifficulty = 1; // Back to first level
			SetGameState(LEVEL_END_LOSE);
		}
	}
}

void
MazeRace::OnPostUpdate()
{           
        
}

    
void
MazeRace::OnUnloadApp()
{
       
}

void 
MazeRace::InitNewLevel( void )
{
	if (mPlayer)
	{
		delete mPlayer;
		mPlayer = NULL;
		mKeyboardSystem->SetPlayerToControl(NULL);
	}

	if (mCurrentLevel)
	{
		delete mCurrentLevel;
		mCurrentLevel = NULL;
	}

	// If first level, reset timer
	if (1 == mCurrentDifficulty)
	{
		mTimeRemaining = 30.f;
	}

	

	//// Hand made lvl lolz
	///*
	//	***0***
	//	***0***
	//	*00000*
	//	*0*0*0*
	//	*00000*
	//	***0***
	//	***0***
	// */
	//MAZE_GRID grid;

	//std::vector<bool> tmp(7);
	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = true;
	//tmp[3] = true;
	//tmp[4] = true;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	
	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = false;
	//tmp[3] = false;
	//tmp[4] = false;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	
	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = false;
	//tmp[3] = true;
	//tmp[4] = false;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	

	//tmp[0] = false;
	//tmp[1] = false;
	//tmp[2] = false;
	//tmp[3] = false;
	//tmp[4] = false;
	//tmp[5] = false;
	//tmp[6] = false;	
	//grid.push_back(tmp);	

	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = false;
	//tmp[3] = true;
	//tmp[4] = false;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	
	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = false;
	//tmp[3] = false;
	//tmp[4] = false;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	
	//tmp[0] = true;
	//tmp[1] = true;
	//tmp[2] = true;
	//tmp[3] = true;
	//tmp[4] = true;
	//tmp[5] = true;
	//tmp[6] = true;	
	//grid.push_back(tmp);	

	//mCurrentLevel = new Level(grid, std::pair<unsigned int,unsigned int>(3, 0), std::pair<unsigned int,unsigned int>(3, 6));


	MAZE_GRID grid;
	std::pair<unsigned int,unsigned int> start, goal;
	ProceduralMazeCreation(grid, start, goal, 10);
	mCurrentLevel = new Level(grid, start, goal);

	mPlayer = new Player(mCurrentLevel);
	mKeyboardSystem->SetPlayerToControl(mPlayer);	

	SetGameState(LEVEL_PLAY);
}

void MazeRace::ProceduralMazeCreation( MAZE_GRID& grid, std::pair<unsigned int,unsigned int>& start, std::pair<unsigned int,unsigned int>& goal, unsigned int gridSize )
{
	//OutputDebugMsg("\nNEW MAZE:\n");
	if (gridSize < 7)
		gridSize = 7;

	// Create grid filled of obstacles
	std::vector<bool> tmp(gridSize);
	for (unsigned int i = 0; i < gridSize; i++) tmp[i] = true;
	for (unsigned int i = 0; i < gridSize; i++)
		grid.push_back(tmp);
	
	// First we chose a starting point between x = [1, gridSize-2] and y = 0
	start.first = Roll(1, gridSize-2);
	start.second = 0;
	grid[start.first][0] = false;

	// Make 1st tile north of start opened as well
	grid[start.first][1] = false;

	unsigned int prev_dir = 0; // The previous taken direction (0 = NORTH)

	std::stack< std::pair<unsigned int, unsigned int> > back_track; // used for back tracking
	back_track.push(std::pair<unsigned int, unsigned int>(start.first, 1));

	// Start creating a unique path
	unsigned int x = start.first;
	unsigned int y = 1;	
	while (y != gridSize-2)
	{
		unsigned int next_dir = Roll(0, 3);
		
		// We're gonna keep rolling until we find a direction that:
		// 1) Doesn't go back to where we came from
		// 2) Goes off the maze
		// 3) Doesn't rejoin a previous tile
		std::vector<unsigned int> tried; // if we see that we tried all 4 directions, then it's backtracking time
		while (true)
		{
			// Tried all dirs, need to back-track
			if (4 == tried.size())
			{
				back_track.pop();
				x = back_track.top().first;
				y = back_track.top().second;
				//OutputDebugMsg("Backtracking to: " + to_string(x) + ", " + to_string(y) + "\n");
				tried.clear();
				prev_dir = 99;
				continue;
			}

			// Find a dir we haven't tried
			while (true)
			{
				if ( std::find(tried.begin(), tried.end(), next_dir) == tried.end() )	
				{
					tried.push_back(next_dir);
					break;
				}
				else
				{
					next_dir += 1;
					if (next_dir > 3)
						next_dir = 0;
				}
			}				
			

			// Make sure we don't come back from where we just came from
			if (0 == next_dir && 2 == prev_dir)
				continue;
			else if (1 == next_dir && 3 == prev_dir)
				continue;
			else if (2 == next_dir && 0 == prev_dir)
				continue;
			else if (3 == next_dir && 1 == prev_dir)
				continue;


			// Make sure we don't go off the maze
			if (0 == next_dir) // NORTH
			{
				if (y == gridSize-2)
					continue;
			}
			else if (1 == next_dir) // WEST
			{
				if (x == 1)
					continue;
			}
			else if (2 == next_dir) // SOUTH
			{
				if (y == 1)
					continue;
			}
			else if (3 == next_dir) // EAST
			{
				if (x == gridSize-2)
					continue;
			}

			// Rejoin a previous tile
			if (0 == next_dir) // NORTH
			{
				if (!grid[x+1][y+1])
					continue;
				if (!grid[x-1][y+1])
					continue;
				if (!grid[x][y+2])
					continue;
				if (!grid[x][y+1])
					continue;
			}
			else if (1 == next_dir) // WEST
			{
				if (!grid[x-1][y-1])
					continue;
				if (!grid[x-1][y+1])
					continue;
				if (!grid[x-2][y])
					continue;
				if (!grid[x-1][y])
					continue;
			}
			else if (2 == next_dir) // SOUTH
			{
				if (!grid[x-1][y-1])
					continue;
				if (!grid[x+1][y-1])
					continue;
				if (!grid[x][y-2])
					continue;
				if (!grid[x][y-1])
					continue;
			}
			else if (3 == next_dir) // EAST
			{
				if (!grid[x+1][y-1])
					continue;
				if (!grid[x+1][y+1])
					continue;
				if (!grid[x+2][y])
					continue;
				if (!grid[x+1][y])
					continue;
			}

			break;
		}

		tried.clear();		
		prev_dir = next_dir;
			
		// Set next tile
		if (0 == next_dir) // NORTH
		{
			grid[x][y+1] = false;
			y += 1;
		}
		else if (1 == next_dir) // WEST
		{
			grid[x-1][y] = false;
			x -= 1;
		}
		else if (2 == next_dir) // SOUTH
		{
			grid[x][y-1] = false;			
			y -= 1;
		}
		else if (3 == next_dir) // EAST
		{
			grid[x+1][y] = false;
			x += 1;
		}		

		/*if (x == back_track.top().first && y == back_track.top().second)
			OutputDebugMsg("Repeating trail!!!!\n");*/
		back_track.push(std::pair<unsigned int, unsigned int>(x, y));
		//OutputDebugMsg(to_string(x) + ", " + to_string(y) + "\n");
	}

	// Randomly chose an open spot at y = gridSize-1 that will lead us out
	unsigned int rdm = Roll(0, gridSize-1);
	while (grid[rdm][gridSize-2])
	{
		++rdm;
		if (rdm >= gridSize)
			rdm = 0;
	}

	grid[x][gridSize-1] = false;
	goal.first = x; 	
	goal.second = gridSize-1;
}

int 
MazeRace::Roll(int min, int max)
{
	// x is in [0,1[
	double x = rand()/static_cast<double>(RAND_MAX); 

	// [0,1[ * (max - min) + min is in [min,max[
	int that = min + static_cast<int>( x * (max - min) );

	return that;
}

   

