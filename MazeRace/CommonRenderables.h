/// Commonly used renderable components such as bound box, floor grid, etc...
#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <vector>
#include "Components/Renderable.h"
#include "BaseApp.h"

extern BaseApp* gApp;

class FloorGridRenderable : public Renderable
{
public:    
	FloorGridRenderable(ID3D11Device* d3dDevice);
};

class BoundingBoxRenderable : public Renderable
{
public:    
	BoundingBoxRenderable(ID3D11Device* d3dDevice);
	BoundingBoxRenderable(ID3D11Device* d3dDevice, DirectX::BoundingBox const& aabb);
};

//class AxesRenderable : public Renderable
//{
//public:   
//	enum AxisName
//	{
//		X_AXIS,
//		Y_AXIS,
//		Z_AXIS,
//	};
//	/// entId used to relation the axes arrows to an actual entity
//	AxesRenderable(ID3D11Device* d3dDevice, unsigned int entId, AxisName const& axis, std::vector<DirectX::XMFLOAT3>* vertsOut = NULL);	
//};