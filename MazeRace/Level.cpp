#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Utils/ComponentFetches.h"
#include "BaseApp.h"
#include "Scene/ObjRenderable/ObjRenderable.old/ObjRenderable.h"
#include "Level.h"

extern std::wstring gResourcesDir;
extern BaseApp* gApp;

#define OBSTACLES_BATCH_ID	0
#define TRAIL_BATCH_ID		1

Level::Level(MAZE_GRID& obstacles, std::pair<unsigned int, unsigned int>& start, std::pair<unsigned int, unsigned int>& goal)
{
	mGrid = obstacles;
	mStart = start;
	mGoal = goal;

	InitSceneObjects();
}


Level::~Level(void)
{
	std::map< std::pair<unsigned int, unsigned int>, SceneObject* >::iterator iter = mSceneObjects.begin();
	for (; iter != mSceneObjects.end(); ++iter)
		if (iter->second)
			delete iter->second;

	mSceneObjects.clear();

	gApp->Renderer()->InstancingMngr()->FreeBatchBuffer(OBSTACLES_BATCH_ID);
	gApp->Renderer()->InstancingMngr()->FreeBatchBuffer(TRAIL_BATCH_ID);
}

void 
Level::InitSceneObjects()
{	
	// OBSTACLE INSTANCING BATCH CREATION
	std::vector<DirectX::XMFLOAT3> obs_verts_pos;
	std::vector<DirectX::XMFLOAT3> trail_verts_pos;
	{		
		std::wstring obstacle_model = gResourcesDir + L"MazeRace\\obstacleBlock.obj";
		std::shared_ptr<Renderable> renderable = std::make_shared<ObjRenderable>();
		
		std::string s(obstacle_model.begin(), obstacle_model.end());		
		// TODO:: unconst is ugly... fix?
		assert (static_cast<ObjRenderable*>(renderable.get())->LoadObj((char*)s.c_str(), gApp->Renderer()->Device(), &obs_verts_pos));
		
		std::wstring obstacle_diffuse = gResourcesDir + L"MazeRace\\concrete.dds";		
		s.assign(obstacle_diffuse.begin(), obstacle_diffuse.end());			
		std::shared_ptr<DiffuseMapped> diff_map = std::make_shared<DiffuseMapped>(DiffuseMapped((char*)s.c_str()));
		
		std::wstring obstacle_normal = gResourcesDir + L"MazeRace\\concrete_normal.dds";		
		s.assign(obstacle_normal.begin(), obstacle_normal.end());			
		std::shared_ptr<NormalMapped> normal_map = std::make_shared<NormalMapped>( NormalMapped((char*)s.c_str()));		

		D3D11_SAMPLER_DESC sampler_desc;
		sampler_desc.Filter = D3D11_FILTER_ANISOTROPIC;
		sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampler_desc.MipLODBias = 0.0f;
		sampler_desc.MaxAnisotropy = 4;
		sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		sampler_desc.BorderColor[0] = 0;
		sampler_desc.BorderColor[1] = 0;
		sampler_desc.BorderColor[2] = 0;
		sampler_desc.BorderColor[3] = 0;
		sampler_desc.MinLOD = 0;
		sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;

		diff_map->DiffuseMapSamplerDesc(sampler_desc);

		Material mat;
		mat.ambient = DirectX::XMFLOAT4(0.85f, 0.85f, 0.85f, 1.0f);
		mat.diffuse = DirectX::XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
		mat.specular = DirectX::XMFLOAT4(0.1f, 0.1f, 0.1f, 0.3f);
		renderable->MaterialProperties(mat);

		gApp->Renderer()->InstancingMngr()->CreateDynamicBatchBuffer(gApp->Renderer()->Device(), OBSTACLES_BATCH_ID, (unsigned int)(mGrid.size()*mGrid.size()), renderable, diff_map, normal_map, std::shared_ptr<ReflectionMapped>());
	}

	// TRAIL INSTANCING BATCH CREATION
	{		
		std::wstring trail_model = gResourcesDir + L"MazeRace\\trailTile.obj";
		std::shared_ptr<Renderable> renderable = std::make_shared<ObjRenderable>();

		std::string s(trail_model.begin(), trail_model.end());		
		// TODO:: unconst is ugly... fix?
		assert (static_cast<ObjRenderable*>(renderable.get())->LoadObj((char*)s.c_str(), gApp->Renderer()->Device(), &trail_verts_pos));

		
		std::wstring trail_diffuse = gResourcesDir + L"MazeRace\\trail.dds";
		s.assign(trail_diffuse.begin(), trail_diffuse.end());			
		std::shared_ptr<DiffuseMapped> diff_map = std::make_shared<DiffuseMapped>(DiffuseMapped((char*)s.c_str()));

		std::wstring trail_normal = gResourcesDir + L"MazeRace\\trail_normal.dds";
		s.assign(trail_normal.begin(), trail_normal.end());			
		std::shared_ptr<NormalMapped> normal_map = std::make_shared<NormalMapped>( NormalMapped((char*)s.c_str()));
		
		Material mat;		
		mat.ambient = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.0f);
		mat.diffuse = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.0f);
		mat.specular = DirectX::XMFLOAT4(0.25f, 0.25f, 0.25f, 0.05f);
		renderable->MaterialProperties(mat);

		gApp->Renderer()->InstancingMngr()->CreateDynamicBatchBuffer(gApp->Renderer()->Device(), TRAIL_BATCH_ID, (unsigned int)(mGrid.size()*mGrid.size()), renderable, diff_map, normal_map, std::shared_ptr<ReflectionMapped>());
	}
		

	for(unsigned int i = 0; i <  mGrid.size(); i++)
		for(unsigned int j = 0; j <  mGrid[i].size(); j++)
		{
			if ( mGrid[i][j])
			{	
				SceneObject* so = new SceneObject;

				Spatialized* spatialized = SpatializedFromEntity(so->EntityId());

				DirectX::XMFLOAT3 pos;
				pos.x = (MAZE_TILE_SIZE * i) - (MAZE_TILE_SIZE * (mGrid[i].size()-1))/2; 
				pos.y = (MAZE_TILE_SIZE * j) - (MAZE_TILE_SIZE * (mGrid[j].size()-1))/2; 
				pos.z = 100.f; //TODO:: calculate this depending on size of maze so that it fits the screen nicely

				spatialized->LocalPosition(pos);

				so->MakeGeometryInstanced(OBSTACLES_BATCH_ID, DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f), obs_verts_pos);

				so->ToggleBoundBox(false);
				
				mSceneObjects[std::pair<unsigned int, unsigned int>(i,j)] = so;
			}
			else
			{	
				SceneObject* so = new SceneObject;

				Spatialized* spatialized = SpatializedFromEntity(so->EntityId());

				DirectX::XMFLOAT3 pos;
				pos.x = (MAZE_TILE_SIZE * i) - (MAZE_TILE_SIZE * (mGrid[i].size()-1))/2; 
				pos.y = (MAZE_TILE_SIZE * j) - (MAZE_TILE_SIZE * (mGrid[j].size()-1))/2; 
				pos.z = 100.f; //TODO:: calculate this depending on size of maze so that it fits the screen nicely

				spatialized->LocalPosition(pos);
								
				if (mGoal.first == i && mGoal.second == j)
					so->MakeGeometryInstanced(TRAIL_BATCH_ID, DirectX::XMFLOAT4(0.001f, 1.f, 0.001f, 1.f), trail_verts_pos);
				else
					so->MakeGeometryInstanced(TRAIL_BATCH_ID, DirectX::XMFLOAT4(0.001f, 0.001f, 0.001f, 1.f), trail_verts_pos);

				so->ToggleBoundBox(false);

				mSceneObjects[std::pair<unsigned int, unsigned int>(i,j)] = so;
			}
		}
}

bool const 
Level::IsPositionAnObstacle( std::pair<unsigned int, unsigned int> const& pos )
{
	return mGrid[pos.first][pos.second];
}

void 
Level::MarkTrail( std::pair<unsigned int, unsigned int> const& pos )
{
	if ( !mGrid[pos.first][pos.second])
	{
		mSceneObjects[std::pair<unsigned int, unsigned int>(pos.first, pos.second)]->GetRenderable()->InstancedColor(DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	}
}
