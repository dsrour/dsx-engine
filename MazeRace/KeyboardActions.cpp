#include <windows.h>
#include <DirectXMath.h>
#include <string>
#include <sstream>
#include "BaseApp.h"
#include "MazeRace.h"
#include "KeyboardActions.h"

extern BaseApp* gApp;

ApplicationKeyboardActions::ApplicationKeyboardActions( std::shared_ptr<Camera> camera, std::shared_ptr<InputDeviceManager> inputDeviceManager ) : 
	InputDeviceActions(inputDeviceManager)
{
	mCamera = camera;
	mLastTime = 0.0;    
	mPlayer = NULL;
}

void 
ApplicationKeyboardActions::OnStateChange( void )
{
	std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
	if (!manager)
		return;

	if ( manager->Keyboard()->IsKeyDown(VK_ESCAPE) )
	{
		gApp->EndEngine();
	}

	double new_time = gApp->Timer().ElapsedTimeSecs();
	float delta = (float)new_time - (float)mLastTime;
	mLastTime = new_time;
		
	          
	if ( manager->Keyboard()->IsKeyPressed('W') )
	{        
		if ( manager->Keyboard()->IsKeyPressed(VK_SHIFT) )
			static_cast<FpCamera*>(mCamera.get())->Move(0.01f);  
		else
			static_cast<FpCamera*>(mCamera.get())->Move(0.001f);  
	}
	if ( manager->Keyboard()->IsKeyPressed('S') )
	{
		if ( manager->Keyboard()->IsKeyPressed(VK_SHIFT) )
			static_cast<FpCamera*>(mCamera.get())->Move(-0.01f);  
		else
			static_cast<FpCamera*>(mCamera.get())->Move(-0.001f);  
	}	

	if ( manager->Keyboard()->IsKeyPressed('L') )
	{
		static_cast<FpCamera*>(mCamera.get())->Yaw(0.0001f/DirectX::XM_PI);      
	}    

	if ( manager->Keyboard()->IsKeyPressed('J') )
	{
		static_cast<FpCamera*>(mCamera.get())->Yaw(-0.0001f/DirectX::XM_PI);      
	}   

	if ( manager->Keyboard()->IsKeyPressed('K') )
	{
		static_cast<FpCamera*>(mCamera.get())->Pitch(0.0001f/DirectX::XM_PI);      
	}    

	if ( manager->Keyboard()->IsKeyPressed('I') )
	{
		static_cast<FpCamera*>(mCamera.get())->Pitch(-0.0001f/DirectX::XM_PI);      
	} 

	if ( manager->Keyboard()->IsKeyDown(VK_RETURN) )
	{
		MazeRace* mr = static_cast<MazeRace*>(gApp);
		if (LEVEL_END_WIN == mr->CurrentGameState() || LEVEL_END_LOSE == mr->CurrentGameState())
			mr->SetGameState(LEVEL_START);
	} 

	if ( mPlayer && LEVEL_PLAY == static_cast<MazeRace*>(gApp)->CurrentGameState() )        
	{ 
		if ( manager->Keyboard()->IsKeyDown(VK_RIGHT) )
		{
			mPlayer->MoveEast();
			return;
		}    

		if ( manager->Keyboard()->IsKeyDown(VK_UP) )
		{
			mPlayer->MoveNorth();    
			return;
		}   

		if ( manager->Keyboard()->IsKeyDown(VK_LEFT) )
		{
			mPlayer->MoveWest();     
			return;
		}    

		if ( manager->Keyboard()->IsKeyDown(VK_DOWN) )
		{
			mPlayer->MoveSouth();
			return;
		} 
	}
}

void 
ApplicationKeyboardActions::SetPlayerToControl( Player* const player )
{
	mPlayer = player;
}
