#include "CommonRenderables.h"
#include "Scene/ObjRenderable/ObjRenderable.old/ObjRenderable.h"
#include "Debug/Debug.h"
#include "SceneObject.h"


SceneObject::SceneObject(void)
{
	mEntityId = gApp->EntityMngr()->CreateEntity();                  

	DirectX::BoundingBox aabb(DirectX::XMFLOAT3(0.f, 0.f, 0.f), DirectX::XMFLOAT3(0.5f,0.5f,0.5f));		
	std::shared_ptr<Bounded> bounded( new Bounded(aabb) );	
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, bounded);

	std::shared_ptr<Spatialized> spatialized( new Spatialized() );	
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, spatialized);

	mSoBoundsId = gApp->EntityMngr()->CreateEntity();    	
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, bounded);
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, spatialized);
	mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>( gApp->Renderer()->Device() );		
	BoundingRenderableColor(DirectX::XMFLOAT4(0.8f, 0.8f, 0.8f, 1.f));			
	ToggleBoundBox(true);

	mRenderableComp = NULL;
	mDiffuseMappedComp = NULL;
	mNormalMappedComp = NULL;
}


SceneObject::~SceneObject(void)
{	
	gApp->EntityMngr()->DestroyEntity(mSoBoundsId);
	gApp->EntityMngr()->DestroyEntity(mEntityId);	
}

void 
SceneObject::BoundingRenderableColor( DirectX::XMFLOAT4 const& color )
{
	Material mat;
	mat.diffuse = color;
	mSoBoundsRenderable->MaterialProperties(mat);
}

void 
SceneObject::SetGeometryFromFile(std::wstring& fileName, bool const& lit)
{
	// Remove renderable if there is one
	if (mRenderableComp != NULL)
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, Renderable::GetGuid());

	mRenderableComp = std::make_shared<ObjRenderable>();
	std::vector<DirectX::XMFLOAT3> verts_pos;
	std::string s(fileName.begin(), fileName.end());
	s.assign(fileName.begin(), fileName.end());	
	// TODO:: unconst is ugly... fix?
	if (!static_cast<ObjRenderable*>(mRenderableComp.get())->LoadObj((char*)s.c_str(), gApp->Renderer()->Device(), &verts_pos))
	{
		OutputDebugMsg("AttachMeshToEntityFromFile: bad file passed in: \n " + s + "\n");
		return; 
	}
	if (!lit)
		mRenderableComp->RenderableType(Renderable::FLAT);
	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mRenderableComp);	

	Material mat;
	mat.ambient = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f);
	mat.diffuse = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f);
	mat.specular = DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 0.f);
	mRenderableComp->MaterialProperties(mat);

	// Set new bound box	
	DirectX::BoundingBox aabb;
	DirectX::BoundingBox::CreateFromPoints(aabb, verts_pos.size(), &verts_pos[0], sizeof(DirectX::XMFLOAT3));
	std::shared_ptr<IComponent> tmp_comp;		
	gApp->EntityMngr()->GetComponentFromEntity(mEntityId, Bounded::GetGuid(), tmp_comp);
	if (tmp_comp)
	{
		Bounded* bounded = static_cast<Bounded*>(tmp_comp.get());
		bounded->AabbBounds(aabb);
	}	
	// Delete old bound renderable and give it a new one with new size	
	mat = mSoBoundsRenderable->MaterialProperties();
	mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>(gApp->Renderer()->Device(), aabb);	
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable);	
	mSoBoundsRenderable->MaterialProperties(mat);
}

void 
SceneObject::ToggleBoundBox( bool const& show )
{
	 show ? gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable) : gApp->EntityMngr()->RemoveComponentFromEntity(mSoBoundsId, Renderable::GetGuid());	 
}

void 
SceneObject::SetDiffuseMapFromFile( std::wstring& fileName )
{
	// Remove diffuse if there is one
	if (mDiffuseMappedComp != NULL)
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, DiffuseMapped::GetGuid());

	if (fileName.empty())
		return;
		  
	std::string s(fileName.begin(), fileName.end());
	s.assign(fileName.begin(), fileName.end());	
	// TODO:: unconst is ugly... fix?
	mDiffuseMappedComp = std::make_shared<DiffuseMapped>(DiffuseMapped((char*)s.c_str()));

	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mDiffuseMappedComp);
}

void 
SceneObject::SetNormalMapFromFile( std::wstring& fileName )
{
	// Remove diffuse if there is one
	if (mNormalMappedComp != NULL)
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, NormalMapped::GetGuid());

	if (fileName.empty())
		return;

	std::string s(fileName.begin(), fileName.end());
	s.assign(fileName.begin(), fileName.end());	
	// TODO:: unconst is ugly... fix?
	mNormalMappedComp = std::make_shared<NormalMapped>( NormalMapped((char*)s.c_str()));

	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mNormalMappedComp);
}

void 
SceneObject::SetMaterial( Material& mat )
{
	if (mRenderableComp)
		mRenderableComp->MaterialProperties(mat);
}

void
SceneObject::MakeGeometryInstanced(unsigned int const& batchId, DirectX::XMFLOAT4 const& instancedColor, std::vector<DirectX::XMFLOAT3>& verts)
{	
	// Remove renderable if there is one
	if (mRenderableComp != NULL)
		gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, Renderable::GetGuid());

	mRenderableComp = std::make_shared<Renderable>();

	mRenderableComp->DynamicInstancedBatchId(batchId);
	mRenderableComp->InstancedColor(instancedColor);

	gApp->EntityMngr()->AddComponentToEntity(mEntityId, mRenderableComp);	


	// Set new bound box	
	DirectX::BoundingBox aabb;
	DirectX::BoundingBox::CreateFromPoints(aabb, verts.size(), &verts[0], sizeof(DirectX::XMFLOAT3));
	std::shared_ptr<IComponent> tmp_comp;		
	gApp->EntityMngr()->GetComponentFromEntity(mEntityId, Bounded::GetGuid(), tmp_comp);
	if (tmp_comp)
	{
		Bounded* bounded = static_cast<Bounded*>(tmp_comp.get());
		bounded->AabbBounds(aabb);
	}	
	gApp->EntityMngr()->RemoveComponentFromEntity(mEntityId, Bounded::GetGuid()); // so we can bypass frustum culling.... 

	// Delete old bound renderable and give it a new one with new size	
	Material mat = mSoBoundsRenderable->MaterialProperties();
	mSoBoundsRenderable = std::make_shared<BoundingBoxRenderable>(gApp->Renderer()->Device(), aabb);	
	gApp->EntityMngr()->AddComponentToEntity(mSoBoundsId, mSoBoundsRenderable);	
	mSoBoundsRenderable->MaterialProperties(mat);
	
}
