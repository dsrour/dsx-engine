#include "BaseApp.h"
#include "Debug/Debug.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Scene/SceneObject.h"
#include "PostProcessPipeline.h"
#include "ParticlesApp.h"

// Systems
#include "Systems/Renderer/Pipelines/ForwardRenderer/ForwardRenderer.h"
#include "Systems/Renderer/Pipelines/ParticleRenderer/ParticleRenderer.h"

// Components
#include "Components/Renderable.h"
#include "Components/Spatialized.h"
#include "Components/Bounded.h"
#include "Components/DiffuseMapped.h"
#include "Components/ParticleEmitting.h"

BaseApp* gApp = new ParticlesApp;
extern std::wstring gResourcesDir;

TwBar*			ParticlesApp::mMenu = NULL;
unsigned int	ParticlesApp::mNumParticles = 32;
float			ParticlesApp::mSpawnColor[3] = { 1.f, 1.f, 1.f };
float			ParticlesApp::mDeathColor[3] = { 0.f, 0.f, 0.f };
float			ParticlesApp::mLifeTime = 5.f;
float			ParticlesApp::mParticleSize = 1.f;
float			ParticlesApp::mGravity = 100.f;
float			ParticlesApp::mRespawnDelay = 2.f;
float			ParticlesApp::mOrigPosX = 0.f;
float			ParticlesApp::mOrigPosY = 0.f;
float			ParticlesApp::mOrigPosZ = 0.f;
float			ParticlesApp::mSpawnDirection[3] = { 15.f, 30.f, -15.f };
float			ParticlesApp::mSpawnTimeOffset = 0.025f;
bool			ParticlesApp::mRecreateImitter = false;

float			ParticlesApp::mBoundsPosX = 0.f;
float			ParticlesApp::mBoundsPosY = 0.f;
float			ParticlesApp::mBoundsPosZ = 0.f;
float			ParticlesApp::mBoundsExtentsX = 25.f;
float			ParticlesApp::mBoundsExtentsY = 25.f;
float			ParticlesApp::mBoundsExtentsZ = 25.f;




ParticlesApp::ParticlesApp() 
{
    WinWidth(1888);
    WinHeight(992);
}

ParticlesApp::~ParticlesApp()
{
}

bool const
ParticlesApp::OnLoadApp()
{
	TitleName(L"DirectCompute Particle System Demo");

    Renderer()->ClearColor(0.f, 0.f, 0.f, 1.0f);

	// Setup the rendering pipeline

	std::shared_ptr<ForwardRenderer> frwrd_rndrer(new ForwardRenderer(gApp->Renderer()));
	mParticleRenderer = std::make_shared<ParticleRenderer>(gApp->Renderer());	
	std::shared_ptr<PostProcessPipeline> post_proc(new PostProcessPipeline(gApp->Renderer())); // to display fps

	std::list< std::shared_ptr<Pipeline> > path;
	path.push_back(frwrd_rndrer);
	path.push_back(post_proc);
	path.push_back(mParticleRenderer);		
	gApp->Renderer()->RenderPath(path);
	

	// Move camera backwards off the center of the scene
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Move(-70.0f); 
	

	// Create particle emitter entity//////////////////////////////////////////////////////////
	mParticleEmitterEntityId = gApp->EntityMngr()->CreateEntity();

	std::shared_ptr<Renderable> particles_renderable(new Renderable());
	particles_renderable->RenderableType(Renderable::PARTICLES);
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, particles_renderable);

	mParticlesSpatialized = std::make_shared<Spatialized>();
	mParticlesSpatialized->LocalPosition(DirectX::XMFLOAT3(0.f, 0.f, 0.f));
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, mParticlesSpatialized);

	mParticlesBounded = std::make_shared<Bounded>(DirectX::BoundingBox(DirectX::XMFLOAT3(0.f, 0.f, 0.f), DirectX::XMFLOAT3(25.f, 25.f, 25.f)));
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, mParticlesBounded);

	std::wstring wstr_texture_path = gResourcesDir + std::wstring(L"Particles\\particle.dds");
	std::shared_ptr<DiffuseMapped> particles_texture(new DiffuseMapped(std::string(wstr_texture_path.begin(), wstr_texture_path.end())));
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, particles_texture);

	mParticleEmittingComponent = std::make_shared<ParticleEmitting>(mNumParticles, gApp->Renderer());	
	mParticleEmittingComponent->ColorDie((DirectX::XMFLOAT3)mDeathColor);
	mParticleEmittingComponent->ColorSpawn((DirectX::XMFLOAT3)mSpawnColor);
	mParticleEmittingComponent->Gravity(mGravity);
	mParticleEmittingComponent->LifeTime(mLifeTime);
	mParticleEmittingComponent->ParticleSize(mParticleSize);
	mParticleEmittingComponent->RespawnDelay(mRespawnDelay);
	DirectX::XMFLOAT3 spawn_dir = (DirectX::XMFLOAT3)mSpawnDirection; spawn_dir.z *= -1.f;
	mParticleEmittingComponent->SpawnDirection(spawn_dir);
	mParticleEmittingComponent->SpawnTimeOffset(mSpawnTimeOffset);
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, mParticleEmittingComponent);
	///////////////////////////////////////////////////////////////////////////////////////////

	// Create bounds renderable ///////////////////////////////////////////////////////////////
	mBoundsSo = std::make_shared<SceneObject>();
	mBoundsSo->SetBounds(mParticlesBounded->AabbBounds());
	///////////////////////////////////////////////////////////////////////////////////////////

	// Setup menu /////////////////////////////////////////////////////////////////////////////
	mMenu = TwNewBar("Particle Emitter Menu");
	TwAddButton(mMenu, "Reset Particle Emitter", NULL, NULL, "");
	TwAddVarRW(mMenu, "SpawnTimeOffset", TW_TYPE_FLOAT, &mSpawnTimeOffset, "label = 'Spawn Time Offset' min=0.0 step=0.0025 precision=6");
	TwAddVarRW(mMenu,  "NumParticles", TW_TYPE_UINT32, &mNumParticles, "label='Number of Particles' min=32 max=2097120");
	TwAddButton(mMenu, "Diffuse Map", RecreateEmitter, NULL, " label='Init' ");

	TwAddButton(mMenu, "space1", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "Particle Emitter Settings", NULL, NULL, "");
	TwAddVarRW(mMenu, "SpawnColor", TW_TYPE_COLOR3F, &mSpawnColor[0], "label='Spawn Color'");
	TwAddVarRW(mMenu, "DeathColor", TW_TYPE_COLOR3F, &mDeathColor[0], "label='Death Color'");
	TwAddVarRW(mMenu, "LifeTime", TW_TYPE_FLOAT, &mLifeTime, "label='Life Time' min=0.0 step=0.5");
	TwAddVarRW(mMenu, "ParticleSize", TW_TYPE_FLOAT, &mParticleSize, "label='Particle Size' min=0.0 step=0.25");
	TwAddVarRW(mMenu, "Gravity", TW_TYPE_FLOAT, &mGravity, "label='Gravity'");
	TwAddVarRW(mMenu, "RespawnDelay", TW_TYPE_FLOAT, &mRespawnDelay, "label='Respawn Delay' min=0.0 ");
	TwAddVarRW(mMenu, "PosX", TW_TYPE_FLOAT, &mOrigPosX, "label='Origin X'");
	TwAddVarRW(mMenu, "PosY", TW_TYPE_FLOAT, &mOrigPosY, "label='Origin Y'");
	TwAddVarRW(mMenu, "PosZ", TW_TYPE_FLOAT, &mOrigPosZ, "label='Origin Z'");
	TwAddVarRW(mMenu, "SpawnDirection", TW_TYPE_DIR3F, &mSpawnDirection, "label = 'Spawn Direction'");

	TwAddButton(mMenu, "space2", NULL, NULL, "label = ' '");
	TwAddButton(mMenu, "Bounds", NULL, NULL, "");
	TwAddVarRW(mMenu, "BoundsPosX", TW_TYPE_FLOAT, &mBoundsPosX, "label='Position X' ");
	TwAddVarRW(mMenu, "BoundsPosY", TW_TYPE_FLOAT, &mBoundsPosY, "label='Position Y' ");
	TwAddVarRW(mMenu, "BoundsPosZ", TW_TYPE_FLOAT, &mBoundsPosZ, "label='Position Z' ");
	TwAddVarRW(mMenu, "BoundsExtentsX", TW_TYPE_FLOAT, &mBoundsExtentsX, "label='Extent X' min=1.0");
	TwAddVarRW(mMenu, "BoundsExtentsY", TW_TYPE_FLOAT, &mBoundsExtentsY, "label='Extent Y' min=1.0");
	TwAddVarRW(mMenu, "BoundsExtentsZ", TW_TYPE_FLOAT, &mBoundsExtentsZ, "label='Extent Z' min=1.0");

	TwDefine(" 'Particle Emitter Menu' size='350 650' valueswidth=180  ");
	///////////////////////////////////////////////////////////////////////////////////////////

	// User input
	mKeyboardSystem = std::make_shared<ApplicationKeyboardActions>(Renderer()->CurrentCamera(), InputDeviceMngr());
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardSystem);
	mMouseActions = std::make_shared<ApplicationMouseActions>(InputDeviceMngr());
	InputDeviceMngr()->Mouse()->AddMouseActions(mMouseActions);

    return true;
}

    
void
ParticlesApp::OnPreUpdate()
{        

}

void
ParticlesApp::OnPostUpdate()
{           
	if (mRecreateImitter)
	{
		mRecreateImitter = false;
		RecreateEmitter();
	}

	// Update vars
	mParticleEmittingComponent->ColorDie((DirectX::XMFLOAT3)mDeathColor);
	mParticleEmittingComponent->ColorSpawn((DirectX::XMFLOAT3)mSpawnColor);
	mParticleEmittingComponent->Gravity(mGravity);
	mParticleEmittingComponent->LifeTime(mLifeTime);
	mParticleEmittingComponent->ParticleSize(mParticleSize);
	mParticleEmittingComponent->RespawnDelay(mRespawnDelay);
	DirectX::XMFLOAT3 spawn_dir = (DirectX::XMFLOAT3)mSpawnDirection; spawn_dir.z *= -1.f;
	mParticleEmittingComponent->SpawnDirection(spawn_dir);
	mParticleEmittingComponent->SpawnTimeOffset(mSpawnTimeOffset);

	mParticlesSpatialized->LocalPosition(DirectX::XMFLOAT3(mOrigPosX, mOrigPosY, mOrigPosZ));
	mParticlesBounded->AabbBounds().Center = DirectX::XMFLOAT3(mBoundsPosX, mBoundsPosY, mBoundsPosZ);
	mParticlesBounded->AabbBounds().Extents = DirectX::XMFLOAT3(mBoundsExtentsX, mBoundsExtentsY, mBoundsExtentsZ);

	// Update bounds renderable
	mBoundsSo->SpatializedComponent()->LocalPosition(DirectX::XMFLOAT3(mBoundsPosX, mBoundsPosY, mBoundsPosZ));
	mBoundsSo->SpatializedComponent()->LocalScale(DirectX::XMFLOAT3(mBoundsExtentsX, mBoundsExtentsY, mBoundsExtentsZ));
	DirectX::BoundingBox aabb;
	aabb.Center = DirectX::XMFLOAT3(mBoundsPosX, mBoundsPosY, mBoundsPosZ);
	aabb.Extents = DirectX::XMFLOAT3(mBoundsExtentsX, mBoundsExtentsY, mBoundsExtentsZ);
	mBoundsSo->SetBounds(aabb);
}
    
void
ParticlesApp::OnUnloadApp()
{
       
}

void TW_CALL 
ParticlesApp::RecreateEmitter(void* /*clientData*/)
{
	mRecreateImitter = true;
}

void 
ParticlesApp::RecreateEmitter()
{
	gApp->EntityMngr()->RemoveComponentFromEntity(mParticleEmitterEntityId, ParticleEmitting::GetGuid());
	mParticleEmittingComponent = std::make_shared<ParticleEmitting>(mNumParticles, gApp->Renderer());
	gApp->EntityMngr()->AddComponentToEntity(mParticleEmitterEntityId, mParticleEmittingComponent);
	mParticleRenderer->Reset();
}

   

