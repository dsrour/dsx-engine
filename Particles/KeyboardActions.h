#pragma once

#include <windows.h>
#include <DirectXMath.h>

#include <string>
#include <sstream>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "InputDeviceManager.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Components/Spatialized.h"
#include "BaseApp.h"

extern BaseApp* gApp;

class ApplicationKeyboardActions : public InputDeviceActions
{
public:
    ApplicationKeyboardActions(std::shared_ptr<Camera> camera, std::shared_ptr<InputDeviceManager> inputDeviceManager) : 
        InputDeviceActions(inputDeviceManager)
    {
        mCamera = camera;
        mLastTime = 0.0;       
    }

    void
    SpatialComponentToControl(std::shared_ptr<Spatialized> spatial) 
    { 
        mSpatialized = spatial;         
    }

    void
    UpdateWinTitleWithSpatialPos(void) 
    { 
        if (mSpatialized)
        {
            DirectX::XMFLOAT3 pos = mSpatialized->LocalPosition(); 
            std::wstringstream wss_x;
            std::wstringstream wss_y;
            std::wstringstream wss_z;
            wss_x.precision(2);
            wss_y.precision(2);
            wss_z.precision(2);
            wss_x << pos.x;       
            wss_y << pos.y;
            wss_z << pos.z;
            std::wstring ws = L"Pos: {" + wss_x.str() + L", " + wss_y.str() + L", " + wss_z.str() + L"}";
            wchar_t const* title = ws.c_str();     
            gApp->TitleName(title);
        }
    }

    virtual void
    OnStateChange(void)
    {
        std::shared_ptr<InputDeviceManager> manager = mInputDeviceManager.lock();
        if (!manager)
            return;

        if ( manager->Keyboard()->IsKeyDown(VK_ESCAPE) )
        {
            gApp->EndEngine();
        }

		if (manager->Keyboard()->IsKeyUp('E'))
			manager->Mouse()->ToggleRelativeMode(!manager->Mouse()->RelativeMode());

        double new_time = gApp->Timer().ElapsedTimeSecs();
        float delta = (float)new_time - (float)mLastTime;
        mLastTime = new_time;

		float move_speed_per_sec = 85.f;

		if (manager->Keyboard()->IsKeyPressed(VK_SHIFT))
			move_speed_per_sec = 450.f;

        if (
            manager->Keyboard()->IsKeyDown('W') ||
            manager->Keyboard()->IsKeyDown('S') ||
            manager->Keyboard()->IsKeyDown('A') ||
            manager->Keyboard()->IsKeyDown('D') ||             
            manager->Keyboard()->IsKeyDown(VK_RIGHT) ||
            manager->Keyboard()->IsKeyDown(VK_UP)    ||
            manager->Keyboard()->IsKeyDown(VK_DOWN)  ||
            manager->Keyboard()->IsKeyDown(VK_LEFT) )             
        {
            mLastTime = gApp->Timer().ElapsedTimeSecs();
        }
        else
        {           
            if ( manager->Keyboard()->IsKeyPressed('W') )
            {        
                static_cast<FpCamera*>(mCamera.get())->Move(move_speed_per_sec, delta);  
            }
            if ( manager->Keyboard()->IsKeyPressed('S') )
            {
                static_cast<FpCamera*>(mCamera.get())->Move(-move_speed_per_sec, delta);  
            }
            if ( manager->Keyboard()->IsKeyPressed('A') )
            {        
                static_cast<FpCamera*>(mCamera.get())->Strafe(-move_speed_per_sec, delta);  
            }
            if ( manager->Keyboard()->IsKeyPressed('D') )
            {
                static_cast<FpCamera*>(mCamera.get())->Strafe(move_speed_per_sec, delta);  
            }

            if ( manager->Keyboard()->IsKeyPressed(VK_RIGHT) )
            {
                static_cast<FpCamera*>(mCamera.get())->Yaw(8.0f/DirectX::XM_PI, delta);      
            }    

            if ( manager->Keyboard()->IsKeyPressed(VK_LEFT) )
            {
                static_cast<FpCamera*>(mCamera.get())->Yaw(-8.0f/DirectX::XM_PI, delta);      
            }   

            if ( manager->Keyboard()->IsKeyPressed(VK_UP) )
            {
                static_cast<FpCamera*>(mCamera.get())->Pitch(8.0f/DirectX::XM_PI, delta);      
            }    

            if ( manager->Keyboard()->IsKeyPressed(VK_DOWN) )
            {
                static_cast<FpCamera*>(mCamera.get())->Pitch(-8.0f/DirectX::XM_PI, delta);      
            }             
        }
    }    

private:
    std::shared_ptr<Camera> mCamera;
    double mLastTime;  
    std::shared_ptr<Spatialized> mSpatialized;        
};