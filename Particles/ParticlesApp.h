#pragma once

#include <memory>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "KeyboardActions.h"
#include "MouseActions.h"
#include "BaseApp.h"

// Forward decs
class ParticleEmitting;
class ParticleRenderer;
class Spatialized;
class Bounded;
class SceneObject;

class ParticlesApp : public BaseApp
{
public:
    ParticlesApp(void);

    virtual
    ~ParticlesApp(void);

    bool const
    OnLoadApp(void);
    
    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);
        
    void
    OnUnloadApp(void);
   
private: 
	void 
	RecreateEmitter();

	std::shared_ptr<ApplicationKeyboardActions> mKeyboardSystem;
	std::shared_ptr<ApplicationMouseActions>	mMouseActions;

	std::shared_ptr<ParticleRenderer> mParticleRenderer; 
	std::shared_ptr<SceneObject> mBoundsSo;


	// Menu Callbacks //////////////////////////////////////////////
	static void TW_CALL
	RecreateEmitter(void* /*clientData*/);
	////////////////////////////////////////////////////////////////

	// Menu vars ///////////////////////////////////////////////////
	static TwBar*           mMenu;
	
	static unsigned int		mNumParticles; // has to be multiple of 32
	static float			mSpawnColor[3];
	static float			mDeathColor[3];
	static float			mLifeTime;
	static float			mParticleSize;
	static float			mGravity;
	static float			mRespawnDelay;
	static float			mOrigPosX;
	static float			mOrigPosY;
	static float			mOrigPosZ;
	static float			mSpawnDirection[3];
	static float			mSpawnTimeOffset;

	static float			mBoundsPosX;
	static float			mBoundsPosY;
	static float			mBoundsPosZ;
	static float			mBoundsExtentsX;
	static float			mBoundsExtentsY;
	static float			mBoundsExtentsZ;
	
	static bool				mRecreateImitter;
	////////////////////////////////////////////////////////////////

	// Particle Emitter Vars ///////////////////////////////////////
	unsigned int mParticleEmitterEntityId;	
	std::shared_ptr<ParticleEmitting> mParticleEmittingComponent;
	std::shared_ptr<Spatialized> mParticlesSpatialized;
	std::shared_ptr<Bounded> mParticlesBounded;
	////////////////////////////////////////////////////////////////
};

