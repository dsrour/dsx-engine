#include <sstream> 
#include <iostream>
#include <iomanip>
#include "BaseApp.h"
#include "PostProcessPipeline.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;


PostProcessPipeline::PostProcessPipeline( std::shared_ptr<D3dRenderer> renderer ) : Pipeline(renderer)
{
	mFpsSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	
	std::wstring font_path = gResourcesDir + L"\\Fonts\\font.spritefont";
	mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (WCHAR*)font_path.c_str());
}

PostProcessPipeline::~PostProcessPipeline( void )
{
	if (mSpriteFont)
		delete mSpriteFont;

	if (mFpsSprite)
		delete mFpsSprite;	
}

void 
PostProcessPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	// FPS
	{
		// Since this gets called every time we render... we just see how many frames we get per second
		static double elapsed = gApp->Timer().ElapsedTimeSecs();
		static unsigned int frames = 0;			
		frames++;
					
		static std::wstring fps_str;

		if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
		{
			std::wstringstream ss;   
			ss << frames; 
			fps_str = ss.str() + L" FPS";
			frames = 0;
			elapsed = gApp->Timer().ElapsedTimeSecs();
		}						
		mFpsSprite->Begin();
		mSpriteFont->DrawString(mFpsSprite, fps_str.c_str(), DirectX::XMFLOAT2(25.f, gApp->WinHeight() - 30.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
		mFpsSprite->End();
	}		
}
