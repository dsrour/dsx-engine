#pragma once

#include "Systems/Renderer/Renderer.h"

//#include <wrl/client.h>
//#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include <vector>
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"


class ShadowPenumbraEffect : public Effect
{
public:
    ShadowPenumbraEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~ShadowPenumbraEffect(void); 
	
	
	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	void
	UpdateShadowRays(
		unsigned int const numSamples,
		std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator const raysListSrvs,
		std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator const raysListUavs,
		std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator const rayVoxelIdsSrvs,
		std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator const rayVoxelIdsUavs,
		std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator const rayHitsSrvs,
		std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator const rayHitsUavs)
	{
		mNumSamples = numSamples;
		mRaysListSrvs = raysListSrvs;
		mRaysListUavs = raysListUavs;
		mRaysVoxelIdsSrvs = rayVoxelIdsSrvs;
		mRaysVoxelIdsUavs = rayVoxelIdsUavs;
		mRayHitsSrvs = rayHitsSrvs;
		mRayHitsUavs = rayHitsUavs;
	}

	void
	UpdateResolutionVar(unsigned int const x, unsigned int const y)
	{
		mDilationCbVars.resolution[0] = x;
		mDilationCbVars.resolution[1] = y;
	}

	void
	UpdateMaxDilationVar(int dilation)
	{
		if (dilation < 0)
			dilation = 0;

		mDilationCbVars.maxDilation = dilation;
	}

	void
	UpdateDispatchParams(int const x, int const y, int const z);

	void
	UpdateCurrentSampleVar(int const currSample) { mDilationCbVars.currSample = currSample; }

	void
	UpdateTotalSamplesVar(int const totalSamples) { mResolveCbVars.totalSamples = totalSamples; }

	void
	UpdateDirectionalLightSizeVar(float const lightSize) { mDilationCbVars.lightSize = lightSize; }

	void 
	UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX const& vp);

	void
	UpdateGenRaysSampler(ID3D11SamplerState* const sampler) { mGenRaysSampler = sampler; }

	void
	UpdateResolveSampler(ID3D11SamplerState* const sampler) { mResolveSampler = sampler; }

	void
	UpdateMipMapLevel(unsigned int const lod) { mGenRaysCbVars.lod = lod; }

	void
	UpdateMipMapResolution(unsigned int const w, unsigned int const h) 
	{ 
		mGenRaysCbVars.res[0] = w;
		mGenRaysCbVars.res[1] = h;
	}

	void
	UpdateLightDirection(DirectX::XMFLOAT3 const& dir) { mGenRaysCbVars.lightDirection = dir; }

	void
	UpdateNumVoxelsPerLength(unsigned int const numVoxels) { mGenRaysCbVars.numVoxelsPerLength = numVoxels; }
	
	void
	UpdateSimGridCenterAndVoxelSizePerLength(DirectX::XMFLOAT4 const& val) { mGenRaysCbVars.simGridCenterAndVoxelSizePerLength = val; }

	void 
	UpdatePosShadowIntensityInputSrv(ID3D11ShaderResourceView* const val) { mPosShadowIntensityInputSrv = val; }

	void
	UpdateComputedSamplesVar(unsigned int const numCompSamples) { mResolveCbVars.computedSamples = numCompSamples; }

	void
	UpdateLightShadowIntensityVar(float const intensity) { mResolveCbVars.lightShadowIntensity = intensity; }

private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);
	
	Microsoft::WRL::ComPtr<ID3DBlob>			mGenRaysBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mGenRaysCs;

	Microsoft::WRL::ComPtr<ID3DBlob>			mClearDilationMaskBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mClearDilationMaskCs;

	Microsoft::WRL::ComPtr<ID3DBlob>			mGenDilationMaskBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mGenDilationMaskCs;

	Microsoft::WRL::ComPtr<ID3DBlob>			mApplyDilationMaskBlob;
	Microsoft::WRL::ComPtr<ID3D11ComputeShader> mApplyDilationMaskCs;

	Microsoft::WRL::ComPtr<ID3DBlob>			mResolveVsBlob;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>  mResolveVs;

	Microsoft::WRL::ComPtr<ID3DBlob>			mResolve4PsBlob;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	mResolve4Ps;

	Microsoft::WRL::ComPtr<ID3DBlob>			mResolve8PsBlob;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	mResolve8Ps;

	Microsoft::WRL::ComPtr<ID3DBlob>			mResolve16PsBlob;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	mResolve16Ps;

	struct DilationResources
	{
		D3D11_BUFFER_DESC									dilationMaskBufferDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer>				dilationMaskBuffer;

		D3D11_UNORDERED_ACCESS_VIEW_DESC					dilationMaskUavDesc;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>	dilationMaskUav;

		D3D11_SHADER_RESOURCE_VIEW_DESC						dilationMaskSrvDesc;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>	dilationMaskSrv;
	} mDilationResources;
	
	D3dRenderer* mRendererRef; /// Kept for recompile() func

	unsigned int mNumSamples;
	std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator     mRaysListSrvs;
	std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator	mRaysListUavs;
	std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator		mRaysVoxelIdsSrvs;
	std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator    mRaysVoxelIdsUavs;
	std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator		mRayHitsSrvs;
	std::vector<Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>>::iterator    mRayHitsUavs;

	struct DilationCb
	{	
		int dispatchParams[3];
		int maxDilation;
		int resolution[2];
		int currSample;
		float lightSize;
		DirectX::XMMATRIX vpMat;
	} mDilationCbVars;
	DirectX::ConstantBuffer< DilationCb > mDilationCb;

	struct ResolveCb
	{
		int totalSamples;
		int computedSamples;
		float lightShadowIntensity;
		int pad;
	} mResolveCbVars;
	DirectX::ConstantBuffer< ResolveCb > mResolveCb;

	struct GenRaysCb
	{
		int					lod;
		int					res[2];
		int					pad;
		DirectX::XMFLOAT3 	lightDirection;
		int					numVoxelsPerLength;
		DirectX::XMFLOAT4	simGridCenterAndVoxelSizePerLength;
	} mGenRaysCbVars;
	DirectX::ConstantBuffer< GenRaysCb > mGenRaysCb;

	ID3D11SamplerState*			 mGenRaysSampler;
	ID3D11SamplerState*			 mResolveSampler;

	ID3D11ShaderResourceView*	mPosShadowIntensityInputSrv;
};