//#include <assert.h>
//#include "Debug/Debug.h"
//#include "Systems/Renderer/Shaders/ShaderDefines.h"
//#include "DebugEffect.h"
//
//extern std::wstring gEngineRootDir;
//
//DebugEffect::DebugEffect(std::shared_ptr<D3dRenderer> renderer)
//{
//	mEffect = nullptr;	
//
//	mRendererRef = renderer.get(); 
//	Init(mRendererRef);
//}
//
///*virtual*/
//DebugEffect::~DebugEffect()
//{    
//    
//}
//
//void
//DebugEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
//{
//	// Compile effect
//	std::wstring fx_path = gEngineRootDir+ L"Source\\Systems\\Renderer\\Shaders\\Debug\\Debug.fx";
//	std::wstring cso_name = L"Debug.cso";
//	bool success = false;
//	if (!silentFail)
//	{	
//		if (!forceCompile)
//			success = CreateEffectFromFile((WCHAR*)cso_name.c_str(), renderer->Device());
//		if (!success)
//			success = CreateEffectFromFile((WCHAR*)fx_path.c_str(), renderer->Device());
//
//		assert(success);
//	}
//	else
//	{
//		if (!forceCompile)
//			success = CreateEffectFromFile((WCHAR*)cso_name.c_str(), renderer->Device());
//		if (!success)
//			success = CreateEffectFromFile((WCHAR*)fx_path.c_str(), renderer->Device());
//
//		if (!success)
//			return;
//	}
//	
//	// Init shader variables
//	mNumVoxelsPerLength = mEffect->GetVariableByName("gNumVoxelsPerLength")->AsScalar();
//	mSimGridCenterAndVoxelSizePerLength = mEffect->GetVariableByName("gSimGridCenterAndVoxelSizePerLength")->AsVector();
//	mBinaryGridSrv = mEffect->GetVariableByName("gBinaryVoxelGridIn")->AsShaderResource();
//	mOccupiedVoxelsUav = mEffect->GetVariableByName("gOccupiedVoxelsOut")->AsUnorderedAccessView();
//	mDrawIndInstArgsUav = mEffect->GetVariableByName("gDrawIndexedInstancedArgs")->AsUnorderedAccessView();
//	mOccupiedVoxels = mEffect->GetVariableByName("gOccupiedVoxelsIn")->AsShaderResource();
//	mViewProj = mEffect->GetVariableByName("gViewProj")->AsMatrix();
//
//
//	{
//		D3DX11_PASS_DESC pass_desc;
//
//		// Create input layout descs
//		std::pair<LPCSTR, LPCSTR> voxel_dbg_grid_tech_p1(DEBUG_VOXEL_GRID_TECH, "P1");
//		
//
//		std::list<InputLayoutManager::SubInputLayout> input_layout_desc;
//
//		input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
//		mInputLayoutDescs[voxel_dbg_grid_tech_p1] = input_layout_desc;
//
//		ZeroMemory(&pass_desc, sizeof(D3DX11_PASS_DESC));
//		mEffect->GetTechniqueByName(voxel_dbg_grid_tech_p1.first)->GetPassByName(voxel_dbg_grid_tech_p1.second)->GetDesc(&pass_desc);
//		mInputLayouts[voxel_dbg_grid_tech_p1] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, pass_desc, renderer->Device());
//	}
//}
//
//void 
//DebugEffect::UpdateVoxelGridBuffers(
//	ID3D11ShaderResourceView* const binaryVoxelGridSrv, 
//	ID3D11UnorderedAccessView* const occupiedVoxelsUav, 
//	ID3D11ShaderResourceView* const occupiedVoxelsSrv)
//{
//	assert(mOccupiedVoxelsUav->IsValid());	
//	assert(SUCCEEDED(mOccupiedVoxelsUav->SetUnorderedAccessView(occupiedVoxelsUav)));
//
//	assert(mBinaryGridSrv->IsValid());
//	assert(SUCCEEDED(mBinaryGridSrv->SetResource(binaryVoxelGridSrv)));
//
//	assert(mOccupiedVoxels->IsValid());
//	assert(SUCCEEDED(mOccupiedVoxels->SetResource(occupiedVoxelsSrv)));
//}
//
//void
//DebugEffect::UpdateCommonBuffers(ID3D11UnorderedAccessView* const drawInstIndArgsBuff)
//{
//	assert(mDrawIndInstArgsUav->IsValid());
//	assert(SUCCEEDED(mDrawIndInstArgsUav->SetUnorderedAccessView(drawInstIndArgsBuff)));
//}
//
//void 
//DebugEffect::UpdateVoxelGridConstantVariables(VoxelgridConstants& vars)
//{
//	assert(mNumVoxelsPerLength->IsValid());
//	assert(SUCCEEDED(mNumVoxelsPerLength->SetInt(vars.numVoxelsPerLength)));
//
//	assert(mSimGridCenterAndVoxelSizePerLength->IsValid());
//	assert(SUCCEEDED(mSimGridCenterAndVoxelSizePerLength->SetFloatVector(reinterpret_cast<float*>(&vars.simGridCenterAndVoxelSizePerLength))));
//}
//
//void
//DebugEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& mat)
//{
//	assert(mViewProj->IsValid());
//	assert(SUCCEEDED(mViewProj->SetMatrix(reinterpret_cast<float*>(&mat))));
//}