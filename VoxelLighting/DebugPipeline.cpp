//#include <algorithm>
//#include <assert.h>
//#include "Debug/Debug.h"
//#include "CommonRenderables/CubeRenderable.h"
//#include "Systems/Renderer/Renderer.h"
//#include "BaseApp.h"
//#include "Systems/Renderer/Shaders/ShaderDefines.h"
//#include "HybridPipeline.h"
//#include "DebugPipeline.h"
//
//extern BaseApp* gApp;
//
//DebugPipeline::DebugPipeline(std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EntityManager> entityManager) : Pipeline(renderer)
//{
//	mVoxLightPln = nullptr;
//
//	HRESULT hr;
//
//	// Init common resources /////////////////////////////////////
//	ZeroMemory(&mIndDrawArgsDesc, sizeof(mIndDrawArgsDesc));
//	mIndDrawArgsDesc.ByteWidth = sizeof(UINT)* 5;
//	mIndDrawArgsDesc.Usage = D3D11_USAGE_DEFAULT;
//	mIndDrawArgsDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
//	mIndDrawArgsDesc.CPUAccessFlags = 0;
//	mIndDrawArgsDesc.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
//	mIndDrawArgsDesc.StructureByteStride = sizeof(float);
//	hr = renderer->Device()->CreateBuffer(&mIndDrawArgsDesc, NULL, &mIndDrawArgsBuff);
//	assert(SUCCEEDED(hr));
//
//	ZeroMemory(&mIndDrawArgsUavDesc, sizeof(mIndDrawArgsUavDesc));
//	mIndDrawArgsUavDesc.Format = DXGI_FORMAT_R32_UINT;
//	mIndDrawArgsUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
//	mIndDrawArgsUavDesc.Buffer.FirstElement = 0;
//	mIndDrawArgsUavDesc.Buffer.Flags = 0;
//	mIndDrawArgsUavDesc.Buffer.NumElements = 5;
//	hr = renderer->Device()->CreateUnorderedAccessView((ID3D11Resource *)mIndDrawArgsBuff, &mIndDrawArgsUavDesc, &mIndDrawArgsUav);
//	assert(SUCCEEDED(hr));
//
//	mOccupiedVoxelsBuff = nullptr;
//	mOccupiedVoxelsUav = nullptr;
//	mOccupiedVoxelsSrv = nullptr;
//
//	mDebugEffect = std::make_shared<DebugEffect>(renderer);
//}
//
//
//DebugPipeline::~DebugPipeline()
//{
//	ReleaseCommonResources();
//	ReleaseVoxelGridResources();
//}
//
//void 
//DebugPipeline::MadeActive(void)
//{
//
//}
//
//void 
//DebugPipeline::MadeInactive(void)
//{
//
//}
//
//void 
//DebugPipeline::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime)
//{	
//	if (mVoxLightPln)
//		DebugVoxelLightPipeline();
//}
//
//void 
//DebugPipeline::RecompileShaders()
//{
//	mDebugEffect->Recompile();
//}
//
//void 
//DebugPipeline::UnbindResources(std::shared_ptr<D3dRenderer> renderer, std::string const& passName)
//{
//
//}
//
//void 
//DebugPipeline::DebugVoxelLightingPipeline(std::shared_ptr<HybridPipeline> voxelLightPipeline)
//{
//	mVoxLightPln = voxelLightPipeline;
//
//	// Clear resources
//	ReleaseVoxelGridResources();
//
//	// Create resources
//	HRESULT hr;
//
//	/* NOTE: 
//	 * This is a hard coded max size.
//	 * Note it is less than the actual amount of voxels in the grid.  The reason for this is
//	 * to allow the graphics debugger to run.  Having 512^3 elements causes the debugger to hang
//	 * while capturing the frame.  This is reasonable though since more than 1/2 voxels will be culled.
//	 */
//	unsigned int const elem_count = 256*256*256; //512*512*512; 
//
//	ZeroMemory(&mOccupiedVoxelsDesc, sizeof(mOccupiedVoxelsDesc));	
//	mOccupiedVoxelsDesc.ByteWidth = sizeof(unsigned int)* elem_count;
//	mOccupiedVoxelsDesc.Usage = D3D11_USAGE_DEFAULT;
//	mOccupiedVoxelsDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
//	mOccupiedVoxelsDesc.CPUAccessFlags = 0;
//	mOccupiedVoxelsDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
//	mOccupiedVoxelsDesc.StructureByteStride = sizeof(unsigned int);
//	hr = gApp->Renderer()->Device()->CreateBuffer(&mOccupiedVoxelsDesc, NULL, &mOccupiedVoxelsBuff);
//	assert(SUCCEEDED(hr));
//
//	ZeroMemory(&mOccupiedVoxelsUavDesc, sizeof(mOccupiedVoxelsUavDesc));
//	mOccupiedVoxelsUavDesc.Format = DXGI_FORMAT_UNKNOWN;
//	mOccupiedVoxelsUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
//	mOccupiedVoxelsUavDesc.Buffer.FirstElement = 0;
//	mOccupiedVoxelsUavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_APPEND;
//	mOccupiedVoxelsUavDesc.Buffer.NumElements = elem_count;
//	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mOccupiedVoxelsBuff, &mOccupiedVoxelsUavDesc, &mOccupiedVoxelsUav);
//	assert(SUCCEEDED(hr));
//		
//	ZeroMemory(&mOccupiedVoxelsSrvDesc, sizeof(mOccupiedVoxelsSrvDesc));
//	mOccupiedVoxelsSrvDesc.Format = DXGI_FORMAT_UNKNOWN;
//	mOccupiedVoxelsSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
//	mOccupiedVoxelsSrvDesc.Buffer.FirstElement = 0;
//	mOccupiedVoxelsSrvDesc.Buffer.NumElements = elem_count;
//	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mOccupiedVoxelsBuff, &mOccupiedVoxelsSrvDesc, &mOccupiedVoxelsSrv);
//	assert(SUCCEEDED(hr));
//}
//
//void 
//DebugPipeline::DebugVoxelLightPipeline()
//{
//	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
//	if (!renderer)
//		return;
//
//	DebugEffect::VoxelgridConstants consts{ renderer->CurrentCamera()->Position(), mVoxLightPln->NumVoxelsPerLength() };
//	consts.simGridCenterAndVoxelSizePerLength.w = mVoxLightPln->VoxelLength();
//	mDebugEffect->UpdateVoxelGridConstantVariables(consts);
//	
//	mDebugEffect->UpdateVoxelGridBuffers(mVoxLightPln->BinaryVoxelGridSrv(), nullptr, nullptr);
//	mDebugEffect->Apply(DEBUG_VOXEL_GRID_TECH, "P0", renderer->DeviceContext());
//	
//
//	// NOTE:: Gotta do goon shit since the fx framework makes it impossible to set the initial count offset on the append buffer
//	// TODO:: modify (or get rid) of the fx framework	
//	ID3D11UnorderedAccessView*  uavs[2] = { mIndDrawArgsUav, mOccupiedVoxelsUav };
//	unsigned int counts[2] = { 0, 0 };
//	renderer->DeviceContext()->CSSetUnorderedAccessViews(0, 2, uavs, counts); (D3D11_KEEP_RENDER_TARGETS_AND_DEPTH_STENCIL, NULL, NULL, 0, 2, uavs, counts);
//
//	renderer->DeviceContext()->Dispatch(mVoxLightPln->NumVoxelsPerLength(), mVoxLightPln->NumVoxelsPerLength(), 1);
//
//	renderer->SetBackBufferRenderTarget();
//
//	// Set raster state
//	ID3D11RasterizerState* raster_state = renderer->StateMngr()->RasterizerState(RenderStateManager::Wireframe);
//	renderer->DeviceContext()->RSSetState(raster_state);
//
//	ID3D11BlendState* state = NULL;
//	renderer->DeviceContext()->OMSetBlendState(state, NULL, 0xffffffff);		
//	renderer->DeviceContext()->OMSetDepthStencilState(NULL, NULL);
//
//
//	// Input layout
//	renderer->DeviceContext()->IASetInputLayout(mDebugEffect->InputLayout(DEBUG_VOXEL_GRID_TECH, "P1"));
//	auto input_layout_desc = mDebugEffect->InputLayoutDescription(DEBUG_VOXEL_GRID_TECH, "P1");
//	assert(!input_layout_desc.empty());
//
//	// Index buffer
//	static std::shared_ptr<CubeRenderable> cube_rndrbl(new CubeRenderable(renderer->Device(), 0.5f, true));
//	renderer->DeviceContext()->IASetIndexBuffer(
//		cube_rndrbl->IndexBuffer().indexBuffer,
//		cube_rndrbl->IndexBuffer().indexBufferFormat,
//		cube_rndrbl->IndexBuffer().indexBufferOffset);
//	
//	// Set primitive topology to point list
//	renderer->DeviceContext()->IASetPrimitiveTopology(cube_rndrbl->PrimitiveTopology());
//
//	// Go through needed subinputs and accumulate vertex buffers
//	std::vector<ID3D11Buffer*> buffers;
//	std::vector<unsigned int> strides;
//	std::vector<unsigned int> offsets;
//
//	unsigned int vert_count = 0;
//
//	std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();
//	for (sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++)
//	{
//		buffers.push_back(cube_rndrbl->VertexBuffer(*sub_input_iter).vertexBuffer);
//		strides.push_back(cube_rndrbl->VertexBuffer(*sub_input_iter).stride);
//		offsets.push_back(cube_rndrbl->VertexBuffer(*sub_input_iter).offset);
//
//		vert_count += cube_rndrbl->VertexBuffer(*sub_input_iter).vertexCount;
//	}
//	renderer->DeviceContext()->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);
//
//	mDebugEffect->UpdateVoxelGridBuffers(nullptr, nullptr, mOccupiedVoxelsSrv);
//
//	// Update View Projection matrix variable
//	DirectX::XMMATRIX proj = XMLoadFloat4x4(&(renderer->CurrentCamera()->ProjMatrix()));
//	DirectX::XMMATRIX view = XMLoadFloat4x4(&(renderer->CurrentCamera()->ViewMatrix()));
//	DirectX::XMMATRIX vp = view * proj;
//	mDebugEffect->UpdateViewProjectionMatrixVariable(vp);
//
//	mDebugEffect->Apply(DEBUG_VOXEL_GRID_TECH, "P0", renderer->DeviceContext());
//
//	mDebugEffect->UpdateVoxelGridConstantVariables(consts);
//
//	mDebugEffect->Apply(DEBUG_VOXEL_GRID_TECH, "P1", renderer->DeviceContext());
//	renderer->DeviceContext()->DrawIndexedInstancedIndirect(mIndDrawArgsBuff, 0);
//
//	// Unbind resources
//	mDebugEffect->UpdateCommonBuffers(nullptr);
//	mDebugEffect->UpdateVoxelGridBuffers(nullptr, nullptr, nullptr);
//	mDebugEffect->Apply(DEBUG_VOXEL_GRID_TECH, "P0", renderer->DeviceContext());
//	mDebugEffect->Apply(DEBUG_VOXEL_GRID_TECH, "P1", renderer->DeviceContext());
//}
//
//void
//DebugPipeline::ReleaseCommonResources()
//{
//	if (mIndDrawArgsUav)
//	{
//		mIndDrawArgsUav->Release();
//		mIndDrawArgsUav = nullptr;
//	}
//
//	if (mIndDrawArgsBuff)
//	{
//		mIndDrawArgsBuff->Release();
//		mIndDrawArgsBuff = nullptr;
//	}
//}
//
//void
//DebugPipeline::ReleaseVoxelGridResources()
//{
//	if (mOccupiedVoxelsSrv)
//	{
//		mOccupiedVoxelsSrv->Release();
//		mOccupiedVoxelsSrv = nullptr;
//	}
//	if (mOccupiedVoxelsUav)
//	{
//		mOccupiedVoxelsUav->Release();
//		mOccupiedVoxelsUav = nullptr;
//	}
//	if (mOccupiedVoxelsBuff)
//	{
//		mOccupiedVoxelsBuff->Release();
//		mOccupiedVoxelsBuff = nullptr;
//	}
//}
//
