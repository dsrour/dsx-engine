#include <sstream> 
#include <iostream>
#include <iomanip>
#include "BaseApp.h"
#include "PostProcessPipeline.h"

extern BaseApp* gApp;
extern std::wstring gResourcesDir;


PostProcessPipeline::PostProcessPipeline( std::shared_ptr<D3dRenderer> renderer ) : Pipeline(renderer)
{
	mFpsSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mFrameProfileSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	mMsgBroadcastSprite = new DirectX::SpriteBatch(gApp->Renderer()->DeviceContext());
	
	std::wstring font_path = gResourcesDir + L"Fonts\\font.spritefont";
	mSpriteFont = new DirectX::SpriteFont(gApp->Renderer()->Device(), (WCHAR*)font_path.c_str());

	mMsgBroadcast.msg = L"";
}

PostProcessPipeline::~PostProcessPipeline( void )
{
	if (mMsgBroadcastSprite)
		delete mMsgBroadcastSprite; 

	if (mSpriteFont)
		delete mSpriteFont;

	if (mFpsSprite)
		delete mFpsSprite;	

	if (mFrameProfileSprite)
		delete mFrameProfileSprite;
}

void 
PostProcessPipeline::EnterPipeline( std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime )
{
	auto dev_c = gApp->Renderer()->DeviceContext();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Post Process Text", 0);

	// FPS
	{
		// Since this gets called every time we render... we just see how many frames we get per second
		static double elapsed = gApp->Timer().ElapsedTimeSecs();
		static unsigned int frames = 0;			
		frames++;
					
		static std::wstring fps_str;

		if (gApp->Timer().ElapsedTimeSecs() - elapsed >= 1.0)
		{
			std::wstringstream ss;   
			ss << frames; 
			fps_str = ss.str() + L" FPS";
			frames = 0;
			elapsed = gApp->Timer().ElapsedTimeSecs();
		}						
		mFpsSprite->Begin();
		mSpriteFont->DrawString(mFpsSprite, fps_str.c_str(), DirectX::XMFLOAT2(25.f, gApp->WinHeight() - 30.f), DirectX::XMVectorSet(0.75f,0.25f,0.25f,1.f));
		mFpsSprite->End();
	}		

	// Frame Profiler
	if (!mFrameProfileStr.empty())
	{
		mFrameProfileSprite->Begin();
		mSpriteFont->DrawString(mFrameProfileSprite, mFrameProfileStr.c_str(), DirectX::XMFLOAT2(25.f, gApp->WinHeight() - 575.f), DirectX::XMVectorSet(1.f, 1.f, 1.f, 1.f));
		mFrameProfileSprite->End();
	}

	// Msg broadcast	
	static double last_time = gApp->Timer().ElapsedTimeSecs();
	double delta = gApp->Timer().ElapsedTimeSecs() - last_time;
	if (!mMsgBroadcast.msg.empty() && mMsgBroadcast.duration > 0.0)
	{
		mMsgBroadcast.duration -= delta;

		mMsgBroadcastSprite->Begin();
		mSpriteFont->DrawString(mMsgBroadcastSprite, mMsgBroadcast.msg.c_str(), mMsgBroadcast.pos, DirectX::XMVectorSet(mMsgBroadcast.color.x, mMsgBroadcast.color.y, mMsgBroadcast.color.z, 1.f));
		mMsgBroadcastSprite->End();
	}
	else
	{
		mMsgBroadcast.duration = -1.0;
		mMsgBroadcast.msg = L"";
	}

	last_time = gApp->Timer().ElapsedTimeSecs();

	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();
}
