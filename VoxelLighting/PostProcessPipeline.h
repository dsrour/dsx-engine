#pragma once

#include <DirectXMath.h>
#include "Systems/Renderer/DirectXTK/Inc/SpriteFont.h"
#include "Systems/Renderer/Pipeline.h"


class PostProcessPipeline : public Pipeline
{
public:
	PostProcessPipeline(std::shared_ptr<D3dRenderer> renderer);
    
    virtual 
    ~PostProcessPipeline(void);
          
    virtual void
    MadeActive(void) {}

    virtual void
    MadeInactive(void) {}

	virtual void
	RecompileShaders() { /*no shaders*/ }

    virtual void
    EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);

	void
	SetProfileString(std::wstring& str) {mFrameProfileStr = str;}

	struct Message
	{
		std::wstring msg;
		DirectX::XMFLOAT2 pos; 
		DirectX::XMFLOAT3 color;
		double	duration;
	};
	void
	BroadcastMessage(Message const& msg) { mMsgBroadcast  = msg; }
        
private:	
	DirectX::SpriteFont*	  mSpriteFont;   
	DirectX::SpriteBatch*	  mFpsSprite;
	DirectX::SpriteBatch*	  mFrameProfileSprite;
	std::wstring mFrameProfileStr;
	
	DirectX::SpriteBatch*	  mMsgBroadcastSprite;
	Message mMsgBroadcast;
};