#pragma once

#include <assert.h>
#include <windows.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "Components/Renderable.h"
#include "Component.hpp"

class Voxelized : public Component<Voxelized>
{
public:
	Voxelized() 
	{ 
		mNumTriangles = 0;
		mTrianglesBuffer = nullptr;
		mTrianglesSrv = nullptr;
	}
		
	~Voxelized()
	{
		if (mTrianglesSrv)
			mTrianglesSrv->Release();

		if (mTrianglesBuffer)
			mTrianglesBuffer->Release();
	}

	void
	InitFromTriangleList(std::vector<DirectX::XMFLOAT3> const& mTris, std::shared_ptr<D3dRenderer> renderer);

	ID3D11ShaderResourceView* const
	TrianglesSrv() const {return mTrianglesSrv;}

	unsigned int const
	NumTriangles(void) const { return mNumTriangles; }

private:
	D3D11_BUFFER_DESC				mTrianglesBuffDesc;
	ID3D11Buffer*					mTrianglesBuffer;
	D3D11_SHADER_RESOURCE_VIEW_DESC mTrianglesSrvDesc;
	ID3D11ShaderResourceView*		mTrianglesSrv;	
	unsigned int					mNumTriangles;
};

inline void 
Voxelized::InitFromTriangleList(std::vector<DirectX::XMFLOAT3> const& mTris, std::shared_ptr<D3dRenderer> renderer)
{
	unsigned int size = (unsigned int)mTris.size();

	assert(size%3 == 0);
	mNumTriangles = size / 3;

	struct Tri
	{
		// TODO:: Can we reduce this somehow (due to padding)?
		DirectX::XMFLOAT4 v[3];
	} triangle;

	std::vector<Tri> tri_buff;
	tri_buff.reserve(mNumTriangles);

	unsigned int curr_tri_vert = 0;
	for (auto v : mTris)
	{		
		triangle.v[curr_tri_vert++] = DirectX::XMFLOAT4(v.x, v.y, v.z, 1.f);

		if (curr_tri_vert == 3)
		{
			tri_buff.push_back(triangle);
			curr_tri_vert = 0;
		}
	}

	ZeroMemory(&mTrianglesBuffDesc, sizeof(mTrianglesBuffDesc));
	mTrianglesBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mTrianglesBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	mTrianglesBuffDesc.StructureByteStride = sizeof(Tri);
	mTrianglesBuffDesc.ByteWidth = mTrianglesBuffDesc.StructureByteStride * size;
	mTrianglesBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mTrianglesBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	HRESULT hr = renderer->Device()->CreateBuffer(&mTrianglesBuffDesc, NULL, &mTrianglesBuffer);
	assert(SUCCEEDED(hr));

	D3D11_MAPPED_SUBRESOURCE mapped_rsrc;
	renderer->DeviceContext()->Map(mTrianglesBuffer, 0, D3D11_MAP_WRITE, 0, &mapped_rsrc);
	Tri* data = reinterpret_cast<Tri*>(mapped_rsrc.pData);
	for (unsigned int i = 0; i < tri_buff.size(); i++)
		data[i] = tri_buff[i];
	renderer->DeviceContext()->Unmap(mTrianglesBuffer, 0);

	ZeroMemory(&mTrianglesSrvDesc, sizeof(mTrianglesSrvDesc));
	mTrianglesSrvDesc.Buffer.ElementWidth = mTrianglesBuffDesc.StructureByteStride;
	mTrianglesSrvDesc.Buffer.FirstElement = 0;
	mTrianglesSrvDesc.Buffer.NumElements = mTrianglesBuffDesc.ByteWidth / mTrianglesBuffDesc.StructureByteStride;
	mTrianglesSrvDesc.Format = DXGI_FORMAT_UNKNOWN;
	mTrianglesSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	hr = renderer->Device()->CreateShaderResourceView((ID3D11Resource *)mTrianglesBuffer, &mTrianglesSrvDesc, &mTrianglesSrv);

	return;
}
