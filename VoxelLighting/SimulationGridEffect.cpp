#include <assert.h>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "SimulationGridEffect.h"

extern std::wstring gEngineRootDir;

SimulationGridEffect::SimulationGridEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mInitCsBlob = nullptr;
	mInitCS = nullptr;
	mVoxelGridUav = nullptr;
	mConstructGridCsBlob = nullptr;
	mConstructGridCS = nullptr;

	ZeroMemory(&mCbInitConstantVariables, sizeof(CbInitConstants));
	mCbInitConstants.Create(renderer->Device());

	ZeroMemory(&mCbPerObjectConstantVars, sizeof(CbPerObjectConstants));
	mCbPerObjectConstants.Create(renderer->Device());

	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}

/*virtual*/
SimulationGridEffect::~SimulationGridEffect()
{    
	if (mInitCsBlob)
		mInitCsBlob->Release();
	if (mInitCS)
		mInitCS->Release();
	if (mConstructGridCsBlob)
		mConstructGridCsBlob->Release();
	if (mConstructGridCS)
		mConstructGridCS->Release();
}

void
SimulationGridEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	// Load shaders
	cso_name = std::wstring(HR_INIT_GRID_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInitCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mInitCsBlob->GetBufferPointer(),
		mInitCsBlob->GetBufferSize(),
		NULL,
		&mInitCS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_CONSTRUCT_VOXEL_GRID_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mConstructGridCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mConstructGridCsBlob->GetBufferPointer(),
		mConstructGridCsBlob->GetBufferSize(),
		NULL,
		&mConstructGridCS);
	assert(SUCCEEDED(hr));
}

void 
SimulationGridEffect::UpdateNumVoxelsPerLengthVariable(unsigned int const numVxlsPerLngth)
{	
	mCbInitConstantVariables.numVoxelsPerLength = numVxlsPerLngth;
	mCbPerObjectConstantVars.numVoxelsPerLength = numVxlsPerLngth;
}

void 
SimulationGridEffect::UpdateVoxelGridUav(ID3D11UnorderedAccessView* const gridUav)
{
	mVoxelGridUav = gridUav;
}

void 
SimulationGridEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (techName == HR_INIT_GRID_TECH)
	{
		mCbInitConstants.SetData(deviceContext, mCbInitConstantVariables);

		auto cb = mCbInitConstants.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11UnorderedAccessView* uavs[] = { mVoxelGridUav };
		unsigned int init_cnts[] = { 1 };
		deviceContext->CSSetUnorderedAccessViews(0, 1, uavs, init_cnts);

		deviceContext->CSSetShader(mInitCS, nullptr, 0);
	}
	else if (HR_CONSTRUCT_VOXEL_GRID_TECH)
	{
		mCbPerObjectConstants.SetData(deviceContext, mCbPerObjectConstantVars);

		auto cb = mCbPerObjectConstants.GetBuffer();

		deviceContext->CSSetConstantBuffers(0, 1, &cb);
		deviceContext->CSSetShaderResources(0, 1, &mTrianglesSrv);

		deviceContext->CSSetShader(mConstructGridCS, nullptr, 0);
	}
}

void 
SimulationGridEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (techName == HR_INIT_GRID_TECH)
	{
		ID3D11Buffer* nb = nullptr;
		ID3D11UnorderedAccessView* nuavs[] = { nullptr };
		unsigned int init_cnts[] = { 0 };

		deviceContext->CSSetShader(nullptr, nullptr, 0);
		deviceContext->CSSetConstantBuffers(0, 1, &nb);
		deviceContext->CSSetUnorderedAccessViews(0, 1, &nuavs[0], init_cnts);
	}
	else
	{
		ID3D11UnorderedAccessView* uavs[] = { mVoxelGridUav };
		unsigned int init_cnts[] = { 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 1, uavs, init_cnts);
	}	
}

void 
SimulationGridEffect::UpdateTrianglesBufferSrv(ID3D11ShaderResourceView* const triangles)
{
	mTrianglesSrv = triangles;
}

void 
SimulationGridEffect::UpdateWorldTransformVariable(DirectX::XMMATRIX const& worldTransform)
{
	mCbPerObjectConstantVars.worldTransform = DirectX::XMMatrixTranspose(worldTransform);
}

void 
SimulationGridEffect::UpdateSimGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4 const& simGridCenterAndVoxelSizePerLength)
{
	mCbPerObjectConstantVars.simGridCenterAndVoxelSizePerLength = simGridCenterAndVoxelSizePerLength;
}

void 
SimulationGridEffect::UpdateNumTrisVariable(int const numTris)
{
	mCbPerObjectConstantVars.numTris = numTris;
}

void SimulationGridEffect::UnbindAllResources(ID3D11DeviceContext* const deviceContext)
{
	ID3D11UnorderedAccessView* uavs[] = { nullptr };
	unsigned int init_cnts[] = { 0 };
	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11Buffer* nb = nullptr;

	deviceContext->CSSetUnorderedAccessViews(0, 1, uavs, init_cnts);
	deviceContext->CSSetShaderResources(0, 1, &nsrv);
	deviceContext->CSSetConstantBuffers(0, 1, &nb);

	deviceContext->CSSetShader(nullptr, nullptr, 0);
}

void 
SimulationGridEffect::BindPerFrameResources(ID3D11DeviceContext* const deviceContext)
{
	ID3D11UnorderedAccessView* uavs[] = { mVoxelGridUav };
	unsigned int init_cnts[] = { 0 };
	deviceContext->CSSetUnorderedAccessViews(0, 1, uavs, init_cnts);
}
