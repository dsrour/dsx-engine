#pragma once

#include <memory>
#include <string>
#include "Systems/Renderer/AntTweakBar/include/AntTweakBar.h"
#include "KeyboardActions.h"
#include "MouseActions.h"
#include "Scene/SceneObjectManager.h"
#include "BaseApp.h"

class HybridPipeline;
class DebugPipeline;
class PostProcessPipeline;

class VoxelLightingApp : public BaseApp
{
public:
    VoxelLightingApp(void);

    virtual
    ~VoxelLightingApp(void);

    bool const
    OnLoadApp(void);
    
    void
    OnPreUpdate(void);

    void
    OnPostUpdate(void);
        
    void
    OnUnloadApp(void);
   
private: 
	// This function sets up anything needed for the 
	// voxel lighting pipeline to function upon loading a new scene.
	void
	InitLoadedScene();

	unsigned int
	CreateAndAddVoxelizedComponentToEntity(unsigned int endityId);

	void
	GenerateTrianglesFromRenderable(std::shared_ptr<Renderable>& renderable, std::vector< DirectX::XMFLOAT3>& trisOut);

	std::shared_ptr<ApplicationKeyboardActions> mKeyboardSystem;
	std::shared_ptr<ApplicationMouseActions>	mMouseActions;

	static std::shared_ptr<HybridPipeline> mHybridPipeline;
	//static std::shared_ptr<DebugPipeline> mDebugPipeline;
	std::shared_ptr<PostProcessPipeline> mPostProcPipeline;
	static std::shared_ptr<SceneObjectManager> mSoMngr;

	static bool mNeedsInit;

	
	// ANTTWEEKBAR Menu  /////////////////////////////////////////////////
	static void TW_CALL
	LoadScene(void* /*clientData*/);

	static void TW_CALL
	InitSimGrid(void* /*clientData*/);

	static void TW_CALL
	ToggleVoxelGridUpdates(void* /*clientData*/);

	static void TW_CALL
	ToggleFrameProfiling(void* /*clientData*/);

	static void TW_CALL
	RecompileDebugPipeline(void* /*clientData*/);

	static void TW_CALL
	RecompileVoxelLightPipeline(void* /*clientData*/);

public:
	typedef enum { QUARTER_GIG, HALF_GIG, ONE_GIG } TilePoolSizeEnum;
private:		
	static TilePoolSizeEnum mTilePoolSize;
	static unsigned int mSimGridNumVoxels;
	static float		mSimGridVoxelSize;

	static bool			mDoTiledResourcesFitmentChecks;
	static bool			mDoMetricsCollection;

	static unsigned int mNumRayMarches;

	static float mLightSize;
	static int mMaxDilation;

	static TwBar*   mSceneTwBar;
	static TwBar*   mGiSimulationTwBar;
	static TwBar*   mUtilsTwBar;
	static TwBar*   mShadowsTwBar;

};

