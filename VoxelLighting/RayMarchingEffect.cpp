#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "RayMarchingEffect.h"

extern std::wstring gEngineRootDir;

RayMarchingEffect::RayMarchingEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mRaysListSrv = nullptr;
	mRayPacketsUav = nullptr;
	mRayPacketCountsUav = nullptr;
	mRayVoxelIdsUav = nullptr;
	mRayPacketCountsSrv = nullptr;
	mRayPacketsSrv = nullptr;
	mRayVoxelIdsSrv = nullptr;
	mPrefixSumSrv = nullptr;
	mRayPacketHeadersUav = nullptr;
	mVoxelGridUav = nullptr;
	mRaysHitUav = nullptr;
	mRaysListUav = nullptr;
	mHashLookupSrv = nullptr;
	mHashLookupUav = nullptr;
	mIndirectDispatchArgsUav = nullptr;
	mMetricsUav = nullptr;

	mGeneratePacketCountsCb.Create(renderer->Device());
	ZeroMemory(&mInitActiveRaysVars, sizeof(mInitActiveRaysVars));

	mCreatePacketsCb.Create(renderer->Device());
	ZeroMemory(&mCreatePacketsVars, sizeof(mCreatePacketsVars));

	mIntersectCb.Create(renderer->Device());
	ZeroMemory(&mIntersectVars, sizeof(mIntersectVars));

	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}

/*virtual*/
RayMarchingEffect::~RayMarchingEffect()
{    
	if (mGeneratePacketCountsCsBlob)
		mGeneratePacketCountsCsBlob->Release();
	if (mGeneratePacketCountsCs)
		mGeneratePacketCountsCs->Release();		

	if (mCreatePacketsCsBlob)
		mCreatePacketsCsBlob->Release();
	if (mCreatePacketsCs)
		mCreatePacketsCs->Release();

	if (mIntersectCsBlob)
		mIntersectCsBlob->Release();
	if (mIntersectCs)
		mIntersectCs->Release();

	if (mTraverseGridCsBlob)
		mTraverseGridCsBlob->Release();
	if (mTraverseGridCs)
		mTraverseGridCs->Release();
}

void
RayMarchingEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	cso_name = std::wstring(HR_RAY_MARCH_GENERATE_RAY_PACKET_COUNTS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mGeneratePacketCountsCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mGeneratePacketCountsCsBlob->GetBufferPointer(),
		mGeneratePacketCountsCsBlob->GetBufferSize(),
		NULL,
		&mGeneratePacketCountsCs);
	assert(SUCCEEDED(hr));	

	cso_name = std::wstring(HR_RAY_MARCH_CREATE_RAY_PACKETS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mCreatePacketsCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mCreatePacketsCsBlob->GetBufferPointer(),
		mCreatePacketsCsBlob->GetBufferSize(),
		NULL,
		&mCreatePacketsCs);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_RAY_MARCH_INTERSECT_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mIntersectCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mIntersectCsBlob->GetBufferPointer(),
		mIntersectCsBlob->GetBufferSize(),
		NULL,
		&mIntersectCs);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_RAY_MARCH_TRAVERSE_GRID_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mTraverseGridCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mTraverseGridCsBlob->GetBufferPointer(),
		mTraverseGridCsBlob->GetBufferSize(),
		NULL,
		&mTraverseGridCs);
	assert(SUCCEEDED(hr));
}

void 
RayMarchingEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (HR_RAY_MARCH_GENERATE_RAY_PACKET_COUNTS_TECH == techName)
	{
		mGeneratePacketCountsCb.SetData(deviceContext, mInitActiveRaysVars);

		auto cb = mGeneratePacketCountsCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11ShaderResourceView* srvs[] = { mRayVoxelIdsSrv };
		deviceContext->CSSetShaderResources(0, 1, srvs);

		ID3D11UnorderedAccessView* uavs[] = { mRayPacketCountsUav, mHashLookupUav, mIndirectDispatchArgsUav };
		unsigned int init_cnts[] = { 0, 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 3, uavs, init_cnts);

		deviceContext->CSSetShader(mGeneratePacketCountsCs, nullptr, 0);
	}
	else if (HR_RAY_MARCH_CREATE_RAY_PACKETS_TECH == techName)
	{
		mCreatePacketsCb.SetData(deviceContext, mCreatePacketsVars);

		auto cb = mCreatePacketsCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11ShaderResourceView* srvs[] = { mRaysListSrv, mPrefixSumSrv, mRayVoxelIdsSrv, mHashLookupSrv };
		deviceContext->CSSetShaderResources(0, 4, srvs);

		ID3D11UnorderedAccessView* uavs[] = { mRayPacketsUav, mRayPacketHeadersUav };
		unsigned int init_cnts[] = { 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 2, uavs, init_cnts);

		deviceContext->CSSetShader(mCreatePacketsCs, nullptr, 0);
	}
	else if (HR_RAY_MARCH_INTERSECT_TECH == techName)
	{
		mIntersectCb.SetData(deviceContext, mIntersectVars);

		auto cb = mIntersectCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11UnorderedAccessView* uavs[] = { mRayPacketsUav, mVoxelGridUav, mRayPacketHeadersUav, mRaysHitUav, mRaysListUav, mRayVoxelIdsUav, mMetricsUav };
		unsigned int init_cnts[] = { 0, 0, -1, 0, 0, 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 7, uavs, init_cnts);

		deviceContext->CSSetShader(mIntersectCs, nullptr, 0);
	}
	else if (HR_RAY_MARCH_TRAVERSE_GRID_TECH == techName)
	{
		mGeneratePacketCountsCb.SetData(deviceContext, mInitActiveRaysVars);

		auto cb = mGeneratePacketCountsCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11ShaderResourceView* srvs[] = { mRaysListSrv };
		deviceContext->CSSetShaderResources(0, 1, srvs);

		ID3D11UnorderedAccessView* uavs[] = { mVoxelGridUav, mRayVoxelIdsUav, mRaysHitUav };
		unsigned int init_cnts[] = { 0, 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 3, uavs, init_cnts);

		deviceContext->CSSetShader(mTraverseGridCs, nullptr, 0);
	}
}

void
RayMarchingEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	ID3D11UnorderedAccessView* uavs[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
	unsigned int init_cnts[] = { 0, 0, 0, 0, 0, 0, 0 };
	ID3D11ShaderResourceView* nsrv[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
	ID3D11Buffer* nb = nullptr;

	deviceContext->CSSetUnorderedAccessViews(0, 7, uavs, init_cnts);
	deviceContext->CSSetShaderResources(0, 6, nsrv);
	deviceContext->CSSetConstantBuffers(0, 1, &nb);

	deviceContext->CSSetShader(nullptr, nullptr, 0);
}

void
RayMarchingEffect::UpdateNumVoxelsPerLengthVariable(int const& numVoxelsPerLength)
{
	mInitActiveRaysVars.numVoxelsPerLength = numVoxelsPerLength;
}

void 
RayMarchingEffect::UpdateSimulationGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4 const& simGridCenterAndVoxelSizePerLength)
{
	mInitActiveRaysVars.simGridCenterAndVoxelSizePerLength = simGridCenterAndVoxelSizePerLength;
}

void 
RayMarchingEffect::UpdateRaysListSrv(ID3D11ShaderResourceView* const srv)
{
	mRaysListSrv = srv;
}

void 
RayMarchingEffect::UpdateRayPacketsUav(ID3D11UnorderedAccessView* const uav)
{
	mRayPacketsUav = uav;
}

void 
RayMarchingEffect::UpdateRayPacketCountsUav(ID3D11UnorderedAccessView* const uav)
{
	mRayPacketCountsUav = uav;
}

void 
RayMarchingEffect::UpdateDispatchParamsVariable(int const& x, int const& y, int const& z)
{
	mInitActiveRaysVars.dispatchParams[0] = x;
	mInitActiveRaysVars.dispatchParams[1] = y;
	mInitActiveRaysVars.dispatchParams[2] = z;

	mCreatePacketsVars.dispatchParams[0] = x;
	mCreatePacketsVars.dispatchParams[1] = y;
	mCreatePacketsVars.dispatchParams[2] = z;
}

void 
RayMarchingEffect::UpdateRayVoxelIdsUav(ID3D11UnorderedAccessView* const uav)
{
	mRayVoxelIdsUav = uav;
}

void 
RayMarchingEffect::UpdateRayPacketCountsSrv(ID3D11ShaderResourceView* const srv)
{
	mRayPacketCountsSrv = srv;
}

void 
RayMarchingEffect::UpdateRayVoxelIdsSrv(ID3D11ShaderResourceView* const srv)
{
	mRayVoxelIdsSrv = srv;
}

void 
RayMarchingEffect::UpdatePrefixSumSrv(ID3D11ShaderResourceView* const srv)
{
	mPrefixSumSrv = srv;
}

void 
RayMarchingEffect::UpdateRayPacketHeadersUav(ID3D11UnorderedAccessView* const uav)
{
	mRayPacketHeadersUav = uav;
}

void 
RayMarchingEffect::UpdateMaxNumPacketsGenerated(unsigned int const maxNum)
{
	mCreatePacketsVars.maxNumPacketsGenerated = maxNum;
}

void 
RayMarchingEffect::UpdateVoxelGridUav(ID3D11UnorderedAccessView* const uav)
{
	mVoxelGridUav = uav;
}

void 
RayMarchingEffect::UpdateRaysHitUav(ID3D11UnorderedAccessView* const uav)
{
	mRaysHitUav = uav;
}

void
RayMarchingEffect::UpdateRaysListUav(ID3D11UnorderedAccessView* const uav)
{
	mRaysListUav = uav;
}

void RayMarchingEffect::UpdateHashLookupUav(ID3D11UnorderedAccessView* const uav)
{
	mHashLookupUav = uav;
}

void RayMarchingEffect::UpdateHashLookupSrv(ID3D11ShaderResourceView* const srv)
{
	mHashLookupSrv = srv;
}

void 
RayMarchingEffect::UpdateIndirectDispatchArgsUav(ID3D11UnorderedAccessView* const uav)
{
	mIndirectDispatchArgsUav = uav;
}

void 
RayMarchingEffect::UpdateMetricsUav(ID3D11UnorderedAccessView* const uav)
{
	mMetricsUav = uav;
}
