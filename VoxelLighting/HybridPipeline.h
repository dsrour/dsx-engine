#pragma once

#include "SimulationGrid.h"
#include "System.h"
#include "Systems/Renderer/Pipeline.h"
#include "ComputePrimitives/Scan.h"

#define NUM_VOXEL_LENGTH					30
#define BYTES_PER_TILE						65536U
#define MAX_NUM_RAYS_PER_PACKET				(8 * 8)
#define NUM_RAY_PACKETS_PER_TILE			32

class SimulationGridEffect;
class HybridForwardRendererEffect;
class RayMarchingEffect;
class ShadowPenumbraEffect;

class HybridPipeline : public Pipeline, public System, public std::enable_shared_from_this<HybridPipeline>
{
public:
	HybridPipeline(std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EntityManager> entityManager);
	~HybridPipeline();

	virtual void
	MadeActive(void);

	virtual void
	MadeInactive(void);

	virtual void
	EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);
	
	virtual void
	RecompileShaders();

	virtual void
	RunImplementation(std::set<unsigned int> const* family, double const currentTime);

	/*
	 * @input voxelLngth:			cubed size of a single voxel
	 * @input numVoxelsPerLngth:	grid size = numVoxelsPerLngth^3
	 * @input numPhysicalTiles:     number of physical tiles (each 64KB) to allocate memory for the tile pool
	 */
	void
	InitGridResources(
		float const voxelLngth, 	
		unsigned int const numPhysicalTiles = 8192);

	unsigned int const
	NumTotalVoxels() const { return mSimulationGrid.NumVoxelsPerLength()*mSimulationGrid.NumVoxelsPerLength() *mSimulationGrid.NumVoxelsPerLength(); }

	unsigned int const
	NumVoxelsPerLength() const { return mSimulationGrid.NumVoxelsPerLength(); }

	float const
	VoxelLength() const {return mSimulationGrid.VoxelSizePerLength(); }

	/* Passing false to this function will essentially "freeze" the simulation grid.
	 * There will be no further:
	 *    - tiled resource updates.
	 *    - filling of geometry in voxels	 
	 */
	void
	ToggleVoxelGridUpdates(bool const doUpdates) { mDoSimGridUpdates = doUpdates; }

	/* Does a read-back on all occupied tiles to ensure
	 * all geometry was able to fit the simulation grid.
	 * Returns a non-empty message if checks failed.
	 */
	std::wstring
	DoGeometryFitmentCheck();

	unsigned int 
	NumRayMarches() const { return mNumRayMarches; }

	void 
	NumRayMarches(unsigned int const val) { mNumRayMarches = val; }

	// Tells the pipeline whether it should collect certain metrics (this will cost some performance)
	void
	DotMetricsCollection(bool const collectMetrics) { mDoMetricsCollection = collectMetrics; }

	struct HybridPipelineMetrics
	{
		unsigned int numIntersections;
	};
	// This function returns collected metrics when the pipeline last executed.
	HybridPipelineMetrics
	CollectMetrics();

	int 
	MaxPenumbraDilation() const { return mMaxPenumbraDilation; }
	
	void
	MaxPenumbraDilation(int val) { mMaxPenumbraDilation = val; }
	
	// Tells the renderer camera has moved...
	// This will allow to reset effects that render over several frames
	void
	SignalCameraMotion();

private:
	void 
	BufferSnapShot(std::shared_ptr<D3dRenderer> renderer);

	void
	ForwardRenderingPass(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType);

	void
	InitSimulationGridPass();

	void
	RayMarchingPass();

	void
	ResolvePass();
	
	/* Modifies rays. 
	 * This function is run after generating rays by the forward renderer.
	 * An example is penumbras dilation.	 
	 */
	void
	IntermediatePass();

	void
	DrawRenderable(Renderable *const renderable, unsigned int const& entityId, std::shared_ptr<D3dRenderer>& renderer);

	void
	MapTiles(std::set<unsigned int>& entitiesInSimulation);

	// Number of 64KB tiles in the tile pool
	unsigned int
	NumOfPhysicalTiles() 
	{
		return mVoxelGridResources.tilePoolDesc.ByteWidth / BYTES_PER_TILE;
	};

	// A functor to sort renderables front-to-back
	static bool  
	FrontToBackCompare(unsigned int const& lhs, unsigned int const& rhs);


	
	std::set<unsigned int> mOccupiedVoxels; // occupied voxel ids for the current state of the sim grid
		
	std::shared_ptr<SimulationGridEffect>			mGridEffect;			
	std::shared_ptr<HybridForwardRendererEffect>	mForwardRendererEffect;
	std::shared_ptr<RayMarchingEffect>				mRayMarchingEffect;	
	std::shared_ptr<ShadowPenumbraEffect>			mShadowPanumbraEffect;

	std::shared_ptr<ScanPrimitive> mScanPrimitive;

	SimulationGrid mSimulationGrid;

	Microsoft::WRL::ComPtr<ID3D11InfoQueue> mD3dInfoQueue;

	std::set<unsigned int> mEntitiesWithVoxelizedComponent;


	D3D11_DEPTH_STENCIL_DESC mShadowResolveDepthStencilDesc;
	D3D11_BLEND_DESC mShadowResolveBlendDesc;

	bool mInitialized;
	bool mDoSimGridUpdates;
	bool mDoMetricsCollection;

	bool mRayPacketsMapped;	// TODO:: right now we only map once for how ever many "ray" packets we need for the curren resolution, in the future
							//        mapping should be dynamic depending on how many rays are being generated.  ie. driven by the GPU rendering.
							//        if little rays are generated for the last N frames, map less resources to free up memory for other GPU operations.

	// This variable represents the maximum amount of ray packets a that can be created.
	// It is calculated in the InitGridResources implementation and is variable on how much space is allocated to the ray packets buffer.
	// It is also passed on to kernels to ensure the ray packets buffer isn't written out of bounds.	
	unsigned int mMaxNumPacketsGenerated;

	unsigned int mNumRayMarches;



	// Tile Read-Back for debugging //////////////////////////
	Microsoft::WRL::ComPtr<ID3D11Buffer> mTriTileReadBackBuff;
	struct TrianglesTile 
	{					 						 
		int header[4]; // very first header
		DirectX::XMFLOAT3 tris[1820][3];
	};

	Microsoft::WRL::ComPtr<ID3D11Buffer> mRayTileReadBackBuff;
	struct RaysTile
	{
		float values[16384];
	};
	//////////////////////////////////////////////////////////

	// Voxel grid virtual resource ///////////////////////////
	struct VoxelGridResources
	{
		// Tile Pool /////////////////////////////////////////
		D3D11_BUFFER_DESC tilePoolDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> tilePool;
		//////////////////////////////////////////////////////

		// Dummy Tile ////////////////////////////////////////
		Microsoft::WRL::ComPtr<ID3D11Buffer> dummyTileBuff;
		unsigned int dummyTileIndex;
		//////////////////////////////////////////////////////

		D3D11_BUFFER_DESC voxelGridBuffDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC voxelGridUavDesc;

		Microsoft::WRL::ComPtr<ID3D11Buffer> voxelGridBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> voxelGridUav;
	} mVoxelGridResources;
	//////////////////////////////////////////////////////////

	// Ray grid virtual resource /////////////////////////////
	struct RayPacketsTiledResources
	{		
		// Tile Pool /////////////////////////////////////////
		D3D11_BUFFER_DESC tilePoolDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> tilePool;
		//////////////////////////////////////////////////////

		// Dummy Tile ////////////////////////////////////////
		Microsoft::WRL::ComPtr<ID3D11Buffer> dummyTileBuff;
		unsigned int dummyTileIndex;
		//////////////////////////////////////////////////////

		D3D11_BUFFER_DESC rayPacketsBuffDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC rayPacketsUavDesc;

		Microsoft::WRL::ComPtr<ID3D11Buffer> rayPacketsBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> rayPacketsUav;
	} mRayPacketResources;
	//////////////////////////////////////////////////////////


	/* PER RAY TYPE RESOURCES 
	 * The following buffers are created per ray type.
	 *
	 * RayList:  an structured buffer that holds the generated rays
	 *
	 * RayHit:   a buffer that holds ray hit information (usually BRDF and ray distances)
	 *           This buffer will differ in size depending on what is needed for rendering calculations.
	 *           It is also indexed similarly to the RayList.
	 */	
	struct PerRayTypeResources
	{
		unsigned int numRaySamples;
		unsigned int numRaySamplesPerFrame;
		unsigned int resolutionFactor;	// texsize = backbufferres.xy / resolutionFactor
		
		unsigned int numComputedSamples; // used to keep track of which samples have been computed (changes per frame)

		std::vector< D3D11_TEXTURE2D_DESC > rayListDesc;
		std::vector< D3D11_SHADER_RESOURCE_VIEW_DESC > rayListSrvDesc;
		std::vector< D3D11_UNORDERED_ACCESS_VIEW_DESC > rayListUavDesc;
		std::vector< Microsoft::WRL::ComPtr<ID3D11Texture2D> > rayListTextureArray;
		std::vector< Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> > rayListUav;
		std::vector< Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> > rayListSrv;
		std::vector< Microsoft::WRL::ComPtr<ID3D11RenderTargetView> > rayListOriginDistRtv;
		std::vector< Microsoft::WRL::ComPtr<ID3D11RenderTargetView> > rayListNormRtv;

		std::vector< D3D11_TEXTURE2D_DESC > rayHitDesc;
		std::vector< D3D11_SHADER_RESOURCE_VIEW_DESC > rayHitSrvDesc;
		std::vector< D3D11_UNORDERED_ACCESS_VIEW_DESC > rayHitUavDesc;
		std::vector< Microsoft::WRL::ComPtr<ID3D11Texture2D> > rayHitTexture;
		std::vector< Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> > rayHitUav;
		std::vector< Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> > rayHitSrv;
		std::vector< D3D11_SAMPLER_DESC >	rayHitSamplerDesc;


		// Current voxel id the ray is in at the momment //////////////////
		std::vector< D3D11_TEXTURE2D_DESC > rayVoxelIdsDesc;
		std::vector< D3D11_UNORDERED_ACCESS_VIEW_DESC > rayVoxelIdsUavDesc;
		std::vector< D3D11_SHADER_RESOURCE_VIEW_DESC > rayVoxelIdsSrvDesc;
		std::vector< Microsoft::WRL::ComPtr<ID3D11Texture2D> > rayVoxelIdsTexture;
		std::vector< Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> > rayVoxelIdsSrv;
		std::vector< Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> > rayVoxelIdsUav;
		///////////////////////////////////////////////////////////////////
	};
	
	PerRayTypeResources mShadowRaysResources;
	int mMaxPenumbraDilation;	
	//////////////////////////////////////////////////////////

	// Depending on the ray type, the number of samples to process each frame differ.
	// This function is called at the end of the pipeline to set which samples should be
	// calculated next frame.
	void
	AdvanceRaySamples(PerRayTypeResources& rayResources);

	/* RAY-TRACING ALGORITHM RESOURCES
	 *
	 */
	struct RayTracingResources
	{		
		// USED TO GENERATE RAY PACKETS ///////////////////////////////////
		D3D11_BUFFER_DESC rayPacketsPerGroupDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC rayPacketsPerGroupUavDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC rayPacketsPerGroupSrvDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> rayPacketsPerGroupBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> rayPacketsPerGroupUav;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> rayPacketsPerGroupSrv;

		D3D11_BUFFER_DESC rayPacketsPerGroupPrefixDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC rayPacketsPerGroupPrefixUavDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC rayPacketsPerGroupPrefixSrvDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> rayPacketsPerGroupPrefixBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> rayPacketsPerGroupPrefixUav;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> rayPacketsPerGroupPrefixSrv;
		///////////////////////////////////////////////////////////////////

		// HASH LOOKUP
		D3D11_BUFFER_DESC rayHashLookupDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC rayHashLookupUavDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC rayHashLookupSrvDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> rayHashLookupBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> rayHashLookupUav;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> rayHashLookupSrv;

		// Ray packets append buffer //////////////////////////////////////
		D3D11_BUFFER_DESC rayPacketHeadersDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC rayPacketHeadersUavDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> rayPacketHeadersBuff;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> rayPacketHeadersUav;
		///////////////////////////////////////////////////////////////////
	} mRayTracingResources;


	struct Ray
	{
		DirectX::XMFLOAT3 origin;
		DirectX::XMFLOAT3 direction;
		float distance;
		float pad;
	};

	struct DispatchIndirectArgsResources
	{
		D3D11_BUFFER_DESC bufferDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> buffer;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> uav;
	} mIndirectDispatchResources;

	

	// Resources used when calculating metrics
	struct MetricsResources
	{
		D3D11_BUFFER_DESC intersectsPerFrameBufferDesc;
		D3D11_UNORDERED_ACCESS_VIEW_DESC intersectsPerFrameUavDesc;
		Microsoft::WRL::ComPtr<ID3D11Buffer> intersectsPerFrameBuffer;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> intersectsPerFrameUav;
		Microsoft::WRL::ComPtr<ID3D11Buffer> intersectsPerFrameReadBackBuffer;
	} mMetricResources;

	void
	InitMetricResources();

	void InitRaysResources(
		PerRayTypeResources& raysResources, 
		DXGI_FORMAT const format, unsigned int const numSamples,
		unsigned int const numSamplesPerFrame, unsigned int const resFactor);

	void
	InitShadowRayResources();

	void
	InitRayMarchingResources();


	struct DeferredBuffers
	{
		Microsoft::WRL::ComPtr<ID3D11Texture2D>  worldPosAndShadowIntensityTexture;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>  worldPosAndShadowIntensityUav;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>  worldPosAndShadowIntensitySrv;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>  worldPosAndShadowIntensityRtv;
	} mDeferredBufferResources;
	void
	InitDeferredBuffers();


};

