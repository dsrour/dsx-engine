#include <ostream>
#include <iostream>
#include <iomanip>
#include <assert.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/MathHelper.h"
#include "Systems/Renderer/Cameras/FpCamera.h"
#include "Scene/SceneObject.h"
#include "Scene/ObjRenderable/objRenderable.h"
#include "PostProcessPipeline.h"
#include "Scene/ObjImporter.h"
#include "Scene/SceneLoaderWriter.h"
#include "Utils/CommonWinDialogs.h"
#include "Scene/ProtoUtils.h"
#include "Voxelized.h"
#include "VoxelLightingApp.h"
#include "GpuProfiler.hpp"
#include "Systems/Renderer/LightingSystem.h"
#include "BaseApp.h"

// Systems
#include "Systems/Renderer/Pipelines/SkyRenderer/SkyRenderer.h"
#include "HybridPipeline.h"
#include "DebugPipeline.h"

BaseApp* gApp = new VoxelLightingApp;

bool VoxelLightingApp::mNeedsInit = false;

TwBar* VoxelLightingApp::mSceneTwBar = nullptr;
TwBar* VoxelLightingApp::mGiSimulationTwBar = nullptr;
TwBar* VoxelLightingApp::mUtilsTwBar = nullptr;
TwBar* VoxelLightingApp::mShadowsTwBar = nullptr;


std::shared_ptr<SceneObjectManager> VoxelLightingApp::mSoMngr = nullptr;
std::shared_ptr<HybridPipeline> VoxelLightingApp::mHybridPipeline = nullptr;
//std::shared_ptr<DebugPipeline> VoxelLightingApp::mDebugPipeline = nullptr;

VoxelLightingApp::TilePoolSizeEnum VoxelLightingApp::mTilePoolSize = HALF_GIG;
float VoxelLightingApp::mSimGridVoxelSize = 65.f;
unsigned int VoxelLightingApp::mNumRayMarches = 8;
unsigned int VoxelLightingApp::mSimGridNumVoxels = NUM_VOXEL_LENGTH;
bool VoxelLightingApp::mDoTiledResourcesFitmentChecks = false;
bool VoxelLightingApp::mDoMetricsCollection = false;
float VoxelLightingApp::mLightSize = 0.f;
int VoxelLightingApp::mMaxDilation = 2;



VoxelLightingApp::VoxelLightingApp() 
{
	//TMP CODE TO TEST HASH FUNCTION
	// Hash function from: http://stackoverflow.com/questions/664014/what-integer-hash-function-are-good-that-accepts-an-integer-hash-key
	//                     by "Thomas Mueller"	
	/*std::vector<unsigned int> counts(64, 0);

	unsigned int old_range = (unsigned int)(4294967294);
	unsigned int new_range = (8 * 8 - 0);
	for (unsigned int i = 0; i <= 30 * 30 * 30; i++)
	{
		unsigned int hash = i;
		hash = ((hash >> 16) ^ hash) * 0x45d9f3b;
		hash = ((hash >> 16) ^ hash) * 0x45d9f3b;
		hash = ((hash >> 16) ^ hash);

		unsigned int index = (unsigned int)(hash / (float)old_range * new_range);
		OutputDebugMsg(to_string(i) + ": \t\t\t\t" + to_string(index) + "\n");

		counts[index]++;
	}
	OutputDebugMsg("\n");
	for (unsigned int i = 0; i < counts.size(); i++)
		OutputDebugMsg(to_string(i) + ": \t\t\t\t" + to_string(counts[i]) + "\n");*/



	// Width and Height should be divisible by 32 to work properly with the shaders.
    WinWidth(1888);
    WinHeight(992);
// 	WinWidth(640);
// 	WinHeight(160);

	assert(WinWidth() % 32 == 0 && WinHeight() % 32 == 0);
}

VoxelLightingApp::~VoxelLightingApp()
{
	gGpuProfiler.Shutdown();
	ShutDownPrototypeLibrary();
}

bool const
VoxelLightingApp::OnLoadApp()
{		
	TitleName(L"Voxel Lighting Simulation");

    Renderer()->ClearColor(0.f, 0.f, 0.f, 1.0f);

	// Setup the rendering pipeline
	mPostProcPipeline = std::make_shared<PostProcessPipeline>(gApp->Renderer());

	// Create VexelLight pipeline...
	mHybridPipeline = std::make_shared<HybridPipeline>(gApp->Renderer(), EntityMngr());
	unsigned int vlp_sys_id = SystemMngr()->AddSystem(mHybridPipeline);
	double target = 1.0 / 60.0;
	SystemSchdlr()->StopSystem(vlp_sys_id);
	SystemSchdlr()->RunSystemEveryDelta(vlp_sys_id, target);
	//mDebugPipeline = std::make_shared<DebugPipeline>(gApp->Renderer(), EntityMngr());
	//mDebugPipeline->DebugVoxelLightingPipeline(mHybridPipeline);

	std::list< std::shared_ptr<Pipeline> > path;
	path.push_back(std::make_shared<SkyRenderer>(gApp->Renderer()));
	path.push_back(mHybridPipeline);
	//path.push_back(mDebugPipeline);
	path.push_back(mPostProcPipeline);		
	gApp->Renderer()->RenderPath(path);

	// Move camera backwards off the center of the scene
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Position(DirectX::XMFLOAT4(0.f, 15.f, 0.f, 1.f));
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Move(-30.0f); 		
	static_cast<FpCamera*>(Renderer()->CurrentCamera().get())->Pitch(0.4f);
		
	// User input
	mKeyboardSystem = std::make_shared<ApplicationKeyboardActions>(Renderer()->CurrentCamera(), InputDeviceMngr());
	InputDeviceMngr()->Keyboard()->AddKeyboardActions(mKeyboardSystem);
	mMouseActions = std::make_shared<ApplicationMouseActions>(InputDeviceMngr());
	InputDeviceMngr()->Mouse()->AddMouseActions(mMouseActions);

	// SO Manager
	mSoMngr = std::make_shared<SceneObjectManager>();

	// Menus ////////////////////////////////////////////////////////////////////////////////////
	mSceneTwBar = TwNewBar("Scene");
	TwAddButton(mSceneTwBar, "load_scene", LoadScene, NULL, " label='Load' ");
	TwDefine(" 'Scene' size='100 5' position='10 10' valueswidth=50 alpha=50 ");

	mGiSimulationTwBar = TwNewBar("GI Simulation");
	TwAddVarRW(mGiSimulationTwBar, "voxel_size", TW_TYPE_FLOAT, &mSimGridVoxelSize, "label='Voxel Length' min=0.001 step=0.25 precision=3");
	TwAddVarRO(mGiSimulationTwBar, "num_voxels", TW_TYPE_UINT32, &mSimGridNumVoxels, "label='Num Voxels Per Length' min=1 max=512");
	TwType                                                               poolSize;
	poolSize = TwDefineEnum("TilePoolSizeEnum", NULL, 0);
	TwAddVarRW(mGiSimulationTwBar, "TilePoolSize", poolSize, &mTilePoolSize, 
		" enum='0 {256 MB}, 1 {512 MB}, 2 {1 GB}'");
	TwAddButton(mGiSimulationTwBar, "init_grid", InitSimGrid, NULL, " label='Init Simulation Grid' ");
	TwAddButton(mGiSimulationTwBar, "space01", NULL, NULL, " label=' ' ");
	TwAddButton(mGiSimulationTwBar, "toggle_grid", ToggleVoxelGridUpdates, NULL, " label='Toggle Simulation Grid Updates' ");
	TwAddButton(mGiSimulationTwBar, "space02", NULL, NULL, " label=' ' ");
	TwAddVarRW(mGiSimulationTwBar, "num_ray_marches", TW_TYPE_UINT32, &mNumRayMarches, "label='Num. Ray Marches'");
	TwDefine(" 'GI Simulation' size='300 145' position='10 100' valueswidth=100 alpha=50 ");
	TwDefine(" 'GI Simulation' iconified=true ");

	mUtilsTwBar = TwNewBar("Utils");
	TwAddButton(mUtilsTwBar, "profile_frame", ToggleFrameProfiling, NULL, " label='Toggle Frame Profiling' ");
	TwAddButton(mUtilsTwBar, "recompile_vxl_lght_ppln", RecompileVoxelLightPipeline, NULL, " label='Recompile VoxelLight Pipeline' ");
	TwAddButton(mUtilsTwBar, "recompile_debug_ppln", RecompileDebugPipeline, NULL, " label='Recompile Debug Pipeline' ");
	TwAddVarRW(mUtilsTwBar, "do_fitment_checks", TW_TYPE_BOOLCPP, &mDoTiledResourcesFitmentChecks, "label='Do T.R. Fitments Checks'");
	TwAddVarRW(mUtilsTwBar, "do_metrics_collectino", TW_TYPE_BOOLCPP, &mDoMetricsCollection, "label='Do Metrics Collection'");
	TwDefine(" 'Utils' size='300 150' position='10 250' valueswidth=100 alpha=50 ");
	TwDefine(" 'Utils' iconified=true ");

	mShadowsTwBar = TwNewBar("Shadows");
	TwAddVarRW(mShadowsTwBar, "LightSize", TW_TYPE_FLOAT, &mLightSize, "label='Light Size' min=0 step=0.2");
	TwAddVarRW(mShadowsTwBar, "PenumbraDilation", TW_TYPE_INT32, &mMaxDilation, "label='Max Penumbra Dilation' min=0");
	TwDefine(" 'Shadows' size='300 5' position='10 90' alpha=50 ");
	/////////////////////////////////////////////////////////////////////////////////////////////
		
	gGpuProfiler.Init();

	InitSimGrid(nullptr);

    return true;
}

    
void
VoxelLightingApp::OnPreUpdate()
{        
	// Update directional light's size
	auto lights = Renderer()->LightingSystm()->Lights();
	if (!lights.empty() &&
		DIRECTIONAL_LIGHT == (*lights.begin()).first)
	{
		Light* light = (*lights.begin()).second;
		static_cast<DirectionalLight*>(light)->lightSize = mLightSize;
	}

	// Update shadow penumbra settings
	static auto last_num_ray_marches = mNumRayMarches;
	mHybridPipeline->NumRayMarches(mNumRayMarches);
	mHybridPipeline->DotMetricsCollection(mDoMetricsCollection);
	mHybridPipeline->MaxPenumbraDilation(mMaxDilation);
	

	// Keep track of camera changes and notify renderer if it occurs
	auto cam = gApp->Renderer()->CurrentCamera();
	static DirectX::XMFLOAT4X4 last_cam_mat = cam->ViewProjMatrix();
	
	auto curr_mat = cam->ViewProjMatrix();
	if (!IsEqual(last_cam_mat, curr_mat))
	{
		last_cam_mat = curr_mat;
		mHybridPipeline->SignalCameraMotion();
	}
}

void
VoxelLightingApp::OnPostUpdate()
{   
	// Get profiler data back /////////////////////////////////////////////////////
	gGpuProfiler.WaitForDataAndUpdate();

	float dt_total = 0.0f;

	for (GTS gts = GTS_BeginFrame; gts < GTS_Max; gts = GTS(gts + 1))
		dt_total += gGpuProfiler.DtAvg(gts);

	std::wstring prflr_str;
	std::ostringstream os;
	if (gGpuProfiler.Enabled())
	{		
		std::string tmp;

		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * dt_total; 
		tmp = os.str();
		prflr_str += L"Hybrid Pipeline Total: " + std::wstring(tmp.begin(), tmp.end());

		prflr_str += L"\n\n    Forward Rendering:";
		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_InitRayLinks);
		tmp = os.str();
		prflr_str += L"\n      Init Ray Links: " + std::wstring(tmp.begin(), tmp.end());

		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_ForwardRendering);
		tmp = os.str();
		prflr_str += L"\n      Render Meshes: " + std::wstring(tmp.begin(), tmp.end());

		prflr_str += L"\n\n    Intermediate:";
		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_PenumbraDilation);
		tmp = os.str();
		prflr_str += L"\n      Penumbra Ray Gen & Dilation: " + std::wstring(tmp.begin(), tmp.end());

		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_TileMappings);
		tmp = os.str();
		prflr_str += L"\n\n    Tile Mappings:  " + std::wstring(tmp.begin(), tmp.end());

		prflr_str += L"\n\n    Init Voxel Grid:"; 
		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_ClearTilesHeaders);
		tmp = os.str();
		prflr_str += L"\n      Reset Tiles Headers: " + std::wstring(tmp.begin(), tmp.end());
		
		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_FillTilesGeometry); 
		tmp = os.str();
		prflr_str += L"\n      Per Object Geometry Fill: " + std::wstring(tmp.begin(), tmp.end());
		
		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_RayMarch);
		tmp = os.str();
		prflr_str += L"\n\n     Ray Marching: " + std::wstring(tmp.begin(), tmp.end());

		os.str(std::string());
		os << std::setprecision(2) << std::fixed << 1000.0f * gGpuProfiler.DtAvg(GTS_Resolve);
		tmp = os.str();
		prflr_str += L"\n\n     Resolve: " + std::wstring(tmp.begin(), tmp.end());
	}

	mPostProcPipeline->SetProfileString(prflr_str);
	///////////////////////////////////////////////////////////////////////////////	


	if (mDoTiledResourcesFitmentChecks)
	{
		PostProcessPipeline::Message msg;
		msg.pos = DirectX::XMFLOAT2(gApp->WinWidth() - (gApp->WinWidth()/2.0f), gApp->WinHeight() - 100.f);
		msg.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		msg.duration = 0.1;
		msg.msg = mHybridPipeline->DoGeometryFitmentCheck();
		if (!msg.msg.empty())
			mPostProcPipeline->BroadcastMessage(msg);
	}

	if (mDoMetricsCollection)
	{
		PostProcessPipeline::Message msg;
		msg.pos = DirectX::XMFLOAT2(gApp->WinWidth() - (gApp->WinWidth() / 2.0f), gApp->WinHeight() - 150.f);
		msg.color = DirectX::XMFLOAT3(1.f, 0.f, 0.f);
		msg.duration = 0.1;

		auto metrics = mHybridPipeline->CollectMetrics();

		msg.msg = std::to_wstring(metrics.numIntersections * (1000.f / (1000.0f * dt_total)) / 1000000.f) + L" M intersections per second";
		mPostProcPipeline->BroadcastMessage(msg);
	}

	if (mNeedsInit)
	{
		static int frame_cnter = 10; // TODO: figure out why the scene doesn't draw shadows without this timer
		if (frame_cnter-- == 10)
		{
			InitLoadedScene();
		}
		if (frame_cnter == 0)
		{
			frame_cnter = 10;
			mNeedsInit = false;
			mHybridPipeline->SignalCameraMotion();
		}
	}
}
    
void
VoxelLightingApp::OnUnloadApp()
{

}

void TW_CALL 
VoxelLightingApp::LoadScene(void* /*clientData*/)
{
	static std::wstring basepath = L"";

	gApp->InputDeviceMngr()->Mouse()->ToggleRelativeMode(false);


	auto file = BasicFileOpen(L"*.sc", basepath);

	if (!file.empty())
	{
		basepath = std::wstring(file);
		auto found = basepath.find_last_of(L"/\\");
		basepath = basepath.substr(0, found + 1);

		if (!file.empty())
		{
			auto so_ids = mSoMngr->SceneObjectsIds();
			mSoMngr->RemoveSceneObjects(so_ids);
			SceneLoaderWriter::LoadSceneFromFile(std::string(file.begin(), file.end()), mSoMngr, nullptr, true);
			mNeedsInit = true;
		}
	}

	mHybridPipeline->MadeInactive(); // this will clear resources in the pipeline
	mHybridPipeline->MadeActive();
}

void
VoxelLightingApp::InitLoadedScene()
{
	unsigned int total_tris = 0;

	auto entities = mSoMngr->SceneObjectsIds();

	for (auto id : entities)
	{
		total_tris += CreateAndAddVoxelizedComponentToEntity(id);
		mSoMngr->SceneObjectRef(id)->ToggleBoundBox(false);
	}

	OutputDebugMsg("Scene has " + to_string(total_tris) + " triangles.\n");
}

unsigned int 
VoxelLightingApp::CreateAndAddVoxelizedComponentToEntity(unsigned int endityId)
{
	unsigned int total_tris = 0;

	auto so = mSoMngr->SceneObjectRef(endityId);

	if (so->HasGeometry())
	{
		for (unsigned int i = 0; i < so->NumSubEntities(); i++)
		{ 
			auto se = so->SubEntityRefByIndex(i);
			if (se->renderableComp && se->renderableComp->HasMetadata())
			{
				std::shared_ptr<Voxelized> voxelized(new Voxelized);
				std::vector< DirectX::XMFLOAT3> tris;
				GenerateTrianglesFromRenderable(se->renderableComp, tris);
				voxelized->InitFromTriangleList(tris, Renderer());
				EntityMngr()->AddComponentToEntity(se->entityId, voxelized);
				total_tris += (unsigned int)tris.size();
			}
		}			
	}

	return total_tris;
}

void 
VoxelLightingApp::GenerateTrianglesFromRenderable(std::shared_ptr<Renderable>& renderable, std::vector< DirectX::XMFLOAT3>& trisOut)
{	
	/// For now only process triangle lists
	if (renderable->HasMetadata() && renderable->PrimitiveTopology() == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
	{
		trisOut.reserve(renderable->GetMetadata().indexMetadata.numIndices);

		for (unsigned int ind : renderable->GetMetadata().indexMetadata.indexBuffer)
		{
			trisOut.push_back(renderable->GetMetadata().geomMetadata.vertsBuffer[ind].pos);
		}
	}
}

void TW_CALL 
VoxelLightingApp::InitSimGrid(void* /*clientData*/)
{
	unsigned int num_tiles = 8192;
	if (QUARTER_GIG == mTilePoolSize)
		num_tiles /= 2;
	else if (ONE_GIG == mTilePoolSize)
		num_tiles *= 2;
	mHybridPipeline->InitGridResources(mSimGridVoxelSize, num_tiles);
}

void TW_CALL 
VoxelLightingApp::ToggleVoxelGridUpdates(void* /*clientData*/)
{
	static bool toggled = true;

	toggled = !toggled;

	mHybridPipeline->ToggleVoxelGridUpdates(toggled);
}

void TW_CALL 
VoxelLightingApp::RecompileDebugPipeline(void* /*clientData*/)
{
	//mDebugPipeline->RecompileShaders();
}

void TW_CALL 
VoxelLightingApp::RecompileVoxelLightPipeline(void* /*clientData*/)
{
	mHybridPipeline->RecompileShaders();
}

void TW_CALL 
VoxelLightingApp::ToggleFrameProfiling(void* /*clientData*/)
{
	gGpuProfiler.Enable(!gGpuProfiler.Enabled());
}






