#pragma once

#include <Windows.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <DirectXCollision.inl>
#include <memory>
#include <set>

class Camera;

class SimulationGrid
{
public:
	struct SimulationGridAttributes
	{		
		float voxelSizePerLength = 10.f;
		unsigned int numVoxelsPerLength = 256; // grid has numVoxelsPerLength^3 voxels.
	};

	void
	Init(SimulationGridAttributes const& attrs) { mAttrs = attrs; }

	void
	CullEntitiesOutOfGrid(std::shared_ptr<Camera> const&, std::set<unsigned int>& entities);

	float const
	VoxelSizePerLength() const {return mAttrs.voxelSizePerLength;}

	int const
	NumVoxelsPerLength() const { return mAttrs.numVoxelsPerLength; }

	// @returns: set of ids of intersected cells... Ids are always valid
	std::set<unsigned int>
	IntersectGridWithAabb(DirectX::XMFLOAT3& center, DirectX::BoundingBox& aabb);

	// @return: id could be out of range and needs to be checked
	int
	CellIdFromPoint(DirectX::XMFLOAT3& center, DirectX::XMFLOAT3& pos);

private:
	void
	XyzToIjk(DirectX::XMFLOAT3& center, DirectX::XMFLOAT3& xyz, int& iOut, int& jOut, int& kOut);

	unsigned int
	IjkToCellId(unsigned int const i, unsigned int const j, unsigned int const k);

	SimulationGridAttributes mAttrs;
};

