#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class SimulationGridEffect : public Effect
{
public:
    SimulationGridEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~SimulationGridEffect(void); 
	
	virtual void
	Recompile() { if(mRendererRef) Init(mRendererRef, true, true); }

	void
	UpdateNumVoxelsPerLengthVariable(unsigned int const numVxlsPerLngth);

	void
	UpdateWorldTransformVariable(DirectX::XMMATRIX const& worldTransform);

	void
	UpdateSimGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4 const& simGridCenterAndVoxelSizePerLength);

	void
	UpdateNumTrisVariable(int const numTris);	
	
	void
	UpdateVoxelGridUav(ID3D11UnorderedAccessView* const gridUav);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	void
	UpdateTrianglesBufferSrv(ID3D11ShaderResourceView* const triangles);

	void
	UnbindAllResources(ID3D11DeviceContext* const deviceContext);

	void
	BindPerFrameResources(ID3D11DeviceContext* const deviceContext);
                
private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);

	ID3DBlob* mInitCsBlob;
	ID3D11ComputeShader* mInitCS;

	ID3DBlob* mConstructGridCsBlob;
	ID3D11ComputeShader* mConstructGridCS;


	struct CbInitConstants
	{
		unsigned int			numVoxelsPerLength;		
		DirectX::XMFLOAT3		pad;
	} mCbInitConstantVariables;
	DirectX::ConstantBuffer< CbInitConstants > mCbInitConstants;

	struct CbPerObjectConstants
	{
		DirectX::XMMATRIX		worldTransform;
		DirectX::XMFLOAT4		simGridCenterAndVoxelSizePerLength;
		int						numTris;
		int						numVoxelsPerLength;
		DirectX::XMFLOAT2		pad;
	} mCbPerObjectConstantVars;
	DirectX::ConstantBuffer< CbPerObjectConstants > mCbPerObjectConstants;

	ID3D11UnorderedAccessView* mVoxelGridUav;
	ID3D11ShaderResourceView*  mTrianglesSrv;
	
	D3dRenderer* mRendererRef;
};