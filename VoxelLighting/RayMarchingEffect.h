#pragma once

#include "Systems/Renderer/Renderer.h"

#include <wrl/client.h>
#include "Systems/Renderer/DirectXTK/Src/ConstantBuffer.h"

#include <vector>
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Components/Renderable.h"
#include "Systems/Renderer/Effect.h"

class RayMarchingEffect : public Effect
{
public:
    RayMarchingEffect(std::shared_ptr<D3dRenderer> renderer);       
    
    virtual 
    ~RayMarchingEffect(void); 
	
	void
	UpdateSimulationGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4 const& simGridCenterAndVoxelSizePerLength);
   
	void
	UpdateNumVoxelsPerLengthVariable(int const& numVoxelsPerLength);

	void
	UpdateDispatchParamsVariable(int const& x, int const& y, int const& z);
		
	void
	UpdateRaysListSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateRayPacketCountsSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateActiveRayLinksSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateRayPacketsUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdatePrefixSumSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateRayVoxelIdsSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateVoxelGridUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateRayOriginDistsSrv(ID3D11ShaderResourceView* const srv);

	void
	UpdateRayPacketCountsUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateRayVoxelIdsUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateRayPacketHeadersUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateRaysListUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateRaysHitUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateHashLookupUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateHashLookupSrv(ID3D11ShaderResourceView* const srv);

	virtual void
	ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	virtual void
	ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext);

	void
	UpdateMaxNumPacketsGenerated(unsigned int const maxNum);

	void
	UpdateIndirectDispatchArgsUav(ID3D11UnorderedAccessView* const uav);
	
	void
	UpdateMetricsUav(ID3D11UnorderedAccessView* const uav);

	void
	UpdateViewportSizeVariable(int const x, int const y)
	{
		mIntersectVars.viewportSize[0] = x;
		mIntersectVars.viewportSize[1] = y;
	}

private:     
	void 
	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);

	ID3DBlob* mGeneratePacketCountsCsBlob;
	ID3D11ComputeShader* mGeneratePacketCountsCs;

	ID3DBlob* mCreatePacketsCsBlob;
	ID3D11ComputeShader* mCreatePacketsCs;

	ID3DBlob* mIntersectCsBlob;
	ID3D11ComputeShader* mIntersectCs;

	ID3DBlob* mTraverseGridCsBlob;
	ID3D11ComputeShader* mTraverseGridCs;

	struct CbGeneratePacketCountsConstants
	{	
		DirectX::XMFLOAT4 simGridCenterAndVoxelSizePerLength;
		int numVoxelsPerLength;
		int dispatchParams[3];
	} mInitActiveRaysVars;
	DirectX::ConstantBuffer< CbGeneratePacketCountsConstants > mGeneratePacketCountsCb;

	struct CbCreatePacketsConstants
	{		
		int maxNumPacketsGenerated;
		int dispatchParams[3];
	} mCreatePacketsVars;
	DirectX::ConstantBuffer< CbCreatePacketsConstants > mCreatePacketsCb;

	struct CbIntersectConstants
	{
		int viewportSize[2];
		int pad[2];
	} mIntersectVars;
	DirectX::ConstantBuffer< CbIntersectConstants > mIntersectCb;

	ID3D11ShaderResourceView*    mRaysListSrv;	
	ID3D11ShaderResourceView*	 mRayPacketCountsSrv;
	ID3D11ShaderResourceView*	 mRayPacketsSrv;
	ID3D11ShaderResourceView*	 mRayVoxelIdsSrv;
	ID3D11ShaderResourceView*	 mPrefixSumSrv;
	ID3D11ShaderResourceView*	 mHashLookupSrv;
	
	ID3D11UnorderedAccessView*	 mVoxelGridUav;
	ID3D11UnorderedAccessView*	 mRayPacketsUav;
	ID3D11UnorderedAccessView*	 mRayPacketCountsUav;
	ID3D11UnorderedAccessView*	 mRayVoxelIdsUav;
	ID3D11UnorderedAccessView*	 mRayPacketHeadersUav;
	ID3D11UnorderedAccessView*   mRaysHitUav;
	ID3D11UnorderedAccessView*   mRaysListUav;
	ID3D11UnorderedAccessView*   mHashLookupUav;
	ID3D11UnorderedAccessView*   mIndirectDispatchArgsUav;

	ID3D11UnorderedAccessView*   mMetricsUav;

	D3dRenderer* mRendererRef; /// Kept for recompile() func	
};