#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "BaseApp.h"
#include "HybridForwardRendererEffect.h"

extern std::wstring gEngineRootDir;
extern BaseApp* gApp;

HybridForwardRendererEffect::HybridForwardRendererEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mVsBlob = nullptr;
	mVS = nullptr;
	mDefaultPsBlob = nullptr;
	mDefaultPS = nullptr;
	mDiffusePsBlob = nullptr;
	mDiffusePS = nullptr;
	mBumpPsBlob = nullptr;
	mBumpPS = nullptr;
	mDiffBumpPsBlob = nullptr;
	mDiffBumpPS = nullptr;

	mInitRayLinksCsBlob = nullptr;
	mInitRayLinksCS = nullptr;

	mBoundedDiffuseMap = nullptr;
	mBoundedNormalMap = nullptr;
	mBoundedReflectionMap = nullptr;

	mDiffuseMapSampler = nullptr;
	mNormalMapSampler = nullptr;
	mReflectionMapSampler = nullptr;

	mDiffuseMap = nullptr;
	mNormalMap = nullptr;
	mReflectionMap = nullptr;

	//mRayLinksUav = nullptr;

	mRayHitUav = nullptr;
	mRayListUav = nullptr;
	mRayVoxelIdsUav = nullptr;

	mMetricsUav = nullptr;

	mShadowMapSampler = nullptr;

	mPerFrameCb.Create(renderer->Device());
	mPerObjectCb.Create(renderer->Device());

	ZeroMemory(&mPerFrameVariables, sizeof(CbPerFrame));
	ZeroMemory(&mPerObjectVariables, sizeof(CbPerObject));

	mRendererRef = renderer.get(); 
	Init(mRendererRef);
}

/*virtual*/
HybridForwardRendererEffect::~HybridForwardRendererEffect()
{    
	if (mVsBlob) 
		mVsBlob->Release();
	if (mVS) 
		mVS->Release();
	if (mDefaultPsBlob) 
		mDefaultPsBlob->Release();
	if (mDefaultPS) 
		mDefaultPS->Release();
	if (mDiffusePsBlob) 
		mDiffusePsBlob->Release();
	if (mDiffusePS) 
		mDiffusePS->Release();
	if (mBumpPsBlob)
		mBumpPsBlob->Release();
	if (mBumpPS) 
		mBumpPS->Release();
	if (mDiffBumpPsBlob) 
		mDiffBumpPsBlob->Release();
	if (mDiffBumpPS) 
		mDiffBumpPS->Release();
	if (mInitRayLinksCsBlob)
		mInitRayLinksCsBlob->Release();
	if (mInitRayLinksCS)
		mInitRayLinksCS->Release();
}

void
HybridForwardRendererEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	cso_name = std::wstring(HR_FR_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mVsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mVsBlob->GetBufferPointer(),
		mVsBlob->GetBufferSize(),
		NULL,
		&mVS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FR_DEFAULT_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDefaultPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDefaultPsBlob->GetBufferPointer(),
		mDefaultPsBlob->GetBufferSize(),
		NULL,
		&mDefaultPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FR_DIFFUSE_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDiffusePsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDiffusePsBlob->GetBufferPointer(),
		mDiffusePsBlob->GetBufferSize(),
		NULL,
		&mDiffusePS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FR_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mBumpPsBlob->GetBufferPointer(),
		mBumpPsBlob->GetBufferSize(),
		NULL,
		&mBumpPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FR_DIFFUSE_BUMP_PS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mDiffBumpPsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mDiffBumpPsBlob->GetBufferPointer(),
		mDiffBumpPsBlob->GetBufferSize(),
		NULL,
		&mDiffBumpPS);
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FR_INIT_RESOURCES_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), &mInitRayLinksCsBlob);
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mInitRayLinksCsBlob->GetBufferPointer(),
		mInitRayLinksCsBlob->GetBufferSize(),
		NULL,
		&mInitRayLinksCS);
	assert(SUCCEEDED(hr));

	{
		// Create input layout descs
		std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

		input_layout_desc.push_back(InputLayoutManager::GEOMETRY);
		input_layout_desc.push_back(InputLayoutManager::TEXTURE);

		mInputLayoutDescs[FR_DEFAULT_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_DIFFUSE_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_BUMP_TECH] = input_layout_desc;
		mInputLayoutDescs[FR_DIFFUSE_BUMP_TECH] = input_layout_desc;

		mInputLayouts[FR_DEFAULT_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_DIFFUSE_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());
		mInputLayouts[FR_DIFFUSE_BUMP_TECH] = renderer->InputLayoutMngr()->GetOrCreateInputLayout(input_layout_desc, mVsBlob, renderer->Device());		
	}
}

void
HybridForwardRendererEffect::UpdateLightArrayVariable(std::vector<LightToGpu> const& lights)
{
	for (int i = 0; i < (std::min)((int)lights.size(), MAX_LIGHTS); i++)
		mPerFrameVariables.lights[i] = lights[i];
}

void
HybridForwardRendererEffect::UpdateNumLightsVariable(unsigned int const& numLights)
{
	mPerFrameVariables.numLights = numLights;
}

void
HybridForwardRendererEffect::UpdateWorldEyePosVariable(DirectX::XMFLOAT3& eyePos)
{
	mPerFrameVariables.eyePos = eyePos;
}

void
HybridForwardRendererEffect::UpdateWorldMatrixVariable(DirectX::XMMATRIX& worldMat)
{
	mPerObjectVariables.worldMat = DirectX::XMMatrixTranspose(worldMat);
}

void
HybridForwardRendererEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& vp)
{
	mPerObjectVariables.vpMat = DirectX::XMMatrixTranspose(vp);
}

void
HybridForwardRendererEffect::UpdateMaterialVariable(Material const& mat)
{
	mPerObjectVariables.material = mat;
}

void
HybridForwardRendererEffect::UpdateFogVariable(Fog const& fog)
{
	mPerFrameVariables.fog = fog;
}

void
HybridForwardRendererEffect::UpdateDiffuseMapVariable(ID3D11ShaderResourceView* const diffuseMap)
{
	mDiffuseMap = diffuseMap;
}

void
HybridForwardRendererEffect::UpdateDiffuseMapSamplerVariable(ID3D11SamplerState* const diffuseSampler)
{
	mDiffuseMapSampler = diffuseSampler;
}

void
HybridForwardRendererEffect::UpdateNormalMapVariable(ID3D11ShaderResourceView* const normalMap)
{
	mNormalMap = normalMap;
}

void
HybridForwardRendererEffect::UpdateNormalMapSamplerVariable(ID3D11SamplerState* const normalSampler)
{
	mNormalMapSampler = normalSampler;
}

void
HybridForwardRendererEffect::UpdateTextureTransformMatrixVariable( DirectX::XMMATRIX& mat )
{
	mPerObjectVariables.texMat = DirectX::XMMatrixTranspose(mat);
}

void 
HybridForwardRendererEffect::UpdateReflectionMapSamplerVariable(ID3D11SamplerState* const reflectionSampler)
{
	mReflectionMapSampler = reflectionSampler;
}

void 
HybridForwardRendererEffect::UpdateReflectionMapVariable(ID3D11ShaderResourceView* const reflectionMap)
{
	mReflectionMap = reflectionMap;
}

void HybridForwardRendererEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (techName == HR_FR_INIT_RESOURCES_TECH)
	{
		ID3D11UnorderedAccessView* uavs[] = { mRayListUav, mRayHitUav, mRayVoxelIdsUav, mMetricsUav };
		unsigned int init_cnts[] = { 0, 0, 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 4, uavs, init_cnts);

		deviceContext->CSSetShader(mInitRayLinksCS, nullptr, 0);
		return;
	}

	auto default_st = mRendererRef->StateMngr()->DefaultSamplerState();
	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11Buffer* nb = nullptr;

	mPerObjectCb.SetData(deviceContext, mPerObjectVariables);

	auto cb = mPerObjectCb.GetBuffer();
	deviceContext->VSSetConstantBuffers(1, 1, &cb);
	deviceContext->PSSetConstantBuffers(1, 1, &cb);

	
	if (mDiffuseMapSampler)
		deviceContext->PSSetSamplers(0, 1, &mDiffuseMapSampler);
	else
		deviceContext->PSSetSamplers(0, 1, &default_st);

	if (mNormalMapSampler)
		deviceContext->PSSetSamplers(1, 1, &mNormalMapSampler);
	else
		deviceContext->PSSetSamplers(1, 1, &default_st);

	if (mReflectionMapSampler)
		deviceContext->PSSetSamplers(2, 1, &mReflectionMapSampler);
	else
		deviceContext->PSSetSamplers(2, 1, &default_st);

	if (mDiffuseMap && mDiffuseMap != mBoundedDiffuseMap)
	{
		deviceContext->PSSetShaderResources(0, 1, &mDiffuseMap);
		mBoundedDiffuseMap = mDiffuseMap;
	}
	if (mNormalMap && mNormalMap != mBoundedNormalMap)
	{
		deviceContext->PSSetShaderResources(1, 1, &mNormalMap);
		mBoundedNormalMap = mNormalMap;
	}
	if (mReflectionMap && mReflectionMap != mBoundedReflectionMap)
	{
		deviceContext->PSSetShaderResources(2, 1, &mReflectionMap);
		mBoundedReflectionMap = mReflectionMap;
	}
	

	if (FR_DEFAULT_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDefaultPS, nullptr, 0);
	}
	else if (FR_DIFFUSE_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDiffusePS, nullptr, 0);
	}
	else if (FR_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mBumpPS, nullptr, 0);
	}
	else if (FR_DIFFUSE_BUMP_TECH == techName)
	{
		deviceContext->VSSetShader(mVS, nullptr, 0);
		deviceContext->PSSetShader(mDiffBumpPS, nullptr, 0);
	}
}

void HybridForwardRendererEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{	
	if (techName == HR_FR_INIT_RESOURCES_TECH) 
	{
		ID3D11UnorderedAccessView* nuavs[] = { nullptr, nullptr, nullptr, nullptr };
		unsigned int init_cnts[] = { 0, 0, 0, 0 };

		deviceContext->CSSetShader(nullptr, nullptr, 0);
		deviceContext->CSSetUnorderedAccessViews(0, 4, &nuavs[0], init_cnts);
	}
	else
	{
		deviceContext->VSSetShader(nullptr, nullptr, 0);
		deviceContext->PSSetShader(nullptr, nullptr, 0);
	}
}

void 
HybridForwardRendererEffect::ApplyPerFrameConstants(ID3D11DeviceContext* const deviceContext)
{
	mPerFrameCb.SetData(deviceContext, mPerFrameVariables);

	auto cb = mPerFrameCb.GetBuffer();
	deviceContext->CSSetConstantBuffers(0, 1, &cb);
	deviceContext->VSSetConstantBuffers(0, 1, &cb);
	deviceContext->PSSetConstantBuffers(0, 1, &cb);	
}

void 
HybridForwardRendererEffect::UnbindAll(ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;
	deviceContext->VSSetConstantBuffers(0, 1, &nb);
	deviceContext->VSSetConstantBuffers(1, 1, &nb);
	deviceContext->PSSetConstantBuffers(0, 1, &nb);
	deviceContext->PSSetConstantBuffers(1, 1, &nb);

	ID3D11ShaderResourceView* nsrv = nullptr;
	ID3D11SamplerState* ns = nullptr;

	deviceContext->PSSetShaderResources(0, 1, &nsrv);
	deviceContext->PSSetSamplers(0, 1, &ns);
	deviceContext->PSSetShaderResources(1, 1, &nsrv);
	deviceContext->PSSetSamplers(1, 1, &ns);
	deviceContext->PSSetShaderResources(2, 1, &nsrv);
	deviceContext->PSSetSamplers(2, 1, &ns);

	deviceContext->PSSetSamplers(3, 1, &ns);
	for (auto i = 0u; i < (unsigned int)mShadowMapCascades.size(); i++)
		deviceContext->PSSetShaderResources(3 + i, 1, &nsrv);
		
	mBoundedDiffuseMap = nullptr;
	mBoundedNormalMap = nullptr;
	mBoundedReflectionMap = nullptr;

	mDiffuseMapSampler = nullptr;
	mNormalMapSampler = nullptr;
	mReflectionMapSampler = nullptr;

	mDiffuseMap = nullptr;
	mNormalMap = nullptr;
	mReflectionMap = nullptr;


	ID3D11UnorderedAccessView* uavs[5] = {
		nullptr,
		nullptr,
		nullptr, 
		nullptr, 
		nullptr
	};
	deviceContext->OMSetRenderTargetsAndUnorderedAccessViews(D3D11_KEEP_RENDER_TARGETS_AND_DEPTH_STENCIL, nullptr, nullptr, 1, 5, &uavs[0], 0);

	//mRayLinksUav = nullptr;

	mRayListUav = nullptr;
	mRayHitUav = nullptr;
}

void 
HybridForwardRendererEffect::UpdateRayUavs(
	ID3D11UnorderedAccessView* const shadowRayListUav, 
	ID3D11UnorderedAccessView* const shadowRayHitUav,
	ID3D11UnorderedAccessView* const shadowRayVoxelIdsUav)
{
	mRayListUav = shadowRayListUav;
	mRayHitUav = shadowRayHitUav;	
	mRayVoxelIdsUav = shadowRayVoxelIdsUav;
}

void
HybridForwardRendererEffect::ApplyRasterizationRenderingResources(ID3D11DeviceContext* const deviceContext)
{

	ID3D11UnorderedAccessView* uavs[] = {
		mRayHitUav,
		mRayVoxelIdsUav
	};
	UINT cnts[] = {
		0, 0
	};
	deviceContext->OMSetRenderTargetsAndUnorderedAccessViews(D3D11_KEEP_RENDER_TARGETS_AND_DEPTH_STENCIL, nullptr, nullptr, 3, 2, &uavs[0], &cnts[0]);


	if (!mShadowMapCascades.empty())
	{
		deviceContext->PSSetShaderResources(3, (unsigned int)mShadowMapCascades.size(), &mShadowMapCascades[0]);
	}
	deviceContext->PSSetSamplers(3, 1, &mShadowMapSampler);	

	D3D11_SAMPLER_DESC samp_desc;
	samp_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.MipLODBias = 0.0f;
	samp_desc.MaxAnisotropy = 1;
	samp_desc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	samp_desc.BorderColor[0] = samp_desc.BorderColor[1] = samp_desc.BorderColor[2] = samp_desc.BorderColor[3] = 0;
	samp_desc.MinLOD = 0;
	samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
	auto sampler = gApp->Renderer()->StateMngr()->GetOrCreateSamplerState(gApp->Renderer()->Device(), samp_desc);
	deviceContext->PSSetSamplers(4, 1, &sampler);
}

void HybridForwardRendererEffect::UpdateMetricsUav(ID3D11UnorderedAccessView* const uav)
{
	mMetricsUav = uav;
}

void 
HybridForwardRendererEffect::UpdateShadowingVariables(ID3D11SamplerState* const shadowMapSampler, std::vector<ID3D11ShaderResourceView*> const& shadowMapCascades)
{
	mShadowMapCascades.clear();
	mShadowMapSampler = shadowMapSampler;
	mShadowMapCascades = shadowMapCascades;
}

// void 
// HybridForwardRendererEffect::UpdateRayLinksUav(ID3D11UnorderedAccessView* const rayLinksUav)
// {
// 	mRayLinksUav = rayLinksUav;
// }
