#include <vector>
#include <algorithm>
#include <d3d11.h>
#include <DirectXMath.h>
#include "Debug/Debug.h"

#include "Systems/Renderer/Renderer.h"
#include "BaseApp.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "Utils/ComponentFetches.h"
#include "GpuProfiler.hpp"

#include "SimulationGridEffect.h"
#include "HybridForwardRendererEffect.h"
#include "RayMarchingEffect.h"
#include "ShadowPenumbraEffect.h"

// Needed components
#include "Components/Renderable.h"
#include "Components/DiffuseMapped.h"
#include "Components/NormalMapped.h"
#include "Components/ReflectionMapped.h"
#include "Components/Spatialized.h"
#include "Components/TextureTransformed.h"
#include "Voxelized.h"

#include "HybridPipeline.h"


extern BaseApp* gApp;

bool
HybridPipeline::FrontToBackCompare(unsigned int const& lhs, unsigned int const& rhs)
{
	auto camera = gApp->Renderer()->CurrentCamera();
	Spatialized* lhs_spatial_comp = SpatializedFromEntity(lhs);
	Spatialized* rhs_spatial_comp = SpatializedFromEntity(rhs);

	auto cam_pos = camera->Position();
	auto lhs_pos = lhs_spatial_comp->LocalPosition();
	auto rhs_pos = rhs_spatial_comp->LocalPosition();

	DirectX::XMVECTOR lhs_pos_minus_cam_pos = DirectX::XMVectorSet(
		lhs_pos.x - cam_pos.x,
		lhs_pos.y - cam_pos.y, 
		lhs_pos.z - cam_pos.z, 
		1.f);

	DirectX::XMVECTOR rhs_pos_minus_cam_pos = DirectX::XMVectorSet(
		rhs_pos.x - cam_pos.x,
		rhs_pos.y - cam_pos.y,
		rhs_pos.z - cam_pos.z,
		1.f);

	auto lhs_lgnth_sqr = DirectX::XMVectorGetByIndex(DirectX::XMVector3LengthSq(lhs_pos_minus_cam_pos), 0);
	auto rhs_lgnth_sqr = DirectX::XMVectorGetByIndex(DirectX::XMVector3LengthSq(rhs_pos_minus_cam_pos), 0);

	return lhs_lgnth_sqr < rhs_lgnth_sqr;
}


HybridPipeline::HybridPipeline(std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EntityManager> entityManager) : Pipeline(renderer), System(entityManager)
{
	std::set<unsigned int> fam_req;
	fam_req.insert(Voxelized::GetGuid());
	SetFamilyRequirements(fam_req);

	mGridEffect = std::make_shared<SimulationGridEffect>(renderer);
	mForwardRendererEffect = std::make_shared<HybridForwardRendererEffect>(renderer);
	mRayMarchingEffect = std::make_shared<RayMarchingEffect>(renderer);	
	mShadowPanumbraEffect = std::make_shared<ShadowPenumbraEffect>(renderer);

	mInitialized = false;
	mDoSimGridUpdates = true;
	mDoMetricsCollection = false;
		
#ifdef _DEBUG
	assert(SUCCEEDED(renderer->Device()->QueryInterface(__uuidof(ID3D11InfoQueue), (void**)mD3dInfoQueue.ReleaseAndGetAddressOf())));
	//set up the list of messages to filter
	D3D11_MESSAGE_ID messageIDs[] = { 
		D3D11_MESSAGE_ID_QUERY_BEGIN_ABANDONING_PREVIOUS_RESULTS,
		D3D11_MESSAGE_ID_QUERY_END_ABANDONING_PREVIOUS_RESULTS
	};

	//set the DenyList to use the list of messages
	D3D11_INFO_QUEUE_FILTER filter = { 0, 0 };
	filter.DenyList.NumIDs = 2;
	filter.DenyList.pIDList = messageIDs;

	//apply the filter to the info queue
	mD3dInfoQueue->AddStorageFilterEntries(&filter);
#endif

	mVoxelGridResources.dummyTileIndex = 0;
	mVoxelGridResources.tilePool = nullptr;
	mVoxelGridResources.voxelGridBuff = nullptr;
	mVoxelGridResources.voxelGridUav = nullptr;
	mVoxelGridResources.dummyTileBuff = nullptr;

	mRayPacketResources.dummyTileIndex = 0;	
	mRayPacketResources.tilePool = nullptr;
	mRayPacketResources.rayPacketsBuff = nullptr;
	mRayPacketResources.rayPacketsUav = nullptr;
	mRayPacketResources.dummyTileBuff = nullptr;

	D3D11_BUFFER_DESC tile_rb_buff_desc;
	ZeroMemory(&tile_rb_buff_desc, sizeof(tile_rb_buff_desc));
	tile_rb_buff_desc.BindFlags = 0;
	tile_rb_buff_desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	tile_rb_buff_desc.StructureByteStride = sizeof(float);
	tile_rb_buff_desc.ByteWidth = BYTES_PER_TILE;
	tile_rb_buff_desc.Usage = D3D11_USAGE_STAGING;
	tile_rb_buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	HRESULT hr = gApp->Renderer()->Device()->CreateBuffer(&tile_rb_buff_desc, NULL, mTriTileReadBackBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));


	mScanPrimitive = std::make_shared<ScanPrimitive>(gApp->WinWidth() * gApp->WinHeight() / MAX_NUM_RAYS_PER_PACKET, renderer->DeviceContext(), renderer->Device());


	// Create state descs
	ZeroMemory(&mShadowResolveDepthStencilDesc, sizeof(mShadowResolveDepthStencilDesc));
	mShadowResolveDepthStencilDesc.DepthEnable = false;
	mShadowResolveDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	mShadowResolveDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	mShadowResolveDepthStencilDesc.StencilEnable = true;
	mShadowResolveDepthStencilDesc.StencilReadMask = 0xFF;
	mShadowResolveDepthStencilDesc.StencilWriteMask = 0xFF;
	mShadowResolveDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mShadowResolveDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	mShadowResolveDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mShadowResolveDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	mShadowResolveDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mShadowResolveDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	mShadowResolveDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mShadowResolveDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	ZeroMemory(&mShadowResolveBlendDesc, sizeof(mShadowResolveBlendDesc));
	mShadowResolveBlendDesc.AlphaToCoverageEnable = false;
	mShadowResolveBlendDesc.IndependentBlendEnable = false;
	mShadowResolveBlendDesc.RenderTarget[0].BlendEnable = true;
	mShadowResolveBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ZERO;//D3D11_BLEND_ONE;
	mShadowResolveBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_COLOR;//D3D11_BLEND_ONE;
	mShadowResolveBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	mShadowResolveBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;//D3D11_BLEND_ONE;
	mShadowResolveBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_SRC_ALPHA;//D3D11_BLEND_ONE;
	mShadowResolveBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	mShadowResolveBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;


	mNumRayMarches = 2;

	mMaxPenumbraDilation = 10;
	
	InitDeferredBuffers();
	InitRayMarchingResources();
	InitShadowRayResources();
	InitMetricResources();
}


HybridPipeline::~HybridPipeline()
{

}

void 
HybridPipeline::MadeActive(void)
{

}

void 
HybridPipeline::MadeInactive(void)
{
	mEntitiesWithVoxelizedComponent.clear();
}

void 
HybridPipeline::EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime)
{	
	auto dev_c = gApp->Renderer()->DeviceContext();

	gGpuProfiler.BeginFrame();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Forward Render Pass", 0);
	ForwardRenderingPass(familyByRenderableType);
	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Intermediate Pass", 0);
	IntermediatePass();
	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Simulation Grid Init", 0);
	InitSimulationGridPass();
	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Ray March", 0);
	RayMarchingPass();
	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();

	if (dev_c->IsAnnotationEnabled())
		dev_c->BeginEventInt(L"Resolve", 0);
	ResolvePass();
	if (dev_c->IsAnnotationEnabled())
		dev_c->EndEvent();

	// Advance ray samples
	AdvanceRaySamples(mShadowRaysResources);

	gGpuProfiler.EndFrame();
}

void 
HybridPipeline::RecompileShaders()
{
	mGridEffect->Recompile();
}

void 
HybridPipeline::RunImplementation(std::set<unsigned int> const* family, double const currentTime)
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	// TMP CODE BELOW /////////////////////////////
	// Testing reading mem-back capabilities   ////
	///////////////////////////////////////////////
	//static unsigned int step = 4;

	//// Init
	//static D3D11_BUFFER_DESC mReadBackBufferDesc;
	//static ID3D11Buffer* mReadBackBuffer = nullptr;
	//if (!mReadBackBuffer)
	//{
	//	ZeroMemory(&mReadBackBufferDesc, sizeof(mReadBackBufferDesc));
	//	mReadBackBufferDesc.BindFlags = 0;
	//	mReadBackBufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	//	mReadBackBufferDesc.StructureByteStride = sizeof(int);
	//	mReadBackBufferDesc.ByteWidth = sizeof(float) * 512 * 512 * 512; // 4 bytes * 512^3 = 512 MB
	//	mReadBackBufferDesc.Usage = D3D11_USAGE_STAGING;
	//	mReadBackBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	//	HRESULT hr = gApp->Renderer()->Device()->CreateBuffer(&mReadBackBufferDesc, NULL, &mReadBackBuffer);
	//	assert(SUCCEEDED(hr));
	//}

	//static D3D11_MAPPED_SUBRESOURCE mapped;

	//switch (step)
	//{
	//case 0:
	//	renderer->DeviceContext()->CopyResource(mReadBackBuffer, mBinaryVoxelGridBuff);
	//	step++;
	//	break;
	//case 1:
	//	step++;
	//	break;
	//case 2:		
	//	step++;
	//	break;
	//case 5:
	//	renderer->DeviceContext()->Map(mReadBackBuffer, 0, D3D11_MAP_READ, 0, &mapped);
	//	renderer->DeviceContext()->Unmap(mReadBackBuffer, 0);
	//	step = 0;
	//	break;
	//case 10: // give 10-3 frames of latency when first starting to ensure all resources were initialized
	//	step = 0;
	//	break;
	//default:
	//	step++;
	//	break;
	//}
	
	///////////////////////////////////////////////
	///////////////////////////////////////////////

	// Update which entities are to be voxelized
	mEntitiesWithVoxelizedComponent = std::set<unsigned int>(family->begin(), family->end());
}

void 
HybridPipeline::InitGridResources(
	float const voxelLngth, 	 
	unsigned int const numPhysicalTiles)
{
	mRayPacketsMapped = false;

	// This function creates 2 pools of tiles for the voxel grid which holds actual geometry
	// and the ray grid which stores traveling rays during ray marching.
	// Since this function is called by specifying the size of the total amount of memory to be used
	// numPhysicalTiles needs to be divided by 2.
	unsigned int num_tiles = numPhysicalTiles / 2;

	// 30^3 * 64KB = 1.64795 GB (nearly the maximum size for a DX 11 resource of 2 GB)  
	// Each voxel corresponds to a tile. A tile can house around 1820 triangles. 
	// The simulation grid can house 1820 * 30^3 = 49,140,000 evenly sparsed triangles.
	const unsigned int num_voxel_length = NUM_VOXEL_LENGTH;

	SimulationGrid::SimulationGridAttributes attrs;
	attrs.numVoxelsPerLength = num_voxel_length;
	attrs.voxelSizePerLength = voxelLngth;
	mSimulationGrid.Init(attrs);

	unsigned int buffer_size = num_voxel_length*num_voxel_length*num_voxel_length;

	// VOXEL GRID ////////////////////////////////////////////////////////////////////////////////////////////////	
	ZeroMemory(&mVoxelGridResources.tilePoolDesc, sizeof(mVoxelGridResources.tilePoolDesc));
	mVoxelGridResources.tilePoolDesc.ByteWidth = BYTES_PER_TILE * num_tiles; // 512 MB, fits 8192 tiles
	mVoxelGridResources.tilePoolDesc.Usage = D3D11_USAGE_DEFAULT;
	mVoxelGridResources.tilePoolDesc.MiscFlags = D3D11_RESOURCE_MISC_TILE_POOL;
	HRESULT hr = gApp->Renderer()->Device()->CreateBuffer(&mVoxelGridResources.tilePoolDesc, NULL, mVoxelGridResources.tilePool.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));



	ZeroMemory(&mVoxelGridResources.voxelGridBuffDesc, sizeof(mVoxelGridResources.voxelGridBuffDesc));
	mVoxelGridResources.voxelGridBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mVoxelGridResources.voxelGridBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_TILED | D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
	mVoxelGridResources.voxelGridBuffDesc.StructureByteStride = BYTES_PER_TILE; // 64KB tile
	mVoxelGridResources.voxelGridBuffDesc.ByteWidth = mVoxelGridResources.voxelGridBuffDesc.StructureByteStride * buffer_size;
	mVoxelGridResources.voxelGridBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mVoxelGridResources.voxelGridBuffDesc.CPUAccessFlags = 0;
	hr = gApp->Renderer()->Device()->CreateBuffer(&mVoxelGridResources.voxelGridBuffDesc, NULL, mVoxelGridResources.voxelGridBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	ZeroMemory(&mVoxelGridResources.voxelGridUavDesc, sizeof(mVoxelGridResources.voxelGridUavDesc));
	mVoxelGridResources.voxelGridUavDesc.Buffer.NumElements = mVoxelGridResources.voxelGridBuffDesc.ByteWidth / sizeof(float); // 16384 * 30^3 = 442368000 4-byte values
	mVoxelGridResources.voxelGridUavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	mVoxelGridResources.voxelGridUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mVoxelGridResources.voxelGridUavDesc.Buffer.FirstElement = 0;
	mVoxelGridResources.voxelGridUavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView((ID3D11Resource *)mVoxelGridResources.voxelGridBuff.Get(), &mVoxelGridResources.voxelGridUavDesc, mVoxelGridResources.voxelGridUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	D3D11_BUFFER_DESC dummy_tile_buff_desc;
	ZeroMemory(&dummy_tile_buff_desc, sizeof(dummy_tile_buff_desc));
	dummy_tile_buff_desc.ByteWidth = BYTES_PER_TILE; // 64KB tile
	dummy_tile_buff_desc.MiscFlags = D3D11_RESOURCE_MISC_TILED;
	dummy_tile_buff_desc.Usage = D3D11_USAGE_DEFAULT;
	hr = gApp->Renderer()->Device()->CreateBuffer(&dummy_tile_buff_desc, NULL, mVoxelGridResources.dummyTileBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	// Map the single tile in the buffer to physical tile 0. 
	// Note that tile 0 in the tile pool is used as the dummy tile.
	D3D11_TILED_RESOURCE_COORDINATE start_coord;
	ZeroMemory(&start_coord, sizeof(start_coord));
	D3D11_TILE_REGION_SIZE region_size;
	ZeroMemory(&region_size, sizeof(region_size));
	region_size.NumTiles = 1;
	UINT range_flags = D3D11_TILE_RANGE_REUSE_SINGLE_TILE;	
	gApp->Renderer()->DeviceContext()->UpdateTileMappings(
		mVoxelGridResources.dummyTileBuff.Get(),
		1,
		&start_coord,
		&region_size,
		mVoxelGridResources.tilePool.Get(),
		1,
		&range_flags,
		&mVoxelGridResources.dummyTileIndex,
		nullptr,
		0);

	// Clear the tile to zeros.
	byte deafult_tile_data[BYTES_PER_TILE];
	ZeroMemory(deafult_tile_data, sizeof(deafult_tile_data));
	gApp->Renderer()->DeviceContext()->UpdateTiles(mVoxelGridResources.dummyTileBuff.Get(), &start_coord, &region_size, deafult_tile_data, 0);

	// Since the runtime doesn't know that all other tiled resources in this sample will point
	// to the data written via the dummy tile buffer, insert a manual barrier to eliminate the hazard.
	gApp->Renderer()->DeviceContext()->TiledResourceBarrier(mVoxelGridResources.dummyTileBuff.Get(), NULL);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	// RAY   GRID ////////////////////////////////////////////////////////////////////////////////////////////////	
	mMaxNumPacketsGenerated = num_tiles * NUM_RAY_PACKETS_PER_TILE;

	ZeroMemory(&mRayPacketResources.tilePoolDesc, sizeof(mRayPacketResources.tilePoolDesc));
	mRayPacketResources.tilePoolDesc.ByteWidth = BYTES_PER_TILE * num_tiles;
	mRayPacketResources.tilePoolDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayPacketResources.tilePoolDesc.MiscFlags = D3D11_RESOURCE_MISC_TILE_POOL;
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayPacketResources.tilePoolDesc, NULL, mRayPacketResources.tilePool.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	ZeroMemory(&mRayPacketResources.rayPacketsBuffDesc, sizeof(mRayPacketResources.rayPacketsBuffDesc));
	mRayPacketResources.rayPacketsBuffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mRayPacketResources.rayPacketsBuffDesc.MiscFlags = D3D11_RESOURCE_MISC_TILED | D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
	mRayPacketResources.rayPacketsBuffDesc.StructureByteStride = BYTES_PER_TILE; // 64KB tile
	mRayPacketResources.rayPacketsBuffDesc.ByteWidth = mRayPacketResources.rayPacketsBuffDesc.StructureByteStride * num_tiles;// buffer_size;
	mRayPacketResources.rayPacketsBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayPacketResources.rayPacketsBuffDesc.CPUAccessFlags = 0;
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayPacketResources.rayPacketsBuffDesc, NULL, mRayPacketResources.rayPacketsBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	ZeroMemory(&mRayPacketResources.rayPacketsUavDesc, sizeof(mRayPacketResources.rayPacketsUavDesc));
	mRayPacketResources.rayPacketsUavDesc.Buffer.NumElements = mRayPacketResources.rayPacketsBuffDesc.ByteWidth / sizeof(float);
	mRayPacketResources.rayPacketsUavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	mRayPacketResources.rayPacketsUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mRayPacketResources.rayPacketsUavDesc.Buffer.FirstElement = 0;
	mRayPacketResources.rayPacketsUavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView((ID3D11Resource *)mRayPacketResources.rayPacketsBuff.Get(), &mRayPacketResources.rayPacketsUavDesc, mRayPacketResources.rayPacketsUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	////D3D11_BUFFER_DESC dummy_tile_buff_desc;
	////ZeroMemory(&dummy_tile_buff_desc, sizeof(dummy_tile_buff_desc));
	////dummy_tile_buff_desc.ByteWidth = BYTES_PER_TILE; // 64KB tile
	////dummy_tile_buff_desc.MiscFlags = D3D11_RESOURCE_MISC_TILED;
	////dummy_tile_buff_desc.Usage = D3D11_USAGE_DEFAULT;
	//hr = gApp->Renderer()->Device()->CreateBuffer(&dummy_tile_buff_desc, NULL, &mRayPacketResources.dummyTileBuff);
	//assert(SUCCEEDED(hr));

	//// Map the single tile in the buffer to physical tile 0. 
	//// Note that tile 0 in the tile pool is used as the dummy tile.
	////D3D11_TILED_RESOURCE_COORDINATE start_coord;
	////ZeroMemory(&start_coord, sizeof(start_coord));
	////D3D11_TILE_REGION_SIZE region_size;
	////ZeroMemory(&region_size, sizeof(region_size));
	////region_size.NumTiles = 1;
	////UINT range_flags = D3D11_TILE_RANGE_REUSE_SINGLE_TILE;
	//gApp->Renderer()->DeviceContext()->UpdateTileMappings(
	//	mRayPacketResources.dummyTileBuff,
	//	1,
	//	&start_coord,
	//	&region_size,
	//	mRayPacketResources.tilePool,
	//	1,
	//	&range_flags,
	//	&mRayPacketResources.dummyTileIndex,
	//	nullptr,
	//	0);

	//// Clear the tile to zeros.
	////byte deafult_tile_data[BYTES_PER_TILE];
	////ZeroMemory(deafult_tile_data, sizeof(deafult_tile_data));
	//gApp->Renderer()->DeviceContext()->UpdateTiles(mRayPacketResources.dummyTileBuff, &start_coord, &region_size, deafult_tile_data, 0);

	//// Since the runtime doesn't know that all other tiled resources in this sample will point
	//// to the data written via the dummy tile buffer, insert a manual barrier to eliminate the hazard.
	//gApp->Renderer()->DeviceContext()->TiledResourceBarrier(mRayPacketResources.dummyTileBuff, NULL);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


void 
HybridPipeline::MapTiles(std::set<unsigned int>& entitiesInSimulation)
{
	static unsigned int prev_num_occupied_voxels = 0;

	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto num_physical_tiles = NumOfPhysicalTiles();

	mOccupiedVoxels.clear();

	std::vector<D3D11_TILED_RESOURCE_COORDINATE> tile_coords;
	std::vector<D3D11_TILE_REGION_SIZE> region_sizes;	

	for (auto entity : entitiesInSimulation)
	{
		std::set<unsigned int> voxel_ids;

		auto aabb_comp = BoundedFromEntity(entity);
		auto xform_comp = SpatializedFromEntity(entity);
		auto cam_pos = renderer->CurrentCamera()->Position();
		DirectX::XMFLOAT3 grid_center = { cam_pos.x, cam_pos.y, cam_pos.z };
		auto aabb = aabb_comp->AabbBounds();
		aabb.Transform(aabb, XMLoadFloat4x4(&xform_comp->LocalTransformation()));
		voxel_ids = mSimulationGrid.IntersectGridWithAabb(grid_center, aabb);

		mOccupiedVoxels.insert(voxel_ids.begin(), voxel_ids.end());
	}

	unsigned int num_occupied_voxels = (unsigned int)mOccupiedVoxels.size();	

	// Clear mapping
	// TODO: updating the virtual table is costly and should not be done every frame... need to find a smarter dynamic solution.

	// Point to dummy tile for tier 1 
	{
		D3D11_TILED_RESOURCE_COORDINATE start_coord;
		ZeroMemory(&start_coord, sizeof(start_coord));
		D3D11_TILE_REGION_SIZE region_size;
		ZeroMemory(&region_size, sizeof(region_size));
		region_size.NumTiles = mSimulationGrid.NumVoxelsPerLength()*mSimulationGrid.NumVoxelsPerLength()*mSimulationGrid.NumVoxelsPerLength();
		UINT range_flags = D3D11_TILE_RANGE_REUSE_SINGLE_TILE;
		renderer->DeviceContext()->UpdateTileMappings(
			mVoxelGridResources.voxelGridBuff.Get(),
			1,
			&start_coord,
			&region_size,
			mVoxelGridResources.tilePool.Get(),
			1,
			&range_flags,
			&mVoxelGridResources.dummyTileIndex,
			nullptr,
			0);

		//if (!mRayPacketsMapped)
		//	renderer->DeviceContext()->UpdateTileMappings(
		//		mRayPacketResources.rayPacketsBuff,
		//		1,
		//		&start_coord,
		//		&region_size,
		//		mRayPacketResources.tilePool,
		//		1,
		//		&range_flags,
		//		&mRayPacketResources.dummyTileIndex,
		//		nullptr,
		//		0);
	}

	// Map occupied voxels
	{ // Voxel Grid
		std::vector<D3D11_TILED_RESOURCE_COORDINATE> trcs;
		std::vector<D3D11_TILE_REGION_SIZE> trss;
		std::vector<UINT> tile_pool_start;
		std::vector<UINT> tile_ranges;
		auto voxel_ids_iter = mOccupiedVoxels.begin();
		for (unsigned int i = 1; i <= (std::min)(num_physical_tiles, num_occupied_voxels); i++) // start at index 1 since 0 is for the dummy tile
		{
			D3D11_TILED_RESOURCE_COORDINATE trc{ *voxel_ids_iter, 0, 0, 0 };
			D3D11_TILE_REGION_SIZE trs{ 1, false, 0, 0, 0 };
			UINT tile_range = 1;
			trcs.push_back(trc);
			trss.push_back(trs);
			tile_ranges.push_back(tile_range);
			tile_pool_start.push_back(i);
			voxel_ids_iter++;
		}
		if (!trcs.empty())
			renderer->DeviceContext()->UpdateTileMappings(
				mVoxelGridResources.voxelGridBuff.Get(),
				(unsigned int)trcs.size(),
				&trcs[0],
				&trss[0],
				mVoxelGridResources.tilePool.Get(),
				(unsigned int)trcs.size(),
				NULL,
				&tile_pool_start[0],
				&tile_ranges[0],
				D3D11_TILE_MAPPING_NO_OVERWRITE);

		if (num_occupied_voxels != prev_num_occupied_voxels)
		{
			if (num_occupied_voxels > num_physical_tiles)
				OutputDebugMsg("WARNING:: Couldn't fit all occupied voxels in tile pool! \n");
			OutputDebugMsg("Num. of occupied voxels: " + to_string(num_occupied_voxels) + "\n");
			prev_num_occupied_voxels = num_occupied_voxels;
		}
	}

	{	// Ray Grid		
		if (!mRayPacketsMapped)
		{
			mRayPacketsMapped = true;

			/* Since there could be multiple ray tiles per occupied voxels, the mapping goes like this:
			 * Tile id 0 is the dummy tile for sparse voxels.
			 * All occupied voxels:
			 * for (j = 0; j < numTilesPerVoxel; j++)
			 *     (tile_id * numTilesPerVoxel) + j - 1 ------>  (current_voxel_id * numTilesPerVoxel) + j
			 */
			std::vector<D3D11_TILED_RESOURCE_COORDINATE> trcs;
			std::vector<D3D11_TILE_REGION_SIZE> trss;
			std::vector<UINT> tile_pool_start;
			std::vector<UINT> tile_ranges;

			// For now map all tiles	
			// TODO: make this dynamic in the future
			auto num_tiles_needed = mMaxNumPacketsGenerated / NUM_RAY_PACKETS_PER_TILE;

			for (unsigned int i = 0; i < num_tiles_needed; i++) 
			{
				D3D11_TILED_RESOURCE_COORDINATE trc{ i, 0, 0, 0 };
				D3D11_TILE_REGION_SIZE trs{ 1, false, 0, 0, 0 };
				UINT tile_range = 1;
				trcs.push_back(trc);
				trss.push_back(trs);
				tile_ranges.push_back(tile_range);
				tile_pool_start.push_back(i); 
			}
			if (!trcs.empty())
				renderer->DeviceContext()->UpdateTileMappings(
					mRayPacketResources.rayPacketsBuff.Get(),
					(unsigned int)trcs.size(),
					&trcs[0],
					&trss[0],
					mRayPacketResources.tilePool.Get(),
					(unsigned int)trcs.size(),
					NULL,
					&tile_pool_start[0],
					&tile_ranges[0],
					D3D11_TILE_MAPPING_NO_OVERWRITE);
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// When debugging the grid data structure and mapping, uncomment the following to map very last tile to pool tile 1.
	//{
	//	D3D11_TILED_RESOURCE_COORDINATE trc = { (unsigned int)(pow(mSimulationGrid.NumVoxelsPerLength(), 3) - 1), 0, 0, 0 };
	//	D3D11_TILE_REGION_SIZE trs = { 1, false, 0, 0, 0 };
	//	UINT tile_range = 1;
	//	UINT range_flags = D3D11_TILE_RANGE_REUSE_SINGLE_TILE;
	//	renderer->DeviceContext()->UpdateTileMappings(mVoxelGridResources.mVoxelGridBuff, 1, &trc, &trs, mVoxelGridResources.tilePool, 1, NULL, &tile_range, &tile_range, D3D11_TILE_MAPPING_NO_OVERWRITE);
	//}


}

void 
HybridPipeline::BufferSnapShot(std::shared_ptr<D3dRenderer> renderer)
{
	//////////// DEBUG ///////////////////////////////////////////
	// Just change the boolean while paused and debugging to see
	// read back data. ///////////////////////////////////////////
	//////////////////////////////////////////////////////////////	
	bool snap_shot_read_back = false;
	if (snap_shot_read_back)
	{
		static D3D11_MAPPED_SUBRESOURCE mapped;

		static TrianglesTile tri_dummy_tile;
		static RaysTile ray_dummy_tile;
		static std::vector<TrianglesTile> occupied_tris_tiles;
		static std::vector<RaysTile> occupied_rays_tiles;

		occupied_tris_tiles.clear();
		occupied_rays_tiles.clear();

		// Read back the dummy triangle tile
		{
			D3D11_TILED_RESOURCE_COORDINATE trc = { 0, 0, 0, 0 };
			D3D11_TILE_REGION_SIZE trs = { 1, false, 0, 0, 0 };
			renderer->DeviceContext()->CopyTiles(mVoxelGridResources.dummyTileBuff.Get(), &trc, &trs, mTriTileReadBackBuff.Get(), 0, D3D11_TILE_COPY_SWIZZLED_TILED_RESOURCE_TO_LINEAR_BUFFER);
			assert(SUCCEEDED(renderer->DeviceContext()->Map(mTriTileReadBackBuff.Get(), 0, D3D11_MAP_READ, 0, &mapped)));
			BYTE* mapped_data = reinterpret_cast<BYTE*>(mapped.pData);
			memcpy(&tri_dummy_tile, mapped_data, sizeof(TrianglesTile));
			renderer->DeviceContext()->Unmap(mTriTileReadBackBuff.Get(), 0);
		}

		// Read back occupied triangle tiles
		{
			for (auto voxel_id : mOccupiedVoxels)
			{
				D3D11_TILED_RESOURCE_COORDINATE trc = { voxel_id, 0, 0, 0 };
				D3D11_TILE_REGION_SIZE trs = { 1, false, 0, 0, 0 };
				renderer->DeviceContext()->CopyTiles(mVoxelGridResources.voxelGridBuff.Get(), &trc, &trs, mTriTileReadBackBuff.Get(), 0, D3D11_TILE_COPY_SWIZZLED_TILED_RESOURCE_TO_LINEAR_BUFFER);
				assert(SUCCEEDED(renderer->DeviceContext()->Map(mTriTileReadBackBuff.Get(), 0, D3D11_MAP_READ, 0, &mapped)));
				BYTE* mapped_data = reinterpret_cast<BYTE*>(mapped.pData);
				occupied_tris_tiles.push_back(TrianglesTile());
				memcpy(&occupied_tris_tiles[occupied_tris_tiles.size()-1], mapped_data, sizeof(TrianglesTile));
				renderer->DeviceContext()->Unmap(mTriTileReadBackBuff.Get(), 0);
 			}
		}


		// Read back ray tiles
		{
			for (unsigned int i = 0; i < mMaxNumPacketsGenerated / NUM_RAY_PACKETS_PER_TILE; i++)
			{
				D3D11_TILED_RESOURCE_COORDINATE trc = { i, 0, 0, 0 };
				D3D11_TILE_REGION_SIZE trs = { 1, false, 0, 0, 0 };
				renderer->DeviceContext()->CopyTiles(mRayPacketResources.rayPacketsBuff.Get(), &trc, &trs, mRayTileReadBackBuff.Get(), 0, D3D11_TILE_COPY_SWIZZLED_TILED_RESOURCE_TO_LINEAR_BUFFER);
				assert(SUCCEEDED(renderer->DeviceContext()->Map(mRayTileReadBackBuff.Get(), 0, D3D11_MAP_READ, 0, &mapped)));
				BYTE* mapped_data = reinterpret_cast<BYTE*>(mapped.pData);
				occupied_rays_tiles.push_back(RaysTile());
				memcpy(&occupied_rays_tiles[occupied_rays_tiles.size() - 1], mapped_data, sizeof(RaysTile));
				renderer->DeviceContext()->Unmap(mRayTileReadBackBuff.Get(), 0);
			}
		}

		return;
	}
}

void 
HybridPipeline::ForwardRenderingPass(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType)
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();

	
	// Update eye pos
	DirectX::XMFLOAT4 cam_pos = renderer->CurrentCamera()->Position();
	DirectX::XMFLOAT3 eye_pos = DirectX::XMFLOAT3(cam_pos.x, cam_pos.y, cam_pos.z);
	mForwardRendererEffect->UpdateWorldEyePosVariable(eye_pos);

	std::shared_ptr<Camera> camera = renderer->CurrentCamera();
	DirectX::XMMATRIX view = XMLoadFloat4x4(&(camera->ViewMatrix()));

	// Update View Projection matrix variable
	DirectX::XMMATRIX proj = XMLoadFloat4x4(&(camera->ProjMatrix()));
	DirectX::XMMATRIX vp = view * proj;
	mForwardRendererEffect->UpdateViewProjectionMatrixVariable(vp);

	// No fog
	Fog fog;
	mForwardRendererEffect->UpdateFogVariable(fog);

	int disp_params[3] = { gApp->WinWidth() / 32, gApp->WinHeight() / 32, 1 };

	mForwardRendererEffect->UpdateNumVoxelsPerLengthVariable(mSimulationGrid.NumVoxelsPerLength());

	mForwardRendererEffect->UpdateDispatchParamsVariable(disp_params[0], disp_params[1], disp_params[2]);

	mForwardRendererEffect->UpdateViewportSizeVariable(gApp->WinWidth(), gApp->WinHeight());

	mForwardRendererEffect->UpdateSimGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4(
		camera->Position().x,
		camera->Position().y,
		camera->Position().z,
		mSimulationGrid.VoxelSizePerLength()));
	
	
	// Reset ray lists
	if (mDoMetricsCollection)
		mForwardRendererEffect->UpdateMetricsUav(mMetricResources.intersectsPerFrameUav.Get());
	else
		mForwardRendererEffect->UpdateMetricsUav(nullptr);
	auto curr_sample = mShadowRaysResources.numComputedSamples;
	mForwardRendererEffect->UpdateRayUavs(mShadowRaysResources.rayListUav[curr_sample].Get(), mShadowRaysResources.rayHitUav[curr_sample].Get(), mShadowRaysResources.rayVoxelIdsUav[curr_sample].Get());
	mForwardRendererEffect->ApplyTechnique(HR_FR_INIT_RESOURCES_TECH, dev_c);
	if (dev_c->IsAnnotationEnabled())
		dev_c->SetMarkerInt(L"Init Resources", 0);
	dev_c->Dispatch(disp_params[0], disp_params[1], disp_params[2]);
	mForwardRendererEffect->ClearTechnique(HR_FR_INIT_RESOURCES_TECH, dev_c);
	gGpuProfiler.Timestamp(GTS_InitRayLinks);
	
	
	// Render effect

	// Set blend state that blends alpha (for things like sprites)
	ID3D11BlendState* state = NULL;
	dev_c->OMSetBlendState(state, NULL, 0xffffffff);

	// bind render targets
	ID3D11RenderTargetView* rtvs[2] = { 
		renderer->BackBufferRTV(),
		mDeferredBufferResources.worldPosAndShadowIntensityRtv.Get() 
	};
	float clr_color[4] = { 0.f, 0.f, 0.f, 0.f };
	dev_c->ClearRenderTargetView(rtvs[1], clr_color);
	dev_c->OMSetRenderTargets(2, rtvs, renderer->DepthStencilView());

	// Set default depth stencil state
	dev_c->OMSetDepthStencilState(NULL, NULL);

	// Get lights to fetch needed vars for lighting
	std::vector<LightToGpu> lights_to_gpu;
	std::vector<ID3D11ShaderResourceView*> shadow_map_cascades;
	renderer->SetupLightsAndShadowMapsForGPU(lights_to_gpu, shadow_map_cascades);
	mForwardRendererEffect->UpdateLightArrayVariable(lights_to_gpu);
	mForwardRendererEffect->UpdateNumLightsVariable((unsigned int)lights_to_gpu.size());

	// Shadowing vars
	D3D11_SAMPLER_DESC samp_desc;
	samp_desc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samp_desc.MipLODBias = 0.0f;
	samp_desc.MaxAnisotropy = 1;
	samp_desc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	samp_desc.BorderColor[0] = samp_desc.BorderColor[1] = samp_desc.BorderColor[2] = samp_desc.BorderColor[3] = 0;
	samp_desc.MinLOD = 0;
	samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
	auto sampler = renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), samp_desc);
	mForwardRendererEffect->UpdateShadowingVariables(sampler, shadow_map_cascades);

	mForwardRendererEffect->ApplyPerFrameConstants(dev_c);

	mForwardRendererEffect->ApplyRasterizationRenderingResources(dev_c);


	// Iterate through families
	if (dev_c->IsAnnotationEnabled())
		dev_c->SetMarkerInt(L"Draw Renderables", 0);

	std::set<unsigned int> renderable_types_to_process;
	renderable_types_to_process.insert(Renderable::LIT);
	renderable_types_to_process.insert(Renderable::FLAT);
	for (std::set<unsigned int>::iterator type_iter = renderable_types_to_process.begin();
		type_iter != renderable_types_to_process.end();
		++type_iter)
	{
		std::vector<unsigned int> frnt_back_ordered(
			familyByRenderableType[*type_iter].begin(), 
			familyByRenderableType[*type_iter].end());

		assert(familyByRenderableType[*type_iter].size() == frnt_back_ordered.size());

		std::sort(frnt_back_ordered.begin(), frnt_back_ordered.end(), HybridPipeline::FrontToBackCompare);
		std::vector<unsigned int>::const_iterator fam_iter = frnt_back_ordered.begin();
		for (fam_iter; fam_iter != frnt_back_ordered.end(); ++fam_iter)
		{
			// Get renderable component
			std::shared_ptr<IComponent> tmp_comp;

			// Check for renderable to draw
			gApp->EntityMngr()->GetComponentFromEntity(*fam_iter, Renderable::GetGuid(), tmp_comp);
			if (tmp_comp)
			{
				Renderable* renderable = static_cast<Renderable*>(tmp_comp.get());
				DrawRenderable(renderable, *fam_iter, renderer);
			}
		}
	}

	mForwardRendererEffect->UnbindAll(dev_c);

	// Generate mips of deferred buffers
	dev_c->GenerateMips(mDeferredBufferResources.worldPosAndShadowIntensitySrv.Get());

	gGpuProfiler.Timestamp(GTS_ForwardRendering);
}

void
HybridPipeline::DrawRenderable(Renderable *const renderable, unsigned int const& entityId, std::shared_ptr<D3dRenderer>& renderer)
{
	std::shared_ptr<IComponent> tmp_comp;
	std::list<InputLayoutManager::SubInputLayout> input_layout_desc;

	bool draw_with_diffuse = false;
	bool draw_with_bump = false;

	// Check if we need to change input layout
	{
		if (Renderable::LIT == renderable->RenderableType())
		{
			renderer->DeviceContext()->IASetInputLayout(mForwardRendererEffect->InputLayout(FR_DEFAULT_TECH));
			input_layout_desc = mForwardRendererEffect->InputLayoutDescription(FR_DEFAULT_TECH);
			assert(!input_layout_desc.empty());
		}	
	}

	renderer->DeviceContext()->IASetPrimitiveTopology(renderable->PrimitiveTopology());

	// Index buffer
	renderer->DeviceContext()->IASetIndexBuffer(renderable->IndexBuffer().indexBuffer,
		renderable->IndexBuffer().indexBufferFormat,
		renderable->IndexBuffer().indexBufferOffset);

	// Go through needed subinputs and accumulate vertex buffers
	std::vector<ID3D11Buffer*> buffers;
	std::vector<unsigned int> strides;
	std::vector<unsigned int> offsets;

	unsigned int vert_count = 0;

	std::list<InputLayoutManager::SubInputLayout>::iterator sub_input_iter = input_layout_desc.begin();
	for (sub_input_iter; sub_input_iter != input_layout_desc.end(); sub_input_iter++)
	{
		buffers.push_back(renderable->VertexBuffer(*sub_input_iter).vertexBuffer);
		strides.push_back(renderable->VertexBuffer(*sub_input_iter).stride);
		offsets.push_back(renderable->VertexBuffer(*sub_input_iter).offset);

		vert_count += renderable->VertexBuffer(*sub_input_iter).vertexCount;
	}
	if (!buffers.empty())
		renderer->DeviceContext()->IASetVertexBuffers(0, (unsigned int)buffers.size(), &buffers[0], &strides[0], &offsets[0]);

	// Set renderable's material
	mForwardRendererEffect->UpdateMaterialVariable(renderable->MaterialProperties());

	// Set raster state
	ID3D11RasterizerState* default_raster;
	if (renderable->BackCulled())
		default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::Default);
	else
		default_raster = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
	renderer->DeviceContext()->RSSetState(default_raster);


	if (Renderable::LIT == renderable->RenderableType())
	{
		// Check for texture transformation
		DirectX::XMMATRIX tex_xform = DirectX::XMMatrixIdentity();
		gApp->EntityMngr()->GetComponentFromEntity(entityId, TextureTransformed::GetGuid(), tmp_comp);
		if (tmp_comp)
			tex_xform = DirectX::XMLoadFloat4x4(&(static_cast<TextureTransformed*>(tmp_comp.get())->TextureTransformation()));
		mForwardRendererEffect->UpdateTextureTransformMatrixVariable(tex_xform);

		// Check for diffuse map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, DiffuseMapped::GetGuid(), tmp_comp);
		if (tmp_comp)
		{
			DiffuseMapped* diff_map = static_cast<DiffuseMapped*>(tmp_comp.get());
			mForwardRendererEffect->UpdateDiffuseMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), diff_map->DiffuseMapSamplerDesc()));
			mForwardRendererEffect->UpdateDiffuseMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), diff_map->DiffuseMapName(), true));
			draw_with_diffuse = true;
		}

		// Check for normal map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, NormalMapped::GetGuid(), tmp_comp);
		if (tmp_comp)
		{
			NormalMapped* normal_map = static_cast<NormalMapped*>(tmp_comp.get());
			mForwardRendererEffect->UpdateNormalMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), normal_map->NormalMapSamplerDesc()));
			mForwardRendererEffect->UpdateNormalMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), normal_map->NormalMapName()));
			draw_with_bump = true;
		}

		// Check for reflection map
		gApp->EntityMngr()->GetComponentFromEntity(entityId, ReflectionMapped::GetGuid(), tmp_comp);
		if (tmp_comp)
		{
			ReflectionMapped* reflection_map = static_cast<ReflectionMapped*>(tmp_comp.get());
			mForwardRendererEffect->UpdateReflectionMapSamplerVariable(renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), reflection_map->ReflectionMapSamplerDesc()));
			mForwardRendererEffect->UpdateReflectionMapVariable(renderer->TextureMngr()->GetOrCreateTexture2dSrv(renderer->Device(), reflection_map->ReflectionMapName()));
		}
	}

	// Update world mat from Spatialized component
	DirectX::XMMATRIX world = DirectX::XMMatrixIdentity();
	gApp->EntityMngr()->GetComponentFromEntity(entityId, Spatialized::GetGuid(), tmp_comp);
	if (tmp_comp)
	{
		Spatialized* spatial_comp = static_cast<Spatialized*>(tmp_comp.get());
		world = XMLoadFloat4x4(&spatial_comp->LocalTransformation());
	}
	mForwardRendererEffect->UpdateWorldMatrixVariable(world);

	// Apply
	if (Renderable::LIT == renderable->RenderableType())
	{
		if (draw_with_diffuse)
			if (draw_with_bump)
				mForwardRendererEffect->ApplyTechnique(FR_DIFFUSE_BUMP_TECH, renderer->DeviceContext());
			else
				mForwardRendererEffect->ApplyTechnique(FR_DIFFUSE_TECH, renderer->DeviceContext());
		else
			if (draw_with_bump)
				mForwardRendererEffect->ApplyTechnique(FR_BUMP_TECH, renderer->DeviceContext());
			else
				mForwardRendererEffect->ApplyTechnique(FR_DEFAULT_TECH, renderer->DeviceContext());
	}

	if (0 == renderable->IndexBuffer().indexCount)
		renderer->DeviceContext()->Draw(vert_count, 0);
	else
		renderer->DeviceContext()->DrawIndexed(renderable->IndexBuffer().indexCount, 0, 0);
}

void 
HybridPipeline::InitSimulationGridPass()
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();
		
	if (mDoSimGridUpdates)
	{
		auto camera = gApp->Renderer()->CurrentCamera();		
		mGridEffect->UpdateNumVoxelsPerLengthVariable(mSimulationGrid.NumVoxelsPerLength());

		mGridEffect->UpdateSimGridCenterAndVoxelSizePerLengthVariable(DirectX::XMFLOAT4(
			camera->Position().x,
			camera->Position().y,
			camera->Position().z,
			mSimulationGrid.VoxelSizePerLength()));
		
		// Cull
		mSimulationGrid.CullEntitiesOutOfGrid(renderer->CurrentCamera(), mEntitiesWithVoxelizedComponent);

		static unsigned int prev_entities_in_sim = 0;
		if ((unsigned int)mEntitiesWithVoxelizedComponent.size() != prev_entities_in_sim)
		{
			prev_entities_in_sim = (unsigned int)mEntitiesWithVoxelizedComponent.size();
			OutputDebugMsg("Entities in simulation = " + to_string(prev_entities_in_sim) + "\n");
		}

		// Map the virtual resources				
		MapTiles(mEntitiesWithVoxelizedComponent);
	}
	gGpuProfiler.Timestamp(GTS_TileMappings);


	unsigned int num_dispatches = 1; // used to calculate dispatching for the compute kernels

#ifdef _DEBUG
	//  complains about  [ EXECUTION WARNING #3146100: DUPLICATE_TILE_MAPPINGS_IN_COVERED_AREA] when writing to dummy tile			
	mD3dInfoQueue->SetMuteDebugOutput(true);
#endif

	if (mDoSimGridUpdates)
	{
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Reset Grid Resources", 0);

		// Reset resources		
		mGridEffect->UpdateVoxelGridUav(mVoxelGridResources.voxelGridUav.Get());

		mGridEffect->ApplyTechnique(HR_INIT_GRID_TECH, dev_c);
		num_dispatches = ((unsigned int)pow(mSimulationGrid.NumVoxelsPerLength(), 3) / 1024) + 1;
		assert(num_dispatches <= D3D11_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION);
		dev_c->Dispatch(num_dispatches, 1, 1);
		mGridEffect->ClearTechnique(HR_INIT_GRID_TECH, dev_c);
	}
	gGpuProfiler.Timestamp(GTS_ClearTilesHeaders);
	

	if (mDoSimGridUpdates)
	{
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Simulation Grid Construction", 0);

		// Per object passes
		mGridEffect->BindPerFrameResources(dev_c);

		for (auto entity_id : mEntitiesWithVoxelizedComponent)
		{
			Voxelized*   voxelized_comp = nullptr;
			Spatialized* spatialized_comp = nullptr;

			std::shared_ptr<IComponent> tmp_comp;

			gApp->EntityMngr()->GetComponentFromEntity(entity_id, Voxelized::GetGuid(), tmp_comp);
			if (tmp_comp)
				voxelized_comp = static_cast<Voxelized*>(tmp_comp.get());

			gApp->EntityMngr()->GetComponentFromEntity(entity_id, Spatialized::GetGuid(), tmp_comp);
			if (tmp_comp)
				spatialized_comp = static_cast<Spatialized*>(tmp_comp.get());

			if (!spatialized_comp || !voxelized_comp)
				continue;

			mGridEffect->UpdateNumTrisVariable(voxelized_comp->NumTriangles());
			mGridEffect->UpdateWorldTransformVariable(XMLoadFloat4x4(&spatialized_comp->LocalTransformation()));

			mGridEffect->UpdateTrianglesBufferSrv(voxelized_comp->TrianglesSrv());

			// Dispatch as many necessary thread groups to process all triangles in as few passes as possible
			mGridEffect->ApplyTechnique(HR_CONSTRUCT_VOXEL_GRID_TECH, dev_c);
			if (voxelized_comp->NumTriangles() > 0)
			{
				num_dispatches = (voxelized_comp->NumTriangles() / 512) + 1;
				assert(num_dispatches <= D3D11_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION);
				dev_c->Dispatch(num_dispatches, 1, 1);
				BufferSnapShot(renderer);
			}

		}
		mGridEffect->UnbindAllResources(dev_c);
	}
	gGpuProfiler.Timestamp(GTS_FillTilesGeometry);


	// For debugging
	//BufferSnapShot(renderer);	

#ifdef _DEBUG
	//  complains about  [ EXECUTION WARNING #3146100: DUPLICATE_TILE_MAPPINGS_IN_COVERED_AREA] when writing to dummy tile			
	mD3dInfoQueue->SetMuteDebugOutput(false);
#endif
}

void 
HybridPipeline::InitShadowRayResources()
{	
	auto const num_samples = 16u;
	auto const num_spf = 1u;
	auto const res_fctr = 1u;

	InitRaysResources(mShadowRaysResources, DXGI_FORMAT_R8_UNORM, num_samples, num_spf, res_fctr);
}


void HybridPipeline::InitRaysResources(
	PerRayTypeResources& raysResources,
	DXGI_FORMAT const format, 
	unsigned int const numSamples,
	unsigned int const numSamplesPerFrame, 
	unsigned int const resFactor)
{
	assert(numSamples > 0);
	assert(resFactor > 0);

	raysResources.rayListDesc.resize(numSamples, D3D11_TEXTURE2D_DESC());
	raysResources.rayListSrvDesc.resize(numSamples, D3D11_SHADER_RESOURCE_VIEW_DESC());
	raysResources.rayListUavDesc.resize(numSamples, D3D11_UNORDERED_ACCESS_VIEW_DESC());
	raysResources.rayListTextureArray.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11Texture2D>());
	raysResources.rayListUav.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>());
	raysResources.rayListSrv.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>());
	raysResources.rayListOriginDistRtv.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11RenderTargetView>());
	raysResources.rayListNormRtv.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11RenderTargetView>());
	raysResources.rayHitDesc.resize(numSamples, D3D11_TEXTURE2D_DESC());
	raysResources.rayHitSrvDesc.resize(numSamples, D3D11_SHADER_RESOURCE_VIEW_DESC());
	raysResources.rayHitUavDesc.resize(numSamples, D3D11_UNORDERED_ACCESS_VIEW_DESC());
	raysResources.rayHitTexture.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11Texture2D>());
	raysResources.rayHitUav.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>());
	raysResources.rayHitSrv.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>());
	raysResources.rayHitSamplerDesc.resize(numSamples, D3D11_SAMPLER_DESC());
	raysResources.rayVoxelIdsDesc.resize(numSamples, D3D11_TEXTURE2D_DESC());
	raysResources.rayVoxelIdsUavDesc.resize(numSamples, D3D11_UNORDERED_ACCESS_VIEW_DESC());
	raysResources.rayVoxelIdsSrvDesc.resize(numSamples, D3D11_SHADER_RESOURCE_VIEW_DESC());
	raysResources.rayVoxelIdsTexture.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11Texture2D>());
	raysResources.rayVoxelIdsSrv.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>());
	raysResources.rayVoxelIdsUav.resize(numSamples, Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView>());

	raysResources.numRaySamples = numSamples;
	raysResources.numRaySamplesPerFrame = numSamplesPerFrame;
	raysResources.numComputedSamples = 0;
	raysResources.resolutionFactor = resFactor;

	for (unsigned int i = 0; i < numSamples; i++)
	{
		HRESULT hr;
		unsigned int resolution[2] = { gApp->WinWidth(), gApp->WinHeight() };

		ZeroMemory(&raysResources.rayListDesc[i], sizeof(raysResources.rayListDesc[i]));
		raysResources.rayListDesc[i].Width = resolution[0] / resFactor;
		raysResources.rayListDesc[i].Height = resolution[1] / resFactor;
		raysResources.rayListDesc[i].ArraySize = 2;
		raysResources.rayListDesc[i].SampleDesc.Count = 1;
		raysResources.rayListDesc[i].Usage = D3D11_USAGE_DEFAULT;
		raysResources.rayListDesc[i].BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_RENDER_TARGET;
		raysResources.rayListDesc[i].CPUAccessFlags = 0;
		raysResources.rayListDesc[i].MiscFlags = 0;
		raysResources.rayListDesc[i].MipLevels = 1;
		raysResources.rayListDesc[i].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		hr = gApp->Renderer()->Device()->CreateTexture2D(&raysResources.rayListDesc[i], NULL, raysResources.rayListTextureArray[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayListUavDesc[i], sizeof(raysResources.rayListUavDesc[i]));
		raysResources.rayListUavDesc[i].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		raysResources.rayListUavDesc[i].ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
		raysResources.rayListUavDesc[i].Texture2DArray.ArraySize = 2;
		raysResources.rayListUavDesc[i].Texture2DArray.FirstArraySlice = 0;
		raysResources.rayListUavDesc[i].Texture2DArray.MipSlice = 0;
		hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(raysResources.rayListTextureArray[i].Get(), &raysResources.rayListUavDesc[i], raysResources.rayListUav[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayListSrvDesc[i], sizeof(raysResources.rayListSrvDesc[i]));
		raysResources.rayListSrvDesc[i].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		raysResources.rayListSrvDesc[i].ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
		raysResources.rayListSrvDesc[i].Texture2DArray.ArraySize = 2;
		raysResources.rayListUavDesc[i].Texture2DArray.FirstArraySlice = 0;
		raysResources.rayListSrvDesc[i].Texture2DArray.MipLevels = 1;
		raysResources.rayListSrvDesc[i].Texture2DArray.MostDetailedMip = 0;
		hr = gApp->Renderer()->Device()->CreateShaderResourceView(raysResources.rayListTextureArray[i].Get(), &raysResources.rayListSrvDesc[i], raysResources.rayListSrv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayHitDesc[i], sizeof(raysResources.rayHitDesc[i]));
		raysResources.rayHitDesc[i].Width = resolution[0] / resFactor;
		raysResources.rayHitDesc[i].Height = resolution[1] / resFactor;
		raysResources.rayHitDesc[i].ArraySize = 1;
		raysResources.rayHitDesc[i].SampleDesc.Count = 1;
		raysResources.rayHitDesc[i].Usage = D3D11_USAGE_DEFAULT;
		raysResources.rayHitDesc[i].BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		raysResources.rayHitDesc[i].CPUAccessFlags = 0;
		raysResources.rayHitDesc[i].MiscFlags = 0;
		raysResources.rayHitDesc[i].MipLevels = 1;
		raysResources.rayHitDesc[i].Format = format;
		hr = gApp->Renderer()->Device()->CreateTexture2D(&raysResources.rayHitDesc[i], NULL, raysResources.rayHitTexture[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayHitUavDesc[i], sizeof(raysResources.rayHitUavDesc[i]));
		raysResources.rayHitUavDesc[i].Format = format;
		raysResources.rayHitUavDesc[i].ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(raysResources.rayHitTexture[i].Get(), &raysResources.rayHitUavDesc[i], raysResources.rayHitUav[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayHitSrvDesc[i], sizeof(raysResources.rayHitSrvDesc[i]));
		raysResources.rayHitSrvDesc[i].Format = format;
		raysResources.rayHitSrvDesc[i].ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		raysResources.rayHitSrvDesc[i].Texture2D.MostDetailedMip = 0;
		raysResources.rayHitSrvDesc[i].Texture2D.MipLevels = 1;
		hr = gApp->Renderer()->Device()->CreateShaderResourceView(raysResources.rayHitTexture[i].Get(), &raysResources.rayHitSrvDesc[i], raysResources.rayHitSrv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayHitSamplerDesc[i], sizeof(raysResources.rayHitSamplerDesc[i]));
		raysResources.rayHitSamplerDesc[i].Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		raysResources.rayHitSamplerDesc[i].AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		raysResources.rayHitSamplerDesc[i].AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		raysResources.rayHitSamplerDesc[i].AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		raysResources.rayHitSamplerDesc[i].MipLODBias = 0.0f;
		raysResources.rayHitSamplerDesc[i].MaxAnisotropy = 1;
		raysResources.rayHitSamplerDesc[i].ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		raysResources.rayHitSamplerDesc[i].BorderColor[0] = 0;
		raysResources.rayHitSamplerDesc[i].BorderColor[1] = 0;
		raysResources.rayHitSamplerDesc[i].BorderColor[2] = 0;
		raysResources.rayHitSamplerDesc[i].BorderColor[3] = 0;
		raysResources.rayHitSamplerDesc[i].MinLOD = 0;
		raysResources.rayHitSamplerDesc[i].MaxLOD = D3D11_FLOAT32_MAX;

		D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
		ZeroMemory(&rtv_desc, sizeof(rtv_desc));
		rtv_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		rtv_desc.Texture2DArray.ArraySize = 1;
		rtv_desc.Texture2DArray.MipSlice = 0;
		rtv_desc.Texture2DArray.FirstArraySlice = 0;
		hr = gApp->Renderer()->Device()->CreateRenderTargetView(raysResources.rayListTextureArray[i].Get(), &rtv_desc, raysResources.rayListOriginDistRtv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		rtv_desc.Texture2DArray.FirstArraySlice = 1;
		hr = gApp->Renderer()->Device()->CreateRenderTargetView(raysResources.rayListTextureArray[i].Get(), &rtv_desc, raysResources.rayListNormRtv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));


		ZeroMemory(&raysResources.rayVoxelIdsDesc[i], sizeof(raysResources.rayVoxelIdsDesc[i]));
		raysResources.rayVoxelIdsDesc[i].Width = resolution[0] / resFactor;
		raysResources.rayVoxelIdsDesc[i].Height = resolution[1] / resFactor;
		raysResources.rayVoxelIdsDesc[i].ArraySize = 1;
		raysResources.rayVoxelIdsDesc[i].SampleDesc.Count = 1;
		raysResources.rayVoxelIdsDesc[i].Usage = D3D11_USAGE_DEFAULT;
		raysResources.rayVoxelIdsDesc[i].BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		raysResources.rayVoxelIdsDesc[i].CPUAccessFlags = 0;
		raysResources.rayVoxelIdsDesc[i].MiscFlags = 0;
		raysResources.rayVoxelIdsDesc[i].MipLevels = 1;
		raysResources.rayVoxelIdsDesc[i].Format = DXGI_FORMAT_R32_TYPELESS;
		hr = gApp->Renderer()->Device()->CreateTexture2D(&raysResources.rayVoxelIdsDesc[i], NULL, raysResources.rayVoxelIdsTexture[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayVoxelIdsUavDesc[i], sizeof(raysResources.rayVoxelIdsUavDesc[i]));
		raysResources.rayVoxelIdsUavDesc[i].Format = DXGI_FORMAT_R32_SINT;
		raysResources.rayVoxelIdsUavDesc[i].ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(raysResources.rayVoxelIdsTexture[i].Get(), &raysResources.rayVoxelIdsUavDesc[i], raysResources.rayVoxelIdsUav[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));

		ZeroMemory(&raysResources.rayVoxelIdsSrvDesc[i], sizeof(raysResources.rayVoxelIdsSrvDesc[i]));
		raysResources.rayHitSrvDesc[i].Format = DXGI_FORMAT_R32_SINT;
		raysResources.rayHitSrvDesc[i].ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		raysResources.rayHitSrvDesc[i].Texture2D.MostDetailedMip = 0;
		raysResources.rayHitSrvDesc[i].Texture2D.MipLevels = 1;
		hr = gApp->Renderer()->Device()->CreateShaderResourceView(raysResources.rayVoxelIdsTexture[i].Get(), &raysResources.rayHitSrvDesc[i], raysResources.rayVoxelIdsSrv[i].ReleaseAndGetAddressOf());
		assert(SUCCEEDED(hr));
	}
}

void 
HybridPipeline::RayMarchingPass()
{
 	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
 	if (!renderer)
 		return;

	auto dev_c = renderer->DeviceContext();
 
 	auto camera = gApp->Renderer()->CurrentCamera();
 	int num_voxels_per_length = mSimulationGrid.NumVoxelsPerLength();		
 	DirectX::XMFLOAT4 sim_grd_cnter_and_size = {
 		camera->Position().x,
 		camera->Position().y,
 		camera->Position().z,
 		mSimulationGrid.VoxelSizePerLength() };

	auto curr_sample = mShadowRaysResources.numComputedSamples;

	int dispatch_params[3] = { gApp->WinWidth() / 8, gApp->WinHeight() / 8, 1 };
	mRayMarchingEffect->UpdateIndirectDispatchArgsUav(mIndirectDispatchResources.uav.Get());
	mRayMarchingEffect->UpdateHashLookupUav(mRayTracingResources.rayHashLookupUav.Get());
	mRayMarchingEffect->UpdateNumVoxelsPerLengthVariable(num_voxels_per_length);
	mRayMarchingEffect->UpdateSimulationGridCenterAndVoxelSizePerLengthVariable(sim_grd_cnter_and_size);
	mRayMarchingEffect->UpdateRayPacketCountsUav(mRayTracingResources.rayPacketsPerGroupUav.Get());
	mRayMarchingEffect->UpdateDispatchParamsVariable(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
	mRayMarchingEffect->UpdateRayPacketCountsSrv(mRayTracingResources.rayPacketsPerGroupSrv.Get());
	mRayMarchingEffect->UpdatePrefixSumSrv(mRayTracingResources.rayPacketsPerGroupPrefixSrv.Get());
	mRayMarchingEffect->UpdateHashLookupSrv(mRayTracingResources.rayHashLookupSrv.Get());
	mRayMarchingEffect->UpdateRayPacketsUav(mRayPacketResources.rayPacketsUav.Get());
	mRayMarchingEffect->UpdateRayPacketHeadersUav(mRayTracingResources.rayPacketHeadersUav.Get());
	mRayMarchingEffect->UpdateMaxNumPacketsGenerated(mMaxNumPacketsGenerated);
	mRayMarchingEffect->UpdateVoxelGridUav(mVoxelGridResources.voxelGridUav.Get());
	mRayMarchingEffect->UpdateViewportSizeVariable(gApp->WinWidth(), gApp->WinHeight());

	mRayMarchingEffect->UpdateRaysListSrv(mShadowRaysResources.rayListSrv[curr_sample].Get());
	mRayMarchingEffect->UpdateRayVoxelIdsSrv(mShadowRaysResources.rayVoxelIdsSrv[curr_sample].Get());
	mRayMarchingEffect->UpdateRayVoxelIdsUav(mShadowRaysResources.rayVoxelIdsUav[curr_sample].Get());
	mRayMarchingEffect->UpdateRaysListUav(mShadowRaysResources.rayListUav[curr_sample].Get());
	mRayMarchingEffect->UpdateRaysHitUav(mShadowRaysResources.rayHitUav[curr_sample].Get());

	if (mDoMetricsCollection)
		mRayMarchingEffect->UpdateMetricsUav(mMetricResources.intersectsPerFrameUav.Get());
	else
		mRayMarchingEffect->UpdateMetricsUav(nullptr);
 
	for (unsigned int i = 0; i < mNumRayMarches; i++)
	{

		// GENERATE RAY PACKET COUNTS
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Generate Ray Packet Counts", 0);

		mRayMarchingEffect->ApplyTechnique(HR_RAY_MARCH_GENERATE_RAY_PACKET_COUNTS_TECH, dev_c);
		dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
		mRayMarchingEffect->ClearTechnique("", dev_c);

		// PREFIX SUM
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Prefix Sum", 0);

		mScanPrimitive->Scan(
			gApp->WinWidth() * gApp->WinHeight() / MAX_NUM_RAYS_PER_PACKET,
			mRayTracingResources.rayPacketsPerGroupUav.Get(),
			mRayTracingResources.rayPacketsPerGroupPrefixUav.Get());

				
		// CREATE RAY PACKETS
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Create Ray Packet", 0);

		mRayMarchingEffect->ApplyTechnique(HR_RAY_MARCH_CREATE_RAY_PACKETS_TECH, dev_c);
		dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
		mRayMarchingEffect->ClearTechnique("", dev_c);

	
#ifdef _DEBUG
		//  complains about  [ EXECUTION WARNING #3146100: DUPLICATE_TILE_MAPPINGS_IN_COVERED_AREA] when writing to dummy tile			
		mD3dInfoQueue->SetMuteDebugOutput(true);
#endif
		// INTERSECTION		
		if (dev_c->IsAnnotationEnabled())
			dev_c->SetMarkerInt(L"Ray Intersection", 0);

		mRayMarchingEffect->ApplyTechnique(HR_RAY_MARCH_INTERSECT_TECH, dev_c);
		dev_c->DispatchIndirect(mIndirectDispatchResources.buffer.Get(), 0);
		mRayMarchingEffect->ClearTechnique("", dev_c);


		if (i < mNumRayMarches - 1)
		{
			// GRID TRAVERSAL
			if (dev_c->IsAnnotationEnabled())
				dev_c->SetMarkerInt(L"Grid Traversal", 0);

			mRayMarchingEffect->ApplyTechnique(HR_RAY_MARCH_TRAVERSE_GRID_TECH, dev_c);
			dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
			mRayMarchingEffect->ClearTechnique("", dev_c);
		}

#ifdef _DEBUG
		//  complains about  [ EXECUTION WARNING #3146100: DUPLICATE_TILE_MAPPINGS_IN_COVERED_AREA] when writing to dummy tile			
		mD3dInfoQueue->SetMuteDebugOutput(false);
#endif
	}
	gGpuProfiler.Timestamp(GTS_RayMarch);	
}

void
HybridPipeline::InitRayMarchingResources()
{
	ZeroMemory(&mRayTracingResources, sizeof(mRayTracingResources));
		
	HRESULT hr;

	mRayTracingResources.rayPacketsPerGroupDesc.ByteWidth = (UINT)(sizeof(unsigned int) * std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET));
	mRayTracingResources.rayPacketsPerGroupDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayTracingResources.rayPacketsPerGroupDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	mRayTracingResources.rayPacketsPerGroupDesc.CPUAccessFlags = 0;
	mRayTracingResources.rayPacketsPerGroupDesc.MiscFlags = 0;
	mRayTracingResources.rayPacketsPerGroupDesc.StructureByteStride = (UINT)(sizeof(unsigned int));
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayTracingResources.rayPacketsPerGroupDesc, NULL, mRayTracingResources.rayPacketsPerGroupBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketsPerGroupUavDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayPacketsPerGroupUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mRayTracingResources.rayPacketsPerGroupUavDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayPacketsPerGroupUavDesc.Buffer.Flags = 0;
	mRayTracingResources.rayPacketsPerGroupUavDesc.Buffer.NumElements = (UINT)std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET);
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mRayTracingResources.rayPacketsPerGroupBuff.Get(), &mRayTracingResources.rayPacketsPerGroupUavDesc, mRayTracingResources.rayPacketsPerGroupUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketsPerGroupSrvDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayPacketsPerGroupSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	mRayTracingResources.rayPacketsPerGroupSrvDesc.Buffer.ElementWidth = mRayTracingResources.rayPacketsPerGroupDesc.StructureByteStride;
	mRayTracingResources.rayPacketsPerGroupSrvDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayPacketsPerGroupSrvDesc.Buffer.NumElements = (UINT)std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET);
	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mRayTracingResources.rayPacketsPerGroupBuff.Get(), &mRayTracingResources.rayPacketsPerGroupSrvDesc, mRayTracingResources.rayPacketsPerGroupSrv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketsPerGroupPrefixDesc.ByteWidth = (UINT)(sizeof(unsigned int) * std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET));
	mRayTracingResources.rayPacketsPerGroupPrefixDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayTracingResources.rayPacketsPerGroupPrefixDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	mRayTracingResources.rayPacketsPerGroupPrefixDesc.CPUAccessFlags = 0;
	mRayTracingResources.rayPacketsPerGroupPrefixDesc.MiscFlags = 0;
	mRayTracingResources.rayPacketsPerGroupPrefixDesc.StructureByteStride = (UINT)(sizeof(unsigned int));
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayTracingResources.rayPacketsPerGroupPrefixDesc, NULL, mRayTracingResources.rayPacketsPerGroupPrefixBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketsPerGroupPrefixUavDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayPacketsPerGroupPrefixUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mRayTracingResources.rayPacketsPerGroupPrefixUavDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayPacketsPerGroupPrefixUavDesc.Buffer.Flags = 0;
	mRayTracingResources.rayPacketsPerGroupPrefixUavDesc.Buffer.NumElements = (UINT)std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET);
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mRayTracingResources.rayPacketsPerGroupPrefixBuff.Get(), &mRayTracingResources.rayPacketsPerGroupPrefixUavDesc, mRayTracingResources.rayPacketsPerGroupPrefixUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc.Buffer.ElementWidth = mRayTracingResources.rayPacketsPerGroupPrefixDesc.StructureByteStride;
	mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc.Buffer.NumElements = (UINT)std::ceil((gApp->WinWidth() * gApp->WinHeight()) / MAX_NUM_RAYS_PER_PACKET);
	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mRayTracingResources.rayPacketsPerGroupPrefixBuff.Get(), &mRayTracingResources.rayPacketsPerGroupPrefixSrvDesc, mRayTracingResources.rayPacketsPerGroupPrefixSrv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
				
	mRayTracingResources.rayPacketHeadersDesc.ByteWidth = (UINT)(sizeof(int) * 4 * gApp->WinWidth() * gApp->WinHeight());
	mRayTracingResources.rayPacketHeadersDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayTracingResources.rayPacketHeadersDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	mRayTracingResources.rayPacketHeadersDesc.CPUAccessFlags = 0;
	mRayTracingResources.rayPacketHeadersDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	mRayTracingResources.rayPacketHeadersDesc.StructureByteStride = (UINT)(sizeof(int) * 4);
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayTracingResources.rayPacketHeadersDesc, NULL, mRayTracingResources.rayPacketHeadersBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayPacketHeadersUavDesc.Format = DXGI_FORMAT_UNKNOWN;
	mRayTracingResources.rayPacketHeadersUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mRayTracingResources.rayPacketHeadersUavDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayPacketHeadersUavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_APPEND;
	mRayTracingResources.rayPacketHeadersUavDesc.Buffer.NumElements = (UINT)(gApp->WinWidth() * gApp->WinHeight());
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mRayTracingResources.rayPacketHeadersBuff.Get(), &mRayTracingResources.rayPacketHeadersUavDesc, mRayTracingResources.rayPacketHeadersUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));




	mRayTracingResources.rayHashLookupDesc.ByteWidth = (UINT)(sizeof(int) * std::ceil((gApp->WinWidth() * gApp->WinHeight())));
	mRayTracingResources.rayHashLookupDesc.Usage = D3D11_USAGE_DEFAULT;
	mRayTracingResources.rayHashLookupDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	mRayTracingResources.rayHashLookupDesc.CPUAccessFlags = 0;
	mRayTracingResources.rayHashLookupDesc.MiscFlags = 0;
	mRayTracingResources.rayHashLookupDesc.StructureByteStride = (UINT)(sizeof(int));
	hr = gApp->Renderer()->Device()->CreateBuffer(&mRayTracingResources.rayHashLookupDesc, NULL, mRayTracingResources.rayHashLookupBuff.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayHashLookupUavDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayHashLookupUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mRayTracingResources.rayHashLookupUavDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayHashLookupUavDesc.Buffer.Flags = 0;
	mRayTracingResources.rayHashLookupUavDesc.Buffer.NumElements = (UINT)std::ceil((gApp->WinWidth() * gApp->WinHeight()));
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mRayTracingResources.rayHashLookupBuff.Get(), &mRayTracingResources.rayHashLookupUavDesc, mRayTracingResources.rayHashLookupUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mRayTracingResources.rayHashLookupSrvDesc.Format = DXGI_FORMAT_R32_SINT;
	mRayTracingResources.rayHashLookupSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	mRayTracingResources.rayHashLookupSrvDesc.Buffer.ElementWidth = mRayTracingResources.rayHashLookupDesc.StructureByteStride;
	mRayTracingResources.rayHashLookupSrvDesc.Buffer.FirstElement = 0;
	mRayTracingResources.rayHashLookupSrvDesc.Buffer.NumElements = mRayTracingResources.rayHashLookupUavDesc.Buffer.NumElements;
	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mRayTracingResources.rayHashLookupBuff.Get(), &mRayTracingResources.rayHashLookupSrvDesc, mRayTracingResources.rayHashLookupSrv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));



	// Indirect dispatch arguments buffer
	ZeroMemory(&mIndirectDispatchResources, sizeof(mIndirectDispatchResources));

	D3D11_SUBRESOURCE_DATA init_data;
	unsigned int default_dispatch[3] = {1,1,1};
	ZeroMemory(&init_data, sizeof(D3D11_SUBRESOURCE_DATA));
	init_data.pSysMem = default_dispatch;		

	mIndirectDispatchResources.bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	mIndirectDispatchResources.bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
	mIndirectDispatchResources.bufferDesc.ByteWidth = (UINT)sizeof(int) * 3;
	mIndirectDispatchResources.bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	mIndirectDispatchResources.bufferDesc.CPUAccessFlags = 0;
	hr = gApp->Renderer()->Device()->CreateBuffer(&mIndirectDispatchResources.bufferDesc, &init_data, mIndirectDispatchResources.buffer.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mIndirectDispatchResources.uavDesc.Buffer.NumElements = 3;
	mIndirectDispatchResources.uavDesc.Format = DXGI_FORMAT_R32_UINT;
	mIndirectDispatchResources.uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mIndirectDispatchResources.buffer.Get(), &mIndirectDispatchResources.uavDesc, mIndirectDispatchResources.uav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
}


std::wstring
HybridPipeline::DoGeometryFitmentCheck()
{
	std::wstring msg;

	static D3D11_MAPPED_SUBRESOURCE mapped;

	
	auto renderer = gApp->Renderer();
	
	// Read back occupied triangle tiles
	for (auto voxel_id : mOccupiedVoxels)
	{
		D3D11_TILED_RESOURCE_COORDINATE trc = { voxel_id, 0, 0, 0 };
		D3D11_TILE_REGION_SIZE trs = { 1, false, 0, 0, 0 };
		renderer->DeviceContext()->CopyTiles(mVoxelGridResources.voxelGridBuff.Get(), &trc, &trs, mTriTileReadBackBuff.Get(), 0, D3D11_TILE_COPY_SWIZZLED_TILED_RESOURCE_TO_LINEAR_BUFFER);
		assert(SUCCEEDED(renderer->DeviceContext()->Map(mTriTileReadBackBuff.Get(), 0, D3D11_MAP_READ, 0, &mapped)));
		BYTE* mapped_data = reinterpret_cast<BYTE*>(mapped.pData);
		TrianglesTile tile;
		memcpy(&tile, mapped_data, sizeof(TrianglesTile));
		renderer->DeviceContext()->Unmap(mTriTileReadBackBuff.Get(), 0);

		auto num_tris = tile.header[1];
		auto const max_tris_in_tile = (BYTES_PER_TILE - (sizeof(int) * 4)) / ((sizeof(float)*3)*3); // 1820 tris total with header per tile
		if (num_tris >= max_tris_in_tile - 1)
		{
			msg = L"Some geometry tiles are full!\n";
			break;
		}
	}

	return msg;
}

void 
HybridPipeline::InitMetricResources()
{
	ZeroMemory(&mMetricResources, sizeof(mMetricResources));

	HRESULT hr;

	mMetricResources.intersectsPerFrameBufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;	
	mMetricResources.intersectsPerFrameBufferDesc.ByteWidth = (UINT)sizeof(int);
	mMetricResources.intersectsPerFrameBufferDesc.Usage = D3D11_USAGE_DEFAULT;// D3D11_USAGE_STAGING;
	mMetricResources.intersectsPerFrameBufferDesc.CPUAccessFlags = 0;// D3D11_CPU_ACCESS_READ;
	hr = gApp->Renderer()->Device()->CreateBuffer(&mMetricResources.intersectsPerFrameBufferDesc, NULL, mMetricResources.intersectsPerFrameBuffer.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mMetricResources.intersectsPerFrameUavDesc.Buffer.NumElements = 1;
	mMetricResources.intersectsPerFrameUavDesc.Format = DXGI_FORMAT_R32_UINT;
	mMetricResources.intersectsPerFrameUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mMetricResources.intersectsPerFrameBuffer.Get(), &mMetricResources.intersectsPerFrameUavDesc, mMetricResources.intersectsPerFrameUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));



	D3D11_BUFFER_DESC rb_buff_desc;
	ZeroMemory(&rb_buff_desc, sizeof(rb_buff_desc));
	rb_buff_desc.BindFlags = 0;
	rb_buff_desc.ByteWidth = sizeof(int);
	rb_buff_desc.Usage = D3D11_USAGE_STAGING;
	rb_buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	hr = gApp->Renderer()->Device()->CreateBuffer(&rb_buff_desc, NULL, mMetricResources.intersectsPerFrameReadBackBuffer.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
}

HybridPipeline::HybridPipelineMetrics 
HybridPipeline::CollectMetrics()
{
	HybridPipelineMetrics metrics;
	ZeroMemory(&metrics, sizeof(metrics));

	auto renderer = gApp->Renderer();
	renderer->DeviceContext()->CopyResource(mMetricResources.intersectsPerFrameReadBackBuffer.Get(), mMetricResources.intersectsPerFrameBuffer.Get());
	
	static D3D11_MAPPED_SUBRESOURCE mapped;

	assert(SUCCEEDED(renderer->DeviceContext()->Map(mMetricResources.intersectsPerFrameReadBackBuffer.Get(), 0, D3D11_MAP_READ, 0, &mapped)));
	BYTE* mapped_data = reinterpret_cast<BYTE*>(mapped.pData);	
	memcpy(&metrics.numIntersections, mapped_data, sizeof(unsigned int));
	renderer->DeviceContext()->Unmap(mMetricResources.intersectsPerFrameReadBackBuffer.Get(), 0);

	return metrics;
}

void
HybridPipeline::ResolvePass()
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();

	auto lights = renderer->LightingSystm()->Lights();
	if (!lights.empty())
	{
		std::pair<LightType, Light*> first_light = *(lights.begin());
		if (first_light.first == DIRECTIONAL_LIGHT)
		{
			mShadowPanumbraEffect->UpdateComputedSamplesVar(mShadowRaysResources.numComputedSamples);

			ID3D11RasterizerState* raster_state = renderer->StateMngr()->RasterizerState(RenderStateManager::NoCull);
			dev_c->RSSetState(raster_state);
			dev_c->OMSetBlendState(renderer->StateMngr()->GetOrCreateBlendState(renderer->Device(), mShadowResolveBlendDesc), NULL, 0xffffffff);
			dev_c->OMSetDepthStencilState(renderer->StateMngr()->GetOrCreateDepthStencilState(renderer->Device(), mShadowResolveDepthStencilDesc), 1);
			dev_c->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);			
			dev_c->IASetInputLayout(nullptr);
			dev_c->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
			mShadowPanumbraEffect->ApplyTechnique(HR_RESOLVE_SHADOWS_TECH, dev_c);
			dev_c->Draw(3, NULL);
			mShadowPanumbraEffect->ClearTechnique("", dev_c);			
		}
	}
	gGpuProfiler.Timestamp(GTS_Resolve);
}

void 
HybridPipeline::IntermediatePass()
{
	std::shared_ptr<D3dRenderer> renderer = mRenderer.lock();
	if (!renderer)
		return;

	auto dev_c = renderer->DeviceContext();

	auto lights = renderer->LightingSystm()->Lights();
	if (!lights.empty())
	{
		std::pair<LightType, Light*> first_light = *(lights.begin());
		if (first_light.first == DIRECTIONAL_LIGHT)
		{			
			auto light_size = static_cast<DirectionalLight*>(first_light.second)->lightSize;
			auto light_dir = static_cast<DirectionalLight*>(first_light.second)->direction;

			mShadowPanumbraEffect->UpdateDirectionalLightSizeVar(light_size);
			mShadowPanumbraEffect->UpdateLightDirection(light_dir);

			auto total_samples = mShadowRaysResources.numRaySamples;
			mShadowPanumbraEffect->UpdateTotalSamplesVar(total_samples);

			mShadowPanumbraEffect->UpdateMipMapLevel(mShadowRaysResources.resolutionFactor);

			unsigned int res[2] = { 
				gApp->WinWidth() / mShadowRaysResources.resolutionFactor,
				gApp->WinHeight() / mShadowRaysResources.resolutionFactor
			};
			mShadowPanumbraEffect->UpdateMipMapResolution(
				res[0],
				res[1]);

			mShadowPanumbraEffect->UpdateNumVoxelsPerLength( mSimulationGrid.NumVoxelsPerLength() );

			mShadowPanumbraEffect->UpdatePosShadowIntensityInputSrv(mDeferredBufferResources.worldPosAndShadowIntensitySrv.Get());

			auto camera = renderer->CurrentCamera();
			mShadowPanumbraEffect->UpdateSimGridCenterAndVoxelSizePerLength(DirectX::XMFLOAT4(
				camera->Position().x,
				camera->Position().y,
				camera->Position().z,
				mSimulationGrid.VoxelSizePerLength()));

			auto curr_sample = mShadowRaysResources.numComputedSamples;
			mShadowPanumbraEffect->UpdateCurrentSampleVar(curr_sample);

			// sampler here gets reused in the resolve pass as well
			D3D11_SAMPLER_DESC samp_desc;
			samp_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.MipLODBias = 0.0f;
			samp_desc.MaxAnisotropy = 16;
			samp_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			samp_desc.BorderColor[0] = samp_desc.BorderColor[1] = samp_desc.BorderColor[2] = samp_desc.BorderColor[3] = 1.f;
			samp_desc.MinLOD = -D3D11_FLOAT32_MAX;
			samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
			auto sampler = renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), samp_desc);
			mShadowPanumbraEffect->UpdateGenRaysSampler(sampler);

			samp_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			samp_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			samp_desc.MipLODBias = 0.0f;
			samp_desc.MaxAnisotropy = 4;
			samp_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			samp_desc.BorderColor[0] = samp_desc.BorderColor[1] = samp_desc.BorderColor[2] = samp_desc.BorderColor[3] = 1.f;
			samp_desc.MinLOD = -D3D11_FLOAT32_MAX;
			samp_desc.MaxLOD = D3D11_FLOAT32_MAX;
			sampler = renderer->StateMngr()->GetOrCreateSamplerState(renderer->Device(), samp_desc);
			mShadowPanumbraEffect->UpdateResolveSampler(sampler);

			mShadowPanumbraEffect->UpdateShadowRays(
				mShadowRaysResources.numRaySamples,
				mShadowRaysResources.rayListSrv.begin(),
				mShadowRaysResources.rayListUav.begin(),
				mShadowRaysResources.rayVoxelIdsSrv.begin(),
				mShadowRaysResources.rayVoxelIdsUav.begin(),
				mShadowRaysResources.rayHitSrv.begin(),
				mShadowRaysResources.rayHitUav.begin());

			int dispatch_params[3] = { (int)ceil(res[0] / 32.f), (int)ceil(res[1] / 32.f), 1 };
			mShadowPanumbraEffect->ApplyTechnique(HR_GENERATE_PENUMBRA_RAYS_TECH, dev_c);
			//if (light_size > 0)
			{
				if (dev_c->IsAnnotationEnabled())
					dev_c->SetMarkerInt(L"Penumbras Ray Gen + Dilation", 0);

				dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
			}
			mShadowPanumbraEffect->ClearTechnique("", dev_c);
						
			// Update View Projection matrix variable		
			// TODO:: make the following work for more than 1 light
			std::vector<LightToGpu> lights_to_gpu;
			std::vector<ID3D11ShaderResourceView*> shadow_map_cascades;
			renderer->SetupLightsAndShadowMapsForGPU(lights_to_gpu, shadow_map_cascades);
			mShadowPanumbraEffect->UpdateViewProjectionMatrixVariable((lights_to_gpu.begin())->shadowMatrix);
			mShadowPanumbraEffect->UpdateLightShadowIntensityVar((lights_to_gpu.begin())->shadowIntensity);

			dispatch_params[0] = (int)ceil(res[0] / 8.f);
			dispatch_params[1] = (int)ceil(res[1] / 8.f);
			
			mShadowPanumbraEffect->UpdateDispatchParams(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
			mShadowPanumbraEffect->UpdateResolutionVar(res[0], res[1]);
			mShadowPanumbraEffect->UpdateMaxDilationVar(mMaxPenumbraDilation);
			mShadowPanumbraEffect->ApplyTechnique(HR_GENERATE_PENUMBRA_DILATION_MASK_TECH, dev_c);
			if (light_size > 0)
				dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
			mShadowPanumbraEffect->ApplyTechnique(HR_APPLY_PENUMBRA_DILATION_MASK_TECH, dev_c);
			if (light_size > 0)
				dev_c->Dispatch(dispatch_params[0], dispatch_params[1], dispatch_params[2]);
			mShadowPanumbraEffect->ClearTechnique("", dev_c);
		}
	}
	gGpuProfiler.Timestamp(GTS_PenumbraDilation);
	

}

void 
HybridPipeline::AdvanceRaySamples(PerRayTypeResources& rayResources)
{
	for (auto i = 0u; i < rayResources.numRaySamplesPerFrame; i++)
	{
		if (rayResources.numComputedSamples+1 < rayResources.numRaySamples)
			++rayResources.numComputedSamples;
	}
}

void 
HybridPipeline::InitDeferredBuffers()
{
	auto const num_mipmaps = 5u;

	D3D11_TEXTURE2D_DESC tex_desc;
	ZeroMemory(&tex_desc, sizeof(tex_desc));
	tex_desc.Width = gApp->WinWidth();
	tex_desc.Height = gApp->WinHeight();
	tex_desc.ArraySize = 1;
	tex_desc.SampleDesc.Count = 1;
	tex_desc.Usage = D3D11_USAGE_DEFAULT;
	tex_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_RENDER_TARGET; 
	tex_desc.CPUAccessFlags = 0;
	tex_desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
	tex_desc.MipLevels = num_mipmaps;
	tex_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	auto hr = gApp->Renderer()->Device()->CreateTexture2D(&tex_desc, NULL, mDeferredBufferResources.worldPosAndShadowIntensityTexture.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
	ZeroMemory(&uav_desc, sizeof(uav_desc));
	uav_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;	
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mDeferredBufferResources.worldPosAndShadowIntensityTexture.Get(), &uav_desc, mDeferredBufferResources.worldPosAndShadowIntensityUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	ZeroMemory(&srv_desc, sizeof(srv_desc));
	srv_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv_desc.Texture2D.MostDetailedMip = 0;
	srv_desc.Texture2D.MipLevels = -1;
	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mDeferredBufferResources.worldPosAndShadowIntensityTexture.Get(), &srv_desc, mDeferredBufferResources.worldPosAndShadowIntensitySrv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
	ZeroMemory(&rtv_desc, sizeof(rtv_desc));
	rtv_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;	
	hr = gApp->Renderer()->Device()->CreateRenderTargetView(mDeferredBufferResources.worldPosAndShadowIntensityTexture.Get(), &rtv_desc, mDeferredBufferResources.worldPosAndShadowIntensityRtv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
}

void 
HybridPipeline::SignalCameraMotion()
{
	mShadowRaysResources.numComputedSamples = 0;
}
