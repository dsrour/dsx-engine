#include <assert.h>
#include <algorithm>
#include <d3dcompiler.h>
#include "Debug/Debug.h"
#include "Systems/Renderer/Shaders/ShaderDefines.h"
#include "BaseApp.h"
#include "ShadowPenumbraEffect.h"

extern std::wstring gEngineRootDir;
extern BaseApp* gApp;

ShadowPenumbraEffect::ShadowPenumbraEffect(std::shared_ptr<D3dRenderer> renderer)
{
	mRendererRef = renderer.get(); 
	Init(mRendererRef);

	mDilationCb.Create(renderer->Device());
	ZeroMemory(&mDilationCbVars, sizeof(mDilationCbVars));

	mResolveCb.Create(renderer->Device());
	ZeroMemory(&mResolveCbVars, sizeof(mResolveCbVars));

	mGenRaysCb.Create(renderer->Device());
	ZeroMemory(&mGenRaysCbVars, sizeof(mGenRaysCbVars));
	
	mNumSamples = 0;
	mPosShadowIntensityInputSrv = nullptr;
}

/*virtual*/
ShadowPenumbraEffect::~ShadowPenumbraEffect()
{    
	
}

void
ShadowPenumbraEffect::Init( D3dRenderer* const renderer, bool const& silentFail /*= false*/, bool const& forceCompile /*= false*/ )
{
	std::wstring cso_name;
	HRESULT hr;

	// Shaders
	cso_name = std::wstring(HR_GENERATE_PENUMBRA_RAYS_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mGenRaysBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mGenRaysBlob->GetBufferPointer(),
		mGenRaysBlob->GetBufferSize(),
		NULL,
		mGenRaysCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_CLEAR_PENUMBRA_DILATION_MASK_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mClearDilationMaskBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mClearDilationMaskBlob->GetBufferPointer(),
		mClearDilationMaskBlob->GetBufferSize(),
		NULL,
		mClearDilationMaskCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_GENERATE_PENUMBRA_DILATION_MASK_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mGenDilationMaskBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mGenDilationMaskBlob->GetBufferPointer(),
		mGenDilationMaskBlob->GetBufferSize(),
		NULL,
		mGenDilationMaskCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));	

	cso_name = std::wstring(HR_APPLY_PENUMBRA_DILATION_MASK_CS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mApplyDilationMaskBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateComputeShader(
		mApplyDilationMaskBlob->GetBufferPointer(),
		mApplyDilationMaskBlob->GetBufferSize(),
		NULL,
		mApplyDilationMaskCs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_FULL_SCREEN_TRI_VS_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mResolveVsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreateVertexShader(
		mResolveVsBlob->GetBufferPointer(),
		mResolveVsBlob->GetBufferSize(),
		NULL,
		mResolveVs.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_RESOLVE_SHADOWS_PS_4_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mResolve4PsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mResolve4PsBlob->GetBufferPointer(),
		mResolve4PsBlob->GetBufferSize(),
		NULL,
		mResolve4Ps.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_RESOLVE_SHADOWS_PS_8_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mResolve8PsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mResolve8PsBlob->GetBufferPointer(),
		mResolve8PsBlob->GetBufferSize(),
		NULL,
		mResolve8Ps.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	cso_name = std::wstring(HR_RESOLVE_SHADOWS_PS_16_FILE) + L".cso";
	hr = D3DReadFileToBlob(cso_name.c_str(), mResolve16PsBlob.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));
	hr = renderer->Device()->CreatePixelShader(
		mResolve16PsBlob->GetBufferPointer(),
		mResolve16PsBlob->GetBufferSize(),
		NULL,
		mResolve16Ps.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	// Dilation resources
	ZeroMemory(&mDilationResources, sizeof(mDilationResources));
	mDilationResources.dilationMaskBufferDesc.ByteWidth = (UINT)(sizeof(float) * 4 * (gApp->WinWidth() * gApp->WinHeight()));
	mDilationResources.dilationMaskBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	mDilationResources.dilationMaskBufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	mDilationResources.dilationMaskBufferDesc.CPUAccessFlags = 0;
	mDilationResources.dilationMaskBufferDesc.MiscFlags = 0;
	mDilationResources.dilationMaskBufferDesc.StructureByteStride = (UINT)(sizeof(float));
	hr = gApp->Renderer()->Device()->CreateBuffer(&mDilationResources.dilationMaskBufferDesc, NULL, mDilationResources.dilationMaskBuffer.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mDilationResources.dilationMaskUavDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	mDilationResources.dilationMaskUavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	mDilationResources.dilationMaskUavDesc.Buffer.FirstElement = 0;
	mDilationResources.dilationMaskUavDesc.Buffer.Flags = 0;
	mDilationResources.dilationMaskUavDesc.Buffer.NumElements = (UINT)(gApp->WinWidth() * gApp->WinHeight());
	hr = gApp->Renderer()->Device()->CreateUnorderedAccessView(mDilationResources.dilationMaskBuffer.Get(), &mDilationResources.dilationMaskUavDesc, mDilationResources.dilationMaskUav.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));

	mDilationResources.dilationMaskSrvDesc.Format = mDilationResources.dilationMaskUavDesc.Format;
	mDilationResources.dilationMaskSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	mDilationResources.dilationMaskSrvDesc.Buffer.ElementWidth = mDilationResources.dilationMaskBufferDesc.StructureByteStride;
	mDilationResources.dilationMaskSrvDesc.Buffer.NumElements = mDilationResources.dilationMaskUavDesc.Buffer.NumElements;
	hr = gApp->Renderer()->Device()->CreateShaderResourceView(mDilationResources.dilationMaskBuffer.Get(), &mDilationResources.dilationMaskSrvDesc, mDilationResources.dilationMaskSrv.ReleaseAndGetAddressOf());
	assert(SUCCEEDED(hr));


}

void 
ShadowPenumbraEffect::ApplyTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	if (HR_GENERATE_PENUMBRA_RAYS_TECH == techName)
	{
		mGenRaysCb.SetData(deviceContext, mGenRaysCbVars);

		auto cb = mGenRaysCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);
		deviceContext->CSSetSamplers(0, 1, &mGenRaysSampler);

		auto list_uav = (*(mRaysListUavs + mDilationCbVars.currSample)).Get();
		auto voxel_uav = (*(mRaysVoxelIdsUavs + mDilationCbVars.currSample)).Get();
		ID3D11UnorderedAccessView* uavs[] = {
			list_uav,
			voxel_uav };
		unsigned int init_cnts[] = { 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 2, uavs, init_cnts);

		ID3D11ShaderResourceView* srvs[] = { mPosShadowIntensityInputSrv };
		deviceContext->CSSetShaderResources(0, 1, srvs);

		deviceContext->CSSetShader(mGenRaysCs.Get(), nullptr, 0);
	}
	else if (HR_GENERATE_PENUMBRA_DILATION_MASK_TECH == techName)
	{
		mDilationCb.SetData(deviceContext, mDilationCbVars);

		auto cb = mDilationCb.GetBuffer();
		deviceContext->CSSetConstantBuffers(0, 1, &cb);

		ID3D11UnorderedAccessView* uavs[] = { 	mDilationResources.dilationMaskUav.Get() };
		unsigned int init_cnts[] = { 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 1, uavs, init_cnts);
		
		auto list_srv = (*(mRaysListSrvs + mDilationCbVars.currSample)).Get();
		auto voxel_srv = (*(mRaysVoxelIdsSrvs + mDilationCbVars.currSample)).Get();
		ID3D11ShaderResourceView* srvs[] = { list_srv, voxel_srv };
		deviceContext->CSSetShaderResources(0, 2, srvs);

		deviceContext->CSSetShader(mClearDilationMaskCs.Get(), nullptr, 0);
		deviceContext->Dispatch(mDilationCbVars.dispatchParams[0], mDilationCbVars.dispatchParams[1], mDilationCbVars.dispatchParams[2]);

		deviceContext->CSSetShader(mGenDilationMaskCs.Get(), nullptr, 0);
	}
	else if (HR_APPLY_PENUMBRA_DILATION_MASK_TECH == techName)
	{	
		ID3D11ShaderResourceView* srvs[] = { nullptr, nullptr };
		deviceContext->CSSetShaderResources(0, 2, srvs);

		auto list_uav = (*(mRaysListUavs + mDilationCbVars.currSample)).Get();
		auto voxel_uav = (*(mRaysVoxelIdsUavs + mDilationCbVars.currSample)).Get();
		ID3D11UnorderedAccessView* uavs[] = { 
			list_uav,
			voxel_uav };
		unsigned int init_cnts[] = { 0, 0 };
		deviceContext->CSSetUnorderedAccessViews(0, 2, uavs, init_cnts);

		srvs[0] = mDilationResources.dilationMaskSrv.Get();
		deviceContext->CSSetShaderResources(0, 1, srvs);
		
		deviceContext->CSSetShader(mApplyDilationMaskCs.Get(), nullptr, 0);		
	}
	else if (HR_RESOLVE_SHADOWS_TECH == techName)
	{
		mResolveCb.SetData(deviceContext, mResolveCbVars);

		auto cb = mResolveCb.GetBuffer();
		deviceContext->PSSetConstantBuffers(0, 1, &cb);

		auto list_srv = (*(mRaysListSrvs + mDilationCbVars.currSample)).Get();
		std::vector<ID3D11ShaderResourceView*> srvs;
		srvs.push_back(list_srv);
		for (auto i = 0; i < mResolveCbVars.totalSamples; i++)
		{
			auto hit_srv = (*(mRayHitsSrvs + i)).Get();
			srvs.push_back(hit_srv);
		}		
		deviceContext->PSSetShaderResources(0, mResolveCbVars.totalSamples+1, &srvs[0]);

		deviceContext->PSSetSamplers(0, 1, &mResolveSampler);

		deviceContext->VSSetShader(mResolveVs.Get(), nullptr, 0);

		switch (mResolveCbVars.totalSamples)
		{
		case 1:
		case 4:
			deviceContext->PSSetShader(mResolve4Ps.Get(), nullptr, 0);
			break;
		case 8:
			deviceContext->PSSetShader(mResolve8Ps.Get(), nullptr, 0);
			break;
		case 16:
			deviceContext->PSSetShader(mResolve16Ps.Get(), nullptr, 0);
			break;
		default:
			break;
		};
	}
}

void
ShadowPenumbraEffect::ClearTechnique(std::string const& techName, ID3D11DeviceContext* const deviceContext)
{
	ID3D11Buffer* nb = nullptr;	
	ID3D11SamplerState* ns = nullptr;
	
	deviceContext->CSSetShader(nullptr, nullptr, 0);	
	deviceContext->CSSetConstantBuffers(0, 1, &nb);

	std::vector<ID3D11ShaderResourceView*> vec_srvs(mResolveCbVars.totalSamples + 1, nullptr);
	deviceContext->CSSetShaderResources(0, 3, &vec_srvs[0]);
	
	ID3D11UnorderedAccessView* uavs[] = {
		nullptr,
		nullptr,
		nullptr };
	unsigned int init_cnts[] = { 0, 0, 0 };
	deviceContext->CSSetUnorderedAccessViews(0, 3, uavs, init_cnts);

	deviceContext->VSSetShader(nullptr, nullptr, 0);
	deviceContext->PSSetShader(nullptr, nullptr, 0);

	deviceContext->PSSetShaderResources(0, mResolveCbVars.totalSamples + 1, &vec_srvs[0]);
	
	deviceContext->PSSetSamplers(0, 1, &ns);
	deviceContext->PSSetConstantBuffers(0, 1, &nb);

	deviceContext->CSSetSamplers(0, 1, &ns);
}

void 
ShadowPenumbraEffect::UpdateDispatchParams(int const x, int const y, int const z)
{
	mDilationCbVars.dispatchParams[0] = x;
	mDilationCbVars.dispatchParams[1] = y;
	mDilationCbVars.dispatchParams[2] = z;
}

void
ShadowPenumbraEffect::UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX const& vp)
{
	mDilationCbVars.vpMat = DirectX::XMMatrixTranspose(vp);
}
