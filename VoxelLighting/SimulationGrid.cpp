#include "SimulationGrid.h"

#include <algorithm>

#include "Utils/ComponentFetches.h"
#include "Systems/Renderer/Camera.h"

void 
SimulationGrid::CullEntitiesOutOfGrid(std::shared_ptr<Camera> const& camera, std::set<unsigned int>& entities)
{
	float grid_length = mAttrs.numVoxelsPerLength * mAttrs.voxelSizePerLength;
	float grid_half_length = grid_length / 2.f;	
	auto center = DirectX::XMFLOAT3(camera->Position().x, camera->Position().y, camera->Position().z);
	
	DirectX::BoundingBox grid_aabb(center, DirectX::XMFLOAT3(grid_half_length, grid_half_length, grid_half_length));

	for (auto iter = entities.begin(); iter != entities.end();) 
	{
		auto entity_bounded = BoundedFromEntity(*iter);
		auto entity_spatialized = SpatializedFromEntity(*iter);		

		if (entity_bounded && entity_spatialized)
		{
			DirectX::XMMATRIX world = DirectX::XMLoadFloat4x4(&entity_spatialized->LocalTransformation());
			auto aabb = entity_bounded->AabbBounds();

			aabb.Transform(aabb, DirectX::XMLoadFloat4x4(&entity_spatialized->LocalTransformation()));

			// perform box/box intersection test in local space and cull out if test doesn't pass
			if (grid_aabb.Contains(aabb) == DirectX::DISJOINT)
				entities.erase(iter++);
			else
				++iter;
		}
		else 
			++iter;
	}
}

std::set<unsigned int> 
SimulationGrid::IntersectGridWithAabb(DirectX::XMFLOAT3& center, DirectX::BoundingBox& aabb)
{
	std::set<unsigned int> ret;

	DirectX::XMFLOAT3 pos_1 = {
		aabb.Center.x - aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z };

	DirectX::XMFLOAT3 pos_2 = {
		aabb.Center.x + aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z };

	DirectX::XMFLOAT3 pos_3 = {
		aabb.Center.x - aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z };

	DirectX::XMFLOAT3 pos_4 = {
		aabb.Center.x - aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z };

	DirectX::XMFLOAT3 pos_5 = {
		aabb.Center.x + aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z - aabb.Extents.z };

	DirectX::XMFLOAT3 pos_6 = {
		aabb.Center.x + aabb.Extents.x,
		aabb.Center.y - aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z };

	DirectX::XMFLOAT3 pos_7 = {
		aabb.Center.x - aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z };

	DirectX::XMFLOAT3 pos_8 = {
		aabb.Center.x + aabb.Extents.x,
		aabb.Center.y + aabb.Extents.y,
		aabb.Center.z + aabb.Extents.z };

	int max_i = 0;
	int max_j = max_i;
	int max_k = max_i;
	int min_i = 1024 * 1024 * 1024;
	int min_j = min_i;
	int min_k = min_i;

	int i,j,k;

	XyzToIjk(center, pos_1, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_2, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_3, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_4, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_5, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_6, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_7, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;
	XyzToIjk(center, pos_8, i, j, k);
	if (i > max_i) max_i = i;
	if (j > max_j) max_j = j;
	if (k > max_k) max_k = k;
	if (i < min_i) min_i = i;
	if (j < min_j) min_j = j;
	if (k < min_k) min_k = k;

	if (min_i < 0)
		min_i = 0;
	if (min_j < 0)
		min_j = 0;
	if (min_k < 0)
		min_k = 0;

	auto max_voxel_index = (int)mAttrs.numVoxelsPerLength - 1;

	if (min_i < 0)
		min_i = 0;
	if (min_j < 0)
		min_j = 0;
	if (min_k < 0)
		min_k = 0;

	if (max_i > max_voxel_index)
		max_i = max_voxel_index;
	if (max_j > max_voxel_index)
		max_j = max_voxel_index;
	if (max_k > max_voxel_index)
		max_k = max_voxel_index;

	for (i = min_i; i <= max_i; i++)
		for (j = min_j; j <= max_j; j++)
			for (k = min_k; k <= max_k; k++)
				ret.insert(IjkToCellId(i, j, k));

	return ret;
}

int 
SimulationGrid::CellIdFromPoint(DirectX::XMFLOAT3& center, DirectX::XMFLOAT3& pos)
{
	// xyz to ijk
	int i,j,k;
	XyzToIjk(center, pos, i, j, k);

	// ijk to index
	return IjkToCellId(i,j,k);
}

void 
SimulationGrid::XyzToIjk(DirectX::XMFLOAT3& center, DirectX::XMFLOAT3& xyz, int& iOut, int& jOut, int& kOut)
{
	float grid_length = mAttrs.numVoxelsPerLength * mAttrs.voxelSizePerLength;
	float grid_half_length = grid_length / 2.f;
	
	DirectX::XMFLOAT3 sim_grid_min = { center.x - grid_half_length, center.y - grid_half_length, center.z - grid_half_length };
		
	// xyz to ijk
	iOut = (int)std::floorf((xyz.x - sim_grid_min.x) / mAttrs.voxelSizePerLength);
	jOut = (int)std::floorf((xyz.y - sim_grid_min.y) / mAttrs.voxelSizePerLength);
	kOut = (int)std::floorf((xyz.z - sim_grid_min.z) / mAttrs.voxelSizePerLength);
}

unsigned int 
SimulationGrid::IjkToCellId(unsigned int const i, unsigned int const j, unsigned int const k)
{
	return i + (j * mAttrs.numVoxelsPerLength) + (k * mAttrs.numVoxelsPerLength * mAttrs.numVoxelsPerLength);
}
