//#pragma once
//
//#include <memory>
//#include "DebugEffect.h"
//
//class HybridPipeline;
//
///*  A messy "do-it-all" pipeline used for general debugging on the GPU.
// *  It should have multiple "states" that the client can set by providing
// *  the appropriate resources (from other pipelines) that this debug pipeline 
// *  will use.  All GPU code should come from 1 "ubershader" effect... 
// *
// *  ====== This will obviously not scale in the long run! ======
// */
//
//#include "System.h"
//#include "Systems/Renderer/Pipeline.h"
////#include "DebugEffect.h"
//
//class DebugPipeline : public Pipeline
//{
//public:
//	DebugPipeline(std::shared_ptr<D3dRenderer> renderer, std::shared_ptr<EntityManager> entityManager);
//	~DebugPipeline();
//
//	virtual void
//	MadeActive(void);
//
//	virtual void
//	MadeInactive(void);
//
//	virtual void
//	EnterPipeline(std::map< unsigned int, std::set<unsigned int> >& familyByRenderableType, double const currentTime);
//
//	virtual void
//	RecompileShaders();
//
//	void 
//	DebugVoxelLightingPipeline(std::shared_ptr<HybridPipeline> voxelLightPipeline);
//	
//private:
//	void
//	UnbindResources(std::shared_ptr<D3dRenderer> renderer, std::string const& passName);	
//
//	void
//	DebugVoxelLightPipeline();
//
//	void
//	ReleaseCommonResources();
//
//	void
//	ReleaseVoxelGridResources();
//
//
//	std::shared_ptr<DebugEffect> mDebugEffect;
//
//	// Indirect call buffer
//	D3D11_BUFFER_DESC mIndDrawArgsDesc;
//	D3D11_UNORDERED_ACCESS_VIEW_DESC mIndDrawArgsUavDesc;
//	ID3D11Buffer* mIndDrawArgsBuff;
//	ID3D11UnorderedAccessView* mIndDrawArgsUav;
//
//
//	///////////////////////////////////////////////////////////
//	std::shared_ptr<HybridPipeline> mVoxLightPln;
//	
//	D3D11_BUFFER_DESC mOccupiedVoxelsDesc;
//	D3D11_UNORDERED_ACCESS_VIEW_DESC mOccupiedVoxelsUavDesc;
//	D3D11_SHADER_RESOURCE_VIEW_DESC mOccupiedVoxelsSrvDesc;
//	ID3D11Buffer* mOccupiedVoxelsBuff;
//	ID3D11UnorderedAccessView* mOccupiedVoxelsUav;
//	ID3D11ShaderResourceView* mOccupiedVoxelsSrv;
//	///////////////////////////////////////////////////////////
//};
//
