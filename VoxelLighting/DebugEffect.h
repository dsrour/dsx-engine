//#pragma once
//
//#include "Systems/Renderer/Renderer.h"
//#include "Systems/Renderer/Shaders/ShaderDefines.h"
//#include "Components/Renderable.h"
//#include "Systems/Renderer/Effect.h"
//
//class DebugEffect : public Effect
//{
//public:
//    DebugEffect(std::shared_ptr<D3dRenderer> renderer);       
//    
//    virtual 
//    ~DebugEffect(void); 
//	
//	virtual void
//	Recompile() { if(mRendererRef) Init(mRendererRef, true, true); }
//
//	struct VoxelgridConstants
//	{		
//		DirectX::XMFLOAT4 simGridCenterAndVoxelSizePerLength;
//		int				  numVoxelsPerLength;
//	};
//	void
//	UpdateVoxelGridConstantVariables(VoxelgridConstants& vars);
//	
//	void
//	UpdateVoxelGridBuffers(
//		ID3D11ShaderResourceView* const binaryVoxelGridSrv,
//		ID3D11UnorderedAccessView* const occupiedVoxelsUav,
//		ID3D11ShaderResourceView* const occupiedVoxelsSrv );
//
//	void
//	UpdateCommonBuffers(
//	ID3D11UnorderedAccessView* const drawInstIndArgsBuff);
//
//	ID3DX11EffectShaderResourceVariable* const
//	BinaryGridSrv() const { return mBinaryGridSrv; }
//
//	void
//	UpdateViewProjectionMatrixVariable(DirectX::XMMATRIX& mat);
//                
//private:     
//	void 
//	Init(D3dRenderer* const renderer, bool const& silentFail = false, bool const& forceCompile = false);
//
//    // Shader Variables		
//	ID3DX11EffectScalarVariable*			   mNumVoxelsPerLength;
//	ID3DX11EffectVectorVariable*			   mSimGridCenterAndVoxelSizePerLength;
//	
//	ID3DX11EffectShaderResourceVariable*		mBinaryGridSrv;
//	ID3DX11EffectUnorderedAccessViewVariable*	mOccupiedVoxelsUav;
//	ID3DX11EffectUnorderedAccessViewVariable*	mDrawIndInstArgsUav;
//	ID3DX11EffectShaderResourceVariable*		mOccupiedVoxels;
//
//	ID3DX11EffectMatrixVariable*				mViewProj;
//	
//	D3dRenderer* mRendererRef; /// Kept for recompile() func
//};